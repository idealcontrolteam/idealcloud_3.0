console.log('Loaded service worker!');

self.addEventListener('push', ev => {
  const data = ev.data.json();
  console.log('Got push', data);
  self.registration.showNotification(data.title, {
    body: data.body,
    icon: 'https://media-exp1.licdn.com/dms/image/C4E0BAQEywhxcAf-5IQ/company-logo_200_200/0/1623875158997?e=2159024400&v=beta&t=xFsiaufHr_7IonQQskZ1zckX7xTL1SCYWrpIlYAQoYM'
  });
});