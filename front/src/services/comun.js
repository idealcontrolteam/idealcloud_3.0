import axios from 'axios';
import { API_ROOT} from '../api-config';
import moment from 'moment';

const APItag1 = `${API_ROOT}/tag/filteroregister/true`;
//let empresa=sessionStorage.getItem("Empresa");
//console.log(APItag1);
export class TagServices {    

    getDataTag() { 
        const token = "tokenFalso"; 
        return axios.get(APItag1, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
   
     }

     getAreas() { 
      const EndPoint = `${API_ROOT}/area`;
      return axios.get(EndPoint)
      .then(response => response.data.data)
      .catch((e)=>e)
   }

     getDataIdTags(id) { 
      const token = "tokenFalso"; 
      return axios.get(APItag1+'/'+id, {
         headers: {  
           'Authorization': 'Bearer ' + token
         }
       })
       .then(response =>  response.data.data)
 
   }

  reformatDate(dateTime){
    var fecha=new Date(dateTime);
    var zona_horaria=new Date(dateTime).getTimezoneOffset();
    zona_horaria=zona_horaria/60;
    fecha.setHours(fecha.getHours()+zona_horaria);
    return fecha
  }

  getRegistros(id,prom){// no se esta ocupando
      var date = moment().subtract(1, 'hours');
      var minute = date.minutes();
      let now = new Date(); 
      const f1 = date.subtract(prom, 'minutes').format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
      let f2 = prom==null?moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z":moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
      //console.log(f1)
    
      let f1_split=f1.split("T");
      let f2_split=f2.split("T");
      //const API1 = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
      //const EndPoint = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
      //const EndPoint =`${API_ROOT}/${empresa}/registros/sonda/xy/${id}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
      let EndPoint = prom==null?
      `${API_ROOT}/measurement/xy/tag/${id}/${f1_split[0]}T00:00:00.000Z/${f2_split[0]}T23:59:59.000Z`:
      `${API_ROOT}/measurement/xy/tag/${id}/${f1}/${f2}`;
      //console.log(EndPoint)

      return axios
      .get(EndPoint,  {
          "login_usuarios": "purrutia",
          "clave_usuarios": "purrutia"
        })
      .then(response => prom==null?response.data.data:response.data)
  }

  getRegistros2(id,prom){ //No se esta cupando
    var date = moment().subtract(6, 'hours');
    var minute = date.minutes();
    let now = new Date(); 
    const f1 = date.subtract(prom, 'minutes').format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    let f2 = moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    //console.log(f1)
  
    let f1_split=f1.split("T");
    let f2_split=f2.split("T");
    //const API1 = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
    //const EndPoint = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
    //const EndPoint =`${API_ROOT}/${empresa}/registros/sonda/xy/${id}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
    let EndPoint = prom==null?
    `${API_ROOT}/measurement_today/xy/tag/active/${id}/${f1_split[0]}T${f1_split[1]}/${f2_split[0]}T${f2_split[1]}`:
    `${API_ROOT}/measurement/xy/tag/active/${id}/${f1}/${f2}`;
    //console.log(EndPoint)

    return axios
    .get(EndPoint,  {
        "login_usuarios": "purrutia",
        "clave_usuarios": "purrutia"
      })
    .then(response => prom==null?response.data.data:response.data)
}

getRegistrosToday(id,format,type){
  var date = new Date();
  if(type=="control"){
    date = moment().subtract(3, 'hours');
  }else{
    date = moment().subtract(6, 'hours');
  }
  var minute = date.minutes();
  let now = new Date(); 
  let f1 = date.format(`${format!=null?format:"YYYY-MM-DDTHH:mm:ss"}`) + ".000Z";
  let f2 = moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
  //console.log(f1)

  let f1_split=f1.split("T");
  let f2_split=f2.split("T");
  //const API1 = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
  //const EndPoint = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
  //const EndPoint =`${API_ROOT}/${empresa}/registros/sonda/xy/${id}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
  let EndPoint = "";
  if(type=="control"){
    EndPoint=`${API_ROOT}/measurement-today-control/location/${id}/${f1_split[0]}T${f1_split[1]}/${f2_split[0]}T${f2_split[1]}`
  }else{
    EndPoint=`${API_ROOT}/registertoday/location/${id}/${f1_split[0]}T${f1_split[1]}/${f2_split[0]}T${f2_split[1]}`
  }
  
  //console.log(EndPoint)

  return axios
  .get(EndPoint,  {
      "login_usuarios": "purrutia",
      "clave_usuarios": "purrutia"
    })
  .then(response => response.data.data)
}

  getZones(id,metodo){
    const EndPoint = `${API_ROOT}/workPlace/${id}/zone`;
    return axios({method: metodo,url:EndPoint})
    .then(response => response)
    .catch((e)=>e)
  }

  getAlarm(accion){
    const EndPoint = `${API_ROOT}/alarm/accion/${accion}`;
    return axios.get(EndPoint)
    .then(response => response)
    .catch((e)=>e)
  }

  getTagsWorkplace(id,metodo){
    const EndPoint = `${API_ROOT}/tag/${id}/workPlace`;
    return axios({method: metodo,url:EndPoint})
    .then(response => response.data)
    .catch((e)=>e)
  }

  getZonesTags(id){
    const EndPoint = `${API_ROOT}/zone/${id}/oxd`;
    return axios.get(EndPoint)
    .then(response => response.data.data)
    .catch((e)=>e)
  }

  getLocation(id){
    const EndPointLocation = `${API_ROOT}/zone/${id}/location`;
    return axios.get(EndPointLocation)
    .then(response=>response)
    .catch((e)=>e)
  }

  updateLocation(id,data){
    const EndPointLocation = `${API_ROOT}/location/${id}`;
    return axios.put(EndPointLocation,data)
    .then(response=>response)
    .catch((e)=>e)
  }

  updateTag(id,data){
    const EndPointLocation = `${API_ROOT}/tag/${id}`;
    return axios.put(EndPointLocation,data)
    .then(response=>response)
    .catch((e)=>e)
  }

  updateTagForLocation(id,data){
    const EndPointLocation = `${API_ROOT}/tag/${id}/location_tags`;
    return axios.put(EndPointLocation,data)
    .then(response=>response)
    .catch((e)=>e)
  }

 

  updateWorkplace(id,data){
    const EndPointLocation = `${API_ROOT}/workplace/${id}`;
    return axios.put(EndPointLocation,data)
    .then(response=>response)
    .catch((e)=>e)
  }

  getWorkplace(id){
    let EndPoint = `${API_ROOT}/workplace/${id}`;
    return axios.get(EndPoint)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  getWorkplaceUser(centros){
    let data={
      centros:centros
    }
    let EndPoint = `${API_ROOT}/workplace/user_centros`;
    return axios.post(EndPoint,data)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  validateTokenSMA(data){
    let EndPoint = `https://conexiones.sma.gob.cl/api/v1/auth`;
    return axios.post(EndPoint,data,{
      headers: {'content-type': 'application/json'}
    })
    .then(response=>response)
    .catch((e)=>e)
  }

  getCycles(id){
    let EndPoint = `${API_ROOT}/cycles/workplace/${id}`;
    return axios.get(EndPoint)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  getArrayCycles(data){
    let EndPoint = `${API_ROOT}/cycles/workplaces`;
    return axios.post(EndPoint,data)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  getTags(id){
    let EndPoint=`${API_ROOT}/location/${id}/tag`;
    return axios.get(EndPoint)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  getfindEventActive(id){
    let EndPoint=`${API_ROOT}/event/${id}/active`;
    return axios.get(EndPoint)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  getfindEventActiveEnd(ids){
    let EndPoint=`${API_ROOT}/event/workplaceIds/active_end`;
    return axios.post(EndPoint,ids)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  getfindEvent(id){
    let EndPoint=`${API_ROOT}/event/workplace/${id}`;
    return axios.get(EndPoint)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  updateActivity(id,obj){
    let EndPoint=`${API_ROOT}/activity/${id}`
    return axios.put(EndPoint,obj)
    .then(response => response.data.data)
    .catch((e)=>e);
  }

  updateEvent(id,obj){
    let EndPoint=`${API_ROOT}/event/${id}`
    return axios.put(EndPoint,obj)
    .then(response => response.data.data)
    .catch((e)=>e);
  }
  
  getUserToken(token){
    let EndPoint=`${API_ROOT}/user/check_token/${token}`
    return axios.get(EndPoint)
    .then(response => response.data.data)
    .catch((e)=>e);
  }

  getWorkplaceCode(code){
    let EndPoint=`${API_ROOT}/workplace/${code}/centro`
    return axios.get(EndPoint)
    .then(response => response.data.data)
    .catch((e)=>e);
  }

  getWorkplaceCompany(id){
    let EndPoint=`${API_ROOT}/workplace/${id}/company`
    return axios.get(EndPoint)
    .then(response => response.data.data)
    .catch((e)=>e);
  }

  crudCycles(id,data,metodo){
    let EndPoint="";
    if(id!=""){
      EndPoint=`${API_ROOT}/cycles/${id}`
    }else{
      EndPoint=`${API_ROOT}/cycles`
    }
    return axios({method:metodo, url:EndPoint, data})
    .then(response => response.data)
    .catch((e)=>e);
  }

  crudArea(id,data,metodo){
    let EndPoint="";
    if(id!=""){
      EndPoint=`${API_ROOT}/area/${id}`
    }
    else{
      EndPoint=`${API_ROOT}/area`
    }
    return axios({method:metodo, url:EndPoint, data})
    .then(response => response.data)
    .catch((e)=>e);
  }

  crudtagConfigControl(id,data,metodo){
    let EndPoint="";
    if(id!=""){
      EndPoint=`${API_ROOT}/tagConfigControl/${id}`
    }
    else{
      EndPoint=`${API_ROOT}/tagConfigControl`
    }
    return axios({method:metodo, url:EndPoint, data})
    .then(response => response.data)
    .catch((e)=>e);
  }
  
  getEndRegistroSMA(id){
    let EndPoint = `${API_ROOT}/register_sma/dispositivo/${id}`;
    return axios.get(EndPoint)
    .then(response=>response.data.data)
    .catch((e)=>e)
  }

  crudCompany(id,data,metodo){
    let EndPoint="";
    if(id!=""){
      EndPoint=`${API_ROOT}/company/${id}`
    }
    else{
      EndPoint=`${API_ROOT}/company`
    }
    return axios({method:metodo, url:EndPoint, data})
    .then(response => response.data)
    .catch((e)=>e);
  }

  crudCentro(id,data,metodo){
    let EndPoint="";
    if(id!=""){
      EndPoint=`${API_ROOT}/workplace/${id}`
    }
    else{
      EndPoint=`${API_ROOT}/workplace`
    }
    return axios({method:metodo, url:EndPoint, data})
    .then(response => response.data)
    .catch((e)=>e);
  }

  getRole(){
    let EndPoint = `${API_ROOT}/role`;
    return axios.get(EndPoint)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getTagsEfectividad(tags){
    let EndPoint = `${API_ROOT}/measurement_today/tags/efectividad`;
    return axios.post(EndPoint,tags)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getWorkplaceEfectividadMonth(workplaceId,fechaInicio,fechaFin){
    let EndPoint = `${API_ROOT}/efectivity_month/workplace/${workplaceId}/${fechaInicio}.000Z/${fechaFin}.000Z`;
    return axios.get(EndPoint)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getWorkplaceEfectividadDay(workplaceId,fechaInicio,fechaFin){
    let EndPoint = `${API_ROOT}/efectivity_day/workplace`;
    let data={
      workplaceId:workplaceId,
      fini:fechaInicio+".000Z",
      ffin:fechaFin+".000Z",
    }
    return axios.post(EndPoint,data)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getWorkplaceArrowEfectividad(workplaceId,f1,f2,f3,f4){
    let EndPoint=`${API_ROOT}/efectivity_day/workplace/arrow/${workplaceId}/${f1}.000Z/${f2}.000Z/${f3}.000Z/${f4}.000Z`
    return axios.get(EndPoint)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getWorkplaces_array(workplacesId){
    let EndPoint=`${API_ROOT}/workplace/workplaces_array`
    return axios.post(EndPoint,workplacesId)
    .then(response=>response)
    .catch((e)=>e)
  }

  getSensors_array(sensorsId,f1,f2){
    let data={
      ids:sensorsId,
      fini:f1,
      ffin:f2
    }
    let EndPoint=`${API_ROOT}/register_sma/sensor/array`
    return axios.post(EndPoint,data)
    .then(response=>response)
    .catch((e)=>e)
  }

  getTendenciaAlarmsActive(fini,ffin){
    let EndPoint=`${API_ROOT}/alarm/tendencia/${fini}.000Z/${ffin}.000Z/active`
    return axios.get(EndPoint)
    .then(response=>response)
    .catch((e)=>e)
  }

  getTendenciaAlarms(fini,ffin){
    let EndPoint=`${API_ROOT}/alarm/tendencia/${fini}.000Z/${ffin}.000Z`
    return axios.get(EndPoint)
    .then(response=>response)
    .catch((e)=>e)
  }

  getCodeAlarms(id){
    let EndPoint=`${API_ROOT}/alarm/find/${id}`
    return axios.get(EndPoint)
    .then(response=>response)
    .catch((e)=>e)
  }

  uptadeAlarmsId(id,data){
    let EndPoint=`${API_ROOT}/alarm/${id}`
    return axios.put(EndPoint,data)
    .then(response=>response)
    .catch((e)=>e)
  }

  getTagsArray(locationsId){
    let EndPoint=`${API_ROOT}/tag/tag_array`
    return axios.post(EndPoint,locationsId)
    .then(response=>response)
    .catch((e)=>e)
  }
  
  createEmailSuport(data){
    let EndPoint=`${API_ROOT}/email_suport`
    return axios.post(EndPoint,data)
    .then(response=>response)
    .catch((e)=>e)
  }

  getTagsLocationForWorkplace(workplaceId){
    let EndPoint=`${API_ROOT}/tag/location/workplace/${workplaceId}`
    return axios.get(EndPoint)
    .then(response=>response.data)
    .catch((e)=>e)
  }
  
  upDataProm(mediciones){
    let EndPoint=`${API_ROOT}/measurement/multiple`
    return axios.post(EndPoint,mediciones)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getMeasurementForLocation(locationsId,fini,ffin){
    let data={
      locationsId,
      fini,
      ffin
    }
    let EndPoint=`${API_ROOT}/measurement/locations_array`
    return axios.post(EndPoint,data)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  getRegistrosServer(fechaInicio,fechaFin){
    let EndPoint=`${API_ROOT}/register_server/fechas/${moment(fechaInicio).format('YYYY-MM-DD HH:mm:00')}.000Z/${moment(fechaFin).format('YYYY-MM-DD HH:mm:59')}.000Z`
    return axios.get(EndPoint)
    .then(response=>response.data)
    .catch((e)=>e)
  }

  // getLocationIds(ids){
  //   let EndPoint=`${API_ROOT}/location/locations_ids`
  //   return axios.post(EndPoint,ids)
  //   .then(response=>response.data)
  //   .catch((e)=>e)
  // }
  

}