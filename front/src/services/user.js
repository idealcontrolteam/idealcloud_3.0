import axios from 'axios';
import { API_ROOT} from '../api-config';

const APItag1 = `${API_ROOT}/user/user_companys`;
const APItag2 = `${API_ROOT}/company`;

export  function currentAccount() {
   // const User = localStorage.getItem("User")
   // return User;

   return "Patricio Urrutia";
}

export  function getUserCompanys(email) {
   // const User = localStorage.getItem("User")
   // return User;
   const token = "tokenFalso"; 
   return axios.get(APItag1+'/'+email, {
      headers: {  
        'Authorization': 'Bearer ' + token
      }
    })
    .then(response =>  response.data.data)
}

export function getCompanys(){
   const token = "tokenFalso"; 
   return axios.get(APItag2, {
   headers: {  
       'Authorization': 'Bearer ' + token
   }
   })
   .then(response =>  response.data.data)
};

export function getWorkPlace(id){
   const token = "tokenFalso"; 
   return axios.get(APItag2+"/"+id+"/workPlace", {
   headers: {  
       'Authorization': 'Bearer ' + token
   }
   })
   .then(response =>  response.data.data)
};

export  function logout() {
   let url=window.location.href;
      if(url.includes('https://ihc.idealcontrol.cl')){
        window.location.replace("/idealcloud2");
        sessionStorage.clear();
      }else{
        window.location.replace("/");
        sessionStorage.clear();
      }
     //localStorage.removeItem("token");
 /// localStorage.removeItem("expirationDate"); 
}

export function sessionCheck(){
   try{
      if(
         // sessionStorage.getItem("Empresa") === null || 
         sessionStorage.getItem("token_cloud") === null && sessionStorage.getItem("token_cloud") === undefined){
         let url=window.location.href;
      if(url.includes('https://ihc.idealcontrol.cl')){
         window.location.replace("/idealcloud2");
         sessionStorage.clear();
      }else{
         window.location.replace("/");
         sessionStorage.clear();
      }
      }
   }catch(e){
      //console.log(e)
   }
}


export function checkStatusLogin() {
         // try{
         //    if(JSON.parse(sessionStorage.getItem("Empresa")) === null){
         //    let url=window.location.href;
         //    if(url.includes('https://ihc.idealcontrol.cl')){
         //       window.location.replace("/idealcloud2");
         //       sessionStorage.clear();
         //    }else{
         //       window.location.replace("/");
         //       sessionStorage.clear();
         //    }
         //    }
         // }catch(e){
         //    //console.log(e)
         // }
         return true;

        // if (token) {
        //   const expirationDate = localStorage.getItem("expirationDate");
        //   const dateNow = new Date();
        //   const dateExp = new Date(expirationDate);

        //   if (dateExp.getTime() < dateNow.getTime()) {
        //    // localStorage.removeItem("token");
        //     //localStorage.removeItem("expirationDate");
        //      // console.log("[TOKEN EXPIRED]");   
        //     return false;
        //   } else {        
        //     //console.log("[TOKEN VALID]");
        //     //console.log("EXPIRATION", new Date(dateExp.getTime()));
        //     //console.log("CURRENT DATE", new Date(dateNow.getTime()));
        //     return true;
        //   }
        // } else {
        //   //console.log("[NO TOKEN]");
        //   return false;

        // }

}

