

let backendHost;
let frontHost;
let backendHost_rupanquito;
const hostname = window && window.location && window.location.hostname;
 

if(hostname === 'ihc.idealcontrol.cl') {
  //backendHost = 'https://ihc.idealcontrol.cl/aquagen/apiv1/';
  backendHost_rupanquito="https://ihc.idealcontrol.cl/rupanquito/apiv1/";
  backendHost = 'https://ihc.idealcontrol.cl/ideal_cloud3/back/';
  frontHost = 'https://ihc.idealcontrol.cl/idealcloud2/';
} else {
    backendHost =  `http://${hostname}:3014`;
    backendHost_rupanquito="https://ihc.idealcontrol.cl/rupanquito/apiv1/";
    //backendHost_rupanquito =  `http://${hostname}:3003`;
    frontHost = `http://${hostname}:5014`;
}


export const API_ROOT = `${backendHost}`;
export const API_ROOT2 = `${frontHost}`;
export const API_ROOT3 = `${backendHost_rupanquito}`;

