import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';
import {Col, Row, Button, Form, FormGroup, Label, ModalHeader,Modal, ModalBody, ModalFooter,
    CardTitle, Card, CardBody,Input} from 'reactstrap';
import backgroundImage from '../../../assets/utils/images/sidebar/abstract2.jpg';  
import city3 from '../../../assets/utils/images/dropdown-header/headerbg1.jpg';
import {connect} from 'react-redux';
import {
    setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../reducers/Session';
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
// import CryptoJS from 'crypto-js';
//import sha256 from 'crypto-js/sha256';
// Layout
import { API_ROOT } from '../../../api-config';
import axios from "axios";
import {Empresas} from '../../Comun/data'
import * as _ from "lodash";
import {TagServices} from '../../../services/comun';



const APILogin= `${API_ROOT}/user/login`;
const app="idealcloud2.0";

console.log(Empresas);

let btn1=true;
let btn2=false;
let btn3=false;
let btn4=false;
let btn5=false;
let modal1=true;


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
                email: "",
                password: "",
                rol:"",
                empresa:"",
                status: "",
                message: "",
                modal1: true,
                btn1:true,
                btn2:false,
                btn3:false,
                btn4:false,
                card:[1],
                Centros:[],
        };
         this.ingresarNameUsuario = this.ingresarNameUsuario.bind(this);
         this.toggleModal1 = this.toggleModal1.bind(this);
         this.tagservices = new TagServices();
        
    }
    componentDidMount = () => {
        if(document.cookie=="bandera=1"){
            // this.setState({modal1:false})
            this.toggleModal1("-1")
         }else{
            document.cookie = "bandera=0;";
         }
    }
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.handleLogin();
        }
    }
    handleChange = event => {
            const name = event.target.name;
            const value = event.target.value;
            this.setState({ [name]: value });
    };

    ingresarNameUsuario = (usuario) => {
        let {setNameUsuario} = this.props;
        setNameUsuario(usuario);
    }
    ingresarEmailUsuario = (email) => {
        let {setEmailUsuario} = this.props;
        setEmailUsuario(email);
    }
    ingresarRolUsuario = (rol) =>{
        let {setRolUsuario} = this.props;
        setRolUsuario(rol);
    }
    ingresarCentroUsuario = (centro) =>{
        let {setCentroUsuario} = this.props;
        setCentroUsuario(centro);
    }
    
    handleLogin = () => {
        this.ingresarCentroUsuario("Login")
        // let empresas=this.state.empresas;
        let email=this.state.email;
        let split_user=email.split('@');
        let user=split_user[0];
        email=String(split_user[1]).split('.')
        //let empresa=Empresas.filter((data)=>email.includes(String(data.title))).map((data)=>{return data.name});
        let company=Empresas.filter((data)=>email.includes(String(data.title))).map((data)=>{return data.title});
        // console.log("empresa "+empresa)
        // console.log(empresa+" "+user)
        // console.log(String(email[0]))
        //alert(email[0])
        const data ={
            email: this.state.email,
            password: this.state.password
          };
        axios
        .post(`${API_ROOT}/user/login`,data)
                .then(response => {   
                if (response.data.statusCode === 200) {
                    sessionStorage.setItem("rol", response.data.data.rol);  
                    console.log(response.data)
                    let centros=response.data.data.centros;
                    let control=response.data.data.control;
                    sessionStorage.setItem("Empresa",company);
                    sessionStorage.setItem("Company",email[0]);
                    sessionStorage.setItem("token_cloud",response.data.data.token);
                    this.ingresarEmailUsuario(this.state.email);
                    this.ingresarRolUsuario(response.data.data.perfil);
                    // this.setState({ isloggedIn: true });
                    // let centro_split=centros.split(",");
                    //console.log(centro_split)
                    console.log(this.state.Centros);
                    this.getAreas();//
                    this.setCompany(String(email[0]),centros+control);
                    //centros.map()
                    
                } else {
                    this.setState({ isloggedIn: false });   
                }
                })
                .catch(error => {
                      
                        this.setState({ isloggedIn: false });
                         this.setState({ status: "error", message: "Credenciales no válidas" });
                });
     };

     setCompany=(empresa,c_centros)=>{
        //this.setState({empresa:empresa})
        // alert(c_centros)

        this.tagservices.getWorkplaceUser(c_centros).then(workplace=>{
            let centros=[]
            workplace.map(d=>{
                centros.push({
                    "code":d._id,
                    "codigo":d.code,
                    "name":d.name,
                    "active":d.active,
                    "endDate":d.endDate!=null?d.endDate:"",
                    "areaId":d.areaId!=null?d.areaId:"",
                })
            })
            console.log(centros)
            let new_centros=_.orderBy(centros, ['name'],['asc'])
            sessionStorage.setItem("Centros",JSON.stringify(new_centros));
            this.setState({ isloggedIn: true });
        }).catch(e=>{
            //console.log(e)
        })
     }

    getAreas(){
        axios
        .get(API_ROOT+`/area`)
        .then(response => {   
            // alert(response.data.statusCode==200);
            if(response.data.statusCode==200){
                let areas=response.data.data;
                sessionStorage.setItem("Areas",JSON.stringify(areas));
            }
        })
        .catch(error => {
            console.log(error)
        });
    }

     toggleModal1(valor) {
        if(valor=="-1"){
            modal1=false
            btn1=false;
            btn2=false;
            btn3=false;
            btn4=false;
           this.setState({
               modal1:false
            });
        }
        //this.setState({modal1:false})
    }

    inputChangeHandler = (event) => {
       // console.log(event.target.value);
       this.setState( {
           ...this.state,
           [event.target.id]: event.target.value
       } );
   }
   
   switchModalAnterior=()=>{
       if(btn2){
           btn1=true;
           btn2=false;
           btn3=false;
           btn4=false;
           btn5=false;
       }if(btn3){
           btn1=false;
           btn2=true;
           btn3=false;
           btn4=false;
           btn5=false;
       }
       // if(btn4){
       //     btn1=false;
       //     btn2=false;
       //     btn3=true;
       //     btn4=false;
       //     btn5=false;
       // }
       // if(btn5){
       //     btn1=false;
       //     btn2=false;
       //     btn3=false;
       //     btn4=true;
       //     btn5=false;
       // }
   }

   switchModalSiguiente=()=>{
       if(btn1){
           btn1=false;
           btn2=true;
           btn3=false;
           btn4=false;
           btn5=false;
           //alert(`btn1 ${btn1} btn2 ${btn2} btn3 ${btn3}`)
       }else if(btn2){
           btn1=false;
           btn2=false;
           btn3=true;
           btn4=false;
           btn5=false;
           //alert(`btn1 ${btn1} btn2 ${btn2} btn3 ${btn3}`)
       // }else if(btn3){
       //     btn1=false;
       //     btn2=false;
       //     btn3=false;
       //     btn4=true;
       //     btn5=false;
       // }
       // else if(btn4){
       //     btn1=false;
       //     btn2=false;
       //     btn3=false;
       //     btn4=false;
       //     btn5=true;
       }
       else if(btn3){
           modal1=false;
           localStorage.setItem("bandera","1");
       }
   }
 
   render()
    {

       
       let message = "";


       const errorStyle = {
         backgroundColor: "#f5d3d3",
         color: "black",
         textAlign: "center",
         borderRadius: "3px",
         border: "1px solid darkred",
         marginBottom: "15px",
         padding: "5px",
         fontSize: "15px",
         width: "100%"
       };
   
       // if (this.state.isloggedIn) {
       //     return <Redirect to="/dashboards/panelgeneral"></Redirect>
       // }
       if (this.state.isloggedIn) {
           return <Redirect to="/dashboards/resumen"></Redirect>
       }

       if (this.state.status === "error") {
           message = <div style={errorStyle}>{this.state.message}</div>;
       }
       return (

   <Fragment>
       {
           this.state.card.map(()=>{
               if(localStorage.getItem("bandera")=="0"){
                   return <Modal style={{maxHeight:660}} isOpen={modal1} size="xl">
                   {/* <ModalHeader toggle={() => this.toggleModal1("-1")}></ModalHeader> */}
                   <ModalBody>
                   <Row>
                       <Col>
                           <Card>
                               
                               <CardBody>
                                     <div style={{display:btn1?`block`:`none`}}>
                                     <img src={require(`../../../assets/logos/empresa/popup1.png`)} style={{width:`${100}%`,maxHeight:660}}></img>
                                     {/* <b style={{color:"#545cd8"}}><h1 style={{textAlign:"center",display:"none"}}>Bienvenido a la nueva plataforma de monitoreo.</h1>
                                     <h1 style={{textAlign:"center",display:"none"}}>Centros de mar para variables abióticas IDEAL CLOUD V2.0.</h1></b> */}
                                     </div>
                                     <div style={{display:btn2?`block`:`none`}}>
                                     {/* <h4>Esta plataforma ha sido diseñada con las ultimas tecnologías disponibles en el desarrollo de software. </h4>
                                     <h4> La nueva versión permite mejorar la experiencia de nuestros usuarios obteniendo mejor fluidez en la navegación.</h4>
                                     <h4>Optimizada para dispositivos móviles.</h4>
         
                                     (foto móvil) */}
                                     <img src={require(`../../../assets/logos/empresa/popup2.png`)} style={{width:`${100}%`,maxHeight:660}}></img> 
                                     </div>
         
                                     {/* <div style={{display:btn3?`block`:`none`}}>
                                     <h4>Esta nueva plataforma incorpora la opción de generar gráficos comparativos de tendencias con sensores 
                                         de diferentes centros, ideal para comparar el comportamiento en centros cercanos.</h4>
                                       (Foto comparador de centro)
                                     </div>
         
                                     <div style={{display:btn4?`block`:`none`}}>
                                     <h4>Permite la descriminacion de busqueda en los centros activos e inactivos.</h4>
         
                                     (foto)
                                     </div> */}
                                     <div style={{display:btn3?`block`:`none`}}>
                                     <h4>Para hacer uso de nuestra nueva plataforma solo debe ingresar como se ve en el ejemplo :</h4>
                                     <img src={require(`../../../assets/img/img.png`)} style={{width:`${100}%`,maxHeight:500,marginTop:15}}></img>
                                     <br></br>
                                     <h5><b>1.-</b>Nombre de usuario</h5>
                                     <h5><b>2.-</b>Nombre de Empresa (sin mayúscula)</h5>
                                     <h5><b>3.-</b>Contraseña (antigua)</h5>
                                     </div>
                                     <br></br>
                                     <div style={{marginLeft:`${45}%`}}>
                                     {
                                         this.state.card.map(()=>{
                                             if(btn1){
                                                 return <Fragment>
                                                     <button style={{width:28,height:28}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     {/* <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button> */}
                                                 </Fragment>
                                             }
                                             if(btn2){
                                                 return <Fragment>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button style={{width:28,height:28}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     {/* <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button> */}
                                                 </Fragment>
                                             }
                                             if(btn3){
                                                 return <Fragment>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button style={{width:28,height:28}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     {/* <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                                     <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button> */}
                                                 </Fragment>
                                             }
                                           //   if(btn4){
                                           //       return <Fragment>
                                           //           <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //           <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //           <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //           <button style={{width:28,height:28}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //           <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //       </Fragment>
                                           //   }
                                           //   if(btn5){
                                           //     return <Fragment>
                                           //         <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //         <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //         <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //         <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //         <button style={{width:28,height:28}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                                           //     </Fragment>
                                           //   }
                                         })
                                     }
                                     </div>
                               </CardBody>
                           </Card>
                       </Col>
                      
                   </Row>
         
                   </ModalBody>
                   <ModalFooter>
                 {/* <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button> */}
                 <Button color="primary" onClick={((e) => {
                   this.switchModalAnterior()
                   this.inputChangeHandler(e)
                 })} >Anterior</Button>{' '}
                 <Button color="primary" onClick={((e) => {
                   this.switchModalSiguiente()
                   this.inputChangeHandler(e)

                 })}>{btn3?"Finalizar":"Siguiente"}</Button>{' '}
         </ModalFooter>
       </Modal>
               }
           })
       }
         
       <div className="h-100 bg-heavy-rain bg-animation opacity-11" >  
           <div className="menu-header-image h-100 opacity-9"
               style={{
                   backgroundImage: 'url(' + city3 + ')'
               }}
           >                          
                           
           <div className="d-flex h-100 justify-content-center align-items-center">
               <Col md="8" className="mx-auto app-login-box">
                   <div className="modal-dialog w-100 mx-auto">
                       <div className="modal-content">
                           <div className="modal-body">
                              <div className="app-logo mx-auto mb-3 mt-4"/>
                               <div className="h5 modal-title text-center">
                                   <h4 className="mt-2">
                                       
                                       <div>Bienvenido</div>
                                       <span>Ingrese credenciales de acceso</span>
                                   </h4>
                               </div>
                               <Form>
                                   <Row form>
                                      {message}
                                       <Col md={12}>
                                       <FormGroup className="mt-2">
                                                   <Label for="exampleEmail" className="m-0">Usuario</Label>
                                                   <Input
                                                   type="email"
                                                   name="email"
                                                   id="exampleEmail"
                                                   placeholder="Usuario"
                                                   onChange={this.handleChange}
                                                   onKeyDown={this.handleKeyDown}
                                                
                                               />
                                               </FormGroup>
                                       </Col>
                                       <Col md={12}>
                                       <FormGroup className="mt-2">
                                                   <Label for="examplePassword" className="m-0">Contraseña</Label>
                                                   <Input
                                                       type="password"
                                                       name="password"
                                                       id="examplePassword"
                                                       placeholder="Contraseña "
                                                       onChange={this.handleChange}
                                                       onKeyDown={this.handleKeyDown}
                                                   />
                                       </FormGroup>
                                       </Col>
                                       {/* <Col md={12}>
                                       <FormGroup className="mt-2">
                                               <Label for="exampleEmpresa" className="m-0">Empresa</Label>
                                               <Input  
                                                type="select" 
                                                name="empresas"
                                               //  disabled={this.state.email!="" && this.state.password!=""?false:true}
                                                onChange={this.handleChange}
                                                onChange={((e)=>{
                                                   // alert(e.target.value);
                                                   this.setState({empresa:e.target.value})

                                               })} id="exampleSelect">
                                                   <option disabled selected>Seleccione una Empresa</option>
                                                   {
                                                       Empresas.map((data,i)=>{
                                                           return <option value={data.name}>{data.title}</option>
                                                       })
                                                   }
                                               </Input>
                                       </FormGroup>
                                       </Col> */}
                                   </Row>
                                  
                               </Form>
                             
                              
                           </div>
                           <div className="modal-footer clearfix">                              
                               <div className="float-right">
                                               <Button
                                                       onClick={this.handleLogin}
                                                       onKeyDown={this.handleKeyDown}
                                                       color="primary"
                                                       size="lg"
                                                       >
                                               Acceder</Button>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div className="text-center text-black opacity-10 mt-3">
                       Copyright &copy; IdealControl 2020
                   </div>
               </Col>
           </div>
             <div className=""
              
                       style={{
                           backgroundImage: 'url(' + backgroundImage + ')'
                       }}>
                   </div>
       </div>

       </div>
   </Fragment>
    );
    
    }
}



const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
    centroUsuario: state.Session.centroUsuario,
});

const mapDispatchToProps = dispatch => ({
    setNameUsuario: enable => dispatch(setNameUsuario(enable)),
    setEmailUsuario: enable => dispatch(setEmailUsuario(enable)),
    setRolUsuario: enable => dispatch(setRolUsuario(enable)),
    setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
