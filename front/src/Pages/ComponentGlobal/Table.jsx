import React from 'react'
import ReactTable from "react-table";

const Table = ({data,columns,filas}) => {
    return (
        <> 
            <ReactTable   
            style={{width:`${98}%`,marginLeft:15}}                                                        
            data={data}
        
            // loading= {false}
            showPagination= {true}
            showPaginationTop= {false}
            showPaginationBottom= {true}
            showPageSizeOptions= {false}
            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
            defaultPageSize={filas}
            columns={columns}
            
            className="-striped -highlight"
            />
        </>
    )
}

export default Table