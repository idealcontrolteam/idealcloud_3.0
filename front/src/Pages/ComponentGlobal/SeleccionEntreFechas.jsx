import React, { useEffect } from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import {Input, InputGroup, InputGroupAddon} from 'reactstrap';

const SeleccionEntreFechas = ({fechaInicio,fechaFin,setFechaInicio,setFechaFin}) => {
    let now = new Date();
    let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
    let end = moment(start).add(1, "days").subtract(1, "seconds");         
    let ranges = {
    "Solo hoy": [moment(start), moment(end)],
    "Solo ayer": [
    moment(start).subtract(1, "days"),
    moment(end).subtract(1, "days")
    ],
    "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
    "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
    "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
    "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
    "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
    "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
    "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
    };
    let local = {
    format: "DD-MM-YYYY HH:mm",
    sundayFirst: false
    };
    let maxDate = moment(start).add(24, "hour");

    useEffect(() => {
        if([fechaInicio,fechaFin].includes('')){
            setFechaInicio(start)
            setFechaFin(end)
        }
    }, [fechaInicio,fechaFin])
    

    const applyCallback = (startDate, endDate) =>{      
        setFechaInicio(startDate)
        setFechaFin(endDate)
    }

    return (
        <>
            <DateTimeRangeContainer
            ranges={ranges}
            start={start}
            end={end}
            local={local}
            maxDate={maxDate}
            applyCallback={applyCallback}
            //rangeCallback={rangeCallback}
            autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={fechaInicio?fechaInicio.format("DD-MM-YYYY"):start.format("DD-MM-YYYY")}
                    />
                        <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={fechaFin?fechaFin.format("DD-MM-YYYY"):end.format("DD-MM-YYYY")}
                    />
                </InputGroup>
            
            </DateTimeRangeContainer>  
        </>
    )
}

export default SeleccionEntreFechas