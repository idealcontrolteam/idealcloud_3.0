import React from 'react'

export const PromediarMediciones = (mediciones1,mediciones2,tags1,tags2,tagsNuevos) => {
    const format = (num, decimals) => num.toLocaleString('en-US', {
        minimumFractionDigits: decimals,      
        maximumFractionDigits: decimals,
     });

    let nueva_medicion=[]
    let new_mediciones1=mediciones1
    new_mediciones1.map(m1=>{
        let shortName1=tags1.filter(t=>t._id==m1.tagId)[0].shortName
        let tagsMediciones2=mediciones2
        .filter(m2=>m2.dateTime==m1.dateTime)
        .map(m2=>{
            m2.shortName=tags2.filter(t=>t._id==m2.tagId)[0].shortName
            return m2
        })
        if(tagsMediciones2.filter(m2=>m2.shortName==shortName1).length>0) {
            let valor2=tagsMediciones2.filter(m2=>m2.shortName==shortName1)[0].value
            let newLocation=tagsNuevos.filter(t=>t.shortName==shortName1)[0].locationId._id
            let newTag=tagsNuevos.filter(t=>t.shortName==shortName1)[0]._id
            // console.log(newTag)
            // console.log(m1.value)
            // console.log(valor2)
            // console.log(parseFloat(((m1.value+valor2)/2 * 100 / 100).toFixed(1)))
            nueva_medicion.push({
                active: m1.active,
                dateTime: m1.dateTime,
                locationId: newLocation,
                tagId: newTag,
                timeStamp: m1.timeStamp,
                type: m1.type,
                value: parseFloat(((m1.value+valor2)/2 * 100 / 100).toFixed(1)),
                // _id: m1._id
            })
        }
    })
    return(nueva_medicion)
}