import moment from 'moment';
import React, { useEffect, useState } from 'react'
import {  ResponsiveContainer} from 'recharts';
import CanvasJSReact from '../../assets/js/canvasjs.react';
import {configDataGraphic} from './configDataGraphic';

const GraphicGlobal = ({Mediciones,config}) => {

  const [data, setData] = useState([])
  const [axis_y, setAxis_y] = useState([])
  const [axis_y2, setAxis_y2] = useState([])
  const [renderChart, setRenderChart] = useState(false)
  const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196",'#66C6E7'];

  useEffect(() => {
    if(Mediciones.length > 0) {
      setData(Mediciones.map((m,i)=>{
        return(configDataGraphic(m.shortName,i,m.data,config,ColorChart))
      }))
      // configDataGraphic(Mediciones,setData)
    }
  }, [Mediciones])
  
  const CanvasJSChart = CanvasJSReact.CanvasJSChart;
  let optionsChart1 = {
    data: data,
    height:400,
    zoomEnabled: true,
    exportEnabled: true,
    animationEnabled: false, 
    toolTip: {
      shared: true,
      contentFormatter: function (e) {
          var content = " ";
          for (var i = 0; i < e.entries.length; i++){
              content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
           } 
           content +=   "<br/> " ;
          for (let i = 0; i < e.entries.length; i++) {
              // eslint-disable-next-line no-useless-concat
              content += e.entries[i].dataPoint.type + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
              content += "<br/>";
          }
          return content;
      },
    },
    legend: {
        horizontalAlign: "center", 
        cursor: "pointer",
        fontSize: 11,
        itemclick: (e) => {
            if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else {
                e.dataSeries.visible = true;
            }
            setRenderChart(!renderChart);   
          
        }
    }, 
    axisX:{
      valueFormatString:  "DDMMM HH:mm",
      labelFontSize: 10

    },
    // title:{
    //   text: "Weekly Revenue Analysis for First Quarter"
    // },
    axisY:[
      {
        title: "Mg/l - °C",
        lineColor: ColorChart[0],
        tickColor: ColorChart[0],
        labelFontColor: ColorChart[0],
        titleFontColor: ColorChart[0],
        includeZero: true,
        labelFontSize: 11,  
        suffix: "mg/l - °C"
      },
      {
      title: "Flujo",
      lineColor: ColorChart[3],
      tickColor: ColorChart[3],
      labelFontColor: ColorChart[3],
      titleFontColor: ColorChart[3],
      includeZero: true,
      labelFontSize: 11,  
      suffix: "flujo"
      }
    ],
    axisY2: {
      title: "% - PSU",
      lineColor: ColorChart[2],
      tickColor: ColorChart[2],
      labelFontColor: ColorChart[2],
      titleFontColor: ColorChart[2],
      includeZero: true,
      labelFontSize: 11,  
      suffix: "% - psu"
    },
  }
  return (
      <>
        {
          Mediciones.length >= 4&&(
          <ResponsiveContainer height='100%' width='100%' >
              <CanvasJSChart options = {optionsChart1} className="altografico  "  />
          </ResponsiveContainer> 
          )
        }
        
      </>
  )
}

export default GraphicGlobal