import React, { useEffect, useState } from 'react'
import { Badge, CardTitle, Modal, ModalBody, ModalHeader } from 'reactstrap'

const ModalGlobal = ({buttonName,typeButton,title,children}) => {
    let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

    const [open, setOpen] = useState(false)

    const buttonSelect=()=>{
        if(typeButton=='Badge'){
            return <Badge color="primary" style={{cursor: "pointer" }} onClick={() =>toggleModalGlobal(true)}>{buttonName}</Badge>
        }
        if(typeButton=='Tendencia'){
            return <a style={{cursor:'pointer'}} onClick={(e)=>{
                e.preventDefault()
                // this.cleanBuffer()
                toggleModalGlobal(true)
              }}>
                <div class="card rm-border bg-transparent widget-chart text-left" style={{height:100,width:`${100}%`}}>
                  <div class="icon-wrapper rounded-circle">
                      <div className={`icon-wrapper-bg opacity-9 bg-${'success'}`} ></div>
                      <i class={'pe-7s-display1'} style={{color:'#fff'}}></i>
                  </div>
                  <div class="widget-chart-content">
                      <div class="widget-subheading"><font ><font >{'Tendencia Servidor'}</font></font></div>
                      <div class="widget-numbers"><span><font ><font color={miscolores[4]}>{'Tendencia'}</font></font></span></div>
                  </div>
                </div>
            </a>
        }
    }

    const toggleModalGlobal=(value)=>{
        setOpen(value)
    }
    
    return (
        <>
            {buttonSelect()}
            <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={open} size="xl">
                <ModalHeader toggle={() => toggleModalGlobal(false)}>
                    <CardTitle style={{fontSize:20}}>{title}</CardTitle>
                </ModalHeader>
                <ModalBody>
                    {children}
                </ModalBody>
            </Modal>
        </>
        
    )
}

export default ModalGlobal