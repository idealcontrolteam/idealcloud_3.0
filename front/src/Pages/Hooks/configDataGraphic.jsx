import React from 'react'

export const typeTagConfig=(shortName,ColorChart)=>{
    let name=shortName
    let color=""
    let type=""
    let ind=0
    let graphic="spline"
    if(shortName=="oxd" || shortName=='temp'){
        name="Mg/l - °C"
        color=ColorChart[0]
        type="primary"
        ind=0
    }else if(shortName=="sal" || shortName=='oxs'){
        name="% - PSU"
        color=ColorChart[2]
        type="secondary"
        ind=0
    }else if(shortName=="Flujo"){
        color=ColorChart[3]
        type="primary"
        ind=1
    }else if(shortName=="Set_Point"){
        color=ColorChart[4]
        type="primary"
        ind=0
    }else if(shortName=="alarma_minima"){
        graphic='stepLine'
        color=ColorChart[6]
        type="primary"
        ind=0
    }else if(shortName=="inyection"){
        graphic='stepLine'
        color=ColorChart[10]
        type="primary"
        ind=0
    }else if(["Banda_Superior",'Banda_Inferior'].includes(shortName)){
        color=ColorChart[9]
        type="primary"
        ind=0
    }
    return {name,color,type,ind,graphic}
}

const generateObjectAxis=(data,name,shortName,type,color,ind,graphic)=>{
    let object={
        type: graphic,
        markerSize: 0,  
        lineThickness: 3,
        visible: true,
        name: name,
        legendText: shortName,
        color: color,
        showInLegend: true,
        axisYIndex: ind,
        axisYType: type,
        dataPoints: data,
    }
    if(["Banda_Superior",'Banda_Inferior'].includes(shortName)){
        object.lineDashType="dash"
    }
    return object
    
}

const varToString = varObj => Object.keys(varObj)[0]

export const configDataGraphic = (shortName,i,data,config,ColorChart) => {
    const [ 
        solo_Oxd,
        Set_Point,
        histerisis,
        inyection,
        alarma_minima,
        Flujo 
    ]=config;
    // console.log(config)
    const {name,color,type,ind, graphic}=typeTagConfig(shortName,ColorChart)
    const object=generateObjectAxis(data,name,shortName,type,color,ind,graphic)
    if(['oxd','temp','oxs','sal'].includes(shortName) && solo_Oxd){
        return object
    }else if(['oxd'].includes(shortName) && !solo_Oxd){
        return object
    }
    if(!Flujo && [varToString({Flujo})].includes(shortName)){
        return object
    }
    if(!Set_Point && [varToString({Set_Point})].includes(shortName)){
        return object
    }
    if(!inyection && [varToString({inyection})].includes(shortName)){
        return object
    }
    if(!alarma_minima && [varToString({alarma_minima})].includes(shortName)){
        return object
    }
    if(!histerisis && ['Banda_Superior','Banda_Inferior'].includes(shortName)){
        return object
    }
    else{
        return {}
    }
        
}