import React, { useState } from 'react'

const chartcolor= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];

const [dataAxisy, setDataAxisy] = useState({})

const [dataCharts, setDataCharts] = useState({})

const [blocking, setBlocking] = useState(false)

const ReformatGraphic = ({mediciones,shortName,sw,workplaceId,active_volt,disabled}) => {
    let  axisy= {};

    let _dataChartsExport = [];
    mediciones.map( item => { 
      _dataChartsExport.push({ x: moment(item.x).format('DD-MM-YYYY HH:mm'), y : item.y.toString().replace(".",",") }) 
    });
    // load=false;
    // let mydatachartExport = {}
    
    if(shortName.includes("inyection")){
      mediciones=mediciones
    }else{
      mediciones=active_config=="block"?mediciones:mediciones.filter(m=>m.y!=0)
    }
    
    if(active_volt){
      this.getMydatachart(mediciones,"Voltaje",3,chartcolor[10]);
      //console.log(chartcolor)
      axisy=this.getMyaxis(chartcolor[0]," ");
      i++;
    //   mydatachartExport = {  
    //     unity : "volt",              
    //     sonda: shortName.toString(),
    //     measurements : _dataChartsExport
    //     }
    //     this.setState({Export6:_dataChartsExport})
    }else{
        if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
          if(shortName.includes("oxd")){
            this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
            axisy=this.getMyaxis(chartcolor[2]," °C");
            i++;
          }
        }else{
          if(shortName.includes("oxd")){
            this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
            axisy=this.getMyaxis(chartcolor[0]," ");
            i++;
        }
        // mydatachartExport = {  
        //   unity : "mg/L",              
        //   sonda: shortName.toString(),
        //   measurements : _dataChartsExport
        //   }      
        // this.setState({Export1:_dataChartsExport})
      }
      if(this.state.solo_Oxd){
        if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
          if(shortName.includes("oxs")){
            this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
            axisy=this.getMyaxis(chartcolor[2]," °C");
            i++;
            // mydatachartExport = {  
            //   unity : "°C",              
            //   sonda: shortName.toString(),
            //   measurements : _dataChartsExport
            //   }
            //   this.setState({Export3:_dataChartsExport})
          }
          if(shortName.includes("temp")){
              this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
              axisy=this.getMyaxis2(chartcolor[3]," %");
              i++;
            //   mydatachartExport = {  
            //     unity : "%",              
            //     sonda: shortName.toString(),
            //     measurements : _dataChartsExport
            //     }
            //     this.setState({Export2:_dataChartsExport})
          }
        }else{
          if(shortName.includes("temp")){
            this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
            axisy=this.getMyaxis(chartcolor[2]," °C");
            i++;
            // mydatachartExport = {  
            //   unity : "°C",              
            //   sonda: shortName.toString(),
            //   measurements : _dataChartsExport
            //   }
            //   this.setState({Export3:_dataChartsExport})
          }
          if(shortName.includes("oxs")){
              this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
              axisy=this.getMyaxis2(chartcolor[3]," %");
              i++;
            //   mydatachartExport = {  
            //     unity : "%",              
            //     sonda: shortName.toString(),
            //     measurements : _dataChartsExport
            //     }
            //     this.setState({Export2:_dataChartsExport})
          }
        }
        
        if(shortName.includes("sal")){
            this.getMydatachart2(mediciones,"Sal",0,chartcolor[3]);
            axisy=this.getMyaxis2(chartcolor[4]," PSU");
            i++;
            // mydatachartExport = {  
            //   unity : "PSU",              
            //   sonda: shortName.toString(),
            //   measurements : _dataChartsExport
            //   }
            //   this.setState({Export4:_dataChartsExport})
        }
        
        //console.log(shortName.includes("Set_Point"));
      }
      if(!this.state.setPoint){
        if(shortName.includes("Set_Point")){
          // console.log(mediciones)
          this.getMydatachart(mediciones,"setPoint",4,chartcolor[4]);
          axisy=this.getMyaxis(chartcolor[4]," setPoint");
          i++;
        //   mydatachartExport = {  
        //     unity : "set_point",              
        //     sonda: shortName.toString(),
        //     measurements : _dataChartsExport
        //     }
        //     this.setState({Export5:_dataChartsExport})
        }
        // this.filtrar2(shortName,workplaceId,_dataChartsExport,mydatachartExport)
      }
      if(!this.state.histerisis){
        if(shortName.includes("Banda_Superior")){
          // console.log(mediciones)
          this.getMydatachartHisterisis(mediciones,"bandasuperior",5,chartcolor[9]);
          axisy=this.getMyaxis(chartcolor[9]," bandasuperior");
          i++;
        }
        if(shortName.includes("Banda_Inferior")){
          // console.log(mediciones)
          this.getMydatachartHisterisis(mediciones,"bandainferior",5,chartcolor[9]);
          axisy=this.getMyaxis(chartcolor[9]," bandainferior");
          i++;
        }
      }
      if(!this.state.inyecciones){
        if(shortName.includes("inyection")){
          console.log(mediciones)
          this.getMydatachartInyeccion(mediciones,"inyection",6,chartcolor[10]);
          axisy=this.getMyaxis(chartcolor[10]," inyection");
          i++;
        }
      }
      if(!this.state.alarma_minima){
        if(shortName.includes("Alarma_Minima")){
          console.log(mediciones)
          this.getMydatachartAlarmaMinima(mediciones,"alarma_minima",6,chartcolor[6]);
          axisy=this.getMyaxis(chartcolor[11]," alarma_minima");
          i++;
        }
      }
    }

    
    setDataAxisy(dataChaAxisy);
    setDataCharts(dataCha) 
    if (sw === 1)
        disabled=false;
        setBlocking(false) 
}

export default ReformatGraphic