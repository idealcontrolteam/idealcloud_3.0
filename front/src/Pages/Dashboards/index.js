import React, {Fragment} from 'react';
import {Route} from 'react-router-dom';

// DASHBOARDS
import PanelGeneral from './PanelGeneral/IndexPanelGeneral';
// import IndexZona1 from './Zonas/IndexZona1';
//import IndexZona2 from './Zonas/IndexZona2';
import IndexNave from './Herramientas/IndexComparador';
import Tendencia from './Tendencia/IndexTendencia';
import PanelDiagnostico from './Resumen/IndexDiagnostico';
import Resumen2 from './Resumen/IndexResumen2';
import Documentos from './Recursos/IndexDocumentos';
import Videos from './Recursos/IndexVideos';
import Configuracion from './Resumen/IndexConfiguracion';
import ConfiguracionSMA from './Resumen/IndexConfiguracionSMA';
import Tendencia_Online from './Tendencia/IndexTendencia_Online';
import Tendencia_Online2 from './Tendencia/IndexTendencia_Online2';
import Datos_Historicos from './Tendencia/IndexDatos_Historicos';
import UsuarioCentros from './Herramientas/IndexUsuarioCentros';
import Empresas from './Herramientas/IndexEmpresas';
import Efectividad from './Herramientas/IndexEfectividad';
import Servidor from './Herramientas/IndexServidor';
import Panel_Rupanquito from './Piscicultura/IndexTendencia';
import Alarmas from './Herramientas/IndexAlarmas';
import Centros from './Herramientas/IndexCentros'
import Promedios from './Herramientas/IndexPromedios'
// import HistAlarmas from './HistAlarmas/IndexHisAlarmas';
// import HistFallas from './HistFallas/IndexHisFallas';

// Layout
import AppHeader from '../../Layout/AppHeader';
import AppSidebar from '../../Layout/AppSidebar';



// Theme Options
import ThemeOptions from '../../Layout/ThemeOptions';





const Dashboards = ({match}) => (
    <Fragment>
        {/* {alert(JSON.stringify(workplace))} */}
        <ThemeOptions/>
        <AppHeader/>
        <div className="app-main">
            <AppSidebar/>
            <div className="app-main__outer">
                <div className="app-main__inner">
                    <Route path={`${match.url}/panelgeneral`} component={PanelGeneral}/>
                    {/* <Route path={`${match.url}/zona1`} component={IndexZona1}/> */}
                    {/* <Route path={`${match.url}/zona2`} component={IndexZona2}/> */}
                    <Route path={`${match.url}/configuracion`} component={Configuracion}/>
                    <Route path={`${match.url}/configuracion_sma`} component={ConfiguracionSMA}/>
                    {/* <Route path={`${match.url}/resumen2`} component={Resumen}/> */}
                    <Route path={`${match.url}/resumen`} component={Resumen2}/>
                    <Route path={`${match.url}/documentos`} component={Documentos}/>
                    <Route path={`${match.url}/videos`} component={Videos}/>
                    <Route path={`${match.url}/tendencia`} component={Tendencia}/>
                    {/* <Route path={`${match.url}/tendencia_online2`} component={Tendencia_Online}/> */}
                    <Route path={`${match.url}/tendencia_online`} component={Tendencia_Online2}/>
                    <Route path={`${match.url}/datos_historicos`} component={Datos_Historicos}/>
                    <Route path={`${match.url}/comparador`} component={IndexNave}/>
                    <Route path={`${match.url}/empresas`} component={Empresas}/>
                    <Route path={`${match.url}/usuario_centros`} component={UsuarioCentros} />
                    <Route path={`${match.url}/efectividad`} component={Efectividad} />
                    <Route path={`${match.url}/servidor`} component={Servidor} />
                    <Route path={`${match.url}/panel_rupanquito`} component={Panel_Rupanquito} />
                    <Route path={`${match.url}/alarmas`} component={Alarmas} />
                    <Route path={`${match.url}/centros`} component={Centros} />
                    <Route path={`${match.url}/promedios`} component={Promedios} />
                    <Route path={`${match.url}/diagnostico`} component={PanelDiagnostico} />
                </div>           
            </div>
        </div>
    </Fragment>
);

export default Dashboards;