import React, { useEffect, useState } from 'react'
import { Badge, Col, Row } from 'reactstrap'
import '../../../../../assets/css/styleTree.css'
import * as _ from "lodash";
import {TagServices} from '../../../../../services/comun'
import { toast } from 'react-toastify';


const TreeStructure = ({modulos,locations}) => {
  const tagservices=new TagServices();
  const icon1="pe-7s-bandaid";
  const icon2="pe-7s-keypad";
  const [new_locations, setNew_Locations] = useState([])

  useEffect(() => {
    if(locations){
        const n=locations.length;
        let c=0;
        while(c<n){
            locations[c].map(m=>{
                // new_locations.push(m)
                setNew_Locations(new_locations=>[...new_locations,m])
            })
            c++;
        }
        
    }
  },[locations])

  const getLocations=(m)=>{
    
    return(
    <>
        {
            _.orderBy(new_locations.filter(l=>l.zoneId==m._id&&!l.name.includes('Salinidad')).map(l=>{
                return(
                <li>
                    {getBadge(l,icon1)}
                </li>
                )
            }),["name"],["asc"])
        }
        {
            _.orderBy(new_locations.filter(l=>l.zoneId==m._id&&l.name.includes('Salinidad')).map(l=>{
                return(
                <li>
                    {getBadge(l,icon1)}
                </li>
                )
            }),["name"],["asc"])
        }
    </>
    )
  }

  const getBadge=(loc,icon)=>{
    return(
        <Badge onClick={()=>switchActive(loc)} color={loc.active==true?'success':'secondary'} style={{cursor: "pointer" }}>
            <i class={icon} style={{color:'write'}}> </i> {loc.name}
        </Badge>)
  }

  const switchActive=(loc)=>{
    let data={
        active:!loc.active
    }
    tagservices.updateLocation(loc._id,data).then((respuesta)=>{
        if(respuesta.data.statusCode==200 || respuesta.data.statusCode==201){
            loc.active=!loc.active
            const locationActualizados = new_locations.map(l=>loc._id===l._id?loc:l)
            setNew_Locations(locationActualizados)
            tagservices.updateTagForLocation(loc._id,data).then((respuesta2)=>{
              if(respuesta2.data.statusCode==200 || respuesta2.data.statusCode==201){
                toast['success']('El dispositivo fue actualizado con exito', { autoClose: 4000 })
              }
            })
        }
    }).catch(e=>{console.log(e)})
  }

  return (
    <Row>
    {modulos.map(m=>{
        return(
        <Col sx={12} md={6} lx={4}>
            <div class="tree center" style={{fontSize:19}}>
                <ul>
                    <li>
                        {getBadge(m,icon2)}
                        <ul>
                            {getLocations(m)}
                        </ul>
                    </li>
                </ul>
            </div>
        </Col>
        )
    })}
    </Row>
  )
}

export default TreeStructure