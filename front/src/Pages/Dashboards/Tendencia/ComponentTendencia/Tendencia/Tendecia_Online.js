import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../../api-config'
import Loader from 'react-loaders';
import Media from 'react-media';
import {
    toast

} from 'react-toastify';
import CanvasJSReact from '../../../../../assets/js/canvasjs.react';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    Badge,Breadcrumb, BreadcrumbItem, ButtonGroup,UncontrolledTooltip
} from 'reactstrap';
import { Redirect} from 'react-router-dom';
import {logout,sessionCheck} from '../../../../../services/user';
//CustomInput,
import '../../../Zonas/style.css'
import {connect} from 'react-redux';

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';

//import ventilador from '../../../../../src/assets/utils/images/misimagenes/fan.png';
//import valvula from '../../../../../src/assets/utils/images/misimagenes/pipe.png';
import { SizeMe } from 'react-sizeme';
import { number } from 'prop-types';
import {TagServices} from '../../../../../services/comun';
import * as _ from "lodash";


//console.log("props: "+this.props.Mensaje)

//console.log("props: "+JSON.stringify(this.props));
const CanvasJSChart = CanvasJSReact.CanvasJSChart;


let sw=false;
let visible="block";

let check=true;
let check2=false;

let contador=0;
let dataChaAxisy= []
let dataCha = []
let Id_Workplace=0;

let empresa=sessionStorage.getItem("Empresa");
let cont=0;
let workplace="";
let Ultimo_registro="";
let active_config="none";
let new_ponton=[];

class IndexZonaParcelaLarvas extends Component {   
    constructor(props) {
        super(props);
        
        this.state = {   
            myDataChart1: [],   
            NameWorkplace:"",
            ActiveWorkplace:"",
            misTag: [],     
            isLoading: false,
            error: null,
            modal1: false,
            setpointOx:1,
            setpointOxs:[],         
            minOx:3,
            minOxs:[],
            maxOx:0,
            maxOxs:[],
            timeOn:5,
            timeOns:[],
            timeOff:0,
            timeOffs:[],
            alarmTemp:7,
            histeresisOx:0,
            histeresisOxs:[],
            estadoTK:[],
            habilitadoTK:[],
            OxdTK:[],
            Modulos:[],
            dataCharts:[],
            dataAxisy: [],
            redirect2:false,
            redirect3:false,

            activoTemp: true,
            activoHum: true,
            blocking1: false,  
            intentosGuardado: 0,
            check:true,
            check2:false,
            
            bitValTK801:0,
            bitVenTK801:0,
            // bitValTK802:0,
            // bitVenTK802:0,
            // bitValS3:0,
            // bitVenS3:0,
            mySalaModal:0,
            MyTKN:[],
            nCard:[],
            Mis_Sondas:[],
            Mis_Sondas2:[],
            Mis_Registros:[],
            DataTags:[],
            // Tags:[],
        }
        this.tagservices = new TagServices();
        this.intervalIdTag1 = this.updateTags.bind(this);
        // this.loadDataChar = this.loadDataChar.bind(this);
        // this.loadDataTag = this.loadDataTag.bind(this);
        // this.toggleModal1 = this.toggleModal1.bind(this);
        // this.almacenarConfig = this.almacenarConfig.bind(this);
        // this.confirmacionTermino = this.confirmacionTermino.bind(this);
    }
    
  
    componentDidMount = () => {
        window.scroll(0, 0);
        // alert(workplace.id)
        sessionCheck()
        //console.log(this.props)
        //this.getTags();
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        workplace=JSON.parse(sessionStorage.getItem("workplace"));
        this.setState({Mis_Registros:[]});
        const intervaloRefresco =120000;
        //const intervaloRefresco =24000;
        if(workplace!=null){
            Id_Workplace=workplace.id;
            //console.log(Id_Workplace)
            //alert(workplace.active)
            this.setState({NameWorkplace:workplace.name});
            this.getSondas(workplace.id);
            this.intervalIdTag1 = setInterval(() => this.updateTags(),intervaloRefresco);
        }
        //console.log(this.state.Mis_Sondas);
        let { emailUsuario,centroUsuario } = this.props;
        //alert(JSON.stringify(nameCentro))
        //console.log(centroUsuario)
        check=false;
        check2=false;
        // this.getTags();
        this.setState({ isLoading: true });
        // this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        // // this.loadDataChar(); 
        // // this.loadDataTag();
        // this.intervalIdtag = setInterval(() => this.loadDataTag(),intervaloRefresco);
    }

    componentWillUnmount = () => {
       //clearInterval(this.getSondas);
       clearInterval(this.intervalIdTag1);
    }  

    // componentDidUpdate(prevProps, prevState){
    //     // alert(JSON.stringify(prevProps)); extrae los states globales de readux
    //     alert(prevProps.centroUsuario);
    // }

    componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:centro[1]});
        if(this.state.NameWorkplace!=centro[1]){
            this.setState({Mis_Registros:[],Mis_Sondas:[],Mis_Sondas2:[]});
            Id_Workplace=centro[0].toString();
            this.getSondas(centro[0].toString());
        }
      }

    // getTags(){
    //     const EndPointLocation = `${API_ROOT}/tag`;
    //     axios
    //     .get(EndPointLocation)
    //     .then(response => {
    //         if(response.data.statusCode==200){
    //             //console.log("lala "+response.data.data)
    //            this.setState({Tags:response.data.data})
    //         }
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    // }

    getSondas=(id)=>{
        //console.log("wena :"+Id_Workplace);
        //alert(em[0].active)
        const EndPointTag = `${API_ROOT}/workPlace/${Id_Workplace}/zone`;
        let empresas=JSON.parse(sessionStorage.getItem("Centros"));
        //alert(activo)
        let Mis_Locations=[];
        let Modulos=[];
        axios
        .get(EndPointTag)
        .then(response => {
            if(response.data.statusCode==200){
                let mis_zonas = response.data.data;
                let ind=0;
                
                mis_zonas.filter(z=>z.active==true).map((z)=>{
                    Modulos.push(z.name)
                    const EndPointLocation = `${API_ROOT}/zone/${z._id}/location`;
                    axios
                    .get(EndPointLocation)
                    .then(response => {
                        if(response.data.statusCode==200){
                           //console.log("ob"+response.data.data._id)
                           let locations=response.data.data;
                           locations.filter(l=>l.active==true).map((l,i)=>{
                            l.index=ind;
                            ind++;
                            l.module=z.name;
                            let EndPointLocation=`${API_ROOT}/location/${l._id}/tag`;
                                axios
                                .get(EndPointLocation)
                                .then(response => {
                                    if(response.data.statusCode==200){
                                    //console.log("ob"+response.data.data._id)
                                    let tags=response.data.data;
                                        try{
                                            tags.filter(s=>s.active==true).map((s)=>{
                                                    this.tagservices.getRegistros2(s._id).then(r =>{
                                                        r=active_config=="block"?r:r.filter(re=>re.y!=0)
                                                        s.registros=r;
                                                        this.setState({DataTags:this.state.DataTags.filter(t=>t._id!=s._id).concat(s)})
                                                    });
                                                this.setState({isLoading: false})
                                            })
                                            l.tags=tags;
                                        }catch(e){
                                            //console.log(e)
                                        }
                                    }
                                })
                                .catch(error => {
                                //console.log(error);
                                });
                           })
                           let loc_=[];
                           let pri_loc=locations.filter(l=>l.name.includes(' 5m'))
                           let ult_loc=locations.filter(l=>l.name.includes(' 0.5m'))
                           let loc=_.orderBy(locations.filter(l=>!l.name.includes(' 0.5m')&&!l.name.includes(' 5m')),["name"],["asc"])
                           pri_loc.map(l=>{
                                loc_.push(l)
                            })
                           loc.map(l=>{
                                loc_.push(l)
                           })
                           ult_loc.map(l=>{
                               loc_.push(l)
                           })
                           //console.log("hola"+JSON.stringify(loc));
                           Mis_Locations.push(loc_);
                        }
                    })
                    .catch(error => {
                      //console.log(error);
                    });
                    Modulos=Modulos.filter(this.onlyUnique);
                })
                
                Modulos=Modulos.filter(this.onlyUnique);
               this.setState({ isLoading: false,Modulos,Mis_Sondas:Mis_Locations,Mis_Sondas2:Mis_Locations});
            }
        })
        .catch(error => {
          //console.log(error);
        });
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    
    updateTags(){
        try{
            let new_sondas=this.state.Mis_Sondas;
            new_sondas[0]=new_sondas[0].map((l)=>{
                try{
                    let loc=l;
                    let EndPointLocation=`${API_ROOT}/location/${l._id}/tag`;
                    axios
                    .get(EndPointLocation)
                    .then(response => {
                        if(response.data.statusCode==200){
                        //console.log("ob"+response.data.data._id)
                        let tags=response.data.data;
                            try{
                                tags.filter(s=>s.active==true).map((s)=>{
                                        this.tagservices.getRegistros2(s._id).then(r =>{
                                            r=active_config=="block"?r:r.filter(re=>re.y!=0)
                                            s.registros=r;
                                            this.setState({isLoading: false,DataTags:this.state.DataTags.filter(t=>t._id!=s._id).concat(s)})
                                        });
                                    this.setState({isLoading: false})
                                })
                                loc.tags=tags;
                            }catch(e){
                                //console.log(e)
                            }
                        }
                    })
                    .catch(error => {
                    //console.log(error);
                    });
                    l.tags=loc.tags
                    return l
                }
                catch(e){
                    //console.log(e)
                }
                
            })
            this.setState({Mis_Sondas2:new_sondas})
        }
        catch(e){

        }
    }
 
    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    checkChangeHandler = (event) => { 
        // console.log(event.target.defaultChecked);  
        this.setState( { 
            ...this.state,
            [event.target.id]: !event.target.defaultChecked
        } );
    }

    getColor(unidad){
        // #FC3939
        //1,,3,2
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if(empresa=="sasa"){
            if(unidad=="oxd"){
                return miscolores[0]
            }else if(unidad=="oxs"){
                return miscolores[2]
            }else if(unidad=="temp"){
                return miscolores[1]
            }else if(unidad=="sal"){
                return miscolores[3]
            }
            else if(unidad=="oxd_n"){
                return 0
            }else if(unidad=="oxs_n"){
                return 2
            }else if(unidad=="temp_n"){
                return 1
            }else if(unidad=="sal_n"){
                return 3
            }else{
                return ""
            }
        }else{
            if(unidad=="oxd"){
                return miscolores[0]
            }else if(unidad=="oxs"){
                return miscolores[2]
            }else if(unidad=="temp"){
                return miscolores[1]
            }else if(unidad=="sal"){
                return miscolores[3]
            }
            else if(unidad=="oxd_n"){
                return 0
            }else if(unidad=="oxs_n"){
                return 1
            }else if(unidad=="temp_n"){
                return 2
            }else if(unidad=="sal_n"){
                return 3
            }else{
                return ""
            }
        }
        
    }

    getEndRegister(type,data){
        // console.log(data.registros[data.registros.length-1].y)
        let val=this.state.DataTags.filter(t=>t._id==data._id).map(t=>{
            let registros=t.registros;
            if(type=="ultimo"){
                return registros[registros.length-1].y
            }
            if(type=="max"){
                return  registros.reduce((max, b) => Math.max(max, b.y), registros[0].y)
            }
            if(type=="min"){
                return registros.reduce((min, b) => Math.min(min, b.y), registros[0].y);
            }
            if(type=="array"){
                return registros.map((r)=>{
                    return {x:r.x, value:r.y}
                })
            }
        })
        //console.log(val)
        if(val.length>0){
            return val
        }else{
            return ""
        }
    }

    getButtons(data,ind){
        let tags=data.tags;
        console.log(tags)
        try{
            return tags.map((d,i)=>{
                if(d.name.includes("oxd")){
                    return <Col sm="3"> 
                        {this.getButton(d,ind,"oxd"," mg/L")}
                    </Col>
                }
                if(d.name.includes("oxs")){
                    return <Col sm="3"> 
                        {this.getButton(d,ind,"oxs"," %")}
                    </Col>
                }
                if(d.name.includes("temp")){
                    return <Col sm="3"> 
                        {this.getButton(d,ind,"temp"," °C")}
                    </Col>
                }
                if(d.name.includes("sal")&&this.getEndRegister("array",d)!=""){
                    return <Col sm="3"> 
                        {this.getButton(d,ind,"sal"," PSU")}
                    </Col>
                }
            })
        }catch(e){
            //console.log(e)
        }
        
    }

    //genera cada boton para el Oxs, Temp y Oxd
    getButton = (data,i,type,unity)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        //console.log(data.tags)        
        try{
            return<div className="widget-chart widget-chart-hover  p-0 p-0 ">
            <Button 
               onClick={ e => {
                   //this.slider1.slickGoTo(this.getColor(type+"_n"))
                   if(this.state.check)
                       if(i==0)
                           this.slider1.slickGoTo(this.getColor(type+"_n"))
                       else if(i==1)
                           this.slider2.slickGoTo(this.getColor(type+"_n"))
                       else if(i==2)
                           this.slider3.slickGoTo(this.getColor(type+"_n"))
                       else if(i==3)
                           this.slider4.slickGoTo(this.getColor(type+"_n"))
                       else if(i==4)
                           this.slider5.slickGoTo(this.getColor(type+"_n"))
                       else if(i==5)
                           this.slider6.slickGoTo(this.getColor(type+"_n"))
                       else if(i==6)
                           this.slider7.slickGoTo(this.getColor(type+"_n"))
                       else if(i==7)
                           this.slider8.slickGoTo(this.getColor(type+"_n"))
                       else if(i==8)
                           this.slider9.slickGoTo(this.getColor(type+"_n"))
                       else if(i==9)
                           this.slider10.slickGoTo(this.getColor(type+"_n"))
                       else if(i==10)
                           this.slider11.slickGoTo(this.getColor(type+"_n"))
                       else if(i==11)
                           this.slider12.slickGoTo(this.getColor(type+"_n"))
               }}
               key={i} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[0]}>  
               
               <div className="size-boton mt-0" style={{color:`${this.getColor(type)}`}} > 
               {this.getEndRegister("ultimo",data)}     
                   <span className="opacity-6  pl-0 size_unidad"> 
                   {unity}
                   </span>
               </div>
           </Button>                                              
       </div>
        }catch(e){
            //console.log(e)
        }
            
    }

    createOption=(dataCharts,dataAxisy)=>{
        //const {dataCharts,dataAxisy} = this.state; 

        let optionsChart1 = {

            
            data: dataCharts,
           
            height:200,
            zoomEnabled: true,
            //exportEnabled: true,
            animationEnabled: false, 
          
            toolTip: {
                shared: true,
                contentFormatter: function (e) {
                    var content = " ";
                    for (var i = 0; i < e.entries.length; i++){
                        content = moment(e.entries[i].dataPoint.x).format("DDMMM HH:mm");       
                     } 
                     content +=   "<br/> " ;
                    for (let i = 0; i < e.entries.length; i++) {
                        // eslint-disable-next-line no-useless-concat
                        content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                        content += "<br/>";
                    }
                    return content;
                }
            },
        //     legend: {
        //       horizontalAlign: "center", 
        //       cursor: "pointer",
        //       fontSize: 11,
        //       itemclick: (e) => {
        //           if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        //               e.dataSeries.visible = false;
        //           } else {
        //               e.dataSeries.visible = true;
        //           }
        //           this.setState({renderChart:!this.state.renderChart});   
                
        //       }
        //   }, 
           
            axisX:{
                 valueFormatString:  "HH:mm",
                 labelFontSize: 10,
        
            },
            axisY :
                dataAxisy.filter((data,i)=>!String(data.id).includes("PSU")&&!String(data.id).includes("%")),
            axisY2 :{
                title:"% - PSU"
            },      
            }
        return optionsChart1
    }
    getMydatachart=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "spline",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             //showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return mydatachart
      }
      getMyaxis=(color,unity)=>{
         let axisy= {
            id:unity,
            title: "Mg/L - °C",
             labelFontSize: 11,                   
            //  lineColor: color,
            //  tickColor: color,
            //  labelFontColor: color,
            //  titleFontColor: color,
            //suffix: " "+unity
            //includeZero: false
           }
           dataChaAxisy.push(axisy);
         return axisy;
      }
      getMydatachart2=(data,name,i,color)=>{
         let mydatachart = {
              axisYIndex: i,
              type: "spline",
              axisYType: "secondary",
              legendText: name,
              name: name,
              color:color,
              dataPoints : data,
              xValueType: "dateTime",
              indexLabelFontSize:"30",
              //showInLegend: true,
              markerSize: 0,  
              lineThickness: 3
               }
               i++;
             dataCha.push(mydatachart);
           return mydatachart
       }
       getMyaxis2=(color,unity)=>{
         let axisy= {    
             id:unity,
             labelFontSize: 11, 
             title: "% - PSU",                
            //  lineColor: color,
            //  tickColor: color,
            //  labelFontColor: color,
            //  titleFontColor: color,
            // suffix: " "+unity
            //includeZero: false
           }
           dataChaAxisy.push(axisy);
         return axisy;
      }
 
      loadDataChart = (data,chartcolor,i) => { 
            //console.log(data)
            dataChaAxisy= []
            dataCha = []

            let tags=data.tags;
            //console.log(tags)

            tags.map((d,i)=>{
                    let mediciones=d.registros;
                    let  axisy= {};
                    if(d.name.includes("oxd")){
                        this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
                        axisy=this.getMyaxis(chartcolor[0]," ");
                        i++;
                    }
                    if(d.name.includes("temp")){
                        this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
                        //axisy=this.getMyaxis(chartcolor[2]," °C");
                        i++;
                    }
                    if(d.name.includes("oxs")){
                        this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
                        axisy=this.getMyaxis2(chartcolor[3]," %");
                        i++;
                    }
                    if(d.name.includes("sal")&&this.getEndRegister("array",d)!=""){
                        this.getMydatachart2(mediciones,"Sal",0,chartcolor[3]);
                        axisy=this.getMyaxis2(chartcolor[4]," PSU");
                        i++;
                    }
                    
            })
            // try{
            //     return 
            // }catch(e){
            //     console.log(e)
            // }
             //console.log("punto 2")
             let sonda=data;
            //  let dataCharts = mediciones.map( item => { 
            //      return { x: item.x , y : item.y }; 
            //  });
            //  let dataCharts3 = mediciones.map( item => { 
            //     return { x: item.x , y : item.temp }; 
            // });
            //  let dataCharts2 = mediciones.map( item => { 
            //      return { x: item.x , y : item.oxs }; 
            //  });
            //  let dataCharts4 = mediciones.map( item => { 
            //      return { x: item.x , y : item.sal }; 
            //  });
             //console.log(mediciones)
            //  let mydatachart ={};
            //  let mydatachart2 ={};
            //  let mydatachart3 ={};
            //  let mydatachart4 ={};
            //  let  axisy= {};
             
            //  mydatachart=this.getMydatachart(dataCharts,"Oxd",3,chartcolor[0]);
            //  axisy=this.getMyaxis(chartcolor[0]," ");
            //  i++;
            //  mydatachart3=this.getMydatachart(dataCharts3,"Temp",2,chartcolor[1]);
            //  //axisy=this.getMyaxis(chartcolor[2]," °C");
            //  i++;
            //  mydatachart2=this.getMydatachart2(dataCharts2,"Oxs",1,chartcolor[2]);
            //  axisy=this.getMyaxis2(chartcolor[3]," %");
            //  i++;
            //  mydatachart4=this.getMydatachart2(dataCharts4,"Sal",0,chartcolor[3]);
            //  axisy=this.getMyaxis2(chartcolor[4]," PSU");
            //  i++;
           
             let option=this.createOption(dataCha,dataChaAxisy);
             return <CanvasJSChart id={data.name} key={data.code} options = {option} className="altografico  "  />
            //   this.setState({
            //       dataAxisy:dataChaAxisy,
            //       dataCharts:dataCha
            //  });   
            //   if (sw === 1)
            //      this.setState({blocking: false}); 
      }
     
    
    //genera el grafico de Oxs, Temp y Oxd
    getChart = (data,i,unity,type)=>{
        // console.log(data.registros)
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];

        if(this.state.check2){
            return <Fragment>
            <div>
                    <ResponsiveContainer width="100%" height="100%">
                                {this.loadDataChart(data,miscolores,i)}
                    </ResponsiveContainer>
            </div>
            </Fragment>}
        if(this.state.check){
            let registros=this.getEndRegister("array",data)
            registros=registros[0]
            let mediciones=registros;
            return <Fragment><div className="widget-chart widget-chart2 text-left p-0" style={{display:`${this.state.check?"block":"none"}`}}>
            <div className="widget-chat-wrapper-outer">
                {/* {console.log(data.registros)} */}
                <div className="widget-chart-content widget-chart-content-lg  p-2">
                    <div className="widget-chart-flex ">
                        <div
                            className="widget-title opacity-9 text-muted text-uppercase">
                            {/* {data.name} */}
                        </div>
                    </div>

                    <div className="widget-numbers p-1 m-0">
                        <div className="widget-chart-flex">
                            <div>
                            {this.getEndRegister("ultimo",data)}
                            {/* {type!="oxd"?"":data.registros[data.registros.length-1].oxd}  
                            {type!="temp"?"":data.registros[data.registros.length-1].temp}
                            {type!="oxs"?"":data.registros[data.registros.length-1].oxs}
                            {type!="sal"?"":data.registros[data.registros.length-1]sal}   */}
                                <small className="opacity-5 pl-1 size_unidad3"> {unity}</small>
                            </div>
                        </div>
                    </div> 
                    
                <div className=" opacity-8 text-focus pt-0">
                    <div className=" opacity-5 d-inline">
                    Max
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>  
                                                                                                    
                        <span className="pl-1 size_prom">
                                {this.getEndRegister("max",data)}
                                {/* {type!="oxd"?"":data.registros.reduce((max, b) => Math.max(max, b.y), data.registros[0].y) } 
                                {type!="temp"?"":data.registros.reduce((max, b) => Math.max(max, b.y), data.registros[0].y) }
                                {type!="oxs"?"":data.registros.reduce((max, b) => Math.max(max, b.y), data.registros[0].y) } 
                                {type!="sal"?"":data.registros.reduce((max, b) => Math.max(max, b.y), data.registros[0].y) }                                              */}
                                {/* data.reduce((max, b) => Math.max(max, b.oxd), data[0].oxd) */}
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline  ml-2">
                    Prom
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>                                                                                  
                        <span className="pl-1 size_prom">
                            {this.getEndRegister("prom",data)}
                            {/* {type!="oxd"?"":((data.registros.reduce((a, b) => +a + +b.y, 0)/data.registros.length)).toFixed(1) }  
                            {type!="temp"?"":((data.registros.reduce((a, b) => +a + +b.y, 0)/data.registros.length)).toFixed(1) }
                            {type!="oxs"?"":((data.registros.reduce((a, b) => +a + +b.y, 0)/data.registros.length)).toFixed(1) }
                            {type!="sal"?"":((data.registros.reduce((a, b) => +a + +b.y, 0)/data.registros.length)).toFixed(1) } */}
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline ml-2">
                    Min
                    </div>
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>                                                                                  
                        <span className="pl-1 size_prom">
                                {this.getEndRegister("min",data)}
                                {/* {type!="oxd"?"":data.registros.reduce((min, b) => Math.min(min, b.y), data.registros[0].y) }  
                                {type!="temp"?"":data.registros.reduce((min, b) => Math.min(min, b.y), data.registros[0].y) }
                                {type!="oxs"?"":data.registros.reduce((min, b) => Math.min(min, b.y), data.registros[0].y) }
                                {type!="sal"?"":data.registros.reduce((min, b) => Math.min(min, b.y), data.registros[0].y) } */}
                                {/* { data.reduce((min, b) => Math.min(min, b.oxd), data[0].oxd)} */}
                        </span>
                    </div>
                    
                </div> 
            

                </div>

                <div className="d-inline text-secondary pr-1">                                                                                                                                                                 
                            <span className="pl-1">
                                    {/* { moment(data.registros[data.registros.length-1].x).format('HH:mm DD-MMM')} */}

                            </span>
                </div>


                <div
                    className="widget-chart-wrapper he-auto opacity-10 m-0">
                    <ResponsiveContainer height={150} width='100%'>

                        <AreaChart data={mediciones}
                                // animationDuration={1}
                                isAnimationActive = {false}
                                showInLegend = {true}
                                margin={{
                                    top: 0,
                                    right:10,
                                    left: -30,
                                    bottom: 0
                                }}>
                            
                                <Tooltip                                                                                                        
                                            labelFormatter={function(value) {
                                                return `${ moment(value).format('HH:mm DD-MMM')}`;
                                                }}
                                            formatter={function(value, name) {
                                            return `${value}`;
                                            }}
                                        />
                                <defs>
                                    <linearGradient id={"colorPv" + this.getColor(type+"_n")} x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="10%" stopColor={`${this.getColor(type)}`} stopOpacity={0.7}/>
                                        <stop offset="90%" stopColor={`${this.getColor(type)}`}stopOpacity={0}/>
                                    </linearGradient>
                                </defs>
                                <YAxis                                                                                    
                                tick={{fontSize: '10px'}}
                                // domain={['dataMin - 4','dataMax + 4']}
                                />
                                {/* <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />  */}
                                <XAxis
                                        dataKey={'x'}                                                                              
                                        hide = {false}
                                        tickFormatter={x => moment(x).format('HH:mm')}
                                        tick={{fontSize: '10px'}}
                                        />
                                <Area type='monotoneX' dataKey={'value'}
                            
                                    stroke={`${this.getColor(type)}`}
                                    strokeWidth='3'
                                    fillOpacity={1}
                                    fill={"url(#colorPv" + this.getColor(type+"_n") + ")"}/>
                            </AreaChart>

                    </ResponsiveContainer>
                </div>
            </div>
        </div></Fragment>
        }
    }

    //realiza los filtro y la llamada a generar el button para los 3 tags de la card
    //className={cx(data.active ? '' : 'opacity-3')} #FC3939
    getViewButton=(array,i)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        return array.map((data,i)=>
           <Col sm="6" > 
                <Button 
                onClick={ e => {
                    this.slider1.slickGoTo(i)
                }}
                    className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                    <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                                                                                            
                    {/* {data.oxd}  */}
                        {/* <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span> */}
                        <span className="opacity-6  pl-0 size_unidad">  unidad </span>
                    </div>
                    <div className="widget-subheading">
                    {/* {data.dateTime} */}
                    </div>
                </Button>  
               {/* {this.getButton(data,i)} */}
           </Col>
       )     
    }

    //realiza un switch por separado para optimizar la busqueda del Slider
    switchViewChart=(sonda,ind,settings)=>{
        let slider={};
        // let name_slider=eval("slider"+(ind+1));
        //let slider_interior=eval("slider"+(ind+1)+"="+(eval("this.slider"+(ind+1)+"="+"slider"+(ind+1))));
        //console.log(sonda)
        if(ind==0)
            slider=slider1 => (this.slider1 = slider1)
        else if(ind==1)
            slider=slider2 => (this.slider2 = slider2)
        else if(ind==2)
            slider=slider3 => (this.slider3 = slider3)
        else if(ind==3)
            slider=slider4 => (this.slider4 = slider4)
        else if(ind==4)
            slider=slider5 => (this.slider5 = slider5)
        else if(ind==5)
            slider=slider6 => (this.slider6 = slider6)
        else if(ind==6)
            slider=slider7 => (this.slider7 = slider7)
        else if(ind==7)
            slider=slider8 => (this.slider8 = slider8)
        else if(ind==8)
            slider=slider9 => (this.slider9 = slider9)
        else if(ind==9)
            slider=slider10 => (this.slider10 = slider10)
        else if(ind==10)
            slider=slider11 => (this.slider11 = slider11)
        else if(ind==11)
            slider=slider12 => (this.slider12 = slider12)
        try{
            if(this.state.check){
                return<Slider key={sonda.code} ref={slider} {...settings}>
                    {/* {this.getChart(sonda,ind,"mg/L","oxd")}
                    {this.getChart(sonda,ind,"°C","temp")}
                    {this.getChart(sonda,ind,"%","oxs")}
                    {this.getChart(sonda,ind,"PSU","sal")} */}
                    {
                        sonda.tags.map((d)=>{
                            if(d.name.includes("oxd")){
                                return this.getChart(d,ind,"mg/L","oxd")
                            }
                            if(d.name.includes("oxs")){
                                return this.getChart(d,ind,"%","oxs")
                            }
                            if(d.name.includes("temp")){
                                return this.getChart(d,ind,"°C","temp")
                            }
                            if(d.name.includes("sal")&&this.getEndRegister("array",d)!=""){
                                //console.log(d)
                                return this.getChart(d,ind,"PSU","sal")
                            }
                        })
                    }
                </Slider>
            }if(this.state.check2){
                return<Fragment key={sonda.code} ref={slider} {...settings}>
                    {this.getChart(sonda,ind,"mg/L","oxd")}
                </Fragment>
            }
        }
        catch(e){
            //console.log(e)
        }
            
    }

    endDate=(sonda)=>{
        try{
            Ultimo_registro=moment(sonda.tags[0].registros[sonda.tags[0].registros.length-1].x).format('YYYY-MM-DD HH:mm:ss')
            return Ultimo_registro
        }catch(e){
            //console.log(e)
        }
        
    }

    handleChangeGlobal = event => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    updateDivice(id,name,i){
        let data={
            name:"Sensor "+name
        }
        axios
              .put(API_ROOT+`/location/${id}`,data)
              .then(response => {   
                  if(response.data.statusCode===201 || response.data.statusCode===200){
                    toast['success']('El dispositivo fue actualizado con exito', { autoClose: 4000 })
                  }
              })
              .catch(error => {
                  console.log(error)
              });
    }

    handelkeyDown(e,i,id){
        if (e.key === 'Enter') {
            if(active_config=="block"){
                let valor=document.getElementById("disp"+i).value;
                //console.log(valor)
                this.updateDivice(id,valor,i)
            }
        }
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    getName(name){
        let split=String(name).split('ensor ')
        return `${split[1]}`
    }

    getCard=(i,settings,sonda)=>{
      return  <Card className="mb-3 mr-20">
                            <CardHeader className="card-header-tab">
                                <div className="card-header-title font-size-ls text-capitalize font-weight-normal">
                                    {this.createTooltip("disp"+i,sonda.name)}
                                    <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6" ></i>
                                    {active_config=="block"?
                                    <Input id={"disp"+i} autoComplete="off" placeholder={this.getName(sonda.name)} onChange={this.handleChangeGlobal} onKeyDown={((e)=>{
                                        this.handelkeyDown(e,i,sonda._id)
                                    })}></Input>:<label id={"disp"+i}>{sonda.name}</label>}
                                    {/* id:{sonda.code} */}
                                    {/* {console.log("son_reg:"+JSON.stringify(this.state.Sonda_Registro))} */}
                                </div>
                                {/* <div className="btn-actions-pane-right text-capitalize">
                                    <span className="d-inline-block">
                                            <Button color="primary" disabled={sonda.active==0?1:0} onClick={() => this.toggleModal1(`Cabecera`)}
                                                outline>
                                                <i className="pe-7s-tools btn-icon-wrapper" ></i>
                                            </Button>
                                        
                                    </span>
                                </div> */}
                                 <div className="btn-actions-pane-right text-capitalize">
                                {this.endDate(sonda)}
                                </div>

                                </CardHeader>
                                {/* {console.log(this.getRegistros(myDataChart1.code))} */}
                                <CardBody className="p-0"  >
                                    {this.getCardBody(i,settings,sonda)}
                                </CardBody>
                                    {/* {
                                        this.state.nCard.map((data)=>{
                                            if(data==1)
                                                switch (i) {
                                                    case i=0:
                                                        // this.state.myDataCard1=1?"":"0.3"
                                                        //style={{opacity:`${this.state.myDataCard1==1?"":"0.3"}`}}
                                                        return 
                                                
                                                    default:
                                                        return "";
                                                }
                                        })
                                    }      */}
                            </Card>
    }

    getCardBody=(ind,settings,sonda)=>{
        return <Fragment>
                                {/* <h6 className="text-muted text-uppercase font-size-md opacity-8 pl-3 pt-3 pr-3 pb-1 font-weight-normal">
                                    Valor actual de Sondas
                                    </h6> */}
                                    <Card className="main-card mb-0">
                                        <div className="grid-menu grid-menu-4col">
                                            <Row className="no-gutters" key={ind}>
                                                {/* {console.log("Registros: "+JSON.stringify(this.state.Mis_Registros))} */}
                                                {/* <Col sm="3"> 
                                                    {this.getButton(sonda.tags[0],ind,"oxd"," mg/L")}
                                                </Col> */}
                                                {
                                                    this.getButtons(sonda,sonda.index)
                                                }
                                                {/* {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"temp"," °C")}
                                                    </Col>
                                                }
                                                {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"oxs"," %")}
                                                    </Col>
                                                }
                                                {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"sal"," PSU")}
                                                    </Col>
                                                } */}

                                            </Row>
                                        </div>
                                    </Card>
                                    {/* style={{display:`${check?"block":"none"}`}} */}
                                    <div className="p-1 slick-slider-sm mx-auto" >
                                    

                                    {this.switchViewChart(sonda,sonda.index,settings)}

                                    </div>
                                </Fragment>
    }
    
    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }
    
    cardSonda=(settings,modulo)=>{
        let sondas=this.state.Mis_Sondas;
        //console.log(sondas)
        //sondas=_.orderBy(sondas, ['_id'],['asc']);
        //console.log(sondas)
        //sondas=sondas.filter((data)=>!String(data.name).includes("Salinidad"));
        let i=0;
        return sondas.map((s,i)=>{
            //console.log(s.filter(d=>d.module.includes("100")).length)
            return s.filter((data)=>{
                // if(empresa=="sasa"){
                //     return data
                // }else{
                //     return !String(data.name).includes("Salinidad")
                // }
                return !String(data.name).includes("Salinidad")
            }).map((data,ind)=>{
                //console.log("sonda"+data.name)
                // if(modulo.includes('Ponton')){
                //     let pon=new_ponton.filter(this.onlyUnique)
                //     let new_p=_.orderBy(pon,['_id'],['asc'])
                //     if(i==0){
                //         i++;
                //         return new_p.map((p,ind)=>{
                //             if(empresa=="camanchaca"){
                //                 return<Col key={ind+"s"} sm={3} className="animated fadeIn fast">
                //                     {this.getCard(ind,settings,p)}
                //                 </Col> 
                //             }else{
                //                 return<Col key={ind+"s"} sm={4} className="animated fadeIn fast">
                //                     {this.getCard(ind,settings,p)}
                //                 </Col> 
                //             }
                //         })
                //     }
                // }
                // else if(modulo==data.module){
                //     if(data.name.includes('_Ponton')){
                //         new_ponton=new_ponton.filter(p=>p.name!=data.name).map(p=>p).concat(data)
                //     }
                if(modulo==data.module){
                    if(empresa=="camanchaca"){
                        return<Col key={ind+"s"} sm={3} className="animated fadeIn fast">
                            {this.getCard(ind,settings,data)}
                        </Col> 
                    }else{
                        return<Col key={ind+"s"} sm={4} className="animated fadeIn fast">
                            {this.getCard(ind,settings,data)}
                        </Col> 
                    }
                }
                    
                //}
            })
        })
    }

    getValidate(id){
        let empresas=JSON.parse(sessionStorage.getItem("Centros"));
        //console.log(Ultimo_registro)
        // let fecha="";
        // try{
        //     console.log(this.state.Mis_Sondas[0][0]._id)
        //     fecha=this.endDate(this.state.Mis_Sondas[0][0]._id);
        // }catch(e){
        //     console.log(e)
        // }
        
        try{
            let em=empresas.filter(data=>data.code==id).map((data)=>{
                return {"active":data.active,"date":data.endDate}
            })
            let f=this.getOnline();
            let now=new Date();
            let fecha_in=moment(now).subtract(10, "minutes").format('YYYY-MM-DD HH:mm:ss');
            //let fecha_ind=this.getOnline();
            //console.log(fecha_ind)
            if(em[0].active==1){
            return <Fragment><div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div>(Activo) {f>=fecha_in? <Badge color="success" pill>Online</Badge>: <Badge color="secondary" pill>Offline</Badge>}</Fragment>
            }else{
                return <Fragment><div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>(Inactivo)</Fragment>
            }
        }
        catch(e){
            //console.log(e)
        }
        
    }
    getOnline(){
        try{
            let data=this.state.Mis_Sondas2[0].filter(l=>l.active==true);
            //console.log(data)
            let tags=data[0].tags;
            //console.log(tags[0])
            //return data.dateTimeLastValue;
            return tags.filter((d,i)=>i==0).map((d)=>{
                //console.log("WEMA "+moment(d.registros[d.registros.length-1].x).format('YYYY-MM-DD HH:mm:ss'))
                return moment(d.registros[d.registros.length-1].x).format('YYYY-MM-DD HH:mm:ss')
            })
        }
        catch(e){
            //console.log(e)
        }
    }

    render() {
        // var this_url=window.location.href;
        // var n_nave=this_url.split('/');
        if(this.state.redirect2){
            return <Redirect to="/dashboards/configuracion"></Redirect>
        }
        if(this.state.redirect3){
            return <Redirect to="/dashboards/configuracion_sma"></Redirect>
        }
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { isLoading,error} = this.state;
         
          const settings = {
            autoplaySpeed:6000,
            autoplay: false,        
            centerMode: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };
       
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if (error) {
            return <p>{error.message}</p>;
        }
    
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        

        return (
            <Fragment>     
                {/* {console.log(this.state.Mis_Registros)} */}
                {/* {console.log(this.state.Mis_Sondas)}
                {console.log(this.state.Modulos)} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Tendencias</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Tendecia Online</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace}
                      {/* {alert(this.state.ActiveWorkplace)} */}
                      {this.getValidate(Id_Workplace)}
                      
                      {/* <div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>(Offline) */}
                      </BreadcrumbItem>
                      <Media queries={{ small: { maxWidth: 1158 } }}>
                        {matches =>
                            matches.small ? (
                                <ButtonGroup style={{marginLeft:`${65}%`,marginTop:5}}>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                                    this.setState({check:true,check2:false})
                                                }}>
                                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                                    this.setState({check:false,check2:false})
                                                }}>-</Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    this.setState({check2:true,check:false})
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                                </Button>
                                </ButtonGroup>
                            ) : (
                                <ButtonGroup style={{marginLeft:`${90}%`,marginTop:-20}}>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                                    this.setState({check:true,check2:false})
                                                }}>
                                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                                    this.setState({check:false,check2:false})
                                                }}>-</Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    this.setState({check2:true,check:false})
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                                </Button>
                                </ButtonGroup>
                            )
                        }
                        </Media>
                        <BreadcrumbItem active tag="span" style={{display:active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion</Badge></a></BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect3:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion SMA</Badge></a></BreadcrumbItem>
                      </Breadcrumb>
                      {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        MODULOS
                                    </div>
                                    
                                </div>
                                <div className="page-title-actions"> 
                                </div>                    
                            </div>
                        </div> */}
                        <Row>
                         
                        </Row>
                        {this.state.Modulos.filter(this.onlyUnique).map((data,i)=>{
                                return <Card size={12}>
                                            <CardBody>
                                            <CardTitle>{data}</CardTitle>
                                                <Row size={12} key={i+"m"}>
                                                    {this.cardSonda(settings,data)}
                                                </Row>
                                            </CardBody>
                                        </Card>
                            })}

           
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
  });

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(IndexZonaParcelaLarvas);