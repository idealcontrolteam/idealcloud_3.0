import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../../services/comun';
import {logout,sessionCheck} from '../../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import Swal from 'sweetalert2'

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");
let disabled=false;
let load=false;
let active_config="none";
let active_volt=false;

class Tendencia extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          Export1:[],
          Export2:[],
          Export3:[],
          Export4:[],
          Export5:[],
          Export6:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          MultiExport:false,
          solo_Oxd:true,
          setPoint:true,
          histerisis:true,
          inyecciones:true,
          alarma_minima:false,
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);       
        this.filtrar =this.filtrar.bind(this);
        this.filtrar2 =this.filtrar2.bind(this);
        this.loadDataChart =this.loadDataChart.bind(this);
        
        
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        sessionCheck()
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        // this.tagservices.getDataTag().then(data => 
        //     this.setState({Tags: data})
        // );  
        //this.this_Centro=setInterval(()=>this.getIdCentro(),2000);
        //var data = JSON.parse(sessionStorage.getItem('workplace'));
        // alert(JSON.stringify(data.id));
        // this.getIdCentro();
        this.setState({redirect:false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(this.props.Centro!=null&&this.props.Centro!=undefined){
          let data={NameWorkplace:{id:this.props.Centro,name:this.props.Ncentro}}
          this.getSondas(this.props.Centro);
          // if(this.props.Tag!=null){
          //   console.log(this.props.Tag)
          //   data.TagsSelecionado=this.props.Tag;
          // }
          this.setState(data);
          if(this.props.Tag!=null){
            this.filtrar();
          }
          
          
        }
        else if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            this.getSondas(workplace.id);
        }
        
      }

      componentWillUnmount = () => {
        clearInterval(this.getSondas);
        clearInterval(this.loadDataChart);
        clearInterval(this.loadDataChart2);
        clearInterval(this.filtrar);
        clearInterval(this.filtrar2);
     }  

      componentWillReceiveProps(nextProps){
        if(this.props.Centro==null&&this.props.Centro==undefined){
          if (nextProps.initialCount && nextProps.initialCount > this.state.count){
            this.setState({
              count : nextProps.initialCount
            });
          }
          //alert(JSON.stringify(nextProps));
          let separador=nextProps.centroUsuario;
          let centro=separador.split('/');
          this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
          this.getSondas(centro[0].toString());
          this.setState({redirect:true})
        }
      }

      getSondas=(id)=>{
        const EndPointTag = `${API_ROOT}/workPlace/${id}/zone`;
        let Mis_Locations=[];
        var CancelToken = axios.CancelToken;
        var cancel;
        axios
        .get(EndPointTag,
          {
            cancelToken: new CancelToken(
                function executor(c) {
                    cancel = c;
                 })
          })
        .then(response => {
            if(response.data.statusCode==200){
              let zonas = response.data.data;
              let ind=0;
              zonas.filter(z=>z.active==true).map((z)=>{
                const EndPointLocation = `${API_ROOT}/zone/${z._id}/location`;
                      axios
                      .get(EndPointLocation)
                      .then(response => {
                          if(response.data.statusCode==200){
                            //console.log("ob"+response.data.data._id)
                            let locations=response.data.data;
                            locations.filter(l=>l.active==true).map((l,i)=>{
                              l.index=ind;
                              ind++;
                              l.module=z.name;
                              const EndPointLocation = `${API_ROOT}/location/${l._id}/tag`;
                              axios
                              .get(EndPointLocation)
                              .then(response => {
                                  if(response.data.statusCode==200){
                                    //console.log("ob"+response.data.data._id)
                                    let tags=response.data.data;
                                    l.tags=tags;
                                    this.setState({isLoading: false,Tags:this.state.Tags.filter((t)=>t._id!=l._id&&t.code!=l.code).concat(l)})
                                  }
                              })
                              .catch(error => {
                                console.log(error);
                              });
                            })
                            let loc=_.orderBy(locations,["_id"],["asc"])
                            Mis_Locations.push(loc);
                          }if(response.data.statusCode>=500){
                            this.getSondas()
                          }
                      })
                      .catch(error => {
                        console.log(error);
                      });
              })
            }if(response.data.statusCode>=500){
              this.getSondas()
            }

           this.setState({isLoading: false});   
        })
        .catch(error => {
          console.log(error);
        });
    }
    //  getIdCentro=()=>{
    //     let url = window.location.href;
    //     //console.log(alert);
    //     let s=url.split('/');
    //     console.log(s[s.length-1])

    //     let now = new Date(); 
    //     const f1 = moment(now).subtract(360, "minutes").format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    //     const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";  
    //     const token = "tokenFalso";

    //     axios.get(`${API_ROOT}/measurement/xy/tag/${s[s.length-1]}/${f1}/${f2}`, {
    //          'Authorization': 'Bearer ' + token
    //     })
    //     .then(response => {
    //         const measurements = response.data.data;
    //         console.log(measurements);
    //         //this.setState({ dateTimeRefresh:measurements[measurements.length-1].x});
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    //     return s[s.length-1]
    //  }
    getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }

    getMydatachartInyeccion=(data,name,i,color)=>{
      // data.map((d,i)=>{
      //   let numero=i%2==0?0:1
      //   return ({y:numero,x:d.x})
      // })
      let mydatachart = {
           axisYIndex: i,
           type: "stepLine",
           legendText: name,
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }
    getMydatachartAlarmaMinima=(data,name,i,color)=>{
      // data.map((d,i)=>{
      //   let numero=i%2==0?0:1
      //   return ({y:numero,x:d.x})
      // })
      let mydatachart = {
           axisYIndex: i,
           type: "stepLine",
           lineDashType: "dash",
           legendText: name,
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }
    getMydatachartHisterisis=(data,name,i,color)=>{
      let mydatachart = {
           axisYIndex: i,
           type: "spline",
           legendText: name,
           lineDashType: "dash",
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }
     getMydatachart=(data,name,i,color)=>{
       let mydatachart = {
            axisYIndex: i,
            type: "spline",
            legendText: name,
            name: name,
            color:color,
            dataPoints : data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,  
            lineThickness: 3
             }
             i++;
           dataCha.push(mydatachart);
         return
     }
     getMyaxis=(color,unity)=>{
        let axisy= {    
            id:unity,
            title: "mg/L - °C",
            labelFontSize: 11,                   
            // lineColor: color,
            // tickColor: color,
            // labelFontColor: color,
            // titleFontColor: color,
            // suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }
     getMydatachart2=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "spline",
             axisYType: "secondary",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return
      }
      getMyaxis2=(color,unity)=>{
        let axisy= {
            id:unity,
            labelFontSize: 11,                   
            lineColor: color,
            tickColor: color,
            labelFontColor: color,
            titleFontColor: color,
            suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

     loadDataChart = (shortName, URL,chartcolor,sw,workplaceId) => {   
         
       // this.setState({blocking: true});
        const token = "tokenFalso";
        var CancelToken = axios.CancelToken;
        var cancel;
        axios
          .get(URL,
            {
              cancelToken: new CancelToken(
                  function executor(c) {
                      cancel = c;
                   })
            })
         .then(response => { 
          if(response.data.statusCode>=200 && response.data.statusCode<400){
            console.log("punto 2")
            let mediciones=response.data.data;
            let  axisy= {};
            if(shortName.includes("inyection")){
              mediciones=mediciones
            }else{
              mediciones=active_config=="block"?mediciones:mediciones.filter(m=>m.y!=0)
            }
            
            if(active_volt){
              this.getMydatachart(mediciones,"Voltaje",3,chartcolor[10]);
              //console.log(chartcolor)
              axisy=this.getMyaxis(chartcolor[0]," ");
              i++;
            }else{
                if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
                  if(shortName.includes("oxd")){
                    this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
                    axisy=this.getMyaxis(chartcolor[2]," °C");
                    i++;
                  }
                }else{
                  if(shortName.includes("oxd")){
                    this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
                    axisy=this.getMyaxis(chartcolor[0]," ");
                    i++;
                }
              }
              if(this.state.solo_Oxd){
                if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
                  if(shortName.includes("oxs")){
                    this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
                    axisy=this.getMyaxis(chartcolor[2]," °C");
                    i++;
                  }
                  if(shortName.includes("temp")){
                      this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
                      axisy=this.getMyaxis2(chartcolor[3]," %");
                      i++;
                  }
                }else{
                  if(shortName.includes("temp")){
                    this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
                    axisy=this.getMyaxis(chartcolor[2]," °C");
                    i++;
                  }
                  if(shortName.includes("oxs")){
                      this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
                      axisy=this.getMyaxis2(chartcolor[3]," %");
                      i++;
                  }
                }
                
                if(shortName.includes("sal")){
                    this.getMydatachart2(mediciones,"Sal",0,chartcolor[3]);
                    axisy=this.getMyaxis2(chartcolor[4]," PSU");
                    i++;
                }
                
                //console.log(shortName.includes("Set_Point"));
              }
              if(!this.state.setPoint){
                if(shortName.includes("Set_Point")){
                  // console.log(mediciones)
                  this.getMydatachart(mediciones,"setPoint",4,chartcolor[4]);
                  axisy=this.getMyaxis(chartcolor[4]," setPoint");
                  i++;
                }
              }
              if(!this.state.histerisis){
                if(shortName.includes("Banda_Superior")){
                  // console.log(mediciones)
                  this.getMydatachartHisterisis(mediciones,"bandasuperior",5,chartcolor[9]);
                  axisy=this.getMyaxis(chartcolor[9]," bandasuperior");
                  i++;
                }
                if(shortName.includes("Banda_Inferior")){
                  // console.log(mediciones)
                  this.getMydatachartHisterisis(mediciones,"bandainferior",5,chartcolor[9]);
                  axisy=this.getMyaxis(chartcolor[9]," bandainferior");
                  i++;
                }
              }
              if(!this.state.inyecciones){
                if(shortName.includes("inyection")){
                  console.log(mediciones)
                  this.getMydatachartInyeccion(mediciones,"inyection",6,chartcolor[10]);
                  axisy=this.getMyaxis(chartcolor[10]," inyection");
                  i++;
                }
              }
              if(!this.state.alarma_minima){
                if(shortName.includes("Alarma_Minima")){
                  console.log(mediciones)
                  this.getMydatachartAlarmaMinima(mediciones,"alarma_minima",6,chartcolor[6]);
                  axisy=this.getMyaxis(chartcolor[11]," alarma_minima");
                  i++;
                }
              }
            }
            
            //console.log(mediciones)
            
          
             this.setState({
                 dataAxisy:dataChaAxisy,
                 dataCharts:dataCha
            });  
            //                 dataExport:dataChaExport,
            //   this.setState({dataExport:dataChaExport});  
            //   this.setState({dataCharts:dataCha});  
             if (sw === 1)
                disabled=false;
                this.setState({blocking: false}); 

           
          }else if(response.data.statusCode===500 || response.data.statusCode===504 || response.data.statusCode===502){
            if (sw === 1)
              disabled=false;
            // this.setState({blocking: false}); 
              Swal.fire({
                title: 'Ops!',
                text: "¡Ocurrio un problema, porfavor intente nuevamente!",
                icon: 'warning',
                showCancelButton: false,
                //cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
              //confirmButtonText: 'Eliminar'
            })
            //this.loadDataChart(shortName, URL,chartcolor,sw);
          }
                 // console.timeEnd('loop2');
         })
         .catch(error => {
          if (sw === 1)
            Swal.fire({
              title: 'Ops!',
              text: "¡Ocurrio un problema, porfavor intente nuevamente!, Error: '"+error+"'",
              icon: 'warning',
              showCancelButton: false,
              //cancelButtonText: 'Cancelar',
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
            //confirmButtonText: 'Eliminar'
            })
            disabled=false;
            this.setState({blocking: false});
            console.log(error);
         });
         
 
     }
    

    filtrar =(new_fecha) => {
        let TagsSelecionado=[];
        let nombre_centro="";
        if(this.state.TagsSelecionado!==null){
          TagsSelecionado=this.state.TagsSelecionado;
          nombre_centro=this.state.NameWorkplace.name;
        }else if(this.props.Tag!=null){
          TagsSelecionado=this.props.Tag;
          nombre_centro=this.props.Ncentro;
          this.setState({TagsSelecionado:this.props.Tag})
        }
        //console.log(TagsSelecionado)
        //const {TagsSelecionado} = this.state;

        //this.setState({dataExport:[]});
        // console.log(TagsSelecionado)
        if (TagsSelecionado !== null){       
            // console.log(TagsSelecionado)    
            // console.log(TagsSelecionado.name.includes('VoltajeR'))
            active_volt=false;
            let f1="";
            let f2="";
            if(new_fecha!=null){
              this.setState({start:new_fecha,MultiExport:false})
              f1 = new_fecha.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            }else{
              f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            }
            //const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";

            var fecha1 = moment(f1);
            var fecha2 = moment(f2);
            //console.log(fecha2.diff(fecha1, 'days'), ' dias de diferencia');
            let f1_split=f1.split("T");
            let f2_split=f2.split("T");

            dataChaAxisy = [] ;
            dataCha = [] ;
            dataChaExport = [];
            i = 0;
            const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
           
            console.log(TagsSelecionado)
            let tags=TagsSelecionado.tags;
            //console.log(tags)  
            if(TagsSelecionado.name.includes('VoltajeR')){
              active_volt=true;
              tags=tags.filter(l=>l.name.includes('volt')
              )
            }else{
              tags=tags.filter(l=>l.name.includes('oxd')
                ||l.name.includes('temp')
                ||l.name.includes('oxs')
                ||l.name.includes('sal')
                ||l.name.includes('inyection')
                ||l.name.includes('setpoint')
                ||l.name.includes('banda_superior')
                ||l.name.includes('banda_inferior')
                ||l.name.includes('alarma_minima')
              )
            }
            console.log(tags)
            // console.time('loop');
            for (let i = 0; i < tags.length; i++) {
              this.setState({blocking: true});
              let APItagMediciones ="";
              APItagMediciones = `${API_ROOT}/measurement/xy/tag/${tags[i]._id}/${f1}/${f2}`;
              if(nombre_centro.includes(" Control")){
                APItagMediciones=`${API_ROOT}/measurement-control/xy/tag/${tags[i]._id}/${f1}/${f2}`;
              }
              // if(fecha2.diff(fecha1, 'days')==0){
              //   APItagMediciones = `${API_ROOT}/measurement_today/xy/tag/${tags[i]._id}/${f1}/${f2}`;
              // }
              //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
              //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/${f1_split[0]}T00:00/${f2_split[0]}T23:59`;
    
              let sw =0
              if (i=== tags.length-1)
                sw = 1;
                console.log("punto 1")
              this.loadDataChart(tags[i].shortName,APItagMediciones, ColorChart,sw,tags[i].workplaceId);
            }
            
            // console.timeEnd('loop');
            
     
         
         //   console.log (dataChaAxisy);

        }
      
   
    }

    //carga los datos para la Exportación de la tendencia
    loadDataChart2 = (shortName, URL, workplaceId) => {   
         
        // this.setState({blocking: true});
        // console.log(URL)
         const token = "tokenFalso";
               
         axios
         .get(URL, {
          headers: {  
            'Authorization': 'Bearer ' + token
           }
         })
        .then(response => { 
          if (response.data.statusCode === 200) {
            const dataChart = response.data.data;
          //   console.log(dataChart);
           //  this.setState({gridDataChart:dataChart});  
            
          // console.log(dataChart); 
          //  console.time('loop2');
            let _dataCharts = dataChart.map( item => { 
               return { x: moment(item.dateTime.substr(0,19)) , y : item.value }; 
             });

              //console.log(dataChart);
             
             let _dataChartsExport = dataChart.map( item => { 
               return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , y : item.value.toString().replace(".",",") }; 
             });
          load=false;
          let mydatachartExport = {}
          try{
            if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
              if(shortName.includes("oxd")){
                mydatachartExport = {  
                  unity : "mg/L",              
                  sonda: shortName.toString(),
                  measurements : _dataChartsExport
                  }      
                this.setState({Export1:_dataChartsExport})
              }
            }else{
              if(shortName.includes("oxd")){
                mydatachartExport = {  
                  unity : "mg/L",              
                  sonda: shortName.toString(),
                  measurements : _dataChartsExport
                  }      
                this.setState({Export1:_dataChartsExport})
              }
            }
              if(this.state.solo_Oxd){
                if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
                  if(shortName.includes("temp")){
                    mydatachartExport = {  
                      unity : "%",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export2:_dataChartsExport})
                  }
                  if(shortName.includes("oxs")){
                    mydatachartExport = {  
                      unity : "°C",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export3:_dataChartsExport})
                  }
                }else{
                  if(shortName.includes("oxs")){
                    mydatachartExport = {  
                      unity : "%",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export2:_dataChartsExport})
                  }
                  if(shortName.includes("temp")){
                    mydatachartExport = {  
                      unity : "°C",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export3:_dataChartsExport})
                  }
                }
                
                if(shortName.includes("sal")){
                  mydatachartExport = {  
                    unity : "PSU",              
                    sonda: shortName.toString(),
                    measurements : _dataChartsExport
                    }
                    this.setState({Export4:_dataChartsExport})
                }
                if(!this.state.setPoint){
                  if(shortName.includes("Set_Point")){
                    mydatachartExport = {  
                      unity : "set_point",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export5:_dataChartsExport})
                  }
                }
                if(this.state.TagsSelecionado.name.includes('VoltajeR')){
                  if(shortName.includes("volt")){
                    mydatachartExport = {  
                      unity : "volt",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export6:_dataChartsExport})
                  }
                }
                
              }
              
                //dataChaAxisy.push(axisy);  
                 dataChaExport.push(mydatachartExport);   
                //dataCha.push(mydatachart);  
           
              this.setState({
                dataExport:dataChaExport,
             });
          }catch(e){
            console.log("object")
          }
          
           
          //  console.log("dataExport: ",dataChaExport);

           //                 dataExport:dataChaExport,
           //   this.setState({dataExport:dataChaExport});  
           //   this.setState({dataCharts:dataCha});  

               console.timeEnd('loop2');
               //this.filtrar2();
          }
          else{
            load=false;
          }
        })
        .catch(error => {
          // console.log(error);
        });
  
      }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =() => {
        
        this.setState({buttonExport:'none'});
         const TagsSelecionado = this.state.TagsSelecionado;
 
         
         if (TagsSelecionado !== null){
          if(this.state.TagsSelecionado.name.toLowerCase().includes(' salinidad')){
            this.setState({MultiExport:false})
          }else{
            this.setState({MultiExport:true})
          }

            //this.filtrar();
             
             const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             let f1_split=f1.split("T");
             let f2_split=f2.split("T");
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;

             const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            
            //  console.time('loop');
            let tags=TagsSelecionado.tags;
            // console.log(TagsSelecionado.tags)
             for (let i = 0; i < tags.length; i++) {
               //this.setState({blocking: true});      
               //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/${tags[i]._id}/${f1_split[0]}T00:00/${f2_split[0]}T23:59`;
               let APItagMediciones = `${API_ROOT}/measurement/tag/${tags[i]._id}/${f1}/${f2}`;
              if(this.state.NameWorkplace.name.includes(" Control")){
                 APItagMediciones = `${API_ROOT}/measurement-control/tag/${tags[i]._id}/${f1}/${f2}`;
              }

              // console.log(APItagMediciones);
     
               this.loadDataChart2(tags[i].shortName,APItagMediciones, tags[i].workplaceId);
             }
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
      let data=Tags;
      try{
        if(this.state.NameWorkplace.name.includes(' Control')){
          data=data.map(l=>{
              let new_name=l.name.split(' ')
              let new_name2=String(new_name[2]).split('_')
              l.nameOrder=parseInt(new_name2[0])
              return l
          })
          // let voltajes=
          data=_.orderBy(data,["nameOrder"],["asc"])
          console.log(data)
       }
        else if(empresa=="multiexport"){
          data=_.orderBy(Tags,["name"],["asc"]);
        }else{
          data=_.orderBy(Tags,["_id"],["asc"]);
        }
      }catch(e){
        data=_.orderBy(Tags,["_id"],["asc"]);
      }
      
        return (
          <div>
               <FormGroup>
                    <Select                
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        // isMulti
                        name="colors"
                        options={data}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Sensor"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
        let value1 = this.state.start.format("DD-MM-YYYY") 
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
      }

    buttonChangeDate=(n)=>{
      if(this.state.TagsSelecionado!=null){
        let now = new Date();
        let hoy = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let new_fecha=moment(hoy).subtract(n, "days")
        // this.setState({
        //   start:new_fecha
        // })
        if(this.state.TagsSelecionado != null){
          disabled=true;
            this.setState({
                dataExport:[],
                buttonExport:'block'
            });
            this.filtrar(new_fecha);
        }
        
      }
    }

    getNameDispositivo(){
      if(this.state.TagsSelecionado!=null){
        return this.state.TagsSelecionado.name
      }else{
        return ""
      }
    }

    getArray(){
      try{
        let oxd=this.state.Export1;
        let oxs=this.state.Export2;
        let temp=this.state.Export3;
        let sal=this.state.Export4;
        let bat=this.state.Export6;
        //console.log(bat)
        
        if(this.state.TagsSelecionado!=null){
          let array=[]
          this.state.Export4.map((d,i)=>{
            if(this.state.TagsSelecionado.name.toLowerCase().includes('salinidad')){
              array.push({x:d.x, y3:d.y})
            }
            else if(this.state.TagsSelecionado.name.includes('VoltajeR')){
              //console.log("holanda")
              array.push({x:d.x, y4:bat[i].y})
            }
            else if(oxd!=null&&oxs!=null&&temp!=null&&sal!=null){
              array.push({
                      x:oxd[i].x,
                      y:oxd[i].y,
                      y1:oxs[i].y,
                      y2:temp[i].y,
                      y3:sal[i].y
                    })
            }else if(oxd==null){
              array.push( {
                x:oxs[i].x,
                y1:oxs[i].y,
                y2:temp[i].y,
                y3:sal[i].y
              })
            }else if(oxs==null){
              array.push({
                y:oxd[i].y,
                y2:temp[i].y,
                y3:sal[i].y
              })
            }else if(temp==null){
              array.push({
                y:oxd[i].y,
                y1:oxs[i].y,
                y3:sal[i].y
              })
            }
            else if(sal==null){
              array.push({
                y:oxd[i].y,
                y1:oxs[i].y,
                y2:temp[i].y,
              })
            }
          })
          return  array.reverse()
        }else{
          return []
        }
      }catch(e){
        console.log(e)
        return []
      }
      
    }

    showTableExport(){
      if(this.state.MultiExport && this.state.buttonExport=="none"&&load==false){
        return "block"
      }else{
        return "none"
      }
    }

    switchFilter(name){
      this.setState({[name]:!this.state[name]})
    }

    generateExport(){
      if (this.state.TagsSelecionado !== null){
        if(this.state.TagsSelecionado.name.includes('VoltajeR')){
          return<CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
                { label: "FECHA", key: "x" },
                { label: "Bateria (Volt)" , key: "y4" }
                // { label: "Temp (°C)", key: "y2" },
                // { label: "O2 SAT ( %)", key: "y1" },
                // { label: "Sal (PSU)", key: "y3" }
              ]                                         
            }
            data={this.getArray()}
            filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
            className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            target="_blank">
            Exportar Datos
          </CSVLink>
        }
        else if(!this.state.solo_Oxd){
          return<CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
                { label: "FECHA", key: "x" },
                { label: "O2 Disuelto (mg/L)" , key: "y" }
                // { label: "Temp (°C)", key: "y2" },
                // { label: "O2 SAT ( %)", key: "y1" },
                // { label: "Sal (PSU)", key: "y3" }
              ]                                         
            }
            data={this.getArray()}
            filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
            className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            target="_blank">
            Exportar Datos
          </CSVLink>
        }else{
          return<CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
                { label: "FECHA", key: "x" },
                { label: "O2 Disuelto (mg/L)" , key: "y" },
                { label: "Temp (°C)", key: "y2" },
                { label: "O2 SAT ( %)", key: "y1" },
                { label: "Sal (PSU)", key: "y3" }
              ]                                         
            }
            data={this.getArray()}
            filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
            className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            target="_blank">
            Exportar Datos
          </CSVLink>
        }
      }
    }

    generateTable(){
      let columna=[
          {
          Header: "FECHA",
          accessor: "x",
          width: 160
          },
      ]  
      if (this.state.TagsSelecionado !== null){
        if(this.state.TagsSelecionado.name.includes('VoltajeR')){
          //console.log("hola")
          columna.push(
            {
            Header: "Bateria (Volt)",
            accessor: "y4",                                                              
            width: 140
            })
        }
        else if(this.state.solo_Oxd){
          columna.push( {
            Header: "O2 mg/L",
            accessor: "y",                                                              
            width: 140
          })
          columna.push(
            {
            Header: "O2 %",
            accessor: "y1",                                                              
            width: 140
            },
            {
            Header: "°C",
            accessor: "y2",                                                              
            width: 140
            },
            {
            Header: "PSU",
            accessor: "y3",                                                              
            width: 140
            })
        }else if(!this.state.solo_Oxd){
          columna.push( {
            Header: "O2 mg/L",
            accessor: "y",                                                              
            width: 140
          })
        }
      }   
      
      
      
      return <ReactTable                                                           
            data={this.getArray()}
        
            // loading= {false}
            showPagination= {true}
            showPaginationTop= {false}
            showPaginationBottom= {true}
            showPageSizeOptions= {false}
            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
            defaultPageSize={10}
            columns={columna}
            
            className="-striped -highlight"
            />
    }
    
    showFilter(name){
      try{
        if(name.includes(' Control')){
          return "block"
        }else{
          return "none"
        }
      }catch(e){

      }
      
    }
    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
         
 
         let optionsChart1 = {

            
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                {title:active_volt?"Volt":"mg/L - °C"},
             axisY2 :{
                title:"% - PSU"
            },   
            
             }
         
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Tendencias</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Tendencia Historica</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">    
                                  <Row style={{display:`${this.showFilter(this.state.NameWorkplace.name)}`}}>
                                    <Col >
                                      <Button color={"primary"} outline={this.state.solo_Oxd} onClick={(()=>this.switchFilter("solo_Oxd"))}>Mg/L</Button>
                                      <Button color={"primary"} outline={this.state.setPoint} onClick={(()=>this.switchFilter("setPoint"))}>setPoint</Button>
                                      <Button color={"primary"} outline={this.state.histerisis} onClick={(()=>this.switchFilter("histerisis"))}>histerisis</Button>
                                      <Button color={"primary"} outline={this.state.inyecciones} onClick={(()=>this.switchFilter("inyecciones"))}>inyecciones</Button>
                                    </Col>
                                  </Row>        
                                  <br></br>                     
                                  <Row>
                                        <Col   md={12} lg={3}> 
                                            {this.renderPickerAutoApply(ranges, local, maxDate)}   
                                        </Col>
                                        <Col md={12}  lg={6}>
                                            {this.renderSelecTag(this.state.Tags)}
                                        </Col>
                                        
                                        <Col   md={12} lg={3}> 
                                            <ButtonGroup>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(31)
                                              })}>- 31 </Button>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(7)
                                              })}>- 7 </Button>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(1)
                                              })}>- 1 </Button>
                                            </ButtonGroup>
                                            <ButtonGroup style={{marginLeft:20}}>
                                                <Button color="primary"
                                                        outline
                                                        disabled={disabled}
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                          if(this.state.TagsSelecionado != null){
                                                          disabled=true;
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                            
                                                              this.filtrar();
                                                              }}
                                                          }
                                                >Filtrar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>      
                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                        <BlockUi tag="div" blocking={this.state.blocking} 
                                    loader={<Loader active type={this.state.loaderType}/>}>
                                    <Card className="main-card mb-1 p-0">
                                        <CardBody className="p-3">                                 
                                            <Row>
                                                <Col   md={12} lg={12}> 
                                                
                                                    <ResponsiveContainer height='100%' width='100%' >
                                                        <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                                    </ResponsiveContainer>   
                                                </Col> 
                                            </Row>
                                        </CardBody>
                                    </Card>
                            </BlockUi>

                        </Col>

                    </Row>
                    <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                    <Spinner color="primary" style={{ marginTop:`${5}%`,marginLeft:`${50}%`,display:load?"block":"none", width: '2rem', height: '2rem'}}/>
                            <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block"}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%`}}
                                onClick={() => {
                                        load=true;
                                        this.filtrar2();
                                        // console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button>
                    </Col>
                    <Col sm="4"></Col>
                    
                    
                    {/* {console.time("loop 3")} */}
                    
                    {
                                                          
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="3" key={data.sonda} style={{display:`${this.state.MultiExport?"none":"block"}`}}>
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        {/* <CSVLink
                                                                        
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: data.unity , key: "y" }
                                                                                // { label: "Temp ("+ data.unity2 + ")", key: "temp" },
                                                                                // { label: "O2 SAT ("+ data.unity3 + ")", key: "oxs" },
                                                                                // { label: "Sal ("+ data.unity4 + ")", key: "sal" }
                                                                            ]
                                                                        
                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                        </CSVLink> */}
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={data.measurements}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: data.unity,
                                                                    accessor: "y",                                                              
                                                                    width: 140
                                                                    }
                                                                    // {
                                                                    //   Header: "Temp ("+ data.unity2+ ")",
                                                                    //   accessor: "temp",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "O2 SAT ("+ data.unity3+ ")",
                                                                    //   accessor: "oxs",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "Sal ("+ data.unity4+ ")",
                                                                    //   accessor: "sal",                                                              
                                                                    //   width: 140
                                                                    // }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         
                         )
        
                            )
                            
                    }
                    <Card className="main-card mb-5 mt-3" style={{display:`${this.showTableExport()}`}}>
                        <CardHeader className="card-header-tab  ">
                          <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                              {this.getNameDispositivo()}
                          </div>
                          <div className="btn-actions-pane-right text-capitalize">
                              {this.generateExport()}
                          </div>                                                  
                        </CardHeader>
                        <CardBody className="p-10 m-10" >          
                          {this.generateTable()}
                      </CardBody>
                      </Card>
                                       

                    </Row>


                 
             
            
                                 

          

                    

               


                                                                 
                                                    
   

            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Tendencia);
