import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../../api-config'
import Loader from 'react-loaders';
import Media from 'react-media';
import {
    toast

} from 'react-toastify';
import CanvasJSReact from '../../../../../assets/js/canvasjs.react';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    Badge,Breadcrumb, BreadcrumbItem, ButtonGroup,UncontrolledTooltip
} from 'reactstrap';
import { Redirect} from 'react-router-dom';
import {logout,sessionCheck} from '../../../../../services/user';
//CustomInput,
import '../../../Zonas/style.css'
import {connect} from 'react-redux';

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import BlockUi from 'react-block-ui';
import Chart from 'react-apexcharts'
// import Ionicon from 'react-ionicons';

//import ventilador from '../../../../../src/assets/utils/images/misimagenes/fan.png';
//import valvula from '../../../../../src/assets/utils/images/misimagenes/pipe.png';
import { SizeMe } from 'react-sizeme';
import { number } from 'prop-types';
import {TagServices} from '../../../../../services/comun';
import * as _ from "lodash";
import Ionicon from 'react-ionicons';
import ModalGlobal from '../../../../Hooks/ModalGlobal';
import TreeStructure from '../ComponentTree/TreeStructure';


//console.log("props: "+this.props.Mensaje)

//console.log("props: "+JSON.stringify(this.props));
const CanvasJSChart = CanvasJSReact.CanvasJSChart;


let dataChaAxisy= []
let dataCha = []
let Id_Workplace=0;

let empresa=sessionStorage.getItem("Empresa");
let cont=0;
let workplace="";
let Ultimo_registro="";
let active_config="none";
let new_ponton=[];

class IndexTendenciaOnline2 extends Component {   
    constructor(props) {
        super(props);
        
        this.state = {   
            myDataChart1: [],   
            NameWorkplace:"",
            ActiveWorkplace:"",
            misTag: [],     
            isLoading: false,
            error: null,
            modal1: false,
            setpointOx:1,
            setpointOxs:[],         
            minOx:3,
            minOxs:[],
            maxOx:0,
            maxOxs:[],
            timeOn:5,
            timeOns:[],
            timeOff:0,
            timeOffs:[],
            alarmTemp:7,
            histeresisOx:0,
            histeresisOxs:[],
            estadoTK:[],
            habilitadoTK:[],
            OxdTK:[],
            Modulos:[],
            dataCharts:[],
            dataAxisy: [],
            redirect2:false,
            redirect3:false,
            ModalGlobal:false,

            activoTemp: true,
            activoHum: true,
            blocking1: false,  
            intentosGuardado: 0,
            check:true,
            check2:false,
            check3:false,
            
            bitValTK801:0,
            bitVenTK801:0,
            // bitValTK802:0,
            // bitVenTK802:0,
            // bitValS3:0,
            // bitVenS3:0,
            mySalaModal:0,
            MyTKN:[],
            nCard:[],
            Mis_Sondas:[],
            Mis_Sondas2:[],
            All_Locations:[],
            Mis_Registros:[],
            DataTags:[],
            DataTags2:[],
            Acceso:0,
            posicion:0,
            Zonas:[],
            Error:0,
            // Tags:[],
        }
        this.tagservices = new TagServices();
        //this.intervalIdTag1 = this.updateTags.bind(this);
        this.timeRef=React.createRef();

        // this.loadDataChar = this.loadDataChar.bind(this);
        // this.loadDataTag = this.loadDataTag.bind(this);
        // this.toggleModal1 = this.toggleModal1.bind(this);
        // this.almacenarConfig = this.almacenarConfig.bind(this);
        // this.confirmacionTermino = this.confirmacionTermino.bind(this);
    }
    
  
    componentDidMount = () => {
        window.scroll(0, 0);
        // alert(workplace.id)
        sessionCheck()
        //console.log(this.props)
        //this.getTags();
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        workplace=JSON.parse(sessionStorage.getItem("workplace"));
        this.setState({Mis_Registros:[]});
        let minutos=3;
        let segundos=60*minutos;
        let intervaloRefresco =segundos*1000;
        //const intervaloRefresco =24000;
        if(workplace!=null&&this.state.Acceso==0){
            Id_Workplace=workplace.id;
            //console.log(Id_Workplace)
            //alert(workplace.active)
            this.setState({NameWorkplace:workplace.name,Acceso:1});
            this.getSondas(workplace.id);
            this.timeRef.current = setInterval(this.updateTags.bind(this),intervaloRefresco);
        }
        //console.log(this.state.Mis_Sondas);
        let { emailUsuario,centroUsuario } = this.props;
        //alert(JSON.stringify(nameCentro))
        //console.log(centroUsuario)
        // this.getTags();
        this.setState({ isLoading: true });
        // this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        // // this.loadDataChar(); 
        // // this.loadDataTag();
        // this.intervalIdtag = setInterval(() => this.loadDataTag(),intervaloRefresco);
    }

    componentWillUnmount = () => {
       clearInterval(this.getSondas);
       clearInterval(this.updateTags);
       clearInterval(this.updateDivice);
       clearInterval(this.timeRef.current);
    }  

    // componentDidUpdate(prevProps, prevState){
    //     // alert(JSON.stringify(prevProps)); extrae los states globales de readux
    //     alert(prevProps.centroUsuario);
    // }

    componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:centro[1]});
        if(this.state.NameWorkplace!=centro[1]&&this.state.Acceso!=0){
            this.setState({Mis_Registros:[],Mis_Sondas:[],Mis_Sondas2:[],Modulos:[],DataTags:[],DataTags2:[],Error:0});
            Id_Workplace=centro[0].toString();
            this.getSondas(centro[0].toString());
        }
      }

    // getTags(){
    //     const EndPointLocation = `${API_ROOT}/tag`;
    //     axios
    //     .get(EndPointLocation)
    //     .then(response => {
    //         if(response.data.statusCode==200){
    //             //console.log("lala "+response.data.data)
    //            this.setState({Tags:response.data.data})
    //         }
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    // }

    getSondas=(id)=>{
        //console.log("wena :"+Id_Workplace);
        //alert(em[0].active)
        const EndPointTag = `${API_ROOT}/workPlace/${id}/zone`;
        let empresas=JSON.parse(sessionStorage.getItem("Centros"));
        //alert(activo)
        let Mis_Locations=[];
        let All_Locations=[];
        let Modulos=[];
        let Zonas=[];
        // var CancelToken = axios.CancelToken;
        // var cancel;
        // ,
        //     {
        //       cancelToken: new CancelToken(
        //           function executor(c) {
        //               cancel = c;
        //            })
        //     }
        this.tagservices.getZones(id,"GET")
        .then(response => {
            if(response.data.statusCode==200){
                let mis_zonas = response.data.data;
                let ind=0;
                
                mis_zonas.map((z,indice)=>{
                    if(indice==mis_zonas.length-1){
                        this.setState({isLoading: false})
                    }
                    if(z.active==true){
                        Modulos.push(z.name)
                    }
                    Zonas.push(z)
                    const EndPointLocation = `${API_ROOT}/zone/${z._id}/location/true`;
                    this.tagservices.getLocation(z._id)
                    .then(response => {
                        if(response.data.statusCode==200){
                           //console.log("ob"+response.data.data._id)
                           let locations=response.data.data;
                           console.log(locations)
                           locations.map((l,i)=>{
                            l.index=ind;
                            ind++;
                            l.module=z.name;
                            //let EndPointLocation=`${API_ROOT}/location/${l._id}/tag`;
                            let type="";
                            if(this.state.NameWorkplace.includes(' Control')){
                               if(!this.state.NameWorkplace.includes('Huenquillahue')){
                                    this.switchGraphic(false,false,true)
                                }
                                type="control";
                            }else{
                                type="monit";
                            }
                            this.tagservices.getRegistrosToday(l._id,null,type).then(respuesta=>{
                                //console.log(respuesta)
                                let res=active_config=="block"?respuesta:respuesta.filter(re=>re.oxd!=0&&re.temp!=0&&re.oxs!=0&&re.sal!=0)
                                
                                // if(type=="control"){
                                //     res=_.orderBy(res,["fecha_registros"],["asc"])
                                // }else{
                                //     res=_.orderBy(res,["fecha_registros"],["desc"])
                                // }
                                
                                this.setState({DataTags:this.state.DataTags.concat(_.orderBy(res,["fecha_registros"],["asc"])),
                                               DataTags2:this.state.DataTags2.concat(_.orderBy(res,["fecha_registros"],["asc"])),Error:1}) 
                            })
                           })
                           let loc_=[];
                           let empresa=sessionStorage.getItem("Empresa");
                           if(empresa!="sasa"){
                            let pri_loc=locations.filter(l=>l.name.includes(' 5m'))
                            let ult_loc=locations.filter(l=>l.name.includes(' 0.5m'))
                            let loc=_.orderBy(locations.filter(l=>!l.name.includes(' 0.5m')&&!l.name.includes(' 5m')),["name"],["asc"])
                            // if(empresa=="camanchaca"){
                            //     ult_loc.map(l=>{
                            //         loc_.push(l)
                            //     })
                            //     pri_loc.map(l=>{
                            //         loc_.push(l)
                            //     })
                            //     loc.map(l=>{
                            //         loc_.push(l)
                            //     })
                            // }else{
                            //     pri_loc.map(l=>{
                            //         loc_.push(l)
                            //     })
                            //     loc.map(l=>{
                            //         loc_.push(l)
                            //     })
                            //     ult_loc.map(l=>{
                            //         loc_.push(l)
                            //     })
                            // }
                            if(this.state.NameWorkplace.includes(' Control')){
                                loc=locations.map(l=>{
                                    let new_name=l.name.split(' ')
                                    let new_name2=String(new_name[2]).split('_')
                                    l.nameOrder=parseInt(new_name2[0])
                                    return l
                                })
                                loc_=_.orderBy(loc,["nameOrder"],["asc"])
                            }
                            else if(empresa=="multiexport"){
                                loc_=_.orderBy(locations,["name"],["asc"])
                            }else{
                                locations=locations.map(l=>{
                                    let split=l.name.split('m_');
                                    let split2=String(split[0]).split(' ')
                                    l.order=parseInt(split2[split2.length-1])
                                    return l
                                })
                                loc_=_.orderBy(locations,["order"],["asc"])
                            }
                           }else{
                            //    console.log(locations)
                            //  locations=locations.map(l=>{
                            //      let split=l.name.split('m_');
                            //      l.
                            //      return l
                            //  })
                             loc_=_.orderBy(locations,["_id"],["asc"])
                           }

                           //console.log("hola"+JSON.stringify(loc));
                           Mis_Locations.push(loc_.filter(l=>l.active==true));
                           All_Locations.push(loc_);
                           
                        }
                    })
                    .catch(error => {
                      //console.log(error);
                    });
                    Modulos=Modulos.filter(this.onlyUnique);
                })
                
                Modulos=Modulos.filter(this.onlyUnique);
               this.setState({ isLoading: false,Modulos,Mis_Sondas:Mis_Locations,Mis_Sondas2:Mis_Locations,Zonas,All_Locations:All_Locations});
            }
        })
        .catch(error => {
          //console.log(error);
        });
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    
    updateTags(){
        
        try{
            let new_sondas=this.state.Mis_Sondas;
            let n=this.state.Mis_Sondas.length;
            let c=0;
            let new_locations=[]
            while(c<n){
                let array=this.state.Mis_Sondas[c].map(m=>{
                    new_locations.push(m)
                })
                c++;
            }
            let sondas=new_locations;
            // console.log(sondas)
            sondas.map((l)=>{
                try{
                    let loc=l;
                    //let EndPointLocation=`${API_ROOT}/location/${l._id}/tag`;
                    let datos=this.state.DataTags.filter(d=>d.id_dispositivos==l._id)
                    let end_date=datos[datos.length-1].fecha_registros
                    end_date=moment(end_date).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
                    let f=end_date.split('.')
                    // console.log("hola_:"+f[0])
                    let type="";
                    if(this.state.NameWorkplace.includes(' Control')){
                        type="control";
                    }else{
                        type="monit";
                    }
                    this.tagservices.getRegistrosToday(l._id,f[0],type).then(respuesta=>{
                        let res=active_config=="block"?respuesta:respuesta.filter(re=>re.oxd!=0&&re.temp!=0&&re.oxs!=0&&re.sal!=0)
                        res=_.orderBy(res,["fecha_registros"],["asc"])
                        res.map(r=>{
                            this.setState({isLoading: false,DataTags:this.state.DataTags.filter(d=>d._id!=r._id).concat(r)})
                        })
                        // this.setState({isLoading: false,DataTags:respuesta})
                    })
                    l.tags=loc.tags
                    return l
                }
                catch(e){
                    //console.log(e)
                }
                
            })
            if(this.state.DataTags2.length>this.state.DataTags.length){
                this.setState({Mis_Sondas2:new_sondas,DataTags:this.state.DataTags2})
            }else{
                this.setState({Mis_Sondas2:new_sondas})
            }
            
        }
        catch(e){

        }
    }
 
    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    checkChangeHandler = (event) => { 
        // console.log(event.target.defaultChecked);  
        this.setState( { 
            ...this.state,
            [event.target.id]: !event.target.defaultChecked
        } );
    }

    getColor(unidad){
        // #FC3939
        //1,,3,2
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        // console.log(this.state.NameWorkplace)
        if(empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA"){
            if(unidad=="bat"){
                return miscolores[10]
            }
            else if(unidad=="oxd"){
                return miscolores[0]
            }else if(unidad=="oxs"){
                return miscolores[1]
            }else if(unidad=="temp"){
                return miscolores[2]
            }else if(unidad=="sal"){
                return miscolores[3]
            }
            else if(unidad=="oxd_n"){
                return 0
            }else if(unidad=="oxs_n"){
                return 1
            }else if(unidad=="temp_n"){
                return 2
            }else if(unidad=="sal_n"){
                return 3
            }else{
                return ""
            }
        }else{
            if(unidad=="bat"){
                return miscolores[10]
            }
            else if(unidad=="oxd"){
                return miscolores[0]
            }else if(unidad=="oxs"){
                return miscolores[2]
            }else if(unidad=="temp"){
                return miscolores[1]
            }else if(unidad=="sal"){
                return miscolores[3]
            }
            else if(unidad=="bat_n"){
                return 4
            }
            else if(unidad=="oxd_n"){
                return 0
            }else if(unidad=="oxs_n"){
                return 1
            }else if(unidad=="temp_n"){
                return 2
            }else if(unidad=="sal_n"){
                return 3
            }else{
                return ""
            }
        }
        
    }

    getButtons(data,ind){
        //console.log(data)
        if(data.name.includes("VoltajeR")){
            return <Fragment>
                <Col sm="3"> 
                    {this.getButton(data,ind,"bat"," volt")}
                </Col>
            </Fragment>
        }
        else if(empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA")
            return <Fragment>
                        <Col sm="3"> 
                            {this.getButton(data,ind,"oxd"," mg/L")}
                        </Col>
                        <Col sm="3"> 
                            {this.getButton(data,ind,"oxs","  °C")}
                        </Col>
                        <Col sm="3"> 
                            {this.getButton(data,ind,"temp"," %")}
                        </Col>
                        <Col sm="3"> 
                            {this.getButton(data,ind,"sal"," PSU")}
                        </Col>
                    </Fragment>
        else
            return <Fragment>
                <Col sm="3"> 
                    {this.getButton(data,ind,"oxd"," mg/L")}
                </Col>
                <Col sm="3"> 
                    {this.getButton(data,ind,"oxs","  %")}
                </Col>
                <Col sm="3"> 
                    {this.getButton(data,ind,"temp"," °C")}
                </Col>
                <Col sm="3"> 
                    {this.getButton(data,ind,"sal"," PSU")}
                </Col>
            </Fragment>
        // try{
        //     return tags.map((d,i)=>{
        //         if(d.name.includes("oxd")){
        //             return <Col sm="3"> 
        //                 {this.getButton(d,ind,"oxd"," mg/L")}
        //             </Col>
        //         }
        //         if(d.name.includes("oxs")){
        //             return <Col sm="3"> 
        //                 {this.getButton(d,ind,"oxs"," %")}
        //             </Col>
        //         }
        //         if(d.name.includes("temp")){
        //             return <Col sm="3"> 
        //                 {this.getButton(d,ind,"temp"," °C")}
        //             </Col>
        //         }
        //         if(d.name.includes("sal")&&this.getEndRegister("array",d)!=""){
        //             return <Col sm="3"> 
        //                 {this.getButton(d,ind,"sal"," PSU")}
        //             </Col>
        //         }
        //     })
        // }catch(e){
        //     //console.log(e)
        // }
        
    }

    valueType(valores,type){
        if(valores.length!=0){
            if(type=="oxd"){
                return valores[valores.length-1].oxd
            }
            if(type=="oxs"){
                return valores[valores.length-1].oxs
            }
            if(type=="temp"){
                return valores[valores.length-1].temp
            }
            if(type=="sal"){
                return valores[valores.length-1].sal
            }
            if(type=="bat"){
                return valores[valores.length-1].bat
            }
        }else{
            return []
        }
        
    }

    //genera cada boton para el Oxs, Temp y Oxd
    getButton = (data,i,type,unity)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        //console.log(data.tags)        
        let valores=this.state.DataTags.filter(d=>d.id_dispositivos==data._id)
        try{
            return<div className="widget-chart widget-chart-hover  p-0 p-0 ">
            <Button 
               onClick={ e => {
                   //this.slider1.slickGoTo(this.getColor(type+"_n"))
                    if(this.state.check)
                        try{
                            this["slider"+(i+1)].slickGoTo(this.getColor(type+"_n"))
                        }   
                        catch(e){
                            console.log(e)
                        }
               }}
               key={i} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[0]}>  
               
               <div className="size-boton mt-0" style={{color:`${this.getColor(type)}`}} > 
               {/* {this.getEndRegister("ultimo",data)}      */}
                   {this.valueType(valores,type)}
                   <span className="opacity-6  pl-0 size_unidad"> 
                   {unity}
                   </span>
               </div>
           </Button>                                              
       </div>
        }catch(e){
            //console.log(e)
        }
            
    }

    createOption=(dataCharts,dataAxisy)=>{
        //const {dataCharts,dataAxisy} = this.state; 

        let optionsChart1 = {

            
            data: dataCharts,
           
            height:200,
            zoomEnabled: true,
            //exportEnabled: true,
            animationEnabled: false, 
          
            toolTip: {
                shared: true,
                contentFormatter: function (e) {
                    var content = " ";
                    for (var i = 0; i < e.entries.length; i++){
                        content = moment(e.entries[i].dataPoint.x).format("DDMMM HH:mm");       
                     } 
                     content +=   "<br/> " ;
                    for (let i = 0; i < e.entries.length; i++) {
                        // eslint-disable-next-line no-useless-concat
                        content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                        content += "<br/>";
                    }
                    return content;
                }
            },
        //     legend: {
        //       horizontalAlign: "center", 
        //       cursor: "pointer",
        //       fontSize: 11,
        //       itemclick: (e) => {
        //           if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        //               e.dataSeries.visible = false;
        //           } else {
        //               e.dataSeries.visible = true;
        //           }
        //           this.setState({renderChart:!this.state.renderChart});   
                
        //       }
        //   }, 
           
            axisX:{
                 valueFormatString:  "HH:mm",
                 labelFontSize: 10,
        
            },
            axisY :
                dataAxisy.filter((data,i)=>!String(data.id).includes("PSU")&&!String(data.id).includes("%")),
            axisY2 :{
                title:"% - PSU"
            },      
            }
        return optionsChart1
    }
    getMydatachart=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "spline",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             //showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return mydatachart
      }
      getMyaxis=(color,unity)=>{
         let axisy= {
            id:unity,
            title: "Mg/L - °C",
             labelFontSize: 11,                   
            //  lineColor: color,
            //  tickColor: color,
            //  labelFontColor: color,
            //  titleFontColor: color,
            //suffix: " "+unity
            //includeZero: false
           }
           dataChaAxisy.push(axisy);
         return axisy;
      }
      getMyaxis3=(color,unity)=>{
        let axisy= {
           id:unity,
           title: "Volt",
            labelFontSize: 11,                   
           //  lineColor: color,
           //  tickColor: color,
           //  labelFontColor: color,
           //  titleFontColor: color,
           //suffix: " "+unity
           //includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }
      getMydatachart2=(data,name,i,color)=>{
         let mydatachart = {
              axisYIndex: i,
              type: "spline",
              axisYType: "secondary",
              legendText: name,
              name: name,
              color:color,
              dataPoints : data,
              xValueType: "dateTime",
              indexLabelFontSize:"30",
              //showInLegend: true,
              markerSize: 0,  
              lineThickness: 3
               }
               i++;
             dataCha.push(mydatachart);
           return mydatachart
       }
       getMyaxis2=(color,unity)=>{
         let axisy= {    
             id:unity,
             labelFontSize: 11, 
             title: "% - PSU",                
            //  lineColor: color,
            //  tickColor: color,
            //  labelFontColor: color,
            //  titleFontColor: color,
            // suffix: " "+unity
            //includeZero: false
           }
           dataChaAxisy.push(axisy);
         return axisy;
      }
 
      loadDataChart = (data,chartcolor,i,type) => { 
            let registros=data;
            let mediciones1=this.getEndRegister("array2",registros,"oxd")
            let mediciones2=this.getEndRegister("array2",registros,"temp")
            let mediciones3=this.getEndRegister("array2",registros,"oxs")
            let mediciones4=this.getEndRegister("array2",registros,"sal")
            let mediciones5=this.getEndRegister("array2",registros,"bat")
            // if(this.state.NameWorkplace.includes(' Control')){
                
            // }
            //console.log(mediciones)

            dataChaAxisy= []
            dataCha = []
            let  axisy= {};

            //let tags=data.tags;
            //console.log(tags)
            if(type!="bat"){
                this.getMydatachart(mediciones1,"Oxd",3,chartcolor[0]);
                axisy=this.getMyaxis(chartcolor[0]," ");
                i++;
                
                this.getMydatachart(mediciones2,empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA"?"Oxs":"Temp",2,empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA"?chartcolor[2]:chartcolor[1]);
                //axisy=this.getMyaxis(chartcolor[2]," °C");
                i++;
                
                this.getMydatachart2(mediciones3,empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA"?"Temp":"Oxs",1,empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA"?chartcolor[1]:chartcolor[2]);
                axisy=this.getMyaxis2(chartcolor[3]," %");
                i++;
                
                this.getMydatachart2(mediciones4,"Sal",0,chartcolor[3]);
                axisy=this.getMyaxis2(chartcolor[4]," PSU");
                i++;
            }else{
                this.getMydatachart2(mediciones5,"Bateria",0,chartcolor[4]);
                axisy=this.getMyaxis3(chartcolor[10]," Volt");
                i++;
            }
           
            // tags.map((d,i)=>{
            //         let mediciones=d.registros;
            //         let  axisy= {};
            //         if(d.name.includes("oxd")){
            //             this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
            //             axisy=this.getMyaxis(chartcolor[0]," ");
            //             i++;
            //         }
            //         if(d.name.includes("temp")){
            //             this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
            //             //axisy=this.getMyaxis(chartcolor[2]," °C");
            //             i++;
            //         }
            //         if(d.name.includes("oxs")){
            //             this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
            //             axisy=this.getMyaxis2(chartcolor[3]," %");
            //             i++;
            //         }
            //         if(d.name.includes("sal")&&this.getEndRegister("array",d)!=""){
            //             this.getMydatachart2(mediciones,"Sal",0,chartcolor[3]);
            //             axisy=this.getMyaxis2(chartcolor[4]," PSU");
            //             i++;
            //         }
                    
            // })

            // try{
            //     return 
            // }catch(e){
            //     console.log(e)
            // }
             //console.log("punto 2")
            //  let dataCharts = mediciones.map( item => { 
            //      return { x: item.x , y : item.y }; 
            //  });
            //  let dataCharts3 = mediciones.map( item => { 
            //     return { x: item.x , y : item.temp }; 
            // });
            //  let dataCharts2 = mediciones.map( item => { 
            //      return { x: item.x , y : item.oxs }; 
            //  });
            //  let dataCharts4 = mediciones.map( item => { 
            //      return { x: item.x , y : item.sal }; 
            //  });
             //console.log(mediciones)
            //  let mydatachart ={};
            //  let mydatachart2 ={};
            //  let mydatachart3 ={};
            //  let mydatachart4 ={};
            //  let  axisy= {};
             
            //  mydatachart=this.getMydatachart(dataCharts,"Oxd",3,chartcolor[0]);
            //  axisy=this.getMyaxis(chartcolor[0]," ");
            //  i++;
            //  mydatachart3=this.getMydatachart(dataCharts3,"Temp",2,chartcolor[1]);
            //  //axisy=this.getMyaxis(chartcolor[2]," °C");
            //  i++;
            //  mydatachart2=this.getMydatachart2(dataCharts2,"Oxs",1,chartcolor[2]);
            //  axisy=this.getMyaxis2(chartcolor[3]," %");
            //  i++;
            //  mydatachart4=this.getMydatachart2(dataCharts4,"Sal",0,chartcolor[3]);
            //  axisy=this.getMyaxis2(chartcolor[4]," PSU");
            //  i++;
           
             let option=this.createOption(dataCha,dataChaAxisy);
             return <CanvasJSChart id={data.name} key={data.code} options = {option} className="altografico  "  />
            //   this.setState({
            //       dataAxisy:dataChaAxisy,
            //       dataCharts:dataCha
            //  });   
            //   if (sw === 1)
            //      this.setState({blocking: false}); 
      }

      getEndRegister(type,data,tipo){
        // console.log(data.registros[data.registros.length-1].y)
        let val=data
        if(type=="ultimo"){
            //return val[val.length-1].fecha_registros
            if(tipo=="oxd")
                return val[val.length-1].oxd
            if(tipo=="oxs")
                return val[val.length-1].oxs
            if(tipo=="temp")
                return val[val.length-1].temp
            if(tipo=="sal")
                return val[val.length-1].sal
            if(tipo=="bat")
                return val[val.length-1].bat
        }
        if(type=="max"){
            //return  val.reduce((max, b) => Math.max(max, b.fecha_registros), val[0].fecha_registros)
            if(tipo=="oxd")
                return val.reduce((max, b) => Math.max(max, b.oxd), val[0].oxd)
            if(tipo=="oxs")
                return val.reduce((max, b) => Math.max(max, b.oxs), val[0].oxs)
            if(tipo=="temp")
                return val.reduce((max, b) => Math.max(max, b.temp), val[0].temp)
            if(tipo=="sal")
                return val.reduce((max, b) => Math.max(max, b.sal), val[0].sal)
            if(tipo=="bat")
            return val.reduce((max, b) => Math.max(max, b.bat), val[0].bat)
        }
        if(type=="min"){
            //return val.reduce((min, b) => Math.min(min, b.fecha_registros), val[0].fecha_registros);
            if(tipo=="oxd")
                return val.reduce((min, b) => Math.min(min, b.oxd), val[0].oxd)
            if(tipo=="oxs")
                return val.reduce((min, b) => Math.min(min, b.oxs), val[0].oxs)
            if(tipo=="temp")
                return val.reduce((min, b) => Math.min(min, b.temp), val[0].temp)
            if(tipo=="sal")
                return val.reduce((max, b) => Math.max(max, b.sal), val[0].sal)
            if(tipo=="bat")
                return val.reduce((max, b) => Math.max(max, b.bat), val[0].bat)
        }
        if(type=="prom"){
            //return val.reduce((min, b) => Math.min(min, b.fecha_registros), val[0].fecha_registros);
            if(tipo=="oxd")
                return ((val.reduce((a, b) => +a + +b.oxd, 0)/val.length)).toFixed(1)
            if(tipo=="oxs")
                return ((val.reduce((a, b) => +a + +b.oxs, 0)/val.length)).toFixed(1)
            if(tipo=="temp")
                return ((val.reduce((a, b) => +a + +b.temp, 0)/val.length)).toFixed(1)
            if(tipo=="sal")
                return ((val.reduce((a, b) => +a + +b.sal, 0)/val.length)).toFixed(1)
            if(tipo=="bat")
                return ((val.reduce((a, b) => +a + +b.bat, 0)/val.length)).toFixed(1)
        }
        if(type=="array"){
            return val.map(d=>{
                var fecha=new Date(d.fecha_registros);
                var zona_horaria=new Date(d.fecha_registros).getTimezoneOffset();
                zona_horaria=zona_horaria/60;
                let f=fecha.setHours(fecha.getHours())+zona_horaria;
                if(tipo=="oxd")
                    return {x:d.fecha_registros, value:d.oxd}
                if(tipo=="oxs")
                    return {x:d.fecha_registros, value:d.oxs}
                if(tipo=="temp")
                    return {x:d.fecha_registros, value:d.temp}
                if(tipo=="sal")
                    return {x:d.fecha_registros, value:d.sal}
                if(tipo=="bat")
                    return {x:d.fecha_registros, value:d.bat}
            })
        }
        if(type=="array2"){
            return val.map(d=>{
                var fecha=new Date(d.fecha_registros);
                var zona_horaria=new Date(d.fecha_registros).getTimezoneOffset();
                zona_horaria=zona_horaria/60;
                let f=fecha.setHours(fecha.getHours())+zona_horaria;
                if(tipo=="oxd")
                    return {x:d.fecha_registros, y:d.oxd}
                if(tipo=="oxs")
                    return {x:d.fecha_registros, y:d.oxs}
                if(tipo=="temp")
                    return {x:d.fecha_registros, y:d.temp}
                if(tipo=="sal")
                    return {x:d.fecha_registros, y:d.sal}
                if(tipo=="bat")
                    return {x:d.fecha_registros, y:d.bat}
            })
        }
        //console.log(val)
        if(val.length>0){
            return val
        }else{
            return ""
        }
    }

    getRegistros(data){
        let data2=this.state.DataTags;
        return data2.filter(re=>re.id_dispositivos==data._id)
        
    }

    getUnity(unity){
        if(empresa=="sasa"&& this.state.NameWorkplace!="PUNTA LUCU SMA"&&this.state.NameWorkplace!="CAGUACHE SMA"){
            if(unity=="°C"){
                return "%"
            }if(unity=="%"){
                return "°C"
            }else{
                return unity
            }
        }else{
            return unity
        }
    }
    
    //genera el grafico de Oxs, Temp y Oxd
    getChart = (data,i,unity,type)=>{
        // console.log(data.registros)
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        let registros=this.getRegistros(data);
        //console.log(registros)
        if(this.state.check2){
            return <Fragment>
            <div>
                    <ResponsiveContainer width="100%" height="100%">
                                {this.loadDataChart(registros,miscolores,i,type)}
                    </ResponsiveContainer>
            </div>
            </Fragment>}
        if(this.state.check){
            
            let mediciones=this.getEndRegister("array",registros,type)
            //console.log("GO"+JSON.stringify(mediciones))
            // console.log(registros)
            // registros=registros[0]
            // let mediciones=registros;
            return <Fragment><div className="widget-chart widget-chart2 text-left p-0" style={{display:`${this.state.check?"block":"none"}`}}>
            <div className="widget-chat-wrapper-outer">
                {/* {console.log(data.registros)} */}
                <div className="widget-chart-content widget-chart-content-lg  p-2">
                    <div className="widget-chart-flex ">
                        <div
                            className="widget-title opacity-9 text-muted text-uppercase">
                            {/* {data.name} */}
                        </div>
                    </div>

                    <div className="widget-numbers p-1 m-0">
                        <div className="widget-chart-flex">
                            <div>
                            {this.getEndRegister("ultimo",registros,type)}
                            {/* {type!="oxd"?"":registros[registros.length-1].oxd}  
                            {type!="temp"?"":registros[registros.length-1].temp}
                            {type!="oxs"?"":registros[registros.length-1].oxs}
                            {type!="sal"?"":registros[registros.length-1].sal}   */}
                                <small className="opacity-5 pl-1 size_unidad3"> {this.getUnity(unity)}</small>
                            </div>
                        </div>
                    </div> 
                    
                <div className=" opacity-8 text-focus pt-0">
                    <div className=" opacity-5 d-inline">
                    Max
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>  
                                                                                                    
                        <span className="pl-1 size_prom">
                                {this.getEndRegister("max",registros,type)}
                                {/* {type!="oxd"?"":registros.reduce((max, b) => Math.max(max, b.oxd), registros[0].oxd) } 
                                {type!="temp"?"":registros.reduce((max, b) => Math.max(max, b.temp), registros[0].temp) }
                                {type!="oxs"?"":registros.reduce((max, b) => Math.max(max, b.oxs), registros[0].oxs) } 
                                {type!="sal"?"":registros.reduce((max, b) => Math.max(max, b.sal), registros[0].sal) } */}
                                {/* data.reduce((max, b) => Math.max(max, b.oxd), data[0].oxd) */}
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline  ml-2">
                    Prom
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>                                                                                  
                        <span className="pl-1 size_prom">
                            {this.getEndRegister("prom",registros,type)}
                            {/* {type!="oxd"?"":((registros.reduce((a, b) => +a + +b.value, 0)/registros.length)).toFixed(1) }  
                            {type!="temp"?"":((registros.reduce((a, b) => +a + +b.value, 0)/registros.length)).toFixed(1) }
                            {type!="oxs"?"":((registros.reduce((a, b) => +a + +b.value, 0)/registros.length)).toFixed(1) }
                            {type!="sal"?"":((registros.reduce((a, b) => +a + +b.value, 0)/registros.length)).toFixed(1) }  */}
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline ml-2">
                    Min
                    </div>
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>                                                                                  
                        <span className="pl-1 size_prom">
                                {this.getEndRegister("min",registros,type)}
                                {/* {type!="oxd"?"":registros.reduce((min, b) => Math.min(min, b.value), registros[0].value) }  
                                {type!="temp"?"":registros.reduce((min, b) => Math.min(min, b.value), registros[0].value) }
                                {type!="oxs"?"":registros.reduce((min, b) => Math.min(min, b.value), registros[0].value) }
                                {type!="sal"?"":registros.reduce((min, b) => Math.min(min, b.value), registros[0].value) } */}
                                {/* { data.reduce((min, b) => Math.min(min, b.oxd), data[0].oxd)} */}
                        </span>
                    </div>
                    
                </div> 
            

                </div>

                <div className="d-inline text-secondary pr-1">                                                                                                                                                                 
                            <span className="pl-1">
                                    {/* { moment(data.registros[data.registros.length-1].x).format('HH:mm DD-MMM')} */}

                            </span>
                </div>


                <div
                    className="widget-chart-wrapper he-auto opacity-10 m-0">
                    <ResponsiveContainer height={150} width='100%'>

                        <AreaChart data={mediciones}
                                // animationDuration={1}
                                isAnimationActive = {false}
                                showInLegend = {true}
                                margin={{
                                    top: 0,
                                    right:10,
                                    left: -30,
                                    bottom: 0
                                }}>
                            
                                <Tooltip                                                                                                        
                                            labelFormatter={function(value) {
                                                return `${ moment(value).format('HH:mm DD-MMM')}`;
                                                }}
                                            formatter={function(value, name) {
                                            return `${value}`;
                                            }}
                                        />
                                <defs>
                                    <linearGradient id={"colorPv" + this.getColor(type+"_n")} x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="10%" stopColor={`${this.getColor(type)}`} stopOpacity={0.7}/>
                                        <stop offset="90%" stopColor={`${this.getColor(type)}`}stopOpacity={0}/>
                                    </linearGradient>
                                </defs>
                                <YAxis                                                                                    
                                tick={{fontSize: '10px'}}
                                // domain={['dataMin - 4','dataMax + 4']}
                                />
                                {/* <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />  */}
                                <XAxis
                                        dataKey={'x'}                                                                              
                                        hide = {false}
                                        tickFormatter={x => moment(x).format('HH:mm')}
                                        tick={{fontSize: '10px'}}
                                        />
                                <Area type='monotoneX' dataKey={'value'}
                            
                                    stroke={`${this.getColor(type)}`}
                                    strokeWidth='3'
                                    fillOpacity={1}
                                    fill={"url(#colorPv" + this.getColor(type+"_n") + ")"}/>
                            </AreaChart>

                    </ResponsiveContainer>
                </div>
            </div>
        </div></Fragment>
        }
    }

    //realiza los filtro y la llamada a generar el button para los 3 tags de la card
    //className={cx(data.active ? '' : 'opacity-3')} #FC3939
    getViewButton=(array,i)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        return array.map((data,i)=>
           <Col sm="6" > 
                <Button 
                onClick={ e => {
                    this["slider"+(i+1)].slickGoTo(i)
                }}
                    className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                    <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                                                                                            
                    {/* {data.oxd}  */}
                        {/* <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span> */}
                        <span className="opacity-6  pl-0 size_unidad">  unidad </span>
                    </div>
                    <div className="widget-subheading">
                    {/* {data.dateTime} */}
                    </div>
                </Button>  
               {/* {this.getButton(data,i)} */}
           </Col>
       )     
    }

    //realiza un switch por separado para optimizar la busqueda del Slider
    switchViewChart=(sonda,ind,settings)=>{
        let slider={};
        slider= slider => (this["slider"+(ind+1)]=slider)
       
        try{
            if(sonda.name.includes("VoltajeR")){
                return<Fragment key={sonda.code} ref={slider} {...settings}>
                    {this.getChart(sonda,ind,"volt","bat")}
                </Fragment>
            }
            if(this.state.check){
                return<Slider key={sonda.code} ref={slider} {...settings}>
                        {this.getChart(sonda,ind,"mg/L","oxd")}
                        {this.getChart(sonda,ind,"%","oxs")}
                        {this.getChart(sonda,ind,"°C","temp")}
                        {this.getChart(sonda,ind,"PSU","sal")}
                </Slider>
            }
            if(this.state.check2){
                return<Fragment key={sonda.code} ref={slider} {...settings}>
                    {this.getChart(sonda,ind,"mg/L","oxd")}
                </Fragment>
            }
        }
        catch(e){
            //console.log(e)
        }
            
    }

    endDate=(sonda,type)=>{
        try{
            let registros=this.state.DataTags.filter(d=>d.id_dispositivos==sonda._id)
            Ultimo_registro=moment(registros[registros.length-1].fecha_registros).format('YYYY-MM-DD HH:mm')
            //console.log(registros[registros.length-1].fecha_registros)
            return Ultimo_registro
        }catch(e){
            //console.log(e)
        }
        
    }

    handleChangeGlobal = event => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    updateDivice(id,name,n){
        let data={
            name:"Sensor "+name
        }

        axios
              .put(API_ROOT+`/location/${id}`,data)
              .then(response => {   
                  if(response.data.statusCode===201 || response.data.statusCode===200){
                    toast['success']('El dispositivo fue actualizado con exito', { autoClose: 4000 })
                    this.tagservices.getTags(id).then(tags=>{
                        //console.log(tags)
                        tags.map(t=>{
                            let id_tag=t._id;
                            let separa=t.nameAddress.split('_')
                            //console.log(separa)
                            let name_=name.split('_');
                            let new_name=name_[0]+"_"+separa[1];
                            let type=separa[1].split(' ')
                            //console.log(separa)
                            //console.log(name+" "+type[1])
                            let data2={
                                nameAddress:n==1?name+" "+type[1]:new_name
                            }
                            toast['success']('El dispositivo fue actualizado con exito '+new_name, { autoClose: 4000 })
                            axios
                                .put(API_ROOT+`/tag/${id_tag}`,data2)
                                .then(response => {   
                                    if(response.data.statusCode===201 || response.data.statusCode===200){
                                        toast['success']('El dispositivo fue actualizado con exito '+new_name, { autoClose: 4000 })
                                    }
                                })
                                .catch(error => {
                                    console.log(error)
                                });
                        })
                    }).catch(e=>e)
                  }
              })
              .catch(error => {
                  console.log(error)
              });
    }

    updateZones(name,i){
        let data={
            name:name
        }
        let zone=this.state.Zonas.filter((z,ind)=>ind==i)[0]
        let id="";
        id=zone._id
        if(id!="" && id!=undefined){
            axios
            .put(API_ROOT+`/zone/${id}`,data)
            .then(response => {   
                if(response.data.statusCode===201 || response.data.statusCode===200){
                    toast['success']('El Modulo fue actualizado con exito', { autoClose: 4000 })
                    //console.log(this.state.Mis_Sondas)
                    let n=this.state.Mis_Sondas.length;
                    let c=0;
                    let new_locations=[]
                    while(c<n){
                        let array=this.state.Mis_Sondas[c].map(m=>{
                            new_locations.push(m)
                        })
                        c++;
                    }
                    //console.log(new_locations.filter(m=>m.zoneId==id&&!m.name.includes('Salinidad')))
                    new_locations.filter(m=>m.zoneId==id&&!m.name.includes('Salinidad')&&!m.name.includes('Salinidad1')&&!m.name.includes('Salinidad2')).map((l,i)=>{
                        let nam=l.name.replace('OD ','')
                        let name_=nam.split('_');
                        let original=name.replace('Modulo ','Mod')
                        let name2=name_[0]+"_"+original
                        name2=name2.replace('Sensor ','')
                        //let rename=String(name_[1]).split(' ');
                        //rename=name_[0]+name+rename[1];
                        //console.log(name2)
                        this.updateDivice(l._id,name2.replace(" ",""),1)
                    })

                }
            })
            .catch(error => {
                console.log(error)
            });
        }else{
            toast['warning']('Ops! Algo Salio Mal', { autoClose: 4000 })
        }
    }

    handelkeyDown(e,i,id){
        if (e.key === 'Enter') {
            if(active_config=="block"){
                
                let valor=document.getElementById("disp"+id).value;
                //console.log(valor)
                if(valor!=""){
                    this.updateDivice(id,valor,0)
                }
                else{
                    toast['info']('Ops! Si desea editar el Dispositivo, por favor ingrese un nombre para el', { autoClose: 4000 })
                }
            }
        }
    }

    handelkeyDownZone(e,i){
        if (e.key === 'Enter') {
            if(active_config=="block"){
                
                let valor=document.getElementById("zone"+i).value;
                //console.log(valor)
                if(valor!=""){
                    this.updateZones(valor,i)
                }
                else{
                    toast['info']('Ops! Si desea editar el Modulo, por favor ingrese un nombre para el', { autoClose: 4000 })
                }
            }
        }
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    getName(name){
        if(name.includes('VoltajeR')){
            return name
        }
        let split="";
        if(name.includes('Sensor')){
            split=String(name).split('ensor ')
        }
        if(name.includes('Jaula')){
            split=String(name).split('Jaula ')
        }
        return `${split[1]==undefined?name:split[1]}`
    }

    visibleCard(name){
        if(active_config!="block"){
            if(!name.includes('VoltajeR')){
                return "block"
            }else{
                return "none"
            }
        }else{
            return "block"
        }
    }

    getCard=(i,settings,sonda)=>{
        return(
            <Card className="mb-3 mr-20" style={{display:`${this.visibleCard(sonda.name)}`}}>
                <CardHeader className="card-header-tab">
                    <div className="card-header-title font-size-ls text-capitalize font-weight-normal">
                        {this.createTooltip("disp"+sonda._id,sonda.name)}
                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6" />
                        {active_config=="block"?
                        <Input id={"disp"+sonda._id} autoComplete="off" placeholder={this.getName(sonda.name)} onChange={this.handleChangeGlobal} onKeyDown={((e)=>{
                            //alert(sonda._id)
                            this.handelkeyDown(e,i,sonda._id)
                        })}></Input>:<label id={"disp"+sonda._id}>{this.getName(sonda.name)}</label>}
                    </div>
                        <div className="btn-actions-pane-right text-capitalize">
                    {this.endDate(sonda)}
                    </div>
                    </CardHeader>
                    <CardBody className="p-0"  >
                        {this.getCardBody(i,settings,sonda)}
                    </CardBody>
            </Card>)
    }

    getCardBody=(ind,settings,sonda)=>{
        return  <Fragment>
                    <Card className="main-card mb-0">
                        <div className="grid-menu grid-menu-4col">
                            <Row className="no-gutters" key={ind}>
                                {
                                    this.getButtons(sonda,sonda.index)
                                }
                            </Row>
                        </div>
                    </Card>
                    {/* style={{display:`${check?"block":"none"}`}} */}
                    <div className="p-1 slick-slider-sm mx-auto" >
                    {this.switchViewChart(sonda,sonda.index,settings)}

                    </div>
                </Fragment>
    }
    
    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }
    
    cardSonda=(settings,modulo)=>{
        try{
            let sondas=this.state.Mis_Sondas;
            //console.log(sondas)
            //sondas=_.orderBy(sondas, ['_id'],['asc']);
            //console.log(sondas)
            //sondas=sondas.filter((data)=>!String(data.name).includes("Salinidad"));
            let i=0;
            // console.log(_.orderBy(sondas[0],["name"],["desc"]))
            if(this.state.NameWorkplace.includes(" Control")){
                if(this.state.NameWorkplace.includes('Huenquillahue')){
                    let new_sondas=[]
                    new_sondas.push(sondas[0].filter(s=>s.index==4)[0])
                    new_sondas.push(sondas[0].filter(s=>s.index==1)[0])

                    new_sondas.push(sondas[0].filter(s=>s.index==5)[0])
                    new_sondas.push(sondas[0].filter(s=>s.index==0)[0])
                    
                    new_sondas.push(sondas[0].filter(s=>s.index==3)[0])
                    new_sondas.push(sondas[0].filter(s=>s.index==2)[0])
                    sondas=[new_sondas]
                }
                //sondas=[_.orderBy(sondas[0],["zoneId"],["asc"])]
            }
            return sondas.map((s,i)=>{
                return s.filter((data)=>{
                    return !String(data.name).includes("Salinidad")
                }).map((data,ind)=>{

                    if(modulo==data.module){
                        if(this.state.check3
                            //this.state.NameWorkplace.includes(" Control")
                            ){
                            //this.state.check3
                            let registros=this.getRegistros(data);
                            // console.log(registros)
                            let alarma_min=0
                            try{
                                alarma_min=registros[registros.length-1].alarma_min
                            }catch(e){
                                alarma_min=0
                            }
                            //console.log()
                            const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
                            return this.getNewGraphic(data,"oxd",alarma_min,6)
                        }
                        if(empresa=="camanchaca"){
                            return<Col sm={this.state.posicion==0?3:12} key={ind+"s"} className="animated fadeIn fast">
                                {this.getCard(ind,settings,data)}
                            </Col> 
                        }
                        if(this.state.NameWorkplace.includes(" Control")){
                            return<Col sm={this.state.posicion==0?6:12} key={ind+"s"} className="animated fadeIn fast">
                                {this.getCard(ind,settings,data)}
                            </Col> 
                        }
                        else{
                            return<Col key={ind+"s"} sm={this.state.posicion==0?4:12} className="animated fadeIn fast">
                                {this.getCard(ind,settings,data)}
                            </Col> 
                        }
                    }
                })
            })
        }
        catch(e){
            console.log(e)
        }
    }

    getValidate(id){
        let empresas=JSON.parse(sessionStorage.getItem("Centros"));
        //console.log(Ultimo_registro)
        // let fecha="";
        // try{
        //     console.log(this.state.Mis_Sondas[0][0]._id)
        //     fecha=this.endDate(this.state.Mis_Sondas[0][0]._id);
        // }catch(e){
        //     console.log(e)
        // }
        
        try{
            let em=empresas.filter(data=>data.code==id).map((data)=>{
                return {"active":data.active,"date":data.endDate}
            })
            // let f=this.getOnline();
            let registros=this.state.DataTags
            //console.log(registros)
            let f=moment(registros[registros.length-1].fecha_registros).format('YYYY-MM-DD HH:mm')
            let now=new Date();
            let fecha_in=moment(now).subtract(15, "minutes").format('YYYY-MM-DD HH:mm:ss');
            //let fecha_ind=this.getOnline();
            //console.log(fecha_ind)
            if(em[0].active==1){
            return <Fragment><div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div>(Activo) {f>=fecha_in? <Badge color="success" pill>Online</Badge>: <Badge color="secondary" pill>Offline</Badge>}</Fragment>
            }else{
                return <Fragment><div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>(Inactivo)</Fragment>
            }
        }
        catch(e){
            //console.log(e)
        }
        
    }
    getOnline(){
        try{
            let data=this.state.Mis_Sondas2[0].filter(l=>l.active==true);
            //console.log(data)
            let tags=data[0].tags;
            //console.log(tags[0])
            //return data.dateTimeLastValue;
            return tags.filter((d,i)=>i==0).map((d)=>{
                //console.log("WEMA "+moment(d.registros[d.registros.length-1].x).format('YYYY-MM-DD HH:mm:ss'))
                return moment(d.registros[d.registros.length-1].x).format('YYYY-MM-DD HH:mm:ss')
            })
        }
        catch(e){
            //console.log(e)
        }
    }

    switchGraphic(check,check2,check3){
        this.setState({check,check2,check3});
    }

    existData(){
        if (this.state.Error>0){
            return this.state.DataTags.length==0&&(
                <Col md={12}>
                    <h3>No hay registros disponibles...</h3>
                </Col>
            )
        }
    }

    bodyCard(data,settings,i){
        // let Push=window.Push;
        // Push.create('Hello World!')
        return <CardBody>
                    <CardTitle>
                        {active_config=="block"?
                        <>
                            <Input autoComplete="off" id={"zone"+i} placeholder={data} 
                            style={{maxWidth:250}}
                            onChange={this.handleChangeGlobal} onKeyDown={((e)=>{
                                //alert(sonda._id)
                                this.handelkeyDownZone(e,i)
                            })} />
                        </>
                        :data}
                    </CardTitle>
                        <Row key={i+"m"}>
                            {/* {this.existData()} */}
                            {this.cardSonda(settings,data)}
                        </Row>
                </CardBody>
    }

    getNewGraphic(location,type,alarmaMin,size){
        //console.log(location.name)
        
        if(!location.name.includes('Voltaje')){
            let color=type=="oxd"?"#2E93fA":this.getColor(type)
            let registros=this.getRegistros(location);
            //console.log(registros)
            let mediciones=[];
            // if(location.name.includes('Voltaje')){
            //     color="#775DD0"
            //     mediciones=this.getEndRegister("array2",registros,"bat")
            // }else{
            mediciones=this.getEndRegister("array2",registros,"oxd")
            // console.log(mediciones.filter(m=>m.y!=0))
            //}
            
            let array_fechas=[];
           
            //console.log(mediciones.map(m=>m.x))
            // console.log(mediciones.map(m=>m.y))
            // console.log(mediciones.length>0?Math.max(mediciones.map(m=>m.y)):0)
            let maximo=mediciones.reduce((max, b) => Math.max(max, b.y), mediciones[0].y)
            // console.log(maximo)
            let conf= {
                series: [{
                    name: type,
                    data: mediciones.map(m=>m.y),
                    color:color
                  },
                ],
                  options:{
                    chart: {
                        type: 'line',
                        width: 100,
                        height: 35,
                        sparkline: {
                          enabled: true
                        }
                    },
                    yaxis:{
                        show: false,
                        max:maximo+4,
                        min:0
                    },
                    tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function (seriesName) {
                                return ''
                            }
                        }
                    },
                    stroke: {
                          curve: 'smooth'
                    },
                    marker: {
                        show: false
                        }
                    },
                    annotations: {
                        yaxis: [
                            {
                            y: alarmaMin,
                            borderColor: 'rgb(255, 64, 64)',
                            strokeDashArray: 5,
                            label: {
                                borderColor: 'rgb(255, 64, 64)',
                                style: {
                                color: '#fff',
                                background: 'rgb(255, 64, 64)'
                                },
                                text: `Alarma Minima ${alarmaMin} .`
                            }
                            }
                        ]
                    },
                    xaxis: {
                        type: 'datetime',
                        categories: mediciones.map(m=>{
                        var fecha=new Date(m.x);
                        var zona_horaria=new Date(m.x).getTimezoneOffset();
                        zona_horaria=zona_horaria/60;
                        fecha.setHours(fecha.getHours()-zona_horaria);
                        return fecha.getTime()
                        })
                    },
                    tooltip: {
                        x: {
                        format: 'dd/MM/yyyy HH:mm'
                        },
                    },
                  },
                //   options: {
                //     yaxis:{
                //         max:maximo+4,
                //         min:0
                //     },
                //     chart: {
                //       height: 350,
                //       type: 'area',
                //       sparkline: {
                //         enabled: true,
                //       },
                //     },
                //     annotations: {
                //         yaxis: [
                //           {
                //             y: alarmaMin,
                //             borderColor: 'rgb(255, 64, 64)',
                //             strokeDashArray: 5,
                //             label: {
                //               borderColor: 'rgb(255, 64, 64)',
                //               style: {
                //                 color: '#fff',
                //                 background: 'rgb(255, 64, 64)'
                //               },
                //               text: `Alarma Minima ${alarmaMin} .`
                //             }
                //           }
                //         ]
                //     },
                //     dataLabels: {
                //       enabled: false
                //     },
                //     stroke: {
                //       curve: 'smooth'
                //     },
                //     marker: {
                //         show: false
                //       },
                //     xaxis: {
                //       type: 'datetime',
                //       categories: mediciones.map(m=>{
                //         var fecha=new Date(m.x);
                //         var zona_horaria=new Date(m.x).getTimezoneOffset();
                //         zona_horaria=zona_horaria/60;
                //         fecha.setHours(fecha.getHours()-zona_horaria);
                //         return fecha.getTime()
                //       })
                //     },
                //     tooltip: {
                //       x: {
                //         format: 'yyyy/MM/dd HH:mm'
                //       },
                //     },
                //   },
              }
            return<Col md={size}>
                    <Card style={{marginTop:10}}>
                        <Row>
                            <Col style={{marginLeft:15,marginTop:15}}>
                                <div className="widget-chart-content widget-chart-content-lg pb-0">
                                    <div className="widget-chart-flex">
                                        <div className="widget-title opacity-5 text-muted text-uppercase">
                                            <Row>
                                                <Col md={12}>
                                                    <i class="header-icon lnr-laptop-phone mr-3 text-muted opacity-6" style={{fontSize:25}}></i>
                                                    {location.name}
                                                </Col>
                                                <Col md={2}></Col>
                                                <Col md={10}>
                                                    <b><font style={{color:"black"}}>{this.endDate(location)}</font></b>
                                                </Col>
                                            </Row>
                                            
                                        </div>
                                    </div>
                                    <div className="widget-numbers">
                                        <Row>
                                            <Col>
                                                <font size={6} color={color}><b>
                                                    <span className="opacity-10 text-success pr-2">
                                                        <i className="fa fa-angle-up"></i>
                                                    </span>
                                                    <span>{mediciones[mediciones.length-1].y}</span></b>
                                                </font>
                                                <font size={5} >
                                                    <small className="opacity-5 pl-1"> mg/L</small>
                                                </font>
                                                <font size={4}>
                                                    <span className="pl-2" style={{color:`${color}`}}> </span>
                                                </font>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Chart options={conf.options} series={conf.series} type="area" width={`${100}%`} height={`${50}%`} />
                            </Col>
                        </Row>
                    </Card>
                  </Col>
        }
        
        // const config = {
        //     binaryThresh: 0.5,
        //     hiddenLayers: [3], // array of ints for the sizes of the hidden layers in the network
        //     activation: 'sigmoid', // supported activation types: ['sigmoid', 'relu', 'leaky-relu', 'tanh'],
        //     leakyReluAlpha: 0.01, // supported for activation type 'leaky-relu'
        //   };
           
        //   // create a simple feed forward neural network with backpropagation
        //   const brain=window.brain
        // //   const net = new brain.NeuralNetwork(config);
           
        // //   net.train([
        // //     { input: [0, 0], output: [0] },
        // //     { input: [0, 1], output: [1] },
        // //     { input: [1, 0], output: [1] },
        // //     { input: [1, 1], output: [0] },
        // //   ]);

        // var net = new brain.recurrent.LSTMTimeStep();

	
		// /* Entrenamiento */
		// net.train( [
		// 	[1,2,3,4,5],
		// 	[6,7,8,9,10],
		// 	[11,12,13,14,15],
		// 	] );

		// /* Predicción */
		// var output=net.run( [1,2,3] );
        
           
          //const output = net.run([1, 0]); // [0.987]

    }

    getSize(){
        // this.state.posicion==0?
        //     this.state.NameWorkplace.includes(' Control')?40:100
        // :33
        if(this.state.posicion==0){
            if(this.state.NameWorkplace.includes(' Control')){
                if(this.state.NameWorkplace.includes('Huenquillahue')){
                    return 100
                }else{
                    return 40
                }
            }else{
                return 100
            }
        }else{
            return 33
        }
    }

    toggleModalGlobal(active){
        this.setState({ModalGlobal:active})
    }

    render() {
        // var this_url=window.location.href;
        // var n_nave=this_url.split('/');
        if(this.state.redirect2){
            return <Redirect to="/dashboards/configuracion"></Redirect>
        }
        if(this.state.redirect3){
            return <Redirect to="/dashboards/configuracion_sma"></Redirect>
        }
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { isLoading,error} = this.state;
         
          const settings = {
            autoplaySpeed:6000,
            autoplay: false,        
            centerMode: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };
       
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if (error) {
            return <p>{error.message}</p>;
        }
    
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }

        return (
            <Fragment>     
                {/* {console.log(this.state.Mis_Registros)} */}
                {/* {console.log(this.state.Mis_Sondas)}
                {console.log(this.state.Modulos)} */}
                {/* {console.log("HOLA:"+JSON.stringify(this.state.Zonas))} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Tendencias</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Tendecia Online</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">
                        {active_config=='block'?(
                            <ModalGlobal
                            buttonName={this.state.NameWorkplace}
                            typeButton="Badge"
                            title={"Estructura "+this.state.NameWorkplace}
                            >
                                <TreeStructure 
                                modulos={this.state.Zonas} 
                                locations={this.state.All_Locations}
                                />
                            </ModalGlobal>
                        ):this.state.NameWorkplace}
                      {/* {alert(this.state.ActiveWorkplace)} */}
                        {this.getValidate(Id_Workplace)}
                      {/* {console.log(this.state.DataTags)} */}
                      
                      {/* <div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>(Offline) */}
                      </BreadcrumbItem>
                      <Media queries={{ small: { maxWidth: 1158 } }}>
                        {matches =>
                            matches.small ? (
                                <ButtonGroup style={{marginLeft:`${62}%`,marginTop:5}}>
                                                <Button color="primary" style={{display:`${this.state.NameWorkplace.includes(' Control')?"block":"none"}`}} outline  onClick={() => {
                                                    //this.setState({check2:true,check:false})
                                                    this.switchGraphic(false,false,true);
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-keypad btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                                    //this.setState({check:true,check2:false})
                                                    this.switchGraphic(true,false,false);
                                                }}>
                                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                                    //this.setState({check:false,check2:false})
                                                    this.switchGraphic(false,false,false);
                                                }}>-</Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    //this.setState({check2:true,check:false})
                                                    this.switchGraphic(false,true,false);
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    if(this.state.posicion==0){
                                                        this.setState({posicion:1})
                                                    }else{
                                                        this.setState({posicion:0})
                                                    }
                                                }} >
                                                    <i style={{fontSize:20,transform:`${this.state.posicion==0?"":"rotate(90deg)"}`}} className={`pe-7s-more`} ></i>
                                                </Button>
                                </ButtonGroup>
                            ) : (
                                <ButtonGroup style={{marginLeft:`${88}%`,marginTop:-20}}>
                                                <Button color="primary" style={{display:`${this.state.NameWorkplace.includes(' Control')?"block":"none"}`}} outline={!this.state.check3}  onClick={() => {
                                                    //this.setState({check2:true,check:false})
                                                    this.switchGraphic(false,false,true);
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-keypad btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline={!this.state.check} onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                                    //this.setState({check:true,check2:false})
                                                    this.switchGraphic(true,false,false);
                                                }}>
                                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                                    //this.setState({check:false,check2:false})
                                                    this.switchGraphic(false,false,false);
                                                }}>-</Button>
                                                <Button color="primary" outline={!this.state.check2}  onClick={() => {
                                                    //this.setState({check2:true,check:false})
                                                    this.switchGraphic(false,true,false);
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    if(this.state.posicion==0){
                                                        this.setState({posicion:1})
                                                    }else{
                                                        this.setState({posicion:0})
                                                    }
                                                }} >
                                                    <i style={{fontSize:20,transform:`${this.state.posicion==0?"":"rotate(90deg)"}`}} className={`pe-7s-more`} ></i>
                                                </Button>
                                </ButtonGroup>
                            )
                        }
                        </Media>
                        <BreadcrumbItem active tag="span" style={{display:active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion</Badge></a></BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect3:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion SMA</Badge></a></BreadcrumbItem>
                      </Breadcrumb>
                      {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        MODULOS
                                    </div>
                                    
                                </div>
                                <div className="page-title-actions"> 
                                </div>                    
                            </div>
                        </div> */}
                         {/* <Row>
                            <Col md={4}><Card>1</Card></Col>
                            <Col md={4}><Card>2</Card></Col>
                            <Col md={4}><Card>3</Card></Col>
                         </Row> */}
                        <Row>
                        {/* {this.getNewGraphic([],"oxd",33,4)} */}
                        {this.existData()}
                        {this.state.Modulos.filter(this.onlyUnique).map((data,i)=>{
                            if(this.state.check3){
                                return <Col><Card md={4} style={{maxWidth:`${this.state.posicion==0?40:100}%`}}>
                                    {this.bodyCard(data,settings,i)}
                                </Card></Col>
                            }else{
                                return <Card md={4} style={{maxWidth:`${this.getSize()}%`}}>
                                    {this.bodyCard(data,settings,i)}
                                </Card>
                            }
                            
                        })}
                        </Row>
           
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
  });

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(IndexTendenciaOnline2);