import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../../services/comun';
import {logout,sessionCheck} from '../../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");
let disabled=false;
let load=false;
let visible="none";
let active_config="none"
let workplaceId=false;

class Datos_Historicos extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          Export1:[],
          Export2:[],
          Export3:[],
          Export4:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          MultiExport:false,
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);   
        this.filtrar2 =this.filtrar2.bind(this);   
    }

  
    componentDidMount = () => {
        visible="none";
        window.scroll(0, 0);
        sessionCheck()
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        // this.tagservices.getDataTag().then(data => 
        //     this.setState({Tags: data})
        // );  
        //this.this_Centro=setInterval(()=>this.getIdCentro(),2000);
        //var data = JSON.parse(sessionStorage.getItem('workplace'));
        // alert(JSON.stringify(data.id));
        // this.getIdCentro();
        this.setState({redirect:false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            this.getSondas(workplace.id);
        }
      }

      componentWillUnmount() {
        clearInterval(this.getSondas);
      } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        this.getSondas(centro[0].toString());
        this.setState({redirect:true})
      }

      getSondas=(id)=>{
        const EndPointTag = `${API_ROOT}/workPlace/${id}/zone`;
        let Mis_Locations=[];
        axios
        .get(EndPointTag)
        .then(response => {

            let zonas = response.data.data;
            let ind=0;
            let count=zonas.length-1;
            zonas.filter(z=>z.active==true).map((z,j)=>{
              const EndPointLocation = `${API_ROOT}/zone/${z._id}/location/true`;
                    axios
                    .get(EndPointLocation)
                    .then(response => {
                        if(response.data.statusCode==200){
                           //console.log("ob"+response.data.data._id)
                           let locations=response.data.data;
                           z.locations=locations.filter(l=>!l.name.includes(" Salinidad"));
                           this.setState({isLoading: false,Tags:this.state.Tags.filter((t)=>t._id!=z._id&&t.code!=z.code).concat(z)})
                           locations.filter(l=>l.active==true).map((l,i)=>{
                            l.index=ind;
                            ind++;
                            const EndPointLocation = `${API_ROOT}/location/${l._id}/tag`;
                            axios
                            .get(EndPointLocation)
                            .then(response => {
                                if(response.data.statusCode==200){
                                   //console.log("ob"+response.data.data._id)
                                   let tags=response.data.data;
                                   l.tags=tags;
                                  //  this.setState({isLoading: false,Tags:this.state.Tags.filter((t)=>t._id!=z._id&&t.code!=z.code).concat(z)})
                                }
                            })
                            .catch(error => {
                              console.log(error);
                            });
                           })
                           let loc=_.orderBy(locations,["name"],["asc"])
                           Mis_Locations.push(loc);
                        }
                    })
                    .catch(error => {
                      console.log(error);
                    });
            })

             
        })
        .catch(error => {
          console.log(error);
        });
    }
    //  getIdCentro=()=>{
    //     let url = window.location.href;
    //     //console.log(alert);
    //     let s=url.split('/');
    //     console.log(s[s.length-1])

    //     let now = new Date(); 
    //     const f1 = moment(now).subtract(360, "minutes").format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    //     const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";  
    //     const token = "tokenFalso";

    //     axios.get(`${API_ROOT}/measurement/xy/tag/${s[s.length-1]}/${f1}/${f2}`, {
    //          'Authorization': 'Bearer ' + token
    //     })
    //     .then(response => {
    //         const measurements = response.data.data;
    //         console.log(measurements);
    //         //this.setState({ dateTimeRefresh:measurements[measurements.length-1].x});
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    //     return s[s.length-1]
    //  }
    


    //carga los datos para la Exportación de la tendencia
    loadDataChart2 = (shortName, URL, ind,largo,workplaceId) => {   
        
        //console.log(URL)
         const token = "tokenFalso";
         axios.get(URL, {headers: {'Authorization': 'Bearer ' + token}})
        .then(response => { 
          if (response.data.statusCode === 200) {
            let dataChart = response.data.data;

            var data=dataChart.map((d)=>{
              d.name=shortName;
              d.dateTime=moment(d.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm')
              d.value=d.value.toString().replace(".",",")
              d.ind=ind;
              return d;
            })
            //console.log(data)            
            if(shortName=="oxd"){
              console.log(data)
              this.setState({
                Export1:this.state.Export1.concat(data)
              })
            }
            if(shortName=="oxs"){
              this.setState({
                Export2:this.state.Export2.concat(data)
              })
            }
            if(shortName=="temp"){
              this.setState({
                Export3:this.state.Export3.concat(data)
              })
            }
            if(shortName=="sal"){
              if(largo==ind){
                this.setState({
                  Export4:this.state.Export4.concat(data),blocking: false
                })
              }else{
                this.setState({
                  Export4:this.state.Export4.concat(data)
                })
              }
             
            }
            
         }
        })
        .catch(error => {
          // console.log(error);
        });
  
      }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =(new_fecha) => {
        this.setState({buttonExport:'none',Export1:[],Export2:[],Export3:[],Export4:[],blocking: true});
         const TagsSelecionado = this.state.TagsSelecionado;
         
         if (TagsSelecionado !== null){
          visible="none";
          let f1="";
          let f2="";
          if(new_fecha!=null){
            this.setState({start:new_fecha,MultiExport:true})
            f1 = new_fecha.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
          }else{
            f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
          }
            //this.filtrar();
             //f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             let f1_split=f1.split("T");
             let f2_split=f2.split("T");
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;

             const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            
            //  console.time('loop');
            let locations=TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'));
            let largo=locations.length-1;
            for(let h=0;h<locations.length;h++){
              if(largo==h){
                disabled=false;
              }
              let tags=locations[h].tags;
              if(tags[0].workplaceId=="5fbd33eabca38e0394bb24b8"||tags[0].workplaceId=="602ef4ba3a885f18fd4b6008"||tags[0].workplaceId=="5fdcdcbab11a950c53e3575c"){
                workplaceId=true;
              }
              tags=tags.filter(l=>l.name.includes('oxd')
                              ||l.name.includes('temp')
                              ||l.name.includes('oxs')
                              ||l.name.includes('sal')
                              // ||l.name.includes('inyection')
                              // ||l.name.includes('setpoint')
                              // ||l.name.includes('banda_superior')
                              // ||l.name.includes('banda_inferior')
                            )
               for (let i = 0; i < tags.length; i++) {
                let APItagMediciones="";
                if(this.state.NameWorkplace.name.includes(" Control")){
                  APItagMediciones=`${API_ROOT}/measurement-control/tag/${tags[i]._id}/${f1}/${f2}`;
                }else{
                  APItagMediciones = `${API_ROOT}/measurement/tag/${tags[i]._id}/${f1}/${f2}`;
                }
                //console.log(APItagMediciones);
                 this.loadDataChart2(tags[i].shortName,APItagMediciones,h,largo,tags[i].workplaceId);
               }
            }
            //console.log(this.state.Export1)
            
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        // isMulti
                        name="colors"
                        options={_.orderBy(Tags,["name"],["ASC"])}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Sensor"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
        let value1 = this.state.start.format("DD-MM-YYYY") 
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
      }

    buttonChangeDate=(n)=>{

      if(this.state.TagsSelecionado!=null){
        let now = new Date();
        let hoy = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let new_fecha=moment(hoy).subtract(n, "days")
        // this.setState({
        //   start:new_fecha
        // })
        if(this.state.TagsSelecionado != null){
          disabled=true;
            this.setState({
                dataExport:[],
                buttonExport:'block'
            });
            this.filtrar2(new_fecha);
        }
        
      }
    }

    getNameDispositivo(){
      if(this.state.TagsSelecionado!=null){
        return this.state.TagsSelecionado.name
      }else{
        return ""
      }
    }

    createArray(ind,fecha,oxd,oxs,temp,sal){
      let obj={};
      if(ind==0){
        obj["x"+ind]=fecha;
        obj["oxd"+ind]=oxd;
        obj["oxs"+ind]=oxs;
        obj["temp"+ind]=temp;
        obj["sal"+ind]=sal;
      }else{
        obj["oxd"+ind]=oxd;
        obj["oxs"+ind]=oxs;
        obj["temp"+ind]=temp;
        obj["sal"+ind]=sal;
      }
      return obj
      
    }

    pushArray(contador,g,g1,g2,g3,g4,g5,g6,g7,ind,ind1,ind2,ind3,ind4,ind5,ind6,ind7){
      let c=0;
      let array1=[]
      while(c<contador){
      c++;
      let data={}
      try{
        data["x0"]=g.oxd[c-1].dateTime
        data["oxd"+ind]=g.oxd[c-1].value
        data["oxs"+ind]=g.oxs[c-1].value
        data["temp"+ind]=g.temp[c-1].value
        data["sal"+ind]=g.sal[c-1].value
      if(g1!=null){
        data["oxd"+ind1]=g1.oxd[c-1].value
        data["oxs"+ind1]=g1.oxs[c-1].value
        data["temp"+ind1]=g1.temp[c-1].value
        data["sal"+ind1]=g1.sal[c-1].value
      }
      if(g2!=null){
        data["oxd"+ind2]=g2.oxd[c-1].value
        data["oxs"+ind2]=g2.oxs[c-1].value
        data["temp"+ind2]=g2.temp[c-1].value
        data["sal"+ind2]=g2.sal[c-1].value
      }
      if(g3!=null){
        data["oxd"+ind3]=g3.oxd[c-1].value
        data["oxs"+ind3]=g3.oxs[c-1].value
        data["temp"+ind3]=g3.temp[c-1].value
        data["sal"+ind3]=g3.sal[c-1].value
      }
      if(g4!=null){
        data["oxd"+ind4]=g4.oxd[c-1].value
        data["oxs"+ind4]=g4.oxs[c-1].value
        data["temp"+ind4]=g4.temp[c-1].value
        data["sal"+ind4]=g4.sal[c-1].value
      }
      if(g5!=null){
        data["oxd"+ind5]=g5.oxd[c-1].value
        data["oxs"+ind5]=g5.oxs[c-1].value
        data["temp"+ind5]=g5.temp[c-1].value
        data["sal"+ind5]=g5.sal[c-1].value
      }
      if(g6!=null){
        data["oxd"+ind6]=g6.oxd[c-1].value
        data["oxs"+ind6]=g6.oxs[c-1].value
        data["temp"+ind6]=g6.temp[c-1].value
        data["sal"+ind6]=g6.sal[c-1].value
      }
      if(g7!=null){
        data["oxd"+ind7]=g7.oxd[c-1].value
        data["oxs"+ind7]=g7.oxs[c-1].value
        data["temp"+ind7]=g7.temp[c-1].value
        data["sal"+ind7]=g7.sal[c-1].value
      }
    }catch(e){
      console.log(e)
    }
      array1.push(data)
      }
      return array1
    }

    getArray(){
      
        let oxd=this.state.Export1;
        let oxs=this.state.Export2;
        let temp=this.state.Export3;
        let sal=this.state.Export4;
        if(this.state.TagsSelecionado!=null && disabled==false){
          //console.log(oxd)
          visible="block";
          let loc=this.state.TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'))
          let count=loc.length;
          let location=loc;
          let array=[]
          if(oxd.length>0&&oxs.length>0&&temp.length>0&&sal.length>0){
            let global=oxd.concat(oxs).concat(temp).concat(sal).map((g)=>{
              return {type: g.name, value:g.value, dateTime:g.dateTime, ind:g.ind}
            })
            let contador=0;
            location.map((l,i)=>{
              contador=global.filter(g=>g.ind==i&&g.type=='oxd').length;
              if(workplaceId){
                array.push({
                  oxd:global.filter(g=>g.ind==i&&g.type=='oxd'),
                  oxs:global.filter(g=>g.ind==i&&g.type=='temp'),
                  temp:global.filter(g=>g.ind==i&&g.type=='oxs'),
                  sal:global.filter(g=>g.ind==i&&g.type=='sal'),
                })
              }else{
                  array.push({
                    oxd:global.filter(g=>g.ind==i&&g.type=='oxd'),
                    oxs:global.filter(g=>g.ind==i&&g.type=='oxs'),
                    temp:global.filter(g=>g.ind==i&&g.type=='temp'),
                    sal:global.filter(g=>g.ind==i&&g.type=='sal'),
                  })
              }
            })
            //return console.log(contador)
            let array1=[]
            if(contador>0){
              //return console.log(array[0])
              array1.push(this.pushArray(contador,array[0],
                                                  array[1],
                                                  array[2],
                                                  array[3],
                                                  array[4],
                                                  array[5],
                                                  array[6],
                                                  array[7],
                                                  0,1,2,3,4,5,6,7
                                        )
                          )
              return array1[0].reverse()
            }
            //return console.log(array)
            
          }
          
        }else{
          return []
        }
      
      
    }

    showTableExport(){
      if(this.state.MultiExport && this.state.buttonExport=="none"&&load==false){
        return "block"
      }else{
        return "none"
      }
      
    }

    createObj(ind,location_name){
      //console.log(location)
      if(ind==0){
        return[{
            Header: `${location_name}`,
            columns: 
              [
                {
                Header: "FECHA",
                accessor: "x0"
                },
                {
                Header: "oxd",
                accessor: "oxd"+ind,                                                              
                width: 80
                },
                {
                Header: "oxs",
                accessor: "oxs"+ind,                                                              
                width: 80
                },
                {
                Header: "temp",
                accessor: "temp"+ind,                                                              
                width: 80
                },
                {
                Header: "sal",
                accessor: "sal"+ind,                                                              
                width: 80
                }
              ],
        },]
      }else{
        return[{
          Header: `${location_name}`,
          columns: 
            [
              {
              Header: "oxd",
              accessor: "oxd"+ind,                                                              
              width: 80
              },
              {
              Header: "oxs",
              accessor: "oxs"+ind,                                                              
              width: 80
              },
              {
              Header: "temp",
              accessor: "temp"+ind,                                                              
              width: 80
              },
              {
              Header: "sal",
              accessor: "sal"+ind,                                                              
              width: 80
              }
            ],
      },]
      }
      
    }

    getColumns(){
      if(this.state.TagsSelecionado!=null){
        let loc=this.state.TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'))
        let count=loc.length;
        let locations=loc;
        //console.log(locations)
        
        //return this.createObj("0",locations[0])
        // console.log(this.createObj("0",locations[0]))
        // console.log(count)
        let objects=[];
        objects=loc.map((l,index)=>{
          return this.createObj(index.toString(),l.name)[0]
        })
        return objects;
        // console.log(objects)
        // console.log(this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])))
        // if(count==1){
        //   return this.createObj("0",locations[0])
        // }
        // else if(count==2){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1]))
        // }
        // else if(count==3){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])).concat(this.createObj("2",locations[2]))
        // }
        // else if(count==4){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])).concat(this.createObj("2",locations[2])).concat(this.createObj("3",locations[3]))
        // }
        // else if(count==5){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])).concat(this.createObj("2",locations[2])).concat(this.createObj("3",locations[3])).concat(this.createObj("4",locations[4]))
        // }
        // else if(count==6){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])).concat(this.createObj("2",locations[2])).concat(this.createObj("3",locations[3])).concat(this.createObj("4",locations[4]))
        //   .concat(this.createObj("5",locations[5]))
        // }
        // else if(count==7){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])).concat(this.createObj("2",locations[2])).concat(this.createObj("3",locations[3])).concat(this.createObj("4",locations[4]))
        //   .concat(this.createObj("5",locations[5])).concat(this.createObj("6",locations[6]))
        // }
        // else if(count==8){
        //   return this.createObj("0",locations[0]).concat(this.createObj("1",locations[1])).concat(this.createObj("2",locations[2])).concat(this.createObj("3",locations[3])).concat(this.createObj("4",locations[4]))
        //   .concat(this.createObj("5",locations[5])).concat(this.createObj("6",locations[6])).concat(this.createObj("7",locations[7]))
        // }
        
      }else{
        return []
      }
      
    }


    getHeader(){
      if(this.state.TagsSelecionado!=null){
        // console.log("wena")
        let array=[];
        let loc=this.state.TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'));
        let count=loc.length;
        // console.log(count)
        // console.log(loc)
        let locations=loc;
        // console.log(locations)
        let i=0
        try{
          while(i<count){
            if(i==0){
              // array.push({ label: `${this.getNameDispositivo()}`, key: "NOMBRE" })
              array.push({ label: "FECHA", key: "x0" })
            }
            // array.push({ label: `${locations[i].name}`, key: "NOMBRE" })
            array.push({ label: `${locations[i].name} O2 Disuelto (mg/L)` , key: "oxd"+i })
            array.push({ label: `${locations[i].name} Temp (°C)`, key: "temp"+i })
            array.push({ label: `${locations[i].name} O2 SAT ( %)`, key: "oxs"+i })
            array.push({ label: `${locations[i].name} Sal (PSU)`, key: "sal"+i })
            i++
          }
        }catch(e){
          console.log("e")
        }
        
        
        return array  
      }else{
        return []
      }
    }
 
    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
         
 
         let optionsChart1 = {

            
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                {title:"mg/L - °C"},
             axisY2 :{
                title:"% - PSU"
            },   
            
             }
         
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Tendencias</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Datos Historicos</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                <Row>
                                        <Col   md={12} lg={3}> 
                                            {this.renderPickerAutoApply(ranges, local, maxDate)}   
                                        </Col>
                                        <Col md={12}  lg={6}>
                                            {this.renderSelecTag(this.state.Tags)}
                                        </Col>
                                        
                                        <Col   md={12} lg={3}> 
                                            <ButtonGroup>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(31)
                                              })}>- 31 </Button>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(7)
                                              })}>- 7 </Button>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(1)
                                              })}>- 1 </Button>
                                            </ButtonGroup>
                                            <ButtonGroup style={{marginLeft:20}}>
                                                <Button color="primary"
                                                        outline
                                                        disabled={disabled}
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                          if(this.state.TagsSelecionado != null){
                                                          disabled=true;
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                            
                                                             this.filtrar2();
                                                             }}
                                                          }
                                                >Filtrar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                              



                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                        <Card className="main-card mb-5 mt-3" >
                        <CardHeader className="card-header-tab  ">
                          <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                              {this.getNameDispositivo()}
                          </div>
                          <div className="btn-actions-pane-right text-capitalize" style={{display:visible}}>
                              <CSVLink                                         
                                separator={";"}                                                                   
                                headers={
                                  this.getHeader()                                      
                                }
                                data={this.getArray()==null?[]:this.getArray()}
                                filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
                                className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                target="_blank">
                                Exportar Datos
                              </CSVLink>
                          </div>                                                  
                        </CardHeader>
                        <CardBody className="p-10 m-10" >
                        <BlockUi tag="div" blocking={this.state.blocking} 
                                    loader={<Loader active type={this.state.loaderType}/>}>   
                          <ReactTable                                                           
                          data={this.getArray()}
                      
                          // loading= {false}
                          showPagination= {true}
                          showPaginationTop= {false}
                          showPaginationBottom= {true}
                          showPageSizeOptions= {false}
                          pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                          defaultPageSize={10}
                          columns={this.getColumns()}
                          
                          className="-striped -highlight"
                          />
                        </BlockUi>
                      </CardBody>
                      </Card>

                        </Col>

                    </Row>
                    {/* <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                    <Spinner color="primary" style={{ marginTop:`${5}%`,marginLeft:`${50}%`,display:load?"block":"none", width: '2rem', height: '2rem'}}/>
                            <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block"}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%`}}
                                onClick={() => {
                                        load=true;
                                        this.filtrar2();
                                        // console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button>
                    </Col>
                    <Col sm="4"></Col>
                    
                    
                    {/* {console.time("loop 3")} */}
                    
                    {/* {
                                                          
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="3" key={data.sonda} style={{display:`${this.state.MultiExport?"none":"block"}`}}>
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        <CSVLink
                                                                        
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: data.unity , key: "y" }
                                                                                
                                                                            ]
                                                                        
                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                        </CSVLink>
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={data.measurements}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: data.unity,
                                                                    accessor: "y",                                                              
                                                                    width: 140
                                                                    }
                                                                    // {
                                                                    //   Header: "Temp ("+ data.unity2+ ")",
                                                                    //   accessor: "temp",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "O2 SAT ("+ data.unity3+ ")",
                                                                    //   accessor: "oxs",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "Sal ("+ data.unity4+ ")",
                                                                    //   accessor: "sal",                                                              
                                                                    //   width: 140
                                                                    // }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         
                         )
        
                            )
                            
                    }
                    
                                       

                  </Row> */}


                 
             
            
                                 

          

                    

               


                                                                 
                                                    
   

            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Datos_Historicos);
