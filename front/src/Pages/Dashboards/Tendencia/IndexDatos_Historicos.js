import React, {Component, Fragment} from 'react';
 import Datos_Historicos from './ComponentTendencia/Tendencia/Datos_Historicos';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {checkStatusLogin} from '../../../services/user';
import { Redirect, Route } from 'react-router-dom';



export default class IndexDatos_Historicos extends Component {
    render() {
        const isLoggedIn = checkStatusLogin();
        return (
            <Fragment>
                <Route 
                    render={() => 
                        isLoggedIn ? ( 
                            <ReactCSSTransitionGroup
                                component="div"
                                transitionName="TabsAnimation"
                                transitionAppear={true}
                                transitionAppearTimeout={0}
                                transitionEnter={false}
                                transitionLeave={false}>
                                <div className="app-inner-layout">                      
                                    <Datos_Historicos/>              
                                </div>
                            </ReactCSSTransitionGroup>
                                ) : (
                                    <Redirect to="/pages/login"></Redirect>
                                )
                        }      
                
                /> 
            </Fragment>
        )
    }
}