import React, {Component, Fragment} from 'react';

 import cx from 'classnames';
import {Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    CardFooter,
    Badge,ButtonGroup} from 'reactstrap';
import Loader from 'react-loaders';
import axios from 'axios';
import moment from 'moment';
import {BrowserRouter as Router,Switch,Route,Link, useRouteMatch} from "react-router-dom";
import ReactDOM from 'react-dom';
import {Redirect} from 'react-router';
import { API_ROOT3 } from '../../../../../api-config';
// import IndexNave from '../../../Naves/IndexNave';
import BlockUi from 'react-block-ui';
import {
    toast

} from 'react-toastify';

//sala Broodstock
const Nave100 = "5d7800be4d43af2b90afd26d";
const Nave200 = "5d7a6234b123470168990aa1";
const Nave300 = "5d7a625bb123470168990aa2";
const Nave400 = "5d7a6267b123470168990aa3";
const Nave500 = "5d7a6279b123470168990aa4";
const Nave600 = "5d7a6282b123470168990aa5";
const Nave700 = "5d7a628fb123470168990aa6";
const Nave800 = "5d94aa611c064a2120e19058";

const tag101="5d7801514d43af2b90afd26e";

const myContext = React.createContext();

let refrescador = 1;

let choicetk = '0';

let numeroToggle = '0';


let habilitado=false;

let sw=false;
let c_sw=false;
let c_sw2=false;
let c_sw3=false;
let visible="block";
let check=false;
let modificacion_general=false;

class PanelGeneralIgnao extends Component {  
    constructor(props) {
        super(props);
        this.state = {         
            myDataTKN100: [],     
            myDataTKN200: [],
            myDataTKN300: [],
            myDataTKN400: [],
            myDataTKN500: [],
            myDataTKN600: [],
            myDataTKN700: [],
            myDataTKN800: [],
            myTag: [],
            reload: 1,
            error: null,
            n_naves:["01","02","03","04","05","06","07","08"],
            Ox:true,
            Temp:false,
            Sat:false,
            dateTimeRefresh:"",
            choice: "0",
            nCard:[1,3,5,7,9,11,2,4,6,8,10,12],
            mySalaModal:0,
            modal1: false,
            setpointOx:1,
            setpointOxs:[],         
            minOx:3,
            minOxs:[],
            maxOx:0,
            maxOxs:[],
            timeOn:5,
            timeOns:[],
            timeOff:0,
            timeOffs:[],
            alarmTemp:7,
            histeresisOx:0,
            histeresisOxs:[],
            estadoTK:[],
            habilitadoTK:[],
            OxdTK:[],

            activoTemp: true,
            activoHum: true,
            blocking1: false,  
            intentosGuardado: 0,
        }
        //this.checkTkChoice = this.checkTkChoice.bind(this);
        this.loadDataTag1 = this.loadDataTag1.bind(this);
        this.getAlarma=this.getAlarma.bind(this);
        //this.refreshPage = this.refreshPage.bind(this);
        this.updateTagState = this.updateTagState.bind(this);
        this.getButtonOx = this.getButtonOx.bind(this);
        this.getButtonTag = this.getButtonTag.bind(this);
        this.getButtonTagCabecera = this.getButtonTagCabecera.bind(this);
        this.getdateLastTime = this.getdateLastTime.bind(this);
        this.toggleModal1 = this.toggleModal1.bind(this);

    }
    
    
    componentDidMount = () => {
        this.setState({ isLoading: true });    
        this.loadDataTag1();
        const intervaloRefresco = 300000;
        this.intervalIdTag1 = setInterval(() => this.loadDataTag1(),intervaloRefresco);
        let url = window.location.host;
        //let u=url.split('/');
        console.log(API_ROOT3)
        const hostname = window && window.location && window.location.hostname;
        if(hostname === 'ihc.idealcontrol.cl'){
            habilitado = true;
        }
      }

    getdateLastTime=(token,f1,f2)=>{
        //, dateTimeRefresh:myDataTKN100.lastData
        //alert(`${API_ROOT3}/measurement/xy/tag/5d87f973a5f89831dc75b24c/${f1}/${f2}`)
        axios
        .get(`${API_ROOT3}/measurement/xy/tag/5d87f973a5f89831dc75b24c/${f1}/${f2}`, {
             'Authorization': 'Bearer ' + token
        })
        .then(response => {
            const measurements = response.data.data;
            this.setState({ dateTimeRefresh:measurements[measurements.length-1].x});
        })
        .catch(error => {
          console.log(error);
        });
    }

    updateTagState=(id,actual_state)=>{
        const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };

       let EndPointTag = `${API_ROOT3}/tag/${id}`; 
        const  content1 = {
            "state": actual_state
        }   
        axios.put(EndPointTag, content1, config)
        .then(response => {
          if(response.status==200){
            this.loadDataTag1();
          }
          console.log("actualizado " + response.status);
          
        });
    }
  
    componentWillUnmount = () => { 
          clearInterval(this.intervalIdTag1);  
    }

    loadDataTag1 = () =>  {  
        var date = moment().subtract(1, 'hours');
        var minute = date.minutes();
        //alert(date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm'))

        let now = new Date(); 
        const f1 = date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
        const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";    
      
        const API1 = `${API_ROOT3}/tag/zone/${Nave100}/${f1}/${f2}`;
        const API2 = `${API_ROOT3}/tag/zone/${Nave200}/${f1}/${f2}`;
        const API3 = `${API_ROOT3}/tag/zone/${Nave300}/${f1}/${f2}`;
        const API4 = `${API_ROOT3}/tag/zone/${Nave400}/${f1}/${f2}`;
        const API5 = `${API_ROOT3}/tag/zone/${Nave500}/${f1}/${f2}`;
        const API6 = `${API_ROOT3}/tag/zone/${Nave600}/${f1}/${f2}`;
        const API7 = `${API_ROOT3}/tag/zone/${Nave700}/${f1}/${f2}`;
        const API8 = `${API_ROOT3}/tag/zone/${Nave800}/${f1}/${f2}`;
        const APITag = `${API_ROOT3}/tag`;

       //const token = localStorage.getItem("token"); 
       const token = "tokenFalso";

       axios
       .get(APITag, {
         headers: {  
           'Authorization': 'Bearer ' + token
         }
       })
       .then(response => {

           let myTag = response.data.data;
           console.log(myTag)

          this.setState({ myTag,isLoading: false });   
       })
       .catch(error => {
         console.log(error);
       });


        this.getdateLastTime(token,f1,f2);

    //Sala Smolt
    //Sala Alevines 
        

    }

    
    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });   
        
        
        //const EndPointTagofLocation =`${API_ROOT3}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT3}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            console.log(response);
            console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );      
            if (TagWrites.length === 0){
                clearInterval(this.intervalConfirmacion);
                this.setState({blocking1: false});
                this.componentDidMount();
                 this.loadDataTag();
                 this.loadDataChar();
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
                //window.location.reload(false);
            }else if (this.state.intentosGuardado >= 10)
            {
                  this.componentDidMount();
                  //this.loadDataTag1();
                  //this.loadDataChar();
                  toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
                  this.setState({blocking1: false}); 
                  clearInterval(this.intervalConfirmacion);
                  //window.location.reload(false);
            }
        })
        .catch(error => {
          console.log(error);
        });

    }


    updateAllConfig=(content,config,include)=>{
        let tags_capture="";
        tags_capture=this.state.myTag.filter((data)=>{
            return String(data.shortName).includes(include)
        })
            .map((data)=>{
                let EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                axios.put(EndPointTag, content, config)
                .then(response => {
                });
                return data
            })
    }

    updateAllConfigAlert=(content,config,include,equals)=>{
        let tags_capture="";
        tags_capture=this.state.myTag.filter((data)=>{
            if(String(data.shortName).includes(include)){
                return data.address==equals
            }
        })
            .map((data)=>{
                let EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                axios.put(EndPointTag, content, config)
                .then(response => {
                });
                return data
            })
    }

    updateTagControl=(address,data,config,EndPointTag,content9,content10,content11)=>{
        //Automatico
        if("M63"+address[1]+address[2]+"TK"+address==data.nameAddress){
            EndPointTag = `${API_ROOT3}/tag/${data._id}`;
            axios.put(EndPointTag, content9, config)
            .then(response => {
            });
        }
        //Manual on
        else if("M61"+address[1]+address[2]+"TK"+address==data.nameAddress){
            EndPointTag = `${API_ROOT3}/tag/${data._id}`;
            axios.put(EndPointTag, content10, config)
            .then(response => {
            });
        }
        //Manual off
        else if("M62"+address[1]+address[2]+"TK"+address==data.nameAddress){
            EndPointTag = `${API_ROOT3}/tag/${data._id}`;
            axios.put(EndPointTag, content11, config)
            .then(response => {
            });
        }
    }

    almacenarConfig = () => {
        console.log("mod_gen: "+modificacion_general)
        console.log(sw);
        this.setState({blocking1: true,intentosGuardado:0});

       this.intervalConfirmacion = setInterval(() => this.confirmacionTermino(),3000);
       
       const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };

       let EndPointTag="";
       const  content1 = {
            "lastValue": this.state.histeresisOx,
            "lastValueWrite":this.state.histeresisOx,
            "write":true
        }  
        const  content2 = {
            "lastValue": this.state.setpointOx,
            "lastValueWrite":this.state.setpointOx,
            "write":true
        } 
        const  content3 = {
            "lastValue": this.state.maxOx,
            "lastValueWrite":this.state.maxOx,
            "write":true
        } 
        const  content4 = {
            "lastValue": this.state.minOx,
            "lastValueWrite":this.state.minOx,
            "write":true
        }
        const  content4_5 = {
            "alertMax": this.state.maxOx,
            "alertMin":this.state.minOx,
            "write":true
        }
        const  content5 = {
            "lastValue": this.state.timeOff,
            "lastValueWrite":this.state.timeOff,
            "write":true
        }  
        const  content6 = {
            "lastValue": this.state.timeOn,
            "lastValueWrite":this.state.timeOn,
            "write":true
        }

       if(modificacion_general){
            this.loadDataTag1();
            console.log("modificacion All")
            
            console.log(this.state.myTag)
            let tags_capture="";
            let cont=0;
            const get_address=this.state.habilitadoTK[0].nameAddress.split('TK');
            let address=get_address[1];
            console.log(address[0]);
            while(cont<=12){
                if(cont<10){
                    this.updateAllConfig(content1, config, 'Histeresis Ox TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content2, config, 'Set Point  Ox TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content3, config, 'Max Ox TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content4, config, 'Min Ox  TK'+address[0]+'0'+cont);

                    let address_num=cont*100;
                    this.updateAllConfigAlert(content4_5, config,'Ox TK'+address[0]+'0'+cont, address_num);

                    this.updateAllConfig(content5, config, 'Time Off Val TK'+address[0]+'0'+cont);
                    this.updateAllConfig(content6, config, 'Time On Val TK'+address[0]+'0'+cont);
                }else{
                    this.updateAllConfig(content1, config, 'Histeresis Ox TK'+address[0]+cont);
                    this.updateAllConfig(content2, config, 'Set Point  Ox TK'+address[0]+cont);
                    this.updateAllConfig(content3, config, 'Max Ox TK'+address[0]+cont);
                    this.updateAllConfig(content4, config, 'Min Ox  TK'+address[0]+cont);

                    let address_num=cont*100;
                    this.updateAllConfigAlert(content4_5, config,'Ox TK'+address[0]+cont, address_num);

                    this.updateAllConfig(content5, config, 'Time Off Val TK'+address[0]+cont);
                    this.updateAllConfig(content6, config, 'Time On Val TK'+address[0]+cont);
                }
                cont++
            }
       }     
       else{
        if(visible!="none"){
            console.log("i "+this.state.histeresisOxs[0]._id);
            EndPointTag = `${API_ROOT3}/tag/${this.state.histeresisOxs[0]._id}`; 

            
            axios.put(EndPointTag, content1, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT3}/tag/${this.state.setpointOxs[0]._id}`; 
            
            axios.put(EndPointTag, content2, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT3}/tag/${this.state.maxOxs[0]._id}`; 
            
            axios.put(EndPointTag, content3, config)
            .then(response => {
                
            });

            EndPointTag = `${API_ROOT3}/tag/${this.state.minOxs[0]._id}`; 
            
            axios.put(EndPointTag, content4, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT3}/tag/${this.state.OxdTK[0]._id}`; 
            
            axios.put(EndPointTag, content4_5, config)
            .then(response => {
                
            });

            EndPointTag = `${API_ROOT3}/tag/${this.state.timeOffs[0]._id}`; 
            
            axios.put(EndPointTag, content5, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });

            EndPointTag = `${API_ROOT3}/tag/${this.state.timeOns[0]._id}`; 
            
            axios.put(EndPointTag, content6, config)
            .then(response => {
            //  console.log("actualizado " + response.status);
            });
        }
        
        EndPointTag = `${API_ROOT3}/tag/${this.state.habilitadoTK[0]._id}`; 
        let content7="";
        if(sw==true){
            content7 = {
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true            
            }
        }else{
            content7 = {
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true            
            }
        }

        axios.put(EndPointTag, content7, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });
        

        const get_address=this.state.habilitadoTK[0].nameAddress.split('TK');
        let address=get_address[1];
        let content8="";//habilitado
        if(sw){
            content8={
               "state":1
           }
       }else{
            content8={
               "state":-1
           }
       }
        let content9="";//automatico
        if(c_sw){
             content9={
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true
            }
        }else{
             content9={
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true
            }
        }
        let content10="";//manual on
        if(c_sw2){
             content10={
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true
            }
        }else{
             content10={
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true
            }
        }
        let content11="";//manual off
        if(c_sw3){
             content11={
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true
            }
        }else{
             content11={
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true
            }
        }
        console.log("mi address "+address);
        let tags_capture="";

            if(address==address[0]+"01N"+address[0]){

                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                })
            }
            else if(address==address[0]+"02N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"03N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"04N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    //  console.log("actualizado " + response.status);
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"05N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"06N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"07N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"08N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"09N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"10N"+address[0]){
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            }
            else if(address==address[0]+"11N"+address[0])
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            else if(address==address[0]+"12N"+address[0])
                tags_capture=this.state.myTag.filter((tag)=>{
                    let name_addr=String(tag.nameAddress);
                    return name_addr.includes(address);
                })
                .map((data)=>{
                    EndPointTag = `${API_ROOT3}/tag/${data._id}`;
                    axios.put(EndPointTag, content8, config)
                    .then(response => {
                    });
                    this.updateTagControl(address,data,config,EndPointTag,content9,content10,content11);
                    return data
                })
            else
                console.log("Rayos")
        
    }
        // this.componentDidMount();
        this.loadDataTag1();
 
    }

    toggleModal1(sala) {

        visible="block";
        
        if (this.state.modal1 === false){ 
        
            
                let sa = "";
                let num="";

                switch (sala) {  
                    case "101":
                        sa = "101";
                        num="01";
                    break;
                    case "102":
                        sa = "102";
                        num="02";
                    break;
                    case "103":
                        sa = "103";
                        num="03";                  
                    break;
                    case "104":
                        sa = "104";
                        num="04";                   
                    break;
                    case "105":
                        sa = "105";
                        num="05";                  
                    break;
                    case "106":
                        sa = "106";
                        num="06";                   
                    break;
                    case "107":
                        sa = "107";
                        num="07";                 
                    break;
                    case "108":
                        sa = "108";
                        num="08";                    
                    break;
                    case "109":
                        sa = "109";
                        num="09";              
                    break;
                    case "110":
                        sa = "110";
                        num="10";                    
                    break;
                    case "111":
                        sa = "111";
                        num="11";                  
                    break;
                    case "112":
                        sa = "112";
                        num="12";                    
                    break;
                    case "201":
                        sa = "201";
                        num="01";
                    break;
                    case "202":
                        sa = "202";
                        num="02";
                    break;
                    case "203":
                        sa = "203";
                        num="03";                  
                    break;
                    case "204":
                        sa = "204";
                        num="04";                   
                    break;
                    case "205":
                        sa = "205";
                        num="05";                  
                    break;
                    case "206":
                        sa = "206";
                        num="06";                   
                    break;
                    case "207":
                        sa = "207";
                        num="07";                 
                    break;
                    case "208":
                        sa = "208";
                        num="08";                    
                    break;
                    case "209":
                        sa = "209";
                        num="09";              
                    break;
                    case "210":
                        sa = "210";
                        num="10";                    
                    break;
                    case "211":
                        sa = "211";
                        num="11";                  
                    break;
                    case "212":
                        sa = "212";
                        num="12";                    
                    break;
                    case "301":
                        sa = "301";
                        num="01";
                    break;
                    case "302":
                        sa = "302";
                        num="02";
                    break;
                    case "303":
                        sa = "303";
                        num="03";                  
                    break;
                    case "304":
                        sa = "304";
                        num="04";                   
                    break;
                    case "305":
                        sa = "305";
                        num="05";                  
                    break;
                    case "306":
                        sa = "306";
                        num="06";                   
                    break;
                    case "307":
                        sa = "307";
                        num="07";                 
                    break;
                    case "308":
                        sa = "308";
                        num="08";                    
                    break;
                    case "309":
                        sa = "309";
                        num="09";              
                    break;
                    case "310":
                        sa = "310";
                        num="10";                    
                    break;
                    case "311":
                        sa = "311";
                        num="11";                  
                    break;
                    case "312":
                        sa = "312";
                        num="12";                    
                    break;
                    case "401":
                        sa = "401";
                        num="01";
                    break;
                    case "402":
                        sa = "402";
                        num="02";
                    break;
                    case "403":
                        sa = "403";
                        num="03";                  
                    break;
                    case "404":
                        sa = "404";
                        num="04";                   
                    break;
                    case "405":
                        sa = "405";
                        num="05";                  
                    break;
                    case "406":
                        sa = "406";
                        num="06";                   
                    break;
                    case "407":
                        sa = "407";
                        num="07";                 
                    break;
                    case "408":
                        sa = "408";
                        num="08";                    
                    break;
                    case "409":
                        sa = "409";
                        num="09";              
                    break;
                    case "410":
                        sa = "410";
                        num="10";                    
                    break;
                    case "411":
                        sa = "411";
                        num="11";                  
                    break;
                    case "412":
                        sa = "412";
                        num="12";                    
                    break;
                    case "501":
                        sa = "501";
                        num="01";
                    break;
                    case "502":
                        sa = "502";
                        num="02";
                    break;
                    case "503":
                        sa = "503";
                        num="03";                  
                    break;
                    case "504":
                        sa = "504";
                        num="04";                   
                    break;
                    case "505":
                        sa = "505";
                        num="05";                  
                    break;
                    case "506":
                        sa = "506";
                        num="06";                   
                    break;
                    case "507":
                        sa = "507";
                        num="07";                 
                    break;
                    case "508":
                        sa = "508";
                        num="08";                    
                    break;
                    case "509":
                        sa = "509";
                        num="09";              
                    break;
                    case "510":
                        sa = "510";
                        num="10";                    
                    break;
                    case "511":
                        sa = "511";
                        num="11";                  
                    break;
                    case "512":
                        sa = "512";
                        num="12";                    
                    break;
                    case "601":
                        sa = "601";
                        num="01";
                    break;
                    case "602":
                        sa = "602";
                        num="02";
                    break;
                    case "603":
                        sa = "603";
                        num="03";                  
                    break;
                    case "604":
                        sa = "604";
                        num="04";                   
                    break;
                    case "605":
                        sa = "605";
                        num="05";                  
                    break;
                    case "606":
                        sa = "606";
                        num="06";                   
                    break;
                    case "607":
                        sa = "607";
                        num="07";                 
                    break;
                    case "608":
                        sa = "608";
                        num="08";                    
                    break;
                    case "609":
                        sa = "609";
                        num="09";              
                    break;
                    case "610":
                        sa = "610";
                        num="10";                    
                    break;
                    case "611":
                        sa = "611";
                        num="11";                  
                    break;
                    case "612":
                        sa = "612";
                        num="12";                    
                    break;
                    case "701":
                        sa = "701";
                        num="01";
                    break;
                    case "702":
                        sa = "702";
                        num="02";
                    break;
                    case "703":
                        sa = "703";
                        num="03";                  
                    break;
                    case "704":
                        sa = "704";
                        num="04";                   
                    break;
                    case "705":
                        sa = "705";
                        num="05";                  
                    break;
                    case "706":
                        sa = "706";
                        num="06";                   
                    break;
                    case "707":
                        sa = "707";
                        num="07";                 
                    break;
                    case "708":
                        sa = "708";
                        num="08";                    
                    break;
                    case "709":
                        sa = "709";
                        num="09";              
                    break;
                    case "710":
                        sa = "710";
                        num="10";                    
                    break;
                    case "711":
                        sa = "711";
                        num="11";                  
                    break;
                    case "712":
                        sa = "712";
                        num="12";                    
                    break;

                    default:
                        sa = "No definido";
                    break;

                }   
                this.setState({
                    mySalaModal:sa
                });

                let mysala = "TK" + sa;   

                const { myTag} = this.state;
                
                // const tags=this.getDataModal(myDataChart1,myDataChart2,myDataChart3,myDataChart4,myDataChart5,myDataChart6,myDataChart7,myDataChart8,myDataChart9,myDataChart10
                //     ,myDataChart11,myDataChart12,sa);

                    try{
                        console.log(myTag.filter((tag) => tag.nameAddress === `M6101TK101N1`)[0].lastValue==1)
                        //alert(sa[0]);
                        if(myTag.filter((tag) => tag.nameAddress === `M63${num}${mysala}N${sa[0]}`)[0].lastValue==1){
                            c_sw=true;
                        }else{
                            c_sw=false;
                        }
                        if(myTag.filter((tag) => tag.nameAddress === `M61${num}${mysala}N${sa[0]}`)[0].lastValue==1){
                            c_sw2=true;
                        }else{
                            c_sw2=false;
                        }
                        if(myTag.filter((tag) => tag.nameAddress === `M62${num}${mysala}N${sa[0]}`)[0].lastValue==1){
                            c_sw3=true;
                        }else{
                            c_sw3=false;
                        }
                    }catch(e){
                        console.log(e)
                    }

                        try{
                            if(myTag.filter((tag) => tag.nameAddress === `M60${num}${mysala}N${sa[0]}`)[0].lastValue==1){
                                sw=true;
                            }else{
                                sw=false;
                            }
                        }catch(e){
                            console.log(e)
                            visible="none";
                            sw=false;
                        }
                        let num2=0;
                        num>9?num2=num:num2=num[1];
                        //console.log(`M60${num}${mysala}N1`);
                        
                        try{
                            console.log("1"+myTag.filter((tag) => tag.nameAddress === `M60${num}${mysala}N${sa[0]}`))
                            console.log("2"+myTag.filter((tag) => tag.nameAddress === `D2${num}05TK${sa}N${sa[0]}`)[0].lastValue)
                            if(sa!="No definido"){
                                this.setState({
                                    histeresisOxs : myTag.filter((tag) => tag.nameAddress === `D2${num}05TK${sa}N${sa[0]}`),
                                    setpointOxs: myTag.filter((tag) => tag.nameAddress === `D2${num}04TK${sa}N${sa[0]}`),
                                    maxOxs : myTag.filter((tag) => tag.nameAddress === `D2${num}06TK${sa}N${sa[0]}`),
                                    minOxs: myTag.filter((tag) => tag.nameAddress === `D2${num}07TK${sa}N${sa[0]}`), 
                                    timeOffs : myTag.filter((tag) => tag.nameAddress === `D2${num}09TK${sa}N${sa[0]}`),  
                                    timeOns: myTag.filter((tag) => tag.nameAddress === `D2${num}08TK${sa}N${sa[0]}`),
                                    habilitadoTK: myTag.filter((tag) => tag.nameAddress === `M60${num}${mysala}N${sa[0]}`),      
                                    
                                    histeresisOx : myTag.filter((tag) => tag.nameAddress === `D2${num}05TK${sa}N${sa[0]}`)[0].lastValue,
                                    setpointOx: myTag.filter((tag) => tag.nameAddress === `D2${num}04TK${sa}N${sa[0]}`)[0].lastValue,
                                    maxOx : myTag.filter((tag) => tag.nameAddress === `D2${num}06TK${sa}N${sa[0]}`)[0].lastValue,
                                    minOx: myTag.filter((tag) => tag.nameAddress === `D2${num}07TK${sa}N${sa[0]}`)[0].lastValue,
                                    timeOff : myTag.filter((tag) => tag.nameAddress === `D2${num}09TK${sa}N${sa[0]}`)[0].lastValue,
                                    timeOn: myTag.filter((tag) => tag.nameAddress === `D2${num}08TK${sa}N${sa[0]}`)[0].lastValue,
                                    estadoTK: myTag.filter((tag) => tag.nameAddress === `M60${num}${mysala}N${sa[0]}`)[0].lastValue,
                                    
                                    OxdTK: myTag.filter((tag) => tag.nameAddress === `D${num2}00TK${sa}N${sa[0]}`),
                                    
                                    modal1: !this.state.modal1,
                                    misTag: myTag
                                });
                            }
                        }catch(e){
                            console.log(e);
                            visible="none";

                            const EndPointTag = `${API_ROOT3}/tag`;
                            const token = "tokenfalso";
                            axios
                            .get(EndPointTag, {headers: {'Authorization': 'Bearer ' + token}})
                            .then(response => {
                                const tags = response.data.data;
                                console.log(tags.filter((tag) => tag.nameAddress === `D${num2}00TK${sa}N${sa[0]}`));
                                this.setState({
                                    habilitadoTK: tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N${sa[0]}`),  

                                    OxdTK: tags.filter((tag) => tag.nameAddress === `D${num2}00TK${sa}N${sa[0]}`),
                                    
                                    modal1: !this.state.modal1,

                                    misTag: tags
                                })
                            })
                            .catch(error => {
                            console.log(error);
                            });

                        }
                   
                    
                    
            }else{
                this.setState({
                    modal1: !this.state.modal1
                });

            }
    }

    getManualAuto=(tag)=>{
        var tkn=tag.split('TK');
        var n=tkn[1];
        let tags_capture="";
        return tags_capture=this.state.myTag
        .map((data)=>{
            let name_addr=String(data.nameAddress);
            if(name_addr.includes(`M63${n[1]}${n[2]}TK${tkn[1]}`)){
                if(data.lastValue==1){
                    return <span className="badge badge-pill badge-success">A</span>
                }
            }
            else if(name_addr.includes(`M61${n[1]}${n[2]}TK${tkn[1]}`)){
                if(data.lastValue==1)
                    return <span className="badge badge-pill badge-warning">ON</span>
            }
            else if(name_addr.includes(`M62${n[1]}${n[2]}TK${tkn[1]}`)){
                if(data.lastValue==1)
                    return <span className="badge badge-pill badge-secondary">OFF</span>
            }
        })
        
    }
    // getManualOn=(tag)=>{
    //     var tkn=tag.split('TK');
    //     var n=tkn[1];
    //     let tags_capture="";
    //     return tags_capture=this.state.myTag.filter((tag)=>{
    //         let name_addr=String(tag.nameAddress);
    //         return name_addr.includes(`M61${n[1]}${n[2]}TK${tkn[1]}`);
    //     })
    //     .map((data)=>{
    //         if(data.lastValue==1)
    //             return <span className="badge badge-pill badge-warning">ON</span>
    //     })

    // }
    // getManualOff=(tag)=>{
    //     var tkn=tag.split('TK');
    //     var n=tkn[1];
    //     let tags_capture="";
    //     return tags_capture=this.state.myTag.filter((tag)=>{
    //         let name_addr=String(tag.nameAddress);
    //         return name_addr.includes(`M62${n[1]}${n[2]}TK${tkn[1]}`);
    //     })
    //     .map((data)=>{
    //         if(data.lastValue==1)
    //             return <span className="badge badge-pill badge-secondary">OFF</span>
    //     })

    // }

    getButtonOx=(data, num, n)=>{
        //alert(data.nameAddress)
        let textInput = React.createRef();
        return (
        
        <Button id={data.id}

            
            className=" mr-0 mb-1 ml-1  pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast"
            style={{marginTop:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}} onClick={((e)=>{

                    this.getButtonOx(data,num,n);
                    let split=data.nameAddress.split('K');
                    let n_tks=split[1].split('N');
                    //alert(n_tks[0]);
                    this.render();
                    this.toggleModal1(n_tks[0]);
                    //this.renderizarToggle(tick);
                    //this.refreshPage();
   
            })}
            >     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                        <span className="badge badge-light m-0 ml-0 pl-1 w-100 " style={{height:28}}>
                        <font size="2">{data.state!=1?"-":data.lastValue}</font>
                        
                        {this.getManualAuto(data.nameAddress)}
                        {/* {this.getManualOn(data.nameAddress)} */}
                        {/* {this.getManualOff(data.nameAddress)} */}
                        {/* <span className="badge badge-pill badge-success">A</span> */}
                        {/* <span className="opacity-6  pl-0 size_unidad2">  
                        225 </span>  */}
                        </span>

                    {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1){
                            if(data.address=="100" || data.address=="200" || data.address=="300" || data.address=="400" ||
                             data.address=="500" || data.address=="600" || data.address=="700" || data.address=="800" ||
                             data.address=="900" || data.address=="1000" || data.address=="1100" || data.address=="1200"){
                                 //console.log(data.name+" "+data.alertMin+" max: "+data.alertMax);
                                if(data.lastValue>data.alertMin && data.lastValue<data.alertMax){
                                    //return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                                    return <span  className={cx("badge badge-dot badge-dot-lg badge-success")}> </span>
                                }else{
                                    return <span className={cx("badge badge-dot badge-dot-lg badge-danger")}> </span>
                                }
                            }
                            
                        }
                        if(data.state==-1){
                            // return <span className={cx("badge badge-dot badge-dot-lg badge-secondary")}> </span>
                        }
                        
                     })} 
                </Button>
                
                //</Link>
                
        )
    }

    getButtonTag=(array,n,num)=>{
        
        if(this.state.Ox)
        return array.filter(()=>n==num).map((data)=>{
            //console.log(n);
            // let split=data.nameAddress.split('K');
            // let n_tks=split[1].split('N');
            // console.log(n_tks[0]);
            if(data.nameAddress=="D100TK"+num+"01N"+num || data.nameAddress=="D100TK"+num+"02N"+num || data.nameAddress=="D100TK"+num+"03N"+num ||
            data.nameAddress=="D100TK"+num+"04N"+num || data.nameAddress=="D100TK"+num+"05N"+num || data.nameAddress=="D100TK"+num+"06N"+num ||
            data.nameAddress=="D100TK"+num+"07N"+num || data.nameAddress=="D100TK"+num+"08N"+num || data.nameAddress=="D100TK"+num+"09N"+num ||
            data.nameAddress=="D100TK"+num+"10N"+num || data.nameAddress=="D100TK"+num+"11N"+num || data.nameAddress=="D100TK"+num+"12N"+num ||
            
            data.nameAddress=="D200TK"+num+"01N"+num || data.nameAddress=="D200TK"+num+"02N"+num || data.nameAddress=="D200TK"+num+"03N"+num ||
            data.nameAddress=="D200TK"+num+"04N"+num || data.nameAddress=="D200TK"+num+"05N"+num || data.nameAddress=="D200TK"+num+"06N"+num ||
            data.nameAddress=="D200TK"+num+"07N"+num || data.nameAddress=="D200TK"+num+"08N"+num || data.nameAddress=="D200TK"+num+"09N"+num ||
            data.nameAddress=="D200TK"+num+"10N"+num || data.nameAddress=="D200TK"+num+"11N"+num || data.nameAddress=="D200TK"+num+"12N"+num ||
            
            data.nameAddress=="D300TK"+num+"01N"+num || data.nameAddress=="D300TK"+num+"02N"+num || data.nameAddress=="D300TK"+num+"03N"+num ||
            data.nameAddress=="D300TK"+num+"04N"+num || data.nameAddress=="D300TK"+num+"05N"+num || data.nameAddress=="D300TK"+num+"06N"+num ||
            data.nameAddress=="D300TK"+num+"07N"+num || data.nameAddress=="D300TK"+num+"08N"+num || data.nameAddress=="D300TK"+num+"09N"+num ||
            data.nameAddress=="D300TK"+num+"10N"+num || data.nameAddress=="D300TK"+num+"11N"+num || data.nameAddress=="D300TK"+num+"12N"+num ||
            
            data.nameAddress=="D400TK"+num+"01N"+num || data.nameAddress=="D400TK"+num+"02N"+num || data.nameAddress=="D400TK"+num+"03N"+num ||
            data.nameAddress=="D400TK"+num+"04N"+num || data.nameAddress=="D400TK"+num+"05N"+num || data.nameAddress=="D400TK"+num+"06N"+num ||
            data.nameAddress=="D400TK"+num+"07N"+num || data.nameAddress=="D400TK"+num+"08N"+num || data.nameAddress=="D400TK"+num+"09N"+num ||
            data.nameAddress=="D400TK"+num+"10N"+num || data.nameAddress=="D400TK"+num+"11N"+num || data.nameAddress=="D400TK"+num+"12N"+num ||
            
            data.nameAddress=="D500TK"+num+"01N"+num || data.nameAddress=="D500TK"+num+"02N"+num || data.nameAddress=="D500TK"+num+"03N"+num ||
            data.nameAddress=="D500TK"+num+"04N"+num || data.nameAddress=="D500TK"+num+"05N"+num || data.nameAddress=="D500TK"+num+"06N"+num ||
            data.nameAddress=="D500TK"+num+"07N"+num || data.nameAddress=="D500TK"+num+"08N"+num || data.nameAddress=="D500TK"+num+"09N"+num ||
            data.nameAddress=="D500TK"+num+"10N"+num || data.nameAddress=="D500TK"+num+"11N"+num || data.nameAddress=="D500TK"+num+"12N"+num ||
            
            data.nameAddress=="D600TK"+num+"01N"+num || data.nameAddress=="D600TK"+num+"02N"+num || data.nameAddress=="D600TK"+num+"03N"+num ||
            data.nameAddress=="D600TK"+num+"04N"+num || data.nameAddress=="D600TK"+num+"05N"+num || data.nameAddress=="D600TK"+num+"06N"+num ||
            data.nameAddress=="D600TK"+num+"07N"+num || data.nameAddress=="D600TK"+num+"08N"+num || data.nameAddress=="D600TK"+num+"09N"+num ||
            data.nameAddress=="D600TK"+num+"10N"+num || data.nameAddress=="D600TK"+num+"11N"+num || data.nameAddress=="D600TK"+num+"12N"+num ||
            
            data.nameAddress=="D700TK"+num+"01N"+num || data.nameAddress=="D700TK"+num+"02N"+num || data.nameAddress=="D700TK"+num+"03N"+num ||
            data.nameAddress=="D700TK"+num+"04N"+num || data.nameAddress=="D700TK"+num+"05N"+num || data.nameAddress=="D700TK"+num+"06N"+num ||
            data.nameAddress=="D700TK"+num+"07N"+num || data.nameAddress=="D700TK"+num+"08N"+num || data.nameAddress=="D700TK"+num+"09N"+num ||
            data.nameAddress=="D700TK"+num+"10N"+num || data.nameAddress=="D700TK"+num+"11N"+num || data.nameAddress=="D700TK"+num+"12N"+num ||
            
            data.nameAddress=="D800TK"+num+"01N"+num || data.nameAddress=="D800TK"+num+"02N"+num || data.nameAddress=="D800TK"+num+"03N"+num ||
            data.nameAddress=="D800TK"+num+"04N"+num || data.nameAddress=="D800TK"+num+"05N"+num || data.nameAddress=="D800TK"+num+"06N"+num ||
            data.nameAddress=="D800TK"+num+"07N"+num || data.nameAddress=="D800TK"+num+"08N"+num || data.nameAddress=="D800TK"+num+"09N"+num ||
            data.nameAddress=="D800TK"+num+"10N"+num || data.nameAddress=="D800TK"+num+"11N"+num || data.nameAddress=="D800TK"+num+"12N"+num ||
            
            data.nameAddress=="D900TK"+num+"01N"+num || data.nameAddress=="D900TK"+num+"02N"+num || data.nameAddress=="D900TK"+num+"03N"+num ||
            data.nameAddress=="D900TK"+num+"04N"+num || data.nameAddress=="D900TK"+num+"05N"+num || data.nameAddress=="D900TK"+num+"06N"+num ||
            data.nameAddress=="D900TK"+num+"07N"+num || data.nameAddress=="D900TK"+num+"08N"+num || data.nameAddress=="D900TK"+num+"09N"+num ||
            data.nameAddress=="D900TK"+num+"10N"+num || data.nameAddress=="D900TK"+num+"11N"+num || data.nameAddress=="D900TK"+num+"12N"+num ||
            
            data.nameAddress=="D1000TK"+num+"01N"+num || data.nameAddress=="D1000TK"+num+"02N"+num || data.nameAddress=="D1000TK"+num+"03N"+num ||
            data.nameAddress=="D1000TK"+num+"04N"+num || data.nameAddress=="D1000TK"+num+"05N"+num || data.nameAddress=="D1000TK"+num+"06N"+num ||
            data.nameAddress=="D1000TK"+num+"07N"+num || data.nameAddress=="D1000TK"+num+"08N"+num || data.nameAddress=="D1000TK"+num+"09N"+num ||
            data.nameAddress=="D1000TK"+num+"10N"+num || data.nameAddress=="D1000TK"+num+"11N"+num || data.nameAddress=="D1000TK"+num+"12N"+num ||
            
            data.nameAddress=="D1100TK"+num+"01N"+num || data.nameAddress=="D1100TK"+num+"02N"+num || data.nameAddress=="D1100TK"+num+"03N"+num ||
            data.nameAddress=="D1100TK"+num+"04N"+num || data.nameAddress=="D1100TK"+num+"05N"+num || data.nameAddress=="D1100TK"+num+"06N"+num ||
            data.nameAddress=="D1100TK"+num+"07N"+num || data.nameAddress=="D1100TK"+num+"08N"+num || data.nameAddress=="D1100TK"+num+"09N"+num ||
            data.nameAddress=="D1100TK"+num+"10N"+num || data.nameAddress=="D1100TK"+num+"11N"+num || data.nameAddress=="D1100TK"+num+"12N"+num ||
            
            data.nameAddress=="D1200TK"+num+"01N"+num || data.nameAddress=="D1200TK"+num+"02N"+num || data.nameAddress=="D1200TK"+num+"03N"+num ||
            data.nameAddress=="D1200TK"+num+"04N"+num || data.nameAddress=="D1200TK"+num+"05N"+num || data.nameAddress=="D1200TK"+num+"06N"+num ||
            data.nameAddress=="D1200TK"+num+"07N"+num || data.nameAddress=="D1200TK"+num+"08N"+num || data.nameAddress=="D1200TK"+num+"09N"+num ||
            data.nameAddress=="D1200TK"+num+"10N"+num || data.nameAddress=="D1200TK"+num+"11N"+num || data.nameAddress=="D1200TK"+num+"12N"+num)

            // if(!n_tks[0]%2){
            //     return this.getButtonOx(data,num,n);
            // }
            // else if(n_tks[0]%2){
            //     return this.getButtonOx(data,num,n);
            // }
            
            
            return this.getButtonOx(data,num,n);
        })
        if(this.state.Temp)
        return array.filter(()=>n==num).map((data)=>{
            if(data.nameAddress=="D101TK"+num+"01N"+num || data.nameAddress=="D101TK"+num+"02N"+num || data.nameAddress=="D101TK"+num+"03N"+num ||
            data.nameAddress=="D101TK"+num+"04N"+num || data.nameAddress=="D101TK"+num+"05N"+num || data.nameAddress=="D101TK"+num+"06N"+num ||
            data.nameAddress=="D101TK"+num+"07N"+num || data.nameAddress=="D101TK"+num+"08N"+num || data.nameAddress=="D101TK"+num+"09N"+num ||
            data.nameAddress=="D101TK"+num+"10N"+num || data.nameAddress=="D101TK"+num+"11N"+num || data.nameAddress=="D101TK"+num+"12N"+num ||
            
            data.nameAddress=="D201TK"+num+"01N"+num || data.nameAddress=="D201TK"+num+"02N"+num || data.nameAddress=="D201TK"+num+"03N"+num ||
            data.nameAddress=="D201TK"+num+"04N"+num || data.nameAddress=="D201TK"+num+"05N"+num || data.nameAddress=="D201TK"+num+"06N"+num ||
            data.nameAddress=="D201TK"+num+"07N"+num || data.nameAddress=="D201TK"+num+"08N"+num || data.nameAddress=="D201TK"+num+"09N"+num ||
            data.nameAddress=="D201TK"+num+"10N"+num || data.nameAddress=="D201TK"+num+"11N"+num || data.nameAddress=="D201TK"+num+"12N"+num ||
            
            data.nameAddress=="D301TK"+num+"01N"+num || data.nameAddress=="D301TK"+num+"02N"+num || data.nameAddress=="D301TK"+num+"03N"+num ||
            data.nameAddress=="D301TK"+num+"04N"+num || data.nameAddress=="D301TK"+num+"05N"+num || data.nameAddress=="D301TK"+num+"06N"+num ||
            data.nameAddress=="D301TK"+num+"07N"+num || data.nameAddress=="D301TK"+num+"08N"+num || data.nameAddress=="D301TK"+num+"09N"+num ||
            data.nameAddress=="D301TK"+num+"10N"+num || data.nameAddress=="D301TK"+num+"11N"+num || data.nameAddress=="D301TK"+num+"12N"+num ||
            
            data.nameAddress=="D401TK"+num+"01N"+num || data.nameAddress=="D401TK"+num+"02N"+num || data.nameAddress=="D401TK"+num+"03N"+num ||
            data.nameAddress=="D401TK"+num+"04N"+num || data.nameAddress=="D401TK"+num+"05N"+num || data.nameAddress=="D401TK"+num+"06N"+num ||
            data.nameAddress=="D401TK"+num+"07N"+num || data.nameAddress=="D401TK"+num+"08N"+num || data.nameAddress=="D401TK"+num+"09N"+num ||
            data.nameAddress=="D401TK"+num+"10N"+num || data.nameAddress=="D401TK"+num+"11N"+num || data.nameAddress=="D401TK"+num+"12N"+num ||
            
            data.nameAddress=="D501TK"+num+"01N"+num || data.nameAddress=="D501TK"+num+"02N"+num || data.nameAddress=="D501TK"+num+"03N"+num ||
            data.nameAddress=="D501TK"+num+"04N"+num || data.nameAddress=="D501TK"+num+"05N"+num || data.nameAddress=="D501TK"+num+"06N"+num ||
            data.nameAddress=="D501TK"+num+"07N"+num || data.nameAddress=="D501TK"+num+"08N"+num || data.nameAddress=="D501TK"+num+"09N"+num ||
            data.nameAddress=="D501TK"+num+"10N"+num || data.nameAddress=="D501TK"+num+"11N"+num || data.nameAddress=="D501TK"+num+"12N"+num ||
            
            data.nameAddress=="D601TK"+num+"01N"+num || data.nameAddress=="D601TK"+num+"02N"+num || data.nameAddress=="D601TK"+num+"03N"+num ||
            data.nameAddress=="D601TK"+num+"04N"+num || data.nameAddress=="D601TK"+num+"05N"+num || data.nameAddress=="D601TK"+num+"06N"+num ||
            data.nameAddress=="D601TK"+num+"07N"+num || data.nameAddress=="D601TK"+num+"08N"+num || data.nameAddress=="D601TK"+num+"09N"+num ||
            data.nameAddress=="D601TK"+num+"10N"+num || data.nameAddress=="D601TK"+num+"11N"+num || data.nameAddress=="D601TK"+num+"12N"+num ||
            
            data.nameAddress=="D701TK"+num+"01N"+num || data.nameAddress=="D701TK"+num+"02N"+num || data.nameAddress=="D701TK"+num+"03N"+num ||
            data.nameAddress=="D701TK"+num+"04N"+num || data.nameAddress=="D701TK"+num+"05N"+num || data.nameAddress=="D701TK"+num+"06N"+num ||
            data.nameAddress=="D701TK"+num+"07N"+num || data.nameAddress=="D701TK"+num+"08N"+num || data.nameAddress=="D701TK"+num+"09N"+num ||
            data.nameAddress=="D701TK"+num+"10N"+num || data.nameAddress=="D701TK"+num+"11N"+num || data.nameAddress=="D701TK"+num+"12N"+num ||
            
            data.nameAddress=="D801TK"+num+"01N"+num || data.nameAddress=="D801TK"+num+"02N"+num || data.nameAddress=="D801TK"+num+"03N"+num ||
            data.nameAddress=="D801TK"+num+"04N"+num || data.nameAddress=="D801TK"+num+"05N"+num || data.nameAddress=="D801TK"+num+"06N"+num ||
            data.nameAddress=="D801TK"+num+"07N"+num || data.nameAddress=="D801TK"+num+"08N"+num || data.nameAddress=="D801TK"+num+"09N"+num ||
            data.nameAddress=="D801TK"+num+"10N"+num || data.nameAddress=="D801TK"+num+"11N"+num || data.nameAddress=="D801TK"+num+"12N"+num ||
            
            data.nameAddress=="D901TK"+num+"01N"+num || data.nameAddress=="D901TK"+num+"02N"+num || data.nameAddress=="D901TK"+num+"03N"+num ||
            data.nameAddress=="D901TK"+num+"04N"+num || data.nameAddress=="D901TK"+num+"05N"+num || data.nameAddress=="D901TK"+num+"06N"+num ||
            data.nameAddress=="D901TK"+num+"07N"+num || data.nameAddress=="D901TK"+num+"08N"+num || data.nameAddress=="D901TK"+num+"09N"+num ||
            data.nameAddress=="D901TK"+num+"10N"+num || data.nameAddress=="D901TK"+num+"11N"+num || data.nameAddress=="D901TK"+num+"12N"+num ||
            
            data.nameAddress=="D1001TK"+num+"01N"+num || data.nameAddress=="D1001TK"+num+"02N"+num || data.nameAddress=="D1001TK"+num+"03N"+num ||
            data.nameAddress=="D1001TK"+num+"04N"+num || data.nameAddress=="D1001TK"+num+"05N"+num || data.nameAddress=="D1001TK"+num+"06N"+num ||
            data.nameAddress=="D1001TK"+num+"07N"+num || data.nameAddress=="D1001TK"+num+"08N"+num || data.nameAddress=="D1001TK"+num+"09N"+num ||
            data.nameAddress=="D1001TK"+num+"10N"+num || data.nameAddress=="D1001TK"+num+"11N"+num || data.nameAddress=="D1001TK"+num+"12N"+num ||
            
            data.nameAddress=="D1101TK"+num+"01N"+num || data.nameAddress=="D1101TK"+num+"02N"+num || data.nameAddress=="D1101TK"+num+"03N"+num ||
            data.nameAddress=="D1101TK"+num+"04N"+num || data.nameAddress=="D1101TK"+num+"05N"+num || data.nameAddress=="D1101TK"+num+"06N"+num ||
            data.nameAddress=="D1101TK"+num+"07N"+num || data.nameAddress=="D1101TK"+num+"08N"+num || data.nameAddress=="D1101TK"+num+"09N"+num ||
            data.nameAddress=="D1101TK"+num+"10N"+num || data.nameAddress=="D1101TK"+num+"11N"+num || data.nameAddress=="D1101TK"+num+"12N"+num ||
            
            data.nameAddress=="D1201TK"+num+"01N"+num || data.nameAddress=="D1201TK"+num+"02N"+num || data.nameAddress=="D1201TK"+num+"03N"+num ||
            data.nameAddress=="D1201TK"+num+"04N"+num || data.nameAddress=="D1201TK"+num+"05N"+num || data.nameAddress=="D1201TK"+num+"06N"+num ||
            data.nameAddress=="D1201TK"+num+"07N"+num || data.nameAddress=="D1201TK"+num+"08N"+num || data.nameAddress=="D1201TK"+num+"09N"+num ||
            data.nameAddress=="D1201TK"+num+"10N"+num || data.nameAddress=="D1201TK"+num+"11N"+num || data.nameAddress=="D1201TK"+num+"12N"+num)
            return <Button  style={{marginTop:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}}  
            className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast" color="primary">     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100">  
                    <font size="2">{data.state!=1?"-":data.lastValue}</font>
                    {/* <span className="opacity-6  pl-0 size_unidad2">  
                    225 </span>  */}
                    </span>
        
                    {/* {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1)
                        return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                    })} */}
                </Button>
        })
        if(this.state.Sat)
        return array.filter(()=>n==num).map((data)=>{
            if(data.nameAddress=="D111TK"+num+"01N"+num || data.nameAddress=="D100TK"+num+"02N"+num || data.nameAddress=="D100TK"+num+"03N"+num ||
            data.nameAddress=="D111TK"+num+"04N"+num || data.nameAddress=="D111TK"+num+"05N"+num || data.nameAddress=="D111TK"+num+"06N"+num ||
            data.nameAddress=="D111TK"+num+"07N"+num || data.nameAddress=="D111TK"+num+"08N"+num || data.nameAddress=="D111TK"+num+"09N"+num ||
            data.nameAddress=="D111TK"+num+"10N"+num || data.nameAddress=="D111TK"+num+"11N"+num || data.nameAddress=="D111TK"+num+"12N"+num ||
            
            data.nameAddress=="D211TK"+num+"01N"+num || data.nameAddress=="D211TK"+num+"02N"+num || data.nameAddress=="D211TK"+num+"03N"+num ||
            data.nameAddress=="D211TK"+num+"04N"+num || data.nameAddress=="D211TK"+num+"05N"+num || data.nameAddress=="D211TK"+num+"06N"+num ||
            data.nameAddress=="D211TK"+num+"07N"+num || data.nameAddress=="D211TK"+num+"08N"+num || data.nameAddress=="D211TK"+num+"09N"+num ||
            data.nameAddress=="D211TK"+num+"10N"+num || data.nameAddress=="D211TK"+num+"11N"+num || data.nameAddress=="D211TK"+num+"12N"+num ||
            
            data.nameAddress=="D311TK"+num+"01N"+num || data.nameAddress=="D311TK"+num+"02N"+num || data.nameAddress=="D311TK"+num+"03N"+num ||
            data.nameAddress=="D311TK"+num+"04N"+num || data.nameAddress=="D311TK"+num+"05N"+num || data.nameAddress=="D311TK"+num+"06N"+num ||
            data.nameAddress=="D311TK"+num+"07N"+num || data.nameAddress=="D311TK"+num+"08N"+num || data.nameAddress=="D311TK"+num+"09N"+num ||
            data.nameAddress=="D311TK"+num+"10N"+num || data.nameAddress=="D311TK"+num+"11N"+num || data.nameAddress=="D311TK"+num+"12N"+num ||
            
            data.nameAddress=="D411TK"+num+"01N"+num || data.nameAddress=="D411TK"+num+"02N"+num || data.nameAddress=="D411TK"+num+"03N"+num ||
            data.nameAddress=="D411TK"+num+"04N"+num || data.nameAddress=="D411TK"+num+"05N"+num || data.nameAddress=="D411TK"+num+"06N"+num ||
            data.nameAddress=="D411TK"+num+"07N"+num || data.nameAddress=="D411TK"+num+"08N"+num || data.nameAddress=="D411TK"+num+"09N"+num ||
            data.nameAddress=="D411TK"+num+"10N"+num || data.nameAddress=="D411TK"+num+"11N"+num || data.nameAddress=="D411TK"+num+"12N"+num ||
            
            data.nameAddress=="D511TK"+num+"01N"+num || data.nameAddress=="D511TK"+num+"02N"+num || data.nameAddress=="D511TK"+num+"03N"+num ||
            data.nameAddress=="D511TK"+num+"04N"+num || data.nameAddress=="D511TK"+num+"05N"+num || data.nameAddress=="D511TK"+num+"06N"+num ||
            data.nameAddress=="D511TK"+num+"07N"+num || data.nameAddress=="D511TK"+num+"08N"+num || data.nameAddress=="D511TK"+num+"09N"+num ||
            data.nameAddress=="D511TK"+num+"10N"+num || data.nameAddress=="D511TK"+num+"11N"+num || data.nameAddress=="D511TK"+num+"12N"+num ||
            
            data.nameAddress=="D611TK"+num+"01N"+num || data.nameAddress=="D611TK"+num+"02N"+num || data.nameAddress=="D611TK"+num+"03N"+num ||
            data.nameAddress=="D611TK"+num+"04N"+num || data.nameAddress=="D611TK"+num+"05N"+num || data.nameAddress=="D611TK"+num+"06N"+num ||
            data.nameAddress=="D611TK"+num+"07N"+num || data.nameAddress=="D611TK"+num+"08N"+num || data.nameAddress=="D611TK"+num+"09N"+num ||
            data.nameAddress=="D611TK"+num+"10N"+num || data.nameAddress=="D611TK"+num+"11N"+num || data.nameAddress=="D611TK"+num+"12N"+num ||
            
            data.nameAddress=="D711TK"+num+"01N"+num || data.nameAddress=="D711TK"+num+"02N"+num || data.nameAddress=="D711TK"+num+"03N"+num ||
            data.nameAddress=="D711TK"+num+"04N"+num || data.nameAddress=="D711TK"+num+"05N"+num || data.nameAddress=="D711TK"+num+"06N"+num ||
            data.nameAddress=="D711TK"+num+"07N"+num || data.nameAddress=="D711TK"+num+"08N"+num || data.nameAddress=="D711TK"+num+"09N"+num ||
            data.nameAddress=="D711TK"+num+"10N"+num || data.nameAddress=="D711TK"+num+"11N"+num || data.nameAddress=="D711TK"+num+"12N"+num ||
            
            data.nameAddress=="D811TK"+num+"01N"+num || data.nameAddress=="D811TK"+num+"02N"+num || data.nameAddress=="D811TK"+num+"03N"+num ||
            data.nameAddress=="D811TK"+num+"04N"+num || data.nameAddress=="D811TK"+num+"05N"+num || data.nameAddress=="D811TK"+num+"06N"+num ||
            data.nameAddress=="D811TK"+num+"07N"+num || data.nameAddress=="D811TK"+num+"08N"+num || data.nameAddress=="D811TK"+num+"09N"+num ||
            data.nameAddress=="D811TK"+num+"10N"+num || data.nameAddress=="D811TK"+num+"11N"+num || data.nameAddress=="D811TK"+num+"12N"+num ||
            
            data.nameAddress=="D911TK"+num+"01N"+num || data.nameAddress=="D911TK"+num+"02N"+num || data.nameAddress=="D911TK"+num+"03N"+num ||
            data.nameAddress=="D911TK"+num+"04N"+num || data.nameAddress=="D911TK"+num+"05N"+num || data.nameAddress=="D911TK"+num+"06N"+num ||
            data.nameAddress=="D911TK"+num+"07N"+num || data.nameAddress=="D911TK"+num+"08N"+num || data.nameAddress=="D911TK"+num+"09N"+num ||
            data.nameAddress=="D911TK"+num+"10N"+num || data.nameAddress=="D911TK"+num+"11N"+num || data.nameAddress=="D911TK"+num+"12N"+num ||
            
            data.nameAddress=="D1011TK"+num+"01N"+num || data.nameAddress=="D1011TK"+num+"02N"+num || data.nameAddress=="D1011TK"+num+"03N"+num ||
            data.nameAddress=="D1011TK"+num+"04N"+num || data.nameAddress=="D1011TK"+num+"05N"+num || data.nameAddress=="D1011TK"+num+"06N"+num ||
            data.nameAddress=="D1011TK"+num+"07N"+num || data.nameAddress=="D1011TK"+num+"08N"+num || data.nameAddress=="D1011TK"+num+"09N"+num ||
            data.nameAddress=="D1011TK"+num+"10N"+num || data.nameAddress=="D1011TK"+num+"11N"+num || data.nameAddress=="D1011TK"+num+"12N"+num ||
            
            data.nameAddress=="D1111TK"+num+"01N"+num || data.nameAddress=="D1111TK"+num+"02N"+num || data.nameAddress=="D1111TK"+num+"03N"+num ||
            data.nameAddress=="D1111TK"+num+"04N"+num || data.nameAddress=="D1111TK"+num+"05N"+num || data.nameAddress=="D1111TK"+num+"06N"+num ||
            data.nameAddress=="D1111TK"+num+"07N"+num || data.nameAddress=="D1111TK"+num+"08N"+num || data.nameAddress=="D1111TK"+num+"09N"+num ||
            data.nameAddress=="D1111TK"+num+"10N"+num || data.nameAddress=="D1111TK"+num+"11N"+num || data.nameAddress=="D1111TK"+num+"12N"+num ||
            
            data.nameAddress=="D1211TK"+num+"01N"+num || data.nameAddress=="D1211TK"+num+"02N"+num || data.nameAddress=="D1211TK"+num+"03N"+num ||
            data.nameAddress=="D1211TK"+num+"04N"+num || data.nameAddress=="D1211TK"+num+"05N"+num || data.nameAddress=="D1211TK"+num+"06N"+num ||
            data.nameAddress=="D1211TK"+num+"07N"+num || data.nameAddress=="D1211TK"+num+"08N"+num || data.nameAddress=="D1211TK"+num+"09N"+num ||
            data.nameAddress=="D1211TK"+num+"10N"+num || data.nameAddress=="D1211TK"+num+"11N"+num || data.nameAddress=="D1211TK"+num+"12N"+num)
            return <Button style={{marginTop:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}} 
              className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast" color="primary" onClick={(()=>{
                //data.state=1?this.updateTagState(data._id,1):this.updateTagState(data._id,0)
            })}>     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100">
                    <font size="2">{data.state!=1?"-":data.lastValue}</font>
                    {/* <span className="opacity-6  pl-0 size_unidad2">  
                    225 </span> */}
                     </span>
        
                     {/* {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1)
                        return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                    })} */}
                </Button>
        })
    }
    getButtonTagCabecera=(array,n,num)=>{
        return array.filter(()=>n==num).map((data)=>{
            
            if(data.nameAddress=="D100TK"+num+"01N"+num || data.nameAddress=="D101TK"+num+"01N"+num || data.nameAddress=="D111TK"+num+"01N"+num ||
             data.nameAddress=="D415TK"+num+"01N"+num)
            return <Button id={data.id} style={{marginTop:30,marginLeft:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}} 
            className=" mr-0 mb-1    pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast opacity-6" color="primary" onClick={((e)=>{
                // console.log(data._id)
                // console.log(data.state);
                //data.state=1?this.updateTagState(data._id,1):this.updateTagState(data._id,0)
                // if(data.state==1){
                //     this.updateTagState(data._id,0)
                // }else{
                //     this.updateTagState(data._id,1)
                // }
                
            })}>     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100 ">
                    <font size="2">{data.state!=1?"-":data.lastValue}</font>
                    {/* <span className="opacity-6  pl-0 size_unidad2">  
                    225 </span>  */}
                    </span>
        
                   
                    {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1 && data.nameAddress=="D100TK"+num+"01N"+num){
                            if(data.lastValue>data.alertMin && data.lastValue<data.alertMax){
                                //return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                                return <span  className={cx("badge badge-dot badge-dot-lg badge-success")}> </span>
                            }else{
                                return <span  className={cx("badge badge-dot badge-dot-lg badge-danger")}> </span>
                            }
                        }
                        
                    })}
                </Button>
        })
    }

    silenciarAlarma=(alarm,address)=>{
        const token = "tokenfalso";
        const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };
        let content="";
        //console.log("alarma :"+alarm)
        content={
            "lastValue": 1,
            "lastValueWrite":1,
            "write":true
        }
        console.log(address)
        let tags_capture=this.state.myTag.filter((tag)=>{
            let name_addr=String(tag.nameAddress);
            return name_addr==address;
        })
        .map((data)=>{
            let EndPointTag = `${API_ROOT3}/tag/${data._id}`;
            axios.put(EndPointTag, content, config)
            .then(response => {
                console.log(response)
                //toast['success']('Almacenado Correctamente', { autoClose: 4000 })
                console.log(response);
                this.loadDataTag1();
                this.getAlarma(address);
            });
        })
    }

    getAlarma=(address)=>{
        let tags_capture="";
        return tags_capture=this.state.myTag.filter((tag)=>{
            let name_addr=String(tag.nameAddress);
            return name_addr==address;
        })
        .map((data)=>{
            return data.lastValue
        })
    }

    handleClickGuardar=()=>{
            modificacion_general=false;
            this.almacenarConfig();
    }

    handleClickAplicar = () =>{
                modificacion_general=true;
                this.almacenarConfig();
    }

    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }
  
    render() { 
        // this.renderizarToggle('101');
        // this.renderizarToggle('201');
        // this.renderizarToggle('301');
        const { myDataTKN100,myDataTKN200,myDataTKN300,myDataTKN400,myDataTKN500,myDataTKN600,myDataTKN700,myDataTKN800,isLoading, error} = this.state;  
        if (error) {
            return <p>{error.message}</p>;
        }  
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }   

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        const testvalue = "hello";

    return (

        
        <Fragment>
            
                  
             

                            {/* <div className="app-page-title">
                                <div className="page-title-wrapper">
                                    <div className="page-title-heading">
                                        <div>
                                        Panel General
                                        </div>
                                    </div> */}
                                   {/* <div className="page-title-actions">
                                        Alarmado
                                        <span  className="badge badge-dot badge-dot-lg badge-danger  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>  
                                         Normal
                                        <span  className="badge badge-dot badge-dot-lg badge-success  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span> 
                                     </div> */}                   
                                {/* </div>
                             </div> */}

<Modal isOpen={this.state.modal1}>
                                                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                    <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion TK {this.state.mySalaModal}</ModalHeader>
                                                    <ModalBody>

                                                    <Row>     
                                                    
                                                        <Col  xs="12" md="12" lg="12">
                                                            <Card className="main-card mb-3">
                                                                <CardBody>
                                                                    <CardTitle>Oxigeno</CardTitle>
                                                                    <Form>
                                                                        <Row>

                                                                            <Col xs="6" md="6" lg="6">
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                <Label for="setpointOx" className="m-0">Set Point</Label>
                                                                                <InputGroup>                                                                                      
                                                                                    <Input   id="setpointOx" defaultValue={this.state.setpointOx}  onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">                                                                                  
                                                                                    <InputGroupText>mg/L</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>       
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="timeOn" className="m-0">Time Off</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input  id="timeOn" defaultValue={this.state.timeOn}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>Seg</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>                                                                    
                                                                            <FormGroup className="mt-2" style={{display:visible}}>
                                                                                <Label for="maxOx" className="m-0">Alarma Alta de Oxígeno</Label>
                                                                                <InputGroup>                                                                                      
                                                                                    <Input   id="maxOx" defaultValue={this.state.maxOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">                                                                                  
                                                                                    <InputGroupText>mg/L</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>                                                                                 
                                                                            </FormGroup>
                                                                            </Col>

                                                                            <Col xs="6" md="6" lg="6">
                                                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="histeresisOx" className="m-0">Histeresis</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input   id="histeresisOx" defaultValue={this.state.histeresisOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>mg/L</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                                <FormGroup   className="mt-2" style={{display:visible}}>
                                                                                    <Label for="timeOff" className="m-0">Time On</Label>
                                                                                    <InputGroup >                                                                                      
                                                                                        <Input  id="timeOff" defaultValue={this.state.timeOff}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>Seg</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                                <FormGroup className="mt-2" style={{display:visible}}>
                                                                                    <Label for="minOx" className="m-0">Alarma Baja de Oxígeno</Label>
                                                                                    <InputGroup>                                                                                      
                                                                                        <Input   id="minOx" defaultValue={this.state.minOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                        <InputGroupAddon addonType="append">                                                                                  
                                                                                        <InputGroupText>mg/L</InputGroupText>
                                                                                        </InputGroupAddon>
                                                                                    </InputGroup>                                                                                 
                                                                                </FormGroup>
                                                                            </Col>

                                                                        </Row>
                                                                    </Form>
                                                                </CardBody>
                                                            </Card>

                                                            <Card>
                                                                <CardBody>
                                                                <FormGroup   className="mt-2" >
                                                                    <Row>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <CardTitle>Estado</CardTitle>
                                                                        </Col>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <InputGroup style={{paddingTop:5}}>
                                                                                {
                                                                                    this.state.nCard.map((c,i)=>{
                                                                                        if(i==1)
                                                                                            if(sw){
                                                                                                return <label key={i} className="switch">
                                                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                                // this.setState({SW:false})
                                                                                                                sw=false;
                                                                                                            })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                            <span className="slider round"></span>
                                                                                                        </label>
                                                                                            }else{
                                                                                                return <label key={i} className="switch">
                                                                                                            <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                                // this.setState({SW:true})
                                                                                                                sw=true;
                                                                                                            })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                            <span className="slider round"></span>
                                                                                                        </label>
                                                                                            }
                                                                                    })
                                                                                }  
                                                                                <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Habilitar TK</Label>
                                                                                </InputGroup>
                                                                        </Col>
                                                                    </Row>
                                                                    <hr />
                                                                    <Row>
                                                                        <Col xs="6" md="6" lg="6">
                                                                            <CardTitle>Control</CardTitle>
                                                                        </Col>
                                                                        <Col xs="6" md="6" lg="6">
                                                                                <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw=false;
                                                                                                                    c_sw3=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw=true;
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw3=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Automatico</Label>
                                                                                    </InputGroup>
                                                                                    
                                                                                    <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw2){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw2} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw2} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw2=true;
                                                                                                                    c_sw=false;
                                                                                                                    c_sw3=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual On</Label>
                                                                                    </InputGroup>      

                                                                                    <InputGroup style={{paddingTop:5}}>
                                                                                    {
                                                                                        this.state.nCard.map((c,i)=>{
                                                                                            if(i==1)
                                                                                                if(c_sw3){
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw3} onClick={(()=>{
                                                                                                                    // this.setState({SW:false})
                                                                                                                    c_sw3=false;
                                                                                                                    c_sw=true;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }else{
                                                                                                    return <label key={i} className="switch">
                                                                                                                <input type="checkbox" id="estadoTK" checked={c_sw3} onClick={(()=>{
                                                                                                                    // this.setState({SW:true})
                                                                                                                    c_sw3=true;
                                                                                                                    c_sw2=false;
                                                                                                                    c_sw=false;
                                                                                                                })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                                <span className="slider round"></span>
                                                                                                            </label>
                                                                                                }
                                                                                        })
                                                                                    }  
                                                                                    <Label for="timeOff" className="m-0" style={{paddingLeft:15}}>Manual Off</Label>
                                                                                    </InputGroup>
                                                                            </Col>
                                                                        </Row>
                                                                         
                                                                        </FormGroup>
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        
                                                    </Row>
                                                    
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Row style={{width:`${100}%`}}>
                                                                <Col xs="6" md="6" lg="6"><Button color="primary" disabled={habilitado} onClick={() =>this.handleClickAplicar()}>Aplicar a Modulo {`${this.state.mySalaModal[0]}00`}</Button></Col>
                                                                <Col xs="3" md="3" lg="3"><Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button></Col>
                                                                <Col xs="3" md="3" lg="3"><Button color="primary" disabled={habilitado} onClick={() => this.handleClickGuardar()}>Guardar</Button>{' '}</Col>
                                                            </Row>
                                                    </ModalFooter>
                                                    </BlockUi>
                                        </Modal>
                            
                                <Row>

                                
                                <Col md="12" lg="12">

                                <Card className="main-card mb-1">

                                <div class="body-block-example-3 d-none">
                                    <div class="loader">
                                        <div class="line-scale-pulse-out">
                                            <div class="bg-warning"></div>
                                            <div class="bg-warning"></div>
                                            <div class="bg-warning"></div>
                                            <div class="bg-warning"></div>
                                            <div class="bg-warning"></div>
                                        </div>
                                    </div>
                                </div>
                                            
                                            <CardHeader className="card-header-tab" style={{height:50}}>
                                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal" style={{marginLeft:-20}}>
                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                <Button 
                                                                className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Ox?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.setState({
                                                                        Ox:true,
                                                                        Temp:false,
                                                                        Sat:false
                                                                    })
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} opacity-1 */}<font color={!this.state.Ox?"":"#fff"}>Ox (mg/L)</font>
                                                                        </div>
                                                                </Button>  
                                                        </div>
                                                        
                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">

                                                            
                                                                <Button 
                                                                className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Temp?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.setState({
                                                                        Ox:false,
                                                                        Temp:true,
                                                                        Sat:false
                                                                    })
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} */}<font color={!this.state.Temp?"":"#fff"}>Temp (°C)</font>
                                                                        </div>
                                                                </Button>  
                                                        </div>
                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                <Button 
                                                                className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Sat?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.setState({
                                                                        Ox:false,
                                                                        Temp:false,
                                                                        Sat:true
                                                                    })
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} */}<font color={!this.state.Sat?"":"#fff"}>Sat (%)</font>
                                                                        </div>
                                                                </Button>  
                                                        </div>
                                                        <div id="rootB1"/>
                                                        <div id="rootB2"/>
                                                        <div id="rootB3"/>
                                                        <div id="rootB4"/>
                                                        <div id="rootB5"/>
                                                        <div id="rootB6"/>
                                                        <div id="rootB7"/>
                                                        <div id="rootB8"/>

                                                        
                                                        
                                                                                    
                                                    </div>
                                                </CardHeader>
                                            <CardBody className="p-0 opacity-0"> 
                                                <Row className="no-gutters" style={{marginLeft:20}}>
                                                    {
                                                        this.state.n_naves.map((n)=>{
                                                            if(n==1)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        {/* <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn text-center">   </div>  */}
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                    
                                                    <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary"
                                                        onClick={(()=>{
                                                        /*redirigir*/

                                                        })}>

                                                        
                                                            
                                                        
                                                    
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,1)
                                                                                }
                                                                                
                                                                  
                                                                            </div>
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div id = "rootboi" className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,1)
                                                                                } 
                                                                                          
                                                                            </div>
                                                                            </Col>                                 
                                                                        </Row>                    
                                                                   </Col>
                                                            if(n==2)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,2)
                                                                                }
                                                                                        
                                                                            </div>
                                                                            </Col>
                                                                            
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary"  style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,2)
                                                                                }
                                                                                       
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                      
                                                                   </Col>
                                                            if(n==3)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,3)
                                                                                } 
                                                                                     
                                                                            </div> 
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,3)
                                                                                }
                                                                                           
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                     
                                                                    </Col>
                                                            if(n==4)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,4)
                                                                                }
                                                                                           
                                                                            </div> 
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,4)
                                                                                }
                                                                                      
                                                                            </div> 
                                                                            </Col>
                                                                        </Row>                       
                                                                    </Col>
                                                            if(n==5)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,5)

                                                                                    

                                                                                    
                                                                                }
                                                                                
                                                                                
                                                                                       
                                                                            </div>
                                                                            
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,5)
                                                                                }
                                                                                       
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                      
                                                                    </Col>
                                                            if(n==6)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,6)
                                                                                }
                                                                                       
                                                                            </div>
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,6)
                                                                                }
                                                                                      
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                     
                                                                    </Col>
                                                            if(n==7)
                                                            return<Col key={n} style={{minWidth:180,maxWidth:180}}> 
                                                                        <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Modulo{` ${n[1]}00`}</b>
                                                                        <span className="badge badge-pill badge-danger btn btn-danger" onClick={(()=>{
                                                                            this.silenciarAlarma(this.getAlarma(`M3N${n[1]}`),`M3N${n[1]}`)
                                                                        })} style={{marginLeft:5,cursor:'pointer'}}><i className="pe-7s-bell"></i>
                                                                        </span>
                                                                        </Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2==0;
                                                                                        }
                                                                                    }),n,7)
                                                                                }
                                                                                       
                                                                            </div> 
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                            
                                                                                {
                                                                                    this.getButtonTag(this.state.myTag.filter((data)=>{
                                                                                        let split="";
                                                                                        let n_tks="";
                                                                                        if(data.nameAddress.includes('TK')){
                                                                                            split=String(data.nameAddress).split('K');
                                                                                            n_tks=split[1].split('N');
                                                                                            return n_tks[0]%2!=0;
                                                                                        }
                                                                                    }),n,7)
                                                                                }
                                                                                       
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                      
                                                                    </Col>
                                                            // if(n==8)
                                                            // return<Col > 
                                                            //             <Row><Col size={12} style={{marginLeft:30,marginTop:10}}><b>Cabecera{n}</b></Col></Row>
                                                            //             <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${150}px`}}>
                                                            //                 {
                                                            //                     this.getButtonTag(myDataTKN800,n,8)
                                                            //                 }           
                                                            //             </div>                        
                                                            //         </Col>
                                                            
                                                        })
                                                    }
                                                    
                                                </Row>
                                            </CardBody>
                                            <CardFooter className="text-muted center">Última Actualización: {this.state.dateTimeRefresh!=""?moment(this.state.dateTimeRefresh).format('DD-MM-YYYY HH:mm:ss'):""}</CardFooter>
                                            </Card>
                                            <Card>
                                                <CardHeader>Cabecera
                                                {
                                                        this.state.n_naves.map((n)=>{
                                                            if(n==8)
                                                            return<Col> 
                                                                        <div className="widget-description opacity-10 text-focus  border-primary">
                                                                            {
                                                                                this.getButtonTagCabecera(this.state.myTag,n,8)
                                                                            }           
                                                                        </div>
                                                                        <div style={{marginTop:25}}></div>                       
                                                                    </Col>
                                                        })
                                                }
                                                    <div className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-danger">Danger</div><p style={{marginLeft:4,marginRight: 4,marginTop:14}}>Alarmado | </p>
                                                    <div className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div><p style={{marginLeft:4,marginRight: 4,marginTop:14}}>Normalizado | </p>
                                                    <span className="badge badge-pill badge-success">A</span><p style={{marginLeft:4,marginRight: 4,marginTop:14}}>Automatico | </p>
                                                    <span className="badge badge-pill badge-warning">On</span><p style={{marginLeft:4,marginRight: 4,marginTop:14}}>Manual On | </p>
                                                    <span className="badge badge-pill badge-secondary">Off</span><p style={{marginLeft:4,marginRight: 4,marginTop:14}}>Manual Off</p>
                                                </CardHeader>
                                            </Card>
                                    </Col>

                   
                                 



                                </Row>
              
          
      </Fragment>
    )
  }

}

export default PanelGeneralIgnao;