import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT } from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import {
  setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../../reducers/Session';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';


//sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let load=false;
let icon_up="lnr-arrow-up";
let icon_right="lnr-arrow-right";
let icon_down="lnr-arrow-down";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
//empresas=empresas.filter(e=>e.active==true).map(e=>e);
let ubicaciones=JSON.parse(sessionStorage.getItem("Areas"));
let c=0;
let areas_centros=[];
if(empresas!=null){
  areas_centros=empresas.map(e=>{return e.areaId});
}



let title_centro="";
let code_centro="";
let Buscar="";
let Buscar2="";
let active_todos=true;
let check=false;
let check2=false;
let active_config="none";
let ModalActive=false;
class Resumen extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Modulos:[],
          isLoading:true,
          Mis_Prom:[],
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Abiotic:true,
          Meteo:false,
          Proceso:false,
          Equipos:false,
          card:[1,2,3,4,5,6,7,8],
          redirect:false,
          redirect2:false,
          Centros:[],
          act_graph0:true,
          act_graph1:false,
          act_graph2:false,
          act_all:true,
          act_oxd:false,
          act_temp:false,
          act_oxs:false,
          act_sal:false,
          act_oxd2:true,
          act_temp2:false,
          act_oxs2:false,
          act_sal2:false,
          active_grafic:"none",
          ModalState:false,
          TypeGraphic:"1",
          TypeGraphic2:"2",
          Events:[],
          Centro_Actividad:[],
          txtCenActividad:"",
          Actividad:[],
          Centros2:[],
        };     
        this.toggle = this.toggle.bind(this);
        this.getDataCentros = this.getDataCentros.bind(this);
        this.intervalIdTag2 = this.getCentros.bind(this);
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);      
        this.toggleModal1 = this.toggleModal1.bind(this);
        
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c2"){
          active_config="block";
        }
        // this.getUbicacion();
        //this.setState({isLoading: true})
        //this.setState({isLoading: false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        //this.setState({ isLoading: true });
        //this.intervalIdTag1 = setInterval(() => this.updatePromedios(),210000);
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
            this.getDataCentros();
        }
        //this.intervalIdTag2 = setInterval(() => this.getCentros(),210000);
        this.getCentros();
        this.getActivity();
      }

      componentWillUnmount = () => {
        clearInterval(this.getDataCentros);
     }  

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        //this.getSondas(centro[0].toString());
        if(c!=0)
          this.setState({redirect:true})
        c++;
      }

      ingresarCentroUsuario = (centro) =>{
          let {setCentroUsuario} = this.props;
          setCentroUsuario(centro);
      }

      toggle(e) {
        let event = e.target.dataset.event;
        this.setState({ collapse: this.state.collapse === Number(event) ? 0 : Number(event) });
      }

      getCentros(){
          let token=sessionStorage.getItem("token_cloud")
          this.tagservices.getUserToken(token).then(respuesta=>{
            let centros=respuesta.centros;
            this.tagservices.getWorkplaceUser(centros).then(workplace=>{
              this.setState({Centros:workplace,isLoading:false})
            })
          }).catch(e=>e)
          // axios
          // //.post(API_ROOT+"/"+empresa+"/centros_activos",{
          // .get(API_ROOT+`/workPlace`)
          // .then(response => {   
          //     // alert(response.data.statusCode==200);
          //     if(response.data.statusCode==200){
          //         let centros=response.data.data;
          //         this.setState({Centros:centros,isLoading:false})
          //     }
          // })
          // .catch(error => {
          //     console.log(error)
          // });
          //console.log(this.state.)
      }

      

      toggleModal1(ModalState,code,title) {
        if(!ModalState){
          this.setState({ModalState,Centro_Actividad:[]})
        }
        ModalActive=ModalState;
        this.setState({ModalState})
        code_centro=code;
        title_centro=title;
      }

      getDataCentros=()=>{
        areas_centros=areas_centros.filter(this.onlyUnique);
        try{
        //empresas=JSON.parse(sessionStorage.getItem("Centros"));
        // alert(this.state.Centros)
        // let token=sessionStorage.getItem("token_cloud")
          // this.tagservices.getUserToken(token).then(respuesta=>{
          //   let centros=respuesta.centros;
          //   let array=centros.split(',')
          //   array.map(c=>{
          //     this.tagservices.getWorkplaceCode(c).then(respuesta=>{
          //       this.setState({Centros2:respuesta})
          //     }).catch(e=>e)
          //   })
          // }).catch(e=>e)
        empresas.map((e,i)=>{
          this.tagservices.getfindEvent(e.code).then(events=>{
              events.map(ev=>{
                let eventos=this.state.Events;
                this.setState({Events:eventos.filter((e)=>{
                  return e._id!=ev._id
                }).concat(ev)})
              })
          }).catch(e=>console.log(e))
          this.tagservices.getZones(e.code,"get").then(zonas=>{
            let zones=zonas.filter(z=>z.active==true);
               zones.map((z)=>{
                   this.tagservices.getZonesTags(z._id).then(tags=>{
                        try{
                          tags.filter(t=>t.active==true).map(t=>{
                            z.locationId=t.locationId;
                            let promedio={
                              id:t._id,
                              name:t.nameAddress,
                              locationId:t.locationId,
                              prom:t.prom,
                              max:t.max,
                              min:t.min,
                              centroId:e.code,
                              ultimo_valor:t.lastValueWrite,
                              ultima_fecha:t.dateTimeLastValue,
                              alerta_max:t.alertMax,
                              alerta_min:t.alertMin,
                            }
                            this.setState({Mis_Prom:this.state.Mis_Prom.filter(p=>p.id!=promedio.id).concat(promedio)});
                          })
                        }catch(e){
                          //console.log(e)
                        }
                          //  this.tagservices.getRegistros(t._id,30).then(registros=>{
                             
                          //  })
                         
                         //l.tags=tags;
                       })
                     this.setState({MyData:this.state.MyData.concat(zones)});
               })
               this.setState({Modulos:this.state.Modulos.filter(m=>m.centro!=e.name).concat({centro:e.name,zonas:zones.map(z=>z.name)})})
         }).catch(e=>{
           console.log(e)
          //  logout();
          //  this.setState({redirect:true})
         });
       })
      }catch(e){
        console.log(e)
        // logout();
        // this.setState({redirect:true})
      }
    }

    updatePromedios(){
      this.state.Mis_Prom.map((p)=>{
        this.tagservices.getTags(p.id).then(t=>{
          let promedio={
            id:t[0]._id,
            name:t[0].nameAddress,
            locationId:t[0].locationId,
            prom:t[0].prom,
            max:t[0].max,
            min:t[0].min,
            centroId:p.centroId,
            ultimo_valor:t[0].lastValueWrite,
            ultima_fecha:t[0].dateTimeLastValue,
            alerta_max:t[0].alertMax,
            alerta_min:t[0].alertMin,
          }
          this.setState({Mis_Prom:this.state.Mis_Prom.filter(p=>p.id!=promedio.id).concat(promedio)});
        }).catch(e=>{
          //console.log(e)
        })
      })
    }
     

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }

    getPromCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      let unity="";
      if(type=="oxd")
        type=" oxd";
        unity="mg/L";
      if(type=="temp")
        unity="°C";
      if(type=="oxs")
        unity="%";
      if(type=="sal")
        unity="PSU";
      let measurements=[];
      modulo!=""?measurements=this.state.Mis_Prom.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p):
      measurements=this.state.Mis_Prom.filter(p=>p.centroId==id&&p.name.includes(type)).map(p=>p)
      //console.log("medio"+JSON.stringify(measurements))
      let prom=((measurements.reduce((a, b) => +a + +b.prom, 0)/measurements.length)).toFixed(1);
      let ultimo_valor=((measurements.reduce((a, b) => +a + +b.ultimo_valor, 0)/measurements.length)).toFixed(1);
      let icon="";
      let mensaje="";
      if(ultimo_valor>Math.round(prom)){
        icon=icon_up;
        mensaje="Tendencia sobre el promedio"
      }if(ultimo_valor=>prom && ultimo_valor<=Math.round(prom)){
        icon=icon_right;
        mensaje="Tendencia entre el promedio"
      }if(ultimo_valor<prom && ultimo_valor<Math.round(prom)){
        icon=icon_down;
        mensaje="Tendencia bajo el promedio"
      }
     if(color!=""){
      return <Fragment>
        {/* <UncontrolledTooltip placement="top-end" target={`oxd_tendencia${id}`}>
          {mensaje}
        </UncontrolledTooltip> */}
        <font color={color}><span id={`oxd_tendencia${id}`} className={icon}></span><b> {check==true?prom:ultimo_valor} {unity}</b></font>
        </Fragment>
    }
    else{
      return check==true?prom:ultimo_valor
    }
  }

    getPromMaxCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      let measurements=this.state.Mis_Prom.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p);
      if(color!="")
        return <font color={color}><b>max: {((measurements.reduce((a, b) => +a + +b.max, 0)/measurements.length)).toFixed(1)}</b></font>
      else
        return ((measurements.reduce((a, b) => +a + +b.max, 0)/measurements.length)).toFixed(1)
    }

    getPromMinCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      let measurements=this.state.Mis_Prom.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p);
      if(color!="")
        return <font color={color}><b>min: {((measurements.reduce((a, b) => +a + +b.min, 0)/measurements.length)).toFixed(1)}</b></font>
      else
        return ((measurements.reduce((a, b) => +a + +b.min, 0)/measurements.length)).toFixed(1)
    }
    getColorCol(prom,min,max,unity,color,index,ultimo_valor,id){
      let icon="";
      let mensaje="";
      if(ultimo_valor>Math.round(prom)){
        icon=icon_up;
        mensaje="Tendencia sobre el promedio"
      }if(ultimo_valor=>prom && ultimo_valor<=Math.round(prom)){
        icon=icon_right;
        mensaje="Tendencia entre el promedio"
      }if(ultimo_valor<prom && ultimo_valor<Math.round(prom)){
        icon=icon_down;
        mensaje="Tendencia bajo el promedio"
      }
      return (
      <Fragment>
        <UncontrolledTooltip placement="top-end" target={`oxd_tendencia2${id}`}>
          {mensaje}
        </UncontrolledTooltip>
        <Col id="PopoverFocus"><font color={color}><span id={`oxd_tendencia2${id}`} className={icon}></span> {check==true?prom:ultimo_valor} {unity}</font></Col>
        <Col id="PopoverFocus"><font color={color}>min {min}</font></Col>
        <Col id="PopoverFocus"><font color={color}>max {max}</font></Col>
      </Fragment>
      )
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    endDate(code){
      empresas=JSON.parse(sessionStorage.getItem("Centros"));
      let data=empresas.filter(e=>e.code==code && e.endDate!="").map((e)=>e.endDate)
      let data2=this.state.Centros.filter(e=>{return e._id==code}).map(e=>{
        // fecha.setHours(fecha.getHours()-1);
        return e.endDate
      })
      //alert(data2[0])
      if(data2[0]!=null && data[0]!=null){
        return moment(data2[0]).format("HH:mm DD-MMM")
      }else{
        return <Badge color="secondary" pill>sin registros actualizados</Badge>
      }
    }

    endDate2(code){
      let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      empresas=JSON.parse(sessionStorage.getItem("Centros"));
      let color="", color2="";
      let data=empresas.filter(e=>e.code==code && e.endDate!="").map((e)=>{
        color=this.getColorAlert(e,"oxd",100,miscolores,0);
        color2=this.getColorAlert(e,"oxd",200,miscolores,0);
        return e.endDate
      })
      let data2=this.state.Centros.filter(e=>e._id==code).map(e=>{
        var fecha=new Date(e.endDate);
        // fecha.setHours(fecha.getHours()-1);
        return fecha
      })
      let myId="status"+code;
      if(data[0]!=null && data2[0]!=null){
        let fecha=new Date();
        let f1=data2[0];
        let f2=new Date(fecha.setHours(fecha.getHours()-1));
        let f3=new Date(fecha.setHours(fecha.getHours()-12));
        if(color!="" || color2!=""){
          //alert(color);
          return <Fragment>{this.createTooltip(myId,"Registros en 0")}
           <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        }
        else if(f1>=f2){
          //alert(f2)
          return <Fragment>{this.createTooltip(myId,"En linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(f1>=f3){
          return <Fragment>{this.createTooltip(myId,"En linea en las 12hrs.")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-dark">Dark</div></Fragment>
        }
        // else if(f1<moment(f2).format("HH:mm DD-MMM") && f1>=moment(f3).format("HH:mm DD-MMM")){
        //   return <Fragment>{this.createTooltip(myId,"En linea dentro de las 24 hrs")}
        //   <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        // }
        else{
          return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
        }
      }else{
        return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
        <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
      }
    }

    getGrafic(data,type,visible,code,graphic){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let max=0;
      let min=0;
      let v="";
      let color="";
      if(visible)
        v="block"
      else
        v="none"
      let unity="";
      if(type=="oxd"){
        color=miscolores[0];
        max=10.8;
        min=4.8;
        unity="mg/L";
      }
      if(type=="temp"){
        color=miscolores[5];
        max=9.1;
        min=6.7;
        unity="°C";
      }
      if(type=="oxs"){
        color=miscolores[2];
        unity="%";
      }
      if(type=="sal"){
        color=miscolores[1];
        unity="PSU";
      }
      if(graphic=="1")
        return <div style={{ width: '100%', height: 300, display:v }} >  
        <ResponsiveContainer>
        <BarChart
          width={window.outerWidth-100}
          height={300}
          data={data}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
          >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis label={{ value: unity, angle: -90, position: 'insideLeft', textAnchor: 'middle' }} />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="oxd" stroke="#82ca9d" />
          {this.getBar(type,"")}
        </BarChart>
        </ResponsiveContainer>
        </div>
      if(graphic=="2")
      return <Fragment><div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart
          layout="vertical"
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20, right: 20, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis type="number" />
          <YAxis dataKey="name" type="category" />
          <Tooltip />
          <Legend />
          {/* <Area dataKey="amt" fill="#8884d8" stroke="#8884d8" /> */}
          {this.getBar(type,"")}
          {/* <Line dataKey="uv" stroke="#ff7300" /> */}
        </ComposedChart>
        </ResponsiveContainer>
        </div>
        <div style={{display:v}}>
        <Row>
          <Col><b>Detalles del Centro</b></Col>
          <Col></Col>
          <Col></Col>
        </Row>
        <br></br>
        {this.getDetails(code,type,color,v)}
        </div>
      </Fragment>
      if(graphic=="2.5")
      return <Fragment><div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart
          layout="vertical"
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20, right: 20, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis type="number" />
          <YAxis dataKey="name" type="category" />
          <Tooltip />
          <Legend />
          {/* <Area dataKey="amt" fill="#8884d8" stroke="#8884d8" /> */}
          {this.getArea(type,"")}
          {/* <Line dataKey="uv" stroke="#ff7300" /> */}
        </ComposedChart>
        </ResponsiveContainer>
        </div>
        <div style={{display:v}}>
        <Row>
          <Col><b>Detalles del Centro</b></Col>
          <Col></Col>
          <Col></Col>
        </Row>
        <br></br>
        {this.getDetails(code,type,color,v)}
        </div>
      </Fragment>
      if(graphic=="3")
      return <Fragment><div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20, right: 80, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis dataKey="name" label={{ value: 'Centros', position: 'insideBottomRight', offset: 0 }} />
          <YAxis label={{ value: 'Index', angle: -90, position: 'insideLeft' }} />
          <Tooltip />
          <Legend />
          {/* <Area dataKey="amt" fill="#8884d8" stroke="#8884d8" /> */}
          {this.getArea(type,"")}
          {/* <Line dataKey="uv" stroke="#ff7300" /> */}
        </ComposedChart>
        </ResponsiveContainer>
        </div>
      </Fragment>
    }

    // getGrafic2(data,type,visible,code){
    //   const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
    //   let max=0;
    //   let min=0;
    //   let v="";
    //   let color="";
    //   if(visible)
    //     v="block"
    //   else
    //     v="none"
    //   let unity="";
    //   if(type=="oxd"){
    //     color=miscolores[0];
    //     max=10.8;
    //     min=4.8;
    //     unity="mg/L";
    //   }
    //   if(type=="temp"){
    //     color=miscolores[5];
    //     max=9.1;
    //     min=6.7;
    //     unity="°C";
    //   }
    //   if(type=="oxs"){
    //     color=miscolores[2];
    //     unity="%";
    //   }
    //   if(type=="sal"){
    //     color=miscolores[1];
    //     unity="PSU";
    //   }
      
    // }

    getGraficAll(data,visible,graphic){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let v="";
      if(visible)
        v="block"
      else
        v="none"

      if(graphic=="1")
        return <div style={{ width: '100%', height: 300, display:v }} >  
        <ResponsiveContainer>
        <BarChart
          width={window.outerWidth-100}
          height={300}
          data={data}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
          >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" label={{ value: 'Centros', position: 'insideBottomRight', offset: 0 }}/>
          <YAxis label={{ value: "Promedios", angle: -90, position: 'insideLeft', textAnchor: 'middle' }} />
          <Tooltip />
          <Legend />
          <Bar dataKey={"oxd"}  fill={miscolores[0]} />
          <Bar dataKey={"temp"} fill={miscolores[5]} />
          <Bar dataKey={"oxs"} fill={miscolores[2]} />
          <Bar dataKey={"sal"} fill={miscolores[1]} />
        </BarChart>
        </ResponsiveContainer>
        </div>
      if(graphic=="3")
      return <div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart width={730} height={250} data={data}
          margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
          <defs>
            <linearGradient id="colorOxd" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[0]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[0]} stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorTemp" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[5]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[5]} stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorOxs" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[2]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[2]} stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorSal" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[1]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[1]} stopOpacity={0}/>
            </linearGradient>
          </defs>
          <XAxis dataKey="name" label={{ value: 'Centros', position: 'insideBottomRight', offset: 0 }}/>
          <YAxis label={{ value: "Promedios", angle: -90, position: 'insideLeft', textAnchor: 'middle' }} />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Area type="monotone" dataKey="oxd" stroke={miscolores[0]} fillOpacity={1} fill={`url(#colorOxd)`} />
          <Area type="monotone" dataKey="temp" stroke={miscolores[5]} fillOpacity={1} fill="url(#colorTemp)" />
          <Area type="monotone" dataKey="oxs" stroke={miscolores[2]}fillOpacity={1} fill="url(#colorOxs)" />
          <Area type="monotone" dataKey="sal" stroke={miscolores[1]} fillOpacity={1} fill="url(#colorSal)" />
        </ComposedChart>
        </ResponsiveContainer>
        </div>
    }

    getBar(type,n){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      if(type=="oxd"){
        return <Bar dataKey={"oxd"+n} barSize={25}  fill={miscolores[0]} />
      }
      if(type=="temp"){
        return <Bar dataKey={"temp"+n} barSize={25} fill={miscolores[5]} />
      }
      if(type=="oxs"){
        return <Bar dataKey={"oxs"+n} barSize={25} fill={miscolores[2]} />
      }
      if(type=="sal"){
        return <Bar dataKey={"sal"+n} barSize={25} fill={miscolores[1]} />
      }
      
    }

    getArea(type,n){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      if(type=="oxd"){
        return <Area dataKey={"oxd"+n} barSize={25}  fill={miscolores[0]} stroke={miscolores[0]} />
      }
      if(type=="temp"){
        return <Area dataKey={"temp"+n} barSize={25} fill={miscolores[5]} stroke={miscolores[5]}/>
      }
      if(type=="oxs"){
        return <Area dataKey={"oxs"+n} barSize={25} fill={miscolores[2]} stroke={miscolores[2]}/>
      }
      if(type=="sal"){
        return <Area dataKey={"sal"+n} barSize={25} fill={miscolores[1]} stroke={miscolores[1]}/>
      }
      
    }
    
    switchActive(act_all,act_oxd,act_temp,act_oxs,act_sal){
      this.setState({act_all,act_oxd, act_temp, act_oxs, act_sal})
    }
    switchActive2(act_oxd2,act_temp2,act_oxs2,act_sal2){
      this.setState({act_oxd2, act_temp2, act_oxs2, act_sal2})
    }

    getChart(){
      try{
        return <Fragment>{this.getGraficAll(empresas.filter(e=>e.active==true).map(e=>{
          return {
           name: e.name, oxd: this.getPromCentro(e.code,"oxd","",""),
           temp: this.getPromCentro(e.code,"temp","",""), 
           oxs: this.getPromCentro(e.code,"oxs","",""), 
           sal: this.getPromCentro(e.code,"sal","","")
          }
        }),this.state.act_all,this.state.TypeGraphic)}
  
        {this.getGrafic(empresas.filter(e=>e.active==true).map(e=>{
          return {name: e.name, oxd: this.getPromCentro(e.code,"oxd","","")}
        }),"oxd",this.state.act_oxd,"",this.state.TypeGraphic)}
        {this.getGrafic(empresas.filter(e=>e.active==true).map(e=>{
          return {name: e.name, temp: this.getPromCentro(e.code,"temp","","")}
        }),"temp",this.state.act_temp,"",this.state.TypeGraphic)}
        {this.getGrafic(empresas.filter(e=>e.active==true).map(e=>{
          return {name: e.name, oxs: this.getPromCentro(e.code,"oxs","","")}
        }),"oxs",this.state.act_oxs,"",this.state.TypeGraphic)}
        {this.getGrafic(empresas.filter(e=>e.active==true).map(e=>{
          return {name: e.name, sal: this.getPromCentro(e.code,"sal","","")}
        }),"sal",this.state.act_sal,"",this.state.TypeGraphic)}</Fragment>
      }catch(e){
        console.log(e)
      }
      
    }

    switchGrafic(active_grafic){
      //let active_grafic="none";
      // if(active=="none"){
      //   active_grafic="block";
      // }else{
      //   active_grafic="none";
      // }
      this.setState({active_grafic})
    }

    inputChangeHandler = (event) => { 
      // console.log(event.target.value);  
      this.setState( { 
          ...this.state,
          [event.target.id]: event.target.value
      } );
  }

  getChartModal(code,type,visible){
    return this.getGrafic(this.state.Mis_Prom.filter(p=>p.centroId==code&&p.name.includes(type)).map(p=>{
      if(type=="oxd")
        return {name: p.name, oxd: p.prom}
      if(type=="temp")
        return {name: p.name, temp: p.prom}
      if(type=="oxs")
        return {name: p.name, oxs: p.prom}
      if(type=="sal")
        return {name: p.name, sal: p.prom}
    }),type,visible,code,this.state.TypeGraphic2)
  }

  getDetails(code,type,color,visible){
    return this.state.Mis_Prom.filter(p=>p.centroId==code&&p.name.includes(type)).map(p=>{
      return (<Fragment><font color={color} style={{display:visible}}>
      <Row >
        <Col><b>{p.name}</b></Col>
      </Row>
      <Row >
        <Col><b>prom.</b> {p.prom}</Col>
        <Col><b>ultimo reg.</b> {p.ultimo_valor}</Col>
        <Col><b>min</b> {p.min}</Col>
        <Col><b>max</b> {p.max}</Col>
      </Row><br></br></font></Fragment>)
    })
  }

  getName(name){
    if(name.includes("OD ")){
      let new_name="";
      if(name.includes("oxd")){
        new_name=name.replace("oxd", "");
        return "Oxd de "+new_name
      }
      if(name.includes("temp")){
        new_name=name.replace("temp", "");
        return "Oxd de "+new_name
      }
      if(name.includes("oxs")){
        new_name=name.replace("oxs", "");
        return "Oxd de "+new_name
      }
      if(name.includes("sal")){
        new_name=name.replace("sal", "");
        return "Oxd de "+new_name
      }
    }else{
      return name
    }
  }

  getColorAlert(c,type,modulo,miscolores,ind){
    let alarma=0;
    let color="";
    if(modulo==900){
      modulo="Ponton"
    }
    this.state.Mis_Prom.filter((l)=>c.code==l.centroId&&l.name.includes(type)&&l.name.includes(modulo)).map((l)=>{
      if(ind!=0){
        if(l.ultimo_valor>=l.alerta_max){
          alarma=1;
        }
        if(l.ultimo_valor<=l.alerta_min){
          alarma=1;
        }
      }else{
        if(l.ultimo_valor<=0){
          alarma=1;
        }
      }
      
    })
    if(alarma==0){
      return color
    }
    else{
      color=miscolores[2];
      return color
    }
  }

  // findEvent(workplaceId){
  //   axios
  //       //.post(API_ROOT+"/"+empresa+"/centros_activos",{
  //       .get(API_ROOT+`/event/workplace/${workplaceId}`)
  //       .then(response => {   
  //           // alert(response.data.statusCode==200);
  //           if(response.data.statusCode==200){
  //               this.setState({Events:response.data.data})
  //           }
            
  //       })
  //       .catch(error => {
  //           console.log(error)
  //       });
  // }

  findOneEvent(workPlace){
    let val=this.state.Events.filter(e=>e.workplaceId==workPlace&&e.active==true)
    let c=val.length;
    return c
  }

  findActiveWorkplace(workPlace){
    let val=this.state.Centros.filter(c=>c._id==workPlace&&c.active==true&&workPlace!="")
    let c=val.length;
    return c
  }

  getAcordion(index,miscolores,collapse,c,modulo){
    // console.log(modulo)
    let m=0;
    let visible="block";
    if(modulo.includes("Ponton")){
      m=900;
    }else{
      m=parseInt(modulo)
    }
    if(modulo.includes("100")){
      m=100;
    }
    if(modulo.includes("200")){
      m=200;
    }
    if(modulo.includes("300")){
      m=300;
    }
    if(modulo.includes("400")){
      m=400;
    }
    if(c.endDate=="" || c.endDate==null){
      visible="none"
    }
    return <Fragment>
    <ListGroupItem style={{borderColor:this.getColorAlert(c,"oxd",m,miscolores),borderWidth:3,display:visible}}  action onClick={this.toggle} data-event={index+100}>
    <UncontrolledTooltip placement="top-end" target={`oxd_divice${c.code}`}>
      {!check?"Promedio de oxígeno disuelto de las sondas instaladas en centro"
      :"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 00:00 hasta la hora actual"}
      
    </UncontrolledTooltip>
      <Row>
      {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c.code&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
        <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice${c.code}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Modulo {modulo} Ox.</Col>
        <Col onClick={this.toggle} data-event={index+m}>{this.getPromCentro(c.code," oxd",miscolores[0],m)}</Col>
        <Col onClick={this.toggle} data-event={index+m}>{this.getPromMinCentro(c.code," oxd",miscolores[0],m)}</Col>
        <Col onClick={this.toggle} data-event={index+m}>{this.getPromMaxCentro(c.code," oxd",miscolores[0],m)}</Col>
      </Row>
    </ListGroupItem>
    <Collapse isOpen={collapse === index+m}>
    {this.state.Mis_Prom.filter((l)=>c.code==l.centroId&&l.name.includes(" oxd")&&l.name.includes(m!=900?m:"Ponton")).map((l)=>{
        let color="";
        if(l.ultimo_valor>=l.alerta_max){
          color=miscolores[2];
        }
        if(l.ultimo_valor<=l.alerta_min){
          color=miscolores[2];
        }
        return (
          <ListGroupItem style={{backgroundColor:color}} action onClick={this.toggle} data-event={index+m}>
          <UncontrolledTooltip placement="top-end" target={`oxd_divice2${l.id}`}>
          {!check?"valor de sonda actual":
          "Promedio de sonda desde las 00:00 hasta hora actual"}
          </UncontrolledTooltip>
          <Row>
            <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${l.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> {this.getName(l.name)}</Col>
            {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],index+100,l.ultimo_valor,l.id)}
          </Row>
          </ListGroupItem>
        )
    })}
    </Collapse>
    </Fragment>
  }

  onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
  }

  filterEvent(workplace){
    let val=this.state.Events.filter(c=>c.workplaceId==workplace)
    this.setState({Centro_Actividad:val.filter(this.onlyUnique)})
  }

  getCardCentros(miscolores,collapse){
    try{
      return empresas.filter(c=>{
          if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
            return c.areaId==Buscar
          }
          if(Buscar==""){
            return c
          }
      }).map((c,index)=>{
        return  <Col md="4" style={{display:this.findActiveWorkplace(c.code)>0?"block":"none"}}>
                    <Card className="main-card mb-5 mt-3" key={index} >
                        <CardHeader className="card-header-tab  ">
                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                        {this.createTooltip(`detalle${c.code}`,"Mas Detalles de Centros")}
                              <a style={{cursor:"pointer"}} onClick={(()=>{
                                sessionStorage.setItem('workplace', JSON.stringify({"id":c.code,"name":c.name,"active":c.active}));
                                this.ingresarCentroUsuario(`${c.code}/${c.name}`);
                                this.setState({redirect:true})
                              })} id={`detalle${c.code}`}>
                              <div class="icon-wrapper rounded-circle" >
                                  <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                  <i class="lnr-apartment text-white"></i>
                              </div>
                              </a>
                              <div class="widget-chart-content font-size-lg font-weight-normal">
                              <p>{c.name}
                              {this.createTooltip(`event_${c.code}`,"Ver Eventos")}
                              <Button id={`event_${c.code}`} color="link" style={{display:this.findOneEvent(c.code)>0?"block":"none",marginLeft:`${90}%`,marginTop:-30}} pill onClick={((e)=>{
                                //alert(c.code)
                                this.filterEvent(c.code)
                                this.toggleModal1(true,c.code,c.name)
                              })}> <i style={{fontSize:20,cursor:"pointer",color:"orange"}} className="pe-7s-bell" ></i></Button>
                              </p>
                              </div>
                        </div>

                        <div className="btn-actions-pane-right text-capitalize">
                            {!this.state.Equipos?this.endDate(c.code):this.endDate2(c.code)}
                        </div>                           
                        </CardHeader>
                        <ListGroup style={{cursor:"pointer"}}>
                        <div style={{display:`${this.state.Abiotic?"block":"none"}`}}>
                              {
                                this.state.Modulos.filter(m=>m.centro==c.name).map((m)=>{
                                  let zonas = m.zonas;
                                  return zonas.filter(this.onlyUnique).map((z)=>{
                                    let modul=z.replace("Modulo ","")
                                    return this.getAcordion(index,miscolores,collapse,c,modul)
                                  })
                                })
                              }
                              {/* {this.getAcordion(index,miscolores,collapse,c,"100")} */}
                              {/* <UncontrolledTooltip placement="top-end" target={`temp_divice${c.code}`}>
                                Promedio de temperatura de las sondas instaladas en centro
                              </UncontrolledTooltip>
                              <UncontrolledTooltip placement="top-end" target={`oxs_divice${c.code}`}>
                                Promedio de oxígeno saturado de las sondas instaladas en centro
                              </UncontrolledTooltip>
                              <UncontrolledTooltip placement="top-end" target={`sal_divice${c.code}`}>
                                Promedio de salinidad de las sondas instaladas en centro
                              </UncontrolledTooltip> */}
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+200}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+200}><i id={`temp_divice${c.code}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Prom. Total Temp.</Col>
                                  <Col onClick={this.toggle} data-event={index+200}>{this.getPromCentro(c.code,"temp",miscolores[5])}</Col>
                                  <Col onClick={this.toggle} data-event={index+200}>{this.getPromMinCentro(c.code,"temp",miscolores[5])}</Col>
                                  <Col onClick={this.toggle} data-event={index+200}>{this.getPromMaxCentro(c.code,"temp",miscolores[5])}</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+200)}>
                              {this.state.Mis_Prom.filter((l)=>c.code==l.centroId&&l.name.includes("temp")).map((l)=>{
                                  return (
                                    <ListGroupItem  action onClick={this.toggle} data-event={index+200}>
                                    <Row>
                                      <Col onClick={this.toggle} data-event={index+200}>Prom. {l.name}</Col>
                                      {this.getColorCol(l.prom,l.min,l.max,"°C",miscolores[5],index+200,l.ultimo_valor)}
                                    </Row>
                                    </ListGroupItem>
                                  )
                              })}
                              </Collapse>

                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+300}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+300}><i id={`oxs_divice${c.code}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Prom. Centro Sat.</Col>
                                  <Col onClick={this.toggle} data-event={index+300}>{this.getPromCentro(c.code,"oxs",miscolores[2])}</Col>
                                  <Col onClick={this.toggle} data-event={index+300}>{this.getPromMinCentro(c.code,"oxs",miscolores[2])}</Col>
                                  <Col onClick={this.toggle} data-event={index+300}>{this.getPromMaxCentro(c.code,"oxs",miscolores[2])}</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+300)}>
                              {this.state.Mis_Prom.filter((l)=>c.code==l.centroId&&l.name.includes("oxs")).map((l)=>{
                                  return (
                                    <ListGroupItem  action onClick={this.toggle} data-event={index+300}>
                                    <Row>
                                      <Col onClick={this.toggle} data-event={index+300}>Prom. {l.name}</Col>
                                      {this.getColorCol(l.prom,l.min,l.max,"%",miscolores[2],index+300,l.ultimo_valor)}
                                    </Row>
                                    </ListGroupItem>
                                  )
                              })}
                              </Collapse>

                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+400}><i id={`sal_divice${c.code}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Prom. Centro Sal.</Col>
                                  <Col onClick={this.toggle} data-event={index+400}>{this.getPromCentro(c.code,"sal",miscolores[1])}</Col>
                                  <Col onClick={this.toggle} data-event={index+400}>{this.getPromMinCentro(c.code,"sal",miscolores[1])}</Col>
                                  <Col onClick={this.toggle} data-event={index+400}>{this.getPromMaxCentro(c.code,"sal",miscolores[1])}</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+400)}>
                              {this.state.Mis_Prom.filter((l)=>c.code==l.centroId&&l.name.includes("sal")).map((l)=>{
                                  return (
                                    <ListGroupItem  action onClick={this.toggle} data-event={index+400}>
                                    <Row>
                                      <Col onClick={this.toggle} data-event={index+400}>Prom. {l.name}</Col>
                                      {this.getColorCol(l.prom,l.min,l.max,"PSU",miscolores[1],index+400,l.ultimo_valor)}
                                    </Row>
                                    </ListGroupItem>
                                  )
                              })}
                              </Collapse> */}
                          </div>
                          <div style={{display:`${this.state.Meteo?"block":"none"}`}}>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Viento</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Velocidad</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Humedad</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                          </div>
                          <div style={{display:`${this.state.Proceso?"block":"none"}`}}>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+600}>Combustible</Col>
                                  <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                  <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                  <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+600)}>
                                <ListGroupItem tag="a" action>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+600}>Flujometro Abducción</Col>
                                    <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                  </Row>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+600}>Flujometro Retorno</Col>
                                    <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                  </Row>
                                </ListGroupItem>
                              </Collapse>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}} tag="a" action>
                                  <Row>
                                    <Col >Consumo calculado</Col>
                                    <Col><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col>min</Col>
                                    <Col>max</Col>
                                  </Row>
                              </ListGroupItem>
                          </div>
                          <div style={{display:`${this.state.Equipos?"block":"none"}`}}>
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+700}>Combustible</Col>
                                  <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                  <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                  <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+700)}>
                                <ListGroupItem tag="a" action>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+700}>Flujometro Abducción</Col>
                                    <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                  </Row>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+700}>Flujometro Retorno</Col>
                                    <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                  </Row>
                                </ListGroupItem>
                              </Collapse> */}
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}} tag="a" action>
                                  <Row>
                                    <Col >Consumo calculado</Col>
                                    <Col><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col>min</Col>
                                    <Col>max</Col>
                                  </Row>
                              </ListGroupItem> */}
                          </div>
                        </ListGroup>
                    </Card>

                </Col>
      })
    }catch(e){
      console.log(e)
      // logout()
    }
  }
  getButtonGraphic(marginLeft){
    return <ButtonGroup style={{marginLeft:`${marginLeft}%`,marginTop:-100}}>
    <UncontrolledTooltip placement="top-end" target="Graficos_Generales_0">
        Ocultar Graficos
      </UncontrolledTooltip>
      <UncontrolledTooltip placement="top-end" target="Graficos_Generales_1">
        Grafico Promedios Generales (Barra)
      </UncontrolledTooltip>
      <UncontrolledTooltip placement="top-end" target="Graficos_Generales_2">
        Grafico Promedios Generales (Lineal)
    </UncontrolledTooltip>
    <Button id="Graficos_Generales_1" active={this.state.act_graph1} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
        this.setState({TypeGraphic:"3"})
        this.switchGrafic("block");
        this.switchActive0(false,true,false)
        // this.switchGrafic(this.state.active_grafic);
      }}>
      <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
    </Button>
    <Button id="Graficos_Generales_0" active={this.state.act_graph0} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
        //this.setState({TypeGraphic:"3"})
        this.switchGrafic("none");
        this.switchActive0(true,false,false)
        // this.switchGrafic(this.state.active_grafic);
      }}>
      -
    </Button>
  <Button id="Graficos_Generales_2" active={this.state.act_graph2} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
        //this.setState({check:true,check2:false})
        this.switchGrafic("block");
        this.setState({TypeGraphic:"1"})
        this.switchActive0(false,false,true)
      }}>
      <i style={{fontSize:20}} className="pe-7s-graph3 btn-icon-wrapper" ></i>
    </Button>
    </ButtonGroup>
  }

  getDetailActivity(activityId){
    let act=this.state.Actividad.filter(a=>a._id==activityId).map((a)=>{
        return a.detail
    })
    //console.log(act)
    return act[0]
  }

  getActivity(){
      axios
      //.post(API_ROOT+"/"+empresa+"/centros_activos",{
      .get(API_ROOT+`/activity/all`)
      .then(response => {   
          // alert(response.data.statusCode==200);
          if(response.data.statusCode==200){
              let actividades=response.data.data;
              let new_actividades=actividades.map((a,i)=>{
                  return {
                      ind:i+1,
                      _id:a._id,
                      detail:a.detail,
                      dateTime:moment(a.dateTime).format("HH:mm DD-MMM"),
                      visible:"block"
                  }
              })
              this.setState({Actividad:new_actividades})
          }
          
      })
      .catch(error => {
          console.log(error)
      });
  }

  switchActive0(act_graph0,act_graph1,act_graph2){
    this.setState({act_graph0,act_graph1,act_graph2})
  }
  
  switchActive_principal(Abiotic,Meteo,Proceso,Equipos){
    this.setState({Abiotic,Meteo,Proceso,Equipos})
  }
  
  switchActiveModal(TypeGraphic2){
    this.setState({TypeGraphic2})
  }

  handleChangeActivity = event => {
      const name = event.target.name;
      const value = event.target.value;
      this.setState({ [name]: value });
  }

    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        if(this.state.redirect2){
          return <Redirect to="/dashboards/configuracion"></Redirect>
        }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
             
         ///***********************************+++ */
        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        {/* {console.log(this.state.Centros2)} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Resumen</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      <BreadcrumbItem active tag="span">Variables</BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{display:active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion</Badge></a></BreadcrumbItem>
                      
                      </Breadcrumb>
                        {/* <Input style={{marginTop:10,width:240}}  type="select" name="select">
                                    <option  selected disabled value='0'>SELECCIONE UNA REGION</option>
                                    {
                                      ubicaciones.map((a)=>{
                                      return <option value={a._id}>{a.name}</option>
                                      })
                                    }
                        </Input> */}
                       
                        <Button active={active_todos} color="primary" outline onClick={((e)=>{
                          Buscar="";
                          active_todos=true;
                          this.inputChangeHandler(e)
                        })} onChange={e => this.inputChangeHandler(e)}>Todos</Button>
                        {
                            areas_centros.map(area=>{
                              return ubicaciones.filter(u=>u._id==area).map((a)=>{
                                  return <Button color="primary" onClick={((e)=>{
                                    Buscar=a._id;
                                    active_todos=false;
                                    this.inputChangeHandler(e)
                                  })} onChange={e => this.inputChangeHandler(e)} outline>{a.name}</Button>
                              })
                            })
                            
                        }
                          {/* <Media query="(min-width: 991px)" render={() =>
                          (
                            this.getButtonGraphic(85)
                          )}
                          />
                          <Media query="(min-width: 500px) and (max-width: 991px)" render={() =>
                          (
                            this.getButtonGraphic(75)
                          )}
                          />
                          <Media query="(max-width:500px)" render={() =>
                          (
                            this.getButtonGraphic(65)
                          )}
                          /> */}
                      <div style={{display:this.state.active_grafic}}>
                        <ButtonGroup size="sm" style={{marginTop:-50}}>
                        <UncontrolledTooltip placement="top-end" target="UncontrolledTooltipExample">
                          Grafico de promedios Generales
                              (Oxd, Temp, Oxs, Sal)
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Oxd">
                          Promedios de Oxigeno Disuelto
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Temp">
                          Promedios de Temperatura
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Oxs">
                          Promedios de Oxigeno Saturado
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Sal">
                          Promedios de Salinidad
                        </UncontrolledTooltip>
                          <Button id="UncontrolledTooltipExample" color="primary"  active={this.state.act_all} onClick={()=>{
                            this.switchActive(true,false,false,false,false)
                          }}>All</Button>
                          <Button id="promedios_Oxd" color="primary" active={this.state.act_oxd} onClick={()=>{
                            this.switchActive(false,true,false,false,false)
                          }}>Oxd</Button>
                          <Button id="promedios_Temp" color="primary" active={this.state.act_temp} onClick={()=>{
                            this.switchActive(false,false,true,false,false)
                          }}>Temp</Button>
                          <Button id="promedios_Oxs" color="primary" active={this.state.act_oxs} onClick={()=>{
                            this.switchActive(false,false,false,true,false)
                          }}>Oxs</Button>
                          <Button id="promedios_Sal" color="primary" active={this.state.act_sal} onClick={()=>{
                            this.switchActive(false,false,false,false,true)
                          }}>Sal</Button>
                        </ButtonGroup>
                        {/* {this.getChart()} */}
                      </div>
                     <Row>
                     
                       {/* {console.log(this.state.Mis_Prom)} */}
                     <Col md="12">
                            <Card className="main-card mb-1 p-0">
                            <CardHeader className="card-header-tab" style={{height:50}}>
                                  <div className="card-header-title font-size-lg text-capitalize font-weight-normal" style={{marginLeft:-20}}>
                                                          <ButtonGroup>
                                                                <Button 
                                                                className="widget-chart widget-chart-hover p-0 p-0  btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Abiotic?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.switchActive_principal(true,false,false,false);
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="pl-0 size_unidad">  </span>
                                                                    <font style={font} color={!this.state.Abiotic?"#6c757d":"#fff"}>&nbsp;&nbsp;Abióticas&nbsp;&nbsp;</font>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} opacity-1 */}
                                                                    </div>
                                                                </Button>  
                                                                <Button 
                                                                className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" disabled style={{backgroundColor:`${!this.state.Meteo?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                  this.switchActive_principal(false,true,false,false);
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    <font style={font} color={!this.state.Meteo?"#6c757d":"#fff"}>&nbsp;&nbsp;Meteorologicas&nbsp;&nbsp;</font>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} opacity-1 */}
                                                                        </div>
                                                                </Button> 
                                                            
                                                                <Button 
                                                                className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" disabled style={{backgroundColor:`${!this.state.Proceso?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                  this.switchActive_principal(false,false,true,false);
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    <font style={font} color={!this.state.Proceso?"#6c757d":"#fff"}>&nbsp;&nbsp;Proceso&nbsp;&nbsp;</font>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} */}
                                                                        </div>
                                                                </Button>  

                                                                <Button 
                                                                className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Equipos?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                  this.switchActive_principal(false,false,false,true);
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    <font style={font} color={!this.state.Equipos?"#6c757d":"#fff"}>&nbsp;&nbsp;Equipos&nbsp;&nbsp;</font>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} */}
                                                                        </div>
                                                                </Button>
                                                          </ButtonGroup>            
                                  </div>
                                  
                                </CardHeader>
                                <CardBody className="p-3">
                                {/* {console.log(this.state.Mis_Prom)}                   */}
                                    <Row>
                                    <Media query="(min-width: 580px)" render={() =>
                                    (
                                    <Fragment ><UncontrolledTooltip style={{display:`${this.state.Abiotic?"block":"none"}`}} placement="right-start" target="check_1">
                                      Ultimo Valor / Prom
                                    </UncontrolledTooltip>
                                    <div style={{marginLeft:`${90}%`,display:`${this.state.Abiotic?"block":"none"}`}}>
                                      <label id="check_1" className="switch">
                                        <input type="checkbox" checked={check} onClick={(()=>{
                                            if(check){check=false;check2=true;}
                                            else{check=true;check2=false;}
                                            })} onChange={e => this.inputChangeHandler(e)} />
                                            <span className="slider round"></span>
                                    </label>
                                    <label style={{marginTop:5,marginLeft:10,fontSize:20}}>{check?"prom":"ult. val."}</label>
                                    </div></Fragment>
                                    )}
                                    />
                                    <Media query="(max-width: 580px)" render={() =>
                                    (
                                      <Fragment><UncontrolledTooltip placement="right-start" target="check_1">
                                      Ultimo Valor / Prom
                                    </UncontrolledTooltip>
                                    <div style={{marginLeft:`${50}%`}}>
                                      <label id="check_1" className="switch">
                                        <input type="checkbox" checked={check} onClick={(()=>{
                                            if(check){check=false;check2=true;}
                                            else{check=true;check2=false;}
                                            })} onChange={e => this.inputChangeHandler(e)} />
                                            <span className="slider round"></span>
                                    </label>
                                    <label style={{marginTop:5,marginLeft:10,fontSize:20}}>{check?"prom ":"ult. val."}</label>
                                    </div></Fragment>
                                    )}
                                    />
                                    
                                          {this.getCardCentros(miscolores,collapse)}
                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                    </Row>

                    {/* <Modal isOpen={this.state.ModalState} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                              <ModalBody >
                              <ButtonGroup size="sm" style={{marginTop:-50}}>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Oxd2">
                                    Promedios de Oxigeno Disuelto
                                  </UncontrolledTooltip>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Temp2">
                                    Promedios de Temperatura
                                  </UncontrolledTooltip>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Oxs2">
                                    Promedios de Oxigeno Saturado
                                  </UncontrolledTooltip>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Sal2">
                                    Promedios de Salinidad
                                  </UncontrolledTooltip>
                                  <Button id="promedios_Oxd2" color="primary" active={this.state.act_oxd2} onClick={()=>{
                                    this.switchActive2(true,false,false,false)
                                  }}>Oxd</Button>
                                  <Button id="promedios_Temp2" color="primary" active={this.state.act_temp2} onClick={()=>{
                                    this.switchActive2(false,true,false,false)
                                  }}>Temp</Button>
                                  <Button id="promedios_Oxs2" color="primary" active={this.state.act_oxs2} onClick={()=>{
                                    this.switchActive2(false,false,true,false)
                                  }}>Oxs</Button>
                                  <Button id="promedios_Sal2" color="primary" active={this.state.act_sal2} onClick={()=>{
                                    this.switchActive2(false,false,false,true)
                                  }}>Sal</Button>
                                </ButtonGroup>
                                {this.getChartModal(code_centro,"oxd",this.state.act_oxd2)}
                                {this.getChartModal(code_centro,"temp",this.state.act_temp2)}
                                {this.getChartModal(code_centro,"oxs",this.state.act_oxs2)}
                                {this.getChartModal(code_centro,"sal",this.state.act_sal2)}
                              </ModalBody>
                              <ModalFooter>
                                <Row style={{width:`${100}%`}}>
                                  <Col xs="6" md="6" lg="6"></Col>
                                  <Col xs="3" md="3" lg="3"></Col>
                                  <Col xs="3" md="3" lg="3">
                                    <ButtonGroup>
                                      <Button color="primary" outline active={this.state.TypeGraphic2=='2.5'?true:false} onClick={(()=>{
                                          this.switchActiveModal('2.5')
                                        })}><i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper"></i></Button>
                                      <Button color="primary" outline active={this.state.TypeGraphic2=='2'?true:false} onClick={(()=>{
                                        this.switchActiveModal('2')
                                      })}><i style={{fontSize:20}}  className="pe-7s-graph3 btn-icon-wrapper"></i></Button>
                                    </ButtonGroup>
                                  </Col>
                                </Row>
                              </ModalFooter>
                          </BlockUi>
                      </Modal> */}
                      <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={ModalActive} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                                <ButtonGroup>
                                                <Input placeholder="Evento..." value={Buscar2} onChange={((e)=>{
                                                  Buscar2=e.target.value;
                                                  this.handleChangeActivity(e)
                                                })}></Input>
                                                <Button color="secondary" disabled>Buscar</Button>
                                                </ButtonGroup>
                                                
                                                <ReactTable                                                           
                                                data={_.orderBy(this.state.Centro_Actividad,["dateTime"],["desc"]).filter((data)=>{
                                                  if(Buscar2!=""){
                                                      return data.detail.toLocaleUpperCase().includes(Buscar2.toLocaleUpperCase())
                                                  }else{
                                                    return data
                                                  }
                                                }).map((c,i)=>{
                                                  return {
                                                      ind:i+1,
                                                      _id:c._id,
                                                      detail:c.detail,
                                                      type:this.getDetailActivity(c.activityId),
                                                      dateTime:moment(c.dateTime).format("HH:mm DD-MMM"),
                                                      workplaceId:c.workplaceId,
                                                      activityId:c.activityId,
                                                      active:c.active,
                                                  }
                                              })}
                                                            
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={10}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                          Header: "asunto",
                                                          width: 350,
                                                          Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={"info"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.type}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "detalle",
                                                        accessor: "detail",                                                              
                                                        width: `${78}%`
                                                        },
                                                        {
                                                        Header: "estado",                                                             
                                                        width: 150,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={row._original.active?"warning":"success"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.active?"activo":"finalizado"}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        // {
                                                        // Header: "Accion",
                                                        // id: "id",
                                                        // width: 100,
                                                        // Cell: ({ row }) => (
                                                        //     <Fragment>
                                                        //         <Badge color="warning" style={{marginLeft:0}} onClick={((e)=>{
                                                        //             //this.updateEvent(row._original.id,true,row._original.workplaceId)
                                                        //         })} pill>
                                                        //         <i class="pe-7s-attention" 
                                                        //         style={{display:active_config,cursor:"pointer",fontSize:20}}></i></Badge>&nbsp;
                                                        //         <Badge color="success" pill onClick={((e)=>{
                                                        //             //this.updateEvent(row._original.id,false,row._original.workplaceId)
                                                        //             //this.switchModal("1")
                                                        //         })}><i style={{fontSize:20,cursor:"pointer"}} className="pe-7s-check                                                                " ></i></Badge>
                                                        //     </Fragment>
                                                        //     )
                                                        // },
                                                    ]                                                             
                                                }
                                                 
                                                className="-striped -highlight"
                                                />
                                          </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({
  setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Resumen);
