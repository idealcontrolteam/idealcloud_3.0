import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';
import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT } from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import {
  setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../../reducers/Session';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import { split } from 'lodash';
import Rupanquito from '../../Piscicultura/ComponentPanelGeneral/Ignao/PanelGeneral'
import Tendencia_Rupanquito from '../../Piscicultura/Tendencia/Tendencia'
import Tendencia from '../../Tendencia/ComponentTendencia/Tendencia/Tendencia'
import Diagnostico from './ComponetDiagnostico/Diagnostico';
// import * as Push from 'push.js'; // if using ES6 


//sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let load=false;
let icon_up="lnr-arrow-up";
let icon_right="lnr-arrow-right";
let icon_down="lnr-arrow-down";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
//empresas=empresas.filter(e=>e.active==true).map(e=>e);
let ubicaciones=JSON.parse(sessionStorage.getItem("Areas"));
let c=0;
let areas_centros=[];
let type_view="";
const publicVapidKey = process.env.REACT_APP_KEY_PUBLIC.toString();
// console.log(publicVapidKey)
// console.log(process.env.REACT_APP_CODE)

try{
  empresas.map(e=>{return e.areaId});
}catch(e){
  //console.log("areas")
  logout()
}
moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);


let title_centro="";
let _id_centro="";
let tag_=[];
let title_disp="";
let Buscar="";
let Buscar2="";
let active_todos=true;
let check=false;
let check2=false;
let active_config="none";
let active_config2="none";
let ModalActive=false;
let ModalActive2=false;
let i=0;

class Resumen2 extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Mis_Sondas2:[],
          Modulos:[],
          isLoading:true,
          Mis_Prom:[],
          Mis_Prom2:[],
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Abiotic:true,
          Meteo:false,
          Proceso:false,
          Equipos:false,
          Diagnostico:false,
          Control:false,
          Piscicultura:false,
          card:[1,2,3,4,5,6,7,8],
          redirect:false,
          redirect2:false,
          Centros:[],
          act_graph0:true,
          act_graph1:false,
          act_graph2:false,
          act_all:true,
          act_oxd:false,
          act_temp:false,
          act_oxs:false,
          act_sal:false,
          act_oxd2:true,
          act_temp2:false,
          act_oxs2:false,
          act_sal2:false,
          active_grafic:"none",
          ModalState:false,
          TypeGraphic:"1",
          TypeGraphic2:"2",
          Events:[],
          Centro_Actividad:[],
          txtCenActividad:"",
          EventCentros:[],
          Actividad:[],
          Centros2:[],
          tags_original:[],
          Companys:[],
          selectCompany:"",
        };     
        this.toggle = this.toggle.bind(this);
        //this.getDataCentros = this.getDataCentros.bind(this);
        //this.intervalIdTag2 = this.getCentros.bind(this);
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);      
        this.toggleModal1 = this.toggleModal1.bind(this);
        this.timerRef=React.createRef();
    }

  
    componentDidMount = () => {
        if ('serviceWorker' in navigator) {
          // console.log('Registering service worker');
          this.run().catch(error => console.error(error));
        }
        this.getCompanys()
        window.scroll(0, 0);
        if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c2"){
          active_config="block";
        }else if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c3"){
          active_config2="block"
        }
        // this.getUbicacion();
        //this.setState({isLoading: true})
        //this.setState({isLoading: false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        //this.setState({ isLoading: true });
        //this.intervalIdTag1 = setInterval(() => this.updatePromedios(),210000);
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
            //this.getDataCentros();
        }
        this.timerRef.current = setInterval(this.getCentros.bind(this),210000);
        this.getCentros();
        this.getActivity();
      }

      componentWillUnmount = () => {
        clearInterval(this.getCentros);
        clearInterval(this.getActivity);
        clearInterval(this.timerRef.current);
     }  

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        //this.getSondas(centro[0].toString());
        if(c!=0)
          this.setState({redirect:true})
        c++;
      }

      ingresarCentroUsuario = (centro) =>{
          let {setCentroUsuario} = this.props;
          setCentroUsuario(centro);
      }

      async run() {
        // console.log('Registering service worker');
        let raiz="";
        API_ROOT.includes('localhost')?
        raiz="/":raiz="/idealcloud2/"
        const registration = await navigator.serviceWorker.
          register(raiz+'worker.js', {scope: raiz});
        // console.log('Registered service worker');

        await navigator.serviceWorker.ready;
      
        // console.log('Registering push');
        const subscription = await registration.pushManager.
          subscribe({
            userVisibleOnly: true,
            applicationServerKey: this.urlBase64ToUint8Array(publicVapidKey)
          });
        // console.log('Registered push');
      
        // console.log('Sending push');
        // await fetch(API_ROOT+'/workplace/subscribe', {
        //   method: 'POST',
        //   body: JSON.stringify(subscription),
        //   headers: {
        //     'content-type': 'application/json'
        //   }
        // });
        //let token="tokenFalso";
        if(empresa.includes('idealcontrol')){
          axios
          .post(API_ROOT+"/workPlace/subscribe",subscription)
          .then(response => {   
              if(response.data.statusCode==200){

              }
          })
          .catch(error => {
          });
          // console.log('Sent push');
        }
        
      }

      urlBase64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
          .replace(/\-/g, '+')
          .replace(/_/g, '/');
      
        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);
      
        for (let i = 0; i < rawData.length; ++i) {
          outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
      }

      toggle(e) {
        let event = e.target.dataset.event;
        this.setState({ collapse: this.state.collapse === Number(event) ? 0 : Number(event) });
      }

      divideNameZone(nameAddress){
        // console.log(nameAddress)
        let namezone0=nameAddress.split('_')
        // console.log(namezone0.)
        let namezone1=String(namezone0[1]).split(' ')
        
        return String(namezone1[0])
        
      }

      getPriority(status){
        if(status){
          if(status=="sin datos"){
            return 1
          }else if(status=="pegados"){
            return 2
          }else if(status=="en cero"){
            return 3
          }
        }else{
          return 4
        }
      }

      newLocations(tags,w){
        return tags.filter(t=>t.active==true).map(t=>{
          let promedio={
            id:t._id,
            name:t.nameAddress,
            locationId:t.locationId,
            prom:t.prom,
            max:t.max,
            min:t.min,
            centroId:w._id,
            ultimo_valor:t.lastValueWrite,
            ultima_fecha:t.dateTimeLastValue,
            alerta_max:t.alertMax,
            alerta_min:t.alertMin,
            zoneId:t.zoneId,
            status:t.locationId.status?t.locationId.status:'ok',
            position:this.getPriority(t.locationId.status)
          }
          return promedio
        })
      }

      getCentros(){
          this.setState({Mis_Prom2:[]})
          let token=sessionStorage.getItem("token_cloud")
          try{
            this.tagservices.getUserToken(token).then(respuesta=>{
              let centros=respuesta.centros+respuesta.control;

              //let centros=respuesta.control;
              // if(this.state.Control){
              //   let control=respuesta.control;
              //   centros=""+control;
              // }
              this.tagservices.getWorkplaceUser(centros).then(workplace=>{
                let workplace_activos=workplace.filter(w=>w.active==true&&!w.code.includes('cnt_'));
                //console.log(workplace_activos)
                if(workplace_activos.length==0 && respuesta.control.length>0){
                  this.switchActive_principal(false,false,false,false,true,false);
                }
                try{
                  areas_centros=workplace.map(e=>{return e.areaId});
                  areas_centros=areas_centros.filter(this.onlyUnique)
                  if(this.state.Mis_Prom.length==0){
                    workplace.filter(w=>w.active==true).map((w)=>{
                      let Modulos=[];
                      this.tagservices.getTagsWorkplace(w._id,"get").then(respuesta=>{
                        let tags=respuesta.data;
                        tags=tags.filter(t=>!t.nameAddress.includes('Salinidad')&&t.active==true)
                        let locations=[];
                        let zones0=[]
                        let zones=[];
                        locations=this.newLocations(tags,w);
                        tags.map(t=>{
                          zones0.push(t.zoneId+this.divideNameZone(t.nameAddress))
                        })
                        zones0=zones0.filter(this.onlyUnique)
                        zones0.map(z=>{
                          let zone=tags.filter(t=>t.zoneId+this.divideNameZone(t.nameAddress)==z)[0]
                          zones.push(this.divideNameZone(zone.nameAddress))
                        })
                        zones=zones.filter(this.onlyUnique)
                        Modulos.push({centro:w.name,zonas:zones})
                        this.setState({Mis_Prom:this.state.Mis_Prom.concat(locations),
                                       Mis_Prom2:this.state.Mis_Prom2.concat(locations),
                                       Modulos:this.state.Modulos.concat(Modulos),
                                       tags_original:this.state.tags_original.concat(tags)
                                      })
                        // console.log(w.name)
                        // console.log(locations)
                        //console.log(tags)
                      }).catch(e=>{
                        //console.log(object)
                      })
                    })
                    this.setState({Centros:workplace,Centros2:_.orderBy(workplace,["name"],["asc"]),isLoading:false})
                  }else{
                    workplace.filter(w=>w.active==true).map((w)=>{
                      let Modulos=[]
                      this.tagservices.getTagsWorkplace(w._id,"get").then(respuesta=>{
                        let tags=respuesta.data;
                        tags=tags.filter(t=>!t.nameAddress.includes('Salinidad')&&t.active==true)
                        let locations=[];
                        let zones0=[]
                        let zones=[];
                        locations=this.newLocations(tags,w);
                        tags.map(t=>{
                          zones0.push(t.zoneId+this.divideNameZone(t.nameAddress))
                        })
                        zones0=zones0.filter(this.onlyUnique)
                        zones0.map(z=>{
                          let zone=tags.filter(t=>t.zoneId+this.divideNameZone(t.nameAddress)==z)[0]
                          zones.push(this.divideNameZone(zone.nameAddress))
                        })
                        zones=zones.filter(this.onlyUnique)
                        Modulos.push({centro:w.name,zonas:zones})
                        this.setState({Mis_Prom2:this.state.Mis_Prom2.concat(locations)})
                        // console.log(w.name)
                        // console.log(locations)
                        //console.log(tags)
                      }).catch(e=>{
                        //console.log(object)
                      })
                    })
                    this.setState({Centros2:_.orderBy(workplace,["name"],["asc"])})
                  }
                }
                catch(e){console.log(e)}
                
                
                
              })
            }).catch(e=>e)
          }catch(e){
            //console.log(e)
          }
          
      }

      

      toggleModal1(ModalState,_id,title) {
        if(!ModalState){
          Buscar2=""
          this.setState({ModalState,Centro_Actividad:[]})
        }
        ModalActive=ModalState;
        this.setState({ModalState})
        _id_centro=_id;
        title_centro=title;
      }
      toggleModal2(ModalState,_id,title,tipo,tag) {
        tag_=[];
        if(!ModalState){
          Buscar2=""
          this.setState({ModalState,Centro_Actividad:[]})
        }
        ModalActive2=ModalState;
        this.setState({ModalState})
        _id_centro=_id;
        title_centro=title;
        type_view=tipo;
        tag_=tag;
      }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }

    getPromCentro(id,type,color,modulo){
      // console.log(modulo)
      if(modulo==900){
        modulo="Ponton"
      }
      if(modulo==1000){
        modulo="MODULOS"
      }
      let unity="";
      if(type=="oxd")
        type=" oxd";
        unity="mg/L";
      if(type=="temp")
        unity="°C";
      if(type=="oxs")
        unity="%";
      if(type=="sal")
        unity="PSU";
      let measurements=[];
      // console.log(this.state.Mis_Prom2)
      modulo!=""?measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p):
      measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)).map(p=>p)
      measurements=measurements.filter(m=>!m.name.includes('VoltajeR'))
      // console.log("medio"+JSON.stringify(measurements))
      let prom=((measurements.reduce((a, b) => +a + +b.prom, 0)/measurements.length)).toFixed(1);
      let ultimo_valor=((measurements.reduce((a, b) => +a + +b.ultimo_valor, 0)/measurements.length)).toFixed(1);
      // console.log(measurements)
      let icon="";
      let mensaje="";
      if(ultimo_valor>Math.round(prom)){
        icon=icon_up;
        mensaje="Tendencia sobre el promedio"
      }if(ultimo_valor=>prom && ultimo_valor<=Math.round(prom)){
        icon=icon_right;
        mensaje="Tendencia entre el promedio"
      }if(ultimo_valor<prom && ultimo_valor<Math.round(prom)){
        icon=icon_down;
        mensaje="Tendencia bajo el promedio"
      }
     if(color!=""){
      return <Fragment>
        {/* <UncontrolledTooltip placement="top-end" target={`oxd_tendencia${id}`}>
          {mensaje}
        </UncontrolledTooltip> */}
        <font color={color}><span id={`oxd_tendencia${id}`} className={icon}></span><b> {check==true?prom:ultimo_valor} {unity}</b></font>
        </Fragment>
    }
    else{
      return check==true?prom:ultimo_valor
    }
  }

    getPromMaxCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      if(modulo==1000){
        modulo="MODULOS"
      }
      let measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p);
      if(color!="")
        return <font color={color}><b>max: {((measurements.reduce((a, b) => +a + +b.max, 0)/measurements.length)).toFixed(1)}</b></font>
      else
        return ((measurements.reduce((a, b) => +a + +b.max, 0)/measurements.length)).toFixed(1)
    }

    getPromMinCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      if(modulo==1000){
        modulo="MODULOS"
      }
      let measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p);
      if(color!="")
        return <font color={color}><b>min: {((measurements.reduce((a, b) => +a + +b.min, 0)/measurements.length)).toFixed(1)}</b></font>
      else
        return ((measurements.reduce((a, b) => +a + +b.min, 0)/measurements.length)).toFixed(1)
    }
    getColorCol(prom,min,max,unity,color,index,ultimo_valor,id){
      let icon="";
      let mensaje="";
      if(ultimo_valor>Math.round(prom)){
        icon=icon_up;
        mensaje="Tendencia sobre el promedio"
      }if(ultimo_valor=>prom && ultimo_valor<=Math.round(prom)){
        icon=icon_right;
        mensaje="Tendencia entre el promedio"
      }if(ultimo_valor<prom && ultimo_valor<Math.round(prom)){
        icon=icon_down;
        mensaje="Tendencia bajo el promedio"
      }
      return (
      <Fragment>
        <UncontrolledTooltip placement="top-end" target={`oxd_tendencia2${id}`}>
          {mensaje}
        </UncontrolledTooltip>
        <Col id="PopoverFocus"><font color={color}><span id={`oxd_tendencia2${id}`} className={icon}></span> {check==true?prom:ultimo_valor} {unity}</font></Col>
        <Col id="PopoverFocus"><font color={color}>min {min}</font></Col>
        <Col id="PopoverFocus"><font color={color}>max {max}</font></Col>
        <Col id="PopoverFocus"><Badge color={"primary"} className={"zoom"} style={{width:`${80}%`}}><font size={4.5}><i class="pe-7s-graph2"></i></font></Badge></Col>
      </Fragment>
      )
    }
    getColorColControl(prom,unity,color,index,ultimo_valor,id){
      let icon="";
      let mensaje="";
      if(ultimo_valor>Math.round(prom)){
        icon=icon_up;
        mensaje="Tendencia sobre el promedio"
      }if(ultimo_valor=>prom && ultimo_valor<=Math.round(prom)){
        icon=icon_right;
        mensaje="Tendencia entre el promedio"
      }if(ultimo_valor<prom && ultimo_valor<Math.round(prom)){
        icon=icon_down;
        mensaje="Tendencia bajo el promedio"
      }
      return (
      <Fragment>
        <UncontrolledTooltip placement="top-end" target={`oxd_tendencia2${id}`}>
          {mensaje}
        </UncontrolledTooltip>
        <Col id="PopoverFocus"><font color={color}><span id={`oxd_tendencia2${id}`} className={icon}></span> {check==true?prom:ultimo_valor}</font></Col>
      </Fragment>
      )
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    endDate(_id){
      let data=this.state.Centros2.filter(e=>e._id==_id && e.endDate!="").map((e)=>e.endDate)
      let data2=this.state.Centros2.filter(e=>{return e._id==_id}).map(e=>{
        // fecha.setHours(fecha.getHours()-1);
        return e.endDate
      })
      //alert(data2[0])
      if(data2[0]!=null && data[0]!=null){
        return moment(data2[0]).format("HH:mm DD-MMM")
      }else{
        return <Badge color="secondary" pill>sin registros actualizados</Badge>
      }
    }

    endDate2(_id){
      let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let color="", color2="";
      let data=this.state.Centros2.filter(e=>e._id==_id && e.endDate!="").map((e)=>{
        color=this.getColorAlert(e,"oxd",100,miscolores,0);
        color2=this.getColorAlert(e,"oxd",200,miscolores,0);
        return e.endDate
      })
      let data2=this.state.Centros2.filter(e=>e._id==_id).map(e=>{
        var fecha=new Date(e.endDate);
        // fecha.setHours(fecha.getHours()-1);
        return fecha
      })
      let myId="status"+_id;
      if(data[0]!=null && data2[0]!=null){
        let fecha=new Date();
        let f1=data2[0];
        let f2=new Date(fecha.setHours(fecha.getHours()-1));
        let f3=new Date(fecha.setHours(fecha.getHours()-12));
        if(color!="" || color2!=""){
          //alert(color);
          return <Fragment>{this.createTooltip(myId,"Registros en 0")}
           <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        }
        else if(f1>=f2){
          //alert(f2)
          return <Fragment>{this.createTooltip(myId,"En linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(f1>=f3){
          return <Fragment>{this.createTooltip(myId,"En linea en las 12hrs.")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-dark">Dark</div></Fragment>
        }
        // else if(f1<moment(f2).format("HH:mm DD-MMM") && f1>=moment(f3).format("HH:mm DD-MMM")){
        //   return <Fragment>{this.createTooltip(myId,"En linea dentro de las 24 hrs")}
        //   <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        // }
        else{
          return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
        }
      }else{
        return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
        <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
      }
    }

    getGrafic(data,type,visible,_id,graphic){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let max=0;
      let min=0;
      let v="";
      let color="";
      if(visible)
        v="block"
      else
        v="none"
      let unity="";
      if(type=="oxd"){
        color=miscolores[0];
        max=10.8;
        min=4.8;
        unity="mg/L";
      }
      if(type=="temp"){
        color=miscolores[5];
        max=9.1;
        min=6.7;
        unity="°C";
      }
      if(type=="oxs"){
        color=miscolores[2];
        unity="%";
      }
      if(type=="sal"){
        color=miscolores[1];
        unity="PSU";
      }
      if(graphic=="1")
        return <div style={{ width: '100%', height: 300, display:v }} >  
        <ResponsiveContainer>
        <BarChart
          width={window.outerWidth-100}
          height={300}
          data={data}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
          >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis label={{ value: unity, angle: -90, position: 'insideLeft', textAnchor: 'middle' }} />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="oxd" stroke="#82ca9d" />
          {this.getBar(type,"")}
        </BarChart>
        </ResponsiveContainer>
        </div>
      if(graphic=="2")
      return <Fragment><div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart
          layout="vertical"
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20, right: 20, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis type="number" />
          <YAxis dataKey="name" type="category" />
          <Tooltip />
          <Legend />
          {/* <Area dataKey="amt" fill="#8884d8" stroke="#8884d8" /> */}
          {this.getBar(type,"")}
          {/* <Line dataKey="uv" stroke="#ff7300" /> */}
        </ComposedChart>
        </ResponsiveContainer>
        </div>
        <div style={{display:v}}>
        <Row>
          <Col><b>Detalles del Centro</b></Col>
          <Col></Col>
          <Col></Col>
        </Row>
        <br></br>
        {this.getDetails(_id,type,color,v)}
        </div>
      </Fragment>
      if(graphic=="2.5")
      return <Fragment><div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart
          layout="vertical"
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20, right: 20, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis type="number" />
          <YAxis dataKey="name" type="category" />
          <Tooltip />
          <Legend />
          {/* <Area dataKey="amt" fill="#8884d8" stroke="#8884d8" /> */}
          {this.getArea(type,"")}
          {/* <Line dataKey="uv" stroke="#ff7300" /> */}
        </ComposedChart>
        </ResponsiveContainer>
        </div>
        <div style={{display:v}}>
        <Row>
          <Col><b>Detalles del Centro</b></Col>
          <Col></Col>
          <Col></Col>
        </Row>
        <br></br>
        {this.getDetails(_id,type,color,v)}
        </div>
      </Fragment>
      if(graphic=="3")
      return <Fragment><div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20, right: 80, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis dataKey="name" label={{ value: 'Centros', position: 'insideBottomRight', offset: 0 }} />
          <YAxis label={{ value: 'Index', angle: -90, position: 'insideLeft' }} />
          <Tooltip />
          <Legend />
          {/* <Area dataKey="amt" fill="#8884d8" stroke="#8884d8" /> */}
          {this.getArea(type,"")}
          {/* <Line dataKey="uv" stroke="#ff7300" /> */}
        </ComposedChart>
        </ResponsiveContainer>
        </div>
      </Fragment>
    }

    // getGrafic2(data,type,visible,_id){
    //   const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
    //   let max=0;
    //   let min=0;
    //   let v="";
    //   let color="";
    //   if(visible)
    //     v="block"
    //   else
    //     v="none"
    //   let unity="";
    //   if(type=="oxd"){
    //     color=miscolores[0];
    //     max=10.8;
    //     min=4.8;
    //     unity="mg/L";
    //   }
    //   if(type=="temp"){
    //     color=miscolores[5];
    //     max=9.1;
    //     min=6.7;
    //     unity="°C";
    //   }
    //   if(type=="oxs"){
    //     color=miscolores[2];
    //     unity="%";
    //   }
    //   if(type=="sal"){
    //     color=miscolores[1];
    //     unity="PSU";
    //   }
      
    // }

    getGraficAll(data,visible,graphic){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let v="";
      if(visible)
        v="block"
      else
        v="none"

      if(graphic=="1")
        return <div style={{ width: '100%', height: 300, display:v }} >  
        <ResponsiveContainer>
        <BarChart
          width={window.outerWidth-100}
          height={300}
          data={data}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
          >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" label={{ value: 'Centros', position: 'insideBottomRight', offset: 0 }}/>
          <YAxis label={{ value: "Promedios", angle: -90, position: 'insideLeft', textAnchor: 'middle' }} />
          <Tooltip />
          <Legend />
          <Bar dataKey={"oxd"}  fill={miscolores[0]} />
          <Bar dataKey={"temp"} fill={miscolores[5]} />
          <Bar dataKey={"oxs"} fill={miscolores[2]} />
          <Bar dataKey={"sal"} fill={miscolores[1]} />
        </BarChart>
        </ResponsiveContainer>
        </div>
      if(graphic=="3")
      return <div style={{ width: '95%', height: 300, display:v }}> 
        <ResponsiveContainer>
        <ComposedChart width={730} height={250} data={data}
          margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
          <defs>
            <linearGradient id="colorOxd" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[0]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[0]} stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorTemp" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[5]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[5]} stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorOxs" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[2]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[2]} stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorSal" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor={miscolores[1]} stopOpacity={0.8}/>
              <stop offset="95%" stopColor={miscolores[1]} stopOpacity={0}/>
            </linearGradient>
          </defs>
          <XAxis dataKey="name" label={{ value: 'Centros', position: 'insideBottomRight', offset: 0 }}/>
          <YAxis label={{ value: "Promedios", angle: -90, position: 'insideLeft', textAnchor: 'middle' }} />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Area type="monotone" dataKey="oxd" stroke={miscolores[0]} fillOpacity={1} fill={`url(#colorOxd)`} />
          <Area type="monotone" dataKey="temp" stroke={miscolores[5]} fillOpacity={1} fill="url(#colorTemp)" />
          <Area type="monotone" dataKey="oxs" stroke={miscolores[2]}fillOpacity={1} fill="url(#colorOxs)" />
          <Area type="monotone" dataKey="sal" stroke={miscolores[1]} fillOpacity={1} fill="url(#colorSal)" />
        </ComposedChart>
        </ResponsiveContainer>
        </div>
    }

    getBar(type,n){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      if(type=="oxd"){
        return <Bar dataKey={"oxd"+n} barSize={25}  fill={miscolores[0]} />
      }
      if(type=="temp"){
        return <Bar dataKey={"temp"+n} barSize={25} fill={miscolores[5]} />
      }
      if(type=="oxs"){
        return <Bar dataKey={"oxs"+n} barSize={25} fill={miscolores[2]} />
      }
      if(type=="sal"){
        return <Bar dataKey={"sal"+n} barSize={25} fill={miscolores[1]} />
      }
      
    }

    getArea(type,n){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      if(type=="oxd"){
        return <Area dataKey={"oxd"+n} barSize={25}  fill={miscolores[0]} stroke={miscolores[0]} />
      }
      if(type=="temp"){
        return <Area dataKey={"temp"+n} barSize={25} fill={miscolores[5]} stroke={miscolores[5]}/>
      }
      if(type=="oxs"){
        return <Area dataKey={"oxs"+n} barSize={25} fill={miscolores[2]} stroke={miscolores[2]}/>
      }
      if(type=="sal"){
        return <Area dataKey={"sal"+n} barSize={25} fill={miscolores[1]} stroke={miscolores[1]}/>
      }
      
    }
    
    switchActive(act_all,act_oxd,act_temp,act_oxs,act_sal){
      this.setState({act_all,act_oxd, act_temp, act_oxs, act_sal})
    }
    switchActive2(act_oxd2,act_temp2,act_oxs2,act_sal2){
      this.setState({act_oxd2, act_temp2, act_oxs2, act_sal2})
    }

    getChart(){
      try{
        return <Fragment>{this.getGraficAll(this.state.Centros2.filter(e=>e.active==true).map(e=>{
          return {
           name: e.name, oxd: this.getPromCentro(e._id,"oxd","",""),
           temp: this.getPromCentro(e._id,"temp","",""), 
           oxs: this.getPromCentro(e._id,"oxs","",""), 
           sal: this.getPromCentro(e._id,"sal","","")
          }
        }),this.state.act_all,this.state.TypeGraphic)}
  
        {this.getGrafic(this.state.Centros2.filter(e=>e.active==true).map(e=>{
          return {name: e.name, oxd: this.getPromCentro(e._id,"oxd","","")}
        }),"oxd",this.state.act_oxd,"",this.state.TypeGraphic)}
        {this.getGrafic(this.state.Centros2.filter(e=>e.active==true).map(e=>{
          return {name: e.name, temp: this.getPromCentro(e._id,"temp","","")}
        }),"temp",this.state.act_temp,"",this.state.TypeGraphic)}
        {this.getGrafic(this.state.Centros2.filter(e=>e.active==true).map(e=>{
          return {name: e.name, oxs: this.getPromCentro(e._id,"oxs","","")}
        }),"oxs",this.state.act_oxs,"",this.state.TypeGraphic)}
        {this.getGrafic(this.state.Centros2.filter(e=>e.active==true).map(e=>{
          return {name: e.name, sal: this.getPromCentro(e._id,"sal","","")}
        }),"sal",this.state.act_sal,"",this.state.TypeGraphic)}</Fragment>
      }catch(e){
        //console.log(e)
      }
      
    }

    switchGrafic(active_grafic){
      //let active_grafic="none";
      // if(active=="none"){
      //   active_grafic="block";
      // }else{
      //   active_grafic="none";
      // }
      this.setState({active_grafic})
    }

    inputChangeHandler = (event) => { 
      // console.log(event.target.value);  
      this.setState( { 
          ...this.state,
          [event.target.id]: event.target.value
      } );
  }

  getChartModal(_id,type,visible){
    return this.getGrafic(this.state.Mis_Prom2.filter(p=>p.centroId==_id&&p.name.includes(type)).map(p=>{
      if(type=="oxd")
        return {name: p.name, oxd: p.prom}
      if(type=="temp")
        return {name: p.name, temp: p.prom}
      if(type=="oxs")
        return {name: p.name, oxs: p.prom}
      if(type=="sal")
        return {name: p.name, sal: p.prom}
    }),type,visible,_id,this.state.TypeGraphic2)
  }

  getDetails(_id,type,color,visible){
    return this.state.Mis_Prom2.filter(p=>p.centroId==_id&&p.name.includes(type)).map(p=>{
      return (<Fragment><font color={color} style={{display:visible}}>
      <Row >
        <Col><b>{p.name}</b></Col>
      </Row>
      <Row >
        <Col><b>prom.</b> {p.prom}</Col>
        <Col><b>ultimo reg.</b> {p.ultimo_valor}</Col>
        <Col><b>min</b> {p.min}</Col>
        <Col><b>max</b> {p.max}</Col>
      </Row><br></br></font></Fragment>)
    })
  }

  getName(name){
    if(name.includes("OD ")){
      let new_name="";
      if(name.includes("oxd")){
        new_name=name.replace("oxd", "");
        return "Oxd de "+new_name
      }
      if(name.includes("temp")){
        new_name=name.replace("temp", "");
        return "Oxd de "+new_name
      }
      if(name.includes("oxs")){
        new_name=name.replace("oxs", "");
        return "Oxd de "+new_name
      }
      if(name.includes("sal")){
        new_name=name.replace("sal", "");
        return "Oxd de "+new_name
      }
    }else{
      return name
    }
  }

  getColorAlert(c,type,modulo,miscolores,ind){
    let alarma=0;
    let color="";
    if(modulo==900){
      modulo="Ponton"
    }
    if(modulo==1000){
      modulo="MODULOS"
    }
    this.state.Mis_Prom2.filter((l)=>c._id==l.centroId&&l.name.includes(type)&&l.name.includes(modulo)&&!l.name.includes("VoltajeR")).map((l)=>{
      //console.log(`name:${l.name} val:${l.ultimo_valor} max:${l.alerta_max} min:${l.alerta_min}`)

      if(ind!=0){
        if(l.ultimo_valor>=l.alerta_max){
          alarma=1;
        }
        if(l.ultimo_valor<=l.alerta_min){
          alarma=1;
        }
      }else{
        if(l.ultimo_valor<=0){
          alarma=1;
        }
      }
      
    })
    if(alarma==0){
      return color
    }
    else{
      color=miscolores[2];
      return color
    }
  }

  findOneEvent(workPlace){
    let val=this.state.Events.filter(e=>e.workplaceId==workPlace&&e.active==true)
    let c=val.length;
    return c
  }

  findActiveWorkplace(workPlace){
    let val=this.state.Centros2.filter(c=>c._id==workPlace&&c.active==true&&workPlace!="")
    let c=val.length;
    return c
  }

  getPar(name){
    let separador=name.split(' ');
    let separador2=String(separador[1]).split('_');
    let num=parseInt(separador2[0]);
    if(num%2==0){
      return "par"
    }else{
      return "impar"
    }
  }

  getColumParInp(l,i,largo,index,m,miscolores,data_par,data_inpar,name,data,superior,inferior,setpoint,c){
    //console.log(superior)
    //if(largo%2==0){
      if(name.includes('Yaguepe')){
        return <Row>
          <Col>
            {this.generateInpar(l,i,largo,index,m,miscolores,data,superior,inferior,setpoint,c)}
          </Col>
        </Row>
      }else{
        return <Row>
          <Col>
            {this.generatePar(l,i,largo,index,m,miscolores,data_inpar,superior,inferior,setpoint,c)}
          </Col>
          <Col>
            {this.generatePar(l,i,largo,index,m,miscolores,data_par,superior,inferior,setpoint,c)}
          </Col>
        </Row>
      }
      

  }

  getNameCentro(name){
    let split=name.split('_Mod')
    return split[0]
  }

  generatePar(l,i,largo,index,m,miscolores,data,superior,inferior,setpoint,c){
    //,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}` 
    //console.log(data)
    //console.log(this.state.tags_original.filter(t=>t.locationId==l.locationId))
    
    return data.map(d=>{
      let array_tendencia={
        active: true,
        code: l.code,
        index: i,
        module: "modulo 100",
        name: d.name,
        tags: this.state.tags_original.filter(t=>t.locationId==d.locationId),
        zoneId: d.zoneId,
        _id: d.locationId
      } 
      let name=d.name.split(" oxd");
      // console.log(name[0])
      // console.log(superior)
      
      let banda_superior=superior.filter(l=>l.name.includes(String(name[0])))[0];
      let banda_inferior=inferior.filter(l=>l.name.includes(String(name[0])))[0];
      let set_point=setpoint.filter(l=>l.name.includes(String(name[0])))[0];
      return <Row>
        <UncontrolledTooltip placement="top-end" target={`oxd_divice2${d.id}`}>
        {!check?"valor de sonda actual":
        "Promedio de sonda desde las 00:00 hasta hora actual"}
        </UncontrolledTooltip>
      <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${d.id}`} style={{color:"black",size:30}} class="pe-7s-help1"></i> 
        <Button  style={{marginTop:15, width:`${55}%`}} 
              onClick={(()=>{
                tag_=[];
                title_disp="";
                title_disp=d.name;
                this.toggleModal2(true,c._id,c.name,"tencencia_abiotica",array_tendencia)
              })}
              className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 animated fadeIn fast" color="primary">     
                          <div className="pl-1"  style={{fontSize:10}}>  
                          {this.getNameCentro(d.name)}</div> 
                      <span className="badge badge-light m-0 ml-0 pl-1 w-100">  
                      <font size="2">{d.ultimo_valor}</font>
                      {/* <span className="opacity-6  pl-0 size_unidad2">  
                      225 </span>  */}
                      </span>
                      {/* <span class="badge badge-dot badge-dot-lg badge-success"> </span> */}
                      {this.getStatusColor(d,banda_superior,banda_inferior,set_point)}
                  </Button>
      </Col>
    </Row>
    })
   
    // return data.filter((d,ind)=>ind==i).map(d=>{
    //   return <Row>
    //     <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${d.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> {this.getName(d.name)}</Col>
    //     {this.getColorColControl(d.prom,"mg/L",miscolores[0],index+100,d.ultimo_valor,d.id)}
    //     <Col data-event={index+m}><div style={{width:15,height:15}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Col>
    //     <Col><Button color="link" >Config.</Button></Col>
    //   </Row>
    // })
    
  }
  getStatusColor(data,superior,inferior,setpoint){
    //console.log(superior)
    let media=data.alerta_max-data.alerta_min;
    media=media/2;
    media=data.alerta_min+media;
    //console.log(`ult${data.ultimo_valor} max${superior.ultimo_valor} min${inferior.ultimo_valor}`)
    // console.log(data.ultimo_valor<inferior.ultimo_valor)
    // console.log(data.ultimo_valor>=inferior.ultimo_valor && data.ultimo_valor<setpoint.ultimo_valor)
    // console.log(data.ultimo_valor>=superior.ultimo_valor)
    if(data.ultimo_valor!=undefined && inferior!=undefined && superior!=undefined ){
      if(data.ultimo_valor<inferior.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF0000"}}> </span>
      }
      if(data.ultimo_valor>=inferior.ultimo_valor && data.ultimo_valor<setpoint.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF8000"}}> </span>
      }
      if(data.ultimo_valor>=setpoint.ultimo_valor && data.ultimo_valor<superior.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FFFF00"}}> </span>
      }
      if(data.ultimo_valor>=superior.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
      }
    }
   
    // if(data.ultimo_valor>=data.alerta_max || data.ultimo_valor<=data.alerta_min){
    //    return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF0000"}}> </span>
    // }else{
    //    return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
    // }
    //return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}></span>
    //return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
    // <span className="badge badge-pill badge-info" style={{backgroundColor:"#007bff"}}>Azul</span>
    // &nbsp;Niveles normales de oxigeno.&nbsp;
    // <span class="badge badge-pill badge-info" style={{backgroundColor:"#FFFF00",color: "#007bff"}}>Amarillo</span>
    // &nbsp;Ox. tendiente a la baja.&nbsp;
    // <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF8000",color:"#fff"}}>Naranjo</span>
    // &nbsp;Nivel Critico Ox.&nbsp;
    // <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF0000",}}>Rojo</span>
    // &nbsp;Ox. bajo nivel alarma.&nbsp;
  }

  generateInpar(l,i,largo,index,m,miscolores,data,superior,inferior,setpoint){
    return data.map(d=>{
      let name=d.name.split(" oxd");
      // console.log(name[0])
      // console.log(superior)
      let banda_superior=superior.filter(l=>l.name.includes(String(name[0])))[0];
      let banda_inferior=inferior.filter(l=>l.name.includes(String(name[0])))[0];
      let set_point=setpoint.filter(l=>l.name.includes(String(name[0])))[0];
      // console.log(name[0])
      //console.log(set_point)
      return <Row>
         <UncontrolledTooltip placement="top-end" target={`oxd_divice2${d.id}`}>
        {!check?"valor de sonda actual":
        "Promedio de sonda desde las 00:00 hasta hora actual"}
        </UncontrolledTooltip>
      <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${d.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> 
        <Button  style={{marginTop:15, width:`${55}%`}} 
              className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 animated fadeIn fast" color="primary">     
                          <div className="pl-1" style={{fontSize:10}}>  
                          {this.getNameCentro(d.name)}</div> 
                      <span className="badge badge-light m-0 ml-0 pl-1 w-100">  
                      <font size="2">{d.ultimo_valor}</font>
                      {/* <span className="opacity-6  pl-0 size_unidad2">  
                      225 </span>  */}
                      </span>
                      {this.getStatusColor(d,banda_superior,banda_inferior,set_point)}
                      
                  </Button>
      </Col>
    </Row>
    })

    // return data.filter((d,ind)=>ind==i).map(d=>{
    //   return <Row>
    //     <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${d.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> {this.getName(d.name)}</Col>
    //     {this.getColorColControl(d.prom,"mg/L",miscolores[0],index+100,d.ultimo_valor,d.id)}
    //     <Col data-event={index+m}><div style={{width:15,height:15}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Col>
    //     <Col><Button color="link" >Config.</Button></Col>
    //   </Row>
    // })
  }

  getAcordionModal(index,miscolores,collapse,c,modulo){
    // let new_modulo=modulo.replace('Mod','');
    // modulo=new_modulo.replace('M','');
    // console.log(modulo)
    // console.log(modulo.includes("MODULOS"))
    //console.log(c.name.includes(" Control"))
    let m=0;
    let visible="block";

    if(modulo.includes("Ponton")){
      m=900;
    }else{
      m=parseInt(modulo)
    }
    if(modulo.includes("MODULOS")){
      m=1000;
    }
    if(modulo.includes("100")){
      m=100;
    }
    if(modulo.includes("200")){
      m=200;
    }
    if(modulo.includes("300")){
      m=300;
    }
    if(modulo.includes("400")){
      m=400;
    }
    if(modulo.includes("500")){
      m=500;
    }
    if(modulo.includes("600")){
      m=600;
    }
    if(c.endDate=="" || c.endDate==null){
      visible="none"
    }
    let promedios=this.state.Mis_Prom;
    if(c.name.includes(' Control')){
      promedios=this.state.Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
                                                                 &&!l.name.includes("VoltajeR"))
    }else{
      promedios=this.state.Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
                                      &&l.name.includes(m!=900?m:"Ponton"))
    }
    
    // let data=this.state.Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
    //                                             )
    // console.log(data)
    return <Fragment>
    <ListGroupItem style={{borderColor:this.getColorAlert(c,"oxd",m,miscolores),borderWidth:3,display:visible}}  action onClick={this.toggle} data-event={index+100}>
    <UncontrolledTooltip placement="top-end" target={`oxd_divice${c._id}`}>
      {!check?"Promedio de oxígeno disuelto de las sondas instaladas en centro"
      :"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 00:00 hasta la hora actual"}
      
    </UncontrolledTooltip>
      <Row>
      {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
        <Col  onClick={this.toggle} data-event={index+m}><i id={`oxd_divice${c._id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Modulo {modulo} Ox.</Col>
        <Col  onClick={this.toggle} data-event={index+m}>{this.getPromCentro(c._id," oxd",miscolores[0],m)}</Col>
        <Col  onClick={this.toggle} data-event={index+m}>{this.getPromMinCentro(c._id," oxd",miscolores[0],m)}</Col>
        <Col  onClick={this.toggle} data-event={index+m}>{this.getPromMaxCentro(c._id," oxd",miscolores[0],m)}</Col>
        {/* <Col md={1} onClick={this.toggle} data-event={index+m}></Col> */}
      </Row>
    </ListGroupItem>
    <Collapse isOpen={collapse === index+m}>
    {promedios.map((l,i)=>{
        let data=promedios.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd"))
        let largo=data.length;
        //console.log(data)
        let color="";
        if(l.ultimo_valor>=l.alerta_max){
          color=miscolores[2];
        }
        if(l.ultimo_valor<=l.alerta_min){
          color=miscolores[2];
        }
        return (
          <ListGroupItem style={{backgroundColor:color}} action onClick={this.toggle} data-event={index+m}>
          <UncontrolledTooltip placement="top-end" target={`oxd_divice2${l.id}`}>
          {!check?"valor de sonda actual":
          "Promedio de sonda desde las 00:00 hasta hora actual"}
          </UncontrolledTooltip>
          <Row onClick={(()=>{
              // alert(JSON.stringify(l))
              //alert(JSON.stringify(data.filter(d=>d.locationId==l.locationId)[0].id))
              let array_tendencia={
                active: true,
                code: l.code,
                index: i,
                module: "modulo 100",
                name: data.filter(d=>d.locationId==l.locationId)[0].name,
                tags: this.state.tags_original.filter(t=>t.locationId==l.locationId),
                zoneId: data.filter(d=>d.locationId==l.locationId)[0].zoneId,
                _id: l.locationId
              } 
              //alert(JSON.stringify(array_tendencia))
              tag_=[];
              title_disp="";
              title_disp=l.name;
              this.toggleModal2(true,c._id,c.name,"tencencia_abiotica",array_tendencia)
            })}>
            <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${l.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> {this.getName(l.name)}</Col>
            {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],index+100,l.ultimo_valor,l.id)}
          </Row>
          </ListGroupItem>
        )
        
    })}
    </Collapse>
    </Fragment>
  }

  getAcordion(index,miscolores,collapse,c,modulo,Mis_Prom){
    // let new_modulo=modulo.replace('Mod','');
    // modulo=new_modulo.replace('M','');
    // console.log(modulo)
    // console.log(modulo.includes("MODULOS"))
    //console.log(c.name.includes(" Control"))
    let m=0;
    let visible="block";

    if(modulo.includes("Ponton")){
      m=900;
    }else{
      m=parseInt(modulo)
    }
    if(modulo.includes("MODULOS")){
      m=1000;
    }
    if(modulo.includes("100")){
      m=100;
    }
    if(modulo.includes("200")){
      m=200;
    }
    if(modulo.includes("300")){
      m=300;
    }
    if(modulo.includes("400")){
      m=400;
    }
    if(modulo.includes("500")){
      m=500;
    }
    if(modulo.includes("600")){
      m=600;
    }
    if(c.endDate=="" || c.endDate==null){
      visible="none"
    }
    let promedios=Mis_Prom;
    if(c.name.includes(' Control')){
      promedios=Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
                                                                 &&!l.name.includes("VoltajeR"))
    }else{
      promedios=Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
                                      &&l.name.includes(m!=900?m:"Ponton"))
    }
    if(!c.name.includes(' Control')){
      promedios=promedios.map(p=>{
        let split=p.name.split('m_')
        p.order=parseInt(split[0])
        return p
      })
      promedios=_.orderBy(promedios,["order"],["asc"])
    }
    // let data=this.state.Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
    //                                             )
    // console.log(data)

    return <Fragment>
    <ListGroupItem style={{borderColor:this.getColorAlert(c,"oxd",m,miscolores),borderWidth:3,display:visible}}  action onClick={this.toggle} data-event={index+100}>
    <UncontrolledTooltip placement="top-end" target={`oxd_divice${c._id}`}>
      {!check?"Promedio de oxígeno disuelto de las sondas instaladas en centro"
      :"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 00:00 hasta la hora actual"}
      
    </UncontrolledTooltip>
      <Row>
      {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
        <Col  onClick={this.toggle} data-event={index+m}><i id={`oxd_divice${c._id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Modulo {modulo} Ox.</Col>
        <Col  onClick={this.toggle} data-event={index+m}>{this.getPromCentro(c._id," oxd",miscolores[0],m)}</Col>
        <Col  onClick={this.toggle} data-event={index+m}>{this.getPromMinCentro(c._id," oxd",miscolores[0],m)}</Col>
        <Col  onClick={this.toggle} data-event={index+m}>{this.getPromMaxCentro(c._id," oxd",miscolores[0],m)}</Col>
        {/* <Col md={1} onClick={this.toggle} data-event={index+m}></Col> */}
      </Row>
    </ListGroupItem>
    <Collapse isOpen={collapse === index+m}>
    {promedios.map((l,i)=>{
        let data=promedios.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd"))
        data=data.map(d=>{
          if(d.name.includes('_')){
            let split=d.name.split('_')
            let split2=String(split[0]).split(' ')
            d.order=parseInt(split2[1])
          }
          return d
        })
        let largo=data.length;
        //console.log(data)
        let color="";
        if(l.ultimo_valor>=l.alerta_max){
          color=miscolores[2];
        }
        if(l.ultimo_valor<=l.alerta_min){
          color=miscolores[2];
        }
        
        if(c.name.includes(' Control')){
            // console.log(data);
            let data_par=data.filter(d=>this.getPar(d.name)=="par")
            data_par=_.orderBy(data_par,["order"],"asc");
            let data_inpar=data.filter(d=>this.getPar(d.name)!="par")
            data_inpar=_.orderBy(data_inpar,["order"],"asc");
            data=_.orderBy(data,["order"],"asc");
            if(c.name.includes('Huenquillahue')){
              let new_promedio1=[]
              let new_promedio2=[]
              new_promedio1.push(promedios.filter((p,i)=>i==3)[0])
              new_promedio2.push(promedios.filter((p,i)=>i==0)[0])
              new_promedio1.push(promedios.filter((p,i)=>i==1)[0])
              new_promedio2.push(promedios.filter((p,i)=>i==2)[0])
              data_par=new_promedio2;
              data_inpar=new_promedio1;
            }
            //console.log(this.state.Mis_Prom)
            let data_superior=Mis_Prom.filter(l=>l.name.includes(" bandasuperior"))
            //console.log(data_superior)
            let data_inferior=Mis_Prom.filter(l=>l.name.includes(" bandainferior"))
            //console.log(data_inferior)
            let data_setpoint=Mis_Prom.filter(l=>l.name.includes(" setpoint"))
            //console.log(data_setpoint)
            if(i==0){
              return (
                <ListGroupItem  action onClick={this.toggle} data-event={index+m}>
                <UncontrolledTooltip placement="top-end" target={`oxd_divice2${l.id}`}>
                {!check?"valor de sonda actual":
                "Promedio de sonda desde las 00:00 hasta hora actual"}
                </UncontrolledTooltip>
                {this.getColumParInp(l,i,largo,index,m,miscolores,data_par,data_inpar,c.name,data,data_superior,data_inferior,data_setpoint,c)}
                
                </ListGroupItem>
              )
            }
        }else{
          return (
            <ListGroupItem style={{backgroundColor:color}} action onClick={this.toggle} data-event={index+m}>
            <UncontrolledTooltip placement="top-end" target={`oxd_divice2${l.id}`}>
            {!check?"valor de sonda actual":
            "Promedio de sonda desde las 00:00 hasta hora actual"}
            </UncontrolledTooltip>
            <Row onClick={(()=>{
                // alert(JSON.stringify(l))
                //alert(JSON.stringify(data.filter(d=>d.locationId==l.locationId)[0].id))
                let array_tendencia={
                  active: true,
                  code: l.code,
                  index: i,
                  module: "modulo 100",
                  name: data.filter(d=>d.locationId==l.locationId)[0].name,
                  tags: this.state.tags_original.filter(t=>t.locationId==l.locationId),
                  zoneId: data.filter(d=>d.locationId==l.locationId)[0].zoneId,
                  _id: l.locationId
                } 
                //alert(JSON.stringify(array_tendencia))
                tag_=[];
                title_disp="";
                title_disp=l.name;
                this.toggleModal2(true,c._id,c.name,"tencencia_abiotica",array_tendencia)
              })}>
              <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${l.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> {this.getName(l.name)}</Col>
              {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],index+100,l.ultimo_valor,l.id)}
            </Row>
            </ListGroupItem>
          )
        }
        
    })}
    </Collapse>
    </Fragment>
  }

  onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
  }

  filterEvent(workplace){
    //let val=this.state.Events.filter(c=>c.workplaceId==workplace)
    this.tagservices.getfindEventActive(workplace).then(respuesta=>{
      this.setState({Centro_Actividad:respuesta.filter(this.onlyUnique)})
    })
    .catch(e=>console.log(e))
  }

  filterEventEnd(workplaceIds){
    this.tagservices.getfindEventActiveEnd(workplaceIds).then(respuesta=>{
      this.setState({EventCentros:respuesta})
    })
    .catch(e=>console.log(e))
  }

  getEventEndView(workplaceId,miscolores){
    //console.log(this.state.EventCentros)
    let centro=[];
    try{
    centro=this.state.EventCentros.filter(e=>e.workplaceId==workplaceId)[0]
    //console.log(centro)
      return <ListGroupItem style={{textAlign:'center',backgroundColor:"#FADF87",fontSize:12,maxHeight:`${50}%`}}>
        <Row style={{marginTop:-10}}>
        {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
          <Col sm={3} ><i id={`oxd_divice`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Evento Activo <br></br> {moment(centro.dateTime).format('DD-MM-YYYY hh:mm')}</Col>
          <Col sm={9} >{centro.detail}</Col>
        </Row>
      </ListGroupItem>
    }catch(e){
      //console.log(e)
    }
  }

  mostrar(){
    let mostrar="none"
    if(this.state.Abiotic || this.state.Control){
      mostrar="block"
    }
    return mostrar
  }

  getCardCentros(miscolores,collapse,centros){
    centros=centros.filter(c=>{
        let posicion1=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==1).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==1):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==1)
        let posicion2=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==2).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==2):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==2)
        let posicion3=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==3).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==3):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==3)
        let posicion4=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==4).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==4):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==4)
        if(posicion1.length>0){
          c.position=1
        }else if(posicion2.length>0){
          c.position=2
        }else if(posicion3.length>0){
          c.position=3
        }else if(posicion4.length>0){
          c.position=4
        }
        if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
          return c.areaId==Buscar
        }
        if(this.state.Control){
          return c.name.includes(' Control')
        }
        if(this.state.Diagnostico){
          // let posicion1=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==1).length>0?
          //               this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==1):
          //               this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==1)
          // let posicion2=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==2).length>0?
          //               this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==2):
          //               this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==2)
          // let posicion3=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==3).length>0?
          //               this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==3):
          //               this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==3)
          // let posicion4=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==4).length>0?
          //               this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==4):
          //               this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==4)
          // if(posicion1.length>0){
          //   c.position=1
          // }else if(posicion2.length>0){
          //   c.position=2
          // }else if(posicion3.length>0){
          //   c.position=3
          // }else if(posicion4.length>0){
          //   c.position=4
          // }
          return c
        }
        if(Buscar=="" && this.state.Control==false){
          return !c.name.includes(' Control')
        }
    })
    this.filterEventEnd(centros.map(c=>{return {workplaceId:c._id}}))
    centros=this.state.Diagnostico?
      _.orderBy(centros.filter(c=>{
        if(c.areaId==Buscar&&Buscar!=""){
          return c.areaId==Buscar
        }if(Buscar==""){
          return c
        }
      }),['position'],['asc']):
      _.orderBy(centros,["name"],["asc"])
    return centros.map((c,index)=>{
        return  <Col md={this.state.Diagnostico?3:4} lg={this.state.Diagnostico?2:4} style={{display:this.findActiveWorkplace(c._id)>0?"block":"none"}}>
                    <Card className="main-card mb-5 mt-3" key={index} >
                        <CardHeader className="card-header-tab  ">
                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                        {this.createTooltip(`detalle${c._id}`,"Mas Detalles de Centros")}
                              <a style={{cursor:"pointer"}} onClick={(()=>{
                                if(!this.state.Piscicultura){
                                  sessionStorage.setItem('workplace', JSON.stringify({"id":c._id,"name":c.name,"active":c.active}));
                                  this.ingresarCentroUsuario(`${c._id}/${c.name}`);
                                  this.setState({redirect:true})
                                }
                              })} id={`detalle${c._id}`}>
                              <div class="icon-wrapper rounded-circle zoom" >
                                  <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                  <i class="lnr-apartment text-white" style={{display:`${this.state.Piscicultura?"none":"block"}`}}></i>
                                  <i class="pe-7s-anchor text-white" style={{display:`${this.state.Piscicultura?"block":"none"}`}}></i>
                              </div>
                              </a>
                              <div class="widget-chart-content font-size-lg font-weight-normal" style={{marginTop:30}}>
                              <p>{c.name}
                              {this.createTooltip(`event_${c._id}`,"Ver Eventos")}
                              <Button id={`event_${c._id}`} color="link" style={{marginLeft:`${90}%`,marginTop:-50,visible:`${this.state.Piscicultura?"hidden":"visible"}`}}
                              pill onClick={((e)=>{
                                //alert(c._id)
                                this.filterEvent(c._id)
                                this.toggleModal1(true,c._id,c.name)
                              })}> <i style={{fontSize:20,cursor:"pointer",color:`${c.event==true?"orange":"silver"}`}} className="pe-7s-bell zoom" ></i></Button>
                              </p>
                              </div>
                        </div>

                        <div className="btn-actions-pane-right text-capitalize" style={{display:`${this.state.Piscicultura?"none":"block"}`}}>
                            {/* {c.endDate} */}
                            {!this.state.Equipos?this.endDate(c._id):this.endDate2(c._id)}
                        </div>                           
                        </CardHeader>
                        <ListGroup style={{cursor:"pointer"}}>
                        <div style={{display:`${this.mostrar()}`}}>
                              {
                                this.state.Modulos.filter(m=>m.centro==c.name).map((m)=>{
                                  //console.log(object)
                                  let zonas = m.zonas;
                                  return zonas.map((z)=>{
                                    // if(c._id=="61e0727c1ce5784b468127bc"){
                                    //   // console.log("zonas : "+z)
                                    //   // console.log(this.state.Modulos.filter(m=>m.centro==c.name))
                                    // }
                                    // console.log(z)
                                    let modul=z.replace("Modulo ","")
                                    //console.log(modul)
                                    return this.getAcordion(index,miscolores,collapse,c,modul,this.state.Mis_Prom)
                                  })
                                })
                              }
                              {this.getEventEndView(c._id,miscolores)}
                          </div>
                          <div style={{display:`${this.state.Meteo?"block":"none"}`}}>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Viento</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Velocidad</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Humedad</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                          </div>
                          <div style={{display:`${this.state.Proceso?"block":"none"}`}}>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+600}>Combustible</Col>
                                  <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                  <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                  <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+600)}>
                                <ListGroupItem tag="a" action>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+600}>Flujometro Abducción</Col>
                                    <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                  </Row>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+600}>Flujometro Retorno</Col>
                                    <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                  </Row>
                                </ListGroupItem>
                              </Collapse>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}} tag="a" action>
                                  <Row>
                                    <Col >Consumo calculado</Col>
                                    <Col><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col>min</Col>
                                    <Col>max</Col>
                                  </Row>
                              </ListGroupItem>
                          </div>
                          <div style={{display:`${this.state.Equipos?"block":"none"}`}}>
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+700}>Combustible</Col>
                                  <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                  <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                  <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+700)}>
                                <ListGroupItem tag="a" action>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+700}>Flujometro Abducción</Col>
                                    <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                  </Row>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+700}>Flujometro Retorno</Col>
                                    <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                  </Row>
                                </ListGroupItem>
                              </Collapse> */}
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}} tag="a" action>
                                  <Row>
                                    <Col >Consumo calculado</Col>
                                    <Col><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col>min</Col>
                                    <Col>max</Col>
                                  </Row>
                              </ListGroupItem> */}
                          </div>
                          {/* {this.state.Diagnostico&&(
                            <div style={{marginLeft:-1,marginRight:-1}}>
                              <Diagnostico
                                numero={900}
                                zonas={this.state.Modulos.filter(m=>m.centro==c.name)}
                                dispositivos={this.state.Mis_Prom}
                                dispositivos2={this.state.Mis_Prom2}
                                centro={c}
                                indice={index}
                              />
                            </div>
                          )} */}
                          <div style={{display:`${this.state.Piscicultura?"block":"none"}`,alignItems: 'center',textAlign:"center"}}>
                            <Row style={{marginTop:10}}>
                              <Col onClick={this.toggle} data-event={index+700}>Botones Rapidos</Col>
                              <Col onClick={this.toggle} data-event={index+700}><span onClick={(()=>{
                                if(c.name=="RUPANQUITO"){
                                  this.toggleModal2(true,c._id,c.name,"panel_general")
                                }
                              })} class="badge badge-pill badge-primary">Panel General</span></Col>
                              <Col onClick={this.toggle} data-event={index+700}><span class="badge badge-pill badge-primary" onClick={(()=>{
                                if(c.name=="RUPANQUITO"){
                                  this.toggleModal2(true,c._id,c.name,"tendencia")
                                }
                              })}>Tendencia Historica</span></Col>
                              <Col onClick={this.toggle} data-event={index+700}><span class="badge badge-pill badge-primary">Modulos</span></Col>
                            </Row>
                            <Row style={{marginTop:10}}></Row>
                          </div>
                        </ListGroup>
                    </Card>

                </Col>
      })
  }

  getCardCentrosPriscultura(miscolores,collapse,centros){
    return centros.filter(c=>{
        if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
          return c.areaId==Buscar
        }
        if(this.state.Control){
          return c.name.includes(' Control')
        }
        if(Buscar=="" && this.state.Control==false){
          return !c.name.includes(' Control')
        }
    }).map((c,index)=>{
      return  <Col md="4" style={{display:this.findActiveWorkplace(c._id)>0?"block":"none"}}>
                  <Card className="main-card mb-5 mt-3" key={index} >
                      <CardHeader className="card-header-tab  ">
                      <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                      {this.createTooltip(`detalle${c._id}`,"Mas Detalles de Centros")}
                            <a style={{cursor:"pointer"}} onClick={(()=>{
                             
                            })} id={`detalle${c._id}`}>
                            <div class="icon-wrapper rounded-circle" >
                                <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                <i class="lnr-apartment text-white" style={{display:`${this.state.Piscicultura?"none":"block"}`}}></i>
                                <i class="pe-7s-anchor text-white" style={{display:`${this.state.Piscicultura?"block":"none"}`}}></i>
                            </div>
                            </a>
                            <div class="widget-chart-content font-size-lg font-weight-normal" style={{marginTop:30}}>
                            <p>{c.name}
                            </p>
                            </div>
                      </div>

                      <div className="btn-actions-pane-right text-capitalize" style={{display:`${this.state.Piscicultura?"none":"block"}`}}>
                          {/* {c.endDate} */}
                          {!this.state.Equipos?this.endDate(c._id):this.endDate2(c._id)}
                      </div>                           
                      </CardHeader>
                      <ListGroup style={{cursor:"pointer"}}>
                      
                        <div style={{display:`${this.state.Piscicultura?"block":"none"}`,alignItems: 'center',textAlign:"center"}}>
                          <Row style={{marginTop:10}}>
                            <Col onClick={this.toggle} data-event={index+700}>Botones Rapidos</Col>
                            <Col onClick={this.toggle} data-event={index+700}><span onClick={(()=>{
                              if(c.name=="RUPANQUITO"){
                                this.toggleModal2(true,c._id,c.name,"panel_general")
                              }
                            })} class="badge badge-pill badge-primary">Panel General</span></Col>
                            <Col onClick={this.toggle} data-event={index+700}><span class="badge badge-pill badge-primary" onClick={(()=>{
                              if(c.name=="RUPANQUITO"){
                                this.toggleModal2(true,c._id,c.name,"tendencia")
                              }
                            })}>Tendencia Historica</span></Col>
                            <Col onClick={this.toggle} data-event={index+700}><span class="badge badge-pill badge-primary">Modulos</span></Col>
                          </Row>
                          <Row style={{marginTop:10}}></Row>
                        </div>
                      </ListGroup>
                  </Card>

              </Col>
    })
}
  getButtonGraphic(marginLeft){
    return <ButtonGroup style={{marginLeft:`${marginLeft}%`,marginTop:-100}}>
    <UncontrolledTooltip placement="top-end" target="Graficos_Generales_0">
        Ocultar Graficos
      </UncontrolledTooltip>
      <UncontrolledTooltip placement="top-end" target="Graficos_Generales_1">
        Grafico Promedios Generales (Barra)
      </UncontrolledTooltip>
      <UncontrolledTooltip placement="top-end" target="Graficos_Generales_2">
        Grafico Promedios Generales (Lineal)
    </UncontrolledTooltip>
    <Button id="Graficos_Generales_1" active={this.state.act_graph1} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
        this.setState({TypeGraphic:"3"})
        this.switchGrafic("block");
        this.switchActive0(false,true,false)
        // this.switchGrafic(this.state.active_grafic);
      }}>
      <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
    </Button>
    <Button id="Graficos_Generales_0" active={this.state.act_graph0} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
        //this.setState({TypeGraphic:"3"})
        this.switchGrafic("none");
        this.switchActive0(true,false,false)
        // this.switchGrafic(this.state.active_grafic);
      }}>
      -
    </Button>
  <Button id="Graficos_Generales_2" active={this.state.act_graph2} color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
        //this.setState({check:true,check2:false})
        this.switchGrafic("block");
        this.setState({TypeGraphic:"1"})
        this.switchActive0(false,false,true)
      }}>
      <i style={{fontSize:20}} className="pe-7s-graph3 btn-icon-wrapper" ></i>
    </Button>
    </ButtonGroup>
  }

  getDetailActivity(activityId){
    let act=this.state.Actividad.filter(a=>a._id==activityId).map((a)=>{
        return a.detail
    })
    //console.log(act)
    return act[0]
  }

  getActivity(){
      axios
      //.post(API_ROOT+"/"+empresa+"/centros_activos",{
      .get(API_ROOT+`/activity/all`)
      .then(response => {   
          // alert(response.data.statusCode==200);
          if(response.data.statusCode==200){
              let actividades=response.data.data;
              let new_actividades=actividades.map((a,i)=>{
                  return {
                      ind:i+1,
                      _id:a._id,
                      detail:a.detail,
                      dateTime:moment(a.dateTime).format("HH:mm DD-MMM"),
                      visible:"block"
                  }
              })
              this.setState({Actividad:new_actividades})
          }
          
      })
      .catch(error => {
          //console.log(error)
      });
  }

  switchActive0(act_graph0,act_graph1,act_graph2){
    this.setState({act_graph0,act_graph1,act_graph2})
  }
  
  switchActive_principal(nombre_menu){
    let menu=["Abiotic","Meteo","Proceso","Equipos","Control","Piscicultura","Diagnostico"]
    let data={}
    menu.map(m=>{
      if(m==nombre_menu){
        data[m]=true
      }else{
        data[m]=false
      }
    })
    console.log(data)
    this.setState(data)
    
  }
  
  switchActiveModal(TypeGraphic2){
    this.setState({TypeGraphic2})
  }

  handleChangeActivity = event => {
      const name = event.target.name;
      const value = event.target.value;
      this.setState({ [name]: value });
  }

  handleChangeArea = event => {
      const name = event.target.name;
      const value = true;
      //console.log(name)
      areas_centros.map((a,i)=>{
        if(name=="btn"+i){
          this.setState({ [name]: value });
        }else{
          this.setState({ ["btn"+i]: false });
        }
      })
  }

  generateAreasCentros(){
    try{
      return areas_centros.map((area,i)=>{
        return ubicaciones.filter(u=>u._id==area).map((a)=>{
            return <Button value={a.name} name={"btn"+i} color="primary" onClick={((e)=>{
              Buscar=a._id;
              active_todos=false;
              this.handleChangeArea(e)
            })} onChange={e => this.handleChangeArea(e)} active={this.state["btn"+i]} outline>{a.name}</Button>
        })
      })
    }catch(e){
      //console.log(e)
    }
  }

  getPisiculturaCard(miscolores,collapse){
    let pisciculturas=["RUPANQUITO","EL PERAL","ALCALDEO"]
    let data=[]
    pisciculturas.map((p,i)=>{
      data.push({
        active: true,
        alertEmail: "none",
        areaId: "",
        categoryId: "",
        code: "-"+i,
        companyId: "5f6e193069e89562400049ca",
        event: true,
        failEmail: "none",
        name: p,
        ubicacion: "none",
        _id: "5f6e49751a080102d0e83bf6"
      })
    })

    return this.getCardCentrosPriscultura(miscolores,collapse,data)

  }

  getTypeView(){
    if(type_view=="panel_general")
      return<Rupanquito></Rupanquito>
    if(type_view=="tendencia")
      return<Tendencia_Rupanquito></Tendencia_Rupanquito>
    if(type_view=="tencencia_abiotica")
      return <Tendencia Centro={_id_centro} Ncentro={title_centro} Tag={tag_} ></Tendencia>
  }

  getCompanys(){
    this.tagservices.crudCompany("",{},'GET').then(respuesta=>{
      if(respuesta.statusCode===201 || respuesta.statusCode===200){
        this.setState({Companys:respuesta.data})
      }
    })
  }

  handleChangeSelect = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value });
  }

    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        if(this.state.redirect2){
          return <Redirect to="/dashboards/configuracion"></Redirect>
        }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
             
         ///***********************************+++ */
        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        {/* {console.log(this.state.Centros2)} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Resumen</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      <BreadcrumbItem active tag="span">Variables</BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{display:active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion</Badge></a></BreadcrumbItem>
                      <Input type={'select'} name="selectCompany" onChange={this.handleChangeSelect} style={{maxWidth:250,marginLeft:20,display:active_config2}}>
                          <option value={""} disabled>-- Seleccione una Empresa --</option>
                          <option value={""} >Todas las Empresa</option>
                          {
                            this.state.Companys.map(company=>{
                              return <option value={company._id}>{company.name}</option>
                            })
                          }
                        </Input>
                      </Breadcrumb>
                        {/* <Input style={{marginTop:10,width:240}}  type="select" name="select">
                                    <option  selected disabled value='0'>SELECCIONE UNA REGION</option>
                                    {
                                      ubicaciones.map((a)=>{
                                      return <option value={a._id}>{a.name}</option>
                                      })
                                    }
                        </Input> */}
                       
                       <Button active={active_todos} name={"btn-1"} color="primary" outline onClick={((e)=>{
                          Buscar="";
                          active_todos=true;
                          this.handleChangeArea(e)
                        })} onChange={e => this.handleChangeArea(e)} >Todos</Button>
                        {
                            this.generateAreasCentros()
                            
                        }
                        
                      <div style={{display:this.state.active_grafic}}>
                        <ButtonGroup size="sm" style={{marginTop:-50}}>
                        <UncontrolledTooltip placement="top-end" target="UncontrolledTooltipExample">
                          Grafico de promedios Generales
                              (Oxd, Temp, Oxs, Sal)
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Oxd">
                          Promedios de Oxigeno Disuelto
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Temp">
                          Promedios de Temperatura
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Oxs">
                          Promedios de Oxigeno Saturado
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Sal">
                          Promedios de Salinidad
                        </UncontrolledTooltip>
                          <Button id="UncontrolledTooltipExample" color="primary"  active={this.state.act_all} onClick={()=>{
                            this.switchActive(true,false,false,false,false)
                          }}>All</Button>
                          <Button id="promedios_Oxd" color="primary" active={this.state.act_oxd} onClick={()=>{
                            this.switchActive(false,true,false,false,false)
                          }}>Oxd</Button>
                          <Button id="promedios_Temp" color="primary" active={this.state.act_temp} onClick={()=>{
                            this.switchActive(false,false,true,false,false)
                          }}>Temp</Button>
                          <Button id="promedios_Oxs" color="primary" active={this.state.act_oxs} onClick={()=>{
                            this.switchActive(false,false,false,true,false)
                          }}>Oxs</Button>
                          <Button id="promedios_Sal" color="primary" active={this.state.act_sal} onClick={()=>{
                            this.switchActive(false,false,false,false,true)
                          }}>Sal</Button>
                        </ButtonGroup>
                        
                        {/* {this.getChart()} */}
                      </div>
                     <Row>
                     {/* <Button onClick={(()=>{
                        Push.create("Bienvenido a IdealCloud", {
                          body: "Una plataforma de Monitoreo y Control",
                          icon: `https://ihc.idealcontrol.cl/idealcloud2/doc/imagenes/logo.png`,
                          tag: 'ideal',
                          timeout: 9000,
                          onClick: function () {
                              window.focus();
                              this.close('ideal');
                          }
                        });
                      })}>-</Button> */}
                     
                       {/* {console.log(this.state.Mis_Prom)} */}
                     <Col md="12">
                            <Card className="main-card mb-1 p-0">
                            <CardHeader className="card-header-tab" style={{height:50}}>
                                  <div style={{marginLeft:-20}}>
                                      <Button 
                                      className="widget-chart widget-chart-hover p-0 p-0  btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Abiotic?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                          this.switchActive_principal('Abiotic');
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Abiotic?"#6c757d":"#fff"}>&nbsp;&nbsp;Abióticas&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} opacity-1 */}
                                          </div>
                                      </Button>  
                                      <Button 
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" disabled style={{backgroundColor:`${!this.state.Meteo?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal('Meteo');
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Meteo?"#6c757d":"#fff"}>&nbsp;&nbsp;Meteorologicas&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} opacity-1 */}
                                              </div>
                                      </Button> 
                                  
                                      <Button 
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" disabled style={{backgroundColor:`${!this.state.Proceso?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal('Proceso');
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Proceso?"#6c757d":"#fff"}>&nbsp;&nbsp;Proceso&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} */}
                                              </div>
                                      </Button>  

                                      <Button 
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Equipos?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal('Equipos');
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Equipos?"#6c757d":"#fff"}>&nbsp;&nbsp;Equipos&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} */}
                                              </div>
                                      </Button>
                                      {/* {active_config2=="block"&&(
                                        <Button 
                                        className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Diagnostico?"":"#545cd8"}`}} outline color={miscolores[0]}
                                        onClick={()=>{
                                          this.switchActive_principal('Diagnostico');
                                        }}>  
                                            <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>  
                                            <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                            {data.measurements[data.measurements.length-1].value   }
                                            <span className="opacity-6  pl-0 size_unidad">  </span>
                                            <font style={font} color={!this.state.Diagnostico?"#6c757d":"#fff"}>&nbsp;&nbsp;Diagnostico&nbsp;&nbsp;</font>
                                            </div>
                                            <div className="widget-subheading">
                                                {data.shortName} 
                                                </div>
                                        </Button>
                                      )} */}
                                      <Button 
                                      disabled={false}
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Control?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal('Control');
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Control?"#6c757d":"#fff"}>&nbsp;&nbsp;Control&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} */}
                                              </div>
                                      </Button>    
                                      <Button 
                                      disabled={false}
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Piscicultura?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal('Piscicultura');
                                      }}>  
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Piscicultura?"#6c757d":"#fff"}>&nbsp;&nbsp;Piscicultura&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              
                                          </div>
                                      </Button>      
                                  </div>
                                </CardHeader>
                                <CardBody className="p-3"  style={{display:`${this.state.Piscicultura?"none":"block"}`}}>
                                {/* {console.log(this.state.Mis_Prom)} style={{display:`${this.state.Control?"none":"block"}`}} */}
                                    <div style={{display:`${!this.state.Control?"none":"block"}`}}>
                                        <span className="badge badge-pill badge-info" style={{backgroundColor:"#007bff"}}>Azul</span>
                                        &nbsp;Niveles normales de oxigeno.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FFFF00",color: "#007bff"}}>Amarillo</span>
                                        &nbsp;Ox. tendiente a la baja.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF8000",color:"#fff"}}>Naranjo</span>
                                        &nbsp;Nivel Critico Ox.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF0000",}}>Rojo</span>
                                        &nbsp;Ox. bajo nivel alarma.&nbsp;
                                    </div>
                                    {this.state.Diagnostico&&(
                                      <div>
                                          <span className="badge badge-pill badge-success">OK</span>
                                          &nbsp;Datos OK.&nbsp;
                                          <span class="badge badge-pill badge-warning" style={{backgroundColor:'yellow',color:'blue'}}>Pegados</span>
                                          &nbsp;Datos Pegados.&nbsp;
                                          <span class="badge badge-pill badge-warning">En Cero</span>
                                          &nbsp;Datos en Cero.&nbsp;
                                          <span class="badge badge-pill badge-danger">Sin Datos</span>
                                          &nbsp;Sin Datos.&nbsp;
                                      </div>
                                    )}
                                    <Row>
                                      {/* {console.log(this.state.selectCompany)} */}
                                      {this.getCardCentros(miscolores,collapse,this.state.Centros2
                                        .filter(c=>this.state.selectCompany!=""?c.companyId==this.state.selectCompany:c))}
                                    </Row>
                                </CardBody>
                                <CardBody className="p-3"  style={{display:`${this.state.Piscicultura?"block":"none"}`}}>
                                    <Row>
                                      {this.getPisiculturaCard(miscolores,collapse,this.state.Centros2)}
                                    </Row>
                                </CardBody>
                                {/* <CardBody style={{display:`${this.state.Control?"block":"none"}`}}>
                                    <Control />
                                </CardBody> */}
                            </Card>

                        </Col>
                    </Row>

                    {/* <Modal isOpen={this.state.ModalState} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                              <ModalBody >
                              <ButtonGroup size="sm" style={{marginTop:-50}}>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Oxd2">
                                    Promedios de Oxigeno Disuelto
                                  </UncontrolledTooltip>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Temp2">
                                    Promedios de Temperatura
                                  </UncontrolledTooltip>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Oxs2">
                                    Promedios de Oxigeno Saturado
                                  </UncontrolledTooltip>
                                  <UncontrolledTooltip placement="top-end" target="promedios_Sal2">
                                    Promedios de Salinidad
                                  </UncontrolledTooltip>
                                  <Button id="promedios_Oxd2" color="primary" active={this.state.act_oxd2} onClick={()=>{
                                    this.switchActive2(true,false,false,false)
                                  }}>Oxd</Button>
                                  <Button id="promedios_Temp2" color="primary" active={this.state.act_temp2} onClick={()=>{
                                    this.switchActive2(false,true,false,false)
                                  }}>Temp</Button>
                                  <Button id="promedios_Oxs2" color="primary" active={this.state.act_oxs2} onClick={()=>{
                                    this.switchActive2(false,false,true,false)
                                  }}>Oxs</Button>
                                  <Button id="promedios_Sal2" color="primary" active={this.state.act_sal2} onClick={()=>{
                                    this.switchActive2(false,false,false,true)
                                  }}>Sal</Button>
                                </ButtonGroup>
                                {this.getChartModal(_id_centro,"oxd",this.state.act_oxd2)}
                                {this.getChartModal(_id_centro,"temp",this.state.act_temp2)}
                                {this.getChartModal(_id_centro,"oxs",this.state.act_oxs2)}
                                {this.getChartModal(_id_centro,"sal",this.state.act_sal2)}
                              </ModalBody>
                              <ModalFooter>
                                <Row style={{width:`${100}%`}}>
                                  <Col xs="6" md="6" lg="6"></Col>
                                  <Col xs="3" md="3" lg="3"></Col>
                                  <Col xs="3" md="3" lg="3">
                                    <ButtonGroup>
                                      <Button color="primary" outline active={this.state.TypeGraphic2=='2.5'?true:false} onClick={(()=>{
                                          this.switchActiveModal('2.5')
                                        })}><i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper"></i></Button>
                                      <Button color="primary" outline active={this.state.TypeGraphic2=='2'?true:false} onClick={(()=>{
                                        this.switchActiveModal('2')
                                      })}><i style={{fontSize:20}}  className="pe-7s-graph3 btn-icon-wrapper"></i></Button>
                                    </ButtonGroup>
                                  </Col>
                                </Row>
                              </ModalFooter>
                          </BlockUi>
                      </Modal> */}
                      <Modal style={{maxHeight:`${100}%`,maxWidth:`${80}%`}} isOpen={ModalActive2} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal2(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro} / {title_disp}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                          {this.getTypeView()}
                                        </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>
                      <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={ModalActive} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                                <ButtonGroup>
                                                <Input placeholder="Evento..." value={Buscar2} onChange={((e)=>{
                                                  Buscar2=e.target.value;
                                                  this.handleChangeActivity(e)
                                                })}></Input>
                                                <Button color="secondary" disabled>Buscar</Button>
                                                </ButtonGroup>
                                                
                                                <ReactTable                                                           
                                                data={_.orderBy(this.state.Centro_Actividad,["dateTime"],["desc"]).filter((data)=>{
                                                  if(Buscar2!=""){
                                                      return data.detail.toLocaleUpperCase().includes(Buscar2.toLocaleUpperCase())
                                                  }else{
                                                    return data
                                                  }
                                                }).map((c,i)=>{
                                                  return {
                                                      ind:i+1,
                                                      _id:c._id,
                                                      detail:c.detail,
                                                      type:this.getDetailActivity(c.activityId),
                                                      dateTime:moment(c.dateTime).format("HH:mm DD-MMM"),
                                                      workplaceId:c.workplaceId,
                                                      activityId:c.activityId,
                                                      active:c.active,
                                                  }
                                              })}
                                                            
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={10}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                          Header: "asunto",
                                                          width: 350,
                                                          Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={"info"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.type}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "detalle",
                                                        accessor: "detail",                                                              
                                                        width: `${78}%`
                                                        },
                                                        {
                                                        Header: "estado",                                                             
                                                        width: 150,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={row._original.active?"warning":"success"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.active?"activo":"finalizado"}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        // {
                                                        // Header: "Accion",
                                                        // id: "id",
                                                        // width: 100,
                                                        // Cell: ({ row }) => (
                                                        //     <Fragment>
                                                        //         <Badge color="warning" style={{marginLeft:0}} onClick={((e)=>{
                                                        //             //this.updateEvent(row._original.id,true,row._original.workplaceId)
                                                        //         })} pill>
                                                        //         <i class="pe-7s-attention" 
                                                        //         style={{display:active_config,cursor:"pointer",fontSize:20}}></i></Badge>&nbsp;
                                                        //         <Badge color="success" pill onClick={((e)=>{
                                                        //             //this.updateEvent(row._original.id,false,row._original.workplaceId)
                                                        //             //this.switchModal("1")
                                                        //         })}><i style={{fontSize:20,cursor:"pointer"}} className="pe-7s-check                                                                " ></i></Badge>
                                                        //     </Fragment>
                                                        //     )
                                                        // },
                                                    ]                                                             
                                                }
                                                 
                                                className="-striped -highlight"
                                                />
                                          </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({
  setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Resumen2);
