import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';
import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT } from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import {
  setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../../reducers/Session';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import { split } from 'lodash';
import Diagnostico from './ComponetDiagnostico/Diagnostico';
// import * as Push from 'push.js'; // if using ES6 


//sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let load=false;
let icon_up="lnr-arrow-up";
let icon_right="lnr-arrow-right";
let icon_down="lnr-arrow-down";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
//empresas=empresas.filter(e=>e.active==true).map(e=>e);
let ubicaciones=JSON.parse(sessionStorage.getItem("Areas"));
let c=0;
let areas_centros=[];
let type_view="";
const publicVapidKey = process.env.REACT_APP_KEY_PUBLIC.toString();
// console.log(publicVapidKey)
// console.log(process.env.REACT_APP_CODE)

try{
  empresas.map(e=>{return e.areaId});
}catch(e){
  //console.log("areas")
  logout()
}
moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);


let title_centro="";
let _id_centro="";
let tag_=[];
let title_disp="";
let Buscar="";
let Buscar2="";
let active_todos=true;
let check=false;
let check2=false;
let active_config="none";
let active_config2="none";
let ModalActive=false;
let ModalActive2=false;
let i=0;

class PanelDiagnostico extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Mis_Sondas2:[],
          Modulos:[],
          isLoading:true,
          Mis_Prom:[],
          Mis_Prom2:[],
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Abiotic:false,
          Meteo:false,
          Proceso:false,
          Equipos:false,
          Diagnostico:true,
          Control:false,
          Piscicultura:false,
          redirect:false,
          redirect2:false,
          Centros:[],
          active_grafic:"none",
          ModalState:false,
          Events:[],
          Centro_Actividad:[],
          txtCenActividad:"",
          EventCentros:[],
          Actividad:[],
          Centros2:[],
          tags_original:[],
          Companys:[],
          selectCompany:"",
        };     
        this.toggle = this.toggle.bind(this);
        //this.getDataCentros = this.getDataCentros.bind(this);
        //this.intervalIdTag2 = this.getCentros.bind(this);
        this.tagservices = new TagServices();    
        // this.toggleModal1 = this.toggleModal1.bind(this);
        this.timerRef=React.createRef();
    }

  
    componentDidMount = () => {
        this.getCompanys()
        window.scroll(0, 0);
        if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c2"){
          active_config="block";
        }else if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c3"){
          active_config2="block"
        }
        // this.getUbicacion();
        //this.setState({isLoading: true})
        //this.setState({isLoading: false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        //this.setState({ isLoading: true });
        //this.intervalIdTag1 = setInterval(() => this.updatePromedios(),210000);
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
            //this.getDataCentros();
        }
        this.timerRef.current = setInterval(this.getCentros.bind(this),210000);
        this.getCentros();
      }

      componentWillUnmount = () => {
        clearInterval(this.getCentros);
        clearInterval(this.timerRef.current);
     }  

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        //this.getSondas(centro[0].toString());
        if(c!=0)
          this.setState({redirect:true})
        c++;
      }

      ingresarCentroUsuario = (centro) =>{
          let {setCentroUsuario} = this.props;
          setCentroUsuario(centro);
      }

      toggle(e) {
        let event = e.target.dataset.event;
        this.setState({ collapse: this.state.collapse === Number(event) ? 0 : Number(event) });
      }

      divideNameZone(nameAddress){
        // console.log(nameAddress)
        let namezone0="";
        if(nameAddress.includes('Salinidad')){
          namezone0=nameAddress.split(' ')
          return namezone0[0]
        }
        namezone0=nameAddress.split('_')
        // console.log(namezone0.)
        let namezone1=String(namezone0[1]).split(' ')
        
        return String(namezone1[0])
        
      }

      getPriority(status){
        if(status){
          if(status=="sin datos"){
            return 1
          }else if(status=="pegados"){
            return 2
          }else if(status=="en cero"){
            return 3
          }
          else if(status=="salinidad 33" || status=="salinidad 34"){
            return 4
          }
        }else{
          return 5
        }
      }

      newLocations(tags,w){
        return tags.filter(t=>t.active==true).map(t=>{
          if(t.shortName.includes('sal')){
            return {
              id:t._id,
              name:t.nameAddress,
              locationId:t.locationId,
              prom:t.prom,
              max:t.max,
              min:t.min,
              centroId:w._id,
              ultimo_valor:t.lastValueWrite,
              ultima_fecha:t.dateTimeLastValue,
              alerta_max:t.alertMax,
              alerta_min:t.alertMin,
              zoneId:t.zoneId,
              status:t.locationId.status_sal?t.locationId.status_sal:'ok',
              position:this.getPriority(t.locationId.status_sal)
            }
          }else{
            return {
              id:t._id,
              name:t.nameAddress,
              locationId:t.locationId,
              prom:t.prom,
              max:t.max,
              min:t.min,
              centroId:w._id,
              ultimo_valor:t.lastValueWrite,
              ultima_fecha:t.dateTimeLastValue,
              alerta_max:t.alertMax,
              alerta_min:t.alertMin,
              zoneId:t.zoneId,
              status:t.locationId.status?t.locationId.status:'ok',
              position:this.getPriority(t.locationId.status)
            }
          }
        })
      }

      getCentros=async()=>{
          this.setState({Mis_Prom2:[]})
          let token=sessionStorage.getItem("token_cloud")
          try{
            this.tagservices.getUserToken(token).then(respuesta=>{
              let centros=respuesta.centros+respuesta.control;

              //let centros=respuesta.control;
              // if(this.state.Control){
              //   let control=respuesta.control;
              //   centros=""+control;
              // }
              this.tagservices.getWorkplaceUser(centros).then(workplace=>{
                let workplace_activos=workplace.filter(w=>w.active==true&&!w.code.includes('cnt_'));
                //console.log(workplace_activos)
                if(workplace_activos.length==0 && respuesta.control.length>0){
                  this.switchActive_principal(false,false,false,false,true,false);
                }
                try{
                  areas_centros=workplace.map(e=>{return e.areaId});
                  areas_centros=areas_centros.filter(this.onlyUnique)
                  if(this.state.Mis_Prom.length==0){
                    workplace.filter(w=>w.active==true).map((w)=>{
                      let Modulos=[];
                      this.tagservices.getTagsWorkplace(w._id,"get").then(respuesta=>{
                        let tags=respuesta.data;
                        tags=tags.filter(t=>t.active==true)
                        let locations=[];
                        let zones0=[]
                        let zones=[];
                        locations=this.newLocations(tags,w);
                        tags.map(t=>{
                          zones0.push(t.zoneId+this.divideNameZone(t.nameAddress))
                        })
                        zones0=zones0.filter(this.onlyUnique)
                        zones0.map(z=>{
                          let zone=tags.filter(t=>t.zoneId+this.divideNameZone(t.nameAddress)==z)[0]
                          zones.push(this.divideNameZone(zone.nameAddress))
                        })
                        zones=zones.filter(this.onlyUnique)
                        Modulos.push({centro:w.name,zonas:_.orderBy(zones)})
                        this.setState({Mis_Prom:this.state.Mis_Prom.concat(locations),
                                       Mis_Prom2:this.state.Mis_Prom2.concat(locations),
                                       Modulos:this.state.Modulos.concat(Modulos),
                                       tags_original:this.state.tags_original.concat(tags)
                                      })
                        // console.log(w.name)
                        // console.log(locations)
                        //console.log(tags)
                      }).catch(e=>{
                        //console.log(object)
                      })
                    })
                    this.setState({Centros:workplace,Centros2:_.orderBy(workplace,["name"],["asc"]),isLoading:false})
                  }else{
                    workplace.filter(w=>w.active==true).map((w)=>{
                      let Modulos=[]
                      this.tagservices.getTagsWorkplace(w._id,"get").then(respuesta=>{
                        let tags=respuesta.data;
                        tags=tags.filter(t=>t.active==true)
                        let locations=[];
                        let zones0=[]
                        let zones=[];
                        locations=this.newLocations(tags,w);
                        tags.map(t=>{
                          zones0.push(t.zoneId+this.divideNameZone(t.nameAddress))
                        })
                        zones0=zones0.filter(this.onlyUnique)
                        zones0.map(z=>{
                          let zone=tags.filter(t=>t.zoneId+this.divideNameZone(t.nameAddress)==z)[0]
                          zones.push(this.divideNameZone(zone.nameAddress))
                        })
                        zones=zones.filter(this.onlyUnique)
                        Modulos.push({centro:w.name,zonas:zones})
                        this.setState({Mis_Prom2:this.state.Mis_Prom2.concat(locations)})
                        // console.log(w.name)
                        // console.log(locations)
                        //console.log(tags)
                      }).catch(e=>{
                        //console.log(object)
                      })
                    })
                    this.setState({Centros2:_.orderBy(workplace,["name"],["asc"])})
                  }
                }
                catch(e){console.log(e)}
                
                
                
              })
            }).catch(e=>e)
          }catch(e){
            //console.log(e)
          }
          
      }

      // toggleModal1(ModalState,_id,title) {
      //   if(!ModalState){
      //     Buscar2=""
      //     this.setState({ModalState,Centro_Actividad:[]})
      //   }
      //   ModalActive=ModalState;
      //   this.setState({ModalState})
      //   _id_centro=_id;
      //   title_centro=title;
      // }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    endDate(_id){
      let data=this.state.Centros2.filter(e=>e._id==_id && e.endDate!="").map((e)=>e.endDate)
      let data2=this.state.Centros2.filter(e=>{return e._id==_id}).map(e=>{
        // fecha.setHours(fecha.getHours()-1);
        return e.endDate
      })
      //alert(data2[0])
      if(data2[0]!=null && data[0]!=null){
        return moment(data2[0]).format("HH:mm DD-MMM")
      }else{
        return <Badge color="secondary" pill>sin registros actualizados</Badge>
      }
    }

    endDate2(_id){
      let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let color="", color2="";
      let data=this.state.Centros2.filter(e=>e._id==_id && e.endDate!="").map((e)=>{
        color=this.getColorAlert(e,"oxd",100,miscolores,0);
        color2=this.getColorAlert(e,"oxd",200,miscolores,0);
        return e.endDate
      })
      let data2=this.state.Centros2.filter(e=>e._id==_id).map(e=>{
        var fecha=new Date(e.endDate);
        // fecha.setHours(fecha.getHours()-1);
        return fecha
      })
      let myId="status"+_id;
      if(data[0]!=null && data2[0]!=null){
        let fecha=new Date();
        let f1=data2[0];
        let f2=new Date(fecha.setHours(fecha.getHours()-1));
        let f3=new Date(fecha.setHours(fecha.getHours()-12));
        if(color!="" || color2!=""){
          //alert(color);
          return <Fragment>{this.createTooltip(myId,"Registros en 0")}
           <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        }
        else if(f1>=f2){
          //alert(f2)
          return <Fragment>{this.createTooltip(myId,"En linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(f1>=f3){
          return <Fragment>{this.createTooltip(myId,"En linea en las 12hrs.")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-dark">Dark</div></Fragment>
        }
        // else if(f1<moment(f2).format("HH:mm DD-MMM") && f1>=moment(f3).format("HH:mm DD-MMM")){
        //   return <Fragment>{this.createTooltip(myId,"En linea dentro de las 24 hrs")}
        //   <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        // }
        else{
          return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
        }
      }else{
        return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
        <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
      }
    }

    inputChangeHandler = (event) => { 
      // console.log(event.target.value);  
      this.setState( { 
          ...this.state,
          [event.target.id]: event.target.value
      } );
  }

  getName(name){
    if(name.includes("OD ")){
      let new_name="";
      if(name.includes("oxd")){
        new_name=name.replace("oxd", "");
        return "Oxd de "+new_name
      }
      if(name.includes("temp")){
        new_name=name.replace("temp", "");
        return "Oxd de "+new_name
      }
      if(name.includes("oxs")){
        new_name=name.replace("oxs", "");
        return "Oxd de "+new_name
      }
      if(name.includes("sal")){
        new_name=name.replace("sal", "");
        return "Oxd de "+new_name
      }
    }else{
      return name
    }
  }

  getColorAlert(c,type,modulo,miscolores,ind){
    let alarma=0;
    let color="";
    if(modulo==900){
      modulo="Ponton"
    }
    if(modulo==1000){
      modulo="MODULOS"
    }
    this.state.Mis_Prom2.filter((l)=>c._id==l.centroId&&l.name.includes(type)&&l.name.includes(modulo)&&!l.name.includes("VoltajeR")).map((l)=>{
      //console.log(`name:${l.name} val:${l.ultimo_valor} max:${l.alerta_max} min:${l.alerta_min}`)

      if(ind!=0){
        if(l.ultimo_valor>=l.alerta_max){
          alarma=1;
        }
        if(l.ultimo_valor<=l.alerta_min){
          alarma=1;
        }
      }else{
        if(l.ultimo_valor<=0){
          alarma=1;
        }
      }
      
    })
    if(alarma==0){
      return color
    }
    else{
      color=miscolores[2];
      return color
    }
  }

  findOneEvent(workPlace){
    let val=this.state.Events.filter(e=>e.workplaceId==workPlace&&e.active==true)
    let c=val.length;
    return c
  }

  findActiveWorkplace(workPlace){
    let val=this.state.Centros2.filter(c=>c._id==workPlace&&c.active==true&&workPlace!="")
    let c=val.length;
    return c
  }

  getPar(name){
    let separador=name.split(' ');
    let separador2=String(separador[1]).split('_');
    let num=parseInt(separador2[0]);
    if(num%2==0){
      return "par"
    }else{
      return "impar"
    }
  }

  getNameCentro(name){
    let split=name.split('_Mod')
    return split[0]
  }

  getStatusColor(data,superior,inferior,setpoint){
    //console.log(superior)
    let media=data.alerta_max-data.alerta_min;
    media=media/2;
    media=data.alerta_min+media;
    //console.log(`ult${data.ultimo_valor} max${superior.ultimo_valor} min${inferior.ultimo_valor}`)
    // console.log(data.ultimo_valor<inferior.ultimo_valor)
    // console.log(data.ultimo_valor>=inferior.ultimo_valor && data.ultimo_valor<setpoint.ultimo_valor)
    // console.log(data.ultimo_valor>=superior.ultimo_valor)
    if(data.ultimo_valor!=undefined && inferior!=undefined && superior!=undefined ){
      if(data.ultimo_valor<inferior.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF0000"}}> </span>
      }
      if(data.ultimo_valor>=inferior.ultimo_valor && data.ultimo_valor<setpoint.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF8000"}}> </span>
      }
      if(data.ultimo_valor>=setpoint.ultimo_valor && data.ultimo_valor<superior.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FFFF00"}}> </span>
      }
      if(data.ultimo_valor>=superior.ultimo_valor){
        return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
      }
    }
   
    // if(data.ultimo_valor>=data.alerta_max || data.ultimo_valor<=data.alerta_min){
    //    return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF0000"}}> </span>
    // }else{
    //    return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
    // }
    //return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}></span>
    //return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
    // <span className="badge badge-pill badge-info" style={{backgroundColor:"#007bff"}}>Azul</span>
    // &nbsp;Niveles normales de oxigeno.&nbsp;
    // <span class="badge badge-pill badge-info" style={{backgroundColor:"#FFFF00",color: "#007bff"}}>Amarillo</span>
    // &nbsp;Ox. tendiente a la baja.&nbsp;
    // <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF8000",color:"#fff"}}>Naranjo</span>
    // &nbsp;Nivel Critico Ox.&nbsp;
    // <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF0000",}}>Rojo</span>
    // &nbsp;Ox. bajo nivel alarma.&nbsp;
  }

  onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
  }

  getEventEndView(workplaceId,miscolores){
    //console.log(this.state.EventCentros)
    let centro=[];
    try{
    centro=this.state.EventCentros.filter(e=>e.workplaceId==workplaceId)[0]
    //console.log(centro)
      return <ListGroupItem style={{textAlign:'center',backgroundColor:"#FADF87",fontSize:12,maxHeight:`${50}%`}}>
        <Row style={{marginTop:-10}}>
        {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
          <Col sm={3} ><i id={`oxd_divice`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Evento Activo <br></br> {moment(centro.dateTime).format('DD-MM-YYYY hh:mm')}</Col>
          <Col sm={9} >{centro.detail}</Col>
        </Row>
      </ListGroupItem>
    }catch(e){
      //console.log(e)
    }
  }

  mostrar(){
    let mostrar="none"
    if(this.state.Abiotic || this.state.Control){
      mostrar="block"
    }
    return mostrar
  }

  getCardCentros(miscolores,collapse,centros){
    centros=centros.filter(c=>{
        let posicion1=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==1).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==1):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==1)
        let posicion2=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==2).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==2):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==2)
        let posicion3=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==3).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==3):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==3)
        let posicion4=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==4).length>0?
                      this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.position==4):
                      this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.position==4)
        if(posicion1.length>0){
          c.position=1
        }else if(posicion2.length>0){
          c.position=2
        }else if(posicion3.length>0){
          c.position=3
        }else if(posicion4.length>0){
          c.position=4
        }
        if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
          return c.areaId==Buscar
        }
        if(this.state.Control){
          return c.name.includes(' Control')
        }
        if(this.state.Diagnostico){
          return c
        }
        if(Buscar=="" && this.state.Control==false){
          return !c.name.includes(' Control')
        }
    })
    // this.filterEventEnd(centros.map(c=>{return {workplaceId:c._id}}))
    centros=this.state.Diagnostico?
      _.orderBy(centros.filter(c=>{
        if(c.areaId==Buscar&&Buscar!=""){
          return c.areaId==Buscar
        }if(Buscar==""){
          return c
        }
      }),['position'],['asc']):
      _.orderBy(centros,["name"],["asc"])
    return centros.map((c,index)=>{
      return(
        <Col md={this.state.Diagnostico?3:4} lg={this.state.Diagnostico?2:4} style={{display:this.findActiveWorkplace(c._id)>0?"block":"none"}}>
          <Card className="main-card mb-5 mt-3" key={index} >
              <CardHeader className="card-header-tab  ">
              <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
              {this.createTooltip(`detalle${c._id}`,"Mas Detalles de Centros")}
                    <a style={{cursor:"pointer"}} onClick={(()=>{
                      if(!this.state.Piscicultura){
                        sessionStorage.setItem('workplace', JSON.stringify({"id":c._id,"name":c.name,"active":c.active}));
                        this.ingresarCentroUsuario(`${c._id}/${c.name}`);
                        this.setState({redirect:true})
                      }
                    })} id={`detalle${c._id}`}>
                    <div class="icon-wrapper rounded-circle zoom" >
                        <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                        <i class="lnr-apartment text-white" style={{display:`${this.state.Piscicultura?"none":"block"}`}}></i>
                        <i class="pe-7s-anchor text-white" style={{display:`${this.state.Piscicultura?"block":"none"}`}}></i>
                    </div>
                    </a>
                    <div class="widget-chart-content font-size-lg font-weight-normal" style={{marginTop:30}}>
                    <p>{c.name}
                    {/* {this.createTooltip(`event_${c._id}`,"Ver Eventos")}
                    <Button id={`event_${c._id}`} color="link" style={{marginLeft:`${90}%`,marginTop:-50,visible:`${this.state.Piscicultura?"hidden":"visible"}`}}
                    pill onClick={((e)=>{
                      //alert(c._id)
                      // this.filterEvent(c._id)
                      this.toggleModal1(true,c._id,c.name)
                    })}> <i style={{fontSize:20,cursor:"pointer",color:`${c.event==true?"orange":"silver"}`}} className="pe-7s-bell zoom" ></i></Button> */}
                    </p>
                    </div>
              </div>

              <div className="btn-actions-pane-right text-capitalize" style={{display:`${this.state.Piscicultura?"none":"block"}`}}>
                  {/* {c.endDate} */}
                  {!this.state.Equipos?this.endDate(c._id):this.endDate2(c._id)}
              </div>                           
              </CardHeader>
              <ListGroup style={{cursor:"pointer"}}>
              {/* <div style={{display:`${this.mostrar()}`}}> */}
                    {/* {
                      this.state.Modulos.filter(m=>m.centro==c.name).map((m)=>{
                        let zonas = m.zonas;
                        return zonas.map((z)=>{
                          let modul=z.replace("Modulo ","")
                          return this.getAcordion(index,miscolores,collapse,c,modul,this.state.Mis_Prom)
                        })
                      })
                    } */}
                    {/* {this.getEventEndView(c._id,miscolores)}
                </div> */}
                {this.state.Diagnostico&&(
                <div style={{marginLeft:-1,marginRight:-1}}>
                  <Diagnostico
                    numero={900}
                    zonas={this.state.Modulos.filter(m=>m.centro==c.name)}
                    dispositivos={this.state.Mis_Prom}
                    dispositivos2={this.state.Mis_Prom2}
                    centro={c}
                    indice={index}
                  />
                </div>
                )}
              </ListGroup>
          </Card>

      </Col>
      )  
    })
  }
  
  switchActive_principal(nombre_menu){
    let menu=["Abiotic","Meteo","Proceso","Equipos","Control","Piscicultura","Diagnostico"]
    let data={}
    menu.map(m=>{
      if(m==nombre_menu){
        data[m]=true
      }else{
        data[m]=false
      }
    })
    console.log(data)
    this.setState(data)
  }

  handleChangeArea = event => {
      const name = event.target.name;
      const value = true;
      //console.log(name)
      areas_centros.map((a,i)=>{
        if(name=="btn"+i){
          this.setState({ [name]: value });
        }else{
          this.setState({ ["btn"+i]: false });
        }
      })
  }

  generateAreasCentros(){
    try{
      return areas_centros.map((area,i)=>{
        return ubicaciones.filter(u=>u._id==area).map((a)=>{
            return <Button value={a.name} name={"btn"+i} color="primary" onClick={((e)=>{
              Buscar=a._id;
              active_todos=false;
              this.handleChangeArea(e)
            })} onChange={e => this.handleChangeArea(e)} active={this.state["btn"+i]} outline>{a.name}</Button>
        })
      })
    }catch(e){
      //console.log(e)
    }
  }

  getCompanys(){
    this.tagservices.crudCompany("",{},'GET').then(respuesta=>{
      if(respuesta.statusCode===201 || respuesta.statusCode===200){
        this.setState({Companys:respuesta.data})
      }
    })
  }

  handleChangeSelect = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value });
  }

    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        if(this.state.redirect2){
          return <Redirect to="/dashboards/configuracion"></Redirect>
        }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
             
         ///***********************************+++ */
        return (
            <Fragment>
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Resumen</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      <BreadcrumbItem active tag="span">Variables</BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{display:active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion</Badge></a></BreadcrumbItem>
                      <Input type={'select'} name="selectCompany" onChange={this.handleChangeSelect} style={{maxWidth:250,marginLeft:20,display:active_config2}}>
                          <option value={""} disabled>-- Seleccione una Empresa --</option>
                          <option value={""} >Todas las Empresa</option>
                          {
                            this.state.Companys.map(company=>{
                              return <option value={company._id}>{company.name}</option>
                            })
                          }
                        </Input>
                      </Breadcrumb>
                       
                       <Button active={active_todos} name={"btn-1"} color="primary" outline onClick={((e)=>{
                          Buscar="";
                          active_todos=true;
                          this.handleChangeArea(e)
                        })} onChange={e => this.handleChangeArea(e)} >Todos</Button>
                        {
                            this.generateAreasCentros()
                            
                        }
                     <Row>
                     <Col md="12">
                            <Card className="main-card mb-1 p-0">
                            <CardHeader className="card-header-tab" style={{height:50}}>
                                  <div style={{marginLeft:-20}}>
                                      {active_config2=="block"&&(
                                        <Button 
                                        className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Diagnostico?"":"#545cd8"}`}} outline color={miscolores[0]}
                                        onClick={()=>{
                                          this.switchActive_principal('Diagnostico');
                                        }}>  
                                            {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                            <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                            {/* {data.measurements[data.measurements.length-1].value   }  */}
                                            <span className="opacity-6  pl-0 size_unidad">  </span>
                                            <font style={font} color={!this.state.Diagnostico?"#6c757d":"#fff"}>&nbsp;&nbsp;Diagnostico&nbsp;&nbsp;</font>
                                            </div>
                                            <div className="widget-subheading">
                                                {/* {data.shortName} */}
                                                </div>
                                        </Button>
                                      )}  
                                  </div>
                                </CardHeader>
                                <CardBody className="p-3"  style={{display:`${this.state.Piscicultura?"none":"block"}`}}>
                                {/* {console.log(this.state.Mis_Prom)} style={{display:`${this.state.Control?"none":"block"}`}} */}
                                    <div style={{display:`${!this.state.Control?"none":"block"}`}}>
                                        <span className="badge badge-pill badge-info" style={{backgroundColor:"#007bff"}}>Azul</span>
                                        &nbsp;Niveles normales de oxigeno.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FFFF00",color: "#007bff"}}>Amarillo</span>
                                        &nbsp;Ox. tendiente a la baja.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF8000",color:"#fff"}}>Naranjo</span>
                                        &nbsp;Nivel Critico Ox.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF0000",}}>Rojo</span>
                                        &nbsp;Ox. bajo nivel alarma.&nbsp;
                                    </div>
                                    {this.state.Diagnostico&&(
                                      <div>
                                          <span className="badge badge-pill badge-success">OK</span>
                                          &nbsp;Datos OK.&nbsp;
                                          <span class="badge badge-pill badge-warning" style={{backgroundColor:'yellow',color:'blue'}}>Pegados</span>
                                          &nbsp;Datos Pegados.&nbsp;
                                          <span class="badge badge-pill badge-warning">En Cero</span>
                                          &nbsp;Datos en Cero.&nbsp;
                                          <span class="badge badge-pill badge-danger">Sin Datos</span>
                                          &nbsp;Sin Datos.&nbsp;
                                          <span class="badge badge-pill badge-warning" style={{backgroundColor:'#59B6F6',color:'black'}}>Salinidad</span>
                                          &nbsp;Salinidad 33 o 34.&nbsp;
                                      </div>
                                    )}
                                    <Row>
                                      {/* {console.log(this.state.selectCompany)} */}
                                      {this.getCardCentros(miscolores,collapse,this.state.Centros2
                                        .filter(c=>this.state.selectCompany!=""?c.companyId==this.state.selectCompany:c))}
                                    </Row>
                                </CardBody>
                                {/* <CardBody style={{display:`${this.state.Control?"block":"none"}`}}>
                                    <Control />
                                </CardBody> */}
                            </Card>

                        </Col>
                    </Row>
                      {/* <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={ModalActive} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                                <ButtonGroup>
                                                <Input placeholder="Evento..." value={Buscar2} onChange={((e)=>{
                                                  Buscar2=e.target.value;
                                                  this.handleChangeActivity(e)
                                                })}></Input>
                                                <Button color="secondary" disabled>Buscar</Button>
                                                </ButtonGroup>
                                                
                                                <ReactTable                                                           
                                                data={_.orderBy(this.state.Centro_Actividad,["dateTime"],["desc"]).filter((data)=>{
                                                  if(Buscar2!=""){
                                                      return data.detail.toLocaleUpperCase().includes(Buscar2.toLocaleUpperCase())
                                                  }else{
                                                    return data
                                                  }
                                                }).map((c,i)=>{
                                                  return {
                                                      ind:i+1,
                                                      _id:c._id,
                                                      detail:c.detail,
                                                      type:this.getDetailActivity(c.activityId),
                                                      dateTime:moment(c.dateTime).format("HH:mm DD-MMM"),
                                                      workplaceId:c.workplaceId,
                                                      activityId:c.activityId,
                                                      active:c.active,
                                                  }
                                              })}
                                                          
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={10}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                          Header: "asunto",
                                                          width: 350,
                                                          Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={"info"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.type}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "detalle",
                                                        accessor: "detail",                                                              
                                                        width: `${78}%`
                                                        },
                                                        {
                                                        Header: "estado",                                                             
                                                        width: 150,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={row._original.active?"warning":"success"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.active?"activo":"finalizado"}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                    ]                                                             
                                                }
                                                 
                                                className="-striped -highlight"
                                                />
                                          </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal> */}
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({
  setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PanelDiagnostico);
