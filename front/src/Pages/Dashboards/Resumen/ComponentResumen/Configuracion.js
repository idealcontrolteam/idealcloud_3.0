import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,UncontrolledCollapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter, CardText,InputGroupText
  , CustomInput } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {
    toast

} from 'react-toastify';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import '../../../../assets/icon/style.css';
import Swal from 'sweetalert2'
import Label from 'reactstrap/lib/Label';
import { parseInt } from 'lodash';
import Jumbotron from 'reactstrap/lib/Jumbotron';
import CardTitle from 'reactstrap/lib/CardTitle';

sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
try{
    empresas=empresas.filter(e=>e.active==true).map(e=>e);
}catch(e){
    // console.log("empresa")
}

let c=0;


let title_centro="";
let code_centro="";
let Buscar="";
let active_todos=true;
let check=true;
let check2=false;
let active_config="none";
let u_ids=[];
let modal1=false;
let filtro_id=""
let name_Workplace="";
let viewExport=false;
class Resumen extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Modulos:[],
          isLoading:false,
          Mis_Prom:[],
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          redirect:false,
          redirect2:false,
          Actividad:[],
          Actividad2:[],
          Centro_Actividad:[],
          Centro_ActividadU:[],
          active_grafic:"none",
          ModalState:false,
          TypeGraphic:"1",
          TypeGraphic2:"2",
          Centros:[],
          Activity:true,
          Act_cen:false,
          Centro:false,
          modal1:false,
          txtActividad:"",
          txtActividad2:"",
          txtActividad3:"",
          txtCenActividad:"",
          Centros_Category:[],
          Areas:[],
          txtCentro:"",
          automatico:false,
          cantidadZonas:[],
          cantidadDispositivos:[],
          DataDetail:[],
        };     
        this.handleChange =this.handleChange.bind(this);
        this.tagservices = new TagServices();        
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        let rol=sessionStorage.getItem("rol");
        //console.log(rol)
        if(rol=="5f6e1ac01a080102d0e268c2"||rol=="5f6e1ac01a080102d0e268c3"){
          active_config="block";
        }
        // if(this.props.Centros!=null){
        //     active_config="none"
        // }
        //alert(JSON.stringify(this.props))

        // this.getUbicacion();
        this.setState({isLoading: false,Centros:empresas})
        //this.setState({isLoading: false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        //this.setState({ isLoading: true });
        let separador=this.props.Centros!=null?this.props.Centros:this.props.centroUsuario;
        //console.log("sep "+separador)
        let centro=separador.split('/');
        this.getCentro_Actividad(centro[0])
        this.filtrar(centro[0],centro[1])
        this.getCentrosCategory();
        this.getAreas();
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
        this.getActivity();
        //this.getCentro_Actividad();
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
        
     } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        if(this.props.Centros==null){
            let separador=nextProps.centroUsuario;
            let centro=separador.split('/');
            this.filtrar(centro[0],centro[1])
            this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        }
        //this.getSondas(centro[0].toString());
        // if(c!=0)
        //   this.setState({redirect:true})
        // c++;
      }

    switchModal(n){
        if(Math.sign(n)===0 || Math.sign(n)===1){
            this.setState({["modal"+n]:true})
        }else{
            let num=n*-1
            this.setState({["modal"+num]:false})
        }
    }

    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    getAreas(){
        this.tagservices.getAreas().then(respuesta=>{
            this.setState({Areas:respuesta})
        }).catch(e=>console.log(e))
    }

    getCentrosCategory(){
        let token=sessionStorage.getItem("token_cloud")
        this.tagservices.getUserToken(token).then(respuesta=>{
            //console.log(respuesta.company)
            let company=respuesta.companyId;
            this.tagservices.getWorkplaceCompany(company).then(workplace=>{
                this.setState({Centros_Category:workplace.map((w,i)=>{
                    w.ind=i+1;
                    return w;
                })})
            })
        }).catch(e=>console.log(e))
    }

    getCentro_Actividad(id){
        axios
        //.post(API_ROOT+"/"+empresa+"/centros_activos",{
        .get(API_ROOT+`/event/workplace/${id}`)
        .then(response => {   
            // alert(response.data.statusCode==200);
            if(response.data.statusCode==200){
                let centros=response.data.data;
                
                
                let mycentros=centros.map((c,i)=>{
                    return {
                        ind:i+1,
                        id:c._id,
                        detail:c.detail,
                        dateTime:moment(c.dateTime).format("HH:mm DD-MMM"),
                        workplaceId:c.workplaceId,
                        activityId:c.activityId,
                        active:c.active,
                        estado:c.active?"activo":"finalizado"
                    }
                })

                let u_activity=centros.map((c)=>{
                    return c.activityId
                })

                u_activity=u_activity.filter(this.onlyUnique);
                let val=[];
                val=this.state.Actividad.filter(function(e) {
                    return this.indexOf(e._id) < 0;
                    },u_activity)
                    
                let new_centros=u_activity.map((u,i)=>{
                    console.log(val)
                    return this.getMyActividades(u,centros,i)
                })
                
                this.setState({Centro_Actividad:mycentros,Centro_ActividadU:new_centros,Actividad2:this.state.Actividad})
            }
            
        })
        .catch(error => {
            console.log(error)
        });
        //console.log(this.state.)
    }

    getMyActividades(id,array,i){
        let act=array.filter(c=>c.activityId==id).map((c)=>{
            return {
                ind:i+1,
                id:c._id,
                detail:this.getDetail(c.activityId),
                dateTime:moment(c.dateTime).format("HH:mm DD-MMM"),
                workplaceId:c.workplaceId,
                activityId:c.activityId,
                active:c.active,
            }
        })
        return act[0]
    }

    getDetail(activityId){
        let act=this.state.Actividad.filter(a=>a._id==activityId).map((a)=>{
            return a.detail
        })
        //console.log(act)
        return act[0]
    }

    getActivity(){
        axios
        //.post(API_ROOT+"/"+empresa+"/centros_activos",{
        .get(API_ROOT+`/activity/all`)
        .then(response => {   
            // alert(response.data.statusCode==200);
            if(response.data.statusCode==200){
                let actividades=response.data.data;
                let new_actividades=actividades.map((a,i)=>{
                    return {
                        ind:i+1,
                        _id:a._id,
                        detail:a.detail,
                        dateTime:moment(a.dateTime).format("HH:mm DD-MMM"),
                        visible:"block"
                    }
                })
                this.setState({Actividad:new_actividades,Actividad2:new_actividades.filter(a=>a.true)})
            }
            
        })
        .catch(error => {
            console.log(error)
        });
    }
      
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
    }

    filtrar=(code,name)=>{
        //const {TagsSelecionado} = this.state;
        // let code=NameWorkplace.id
        // let name=TagsSelecionado[0].name
        
        try{
            if(code!=undefined){
                //alert(JSON.stringify(NameWorkplace.id))
                name_Workplace=name;
                viewExport=true;
                this.getCentro_Actividad(code)
            }
        }catch(e){
            //console.log(e)
            viewExport=false;
            this.getActivity()
            this.setState({Centro_Actividad:[],Centro_ActividadU:[]})
        }

    }

    renderSelectWorkplace = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        isMulti
                        styles={{width:200}}
                        name="colors"
                        options={Tags}
                        onKeyDown={this.handleKeyDown}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Centro"
                        onChange={this.handleChange}
                    />
                    <Input placeholder="Filtra actividad" value={this.state.txtActividad3} name="txtActividad3" onChange={this.handleChangeActivity} />
                </FormGroup>
          </div>
        );
      }

    switchActivity=(Activity,Act_cen,Centro)=>{
        this.setState({Activity,Act_cen,Centro});
    }

    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            const {TagsSelecionado,NameWorkplace} = this.state;
            try{
                this.filtrar(TagsSelecionado[0].code,TagsSelecionado[0].name);
            }catch(e){
                this.filtrar(NameWorkplace.id,NameWorkplace.name);
            }
            
        }
    }

    handleKeyDown2 = (e) => {
        if (e.key === 'Enter') {
            //this.createEvent(this.state.NameWorkplace.id,filtro_id,this.state.txtCenActividad);
            const {TagsSelecionado,NameWorkplace} = this.state;
            try{
                this.createEvent(this.state.TagsSelecionado[0].code,filtro_id,this.state.txtCenActividad)
            }catch(e){
                this.createEvent(this.state.NameWorkplace.id,filtro_id,this.state.txtCenActividad)
                this.filtrar(NameWorkplace.id,NameWorkplace.name);
            }
            this.setState({txtCenActividad:""})
        }
    }

    handleKeyDown3 = (e) =>{
        if(this.state.txtActividad!=""){
            if (e.key === 'Enter') {
            this.createActivity(this.state.txtActividad)
            this.setState({txtActividad:""})
            }
        }
    }

    handleKeyDown4 = (e) =>{
        if(this.state.txtActividad!=""){
            if (e.key === 'Enter') {
            //this.createActivity(this.state.txtActividad)
            //this.setState({txtActividad:""})
            }
        }
    }

    createEvent(workplaceId,activityId,name){
        let detail="";
        if(name!=null && name!=""){
            detail=name
        }else{
            detail=this.getDetail(activityId)
        }
        let fecha=new Date();
        //alert(moment(fecha).format('YYYY-MM-DDTHH:mm:ss'))
        axios
        .post(API_ROOT+`/event`,
            {
                "detail": detail,
                "dateTime": moment(fecha).format('YYYY-MM-DDTHH:mm:ss'),
                "n_visit": 0,
                "activityId": activityId,
                "workplaceId": workplaceId,
                "active":true
            }
        )
        .then(response => {   
            // alert(response.data.statusCode==200);
            const {TagsSelecionado,NameWorkplace} = this.state;
            try{
                this.filtrar(TagsSelecionado[0].code,TagsSelecionado[0].name);
            }catch(e){
                this.filtrar(NameWorkplace.id,name_Workplace.name);
            }
        })
        .catch(error => {
            console.log(error)
        });
    }

    createActivity(name){
        let fecha=new Date();
        //alert(moment(fecha).format('YYYY-MM-DDTHH:mm:ss'))
        axios
        .post(API_ROOT+`/activity`,
            {
                "detail":name,
                "dateTime":moment(fecha).format('YYYY-MM-DDTHH:mm:ss'),
                "active":true
            }
        )
        .then(response => {
            this.getActivity();
        })
        .catch(error => {
            console.log(error)
        });
    }

    updateEvent(id,value,id_centro,accion){
        let obj=[]
        if(accion=="active"){
            obj.push({"active":value})
        }
        if(accion=="detail"){
            obj.push({"detail":value})
        }
        if(accion=="array"){
            obj.push({"active":value})
            let event=this.state.Centro_Actividad.filter(c=>c.activityId==id&&c.workplaceId==id_centro&&c.active==true)
            if(event.length>0){
                toast['success']('La actividad fue finalizada', { autoClose: 4000 })
                event.map(c=>{
                    this.tagservices.updateEvent(c.id,obj[0]).then(response=>{
                        this.getCentro_Actividad(id_centro);
                    }).catch(e=>toast['danger']('Ops.. Ocurrio un error', { autoClose: 4000 }))
                })
            }else{
                toast['warning']('Actividad ya finalizada', { autoClose: 4000 })
            }
        }else{
            this.tagservices.updateEvent(id,obj[0]).then(response=>{
                this.getCentro_Actividad(id_centro);
            }).catch(e=>e)
        } 
    }

    updateActivity(id,value,accion){
        let obj=[]
        if(accion=="detail"){
            obj.push({"detail":value})
        }
        this.tagservices.updateActivity(id,obj[0]).then(response=>{
            this.getActivity();
        }).catch(e=>e)
    }

    deleteActivity(id){
        axios
        .delete(API_ROOT+`/activity/${id}`)
        .then(response => {
            if(response.data.statusCode===200){
                toast['success']('La actividad fue eliminada', { autoClose: 4000 })
                this.getActivity();
            }          
        })
        .catch(error => console.log(error));    
    }

    deleteEvent(id,centro){
        axios
        .delete(API_ROOT+`/event/${id}`)
        .then(response => {
            if(response.data.statusCode===200){
                toast['success']('El evento fue eliminado', { autoClose: 4000 })
                this.getCentro_Actividad(centro);
            }
        })
        .catch(error => console.log(error));    
    }

    activityActive(activity,workplace){
        let active=this.state.Centro_Actividad.filter(c=>c.activityId==activity&&c.workplaceId==workplace&&c.active==true)
        if(active.length>0){
            return <Badge color="warning" style={{marginLeft:0}} onClick={((e)=>{})} pill >Activos</Badge>
        }else{
            return <Badge color="success" style={{marginLeft:0}} onClick={((e)=>{})} pill >Finalizados</Badge>
        }
    }


    handleChangeActivity = event => {
            const name = event.target.name;
            const value = event.target.value;
            //console.log(value)
            this.setState({ [name]: value });
    }

    handleChangeCheck = event => {
            const name = event.target.name;
            const value = event.target.checked;
            this.setState({ [name]: value });
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    activeCentro(id){
        //console.log(id)
        try{
            let centros=this.state.Centros_Category.filter(c=>c.code==id)
            return centros[0].active?
            <div style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div>:
            <div style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>
        }catch(e){
            //console.log(e)
        }   
    }

    areaCentro(id){
        try{
            let centros=this.state.Centros_Category.filter(c=>c.code==id)
            let area=this.state.Areas.filter(a=>a._id==centros[0].areaId)
            return area[0].name
        }catch(e){
            //console.log(e)
        }
    }

    showAreas(){
        if(this.state.Areas!=[]){
            return this.state.Areas.map(a=>{
                return <option value={a._id}>{a.name}</option>
            })
        }else{
            return ""
        }
        
    }

    generateZonas(n){
        return <Card>
            <CardBody>
                <FormGroup row>
                    <Label for="code" sm={2}>*Nombre Zona {n}</Label>
                    <Col sm={4}>
                    <Input type="text" name={`zona${n}`} id={`txtZona${n}`} value={this.state["zona"+n]}  onChange={this.handleChangeActivity} placeholder="Ingrese Nombre" />
                    </Col>
                    <Label for="code" sm={1}>*Code</Label>
                    <Col sm={3}>
                        <Input type="text" name={`zoneCode${n}`} id={`txtZoneCode${n}`} value={this.state["zoneCode"+n]}  onChange={this.handleChangeActivity} placeholder="Ingrese Code" />
                    </Col>
                    <Col sm={2}>
                        <CustomInput type="switch" id={"zoneActivo"+n} value={this.state["zoneActivo"+n]}  onChange={this.handleChangeCheck} name={"zoneActivo"+n} label={this.state["zoneActivo"+n]?"Activo":"Inactivo"} />
                        <div style={{fontSize:20,cursor:"pointer"}}>
                            <Badge color="danger" pill onClick={()=>{
                                this.setState({cantidadZonas:this.state.cantidadZonas.filter((c,i)=>i+2!=n)})
                            }}>-</Badge>
                        </div>
                    </Col>
                    
                </FormGroup>
                <Button color="success" id={"disp"+n}>Añadir Dispositivos</Button>
                <UncontrolledCollapse toggler={"disp"+n}>
                    <ListGroup>
                        <ListGroupItem>
                            {this.generaLocations(1,n)}
                            {
                                this.state.cantidadDispositivos.filter(c=>c.id==n).map((c,i)=>{
                                    return this.generaLocations(i+2,n)
                                })
                            }
                            <div style={{fontSize:20,cursor:"pointer"}}>
                                <Badge color="success" pill onClick={()=>{
                                    this.setState({cantidadDispositivos:this.state.cantidadDispositivos.concat({id:n})})
                                }}>+</Badge>
                            </div>
                        </ListGroupItem>
                    </ListGroup>
                </UncontrolledCollapse>
            </CardBody>
        </Card>
    }

    generaLocations(n,z){
        if(!this.state.automatico){
            return <FormGroup row>
                <Label for="code" sm={2}>*Nombre {this.state.type=="monitoreo"?"Dispositivo":"Jaula"}{n}</Label>
                <Col sm={4}>
                <Input type="text" name={`disp${n}_${z}`} id={`txtDisp${n}_${z}`} value={this.state["disp"+n+"_"+z]}  onChange={this.handleChangeActivity} placeholder="Ingrese Nombre" />
                </Col>
                <Label for="code" sm={1}>*Code</Label>
                <Col sm={3}>
                    <Input type="text" name={`dispCode${n}_${z}`} id={`txtDispCode${n}_${z}`} value={this.state["dispCode"+n]}  onChange={this.handleChangeActivity} placeholder="Ingrese Code" />
                </Col>
                <Col sm={2}>
                    <CustomInput type="switch" id={`txtDispActivo${n}_${z}`} value={this.state["dispActivo"+n+"_"+z]}  onChange={this.handleChangeCheck} name={`dispActivo${n}_${z}`} label={this.state["dispActivo"+n+"_"+z]?"Activo":"Inactivo"} />
                    <div style={{fontSize:20,cursor:"pointer"}}>
                        <Badge color="danger" pill onClick={()=>{
                            this.setState({cantidadDispositivos:this.state.cantidadDispositivos.filter((c,i)=>c.id==z&&n!=i+2)})
                        }}>-</Badge>
                    </div>
                </Col>
            </FormGroup>
        }else{
            return <FormGroup row>
                <Label sm={3}>N° de Sensores</Label>
                <Col sm={5}>
                    <Input name={`dispCantidad${n}_${z}`} id={`txtDispCantidad${n}_${z}`} value={this.state["dispCantidad"+n+"_"+z]}  onChange={this.handleChangeActivity} placeholder="Ingrese Cantidad" />
                </Col>
            </FormGroup>
        }
        
    }

    validateWorkplace(){
        if(this.state.code!=""&&this.state.name!=""&&this.state.area!=""&&
            this.state.code!=null&&this.state.name!=null&&this.state.area!=null){
            return true
        }else{
            return false
        }
    }

    registerWorkplace(){
        //console.log(this.state.code+"-"+this.state.name+"-"+this.state.area)
        //alert(this.state.cantidadZonas)
        //alert(this.state["zona"+1])
        let DataDetail=[];
        let cantidad_z=this.state.cantidadZonas.length;
        let cantidad_l=this.state.cantidadDispositivos.length;
        //alert(cantidad_z)
        let z=0;
        while(z<cantidad_z+1){
            let iden_z=z+1;
            let l=0;
            if(this.state["zona"+iden_z]!=undefined){
                // alert(this.state["zona"+iden_z])
                // alert(this.state["zoneCode"+iden_z])
                // alert(this.state["zoneActivo"+iden_z]==undefined?false:this.state["zoneActivo"+iden_z])
                
                let locations=[];
                while(l<cantidad_l+1){
                    let iden_l=l+1;
                    if(this.state["disp"+iden_l+"_"+iden_z]!=undefined){
                        // alert(this.state["disp"+iden_l+"_"+iden_z])
                        // alert(this.state["dispCode"+iden_l+"_"+iden_z])
                        // alert(this.state["dispActivo"+iden_l+"_"+iden_z])
                        locations.push({
                            name:this.state["disp"+iden_l+"_"+iden_z],
                            code:this.state["dispCode"+iden_l+"_"+iden_z],
                            activo:this.state["dispActivo"+iden_l+"_"+iden_z]==undefined?false:this.state["dispActivo"+iden_l+"_"+iden_z]
                        })
                    }
                    l++;
                }
                DataDetail.push({
                    name:this.state["zona"+iden_z],
                    code:this.state["zoneCode"+iden_z],
                    activo:this.state["zoneActivo"+iden_z]==undefined?"false":String(this.state["zoneActivo"+iden_z]),
                    dispositivos:locations,
                })
            }
            z++;
        }
        this.setState({DataDetail:DataDetail})

    }

    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        if(this.state.redirect2){
            return <Redirect to="/dashboards/resumen"></Redirect>
          }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        // const onRowClick = (state, rowInfo, column, instance) => {
        //     return {
        //         onClick: e => {
        //             console.log('It was in this column:', column)
        //         }
        //     }
        // }
             
         ///***********************************+++ */
        return (
            <Fragment>
                    <Modal style={{maxHeight:660,maxWidth:800}} isOpen={this.state.modal1} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}><b>Eventos</b></ModalHeader>
                            <ModalBody>
                            <Row>
                                <Col>
                                    <Card>
                                        
                                        <CardBody>
                                                <ButtonGroup style={{width:`${100}%`}}>
                                                    <Input placeholder="Evento..." onKeyDown={this.handleKeyDown2} value={this.state.txtCenActividad} name="txtCenActividad" onChange={this.handleChangeActivity}></Input>
                                                    <Button color="success" onClick={((e)=>{
                                                        //alert("buscar")
                                                        if(this.state.txtCenActividad!=""){
                                                            try{
                                                                this.createEvent(this.state.TagsSelecionado[0].code,filtro_id,this.state.txtCenActividad)
                                                            }catch(e){
                                                                this.createEvent(this.state.NameWorkplace.id,filtro_id,this.state.txtCenActividad)
                                                            }
                                                            this.setState({txtCenActividad:""})
                                                        }
                                                    })}>Crear</Button>
                                                </ButtonGroup>
                                                
                                                <ReactTable                                                           
                                                data={this.state.Centro_Actividad.filter(c=>{
                                                    if(this.state.txtCenActividad!=""){
                                                        return c.detail.toLocaleUpperCase().includes(this.state.txtCenActividad.toLocaleUpperCase())&&c.activityId==filtro_id
                                                    }else{
                                                      return c.activityId==filtro_id
                                                    }
                                                })}
                                                            
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={10}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "Fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                        Header: "detalle",                                                              
                                                        width: 200,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Input placeholder={row._original.detail} onKeyDown={((e)=>{
                                                                    if (e.key === 'Enter') {
                                                                        this.updateEvent(row._original.id,e.target.value,row._original.workplaceId,"detail")
                                                                    }
                                                                })}/>
                                                                {/* <Button color="link"  pill onClick={((e)=>{
                                                                    
                                                                })}><i style={{fontSize:20,cursor:"pointer",color:"orange"}} className="pe-7s-pen" ></i></Button> */}
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "estado",                                                             
                                                        width: 200,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={row._original.active?"warning":"success"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.active?"activo":"finalizado"}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "Accion",
                                                        id: "id",
                                                        width: 100,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                {this.createTooltip(`activo_${row._original.ind}`,"Activa Evento")}
                                                                <Badge id={`activo_${row._original.ind}`} color="link" style={{marginLeft:0}} onClick={((e)=>{
                                                                    this.updateEvent(row._original.id,true,row._original.workplaceId,"active")
                                                                })} pill>
                                                                <i class="pe-7s-attention" 
                                                                style={{display:active_config,cursor:"pointer",fontSize:25,color:"orange"}}></i></Badge>&nbsp;
                                                                {this.createTooltip(`finaliza_${row._original.ind}`,"Finaliza Evento")}
                                                                <Badge id={`finaliza_${row._original.ind}`} color="link" pill onClick={((e)=>{
                                                                    this.updateEvent(row._original.id,false,row._original.workplaceId,"active")
                                                                    //this.switchModal("1")
                                                                })}><i style={{fontSize:25,cursor:"pointer",color:"green"}} className="pe-7s-check" ></i></Badge>
                                                                <Badge id={`eliminar_${row._original.ind}`} color="link" pill onClick={((e)=>{
                                                                    Swal.fire({
                                                                        title: '¿Estas seguro?',
                                                                        text: "¡No podras revertir esto!",
                                                                        icon: 'warning',
                                                                        showCancelButton: true,
                                                                        cancelButtonText: 'Cancelar',
                                                                        confirmButtonColor: '#3085d6',
                                                                        cancelButtonColor: '#d33',
                                                                        confirmButtonText: 'Eliminar'
                                                                      }).then((result) => {
                                                                        if (result.isConfirmed) {
                                                                          this.deleteEvent(row._original.id,row._original.workplaceId)
                                                                        }
                                                                      })
                                                                })}><i style={{fontSize:25,cursor:"pointer",color:"red"}} className="pe-7s-close-circle" ></i></Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                    ]                                                             
                                                }
                                                 
                                                className="-striped -highlight"
                                                />
                                        </CardBody>
                                    </Card>
                                </Col>
                                
                            </Row>
                    
                            </ModalBody>
                            <ModalFooter>
                            
                    </ModalFooter>
                </Modal>
                <Modal style={{maxHeight:660,maxWidth:`770px`}} isOpen={this.state.modal2} size="xl">
                    <ModalHeader toggle={() => this.switchModal("-2")}><b>Registrar Centro</b> &nbsp;
                        <button style={{width:22,height:22}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                        <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                        <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                    </ModalHeader>
                    <ModalBody>
                        <form autoComplete="off">
                            <FormGroup row>
                                <Label for="code" sm={2}>*Code</Label>
                                <Col sm={5}>
                                <Input type="text" name="code" id="txtCode" value={this.state.code}  onChange={this.handleChangeActivity} placeholder="Ingrese Code" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="name" sm={2}>*Name</Label>
                                <Col sm={9}>
                                <Input type="text" name="name" id="txtName" value={this.state.name}  onChange={this.handleChangeActivity} placeholder="Ingrese Nombre" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="ubicacion" sm={2}>Ubicación</Label>
                                <Col sm={9}>
                                <Input type="text" name="ubicacion" id="txtUbicacion" value={this.state.ubicacion}  onChange={this.handleChangeActivity} placeholder="Ingrese Ubicación" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="ubicacion" sm={2}>*Tipo</Label>
                                <Col sm={9}>
                                <Input type="select" name="type" id="txtType" value={this.state.type}  onChange={this.handleChangeActivity}>
                                    <option value="0" selected disabled>Seleccione un Tipo</option>
                                    <option value="monitoreo">MONITOREO</option>
                                    <option value="control">CONTROL</option>
                                </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="area" sm={2}>*Area</Label>
                                <Col sm={9}>
                                <Input type="select" name="area" id="txtArea" value={this.state.area}  onChange={this.handleChangeActivity} placeholder="Ingrese Area">
                                    <option value="0" selected disabled>Seleccione un Area</option>
                                    {this.showAreas()}
                                </Input>
                                </Col>
                            </FormGroup>
                            <br></br>
                            <FormGroup>
                                <Label for="exampleCheckbox">Estado Centro</Label>
                                    <CustomInput type="switch" id="txtActivo" name={"activo"} value={this.state.activo}  onChange={this.handleChangeCheck} label="Inactivo" />
                            </FormGroup>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <FormGroup>
                            <Button color="primary" onClick={()=>{
                                let validate=this.validateWorkplace()
                                //alert(validate)
                                if(!validate){
                                    this.switchModal("-2")
                                    this.switchModal(3)
                                }else{
                                    toast['warning']('Porfavor no deje campos vacios', { autoClose: 4000 })
                                }
                                }}>Siguiente</Button>
                        </FormGroup>
                    </ModalFooter>
                </Modal>

                <Modal style={{maxHeight:660,maxWidth:`770px`}} isOpen={this.state.modal3} size="xl">
                    <ModalHeader toggle={() => this.switchModal("-3")}><b>Registrar Centro</b> &nbsp;
                        <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                        <button style={{width:22,height:22}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                        <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                    </ModalHeader>
                    <ModalBody>
                        <form autoComplete="off">
                            <FormGroup>
                                {/* <Label for="exampleCheckbox">Automatico</Label> */}
                                <CustomInput type="switch" id="txtSwitchAuto" value={this.state.automatico}  onChange={this.handleChangeCheck} name="automatico" 
                                label={this.state.automatico?"Automatico":"Manual"} />
                            </FormGroup>
                            {this.generateZonas(1)}
                            {
                                this.state.cantidadZonas.map((c,i)=>{
                                    return this.generateZonas(i+2)
                                })
                            }
                            <div style={{fontSize:20,cursor:"pointer"}}>
                                <Badge color="success" pill onClick={()=>{
                                    this.setState({cantidadZonas:this.state.cantidadZonas.concat(1)})
                                }}>+</Badge>
                            </div>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>{
                            this.switchModal(2)
                            this.switchModal("-3")
                            }}>Anterior</Button>
                        <Button color="primary" onClick={()=>{
                            this.registerWorkplace()
                            this.switchModal("-3")
                            this.switchModal(4)
                            }}>Siguiente</Button>
                    </ModalFooter>
                </Modal>

                <Modal style={{maxHeight:660,maxWidth:`770px`}} isOpen={this.state.modal4} size="xl">
                    <ModalHeader toggle={() => this.switchModal("-4")}><b>Registrar Centro</b> &nbsp;
                        <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                        <button style={{width:22,height:22}} class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                        <button  class="mb-2 mr-2 badge badge-dot badge-dot-xl badge-primary">Primary</button>
                    </ModalHeader>
                    <ModalBody>
                        <form autoComplete="off">
                            <Card>
                            <CardTitle>Zonas</CardTitle>
                            {this.state.DataDetail.map(d=>{
                                return(
                                    <CardBody>
                                        <ListGroup>
                                            <ListGroupItem row>
                                                <Label sm={4}><b>nombre : {d.name}</b></Label>
                                                <Label sm={4}><b>code : {d.code}</b></Label>
                                                <Label sm={4}><b>estado : {d.activo}</b></Label>
                                            </ListGroupItem>
                                        </ListGroup>
                                        <CardTitle>Dispositivos</CardTitle>
                                        <ListGroup>
                                            {d.dispositivos.map(l=>{
                                                return(
                                                    <ListGroupItem row>
                                                        <Col sm={3}></Col>
                                                        <Label sm={3}>nombre : {l.name}</Label>
                                                        <Label sm={3}>codigo : {l.code}</Label>
                                                        <Label sm={3}>estado : {l.activo}</Label>
                                                    </ListGroupItem>
                                                )
                                            })}
                                        </ListGroup>
                                        
                                    </CardBody>
                                )
                            })}
                            </Card>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>{
                            this.switchModal(3)
                            this.switchModal("-4")
                            }}>Anterior</Button>
                        <Button color="primary" onClick={()=>{
                            // this.switchModal("-2")
                            // this.switchModal(3)
                            // this.registerWorkplace()
                            }}>Finalizar</Button>
                    </ModalFooter>
                </Modal>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        {/* {console.log(this.state.Modulos)} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Configuración</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      <BreadcrumbItem tag="span">{name_Workplace}</BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{display:this.state.Centros!=null?"none":active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-back"> </i> Volver</Badge></a></BreadcrumbItem>
                      
                      </Breadcrumb>
                        
                     <Row>
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardHeader className="card-header-tab" style={{height:50}}>
                                    <ButtonGroup>
                                        <Button color="primary" outline={!this.state.Activity} onClick={(()=>{
                                            this.switchActivity(true,false,false)
                                        })}>Centro-Actividad</Button>
                                        <Button color="primary" outline={!this.state.Act_cen} onClick={(()=>{
                                            this.switchActivity(false,true,false)
                                        })}>Actividades</Button>
                                        <Button color="primary" outline={!this.state.Centro} onClick={(()=>{
                                            this.switchActivity(false,false,true)
                                        })}>Centros</Button>
                                    </ButtonGroup>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col xs="6" md="6" lg="6" style={{display:`${this.state.Act_cen==true?"block":"none"}`}} >
                                            <b>Actividades</b>
                                            <br></br>
                                            <ButtonGroup style={{width:`${100}%`}}>
                                                <Input placeholder="Busca actividades..." value={this.state.txtActividad} name="txtActividad" onKeyDown={this.handleKeyDown3} onChange={this.handleChangeActivity}></Input>
                                                {this.createTooltip("crear_actividad","Crear Actividad")}
                                                <Button id="crear_actividad" color="success"  onClick={((e)=>{
                                                    if(this.state.txtActividad!=""){
                                                        this.createActivity(this.state.txtActividad)
                                                        this.setState({txtActividad:""})
                                                    }

                                                })}><i style={{fontSize:20}} className="pe-7s-plus"></i></Button>
                                            </ButtonGroup>
                                            <ReactTable                                                           
                                            data={this.state.Actividad.filter((data)=>{
                                                if(this.state.txtActividad!=""){
                                                    return data.detail.toLocaleUpperCase().includes(this.state.txtActividad.toLocaleUpperCase())
                                                }else{
                                                  return data
                                                }
                                              })}
                                                        
                                            // loading= {false}
                                            showPagination= {true}
                                            showPaginationTop= {false}
                                            showPaginationBottom= {true}
                                            showPageSizeOptions= {false}
                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                            defaultPageSize={10}
                                            // getTrProps={onRowClick}
                                            columns={[
                                                   {
                                                    Header: "n°",
                                                    accessor: "ind",
                                                    width: 80
                                                    },
                                                    {
                                                    Header: "Fecha Reg.",
                                                    accessor: "dateTime",                                                              
                                                    width: 140
                                                    },
                                                    {
                                                        Header: "detalle",                                                              
                                                        width: 400,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Input placeholder={row._original.detail} onKeyDown={((e)=>{
                                                                    if (e.key === 'Enter') {
                                                                        this.updateActivity(row._original._id,e.target.value,"detail")
                                                                    }
                                                                })}/>
                                                                {/* <Button color="link"  pill onClick={((e)=>{
                                                                    
                                                                })}><i style={{fontSize:20,cursor:"pointer",color:"orange"}} className="pe-7s-pen" ></i></Button> */}
                                                            </Fragment>
                                                            )
                                                    },
                                                    {
                                                        Header: "Accion",
                                                        id: "id",
                                                        width: 80,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                {/* <Badge color="primary" style={{marginLeft:0}} onClick={((e)=>{
                                                                    console.log("hola")
                                                                })} pill>
                                                                <i class="pe-7s-angle-right-circle" 
                                                                style={{display:active_config,cursor:"pointer",fontSize:20}}></i></Badge>&nbsp; */}
                                                                {this.createTooltip(`eliminar_${row._original.ind}`,"Elimina Actividad")}
                                                                <Badge id={`eliminar_${row._original.ind}`} color="link" pill onClick={((e)=>{
                                                                    Swal.fire({
                                                                        title: '¿Estas seguro?',
                                                                        text: "¡No podras revertir esto!",
                                                                        icon: 'warning',
                                                                        showCancelButton: true,
                                                                        cancelButtonText: 'Cancelar',
                                                                        confirmButtonColor: '#3085d6',
                                                                        cancelButtonColor: '#d33',
                                                                        confirmButtonText: 'Eliminar'
                                                                      }).then((result) => {
                                                                        if (result.isConfirmed) {
                                                                          this.deleteActivity(row._original._id)
                                                                        //   Swal.fire(
                                                                        //     'Eliminado!',
                                                                        //     'La actividad fue eliminada.',
                                                                        //     'success'
                                                                        //   )
                                                                        }
                                                                      })
                                                                })}><i style={{fontSize:25,cursor:"pointer",color:"red"}} className="pe-7s-close-circle" ></i></Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                ]                                                             
                                            }
                                            
                                            className="-striped -highlight"
                                            />
                                        </Col>
                                        <Col xs="12" md="12" lg="12" style={{display:`${this.state.Activity==true?"block":"none"}`}}>
                                            <Row>
                                                <Col><b>Actividad-Centro</b></Col>
                                            </Row>
                                            <br></br>
                                            <Row>
                                                <Col>
                                                <Row>
                                                    <Col xs="10" md="10" lg="10">{this.renderSelectWorkplace(this.state.Centros)}</Col>
                                                    <Col>
                                                        <ButtonGroup>
                                                        <Button color="secondary" onKeyDown={this.handleKeyDown} onClick={((e)=>{
                                                            const {TagsSelecionado,NameWorkplace} = this.state;
                                                            try{
                                                                this.filtrar(TagsSelecionado[0].code,TagsSelecionado[0].name)
                                                            }catch(e){
                                                                let separador=this.props.centroUsuario;
                                                                let centro=separador.split('/');
                                                                this.getCentro_Actividad(centro[0])
                                                                this.filtrar(centro[0],centro[1])
                                                            }
                                                        })}><i style={{fontSize:17}} className="pe-7s-search"></i></Button>
                                                        {this.createTooltip(`exportar`,"Exportar en Excel")}
                                                        <CSVLink
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: "n°", key: "ind" },
                                                                                { label: "Fecha Reg.", key: "dateTime" },
                                                                                { label: "detalle" , key: "detail" },
                                                                                { label: "estado" , key: "estado" },
                                                                                // { label: "Temp ("+ data.unity2 + ")", key: "temp" },
                                                                                // { label: "O2 SAT ("+ data.unity3 + ")", key: "oxs" },
                                                                                // { label: "Sal ("+ data.unity4 + ")", key: "sal" }
                                                                            ]
                                                                        
                                                                        }
                                                                        style={{display:viewExport?"block":"none"}}
                                                                        id="exportar"
                                                                        data={this.state.Centro_Actividad}
                                                                        filename={name_Workplace +".csv"}
                                                                        className="btn btn-success"
                                                                        target="_blank">
                                                                        <span  style={{fontSize:17}} className="icon-file-excel"></span>
                                                        </CSVLink>
                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                                <ReactTable                                                           
                                                data={this.state.Centro_ActividadU.filter(data=>{
                                                    if(this.state.txtActividad3!=""){
                                                        return data.detail.toLocaleUpperCase().includes(this.state.txtActividad3.toLocaleUpperCase())
                                                    }else{
                                                      return data
                                                    }
                                                })}
                                                            
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={12}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "Fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                        Header: "detalle",
                                                        accessor: "detail",                                                              
                                                        width: 450
                                                        },
                                                        {
                                                        Header: "Evetos",
                                                        id: "state",                                                              
                                                        width: 150,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                    {this.activityActive(row._original.activityId,row._original.workplaceId)}
                                                            </Fragment>
                                                        )
                                                        },
                                                        {
                                                        Header: "Accion",
                                                        id: "id",
                                                        width: 80,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                {/* <Badge color="primary" style={{marginLeft:0}} onClick={((e)=>{
                                                                    console.log("hola")
                                                                })} pill>
                                                                <i class="pe-7s-angle-right-circle" 
                                                                style={{display:active_config,cursor:"pointer",fontSize:20}}></i></Badge>&nbsp; */}
                                                                {this.createTooltip(`asigna_eventos_${row._original.ind}`,"Asigna eventos")}
                                                                <Badge id={`asigna_eventos_${row._original.ind}`} color="link" pill onClick={((e)=>{
                                                                    filtro_id=row._original.activityId
                                                                    this.switchModal("1")
                                                                })}><i style={{fontSize:25,cursor:"pointer",color:"green"}} className="pe-7s-plus" ></i></Badge>
                                                                {this.createTooltip(`finaliza_eventos_${row._original.ind}`,"Finaliza todos los eventos")}
                                                                <Badge id={`finaliza_eventos_${row._original.ind}`} color="link" pill onClick={((e)=>{
                                                                    this.updateEvent(row._original.activityId,false,row._original.workplaceId,"array")
                                                                })}><i style={{fontSize:25,cursor:"pointer",color:"secondary"}} className="pe-7s-check" ></i></Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                    ]                                                             
                                                }
                                                 
                                                className="-striped -highlight"
                                                />
                                                </Col>
                                                <Col>
                                                <Input  placeholder="Buscar Actividad" value={this.state.txtActividad2} name="txtActividad2" onChange={this.handleChangeActivity}></Input>
                                                <ReactTable                                                           
                                                data={this.state.Actividad2.filter((data)=>{
                                                    if(this.state.txtActividad2!=""){
                                                        return data.detail.toLocaleUpperCase().includes(this.state.txtActividad2.toLocaleUpperCase())
                                                    }else{
                                                      return data
                                                    }
                                                  })}
                                                            
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={12}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "Fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 120
                                                        },
                                                        {
                                                        Header: "detalle",
                                                        accessor: "detail",                                                              
                                                        width: 280
                                                        },
                                                        {
                                                        Header: "Accion",
                                                        width: 80,
                                                        id: "id",
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                            {this.createTooltip(`asigna_actividad_${row._original.ind}`,"Asigna actividad")}
                                                            <Badge id={`asigna_actividad_${row._original.ind}`} color="primary" style={{marginLeft:0,display:row._original.visible}} onClick={((e)=>{
                                                                try{
                                                                    this.createEvent(this.state.TagsSelecionado[0].code,row._original._id)
                                                                }catch(e){
                                                                    //console.log(e)
                                                                }
                                                            })} pill>
                                                            <i class="pe-7s-angle-left-circle" 
                                                            style={{display:active_config,cursor:"pointer",fontSize:20}}></i></Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        // {
                                                        //   Header: "Temp ("+ data.unity2+ ")",
                                                        //   accessor: "temp",                                                              
                                                        //   width: 140
                                                        // },
                                                        // {
                                                        //   Header: "O2 SAT ("+ data.unity3+ ")",
                                                        //   accessor: "oxs",                                                              
                                                        //   width: 140
                                                        // },
                                                        // {
                                                        //   Header: "Sal ("+ data.unity4+ ")",
                                                        //   accessor: "sal",                                                              
                                                        //   width: 140
                                                        // }
                                                    ]                                                             
                                                }
                                                
                                                className="-striped -highlight"
                                                />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row style={{display:`${this.state.Centro==true?"block":"none"}`}}>
                                        <Col>
                                            <Row>
                                                <Col><b>Lista de Centros</b></Col>
                                            </Row>
                                            <Row>
                                                <Col md="6">
                                                    <ButtonGroup style={{width:`${100}%`}}>
                                                        <Input placeholder="Busca Centros..." value={this.state.txtCentro} 
                                                        name="txtCentro" onKeyDown={this.handleKeyDown4} onChange={this.handleChangeActivity}></Input>
                                                        {this.createTooltip("crear_centro","Crear Centro")}
                                                        <Button id="crear_centro" color="success"  onClick={((e)=>{
                                                            this.switchModal("2")
                                                            // if(this.state.txtActividad!=""){
                                                            //     // this.createActivity(this.state.txtActividad)
                                                            //     // this.setState({txtActividad:""})
                                                                
                                                            // }
                                                        })}><i style={{fontSize:20}} className="pe-7s-plus"></i></Button>
                                                    </ButtonGroup>
                                                </Col>
                                            </Row>
                                            
                                            <br></br>
                                            {/* {console.log(this.state.Centros_Category)} */}
                                            <ReactTable                                                           
                                                data={this.state.Centros_Category.filter(c=>c.name.toLocaleUpperCase().includes(this.state.txtCentro.toLocaleUpperCase()))}
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={12}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 50
                                                        },
                                                        {
                                                        Header: "Nombre",
                                                        accessor: "name",                                                              
                                                        width: 300
                                                        },
                                                        {
                                                        Header: "codigo",
                                                        accessor: "code",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                        Header: "ubicación",
                                                        accessor: "ubicacion",                                                              
                                                        width: 280
                                                        },
                                                        {
                                                        Header: "Area",
                                                        width: 280,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                {this.areaCentro(row.code)}
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "Estado",
                                                        width: 80,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                {this.activeCentro(row.code)}
                                                            </Fragment>
                                                            )
                                                        },
                                                        
                                                    ]                                                             
                                                }
                                                
                                                className="-striped -highlight"
                                                />
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Resumen);
