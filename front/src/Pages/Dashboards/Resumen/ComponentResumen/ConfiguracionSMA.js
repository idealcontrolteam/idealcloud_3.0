import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter, CardText, CardTitle,Label,CustomInput, Toast,Jumbotron } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {
    toast

} from 'react-toastify';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import '../../../../assets/icon/style.css';
import Swal from 'sweetalert2'
import TendenciaSMA from '../ComponentResumen/TendenciaSMA'

sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
try{
    empresas=empresas.filter(e=>e.active==true).map(e=>e);
}catch(e){
    // console.log("area")
}
let ubicaciones=JSON.parse(sessionStorage.getItem("Areas"));
let c=0;

let active_config="none";
let name_Workplace="";
let viewExport=false;
let check=false;
let area="";
let conexion="";

class ConfiguracionSMA extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
        this.textInput = React.createRef();
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Modulos:[],
          isLoading:false,
          Mis_Prom:[],
          MyData:[],
          NameWorkplace:"",
          Workplace:[],
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          redirect:false,
          Areas:[],
          Dispisitivos:[],
          TagsSMA:[
            {name:"Modulo: Sensor 5m"},
            {name:"Modulo: Sensor 10m"},
            {name:"Modulo: Sensor Sal 5m"},
            {name:"Modulo: Sensor Sal 10m"},
            {name:"Ponton: Sensor 5m"},
            {name:"Ponton: Sensor 10m"},
            {name:"Ponton: Sensor Sal 5m"},
            {name:"Ponton: Sensor Sal 10m"},
          ],
          Estados:[
            //   {name:"Mantención",id:""},
            //   {name:"Midiendo",id:""},
            //   {name:"Calibracion",id:""},
            //   {name:"Fuera de Control",id:""},
              {name:"Disponible",id:"Disponible"},
              {name:"No Disponible",id:"No Disponible"},
          ],
          Tags:[],
          Centro_ActividadU:[],
          active_grafic:"none",
          ModalState:false,
          User:"",
          Pass:"",
          IdUF: "",
          IdProceso: "",
          IdProcesoP: "",
          IdWorplace: "",
          start_date: "",
          estimated_date: "",
          end_date: "",
          Cycles:[],
          Correo:"",
          Nombre:"",
          Telefono:"",
          Latitud:"",
          Longitud:"",
          LatitudP:"",
          LongitudP:"",
          action:false,
          AreaModal:"",
          status5m:false,
          status10m:false,
          status5mp:false,
          status10mp:false,
          inicio_registro:String(moment(new Date).format('YYYY-MM-DD')),
          AreaId:"",
          ListaCorreos:[],
          ListaCorreos2:[],
          ListaCorreos3:[],
          ListaCorreos4:[],
          editaInd:"",
          asignaInd:"",
          editaId:"",
          ProcesoSMA0:[],
          ProcesoSMA1:[],
          ProcesoSMA2:[],
          ProcesoSMA3:[],
          ProcesoSMA4:[],
          ProcesoSMA5:[],
          ProcesoSMA6:[],
          ProcesoSMA7:[],
          ModalArray:"m",
          disp1:"",
          disp2:"",
          disp3:"",
          disp4:"",
          disp5:"",
          disp6:"",
          disp7:"",
          disp8:"",
        };     
        this.tagservices = new TagServices();        
        this.intervalIdTag1=React.createRef();
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        //this.textInput.focus()
        let rol=sessionStorage.getItem("rol");
        // console.log(rol)
        if(rol=="5f6e1ac01a080102d0e268c2"||rol=="5f6e1ac01a080102d0e268c3"){
          active_config="block";
        }else{
            active_config="none";
        }
        //alert(JSON.stringify(this.props))

        // this.getUbicacion();
        this.setState({isLoading: false,Centros:empresas})
        //this.setState({isLoading: false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        //this.setState({ isLoading: true });
        let separador=this.props.Centros!=null?this.props.Centros:this.props.centroUsuario;
        let centro=separador.split('/');
        const intervaloRefresco=100000;
        this.getMyCycle(String(centro[0]),false);
        this.intervalIdTag1.current = setInterval(() => this.getMyCycle(String(centro[0]),true),intervaloRefresco);
        this.getWorkplace(String(centro[0]));
        this.getDispositives(String(centro[0]));
        this.getAreas();
        
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
        //this.getCentro_Actividad();
      }

      componentWillUnmount() {
        clearInterval(this.intervalIdTag1.current);
     } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        if(this.props.Centros==null){
            let separador=nextProps.centroUsuario;
            let centro=separador.split('/');
            this.getWorkplace(String(centro[0]));
            this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
            //this.getSondas(centro[0].toString());
            // if(c!=0)
            this.setState({redirect:true})
        }
        
        // c++;
      }

    switchModal(n,type){
        if(n=="1"){
            this.setState({modal1:true})
        }
        else if(n=="2"){
            this.setState({modal2:true})
        }
        else if(n=="3"){
            this.setState({modal3:true})
        }
        else if(n=="4"){
            this.setState({modal4:true,ModalArray:type})
        }
        else if(n=="5"){
            this.setState({modal5:true})
        }
        else{
            this.setState({modal1:false,modal2:false,modal3:false,modal4:false,modal5:false})
        }
      }

    getDispositives(id){
        this.tagservices.getZones(id,'get').then(zones=>{
            //console.log("zonas"+JSON.stringify(zones.data.data))
            let zonas=zones.data.data;
            zonas.map((z)=>{
                this.tagservices.getLocation(z._id).then(location=>{
                    // this.setState({Dispisitivos:this.state.Dispisitivos.filter(d=>d.zoneId!=z._id).push(location)})
                    let locations=location.data.data;
                    locations.map((l)=>{
                        this.tagservices.getTags(l._id).then(tags=>{
                            tags.map(t=>{
                                this.setState({Tags:this.state.Tags.filter(ta=>ta._id!=t._id).concat(t)})
                            })
                        }).catch(e=>{
                            console.log(e)
                        })
                        this.setState({Dispisitivos:this.state.Dispisitivos.filter(d=>d._id!=l._id).concat(l)})
                    })
                })
            })
        }).catch(e=>{
            console.log(e)
        })
    }

    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }


    handleChangeActivity = event => {
            const name = event.target.name;
            const value = event.target.value;
            this.setState({ [name]: value });
    }

    getAreas(){
        this.tagservices.getAreas().then(respuesta=>{
            this.setState({Areas:respuesta.filter(a=>a.active==true).map(a=>{
                if(this.state.Workplace.areaId==a._id){
                    a.selected=true;
                }
                else{
                    a.selected=false;
                }
                return a
            })})
        }).catch(e=>{
            //console.log(e)
        })
    }

    getWorkplace(id){
        this.tagservices.getWorkplace(id).then(respuesta=>{
            this.setState({Workplace:respuesta,AreaId:respuesta.areaId})
        }).catch(e=>{
            //console.log(e)
        })
    }

    changeValueDate(date){
        let split=date.split('T');
        return String(split[0])
    }

    getMyCycle(id,actualiza){
        // console.log(id)
        this.tagservices.getCycles(id).then(respuesta=>{
            if(respuesta!=[]){
                let IdUF="";
                let IdProceso="";
                let IdProcesoP="";
                let start_date="";
                let estimated_date="";
                let end_date="";
                let Nombre="";
                let Telefono="";
                let Correo="";
                let Latitud="";
                let Longitud="";
                let LatitudP="";
                let LongitudP="";
                let User="";
                let Pass="";
                let status5m=false;
                let status10m=false;
                let statusSal5m=false;
                let statusSal10m=false;
                let status5mp=false;
                let status10mp=false;
                let statusSal5mp=false;
                let statusSal10mp=false;
                let inicio_registro="";
                let ListaCorreos=[];
                let ListaCorreos2=[];
                let ListaCorreos3=[];
                let ListaCorreos4=[];
                // workplaceId: this.state.NameWorkplace.id,
                // start_date: this.state.start_date,
                // estimated_date: this.state.estimated_date,
                // end_date: this.state.end_date,
                // register: false,
                // console.log(`${respuesta[0].status5mp}---${respuesta[0].status10mp}`)
                if(actualiza==false){
                    try{
                        IdUF=respuesta[0].IdUF;
                        IdProceso=respuesta[0].IdProceso;
                        IdProcesoP=respuesta[0].IdProcesoPonton;
                        Nombre=respuesta[0].encargado;
                        Telefono=respuesta[0].telefono;
                        Correo=respuesta[0].correo;
                        Latitud=respuesta[0].latitud;
                        Longitud=respuesta[0].longitud;
                        User=respuesta[0].User;
                        Pass=respuesta[0].Pass;
                        LatitudP=respuesta[0].latitudPonton;
                        LongitudP=respuesta[0].longitudPonton;
                        status5m=respuesta[0].status5m;
                        status10m=respuesta[0].status10m;
                        statusSal5m=respuesta[0].statusSal5m;
                        statusSal10m=respuesta[0].statusSal10m;
                        status5mp=respuesta[0].status5mp;
                        status10mp=respuesta[0].status10mp;
                        statusSal5mp=respuesta[0].statusSal5mp;
                        statusSal10mp=respuesta[0].statusSal10mp;
                        inicio_registro=moment(respuesta[0].registerInit).utc().format('YYYY-MM-DD');
                        ListaCorreos=String(respuesta[0].CorreosEmail).split(',');
                        ListaCorreos2=String(respuesta[0].CorreosEmailSMA).split(',');
                        ListaCorreos3=String(respuesta[0].CorreosEmailSuport).split(',');
                        ListaCorreos4=String(respuesta[0].CorreosEmailOxigeno).split(',');
                        start_date=String(respuesta[0].start_date);
                        estimated_date=String(respuesta[0].estimated_date);
                        end_date=String(respuesta[0].end_date);
                        if(ListaCorreos.length==1){
                            ListaCorreos=[]
                        }
                        else{
                            ListaCorreos=ListaCorreos.filter((l,i)=>i!=ListaCorreos.length-1)
                        }
                        if(ListaCorreos2.length==1){
                            ListaCorreos2=[]
                        }
                        else{
                            ListaCorreos2=ListaCorreos2.filter((l,i)=>i!=ListaCorreos2.length-1)
                        }
                        if(ListaCorreos3.length==1){
                            ListaCorreos3=[]
                        }
                        else{
                            ListaCorreos3=ListaCorreos3.filter((l,i)=>i!=ListaCorreos3.length-1)
                        }
                        if(ListaCorreos4.length==1){
                            ListaCorreos4=[]
                        }
                        else{
                            ListaCorreos4=ListaCorreos4.filter((l,i)=>i!=ListaCorreos4.length-1)
                        }
                    }catch(e){
                        //console.log(e)
                    }
                    if(respuesta[0].start_date!="" && respuesta[0].start_date!=null)
                        start_date=String(respuesta[0].start_date);
                    if(respuesta[0].estimated_date!="" && respuesta[0].estimated_date!=null)
                        estimated_date=String(respuesta[0].estimated_date);
                    if(respuesta[0].end_date!="" && respuesta[0].end_date!=null)
                        end_date=String(respuesta[0].end_date);
                    let new_tag=this.state.TagsSMA.map((t,i)=>{
                        if (i==0){
                            t.value=respuesta[0].Sensor5mId
                            t.estado=respuesta[0].estado5m
                            t.disp=respuesta[0].IdSMA5m
                            this.findRegisterSMA(respuesta[0].IdSMA5m,i)
                        }
                        else if(i==1){
                            t.value=respuesta[0].Sensor10mId
                            t.estado=respuesta[0].estado10m
                            t.disp=respuesta[0].IdSMA10m
                            this.findRegisterSMA(respuesta[0].IdSMA10m,i)
                        }
                        else if(i==2){
                            t.value=respuesta[0].SensorSal5mId
                            t.estado=respuesta[0].estadoSal5m
                            t.disp=respuesta[0].IdSMASal5m
                            this.findRegisterSMA(respuesta[0].IdSMASal5m,i)
                        }
                        else if(i==3){
                            t.value=respuesta[0].SensorSal10mId
                            t.estado=respuesta[0].estadoSal10m
                            t.disp=respuesta[0].IdSMASal10m
                            this.findRegisterSMA(respuesta[0].IdSMASal10m,i)
                        }
                        else if(i==4){
                            t.value=respuesta[0].Ponton5mId
                            t.estado=respuesta[0].estado5mp
                            t.disp=respuesta[0].IdSMA5mp
                            this.findRegisterSMA(respuesta[0].IdSMA5mp,i)
                        }
                        else if(i==5){
                            t.value=respuesta[0].Ponton10mId
                            t.estado=respuesta[0].estado10mp
                            t.disp=respuesta[0].IdSMA10mp
                            this.findRegisterSMA(respuesta[0].IdSMA10mp,i)
                        }
                        else if(i==6){
                            t.value=respuesta[0].PontonSal5mId
                            t.estado=respuesta[0].estadoSal5mp
                            t.disp=respuesta[0].IdSMASal5mp
                            this.findRegisterSMA(respuesta[0].IdSMASal5mp,i)
                        }
                        else if(i==7){
                            t.value=respuesta[0].PontonSal10mId
                            t.estado=respuesta[0].estadoSal10mp
                            t.disp=respuesta[0].IdSMASal10mp
                            this.findRegisterSMA(respuesta[0].IdSMASal10mp,i)
                        }
                        return t
                    })
                    this.setState({Cycles:respuesta,TagsSMA:new_tag,IdUF,IdProceso,IdProcesoP,
                        Nombre,Correo,Telefono,Latitud,Longitud,LatitudP,LongitudP,
                        start_date:this.changeValueDate(start_date),
                        estimated_date:this.changeValueDate(estimated_date),
                        end_date:this.changeValueDate(end_date),Pass,User,
                        status5m,status10m,statusSal5m,statusSal10m,
                        status10mp,status5mp,statusSal10mp,statusSal5mp,inicio_registro,
                        ListaCorreos, ListaCorreos2, ListaCorreos3, ListaCorreos4
                    })
                }else{
                    status5m=respuesta[0].status5m;
                    status10m=respuesta[0].status10m;
                    statusSal5m=respuesta[0].statusSal5m;
                    statusSal10m=respuesta[0].statusSal10m;
                    status5mp=respuesta[0].status5mp;
                    status10mp=respuesta[0].status10mp;
                    statusSal5mp=respuesta[0].statusSal5mp;
                    statusSal10mp=respuesta[0].statusSal10mp;
                    this.state.TagsSMA.map((t,i)=>{
                        if (i==0){
                            this.findRegisterSMA(respuesta[0].IdSMA5m,i)
                        }
                        else if(i==1){
                            this.findRegisterSMA(respuesta[0].IdSMA10m,i)
                        }
                        else if(i==2){
                            this.findRegisterSMA(respuesta[0].IdSMASal5m,i)
                        }
                        else if(i==3){
                            this.findRegisterSMA(respuesta[0].IdSMASal10m,i)
                        }
                        else if(i==4){
                            this.findRegisterSMA(respuesta[0].IdSMA5mp,i)
                        }
                        else if(i==5){
                            this.findRegisterSMA(respuesta[0].IdSMA10mp,i)
                        }
                        else if(i==6){
                            this.findRegisterSMA(respuesta[0].IdSMASal5mp,i)
                        }
                        else if(i==7){
                            this.findRegisterSMA(respuesta[0].IdSMASal10mp,i)
                        }
                        return t
                    })
                    this.setState({
                        status5m,status10m,statusSal5m,statusSal10m,
                        status10mp,status5mp,statusSal10mp,statusSal5mp
                    })
                }
                
            }
        }).catch(e=>{
            //console.log(e)
        })
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    getOptionsDivice(value){
        return this.state.Dispisitivos.map((d,i)=>{
            if(value==d._id){
                return <option selected value={d._id}>{d.name}</option>
            }
            return <option value={d._id}>{d.name}</option>
        })
    }

    updateWorkplace(id){
        let data={
            areaId:area
        }
        axios
              .put(API_ROOT+`/workplace/${id}`,data)
              .then(response => {   
                  //alert(response.data.statusCode===201);
                  if(response.data.statusCode===201){
                    //toast['success']('La configuración fue registrada con exito', { autoClose: 4000 })
                  }
              })
              .catch(error => {
                  console.log(error)
              });
    }

    validateSpaceSMA(){
        if(this.state.User.includes(' ') || this.state.Pass.includes(' ')){
            return false
        }else{
            return true
        }
    }

    validateDuplicados(array){
        let validate=true
        array.map((a)=>{
            array.filter((ar)=>ar.id!=a.id).map(ar=>{
                if(ar.value==a.value){
                    validate=false
                }
            })
        })
        return validate
    }

    validate(){
        
        let area=document.querySelector("#areas1").value;

        let sensor1=document.querySelector("#option1").value;
        let sensor2=document.querySelector("#option2").value;
        let sensor3=document.querySelector("#option3").value;
        let sensor4=document.querySelector("#option4").value;
        let sensor5=document.querySelector("#option5").value;
        let sensor6=document.querySelector("#option6").value;
        let sensor7=document.querySelector("#option7").value;
        let sensor8=document.querySelector("#option8").value;

        let estado1=document.querySelector("#estado1").value;
        let estado2=document.querySelector("#estado2").value;
        let estado3=document.querySelector("#estado3").value;
        let estado4=document.querySelector("#estado4").value;
        let estado5=document.querySelector("#estado5").value;
        let estado6=document.querySelector("#estado6").value;
        let estado7=document.querySelector("#estado7").value;
        let estado8=document.querySelector("#estado8").value;

        let disp1=document.getElementById("id_disp_1").value;
        let disp2=document.getElementById("id_disp_2").value;
        let disp3=document.getElementById("id_disp_3").value;
        let disp4=document.getElementById("id_disp_4").value;
        let disp5=document.getElementById("id_disp_5").value;
        let disp6=document.getElementById("id_disp_6").value;
        let disp7=document.getElementById("id_disp_7").value;
        let disp8=document.getElementById("id_disp_8").value;

        if(sensor1!="0"&&sensor2!="0"&&sensor3!="0"&&sensor4!="0"&&sensor5!="0"&&sensor6!="0"&&sensor7!="0"&&sensor8!="0"&&
           estado1!=""&&estado2!=""&&estado3!=""&&estado4!=""&&estado5!=""&&estado6!=""&&estado7!=""&&estado8!=""&&
           disp1!=""&&disp2!=""&&disp3!=""&&disp4!=""&&disp5!=""&&disp6!=""&&disp7!=""&&disp8!=""&&
           this.state.IdUF!=""&&this.state.IdProceso!=""&&this.state.IdProcesoP!=""&&area!=""&&
          
          this.state.User!="" && this.state.Pass!=""){
            return true
        }
        return false
    }

    CrearCiclo(type){
        area=document.querySelector("#areas1").value;
        //Modulo
        let sensor1=document.querySelector("#option1").value;
        let sensor2=document.querySelector("#option2").value;
        let sensor3=document.querySelector("#option3").value;
        let sensor4=document.querySelector("#option4").value;

        //Modulo Estados
        let estado1=document.querySelector("#estado1").value;
        let estado2=document.querySelector("#estado2").value;
        let estado3=document.querySelector("#estado3").value;
        let estado4=document.querySelector("#estado4").value;

        //Modulo Dispositivos
        let disp1=document.getElementById("id_disp_1").value;
        let disp2=document.getElementById("id_disp_2").value;
        let disp3=document.getElementById("id_disp_3").value;
        let disp4=document.getElementById("id_disp_4").value;



        //Ponton
        let sensor5=document.querySelector("#option5").value;
        let sensor6=document.querySelector("#option6").value;
        let sensor7=document.querySelector("#option7").value;
        let sensor8=document.querySelector("#option8").value;

        //Ponton Estados
        let estado5=document.querySelector("#estado5").value;
        let estado6=document.querySelector("#estado6").value;
        let estado7=document.querySelector("#estado7").value;
        let estado8=document.querySelector("#estado8").value;

         //Ponton Dispositivos
         let disp5=document.getElementById("id_disp_5").value;
         let disp6=document.getElementById("id_disp_6").value;
         let disp7=document.getElementById("id_disp_7").value;
         let disp8=document.getElementById("id_disp_8").value;
        
        let emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        let validate=this.validate();
        let validate_correo=emailRegex.test(this.state.Correo);
        if(!validate_correo&&this.state.Correo!=""&&type==null){
            toast['warning']('El formato de correo esta incorrecto', { autoClose: 4000 })
            validate=false;
        }
        let ListaCorreos="";
        this.state.ListaCorreos.filter(l=>l!="").map(l=>{
            ListaCorreos+=l.trim()+",";
        })

        let ListaCorreos2="";
        this.state.ListaCorreos2.filter(l=>l!="").map(l=>{
            ListaCorreos2+=l.trim()+",";
        })

        let ListaCorreos3="";
        this.state.ListaCorreos3.filter(l=>l!="").map(l=>{
            ListaCorreos3+=l.trim()+",";
        })

        let ListaCorreos4="";
        this.state.ListaCorreos4.filter(l=>l!="").map(l=>{
            ListaCorreos4+=l.trim()+",";
        })
        if(type=="save_info"){
            this.updateWorkplace(this.state.NameWorkplace.id);
            let id="";
            let metodo="";
            let mensaje="";
            mensaje="registrada";
            let data={
                workplaceId: this.state.NameWorkplace.id!=null?this.state.NameWorkplace.id:"",
                start_date: this.state.start_date!="null"&&this.state.start_date!="undefined"?this.state.start_date:"",
                estimated_date: this.state.estimated_date!="null"&&this.state.estimated_date!="undefined"?this.state.estimated_date:"",
                end_date: this.state.end_date!="null"&&this.state.end_date!="undefined"?this.state.end_date:"",
                encargado: this.state.Nombre!=null?this.state.Nombre:"",
                correo: this.state.Correo!=null?this.state.Correo:"",
                telefono: this.state.Telefono!=null?this.state.Telefono:"",
                CorreosEmail:ListaCorreos!=null?ListaCorreos:"",
                CorreosEmailSMA:ListaCorreos2!=null?ListaCorreos2:"",
                CorreosEmailSuport:ListaCorreos3!=null?ListaCorreos3:"",
                CorreosEmailOxigeno:ListaCorreos4!=null?ListaCorreos4:"",
                EnvioModulo: "0",
                EnvioPonton: "0",
                //register: false,

                registerInit:moment(new Date).format('YYYY-MM-DDTHH:mm:ss')+".000Z",
            };
            // console.log(data)
            metodo="post"
            if(this.state.Cycles.length>0){
                id=this.state.Cycles[0]._id;
                metodo="put";
                mensaje="actualizada";
                data=data;
                // {
                //     workplaceId: this.state.NameWorkplace.id!=null?this.state.NameWorkplace.id:"",
                //     start_date: this.state.start_date!=null?this.state.start_date:"",
                //     estimated_date: this.state.estimated_date!=null?this.state.estimated_date:"",
                //     end_date: this.state.end_date!=null?this.state.end_date:"",
                // }
            }
            this.tagservices.crudCycles(id,data,metodo).then(respuesta=>{
                if(respuesta.statusCode===201 || respuesta.statusCode===200){
                    this.setState({action:false})
                    toast['success'](`La configuración fue ${mensaje} con exito`, { autoClose: 4000 })
                    this.getMyCycle(this.state.NameWorkplace.id,false);
                    this.getWorkplace(this.state.NameWorkplace.id);
                    this.getDispositives(this.state.NameWorkplace.id);
                    this.getAreas();
                  }else{
                    toast['warning']('OPS! hubo un problema', { autoClose: 4000 })
                  }
            })
        }
        else if(validate){
            this.updateWorkplace(this.state.NameWorkplace.id);
            let data={
                User: this.state.User,
                Pass:this.state.Pass,
                IdUF: this.state.IdUF,
                IdProceso: this.state.IdProceso,
                IdProcesoPonton: this.state.IdProcesoP,
                workplaceId: this.state.NameWorkplace.id,
                start_date: this.state.start_date!=null?this.state.start_date:"",
                estimated_date: this.state.estimated_date!=null?this.state.estimated_date:"",
                end_date: this.state.end_date!=null?this.state.end_date:"",
                // start_date: this.state.start_date,
                // estimated_date: this.state.estimated_date,
                // end_date: this.state.end_date,
                
                Sensor5mId: sensor1,
                Sensor10mId: sensor2,
                SensorSal5mId: sensor3,
                SensorSal10mId: sensor4,

                Ponton5mId: sensor5,
                Ponton10mId: sensor6,
                PontonSal5mId: sensor7,
                PontonSal10mId: sensor8,

                estado5m: estado1,
                estado10m: estado2,
                estadoSal5m: estado3,
                estadoSal10m: estado4,

                estado5mp: estado5,
                estado10mp: estado6,
                estadoSal5mp: estado7,
                estadoSal10mp: estado8,
                
                encargado: this.state.Nombre,
                correo: this.state.Correo,
                telefono: this.state.Telefono,
                latitud: this.state.Latitud,
                longitud: this.state.Longitud,
                latitudPonton:this.state.LatitudP,
                longitudPonton:this.state.LongitudP,
                register: false,

                IdSMA5m: disp1,
                IdSMA10m: disp2,
                IdSMASal5m: disp3,
                IdSMASal10m: disp4,
                IdSMA5mp: disp5,
                IdSMA10mp: disp6,
                IdSMASal5mp: disp7,
                IdSMASal10mp: disp8,

                status5m:false,
                status10m:false,
                statusSal5m:false,
                statusSal10m:false,
                status5mp:false,
                status10mp:false,
                statusSal5mp:false,
                statusSal10mp:false,

                CorreosEmail:ListaCorreos,
                CorreosEmailSMA:ListaCorreos2,
                CorreosEmailSuport:ListaCorreos3,
                CorreosEmailOxigeno:ListaCorreos4,

                EnvioModulo: "0",
                EnvioPonton: "0",

                registerInit:moment(new Date).format('YYYY-MM-DDTHH:mm:ss')+".000Z",
            };
            this.tagservices.crudCycles("",data,"post").then(respuesta=>{
                if(respuesta.statusCode===201 || respuesta.statusCode===200){
                    this.setState({action:false})
                    toast['success']('La configuración fue registrada con exito', { autoClose: 4000 })
                    this.getMyCycle(this.state.NameWorkplace.id,false);
                    this.getWorkplace(this.state.NameWorkplace.id);
                    this.getDispositives(this.state.NameWorkplace.id);
                    this.getAreas();
                  }else{
                    toast['warning']('OPS! hubo un problema', { autoClose: 4000 })
                  }
            })
        }else{
            this.setState({action:false})
            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
        }
        
    }

    question(){
        Swal.fire({
            title: '¿Estas seguro de esto?',
            text: "estos cambios son importantes!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'si, modificar!'
          }).then((result) => {
            if (result.isConfirmed) {
                this.UpdateCiclo()
            }else{
                this.setState({action:false})
            }
          })
    }

    UpdateCiclo(){
        area=document.querySelector("#areas1").value;
        //Modulo
        let sensor1=document.querySelector("#option1").value;
        let sensor2=document.querySelector("#option2").value;
        let sensor3=document.querySelector("#option3").value;
        let sensor4=document.querySelector("#option4").value;

         //Modulo Estados
        let estado1=document.querySelector("#estado1").value;
        let estado2=document.querySelector("#estado2").value;
        let estado3=document.querySelector("#estado3").value;
        let estado4=document.querySelector("#estado4").value;
        
        //Modulo Dispositivos
        let disp1=document.getElementById("id_disp_1").value;
        let disp2=document.getElementById("id_disp_2").value;
        let disp3=document.getElementById("id_disp_3").value;
        let disp4=document.getElementById("id_disp_4").value;


        //Ponton
        let sensor5=document.querySelector("#option5").value;
        let sensor6=document.querySelector("#option6").value;
        let sensor7=document.querySelector("#option7").value;
        let sensor8=document.querySelector("#option8").value;

        //Ponton Estados
        let estado5=document.querySelector("#estado5").value;
        let estado6=document.querySelector("#estado6").value;
        let estado7=document.querySelector("#estado7").value;
        let estado8=document.querySelector("#estado8").value;

        //Ponton Dispositivos
        let disp5=document.getElementById("id_disp_5").value;
        let disp6=document.getElementById("id_disp_6").value;
        let disp7=document.getElementById("id_disp_7").value;
        let disp8=document.getElementById("id_disp_8").value;

        let ListaCorreos="";
        this.state.ListaCorreos.filter(l=>l!="").map(l=>{
            ListaCorreos+=l.trim()+",";
        })

        let ListaCorreos2="";
        this.state.ListaCorreos2.filter(l=>l!="").map(l=>{
            ListaCorreos2+=l.trim()+",";
        })

        let ListaCorreos3="";
        this.state.ListaCorreos3.filter(l=>l!="").map(l=>{
            ListaCorreos3+=l.trim()+",";
        })

        let ListaCorreos4="";
        this.state.ListaCorreos4.filter(l=>l!="").map(l=>{
            ListaCorreos4+=l.trim()+",";
        })

        if(this.state.IdUF!="" && this.state.IdProceso!="" && area!=""){
            this.updateWorkplace(this.state.NameWorkplace.id);
            let data={
                User: this.state.User,
                Pass:this.state.Pass,
                IdUF: this.state.IdUF,
                IdProceso: this.state.IdProceso,
                IdProcesoPonton: this.state.IdProcesoP,
                workplaceId: this.state.NameWorkplace.id,
            
                Sensor5mId: sensor1,
                Sensor10mId: sensor2,
                SensorSal5mId: sensor3,
                SensorSal10mId: sensor4,

                Ponton5mId: sensor5,
                Ponton10mId: sensor6,
                PontonSal5mId: sensor7,
                PontonSal10mId: sensor8,

                estado5m: estado1,
                estado10m: estado2,
                estadoSal5m: estado3,
                estadoSal10m: estado4,

                estado5mp: estado5,
                estado10mp: estado6,
                estadoSal5mp: estado7,
                estadoSal10mp: estado8,

                encargado: this.state.Nombre,
                correo: this.state.Correo,
                telefono: this.state.Telefono,
                latitud: this.state.Latitud,
                longitud: this.state.Longitud,
                latitudPonton:this.state.LatitudP,
                longitudPonton:this.state.LongitudP,

                IdSMA5m: disp1,
                IdSMA10m: disp2,
                IdSMASal5m: disp3,
                IdSMASal10m: disp4,
                IdSMA5mp: disp5,
                IdSMA10mp: disp6,
                IdSMASal5mp: disp7,
                IdSMASal10mp: disp8,

                CorreosEmail:ListaCorreos,
                CorreosEmailSMA:ListaCorreos2,
                CorreosEmailSuport:ListaCorreos3,
                CorreosEmailOxigeno:ListaCorreos4,

                EnvioModulo: "0",
                EnvioPonton: "0",
            };
            // console.log(data)
            // console.log(this.state.start_date==null)
            // console.log(this.state.start_date)
            if(this.state.start_date!=null && this.state.start_date!="null"){
                data.start_date=this.state.start_date
            }
            if(this.state.estimated_date!=null && this.state.estimated_date!="null"){
                data.estimated_date= this.state.estimated_date
            }
            if(this.state.end_date!=null && this.state.end_date!="null"){
                data.end_date=this.state.end_date
            }
            
            
            this.tagservices.crudCycles(this.state.Cycles[0]._id,data,"put").then(respuesta=>{
                if(respuesta.statusCode===201 || respuesta.statusCode===200){
                    this.setState({action:false})
                    toast['success']('La configuración fue actualizada con exito', { autoClose: 4000 })
                    this.getMyCycle(this.state.NameWorkplace.id,false);
                    this.getWorkplace(this.state.NameWorkplace.id);
                    this.getDispositives(this.state.NameWorkplace.id);
                    this.getAreas();
                  }else{
                    toast['warning']('OPS! hubo un problema', { autoClose: 4000 })
                  }
            })
            
        }else{
            this.setState({action:false})
            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
        }
        
    }

    crearArea(){
        let data={
            name:this.state.AreaModal,
            active:true,
        }
        this.tagservices.crudArea("",data,"post").then(respuesta=>{
            if(respuesta.statusCode===201 || respuesta.statusCode===200){
                toast['success']('Area registrada con exito', { autoClose: 4000 })
                this.getAreas();
                this.switchModal("-1")
                this.setState({AreaModal:""})
            }else{
                toast['warning']('Ops! ocurrio un problema', { autoClose: 4000 })
            }
        })
    }

    findRegisterSMA(id,n){
        if(id!=undefined){
            this.tagservices.getEndRegistroSMA(id).then(respuesta=>{
                this.setState({["ProcesoSMA"+n]:respuesta})
            })
        }
    }

    viewConection(){
        if(this.state.Cycles.length>0){
            return <Fragment><Button style={{display:active_config}} color={`${this.state.Cycles[0].register?"secondary":"success"}`} outline onClick={(()=>{
                //this.switchModal2("1")
                this.state.Cycles[0].register?
                this.conectarSMA("-1"):
                this.switchModal("2")
            })}><b><i className="pe-7s-power" style={{fontSize:16}}></i></b> {this.state.Cycles[0].register?"Desconectar":"Conectar"}</Button>
            <Button color="primary" style={{marginLeft:5,display:`${this.state.Cycles[0].register?"block":"none"}`}} outline onClick={(()=>{
                this.switchModal("5")
            })}>
                Datos Historicos SMA
            </Button>
            </Fragment>
        }
        // else{
        //     return <Button color="success" outline onClick={(()=>{
        //         this.conectarSMA()
        //     })}><b><i className="pe-7s-power" style={{fontSize:16}}></i></b> Conectar</Button>
        // }
        
    }

    conectarSMA(n){
        let data={};
        if(this.state.inicio_registro!=""){
            data={
                register:n=="1"?true:false,
                registerInit:moment(this.state.inicio_registro).format('YYYY-MM-DDTHH:mm:ss')+".000Z"
            }
        }else{
            data={
                register:n=="1"?true:false,
            }
        }
        
        this.tagservices.crudCycles(this.state.Cycles[0]._id,data,"put").then(respuesta=>{
            if(respuesta.statusCode===201 || respuesta.statusCode===200){
                this.state.Cycles[0].register?
                toast['info']('Centro desconectado correctamente de SMA', { autoClose: 4000 }):
                toast['success']('Centro conectado correctamente a SMA', { autoClose: 4000 })
                this.switchModal("-1")
                this.getMyCycle(this.state.NameWorkplace.id,false)
            }
        })
    }

    valueDispositivo(row){
        if(this.state.Cycles.length>0){
            return <Fragment><Input id={"id_disp_"+row.ind} placeholder="Id Dispositivo (SMA)" value={row.disp}></Input><Button color="primary" onClick={(()=>{
                this.setState({editaInd:row.ind,asignaInd:""})
                this.switchModal("3")
            })}>Editar</Button></Fragment>
        }else{
            return <Fragment>
                    <Input id={"id_disp_"+row.ind} value={this.state["disp"+row.ind]} name={"disp"+row.ind} onChange={this.handleChangeActivity} placeholder="Id Dispositivo (SMA)" ></Input>
                    <Button color="primary" onClick={(()=>{
                        this.setState({editaInd:row.ind,asignaInd:row.ind})
                        this.switchModal("3")
                    })}>Asignar</Button>
                </Fragment>
        }
    }

    getStatus(row){
        if(row._original.ind==1&&this.state.status5m==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(row._original.ind==2&&this.state.status10m==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(row._original.ind==3&&this.state.statusSal5m==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(row._original.ind==4&&this.state.statusSal10m==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }

        else if(row._original.ind==5&&this.state.status5mp==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(row._original.ind==6&&this.state.status10mp==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(row._original.ind==7&&this.state.statusSal5mp==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(row._original.ind==8&&this.state.statusSal10mp==true){
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Online")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }

        else{
            return <Fragment>{this.createTooltip("registro"+row._original.ind,"Offline")}
            <div id={"registro"+row._original.ind}  style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
        }
        
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    AgregarCorreo(){
        let correo=document.querySelector("#EmailCorreo").value;
        correo=correo.toString().trim();
        if(correo.includes(',')){
            correo=correo.split(',')
        }
        if(correo!=""){
            this.setState({ListaCorreos:this.state.ListaCorreos.concat(correo)})
            document.querySelector("#EmailCorreo").value="";
        }else{
            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
        }
    }

    AgregarCorreo2(){
        let correo=document.querySelector("#EmailCorreo2").value;
        correo=correo.toString().trim();
        if(correo.includes(',')){
            correo=correo.split(',')
        }
        if(correo!=""){
            this.setState({ListaCorreos2:this.state.ListaCorreos2.concat(correo)})
            document.querySelector("#EmailCorreo2").value="";
        }else{
            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
        }
    }

    AgregarCorreo3(){
        let correo=document.querySelector("#EmailCorreo3").value;
        correo=correo.toString().trim();
        if(correo.includes(',')){
            correo=correo.split(',')
        }
        if(correo!=""){
            this.setState({ListaCorreos3:this.state.ListaCorreos3.concat(correo)})
            document.querySelector("#EmailCorreo3").value="";
        }else{
            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
        }
    }

    AgregarCorreo4(){
        let correo=document.querySelector("#EmailCorreo4").value;
        correo=correo.toString().trim();
        if(correo.includes(',')){
            correo=correo.split(',')
        }
        if(correo!=""){
            this.setState({ListaCorreos4:this.state.ListaCorreos4.concat(correo)})
            document.querySelector("#EmailCorreo4").value="";
        }else{
            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
        }
    }

    QuitarCorreo(ind){
        let correos=this.state.ListaCorreos.filter((l,i)=>i!=ind)
        this.setState({ListaCorreos:correos})
    }

    QuitarCorreo2(ind){
        let correos=this.state.ListaCorreos2.filter((l,i)=>i!=ind)
        this.setState({ListaCorreos2:correos})
    }

    QuitarCorreo3(ind){
        let correos=this.state.ListaCorreos3.filter((l,i)=>i!=ind)
        this.setState({ListaCorreos3:correos})
    }

    QuitarCorreo4(ind){
        let correos=this.state.ListaCorreos4.filter((l,i)=>i!=ind)
        this.setState({ListaCorreos4:correos})
    }

    getEndDate(n,type,title){
        if(type!=null && type!=undefined){
            if(this.state["ProcesoSMA"+n][0]==null){
                if(type=="m"){
                    return JSON.stringify([{"statusCode": 500,"Tipo":"Modulo "+title,"data":[this.state["ProcesoSMA"+n][0],this.state["ProcesoSMA"+(n+1)][0]]}],undefined,3)
                }else if(type=="p"){
                    return JSON.stringify([{"statusCode": 500,"Tipo":"Ponton "+title,"data":[this.state["ProcesoSMA"+n][0],this.state["ProcesoSMA"+(n+1)][0]]}],undefined,3)
                }
            }
            if(type=="m"){
                return JSON.stringify([{"statusCode": 200,"Tipo":"Modulo "+title,"data":[this.state["ProcesoSMA"+n][0],this.state["ProcesoSMA"+(n+1)][0]]}],undefined,3)
            }else if(type=="p"){
                return JSON.stringify([{"statusCode": 200,"Tipo":"Ponton "+title,"data":[this.state["ProcesoSMA"+n][0],this.state["ProcesoSMA"+(n+1)][0]]}],undefined,3)
            }
            
        }
        if(this.state["ProcesoSMA"+n].length>0){
            return String(moment(this.state["ProcesoSMA"+n][0].dateTime).utc().format('YYYY-MM-DD HH:mm:ss'))
        }
        else{
            return ""
        }
    }

    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        // const onRowClick = (state, rowInfo, column, instance) => {
        //     return {
        //         onClick: e => {
        //             console.log('It was in this column:', column)
        //         }
        //     }
        // }
             
         ///***********************************+++ */

        
        return (
            <Fragment>
                    <Modal style={{maxHeight:660,maxWidth:`250px`}} isOpen={this.state.modal1} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}><b>Crear Area</b></ModalHeader>
                            <ModalBody>
                                <ButtonGroup>
                                    <Input placeholder="Nombre Area" autoFocus={true} autoComplete="off" value={this.state.AreaModal} name="AreaModal" onChange={this.handleChange} />
                                    <Button color="primary" onClick={(()=>{
                                        if(this.state.AreaModal!=""){
                                        this.crearArea()
                                        }
                                    })}>Crear</Button>
                                </ButtonGroup>
                                
                            </ModalBody>
                            <ModalFooter>
                        </ModalFooter>
                    </Modal>
                    <Modal style={{maxHeight:660,maxWidth:`290px`}} isOpen={this.state.modal2} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}><b>Fecha Inicio de Registro SMA</b></ModalHeader>
                            <ModalBody>
                                <ButtonGroup>
                                    {/* <Input placeholder="Nombre Area" autoComplete="off" value={this.state.AreaModal} name="AreaModal" onChange={this.handleChange} /> */}
                                    <Input
                                        type="date"
                                        id="inicio_registro"
                                        name="inicio_registro"
                                        value={this.state.inicio_registro} onChange={this.handleChangeActivity}
                                        placeholder="date placeholder"
                                    />
                                    <Button color="primary" onClick={(()=>{
                                        if(this.state.inicio_registro!=""){
                                            //console.log(this.state.inicio_registro)
                                            let array=[
                                                {id:0,value:document.getElementById("id_disp_1").value},
                                                {id:1,value:document.getElementById("id_disp_2").value},
                                                {id:2,value:document.getElementById("id_disp_3").value},
                                                {id:3,value:document.getElementById("id_disp_4").value},
                                                {id:4,value:document.getElementById("id_disp_5").value},
                                                {id:5,value:document.getElementById("id_disp_6").value},
                                                {id:6,value:document.getElementById("id_disp_7").value},
                                                {id:7,value:document.getElementById("id_disp_8").value},
                                            ]
                                            let array2=[
                                                        {id:8,value:this.state.IdProceso},
                                                        {id:9,value:this.state.IdProcesoP}
                                                    ]
                                            let duplicados_disp=this.validateDuplicados(array)
                                            let duplicados_procesos=this.validateDuplicados(array2)
                                            let vacio=this.validate()
                                            let space=this.validateSpaceSMA()
                                            // alert(space)
                                            // alert(duplicados)
                                            // alert(vacio)
                                            if(vacio && duplicados_disp && duplicados_procesos && space){
                                                //alert("conecta SMA")
                                                this.conectarSMA("1")
                                            }else if(!vacio){
                                                toast['warning']('Favor de no dejar campos vacios, verifique sus registros', { autoClose: 4000 })
                                            }else if(!duplicados_disp){
                                                toast['warning']('Favor de no ingresar Id duplicados de Dispositivos', { autoClose: 4000 })
                                            }else if(!duplicados_procesos){
                                                toast['warning']('Favor de no ingresar Id duplicados de Procesos', { autoClose: 4000 })
                                            }else if(!space){
                                                toast['warning']('Favor de no dejar espacios en las credenciales de usuario y contraseña', { autoClose: 4000 })
                                            }
                                        }else{
                                            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
                                        }
                                    })}>Asignar</Button>
                                </ButtonGroup>
                                
                            </ModalBody>
                            <ModalFooter>
                        </ModalFooter>
                    </Modal>
                    <Modal style={{maxHeight:660,maxWidth:`290px`}} isOpen={this.state.modal3} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}><b>ID SMA </b></ModalHeader>
                            <ModalBody>
                                <ButtonGroup>
                                    {/* <Input placeholder="Nombre Area" autoComplete="off" value={this.state.AreaModal} name="AreaModal" onChange={this.handleChange} /> */}
                                    <Input
                                        id="editaId"
                                        name="editaId"
                                        type="text"
                                        ref={(ref) => { this.textInput = ref; }}
                                        focused
                                        value={this.state.editaId} onChange={this.handleChangeActivity}
                                        placeholder={this.state.editaInd!=""?document.querySelector("#id_disp_"+this.state.editaInd).value:""}
                                    />
                                    <Button color="primary" onClick={(()=>{
                                        if(this.state.editaInd!=""){
                                            document.querySelector("#id_disp_"+this.state.editaInd).value=this.state.editaId;
                                            if(this.state.asignaInd!=""){
                                                this.setState({editaId:"",["disp"+this.state.asignaInd]:this.state.editaId})
                                            }else{
                                                this.setState({editaId:"",TagsSMA:this.state.TagsSMA.map((t,i)=>{
                                                    if((i+1)==this.state.editaInd){
                                                        t.disp=this.state.editaId;
                                                        return t
                                                    }
                                                    return t
                                                })})
                                            }
                                            this.switchModal("-1")
                                        }else{
                                            toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
                                        }
                                    })}>Asignar</Button>
                                </ButtonGroup>
                                
                            </ModalBody>
                            <ModalFooter>
                        </ModalFooter>
                    </Modal>
                    <Modal style={{maxHeight:660,maxWidth:`1200px`}} isOpen={this.state.modal4} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}><b>Ultimos Parametros Registrados en SMA {this.state.ModalArray=="m"?"MODULO":"PONTON"}</b></ModalHeader>
                            <ModalBody>
                                <Row>
                                    <Col md="6">
                                        <Jumbotron>
                                            <pre style={{fontSize:15}}>
                                                {
                                                    this.state.ModalArray=="m"?
                                                    this.getEndDate(0,this.state.ModalArray,"5m y 10m"):
                                                    this.getEndDate(4,this.state.ModalArray,"5m y 10m")
                                                }
                                            </pre>
                                        </Jumbotron>
                                    </Col>
                                    <Col md="6">
                                        <Jumbotron>
                                            <pre style={{fontSize:15}}>
                                                {
                                                    this.state.ModalArray=="m"?
                                                    this.getEndDate(2,this.state.ModalArray,"Salinidad 5m y 10m"):
                                                    this.getEndDate(6,this.state.ModalArray,"Salinidad 5m y 10m")
                                                }
                                            </pre>
                                        </Jumbotron>
                                    </Col>
                                </Row>
                            </ModalBody>
                    </Modal>
                    <Modal style={{maxHeight:660,maxWidth:`1300px`}} isOpen={this.state.modal5} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}><b>Datos Historicos SMA</b></ModalHeader>
                            <ModalBody>
                                <TendenciaSMA Select={[{"name":"Modulo","_id":this.state.IdProceso},
                                                       {"name":"Ponton","_id":this.state.IdProcesoP}]}
                                              Divices={this.state.TagsSMA}></TendenciaSMA>
                                {/* disp1:cycle["Sensor5mId"],
                                disp2:cycle["Sensor10mId"],
                                let disp3=cycle["Ponton5mId"];
                                let disp4=cycle["Ponton10mId"]; */}
                            </ModalBody>
                            <ModalFooter>
                        </ModalFooter>
                    </Modal>
                {/* {console.log(this.state.Workplace)} */}
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Configuración SMA</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      <BreadcrumbItem tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{display:this.state.Centros!=null?"none":active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-back"> </i> Volver</Badge></a></BreadcrumbItem>
                      
                      </Breadcrumb>
                        
                      {/* <Row>
                        <code>{array}</code>
                      </Row> */}
                      <Row>
                                <Col md="5">
                                    <Card>
                                        <CardHeader>Información Ciclo Producción</CardHeader>
                                        <CardBody>
                                            
                                            <Row>
                                                <Col>
                                                    <Label for="exampleDate">*Nombre centro</Label>
                                                    <Input placeholder="Nombre centro" disabled value={this.state.NameWorkplace.name} />
                                                </Col>
                                                <Col>
                                                    <Label for="exampleDate">*Areas</Label>
                                                    <Row>
                                                        <ButtonGroup style={{marginLeft:9,width:`${100}%`}}>
                                                            <Input id="areas1" placeholder="Area" value={this.state.AreaId} name="AreaId" onChange={this.handleChangeActivity} type="select">
                                                                <option value="" disabled selected>Seleccione un Area</option>
                                                                {
                                                                    this.state.Areas.map((a)=>{
                                                                        return <option selected={a.selected} value={a._id}>{a.name}</option>
                                                                    })
                                                                }  
                                                            </Input>
                                                            <Button color="success" style={{display:active_config}} onClick={(()=>{
                                                                this.switchModal("1")
                                                            })}>+</Button>
                                                        </ButtonGroup>
                                                    </Row>
                                                </Col>
                                            </Row>
                                            <br></br>
                                            <Row>
                                                <Col>
                                                    <Label for="exampleDate">Fecha Inicio</Label>
                                                    <Input
                                                        type="date"
                                                        name="start_date"
                                                        value={this.state.start_date} onChange={this.handleChange}
                                                        placeholder="date placeholder"
                                                    />
                                                </Col>
                                                <Col>
                                                    <Label for="exampleDate">Fecha Fin Estimado</Label>
                                                    <Input
                                                        type="date"
                                                        name="estimated_date"
                                                        value={this.state.estimated_date} onChange={this.handleChange}
                                                        placeholder="date placeholder"
                                                    />
                                                </Col>
                                                
                                            </Row>
                                            <br></br>
                                            <Row>
                                                <Col>
                                                    <Label for="exampleDate">Fecha Fin Producción</Label>
                                                    <Input
                                                        type="date"
                                                        name="end_date"
                                                        placeholder="date placeholder"
                                                        value={this.state.end_date} onChange={this.handleChange}
                                                    />
                                                </Col>
                                                <Col>
                                                    
                                                </Col>
                                            </Row>
                                            <br></br>
                                            <CardTitle>Información de Contacto</CardTitle>
                                            <Row>
                                                <Col>
                                                    <Label>Nombre Responsable</Label>
                                                    <Input placeholder="Nombre" value={this.state.Nombre} name="Nombre" onChange={this.handleChange} />
                                                </Col>
                                                <Col>
                                                    <Label>Correo Responsable</Label>
                                                    <Input type="email" placeholder="ejemplo@idealcontrol.cl" value={this.state.Correo} name="Correo" onChange={this.handleChange} />
                                                </Col>
                                            </Row>
                                            <br></br>
                                            <Row>
                                                <Col>
                                                    <Label>Teléfono</Label>
                                                    <Input type="tel" placeholder="Ejemplo: 9xxxxxxxx" value={this.state.Telefono}
                                                     name="Telefono" onChange={this.handleChange} />
                                                </Col>
                                                <Col></Col>
                                            </Row>
                                            <hr></hr>
                                            <Row>
                                                <Col>
                                                  <Label>Correo Alarma (comunicacion inestable, datos estaticos, desconexión, alarmas de oxigeno y bateria)</Label>
                                                    <InputGroup>
                                                        <Input id="EmailCorreo" autoComplete="off" placeholder="Ingrese un Correo" />
                                                        <InputGroupAddon addonType="append">
                                                            <Button color="success" onClick={((e)=>{
                                                                this.AgregarCorreo()
                                                            })}>Añadir</Button>
                                                        </InputGroupAddon>
                                                    </InputGroup>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop:10}}>
                                                <Label>Correos Alarma</Label>
                                                    {
                                                        this.state.ListaCorreos.map((l,ind)=>{
                                                            return <Col style={{marginTop:5}}>
                                                                    <Button color="primary" outline>
                                                                        {l.trim()}<Badge color="danger" pill onClick={((e)=>{
                                                                            this.QuitarCorreo(ind);
                                                                        })}>x</Badge>
                                                                    </Button>
                                                                </Col>
                                                        
                                                        })
                                                    }
                                            </Row>
                                            <Row style={{marginTop:20}}>
                                                <Col>
                                                  <Label>Correo Alarma SMA (comunicacion inestable,datos estaticos,desconexión)</Label>
                                                    <InputGroup>
                                                        <Input id="EmailCorreo2" autoComplete="off" placeholder="Ingrese un Correo de SMA" />
                                                        <InputGroupAddon addonType="append">
                                                            <Button color="success" onClick={((e)=>{
                                                                this.AgregarCorreo2()
                                                            })}>Añadir</Button>
                                                        </InputGroupAddon>
                                                    </InputGroup>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop:10}}>
                                                <Label>Correos Alarma SMA</Label>
                                                    {
                                                        this.state.ListaCorreos2.map((l,ind)=>{
                                                            return <Col style={{marginTop:5}}>
                                                                    <Button color="primary" outline>
                                                                        {l.trim()}<Badge color="danger" pill onClick={((e)=>{
                                                                            this.QuitarCorreo2(ind);
                                                                        })}>x</Badge>
                                                                    </Button>
                                                                </Col>
                                                        
                                                        })
                                                    }
                                            </Row>
                                            <Row style={{marginTop:20}}>
                                                <Col>
                                                  <Label>Correo Alarma Soporte (comunicacion inestable, datos estaticos, desconexión, alarmas de oxigeno, alarma salinidad y bateria)</Label>
                                                    <InputGroup>
                                                        <Input id="EmailCorreo3" autoComplete="off" placeholder="Ingrese un Correo de Soporte" />
                                                        <InputGroupAddon addonType="append">
                                                            <Button color="success" onClick={((e)=>{
                                                                this.AgregarCorreo3()
                                                            })}>Añadir</Button>
                                                        </InputGroupAddon>
                                                    </InputGroup>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop:10}}>
                                                <Label>Correos Alarma Soporte</Label>
                                                    {
                                                        this.state.ListaCorreos3.map((l,ind)=>{
                                                            return <Col style={{marginTop:5}}>
                                                                    <Button color="primary" outline>
                                                                        {l.trim()}<Badge color="danger" pill onClick={((e)=>{
                                                                            this.QuitarCorreo3(ind);
                                                                        })}>x</Badge>
                                                                    </Button>
                                                                </Col>
                                                        
                                                        })
                                                    }
                                            </Row>
                                            <Row style={{marginTop:20}}>
                                                <Col>
                                                  <Label>Correo Alarma Oxigeno (alarmas de oxigeno)</Label>
                                                    <InputGroup>
                                                        <Input id="EmailCorreo4" autoComplete="off" placeholder="Ingrese un Correo" />
                                                        <InputGroupAddon addonType="append">
                                                            <Button color="success" onClick={((e)=>{
                                                                this.AgregarCorreo4()
                                                            })}>Añadir</Button>
                                                        </InputGroupAddon>
                                                    </InputGroup>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop:10}}>
                                                <Label>Correos Alarma Oxigeno</Label>
                                                    {
                                                        this.state.ListaCorreos4.map((l,ind)=>{
                                                            return <Col style={{marginTop:5}}>
                                                                    <Button color="primary" outline>
                                                                        {l.trim()}<Badge color="danger" pill onClick={((e)=>{
                                                                            this.QuitarCorreo4(ind);
                                                                        })}>x</Badge>
                                                                    </Button>
                                                                </Col>
                                                        
                                                        })
                                                    }
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Button color={"primary"} style={{marginTop:`${11}%`,width:`${100}%`,display:active_config}}
                                                    onClick={(()=>{
                                                        this.CrearCiclo("save_info")
                                                    })}>Guardar Centro</Button>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                                <Col md="7">
                                    <Card>
                                        <Row>
                                            <Col md="8">
                                            <CardHeader>Información SMA &nbsp; {this.viewConection()} &nbsp;</CardHeader>
                                            </Col>
                                            <Col md="4">
                                                <CardTitle>Id Centro: {this.state.NameWorkplace.id}</CardTitle>
                                            </Col>
                                        </Row>
                                        
                                        <CardBody>
                                            <CardTitle>Credenciales</CardTitle>
                                            <Row>
                                                <Col>
                                                    <Label for="exampleDate">*Id Unidad Fiscalizable</Label>
                                                    {/* {this.createTooltip("Uf","Id Unidad Fiscalizadora")} */}
                                                    <Input id="Uf" autoComplete="off" placeholder="Id Unidad Fiscalizable" value={this.state.IdUF} name="IdUF" onChange={this.handleChange} />
                                                </Col>
                                                <Col>
                                                    <Label>*Usuario SMA</Label>
                                                    <Input autoComplete="off" placeholder="Usuario" value={this.state.User} name="User" onChange={this.handleChange}></Input>
                                                </Col>
                                                <Col>
                                                    <Label>*Contraseña SMA</Label>
                                                    <Input autoComplete="off" type="password" placeholder="Contraseña" value={this.state.Pass} name="Pass" onChange={this.handleChange}></Input>
                                                </Col>
                                            </Row>
                                            <br></br>
                                            
                                            <hr></hr>
                                            <CardTitle>Datos de conexión Modulo</CardTitle>
                                            <Row>
                                                <Col>
                                                    {/* {this.createTooltip("Proceso","Id Proceso")} */}
                                                    <Label for="exampleDate">*Id de Proceso</Label>
                                                    <Input autoComplete="off" placeholder="Id de Proceso" value={this.state.IdProceso} name="IdProceso" onChange={this.handleChange} />
                                                </Col>
                                                <Col>
                                                    <Label>Latitud</Label>
                                                    <Input placeholder="Ej: -41.46574" value={this.state.Latitud} name="Latitud" onChange={this.handleChange} />
                                                </Col>
                                                <Col>
                                                    <Label>Longitud</Label>
                                                    <Input placeholder="Ej: -41.46574-72.94289" value={this.state.Longitud} name="Longitud" onChange={this.handleChange} />
                                                </Col>
                                            </Row>
                                            <br></br>
                                            <Row>
                                                <Col>
                                                    <Card>
                                                        <CardHeader>
                                                            <Row style={{width:`${100}%`}}>
                                                                <Col>
                                                                    <Label>Dispositivos</Label>
                                                                </Col>
                                                                <Col style={{display:this.state.Cycles.length>0?"block":"none"}}><Badge color="primary" style={{cursor:"pointer"}} pill onClick={((e)=>{
                                                                    this.switchModal("4","m")
                                                                    //this.getMyCycle(this.state.NameWorkplace.id,true);
                                                                })}>Ultimos Registros Modulo {
                                                                    this.state.Cycles.length>0?
                                                                    this.getEndDate(0)
                                                                    :"none"
                                                                }</Badge></Col>
                                                                <Col></Col>
                                                                <Col></Col>
                                                                <Col></Col>
                                                                <Col></Col>
                                                            </Row>
                                                        </CardHeader>
                                                        <CardBody>
                    
                                                        <ReactTable                                                           
                                                            data={this.state.TagsSMA.filter((d,i)=>i!=4&&i!=5&&i!=6&&i!=7).map((d,i)=>{
                                                                d.ind=i+1;
                                                                return d
                                                            })}
                                                                        
                                                            // loading= {false}
                                                            showPagination= {false}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {false}
                                                            showPageSizeOptions= {false}
                                                            //pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={4}
                                                            // getTrProps={onRowClick}
                                                            columns={[
                                                                {
                                                                    Header: "n°",
                                                                    accessor: "ind",
                                                                    width: 33
                                                                    },
                                                                    {
                                                                    Header: "Modulos",                                                             
                                                                    width: 195,
                                                                    Cell: ({ row }) => (
                                                                        <Fragment>
                                                                            <Label>{row._original.name} 
                                                                                {this.getStatus(row)}
                                                                            </Label>
                                                                            
                                                                        </Fragment>
                                                                        )
                                                                    },
                                                                    {
                                                                        Header: "Observación",                                                              
                                                                        width: 215,
                                                                        Cell: ({ row }) => (
                                                                            <Fragment>
                                                                                <Input id={"estado"+row._original.ind} name={"estado"+row._original.ind} value={this.state["estado"+row._original.ind]}
                                                                                 onChange={this.handleChangeActivity} type="select">
                                                                                    <option  value="" selected disabled>Seleccione un Estado</option>
                                                                                    {this.state.Estados.map((e)=>{
                                                                                        if(row._original.estado==e.id){
                                                                                            return <option selected value={e.id}>{e.name}</option>
                                                                                        }
                                                                                        return <option value={e.id}>{e.name}</option>
                                                                                    })}
                                                                                </Input>
                                                                            </Fragment>
                                                                            )
                                                                    },
                                                                    {
                                                                        Header: "Instrumentos",                                                              
                                                                        width: 215,
                                                                        Cell: ({ row }) => (
                                                                            <Fragment>
                                                                                <Input id={"option"+row._original.ind} name={"option"+row._original.ind} value={this.state["option"+row._original.ind]}
                                                                                onChange={this.handleChangeActivity} type="select">
                                                                                    <option value="0" selected disabled>Seleccione un Sensor</option>
                                                                                    {this.getOptionsDivice(row._original.value)}
                                                                                </Input>
                                                                            </Fragment>
                                                                            )
                                                                    },
                                                                    {
                                                                        Header: "Id Dispositivo SMA",                                                              
                                                                        width: 185,
                                                                        Cell: ({ row }) => (
                                                                            <Fragment>
                                                                                {this.valueDispositivo(row._original)}
                                                                            </Fragment>
                                                                            )
                                                                    },
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                            <br></br>
                                                            
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row>
                                            <hr></hr>
                                            <CardTitle>Datos de conexión Ponton</CardTitle>
                                            <Row>
                                                <Col>
                                                    {/* {this.createTooltip("Proceso","Id Proceso")} */}
                                                    <Label for="exampleDate">*Id de Proceso</Label>
                                                    <Input autoComplete="off" placeholder="Id de Proceso" value={this.state.IdProcesoP} name="IdProcesoP" onChange={this.handleChange} />
                                                </Col>
                                                <Col>
                                                    <Label>Latitud</Label>
                                                    <Input placeholder="Ej: -41.46574" value={this.state.LatitudP} name="LatitudP" onChange={this.handleChange} />
                                                </Col>
                                                <Col>
                                                    <Label>Longitud</Label>
                                                    <Input placeholder="Ej: -41.46574-72.94289" value={this.state.LongitudP} name="LongitudP" onChange={this.handleChange} />
                                                </Col>
                                            </Row>
                                            <hr></hr>
                                            <Row>
                                                <Col>
                                                    <Card>
                                                        <CardHeader>
                                                            <Row style={{width:`${100}%`}}>
                                                                <Col>
                                                                    <Label>Dispositivos</Label>
                                                                </Col>
                                                                <Col style={{display:active_config}}><Badge color="primary" style={{cursor:"pointer"}} pill onClick={((e)=>{
                                                                    this.switchModal("4","p")
                                                                })}>Ultimos Registros Ponton  {
                                                                    this.state.Cycles.length>0?
                                                                    this.getEndDate(4)
                                                                    :"none"
                                                                }</Badge></Col>
                                                                <Col></Col>
                                                                <Col></Col>
                                                                <Col></Col>
                                                                <Col></Col>
                                                            </Row>
                                                        </CardHeader>
                                                        <CardBody>
                                                        <ReactTable                                                           
                                                            data={this.state.TagsSMA.filter((d,i)=>i!=0&&i!=1&&i!=2&&i!=3).map((d,i)=>{
                                                                d.ind=i+5;
                                                                return d
                                                            })}
                                                                        
                                                            // loading= {false}
                                                            showPagination= {false}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {false}
                                                            showPageSizeOptions= {false}
                                                            //pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={4}
                                                            // getTrProps={onRowClick}
                                                            columns={[
                                                                {
                                                                    Header: "n°",
                                                                    accessor: "ind",
                                                                    width: 33
                                                                    },
                                                                    {
                                                                    Header: "Pontones",                                                             
                                                                    width: 190,
                                                                    Cell: ({ row }) => (
                                                                        <Fragment>
                                                                            <Label>{row._original.name} 
                                                                                {this.getStatus(row)}
                                                                            </Label>
                                                                            
                                                                        </Fragment>
                                                                        )
                                                                    },
                                                                    {
                                                                        Header: "Observación",                                                              
                                                                        width: 215,
                                                                        Cell: ({ row }) => (
                                                                            <Fragment>
                                                                                <Input id={"estado"+row._original.ind} name={"estado"+row._original.ind} value={this.state["estado"+row._original.ind]}
                                                                                onChange={this.handleChangeActivity} type="select">
                                                                                    <option value="" selected disabled>Seleccione un Estado</option>
                                                                                    {this.state.Estados.map((e)=>{
                                                                                        if(row._original.estado==e.id){
                                                                                            return <option selected value={e.id}>{e.name}</option>
                                                                                        }
                                                                                        return <option value={e.id}>{e.name}</option>
                                                                                    })}
                                                                                </Input>
                                                                            </Fragment>
                                                                            )
                                                                    },
                                                                    {
                                                                        Header: "Instrumentos",                                                              
                                                                        width: 215,
                                                                        Cell: ({ row }) => (
                                                                            <Fragment>
                                                                                <Input id={"option"+row._original.ind} name={"option"+row._original.ind} value={this.state["option"+row._original.ind]}
                                                                                onChange={this.handleChangeActivity} type="select">
                                                                                    <option value="0" selected disabled>Selecciones un sensor</option>
                                                                                    {this.getOptionsDivice(row._original.value)}
                                                                                </Input>
                                                                            </Fragment>
                                                                            )
                                                                    },
                                                                    {
                                                                        Header: "Id Dispositivo SMA",                                                              
                                                                        width: 185,
                                                                        Cell: ({ row }) => (
                                                                            <Fragment>
                                                                                {this.valueDispositivo(row._original)}
                                                                            </Fragment>
                                                                            )
                                                                    },
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                            <br></br>
                                                            
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row>
                                            <Row>
                                            <Col md="12"><Button disabled={this.state.action} style={{width:`${100}%`,display:active_config}} color="primary" onClick={((e)=>{
                                                this.setState({action:true})
                                                this.state.Cycles.length>0?this.question():this.CrearCiclo()
                                            })}>{this.state.Cycles.length>0?"Modificar":"Guardar"}</Button></Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                               
                            </Row>
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ConfiguracionSMA);
