import React, { useEffect, useState } from 'react'
import Modulos from './Modulos';
import { TagServices } from '../../../../../services/comun';

const Diagnostico = ({numero,zonas,dispositivos,dispositivos2,centro,indice}) => {

    const [collapse, setCollapse] = useState([])
    const [valor, setValor] = useState('')
    const [centroControl, setcentroControl] = useState(false)
    const [locationActualizados, setLocationActualizados] = useState([])
    const tagservices=new TagServices();

    useEffect(() => {
        if(valor==''){
            setValor(numero)
        }
        // if(indice==0){
        //     setInterval(() => {
        //         console.log('hola')
        //         let ids=dispositivos.map(d=>{ return({_id:d.locationId}) })
        //         tagservices.getLocationIds(ids).then(respuesta=>{
        //             if(respuesta.statusCode===201){
        //                 console.log(respuesta.data)
        //                 setLocationActualizados(respuesta.data)
        //             }
        //         })
        //     },30000)
        // }
    }, [dispositivos2,centro])
    

    const toggle=(e)=> {
        let event = e.target.dataset.event;
        setCollapse(collapse === Number(event) ? 0 : Number(event))
    }

    const index=0

    return (
        <>
            {
                zonas.map(m=>{
                    return m.zonas.map((z,indice)=>{
                        return(
                            <>
                                <Modulos 
                                    key={indice}
                                    toggle={toggle}
                                    numero={numero}
                                    index={indice}
                                    collapse={collapse}
                                    modulo={z}
                                    dispositivos={dispositivos.filter(d=>d.centroId==centro._id)}
                                    dispositivos2={dispositivos2}
                                    centro={centro}
                                    // locationActualizados={locationActualizados}
                                />
                            </>
                        )
                    })
                })
            }
        </>
    )
}

export default Diagnostico