import React from 'react'
import {Col,ListGroupItem,Row,Collapse,Badge} from 'reactstrap';

const Dispositivos = ({dispositivos,dispositivos2,estadoDispositivo}) => {

    
    const obtenerProfundidad=(nombre)=>{
        let split=nombre.split('_')
        if(nombre.includes('Salinidad')){
            split=nombre.split(' ')
            return split[1]
        }
        return split[0].replace('OD','')
    }

    return (
        <>
            <Row>
                <table style={{marginLeft:'auto',marginRight:0}}>
                    <tr style={{textAlign:'center'}}>
                    {
                        dispositivos.map(d=>{
                            return(
                            <>
                                <td>
                                    {estadoDispositivo(
                                         dispositivos2.filter(d2=>d2.id==d.id).length > 0?
                                         dispositivos2.filter(d2=>d2.id==d.id)[0].status:
                                         d.status,
                                         dispositivos2.filter(d2=>d2.id==d.id).length > 0?
                                         dispositivos2.filter(d2=>d2.id==d.id)[0].name:
                                         d.name,
                                    )}
                                </td>
                                <td>&nbsp;</td>
                            </>
                            )
                        })
                    }
                    </tr>
                    <tr>
                    {
                        dispositivos.map(d=>{
                            return(
                            <><td>{obtenerProfundidad(d.name)}</td><td>&nbsp;</td></>
                            )
                        })
                    }
                    </tr>
                </table>
            </Row>
        </>
    )
}

export default Dispositivos