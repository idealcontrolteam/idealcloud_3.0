import React, { useEffect, useState } from 'react'
import {ListGroupItem,Col,Row,Badge} from 'reactstrap';
import Dispositivos from './Dispositivos';
import Jaulas from './Jaulas'
import * as _ from "lodash";

const Zonas = ({
    toggle,
    numero,
    index,
    collapse,
    modulo,
    dispositivos,
    dispositivos2,
    centro,
    // locationActualizados,
    }) => {

    const estadoDispositivo =(estado,shortName)=>{
        let sal=shortName.includes('sal')?true:false;
        if(estado){
            if(sal){
                if(estado=='salinidad 33' || estado=='salinidad 34'){
                    return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-warning`} style={{backgroundColor:'#59B6F6',color:'black',width:18,height:18}}>
                               {sal?'Success':(<>&nbsp;</>)}
                            </span>
                }else if(estado=='sin datos'){
                    return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-danger`} style={{backgroundColor:'#d92550',color:'black',width:18,height:18}}>
                               {sal?'Success':(<>&nbsp;</>)}
                            </span>
                }else if(estado=='en cero'){
                    return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-danger`} style={{backgroundColor:'#f7b924',color:'black',width:18,height:18}}>
                               {sal?'Success':(<>&nbsp;</>)}
                            </span>
                }
                else{
                    return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-success`} style={{width:18,height:18}}>
                        {sal?'Success':(<>&nbsp;</>)}
                    </span>
                }
            }
            else if(estado=='en cero'){
                return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-warning`} style={{width:18,height:18}}>
                    {sal?'Success':(<>&nbsp;</>)}
                </span>
            }else if(estado=='pegados'){
                return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-warning`} style={{backgroundColor:'yellow',color:'blue',width:18,height:18}}>
                            {sal?'Success':(<>&nbsp;</>)}
                        </span>
            }
            else if(estado=='sin datos'){
                return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-danger`} style={{width:18,height:18}}>
                    {sal?'Success':(<>&nbsp;</>)}
                </span>
            }else{
                return <span className={`badge ${sal?'badge-dot badge-dot-xl':'badge-pill'} badge-success`} style={{width:18,height:18}}>
                    {sal?'Success':(<>&nbsp;</>)}
                </span>
            }
        }else{
            return <span class="badge badge-pill badge-success" style={{width:18,height:18}}>
                &nbsp;
            </span>
        }
    }

    return (
    <>
        <ListGroupItem action onClick={toggle} data-event={index+numero} style={{fontWeight: 'bold'}}>
            <Row onClick={toggle} data-event={index+numero} >
                <Col md={1} onClick={toggle} data-event={index+numero}>
                    <b><span className="pe-7s-keypad" style={{fontSize:25,color:'green'}}></span></b>
                </Col>
                <Col onClick={toggle} data-event={index+numero}>{modulo}</Col>
                <Col>
                    {centro.name.includes('Control')?(
                        <Jaulas 
                        dispositivos={dispositivos.filter(d=>d.name.includes(modulo))}
                        dispositivos2={dispositivos2}
                        estadoDispositivo={estadoDispositivo}
                        // locationActualizados={locationActualizados}
                        />
                    ):(
                        <Dispositivos
                        dispositivos={dispositivos.filter(d=>d.name.includes(modulo))}
                        dispositivos2={dispositivos2}
                        estadoDispositivo={estadoDispositivo}
                        // locationActualizados={locationActualizados}
                        />
                    )}
                </Col>
            </Row>
        </ListGroupItem>
        {/* {centroControl?(
            <Jaulas 
                toggle={toggle}
                collapse={collapse}
                numero={numero}
                index={index}
                dispositivos={dispositivos.filter(d=>d.name.includes(modulo))}
            />
        ):(
            <Dispositivos
                toggle={toggle}
                collapse={collapse}
                numero={numero}
                index={index}
                dispositivos={dispositivos.filter(d=>d.name.includes(modulo))}
            />
        )} */}
    </>
    )
}

export default Zonas