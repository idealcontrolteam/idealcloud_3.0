import React, { useEffect, useState } from 'react'
import {Col,ListGroupItem,Row,Collapse,Badge} from 'reactstrap';
import * as _ from "lodash";
import Table from 'reactstrap/lib/Table';

const Jaulas = ({dispositivos,dispositivos2,estadoDispositivo}) => {

    const [pares, setPares] = useState([])
    const [inpares, setInpares] = useState([])

    const getPar=(name)=>{
        let separador=name.split(' ');
        let separador2=String(separador[1]).split('_');
        let num=parseInt(separador2[0]);
        if(num%2==0){
          return "par"
        }else{
          return "inpar"
        }
    }

    const getFilterData=(dispositivos,type)=>{
        return _.orderBy(dispositivos.filter(d=>getPar(d.name)==type&&
                d.name.includes('oxd')&&
                !d.name.includes('Volt'))
        ,["order"],"asc")
    }

    useEffect(() => {
        if(dispositivos){
            setPares(getFilterData(dispositivos,'par'))
            setInpares(getFilterData(dispositivos,'inpar'))
        }
    }, [dispositivos])

    const obtenerNumeroJaula=(nombre)=>{
        let split=nombre.split(' ')
        let split2=split[1].split('_')
        return 'J'+split2[0].replace('Salinidad','')
    }

    const jaulasView =(data)=>{
        return (
        <table style={{marginLeft:'auto',marginRight:0}}>
            <tr style={{textAlign:'center'}}>
            {
                data.map(d=>{
                    return(
                    <>
                        <td>
                            {estadoDispositivo(
                                dispositivos2.filter(d2=>d2.id==d.id).length > 0?
                                dispositivos2.filter(d2=>d2.id==d.id)[0].status:
                                d.status,
                                dispositivos2.filter(d2=>d2.id==d.id).length > 0?
                                dispositivos2.filter(d2=>d2.id==d.id)[0].name:
                                d.name,
                            )}
                        </td>
                        <td>&nbsp;</td>
                    </>
                    )
                })
            }
            </tr>
            <tr>
            {
                data.map(d=>{
                    return(
                    <>
                        <td>{obtenerNumeroJaula(d.name)}</td>
                        <td>&nbsp;</td>
                    </>
                    )
                })
            }
            </tr>
        </table>)
    }

    return (
        <>
            {jaulasView(pares)}
            {jaulasView(inpares)}
        </>
    )
}

export default Jaulas