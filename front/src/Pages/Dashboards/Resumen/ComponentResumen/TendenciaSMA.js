import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError, clearFields } from 'redux-form';
import * as _ from "lodash";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");
let disabled=false;
let load=false;
let visible="none";
let active_config="none"
let workplaceId=false;
let filtrar=new Function();
let check1=true;
let check2=false;
let check3=false;
let check4=false;

class Datos_Historicos extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          Export1:[],
          Export2:[],
          Export3:[],
          Export4:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          MultiExport:false,
          disp1:"",
          disp2:"",
          disp3:"",
          disp4:"",
          Registros:[],
          check1:false,
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);   
        this.filtrar2 =this.filtrar2.bind(this);   
    }

  
    componentDidMount = () => {
        
        visible="none";
        window.scroll(0, 0);
        sessionCheck()
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        // this.tagservices.getDataTag().then(data => 
        //     this.setState({Tags: data})
        // );  
        //this.this_Centro=setInterval(()=>this.getIdCentro(),2000);
        //var data = JSON.parse(sessionStorage.getItem('workplace'));
        // alert(JSON.stringify(data.id));
        // this.getIdCentro();
        this.setState({redirect:false})
        //console.log(this.props.Divices)
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getTendencia(this.props.Divices);
        }
      }

      componentWillUnmount() {
        clearInterval(this.getSondas);
      }

      // getTendencia=(divices)=>{
      //   let divices_ids=divices.filter(d=>d.ind!=3&&d.ind!=4&&d.ind!=6&&d.ind!=7).map(d=>d.disp)
      //   console.log(divices_ids)

      // }
    //  getIdCentro=()=>{
    //     let url = window.location.href;
    //     //console.log(alert);
    //     let s=url.split('/');
    //     console.log(s[s.length-1])

    //     let now = new Date(); 
    //     const f1 = moment(now).subtract(360, "minutes").format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    //     const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";  
    //     const token = "tokenFalso";

    //     axios.get(`${API_ROOT}/measurement/xy/tag/${s[s.length-1]}/${f1}/${f2}`, {
    //          'Authorization': 'Bearer ' + token
    //     })
    //     .then(response => {
    //         const measurements = response.data.data;
    //         console.log(measurements);
    //         //this.setState({ dateTimeRefresh:measurements[measurements.length-1].x});
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    //     return s[s.length-1]
    //  }
    


    //carga los datos para la Exportación de la tendencia
    // loadDataChart2 = (shortName, URL, ind,largo,workplaceId) => {   
        
    //     //console.log(URL)
    //      const token = "tokenFalso";
    //      axios.get(URL, {headers: {'Authorization': 'Bearer ' + token}})
    //     .then(response => { 
    //       if (response.data.statusCode === 200) {
    //         let dataChart = response.data.data;

    //         var data=dataChart.map((d)=>{
    //           d.name=shortName;
    //           d.dateTime=moment(d.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm')
    //           d.value=d.value.toString().replace(".",",")
    //           d.ind=ind;
    //           return d;
    //         })
    //         //console.log(data)            
    //         if(shortName=="oxd"){
    //           this.setState({
    //             Export1:this.state.Export1.concat(data)
    //           })
    //         }
    //         if(shortName=="oxs"){
    //           this.setState({
    //             Export2:this.state.Export2.concat(data)
    //           })
    //         }
    //         if(shortName=="temp"){
    //           this.setState({
    //             Export3:this.state.Export3.concat(data)
    //           })
    //         }
    //         // if(shortName=="sal"){
    //         //   if(largo==ind){
    //         //     this.setState({
    //         //       Export4:this.state.Export4.concat(data),blocking: false
    //         //     })
    //         //   }else{
    //         //     this.setState({
    //         //       Export4:this.state.Export4.concat(data)
    //         //     })
    //         //   }
             
    //         // }
            
    //      }
    //     })
    //     .catch(error => {
    //       // console.log(error);
    //     });
  
    //   }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =(new_fecha) => {
        this.setState({buttonExport:'none',Registros:[],blocking: false});
         const TagsSelecionado = this.state.TagsSelecionado;
         
         if (this.props.Divices !== null){
          visible="none";
          let f1="";
          let f2="";
          if(new_fecha!=null){
            this.setState({start:new_fecha})
            f1 = new_fecha.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
          }else{
            f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
          }
            //this.filtrar();
             //f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;

             const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            
            //  console.time('loop');
            // let locations=TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'));
            // let largo=locations.length-1;
              
              let ids=this.props.Divices.map(d=>{return {"dispositivoId":d.disp}})
              this.tagservices.getSensors_array(ids,f1,f2).then(response=>{
                //console.log(response.data.data)
                if(response.data.statusCode==201 || response.data.statusCode==200){
                  disabled=false
                  // console.log(this.props.Divices)
                  // let divices=this.props.Divices.filter(d=>d.ind!=3&&d.ind!=4&&d.ind!=7&&d.ind!=8)
                  
                  let data=response.data.data;
                  // console.log(data)
                  let sma_disp=[]
                  
                  sma_disp.push(data.map(d=>{
                    let ind=d.dispositivoId
                    try{
                      if(d.parametros.length==1){
                        return {
                          x:moment(this.tagservices.reformatDate(d.dateTime)).format("DD-MM-YYYY HH:mm:ss"),
                          //["data"+ind]:d.parametros,
                          "disp":ind,
                          // ["oxd"+ind]:d.parametros[0].valor,
                          // ["temp"+ind]:d.parametros[1].valor,
                          ["sal"+ind]:d.parametros[0].valor,
                          ["verificacion"+ind]:d.respuesta[0].IdVerificación,
                        }
                      }else{
                        return {
                          x:moment(this.tagservices.reformatDate(d.dateTime)).format("DD-MM-YYYY HH:mm:ss"),
                          //["data"+ind]:d.parametros,
                          "disp":ind,
                          ["oxd"+ind]:d.parametros[0].valor,
                          ["temp"+ind]:d.parametros[1].valor,
                          // ["sal"+ind]:d.parametros[2].valor,
                          ["verificacion"+ind]:d.respuesta[0].IdVerificación,
                        }
                      }
                      
                    }catch(e){
                      if(d.parametros.length==1){
                        return {
                          x:moment(this.tagservices.reformatDate(d.dateTime)).format("DD-MM-YYYY HH:mm:ss"),
                          //["data"+ind]:d.parametros,
                          "disp":ind,
                          // ["oxd"+ind]:d.parametros[0].valor,
                          // ["temp"+ind]:d.parametros[1].valor,
                          ["sal"+ind]:d.parametros[2].valor,
                          ["verificacion"+ind]:d.respuesta[0].IdVerificación,
                        }
                      }else{
                        return {
                          x:moment(this.tagservices.reformatDate(d.dateTime)).format("DD-MM-YYYY HH:mm:ss"),
                          //["data"+ind]:d.parametros,
                          "disp":ind,
                          ["oxd"+ind]:d.parametros[0].valor,
                          ["temp"+ind]:d.parametros[1].valor,
                          // ["sal"+ind]:d.parametros[2].valor,
                          ["verificacion"+ind]:d.respuesta[0].IdVerificación,
                        }
                      }
                    }
                   
                  }))
                  // console.log(sma_disp)
                  let global_register=[];
                  let data1=sma_disp[0].filter(d=>d.disp==this.props.Divices[0].disp);
                  let data2=sma_disp[0].filter(d=>d.disp==this.props.Divices[1].disp);
                  let data3=sma_disp[0].filter(d=>d.disp==this.props.Divices[4].disp);
                  let data4=sma_disp[0].filter(d=>d.disp==this.props.Divices[5].disp);

                  let datasal1=sma_disp[0].filter(d=>d.disp==this.props.Divices[2].disp);
                  let datasal2=sma_disp[0].filter(d=>d.disp==this.props.Divices[3].disp);
                  let datasal3=sma_disp[0].filter(d=>d.disp==this.props.Divices[6].disp);
                  let datasal4=sma_disp[0].filter(d=>d.disp==this.props.Divices[7].disp);
                  data1.map((d,ind)=>{
                    let data_join1=this.jsonConcat(d,data2[ind])
                    let data_join2=this.jsonConcat(data3[ind],data4[ind])

                    let conjunto1=this.jsonConcat(data_join1,data_join2)

                    let data_join3=this.jsonConcat(datasal1[ind],datasal2[ind])
                    let data_join4=this.jsonConcat(datasal3[ind],datasal4[ind])

                    let conjunto2=this.jsonConcat(data_join3,data_join4)


                    global_register.push(this.jsonConcat(conjunto1,conjunto2))
                  })
                  // console.log(global_register)
                  this.setState({Registros:global_register})
                }
              }).catch(e=>e)
                            // ||l.name.includes('inyection')
                            // ||l.name.includes('setpoint')
                            // ||l.name.includes('banda_superior')
                            // ||l.name.includes('banda_inferior')
              // let largo=divices_ids.length-1;
              // for (let i = 0; i < divices_ids.length; i++) {
              // let APItagMediciones="";
              // if(this.state.NameWorkplace.name.includes(" Control")){
              //   APItagMediciones=`${API_ROOT}/register_sma/sensor/${tags[i]}/${f1}/${f2}`;
              // }else{
              //   APItagMediciones = `${API_ROOT}/register_sma/sensor/${tags[i]}/${f1}/${f2}`;
              // }
              // //console.log(APItagMediciones);
              //   this.loadDataChart2(tags[i].shortName,APItagMediciones,largo,this.state.NameWorkplace.id);
              // }
            //console.log(this.state.Export1)
            
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        // isMulti
                        name="colors"
                        options={Tags}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Sensor"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
        let value1 = this.state.start.format("DD-MM-YYYY") 
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
      }

    buttonChangeDate=(n)=>{
        let now = new Date();
        let hoy = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let new_fecha=moment(hoy).subtract(n, "days")
        // this.setState({
        //   start:new_fecha
        // })
        disabled=true;
        this.setState({
            dataExport:[],
            buttonExport:'block'
        });
        this.filtrar2(new_fecha);
    }

    getNameDispositivo(){
      if(this.state.TagsSelecionado!=null){
        return this.state.TagsSelecionado.name
      }else{
        return ""
      }
    }

    createArray(ind,fecha,oxd,oxs,temp,sal){
      let obj={};
      if(ind==0){
        obj["x"+ind]=fecha;
        obj["oxd"+ind]=oxd;
        obj["oxs"+ind]=oxs;
        obj["temp"+ind]=temp;
        // obj["sal"+ind]=sal;
      }else{
        obj["oxd"+ind]=oxd;
        obj["oxs"+ind]=oxs;
        obj["temp"+ind]=temp;
        // obj["sal"+ind]=sal;
      }
      return obj
      
    }

    pushArray(contador,g,g1,g2,g3,g4,g5,g6,g7,ind,ind1,ind2,ind3,ind4,ind5,ind6,ind7){
      let c=0;
      let array1=[]
      while(c<contador){
      c++;
      let data={}
      try{
        data["x0"]=g.oxd[c-1].dateTime
        data["oxd"+ind]=g.oxd[c-1].value
        data["oxs"+ind]=g.oxs[c-1].value
        data["temp"+ind]=g.temp[c-1].value
        // data["sal"+ind]=g.sal[c-1].value
      if(g1!=null){
        data["oxd"+ind1]=g1.oxd[c-1].value
        data["oxs"+ind1]=g1.oxs[c-1].value
        data["temp"+ind1]=g1.temp[c-1].value
        // data["sal"+ind1]=g1.sal[c-1].value
      }
      if(g2!=null){
        data["oxd"+ind2]=g2.oxd[c-1].value
        data["oxs"+ind2]=g2.oxs[c-1].value
        data["temp"+ind2]=g2.temp[c-1].value
        // data["sal"+ind2]=g2.sal[c-1].value
      }
      if(g3!=null){
        data["oxd"+ind3]=g3.oxd[c-1].value
        data["oxs"+ind3]=g3.oxs[c-1].value
        data["temp"+ind3]=g3.temp[c-1].value
        // data["sal"+ind3]=g3.sal[c-1].value
      }
      if(g4!=null){
        data["oxd"+ind4]=g4.oxd[c-1].value
        data["oxs"+ind4]=g4.oxs[c-1].value
        data["temp"+ind4]=g4.temp[c-1].value
        // data["sal"+ind4]=g4.sal[c-1].value
      }
      if(g5!=null){
        data["oxd"+ind5]=g5.oxd[c-1].value
        data["oxs"+ind5]=g5.oxs[c-1].value
        data["temp"+ind5]=g5.temp[c-1].value
        // data["sal"+ind5]=g5.sal[c-1].value
      }
      if(g6!=null){
        data["oxd"+ind6]=g6.oxd[c-1].value
        data["oxs"+ind6]=g6.oxs[c-1].value
        data["temp"+ind6]=g6.temp[c-1].value
        // data["sal"+ind6]=g6.sal[c-1].value
      }
      if(g7!=null){
        data["oxd"+ind7]=g7.oxd[c-1].value
        data["oxs"+ind7]=g7.oxs[c-1].value
        data["temp"+ind7]=g7.temp[c-1].value
        // data["sal"+ind7]=g7.sal[c-1].value
      }
    }catch(e){
      console.log(e)
    }
      array1.push(data)
      }
      return array1
    }

    jsonConcat(o1, o2) {
      for (var key in o2) {
       o1[key] = o2[key];
      }
      return o1;
    }

    getArray(){
        // console.log(this.state.Registros)
        let global_register=[];
        try{
          //console.log(this.state.Registros[0].data)
          let data1=this.state.Registros[0].data;
          let data2=this.state.Registros[1].data;
          let data3=this.state.Registros[2].data;
          let data4=this.state.Registros[3].data;

          let data1sal=this.state.Registros[4].data;
          let data2sal=this.state.Registros[5].data;
          let data3sal=this.state.Registros[6].data;
          let data4sal=this.state.Registros[7].data;
          let i=0;
          this.state.Registros[0].data.map((d,ind)=>{
            let data_join1=this.jsonConcat(d,data2[ind])
            let data_join2=this.jsonConcat(data3[ind],data4[ind])

            let conjunto1=this.jsonConcat(data_join1,data_join2)

            let data_join3=this.jsonConcat(data1sal[ind],data2sal[ind])
            let data_join4=this.jsonConcat(data3sal[ind],data4sal[ind])

            let conjunto2=this.jsonConcat(data_join3,data_join4)

            global_register.push(this.jsonConcat(conjunto1,conjunto2))
          })
          //console.log(global_register)
          visible=true;
          return global_register.reverse()
        }catch(e){
          //console.log(e)
        }
        
        // let oxd=this.state.Export1;
        // let oxs=this.state.Export2;
        // let temp=this.state.Export3;
        // let sal=this.state.Export4;
        // if(this.state.TagsSelecionado!=null && disabled==false){
        //   //console.log(oxd)
        //   visible="block";
        //   let loc=this.state.TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'))
        //   let count=loc.length;
        //   let location=loc;
        //   let array=[]
        //   if(oxd.length>0&&oxs.length>0&&temp.length>0&&sal.length>0){
        //     let global=oxd.concat(oxs).concat(temp).concat(sal).map((g)=>{
        //       return {type: g.name, value:g.value, dateTime:g.dateTime, ind:g.ind}
        //     })
        //     let contador=0;
        //     location.map((l,i)=>{
        //       contador=global.filter(g=>g.ind==i&&g.type=='oxd').length;
        //       if(workplaceId){
        //         array.push({
        //           oxd:global.filter(g=>g.ind==i&&g.type=='oxd'),
        //           oxs:global.filter(g=>g.ind==i&&g.type=='temp'),
        //           temp:global.filter(g=>g.ind==i&&g.type=='oxs'),
        //           sal:global.filter(g=>g.ind==i&&g.type=='sal'),
        //         })
        //       }else{
        //           array.push({
        //             oxd:global.filter(g=>g.ind==i&&g.type=='oxd'),
        //             oxs:global.filter(g=>g.ind==i&&g.type=='oxs'),
        //             temp:global.filter(g=>g.ind==i&&g.type=='temp'),
        //             sal:global.filter(g=>g.ind==i&&g.type=='sal'),
        //           })
        //       }
        //     })
        //     //return console.log(contador)
        //     let array1=[]
        //     if(contador>0){
        //       //return console.log(array[0])
        //       array1.push(this.pushArray(contador,array[0],
        //                                           array[1],
        //                                           array[2],
        //                                           array[3],
        //                                           array[4],
        //                                           array[5],
        //                                           array[6],
        //                                           array[7],
        //                                           0,1,2,3,4,5,6,7
        //                                 )
        //                   )
        //       return array1[0].reverse()
        //     }
            //return console.log(array)
            
        //   }
          
        // }else{
        //   return []
        // }
      
      
    }

    showTableExport(){
      if(this.state.MultiExport && this.state.buttonExport=="none"&&load==false){
        return "block"
      }else{
        return "none"
      }
      
    }

    createObj(ind,location_name,indice){
      //console.log(location)
      if(indice==0){
        return[{
            Header: `${location_name}`,
            columns: 
              [
                {
                Header: "FECHA",
                accessor: "x",
                width: 150
                },
                {
                Header: "oxd",
                accessor: "oxd"+ind,                                                              
                width: 80
                },
                {
                Header: "temp",
                accessor: "temp"+ind,                                                              
                width: 80
                },
                // {
                // Header: "sal",
                // accessor: "sal"+ind,                                                              
                // width: 80
                // },
                {
                  Header: "IdVerificacion",
                  accessor: "verificacion"+ind,                                                              
                  width: 200
                },
              ],
        },]
    }else if(location_name.includes('Sal')){
      return[{
          Header: `${location_name}`,
          columns: 
            [
              // {
              // Header: "FECHA",
              // accessor: "x",
              // width: 80
              // },
              // {
              // Header: "oxd",
              // accessor: "oxd"+ind,                                                              
              // width: 80
              // },
              // {
              // Header: "temp",
              // accessor: "temp"+ind,                                                              
              // width: 80
              // },
              {
              Header: "sal",
              accessor: "sal"+ind,                                                              
              width: 80
              },
              {
                Header: "IdVerificacion",
                accessor: "verificacion"+ind,                                                              
                width: 200
              },
            ],
      },]
    }else{
        return[{
            Header: `${location_name}`,
            columns: 
              [
                // {
                // Header: "FECHA",
                // accessor: "x",
                // width: 80
                // },
                {
                Header: "oxd",
                accessor: "oxd"+ind,                                                              
                width: 80
                },
                {
                Header: "temp",
                accessor: "temp"+ind,                                                              
                width: 80
                },
                // {
                // Header: "sal",
                // accessor: "sal"+ind,                                                              
                // width: 80
                // },
                {
                  Header: "IdVerificacion",
                  accessor: "verificacion"+ind,                                                              
                  width: 200
                },
              ],
        },]
      }
      
      
}

  

    getColumns(){
      // console.log(this.props.Divices)
      // let filtrar=d=>d.ind==1||d.ind==2||d.ind==5||d.ind==6
      // check1=true
      // check2=true
      // filtrar=function(d){
      //   if(check1){
      //     return d.ind==1||d.ind==2||d.ind==5||d.ind==6
      //   }
      // }
      let divice=this.props.Divices
      return divice.map((r,ind)=>{
        return this.createObj(r.disp.toString(),r.name,ind)[0]
      })
    }


    getHeader(){
      let divice=this.props.Divices
      let array=[];
      divice.map((r,ind)=>{
        if(ind==0){
          array.push({ label: "FECHA", key: "x" })
        }
        if(r.name.includes('Sal')){
          array.push({ label: `${r.name} Sal (PSU)`, key: "sal"+r.disp })
        }else{
          array.push({ label: `${r.name} O2 Disuelto (mg/L)` , key: "oxd"+r.disp })
          array.push({ label: `${r.name} Temp (°C)`, key: "temp"+r.disp })
        }
        // array.push({ label: `${locations[i].name}`, key: "NOMBRE" })
       
        // array.push({ label: `${r.name} O2 SAT ( %)`, key: "oxs"+ind })
        array.push({ label: `${r.name} VerificacionId`, key: "verificacion"+r.disp })
      })
      return array

      // if(this.state.TagsSelecionado!=null){
      //   // console.log("wena")
      //   let array=[];
      //   let loc=this.state.TagsSelecionado.locations.filter(l=>!l.name.includes('VoltajeR'));
      //   let count=loc.length;
      //   // console.log(count)
      //   // console.log(loc)
      //   let locations=loc;
      //   // console.log(locations)
      //   let i=0
      //   try{
      //     while(i<count){
      //       if(i==0){
      //         // array.push({ label: `${this.getNameDispositivo()}`, key: "NOMBRE" })
      //         array.push({ label: "FECHA", key: "x0" })
      //       }
      //       // array.push({ label: `${locations[i].name}`, key: "NOMBRE" })
      //       array.push({ label: `${locations[i].name} O2 Disuelto (mg/L)` , key: "oxd"+i })
      //       array.push({ label: `${locations[i].name} Temp (°C)`, key: "temp"+i })
      //       array.push({ label: `${locations[i].name} O2 SAT ( %)`, key: "oxs"+i })
      //       array.push({ label: `${locations[i].name} Sal (PSU)`, key: "sal"+i })
      //       i++
      //     }
      //   }catch(e){
      //     console.log("e")
      //   }
        
        
      //   return array  
      // }else{
      //   return []
      // }
    }
 
    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
      
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Configuración</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Datos Historicos SMA</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                <Row>
                                        <Col   md={12} lg={3}> 
                                            {this.renderPickerAutoApply(ranges, local, maxDate)}   
                                        </Col>
                                        <Col md={12}  lg={6}>
                                            {/* {this.renderSelecTag(this.props.Select)} */}
                                        </Col>
                                        
                                        <Col   md={12} lg={3}> 
                                            <ButtonGroup>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(31)
                                              })}>- 31 </Button>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(7)
                                              })}>- 7 </Button>
                                              <Button color="success" outline onClick={((e)=>{
                                              this.buttonChangeDate(1)
                                              })}>- 1 </Button>
                                            </ButtonGroup>
                                            <ButtonGroup style={{marginLeft:20}}>
                                                <Button color="primary"
                                                        outline
                                                        disabled={disabled}
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                          if(this.props.Divices != null){
                                                          disabled=true;
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                            
                                                             this.filtrar2();
                                                             }}
                                                          }
                                                >Filtrar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                              



                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                        <Card className="main-card mb-5 mt-3" >
                        <CardHeader className="card-header-tab  ">
                          <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                              {this.getNameDispositivo()}
                          </div>
                          <div className="btn-actions-pane-right text-capitalize" style={{display:disabled?"none":"block"}}>
                              <CSVLink                                         
                                separator={";"}                                                                   
                                headers={
                                  this.getHeader()                                      
                                }
                                data={this.state.Registros==null?[]:this.state.Registros}
                                filename={`Historico-Alarmas ${this.state.NameWorkplace.name}_${moment(this.state.start).format("DD-MM-YYYY HH:mm")}-${moment(this.state.end).format("DD-MM-YYYY HH:mm")}.csv`}
                                className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                target="_blank">
                                Exportar Datos
                              </CSVLink>
                          </div>                                                  
                        </CardHeader>
                        <CardBody className="p-10 m-10" >
                        <BlockUi tag="div" blocking={disabled} 
                                    loader={<Loader active type={this.state.loaderType}/>}>   
                          <ReactTable                                                           
                          data={this.state.Registros}
                      
                          // loading= {false}
                          showPagination= {true}
                          showPaginationTop= {false}
                          showPaginationBottom= {true}
                          showPageSizeOptions= {false}
                          pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                          defaultPageSize={10}
                          columns={this.getColumns()}
                          
                          className="-striped -highlight"
                          />
                        </BlockUi>
                      </CardBody>
                      </Card>

                        </Col>

                    </Row>
                    {/* <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                    <Spinner color="primary" style={{ marginTop:`${5}%`,marginLeft:`${50}%`,display:load?"block":"none", width: '2rem', height: '2rem'}}/>
                            <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block"}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%`}}
                                onClick={() => {
                                        load=true;
                                        this.filtrar2();
                                        // console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button>
                    </Col>
                    <Col sm="4"></Col>
                    
                    
                    {/* {console.time("loop 3")} */}
                    
                    {/* {
                                                          
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="3" key={data.sonda} style={{display:`${this.state.MultiExport?"none":"block"}`}}>
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        <CSVLink
                                                                        
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: data.unity , key: "y" }
                                                                                
                                                                            ]
                                                                        
                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                        </CSVLink>
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={data.measurements}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: data.unity,
                                                                    accessor: "y",                                                              
                                                                    width: 140
                                                                    }
                                                                    // {
                                                                    //   Header: "Temp ("+ data.unity2+ ")",
                                                                    //   accessor: "temp",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "O2 SAT ("+ data.unity3+ ")",
                                                                    //   accessor: "oxs",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "Sal ("+ data.unity4+ ")",
                                                                    //   accessor: "sal",                                                              
                                                                    //   width: 140
                                                                    // }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         
                         )
        
                            )
                            
                    }
                    
                                       

                  </Row> */}


                 
             
            
                                 

          

                    

               


                                                                 
                                                    
   

            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Datos_Historicos);
