import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter, InputGroupText, Label} from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT } from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import {
  setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../../reducers/Session';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';


sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let load=false;
let icon_up="lnr-arrow-up";
let icon_right="lnr-arrow-right";
let icon_down="lnr-arrow-down";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
//empresas=empresas.filter(e=>e.active==true).map(e=>e);
let ubicaciones=JSON.parse(sessionStorage.getItem("Areas"));
let c=0;
let areas_centros=[];
try{
  areas_centros=empresas.map(e=>{return e.areaId})
}catch(e){
  console.log("areas")
}

moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);


class Control extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        
        this.state = {       
          TagsSelecionado: null,
          Mis_Sondas:[],
          Mis_Sondas2:[],
          Modulos:[],
          isLoading:true,
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          card:[1,2,3,4,5,6,7,8],
          col:[1,2],
          row:[101,103,105,407,509,111,113,115,117],
          row2:[102,104,106,408,510,112,114,116],
          redirect:false,
          redirect2:false,
          Centros:[],
          modal1:false,
          Events:[],
          Centro_Actividad:[],
          txtCenActividad:"",
          Actividad:[],
          Centros2:[],
          Jaula:"",

        };     
        //this.getDataCentros = this.getDataCentros.bind(this);
        // this.intervalIdTag2 = this.getCentros.bind(this);
        this.tagservices = new TagServices();
        // this.applyCallback = this.applyCallback.bind(this);
        // this.handleChange =this.handleChange.bind(this);      
        // this.toggleModal1 = this.toggleModal1.bind(this);
        this.toggle = this.toggle.bind(this);
        
    }

  
    componentDidMount = () => {
      //this.getCentrosControl()
    }

      componentWillUnmount = () => {
     }  

      componentWillReceiveProps(nextProps){
        // if (nextProps.initialCount && nextProps.initialCount > this.state.count){
        //   this.setState({
        //     count : nextProps.initialCount
        //   });
        // }
        // //alert(JSON.stringify(nextProps));
        // let separador=nextProps.centroUsuario;
        // let centro=separador.split('/');
        // this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        // //this.getSondas(centro[0].toString());
        // if(c!=0)
        //   this.setState({redirect:true})
        // c++;
      }

      getCentrosControl(){
        this.tagservices.crudtagConfigControl("","","GET").then(respuesta=>{
          console.log("Controls :",respuesta)
        }).catch(
          //e=>console.log(e)
        )
      }

      toggle(e) {
        let event = e.target.dataset.event;
        this.setState({ collapse: this.state.collapse === Number(event) ? 0 : Number(event) });
      }

      // ingresarCentroUsuario = (centro) =>{
      //     let {setCentroUsuario} = this.props;
      //     setCentroUsuario(centro);
      // }

      createCollapse(r,i,ind){
        let n1=r;
        let n2=this.state.row2[i];
        try{
          if(n1!=null && n2!=null){
            return(
            <ListGroupItem style={{}} action data-event={ind}>
              <Row>
                <Col>
                  <Row>
                    <Col data-event={ind}>Jaula {n1}</Col>
                    <Col>val</Col>
                    <Col data-event={ind}><div style={{width:15,height:15}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Col>
                    <Col><Button color="link" onClick={()=>{this.switchModal("1",n1)}}>Config.</Button></Col>
                  </Row>
                </Col>
                <Col>
                  <Row>
                    <Col data-event={ind}>Jaula {n2}</Col>
                    <Col>val</Col>
                    <Col data-event={ind}><div style={{width:15,height:15}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Col>
                    <Col><Button color="link" onClick={()=>{this.switchModal("1",n2)}}>Config.</Button></Col>
                  </Row>
                </Col>
              </Row>
            </ListGroupItem>)
          }
          else if(n1!=null&&n2==null){
            return(
              <ListGroupItem style={{}} action data-event={100}>
                <Row>
                  <Col>
                    <Row>
                      <Col data-event={ind}>Jaula {n1}</Col>
                      <Col>val</Col>
                      <Col data-event={ind}><div style={{width:15,height:15}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Col>
                      <Col><Button color="link" onClick={()=>{this.switchModal("1",n1)}}>Config.</Button></Col>
                    </Row>
                  </Col>
                  <Col></Col>
                </Row>
              </ListGroupItem>)
          }
          else{
            return ""
          }
          
        }catch(e){
          console.log(e)
        }
        
      }

    handleChange = event => {
      const name = event.target.name;
      const value = event.target.value;
      this.setState({ [name]: value });
    }

    switchModal(n,Jaula){
      if(n=="1"){
          this.setState({modal1:true,Jaula})
      }
      else{
          this.setState({modal1:false})
      }
    }
      

    render() {
        
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

         ///***********************************+++ */
        return (
            <Fragment>
                <Modal style={{maxHeight:660,maxWidth:`500px`}} isOpen={this.state.modal1} size="xl">
                    <ModalHeader toggle={() => this.switchModal("-1")}><b>Configuración {this.state.Jaula}</b></ModalHeader>
                    <ModalBody>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>Histerisis</InputGroupText>
                          </InputGroupAddon>
                          <Input />
                        </InputGroup>
                        <br></br>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>SetPint</InputGroupText>
                          </InputGroupAddon>
                          <Input />
                        </InputGroup>
                        <br></br>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>Tiempo On</InputGroupText>
                          </InputGroupAddon>
                          <Input />
                        </InputGroup>
                        <br></br>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>Tiempo Off</InputGroupText>
                          </InputGroupAddon>
                          <Input />
                        </InputGroup>
                        <br></br>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>Alarma minima</InputGroupText>
                          </InputGroupAddon>
                          <Input />
                        </InputGroup>
                        <br></br>
                        <FormGroup check>
                          <Label check>
                            <Input type="checkbox" />{' '}
                            Activar/desactivar Sensor
                          </Label>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                          <Button color="secondary" onClick={((e)=>{
                                this.switchModal("-1")
                              })} >Cancel</Button>
                          <Button color="primary" >Guardar Cambios</Button>
                    </ModalFooter>
                </Modal>
                <Row>
                  {this.state.card.map((c,ind)=>{
                    return(
                    <Col md="4">
                      <Card className="main-card mb-5 mt-3" id={"card"+ind}>
                        <CardHeader className="card-header-tab  ">
                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                        {/* {this.createTooltip(`detalle${c._id}`,"Mas Detalles de Centros")} */}
                              <a style={{cursor:"pointer"}} onClick={(()=>{
                                // sessionStorage.setItem('workplace', JSON.stringify({"id":c._id,"name":c.name,"active":c.active}));
                                // this.ingresarCentroUsuario(`${c._id}/${c.name}`);
                                // this.setState({redirect:true})
                              })} id={`detalle`}>
                              <div class="icon-wrapper rounded-circle" >
                                  <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                  <i class="lnr-apartment text-white"></i>
                              </div>
                              </a>
                              <div class="widget-chart-content font-size-lg font-weight-normal" style={{marginTop:30}}>
                              <p>nombre
                              {/* {this.createTooltip(`event_`,"Ver Eventos")} */}
                              <Button id={`event_`} color="link" style={{marginLeft:`${90}%`,marginTop:-50}} pill onClick={((e)=>{
                                // this.filterEvent(c._id)
                                // this.toggleModal1(true,c._id,c.name)
                              })}> <i style={{fontSize:20,cursor:"pointer",color:"orange"}} className="pe-7s-bell" ></i></Button>
                              </p>
                              </div>
                        </div>

                        <div className="btn-actions-pane-right text-capitalize">
                              fecha
                            {/* {!this.state.Equipos?this.endDate(c.code):this.endDate2(c.code)} */}
                        </div>                           
                        </CardHeader>
                        <ListGroup style={{cursor:"pointer"}}>
                          <div >
                            <ListGroupItem style={{borderWidth:3,display:"block"}}  action onClick={this.toggle} data-event={100+ind}>
                                <Row>
                                {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
                                  <Col onClick={this.toggle} data-event={100+ind}><i id={`oxd_divice`} style={{color:"black",size:20}} class="pe-7s-help1"></i> Modulo  Ox.</Col>
                                  <Col onClick={this.toggle} data-event={100+ind}>prom</Col>
                                  <Col onClick={this.toggle} data-event={100+ind}>min</Col>
                                  <Col onClick={this.toggle} data-event={100+ind}>max</Col>
                                  <Col onClick={this.toggle} data-event={100+ind}><div style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Col>
                                </Row>
                            </ListGroupItem>
                            <Collapse isOpen={collapse === 100+ind}>
                                {
                                  this.state.row.map((r,i)=>{
                                    return(
                                    this.createCollapse(r,i,100+ind)
                                  )
                                  })
                                }
                                {/* {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],100,l.ultimo_valor,l.id)} */}
                            </Collapse>
                          </div>
                        </ListGroup>
                      </Card>
                    </Col>)
                  })}
                </Row>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({
  setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Control);
