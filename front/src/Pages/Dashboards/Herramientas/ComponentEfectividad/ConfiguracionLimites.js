import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,UncontrolledCollapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter, CardText,InputGroupText
  , CustomInput } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {
    toast

} from 'react-toastify';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import '../../../../assets/icon/style.css';
import Swal from 'sweetalert2'
import Label from 'reactstrap/lib/Label';
import { parseInt } from 'lodash';
import Jumbotron from 'reactstrap/lib/Jumbotron';
import CardTitle from 'reactstrap/lib/CardTitle';
import Toast from 'reactstrap/lib/Toast';

sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
try{
    empresas=empresas.filter(e=>e.active==true).map(e=>e);
}catch(e){
    console.log("empresa")
}

let c=0;


let title_centro="";
let code_centro="";
let Buscar="";
let active_todos=true;
let check=true;
let check2=false;
let active_config="none";
let u_ids=[];
let modal1=false;
let filtro_id=""
let name_Workplace="";
let viewExport=false;
class ConfiguracionLimites extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Modulos:[],
          isLoading:false,
          Mis_Prom:[],
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          redirect:false,
          redirect2:false,
          Actividad:[],
          Actividad2:[],
          Centro_Actividad:[],
          Centro_ActividadU:[],
          active_grafic:"none",
          ModalState:false,
          TypeGraphic:"1",
          TypeGraphic2:"2",
          Centros:[],
          Activity:true,
          Act_cen:false,
          Centro:false,
          modal1:false,
          txtActividad:"",
          txtActividad2:"",
          txtActividad3:"",
          txtCenActividad:"",
          Centros_Category:[],
          Areas:[],
          txtCentro:"",
          automatico:false,
          cantidadZonas:[],
          cantidadDispositivos:[],
          DataDetail:[],
        };     
        this.tagservices = new TagServices();        
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        let rol=sessionStorage.getItem("rol");
        console.log(rol)
        if(rol=="5f6e1ac01a080102d0e268c2"){
          active_config="block";
        }
        this.setState({isLoading: false,Centros:empresas})
        //this.setState({isLoading: false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        //this.setState({ isLoading: true });
        let separador=this.props.Centros!=null?this.props.Centros:this.props.centroUsuario;
        console.log("sep "+separador)
        let centro=separador.split('/');
        //console.log(this.props.Mis_Prom)
        this.setState({NameWorkplace:{id:centro[0],name:centro[1]}});
        // if(workplace!=null){
        //     //alert(workplace.id)
        //     this.setState({NameWorkplace:{id:workplace.id,name:centro[1]}});
        //     //this.getSondas(workplace.id);
        // }
        //this.getCentro_Actividad();
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
        
     } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        if(this.props.Centros==null){
            let separador=nextProps.centroUsuario;
            let centro=separador.split('/');
            this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        }
      }

      onChangeInput= event => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
      }

      getAcordion(modul){
          return this.props.Mis_Prom.filter(m=>m.name.includes(modul)).map((m,index)=>{
            return<Fragment><Row style={{marginTop:5}}>
                {/* {console.log(m)} */}
                <Col md={4}><font size={3}>{m.name}</font></Col>
                <Col md={4}>
                    <FormGroup>
                        <Label>Limite mínimo</Label><Input name={"min_"+m.id} onChange={this.onChangeInput} type="number"
                         style={{width:`${65}%`}} placeholder={m.min}></Input>
                    </FormGroup>
                </Col>
                <Col md={4}>
                    <FormGroup>
                        <Label>Limite máximo</Label><Input name={"max_"+m.id} onChange={this.onChangeInput} type="number"
                         style={{width:`${65}%`}} placeholder={m.max}></Input>
                    </FormGroup>
                </Col>
            </Row>
            <hr></hr>
            </Fragment>
          })
      }

      updateTags(){
          this.props.Mis_Prom.map(m=>{
              let data={};
              if(this.state["min_"+m.id]!=undefined && this.state["max_"+m.id]!=undefined){
                data={alertMin:this.state["min_"+m.id],alertMax:this.state["max_"+m.id]}
                m.min=this.state["min_"+m.id]
                m.max=this.state["max_"+m.id]
                this.updateOneTag(m.id,data,m)
              }
              if(this.state["max_"+m.id]!=undefined){
                data={alertMax:this.state["max_"+m.id]}
                m.max=this.state["max_"+m.id]
                this.updateOneTag(m.id,data,m)
              }
              if(this.state["min_"+m.id]!=undefined){
                data={alertMin:this.state["min_"+m.id]}
                m.min=this.state["min_"+m.id]
                this.updateOneTag(m.id,data,m)
              }
             // console.log(data)
          })
      }

      updateOneTag(id,data,m){
        this.tagservices.updateTag(id,data).then(response=>{
            if(response.data.statusCode==201 || response.data.statusCode==200){
                  toast['success']('El dispositivo '+m.name+' ya fue actualizado', { autoClose: 4000 })
                this.setState({Mis_Prom:this.state.Mis_Prom.filter(m2=>m2.id!=id).concat(m)})
            }
        })
      }
      
      saveLimitGlobal(){
        this.props.Mis_Prom.map(m=>{
            let data={}
            if(this.state["minGlobal"]!=undefined && this.state["maxGlobal"]!=undefined){
                data={alertMin:this.state["minGlobal"],alertMax:this.state["maxGlobal"]}
                m.min=this.state["minGlobal"]
                m.max=this.state["maxGlobal"]
                this.updateOneTag(m.id,data,m)
            }
            if(this.state["maxGlobal"]!=undefined){
            data={alertMax:this.state["maxGlobal"]}
            m.max=this.state["maxGlobal"]
            this.updateOneTag(m.id,data,m)
            }
            if(this.state["minGlobal"]!=undefined){
            data={alertMin:this.state["minGlobal"]}
            m.min=this.state["minGlobal"]
            this.updateOneTag(m.id,data,m)
            }
        })
      }
    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        if(this.state.redirect2){
            return <Redirect to="/dashboards/resumen"></Redirect>
          }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        
        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        {/* {console.log(this.state.Modulos)} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Configuración</BreadcrumbItem>
                      <BreadcrumbItem tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      <BreadcrumbItem active tag="span" style={{display:this.state.Centros!=null?"none":active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-back"> </i> Volver</Badge></a></BreadcrumbItem>
                      
                      </Breadcrumb>
                        
                     <Row>
                        <Col md="6">
                            <Card>
                                <CardBody>
                                    <Row>
                                        <Col md={3}>
                                            <CardTitle>Asignar Limites a Centro</CardTitle>
                                        </Col>
                                        <Col md={3}>
                                            <Label>Limite mínimo</Label><Input name={"minGlobal"} onChange={this.onChangeInput} type="number" style={{width:`${55}%`}} placeholder=""></Input>
                                        </Col>
                                        <Col md={3}>
                                            <Label>Limite máximo</Label><Input name={"maxGlobal"} onChange={this.onChangeInput} type="number" style={{width:`${55}%`}} placeholder=""></Input>
                                        </Col>
                                        <Col md={3}>
                                            <Button style={{width:`${100}%`,marginTop:`${20}%`}} color={"primary"} onClick={(()=>{
                                                this.saveLimitGlobal()
                                            })} outline>
                                                Asignar
                                            </Button>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col md="12" style={{marginTop:3}}>
                            <Card className="main-card mb-1 p-0">
                                <CardBody>
                                    <Row>
                                        {console.log(this.props.Modulos)}
                                            {
                                                this.props.Modulos.map((m)=>{
                                                //console.log(m)
                                                let zonas = m.zonas;
                                                return zonas.map((z)=>{
                                                    let modul=z.replace("Modulo ","")
                                                    //console.log(modul)
                                                    return<Col md={4}><Card>
                                                        <CardHeader>
                                                            <Col md={11}>
                                                                {modul}
                                                            </Col>
                                                            <Col md={1}>
                                                                <Button style={{marginLeft:-10}} color={"primary"} onClick={(()=>{
                                                                    this.updateTags()
                                                                })} outline>
                                                                    <span style={{fontSize:20}} className="pe-7s-pen"></span>
                                                                </Button>
                                                            </Col>
                                                        </CardHeader>
                                                        <CardBody>
                                                            {this.getAcordion(modul)}
                                                        </CardBody>
                                                    </Card></Col>
                                                })
                                                })
                                            }
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ConfiguracionLimites);
