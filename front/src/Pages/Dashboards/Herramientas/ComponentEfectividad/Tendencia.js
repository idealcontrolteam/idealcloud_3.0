import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../../src/assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../../src/services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import Swal from 'sweetalert2'

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let empresa=sessionStorage.getItem("Empresa");
let disabled=false;
let load=false;
let i=0;
let active_config="none";
let chartcolor= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
let start_date=moment().startOf('month').format('YYYY-MM-DDT00:00:00');
console.log(moment().endOf('month').format('YYYY-MM-DDT00:00:00'))
let end_date=moment().format('YYYY-MM-DDT23:59:ss');
let tendencia="mes";
let f1=new Date(start_date);
let f2=new Date();
var diff = f2 - f1;
let n_dias = Math.floor(diff / (1000 * 60 * 60 * 24))+1;
let n_dias2=1;
let view_fecha="";
let prom_efectividad=0;

moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);

class Tendencia_Efectividad extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          Export1:[],
          Export2:[],
          Export3:[],
          Export4:[],
          Export5:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: true,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          MultiExport:false,
          solo_Oxd:true,
          setPoint:true,
          histerisis:true,
          inyecciones:true,
          Efectividades:[],
          btn1:false,
          btn2:false,
          btn3:false,
          btn4:false,
          // redirect:false,
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);       
        this.filtrar =this.filtrar.bind(this);
        this.filtrar2 =this.filtrar2.bind(this);
        this.loadDataChart =this.loadDataChart.bind(this);
        
        
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        sessionCheck()
        //console.log(this.props.Centro);
        console.log(this.props.Centro)
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        view_fecha="Mes de "+moment(start_date).format('LL');
        this.switchButton(3);
        this.getEfectividadCenterMonth(start_date,end_date);
        //let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        // if(workplace!=null){
        //     //alert(workplace.id)
        //     this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
        //     //this.getSondas(workplace.id);
        // }
      }

      componentWillUnmount = () => {
        //clearInterval(this.getSondas);
        clearInterval(this.loadDataChart);
        clearInterval(this.loadDataChart2);
        clearInterval(this.filtrar);
        clearInterval(this.filtrar2);
     }  

    switchButton(menu){
      let c=1;
      while(c<=4){
        if("btn"+c=="btn"+menu){
          this.setState({
            ["btn"+c]:true
          })
        }else{
          this.setState({
            ["btn"+c]:false
          })
        }
        c++;
      }
    }
    getEfectividadCenterMonth(start_date,end_date){
        prom_efectividad=0;
        dataCha=[];
          this.setState({
            start:moment(start_date),
            end:moment(end_date),
            dataAxisy:[],
            dataCharts:[],
            dataChaAxisy:[],
            blocking: true,
        }); 
        console.log(end_date)
        console.log(start_date)
        let f1=new Date(start_date);
        let f2=new Date(end_date);
        var diff = f2 - f1;
        console.log(diff)

        let diferenciaDias = Math.floor(diff / (1000 * 60 * 60 * 24))+1;
        console.log(diferenciaDias)
        if(this.props.Centro=="General" && tendencia!="mes" && tendencia!="semana" && tendencia!="mes_anterior"){
          this.tagservices.getWorkplaceEfectividadMonth(this.props.Centro,start_date,end_date).then(response=>{
            if(response.statusCode==200 || response.statusCode==201){
              console.log(response)
              this.loadDataChart(response.data,diferenciaDias,f2);
            }
          }).catch(e=>{
            //console.log(e)
          })
        }
        else{
          let workplaceIds=[];
          if(this.props.Centro=="General"){
            workplaceIds=this.props.All_Centros.map(a=>{
              return {workplaceId:a._id}
            })
          }else{
            workplaceIds.push({workplaceId:this.props.Centro});
          }
         
          this.tagservices.getWorkplaceEfectividadDay(workplaceIds,start_date,end_date).then(response=>{
            if(response.statusCode==200 || response.statusCode==201){
              this.loadDataChart(response.data,diferenciaDias,f2);
            }
          }).catch(e=>{
            //console.log(e)
          })
        }
    }

    getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }

    getMydatachartInyeccion=(data,name,i,color)=>{
      // data.map((d,i)=>{
      //   let numero=i%2==0?0:1
      //   return ({y:numero,x:d.x})
      // })
      let mydatachart = {
           axisYIndex: i,
           type: "Line",
           legendText: name,
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }
    getMydatachartHisterisis=(data,name,i,color)=>{
      let mydatachart = {
           axisYIndex: i,
           type: "line",
           legendText: name,
           lineDashType: "dash",
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }
     getMydatachart=(data,name,i,color)=>{
       let mydatachart = {
            axisYIndex: i,
            type: "line",
            legendText: name,
            name: name,
            color:color,
            dataPoints : data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,  
            lineThickness: 3
             }
             i++;
           dataCha.push(mydatachart);
         return
     }
     getMyaxis=(color,unity)=>{
        let axisy= {    
            id:unity,
            title: "%",
            labelFontSize: 11,                   
            // lineColor: color,
            // tickColor: color,
            // labelFontColor: color,
            // titleFontColor: color,
            // suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }
     getMydatachart2=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "line",
             axisYType: "secondary",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return
      }
      getMyaxis2=(color,unity)=>{
        let axisy= {
            id:unity,
            labelFontSize: 11,                   
            lineColor: color,
            tickColor: color,
            labelFontColor: color,
            titleFontColor: color,
            suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

     convert2_decimals(num){
        return Math.round(num * 100) / 100
     }

     onlyEfectivity(efectividad,dif_dias,dif_inicio,f2,reg_100){
       let only=[];
       let c=dif_inicio;
       //console.log(c)
       let c_="";
       //console.log(dif_dias)
       let end_date=moment(f2).format(`YYYY-MM-DDT00:00:00`);
       //console.log(end_date)
       //console.log(diff)
       console.log(dif_dias)
       while(c<=dif_dias){
        //console.log(end_date)
        c_=c<=9?"0"+String(c):String(c);
        //let end_date=moment().format(`YYYY-MM-${c_}T00:00:00`);
        let count=0;
        console.log(reg_100)
        //console.log(this.props.All_Centros)
        let largo_efect=0;
        let new_efect=[];
        console.log(efectividad)
        let prom=this.props.All_Centros;
        let p=((prom.reduce((a, b) => +a + +(b.porcentaje), 0)/prom.length)).toFixed(1)
        console.log(p)
        let efect=efectividad.filter(e=>e.start_date==end_date+".000Z").map(e=>{
          let existe=this.props.All_Centros.filter(a=>e.workplaceId==a._id)
          // let centro=this.props.Mis_Prom.filter(p=>p.centroId==e.workplaceId);
          // let n_centros=centro.length;
          //console.log(centro.length)
          new_efect.push(e)
          largo_efect++;
          if(existe.length>0){
            
            if(this.props.Centro=="General" && tendencia!="mes" && tendencia!="semana" && tendencia!="mes_anterior"){
              count=count+e.porcentaje;
            }else{
              if(this.props.Centro=="General"){
                e.start_date==moment().format("YYYY-MM-DDT00:00:00")+".000Z"?
                count=this.convert2_decimals((e.count*100/reg_100)+count)
                //count=Math.round(((p*288/100)*100/288)+count)
                :count=count+e.porcentaje;
              }else{
                e.start_date==moment().format("YYYY-MM-DDT00:00:00")+".000Z"?
                count=this.convert2_decimals((e.count*100/reg_100)+count)
                :count=(e.count*100/288)+count;
              }
            }
          }
        })
        console.log(new_efect)
        //console.log(`fecha : ${end_date} - registros : ${count} - cantidad : ${efect.length}`)
        // console.log(`count ${count}`)
        // console.log(`locations ${locations}`)
        // let total_mes=((locations*288)*30);
        
        // let porcentaje=count!=0?(count*100/total_mes):0;
        // console.log(`porcentaje ${porcentaje}`)
        console.log(count)
        console.log(count/new_efect.length)
        //console.log(((new_efect.reduce((a, b) => +a + +b.porcentaje, 0)/new_efect.length)).toFixed(1))
        if(this.props.Centro=="General" && tendencia!="mes" && tendencia!="semana" && tendencia!="mes_anterior"){
          console.log(largo_efect)
          let porcentaje=count==0?0:count/largo_efect;
          console.log(porcentaje) 
          only.push({"fecha":moment(end_date).format('L'),"registros":count,"porcentaje":this.convert2_decimals(porcentaje)})
        }else{
          //let porcentaje=((new_efect.reduce((a, b) => +a + +b.porcentaje, 0)/new_efect.length)).toFixed(1)
          let porcentaje=count>0?count/new_efect.length:0
          only.push({"fecha":moment(end_date).format('L'),"registros":count,"porcentaje":this.convert2_decimals(porcentaje)})
        }
        end_date=moment(f2).subtract(c, "days").format(`YYYY-MM-DDT00:00:00`);
        c++;
       }
       console.log(only)
       if(this.props.Centro=="General" && tendencia!="mes" && tendencia!="semana" && tendencia!="mes_anterior"){
        let only_filter=only
        //.filter(o=>o.porcentaje!=0)
        prom_efectividad=((only_filter.reduce((a, b) => +a + +b.porcentaje, 0)/only_filter.length)).toFixed(1);
       }else{
        prom_efectividad=((only.reduce((a, b) => +a + +b.porcentaje, 0)/only.length)).toFixed(1);
       }
       //console.log(only)
       return only
     }

     loadDataChart = (efectividad,diferenciaDias,f2) => {   
      console.log(efectividad)
      let dif_dias=diferenciaDias;
      let dif_inicio=n_dias2;
      var fecha1 = moment().format('YYYY-MM-DDT00:00:00');
      fecha1=new Date(fecha1)
      let fecha2=new Date()
      let reg_100=((fecha2-fecha1)/60000)/5;

      //console.log(fecha2.diff(fecha1, 'days'), ' dias de diferencia');
      let only=this.onlyEfectivity(efectividad,dif_dias,dif_inicio,f2,reg_100);
      //console.log(format_efectividad)
      let mediciones=only.map(e=>{return {y:e.porcentaje,x:new Date(e.fecha)}})
      this.getMydatachart(mediciones,"Efectividad",3,chartcolor[0]);
      this.getMyaxis(chartcolor[0]," ");
      i++;

      this.setState({
        dataAxisy:dataChaAxisy,
        dataCharts:dataCha,
        blocking:false,
      });           
 
     }
    

    filtrar =() => {
      this.switchButton(0);
      let f1="";
      let f2="";
      f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss");
      //const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
      f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss");
      this.getEfectividadCenterMonth(f1,f2);
    }

    //carga los datos para la Exportación de la tendencia
    loadDataChart2 = (shortName, URL) => {   
         
        // this.setState({blocking: true});
        // console.log(URL)
         const token = "tokenFalso";
               
         axios
         .get(URL, {
          headers: {  
            'Authorization': 'Bearer ' + token
           }
         })
        .then(response => { 
          if (response.data.statusCode === 200) {
            const dataChart = response.data.data;
          //   console.log(dataChart);
           //  this.setState({gridDataChart:dataChart});  
            
          // console.log(dataChart); 
          //  console.time('loop2');
            let _dataCharts = dataChart.map( item => { 
               return { x: moment(item.dateTime.substr(0,19)) , y : item.value }; 
             });

              //console.log(dataChart);
             
             let _dataChartsExport = dataChart.map( item => { 
               return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , y : item.value.toString().replace(".",",") }; 
             });
          load=false;
          let mydatachartExport = {}
          try{
              if(shortName.includes("oxd")){
                mydatachartExport = {  
                  unity : "mg/L",              
                  sonda: shortName.toString(),
                  measurements : _dataChartsExport
                  }      
                this.setState({Export1:_dataChartsExport})
              }
              if(this.state.solo_Oxd){
                if(shortName.includes("oxs")){
                  mydatachartExport = {  
                    unity : "%",              
                    sonda: shortName.toString(),
                    measurements : _dataChartsExport
                    }
                    this.setState({Export2:_dataChartsExport})
                }
                if(shortName.includes("temp")){
                  mydatachartExport = {  
                    unity : "°C",              
                    sonda: shortName.toString(),
                    measurements : _dataChartsExport
                    }
                    this.setState({Export3:_dataChartsExport})
                }
                if(shortName.includes("sal")){
                  mydatachartExport = {  
                    unity : "PSU",              
                    sonda: shortName.toString(),
                    measurements : _dataChartsExport
                    }
                    this.setState({Export4:_dataChartsExport})
                }
                if(!this.state.setPoint){
                  if(shortName.includes("Set_Point")){
                    mydatachartExport = {  
                      unity : "mg/L",              
                      sonda: shortName.toString(),
                      measurements : _dataChartsExport
                      }
                      this.setState({Export5:_dataChartsExport})
                  }
                }
                
              }
              
                //dataChaAxisy.push(axisy);  
                 dataChaExport.push(mydatachartExport);   
                //dataCha.push(mydatachart);  
           
              this.setState({
                dataExport:dataChaExport,
             });
          }catch(e){
            console.log("object")
          }
          
           
          //  console.log("dataExport: ",dataChaExport);

           //                 dataExport:dataChaExport,
           //   this.setState({dataExport:dataChaExport});  
           //   this.setState({dataCharts:dataCha});  

               console.timeEnd('loop2');
               //this.filtrar2();
          }
          else{
            load=false;
          }
        })
        .catch(error => {
          // console.log(error);
        });
  
      }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =() => {
        
        this.setState({buttonExport:'none'});
         const TagsSelecionado = this.state.TagsSelecionado;
 
         
         if (TagsSelecionado !== null){
          if(this.state.TagsSelecionado.name.toLowerCase().includes(' salinidad')){
            this.setState({MultiExport:false})
          }else{
            this.setState({MultiExport:true})
          }

            //this.filtrar();
             
             const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             let f1_split=f1.split("T");
             let f2_split=f2.split("T");
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;

             const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            
            //  console.time('loop');
            let tags=TagsSelecionado.tags;
            // console.log(TagsSelecionado.tags)
             for (let i = 0; i < tags.length; i++) {
               //this.setState({blocking: true});      
               //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/${tags[i]._id}/${f1_split[0]}T00:00/${f2_split[0]}T23:59`;
               let APItagMediciones = `${API_ROOT}/measurement/tag/${tags[i]._id}/${f1}/${f2}`;
              if(this.state.NameWorkplace.name.includes(" Control")){
                 APItagMediciones = `${API_ROOT}/measurement-control/tag/${tags[i]._id}/${f1}/${f2}`;
              }

              // console.log(APItagMediciones);
     
               this.loadDataChart2(tags[i].shortName,APItagMediciones);
             }
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
        this.setState({ TagsSelecionado });
    }
    
    renderSelecTag = (Tags) =>{ 
      let data=Tags;
      if(empresa=="multiexport"){
        data=_.orderBy(Tags,["name"],["asc"]);
      }else{
        data=_.orderBy(Tags,["_id"],["asc"]);
      }
        return (
          <div>
               <FormGroup>
                    <Select                
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        // isMulti
                        name="colors"
                        options={data}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Sensor"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
        let value1 = this.state.start.format("DD-MM-YYYY") 
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
      }

    buttonChangeDate=(tipo)=>{
      var now=new Date();
      var current = new Date();
      var hoy=moment(now).format('YYYY-MM-DDTHH:mm:ss');
      // var mes_anterior=moment().format('DD')
      // console.log(mes_anterior)
      //Controla cual sera el primer dia
      if(tipo=="semana"){
        if(current.getDay() == 0){
          current.setDate(((current.getDate() - 7)+1));
        }else{
            current.setDate(((current.getDate() - current.getDay()) + 1));
        }
        //console.log(moment(current).format('DD'))
        //console.log(current)
        view_fecha="Semana del "+moment(new Date(current)).format('DD dddd, YYYY');
        n_dias2=1
        current = moment(current).format('YYYY-MM-DDT00:00:00');
        //console.log(n_dias2)
      }
      if(tipo=="mes"){
        current=start_date;
        hoy=end_date
        //n_dias2=0;
        current = moment(current).format('YYYY-MM-01T00:00:00');
        //console.log(moment(current).format('LL'))
        view_fecha="Mes de "+moment(current).format('LL');
      }
      if(tipo=="mes_anterior"){
        current=moment(end_date).subtract(1, "months").format('YYYY-MM-01T00:00:00');
        hoy=moment(current).endOf('month').format('YYYY-MM-DDT00:00:00');
        //console.log(hoy)
        view_fecha="Mes de "+moment(current).format('LL');
      }
      if(tipo=="año"){
        current=moment(end_date).format('YYYY-01-01T00:00:00');
        view_fecha="Año "+moment(current).format('YYYY');
      }
      tendencia=tipo;
      this.getEfectividadCenterMonth(current,hoy)
      // if(this.state.TagsSelecionado!=null){
      //   let now = new Date();
      //   let hoy = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
      //   let new_fecha=moment(hoy).subtract(n, "days")
      //   // this.setState({
      //   //   start:new_fecha
      //   // })
      //   if(this.state.TagsSelecionado != null){
      //     disabled=true;
      //       this.setState({
      //           dataExport:[],
      //           buttonExport:'block'
      //       });
      //       this.filtrar(new_fecha);
      //   }
      // }
    }

    getNameDispositivo(){
      if(this.state.TagsSelecionado!=null){
        return this.state.TagsSelecionado.name
      }else{
        return ""
      }
    }

    getArray(){
      try{
        let oxd=this.state.Export1;
        let oxs=this.state.Export2;
        let temp=this.state.Export3;
        let sal=this.state.Export4;
        
        if(this.state.TagsSelecionado!=null){
          return this.state.Export4.map((d,i)=>{
            if(this.state.TagsSelecionado.name.toLowerCase().includes('salinidad')){
              return {x:d.x, y3:d.y}
            }
            else if(oxd!=null&&oxs!=null&&temp!=null&&sal!=null){
              return {
                      x:oxd[i].x,
                      y:oxd[i].y,
                      y1:oxs[i].y,
                      y2:temp[i].y,
                      y3:sal[i].y
                    }
            }else if(oxd==null){
              return {
                x:oxs[i].x,
                y1:oxs[i].y,
                y2:temp[i].y,
                y3:sal[i].y
              }
            }else if(oxs==null){
              return {
                y:oxd[i].y,
                y2:temp[i].y,
                y3:sal[i].y
              }
            }else if(temp==null){
              return {
                y:oxd[i].y,
                y1:oxs[i].y,
                y3:sal[i].y
              }
            }
            else if(sal==null){
              return {
                y:oxd[i].y,
                y1:oxs[i].y,
                y2:temp[i].y,
              }
            }
            
          })
          
        }else{
          return []
        }
      }catch(e){
        console.log(e)
        return []
      }
      
    }

    showTableExport(){
      if(this.state.MultiExport && this.state.buttonExport=="none"&&load==false){
        return "block"
      }else{
        return "none"
      }
    }

    switchFilter(name){
      this.setState({[name]:!this.state[name]})
    }

    generateExport(){
      if(!this.state.solo_Oxd){
        return<CSVLink                                         
          separator={";"}                                                                   
          headers={
            [
              { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
              { label: "FECHA", key: "x" },
              { label: "O2 Disuelto (mg/L)" , key: "y" }
              // { label: "Temp (°C)", key: "y2" },
              // { label: "O2 SAT ( %)", key: "y1" },
              // { label: "Sal (PSU)", key: "y3" }
            ]                                         
          }
          data={_.orderBy(this.getArray(),["x"],["desc"])}
          filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
          className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
          target="_blank">
          Exportar Datos
        </CSVLink>
      }else{
        return<CSVLink                                         
          separator={";"}                                                                   
          headers={
            [
              { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
              { label: "FECHA", key: "x" },
              { label: "O2 Disuelto (mg/L)" , key: "y" },
              { label: "Temp (°C)", key: "y2" },
              { label: "O2 SAT ( %)", key: "y1" },
              { label: "Sal (PSU)", key: "y3" }
            ]                                         
          }
          data={_.orderBy(this.getArray(),["x"],["desc"])}
          filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
          className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
          target="_blank">
          Exportar Datos
        </CSVLink>
      }
      
    }

    generateTable(){
      let columna=[
          {
          Header: "FECHA",
          accessor: "x"
          },
          {
            Header: "oxd",
            accessor: "y",                                                              
            width: 140
          }
      ]  
      if(this.state.solo_Oxd){
        columna.push(
          {
          Header: "oxs",
          accessor: "y1",                                                              
          width: 140
          },
          {
          Header: "temp",
          accessor: "y2",                                                              
          width: 140
          },
          {
          Header: "sal",
          accessor: "y3",                                                              
          width: 140
          })
      }
      return <ReactTable                                                           
            data={_.orderBy(this.getArray(),["x"],["desc"])}
        
            // loading= {false}
            showPagination= {true}
            showPaginationTop= {false}
            showPaginationBottom= {true}
            showPageSizeOptions= {false}
            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
            defaultPageSize={10}
            columns={columna}
            
            className="-striped -highlight"
            />
    }
    
    showFilter(name){
      try{
        if(name.includes(' Control')){
          return "block"
        }else{
          return "none"
        }
      }catch(e){

      }
      
    }

    getProm(){
      console.log(this.props.All_Centros)
      return prom_efectividad>99?100:prom_efectividad
    }
    render() {
        // if(this.state.redirect){
        //   return <Redirect to="/dashboards/tendencia_online"></Redirect>
        // }
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
         
 
         let optionsChart1 = {

            
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DD-MMM-YYYY");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                {title:"% Efectividad"},
             axisY2 :{
                title:"% - PSU"
            },   
            
             }
         
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Tendencias</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Tendencia Historica</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">    
                                <Row style={{display:`none`}}>
                                  <Col >
                                    <Button color={"primary"} outline={this.state.solo_Oxd} onClick={(()=>this.switchFilter("solo_Oxd"))}>Mg/L</Button>
                                    <Button color={"primary"} outline={this.state.setPoint} onClick={(()=>this.switchFilter("setPoint"))}>setPoint</Button>
                                    <Button color={"primary"} outline={this.state.histerisis} onClick={(()=>this.switchFilter("histerisis"))}>histerisis</Button>
                                    <Button color={"primary"} outline={this.state.inyecciones} onClick={(()=>this.switchFilter("inyecciones"))}>inyecciones</Button>
                                  </Col>
                                </Row>        
                                <br></br>                     
                                <Row>
                                        <Col   md={12} lg={3}> 
                                          <h3 color="black">{view_fecha}</h3>
                                            {/* {this.renderPickerAutoApply(ranges, local, maxDate)}    */}
                                        </Col>
                                        <Col md={12}  lg={6}>
                                          <div class="widget-content p-0" style={{maxWidth:`${90}%`}}>
                                              <div class="widget-content-outer">
                                                  <div class="widget-content-wrapper">
                                                      <div class="widget-content-left">
                                                          <div class="widget-numbers text-dark"><font ><font>{this.getProm()}%</font></font></div>
                                                      </div>
                                                  </div>
                                                  <div class="widget-progress-wrapper mt-1">
                                                      <div class="progress-bar-xs progress-bar-animated-alt progress">
                                                          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="22" aria-valuemin="0" aria-valuemax="100" style={{width: `${prom_efectividad}%`}}>
                                                          </div>
                                                      </div>
                                                      <div class="progress-sub-label">
                                                          <div class="sub-label-left font-size-md"><font ><font >Promedio Efectividad %</font></font></div>
                                                      </div>
                                                  </div>
                                              </div>
                                            </div>
                                            {/* {this.renderSelecTag(this.state.Tags)} */}
                                        </Col>
                                        
                                        <Col   md={12} lg={3}> 
                                            <ButtonGroup>
                                              <Button color="success" outline active={this.state.btn1} onClick={((e)=>{
                                              this.switchButton(1);
                                              this.buttonChangeDate("año")
                                              })}>Año Actual</Button>
                                              <Button color="success" outline active={this.state.btn2} onClick={((e)=>{
                                              this.switchButton(2);
                                              this.buttonChangeDate("mes_anterior")
                                              })}>Mes Anterior</Button>
                                              <Button color="success" outline active={this.state.btn3} onClick={((e)=>{
                                              this.switchButton(3);
                                              this.buttonChangeDate("mes")
                                              })}>Mes Actual</Button>
                                              <Button color="success" outline active={this.state.btn4} onClick={((e)=>{
                                              this.switchButton(4);
                                              this.buttonChangeDate("semana")
                                              })}>Semana Actual</Button>
                                            </ButtonGroup>
                                            {/* <ButtonGroup style={{marginLeft:20}}>
                                                <Button color="primary"
                                                        outline
                                                        disabled={disabled}
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                          if(this.state.TagsSelecionado != null){
                                                          disabled=true;
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                            
                                                             this.filtrar();
                                                             }}
                                                          }
                                                >Filtrar
                                                </Button>
                                            </ButtonGroup> */}
                                        </Col>          
                                        <Col   md={12} lg={3}> 
                                            
                                        </Col>
                                        <Col md={12}  lg={6}>
                                          <ButtonGroup style={{width:`${100}%`, marginTop:10}} row>
                                            <Col md={8}>
                                              {this.renderPickerAutoApply(ranges, local, maxDate)}  
                                            </Col>
                                            <Col md={4}>
                                              <Button color="primary"
                                                      outline
                                                      disabled={disabled}
                                                      className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                      onClick={() => { 
                                                          view_fecha="";
                                                          this.filtrar();
                                                        }}
                                              >Filtrar
                                              </Button>
                                            </Col>
                                          </ButtonGroup>
                                        </Col>
                                        
                                        <Col   md={12} lg={3}> 
                                            
                                        </Col>                              



                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                        <BlockUi tag="div" blocking={this.state.blocking} 
                                    loader={<Loader active type={this.state.loaderType}/>}>
                                    <Card className="main-card mb-1 p-0">
                                        <CardBody className="p-3">                                 
                                            <Row>
                                                <Col   md={12} lg={12}> 
                                                
                                                    <ResponsiveContainer height='100%' width='100%' >
                                                        <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                                    </ResponsiveContainer>   
                                                </Col> 
                                            </Row>
                                        </CardBody>
                                    </Card>
                            </BlockUi>

                        </Col>

                    </Row>
                    <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                    <Spinner color="primary" style={{ marginTop:`${5}%`,marginLeft:`${50}%`,display:load?"block":"none", width: '2rem', height: '2rem'}}/>
                            <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block"}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%`}}
                                onClick={() => {
                                        load=true;
                                        this.filtrar2();
                                        // console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button>
                    </Col>
                    <Col sm="4"></Col>
                    
                    
                    {/* {console.time("loop 3")} */}
                    
                    {
                                                          
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="3" key={data.sonda} style={{display:`${this.state.MultiExport?"none":"block"}`}}>
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        {/* <CSVLink
                                                                        
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: data.unity , key: "y" }
                                                                                // { label: "Temp ("+ data.unity2 + ")", key: "temp" },
                                                                                // { label: "O2 SAT ("+ data.unity3 + ")", key: "oxs" },
                                                                                // { label: "Sal ("+ data.unity4 + ")", key: "sal" }
                                                                            ]
                                                                        
                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                        </CSVLink> */}
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={data.measurements}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: data.unity,
                                                                    accessor: "y",                                                              
                                                                    width: 140
                                                                    }
                                                                    // {
                                                                    //   Header: "Temp ("+ data.unity2+ ")",
                                                                    //   accessor: "temp",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "O2 SAT ("+ data.unity3+ ")",
                                                                    //   accessor: "oxs",                                                              
                                                                    //   width: 140
                                                                    // },
                                                                    // {
                                                                    //   Header: "Sal ("+ data.unity4+ ")",
                                                                    //   accessor: "sal",                                                              
                                                                    //   width: 140
                                                                    // }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         
                         )
        
                            )
                            
                    }
                    <Card className="main-card mb-5 mt-3" style={{display:`${this.showTableExport()}`}}>
                        <CardHeader className="card-header-tab  ">
                          <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                              {this.getNameDispositivo()}
                          </div>
                          <div className="btn-actions-pane-right text-capitalize">
                              {this.generateExport()}
                          </div>                                                  
                        </CardHeader>
                        <CardBody className="p-10 m-10" >          
                          {this.generateTable()}
                      </CardBody>
                      </Card>
                                       

                    </Row>

            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Tendencia_Efectividad);
