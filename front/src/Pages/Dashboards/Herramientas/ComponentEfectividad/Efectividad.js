import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';
import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter,Progress  } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT } from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import {
  setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../../reducers/Session';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
// import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
// import Media from 'react-media';
// import { split } from 'lodash';
import Tendencia_Efectividad from './Tendencia'
import Tendencia2 from '../../Tendencia/ComponentTendencia/Tendencia/Tendencia';
import Configuracion from '../../Resumen/ComponentResumen/Configuracion';
import Config_SMA from '../../Resumen/ComponentResumen/ConfiguracionSMA'
import ConfiguracionLimites from './ConfiguracionLimites'

//sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let load=false;
let icon_up="lnr-arrow-up";
let icon_right="lnr-arrow-right";
let icon_down="lnr-arrow-down";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
//empresas=empresas.filter(e=>e.active==true).map(e=>e);
let ubicaciones=JSON.parse(sessionStorage.getItem("Areas"));
let c=0;
let areas_centros=[];
let centros_data="";
try{
  empresas.map(e=>{return e.areaId});
}catch(e){
  //console.log("areas")
  logout()
}
moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);


let title_centro="";
let _id_centro="";
let Buscar="";
let Buscar2="";
let active_todos=true;
let check=false;
let check2=false;
let active_config="none";
let ModalActive=false;
let ModalActive2=false;
let ModalActive3=false;
let aux_efectividad=true;
let aux_efectividad_global=true;
let diferencia_fechas=0;
let centros_efectividad=[];

//let config_type="actividades";
class Resumen2 extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Mis_Sondas:[],
          Mis_Sondas2:[],
          Modulos:[],
          isLoading:true,
          Mis_Prom:[],
          Mis_Prom2:[],
          MyData:[],
          NameWorkplace:"",
          blocking1: false,
          blocking2: false,
          blocking3: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Abiotic:true,
          Meteo:false,
          Proceso:false,
          Equipos:false,
          Control:false,
          card:[1,2,3,4,5,6,7,8],
          redirect:false,
          //redirect2:false,
          Centros:[],
          act_graph0:true,
          act_graph1:false,
          act_graph2:false,
          act_all:true,
          act_oxd:false,
          act_temp:false,
          act_oxs:false,
          act_sal:false,
          act_oxd2:true,
          act_temp2:false,
          act_oxs2:false,
          act_sal2:false,
          active_grafic:"none",
          ModalState:false,
          TypeGraphic:"1",
          TypeGraphic2:"2",
          Events:[],
          Centro_Actividad:[],
          txtCenActividad:"",
          Actividad:[],
          Centros2:[],
          Efectividad:[],
          Efectividad2:[],
          GEfectividadDay:0,
          GEfectividadMonth:[],
          changeModal:"lnr-apartment",
          btn_filtro:"up",
          btn_filtro2:"",
          btn_filtro3:"",
          EfectArrow:[],
          config_type:"actividades",
          WorkplaceDateInit:[],
        };     
        this.toggle = this.toggle.bind(this);
        //this.getDataCentros = this.getDataCentros.bind(this);
        //this.intervalIdTag2 = this.getCentros.bind(this);
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);      
        this.toggleModal1 = this.toggleModal1.bind(this);
        this.toggleModal2 = this.toggleModal2.bind(this);
        this.toggleModal3 = this.toggleModal3.bind(this);
        this.getCentros = this.getCentros.bind(this);
        this.timeRef=React.createRef();
    }

  
    componentDidMount = () => {
        aux_efectividad=true;
        aux_efectividad_global=true;
        window.scroll(0, 0);
        if(empresa.includes('ideal')){
          // this.createNotifi("me gustan las notificaciones");
          // this.createNotifi("sines");
        }
        if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c2"){
          active_config="block";
        }
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
        }
        let minutos=15;
        let segundos=60*minutos;
        let intervaloRefresco =segundos*1000;
        this.timeRef.current = setInterval(this.getCentros.bind(this),intervaloRefresco);
        this.getCentros();
        this.getActivity();
      }

      componentWillUnmount = () => {
        clearInterval(this.timeRef.current);
        clearInterval(this.getCentros);
        clearInterval(this.getActivity);
        clearInterval(this.timeRef.current);
     }  

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        aux_efectividad=true;
        //this.getSondas(centro[0].toString());
        if(c!=0)
          this.setState({redirect:true,Mis_Prom:nextProps.Mis_Prom,
            Efectividad:nextProps.Efectividad,Efectividad2:nextProps.Efectividad2})
        c++;
      }      

      ingresarCentroUsuario = (centro) =>{
          let {setCentroUsuario} = this.props;
          setCentroUsuario(centro);
      }

      getInitCycles(workplaces){
        let data=workplaces.map(w=>{return ({"workplaceId":w._id})})
        // console.log(data)
        this.tagservices.getArrayCycles(data).then(respuesta=>{
          this.setState({WorkplaceDateInit:respuesta})
        }).catch(e=>"error")
      }

      // createNotifi(body){
      //   if ('serviceWorker' in navigator) {
      //     navigator.serviceWorker.register('/sw.js')
      //     .then(function(registration) {
      //       registration.addEventListener('updatefound', function() {
      //         // If updatefound is fired, it means that there's
      //         // a new service worker being installed.
      //         var installingWorker = registration.installing;
      //         console.log('A new service worker is being installed:',
      //           installingWorker);
      //         // You can listen for changes to the installing service worker's
      //         // state via installingWorker.onstatechange
      //       },
      //       registration.showNotification('esta es una notificacion',{
      //         body:`${body}`,
      //         icon:'https://idealcontrol.cl/wp/wp-content/uploads/2020/08/cropped-logo-cuadrado-300x300.png',
      //       })
      //       );
      //     })
      //     .catch(function(error) {
      //       console.log('Service worker registration failed:', error);
      //     });
      //   } else {
      //     console.log('Service workers are not supported.');
      //   }
      // }

      toggle(e) {
        let event = e.target.dataset.event;
        //console.log(this.state.collapse)
        //console.log(Number(event))
        this.setState({ collapse: this.state.collapse === Number(event) ? 0 : Number(event) });
      }

      divideNameZone(nameAddress){
        let namezone0=nameAddress.split('_')
        let namezone1=String(namezone0[1]).split(' ')
        return String(namezone1[0])
      }

      getEfectividad(tagsids){
        //let tagsids=this.state.Mis_Prom2.map(m=>m.id);
        tagsids={tags:tagsids}
        //console.log(tagsids)
        try{
            this.tagservices.getTagsEfectividad(tagsids).then(response=>{
              if(response.statusCode===200 || response.statusCode===201){
                aux_efectividad=false;
                let data=response.data
                this.setState({Efectividad:this.state.Efectividad.concat(data.map(e=>{
                  return ({
                    "locationId": e.locationId,
                    "my_register": e.my_register,
                    "porcentaje": e.porcentaje_efectividad,
                    "register_100": e.register_100,
                    "workplaceId": e.workplaceId,
                    "_id": e._id
                    })
                }))})
              }else{
                console.log("error algo salio mal :"+response.statusCode)
              }
            }).catch(e=>e)
        }catch(e){
          //console.log(e)
        }
        
      }

      getEfectividadMonth(workplaceId,fechaInicio,fechaFin){
        let now=moment().format('YYYY-MM-DDT00:00:00')
        let end_date=moment().format('YYYY-MM-DDT23:59:00')
        fechaInicio==now?fechaFin=end_date:fechaFin=fechaFin
        this.tagservices.getWorkplaceEfectividadMonth(workplaceId,fechaInicio,fechaFin).then(response=>{
          if(response.statusCode===200 || response.statusCode===201){
            this.setState({GEfectividadMonth:this.state.GEfectividadMonth.concat(response.data)})
          }else{
            console.log("error algo salio mal :"+response.statusCode)
          }
        }).catch(e=>e)
      }

      getEfectividadDay(workplaceIds){
        //console.log("work"+JSON.stringify(workplaceIds))
        let start_date=moment().format('YYYY-MM-DDT00:00:00');
        let end_date=moment().format('YYYY-MM-DDThh:mm:ss');
        this.tagservices.getWorkplaceEfectividadDay(workplaceIds,start_date,end_date).then(response=>{
          if(response.statusCode==200 || response.statusCode==201){
            //console.log("workplace"+JSON.stringify(response.data))
            this.setState({Efectividad2:response.data})
          }
        }).catch(e=>{
          //console.log(e)
        })
      }

      newLocations(tags,w){
        return tags.filter(t=>t.active==true).map(t=>{

          let promedio={
            id:t._id,
            name:t.nameAddress,
            locationId:t.locationId,
            prom:t.prom,
            max:t.alertMax,
            min:t.alertMin,
            centroId:w._id,
            ultimo_valor:t.lastValueWrite,
            ultima_fecha:t.dateTimeLastValue,
            alerta_max:t.alertMax,
            alerta_min:t.alertMin,
          }
          return promedio
        })
      }

      getWorkplaceArrow(workplaceId){
        let f1=moment().format(`YYYY-MM-DDT00:00:00`)
        let f2=moment(f1).subtract(1,"days").format(`YYYY-MM-DDT00:00:00`)
        let f3=moment(f2).subtract(1,"days").format(`YYYY-MM-DDT00:00:00`)
        let f4=moment(f3).subtract(1,"days").format(`YYYY-MM-DDT00:00:00`)
        this.tagservices.getWorkplaceArrowEfectividad(workplaceId,f1,f2,f3,f4).then(response=>{
          if(response.statusCode===200 || response.statusCode===201){
            this.setState({EfectArrow:this.state.EfectArrow.concat({workplace:workplaceId,medicion:response.data})})
          }
        }).catch(e=>console.log(e))
      }

      getCentros(){
          aux_efectividad=true;
          this.setState({Mis_Prom2:[]})
          
          let start_date=moment().startOf('month').format('YYYY-MM-DDT00:00:00');
          let end_date=moment().subtract(1,"days").format('YYYY-MM-DDT23:59:00');
          //console.log(end_date)
          let f1=new Date(start_date);
          let f2=new Date();
          var diff = f2 - f1;
          //console.log(diff);

          // Calcular días
          let diferenciaDias = Math.floor(diff / (1000 * 60 * 60 * 24));
          diferencia_fechas=diferenciaDias+1;
          let token=sessionStorage.getItem("token_cloud")
          try{
            this.tagservices.getUserToken(token).then(respuesta=>{
              let centros=respuesta.centros+respuesta.control;
              if(respuesta.centros.length==0 && respuesta.control.length>0){
                this.switchActive_principal(false,false,false,false,true);
              }
              //let centros=respuesta.control;
              // if(this.state.Control){
              //   let control=respuesta.control;
              //   centros=""+control;
              // }
              this.tagservices.getWorkplaceUser(centros).then(workplace=>{
                try{
                  this.getInitCycles(workplace)
                  areas_centros=workplace.map(e=>{return e.areaId});
                  areas_centros=areas_centros.filter(this.onlyUnique)
                  if(this.state.Mis_Prom.length==0){
                    this.getEfectividadDay(workplace.filter(w=>w.active==true).map(w=>{return({workplaceId:w._id} )}))
                    workplace.filter(w=>w.active==true).map((w)=>{
                      this.getWorkplaceArrow(w._id)
                      // console.log(start_date)
                      // console.log(end_date)
                      this.getEfectividadMonth(w._id,start_date,end_date)
                      let Modulos=[];
                      this.tagservices.getTagsWorkplace(w._id,"get").then(respuesta=>{
                        let tags=respuesta.data;
                        tags=tags.filter(t=>!t.nameAddress.includes('Salinidad')&&t.active==true)
                        let locations=[];
                        let zones0=[]
                        let zones=[];
                        locations=this.newLocations(tags,w);
                        let ids_location=locations.map(l=>l.id)
                        this.getEfectividad(ids_location);
                        
                        tags.map(t=>{
                          zones0.push(t.zoneId+this.divideNameZone(t.nameAddress))
                        })
                        zones0=zones0.filter(this.onlyUnique)
                        zones0.map(z=>{
                          let zone=tags.filter(t=>t.zoneId+this.divideNameZone(t.nameAddress)==z)[0]
                          zones.push(this.divideNameZone(zone.nameAddress))
                        })
                        zones=zones.filter(this.onlyUnique)
                        Modulos.push({centro:w.name,zonas:zones})
                        this.setState({Mis_Prom:this.state.Mis_Prom.concat(locations),
                                       Mis_Prom2:this.state.Mis_Prom2.concat(locations),
                                       Modulos:this.state.Modulos.concat(Modulos)})
                        // console.log(w.name)
                        // console.log(locations)
                        //console.log(tags)
                      }).catch(e=>{
                        //console.log(object)
                      })
                    })
                    this.setState({Centros:_.orderBy(workplace,["name"],["asc"]),Centros2:_.orderBy(workplace,["name"],["asc"]),isLoading:false})
                  }else{
                    this.setState({Efectividad:[]})
                    workplace.filter(w=>w.active==true).map((w)=>{
                      this.getEfectividadDay(workplace.filter(w=>w.active==true).map(w=>{return({workplaceId:w._id} )}))
                      workplace.filter(w=>w.active==true).map((w)=>{
                        this.getWorkplaceArrow(w._id)
                        // console.log(start_date)
                        // console.log(end_date)
                        this.getEfectividadMonth(w._id,start_date,end_date)
                        let Modulos=[];
                        this.tagservices.getTagsWorkplace(w._id,"get").then(respuesta=>{
                          let tags=respuesta.data;
                          tags=tags.filter(t=>!t.nameAddress.includes('Salinidad')&&t.active==true)
                          let locations=[];
                          let zones0=[]
                          let zones=[];
                          locations=this.newLocations(tags,w);
                          let ids_location=locations.map(l=>l.id)
                          this.getEfectividad(ids_location);
                          
                          tags.map(t=>{
                            zones0.push(t.zoneId+this.divideNameZone(t.nameAddress))
                          })
                          zones0=zones0.filter(this.onlyUnique)
                          zones0.map(z=>{
                            let zone=tags.filter(t=>t.zoneId+this.divideNameZone(t.nameAddress)==z)[0]
                            zones.push(this.divideNameZone(zone.nameAddress))
                          })
                          zones=zones.filter(this.onlyUnique)
                          Modulos.push({centro:w.name,zonas:zones})
                          this.setState({Mis_Prom2:this.state.Mis_Prom2.concat(locations)})
                          // console.log(w.name)
                          // console.log(locations)
                          //console.log(tags)
                        }).catch(e=>{
                          //console.log(object)
                        })
                      })
                    })
                    this.setState({Centros2:_.orderBy(workplace,["name"],["asc"])})
                    aux_efectividad=false;
                  }
                }
                catch(e){
                  //console.log(e)
                }
              })
            }).catch(e=>e)
          }catch(e){
            //console.log(e)
          }
      }

    toggleModal1(ModalState,_id,title) {
      if(!ModalState){
        Buscar2=""
        this.setState({ModalState,Centro_Actividad:[],changeModal:"lnr-apartment"})
      }
      ModalActive=ModalState;
      this.setState({ModalState,changeModal:"lnr-apartment"})
      _id_centro=_id;
      title_centro=title;
    }

    toggleModal2(ModalState,_id,title) {
      if(!ModalState){
        Buscar2=""
        this.setState({ModalState,Centro_Actividad:[],changeModal:"lnr-apartment"})
      }
      ModalActive2=ModalState;
      this.setState({ModalState,changeModal:"lnr-apartment"})
      _id_centro=_id;
      title_centro=title;
    }

    toggleModal3(ModalState,_id,title) {
      centros_data=`${_id}/${title}`
      if(!ModalState){
        Buscar2=""
        this.setState({ModalState,Centro_Actividad:[],changeModal:"lnr-apartment"})
      }
      ModalActive3=ModalState;
      this.setState({ModalState,changeModal:"lnr-apartment"})
      _id_centro=_id;
      title_centro=title;
    }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }

    getFindEfectividad(id){
      try{
        let efectividad=this.state.Efectividad.filter(e=>e._id==id)[0];
        //console.log(efectividad)
        return efectividad
      }catch(e){
        //console.log(e)
      }
    }

    getFindEfectividad2(id){
      try{
        let efectividad=this.state.Efectividad2.filter(e=>e.locationId==id)[0];
        return efectividad
      }catch(e){
        //console.log(e)
      }
    }

    getFindEfectividadMonth(id){
      try{
        let efectividad=this.state.GEfectividadMonth.filter(e=>e.locationId==id)[0];
        return efectividad
      }catch(e){
        //console.log(e)
      }
    }

    getPromCentro(id,type,color,modulo,tipo){
      // console.log(modulo)
      if(modulo==900){
        modulo="Ponton"
      }
      if(modulo==1000){
        modulo="MODULOS"
      }
      let unity="";
      if(type=="oxd")
        type=" oxd";
        unity="mg/L";
      if(type=="temp")
        unity="°C";
      if(type=="oxs")
        unity="%";
      if(type=="sal")
        unity="PSU";
      let measurements=[];
      modulo!=""?measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p):
      measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)).map(p=>p)
      //console.log(measurements)
      let efectividades=[];
      let efectividadesMonth=[];
      try{
        measurements.filter(m=>!m.name.includes('VoltajeR')).map(m=>{
          let efectividad=[]
          //console.log(this.state.Control)
          this.state.Control?
          efectividad=this.getFindEfectividad2(m.locationId):
          efectividad=this.getFindEfectividad(m.id)
          
          let efectividadMonth=this.getFindEfectividadMonth(m.locationId);
          if(efectividadMonth!=undefined){
            efectividadesMonth.push(efectividadMonth)
          }else{
            efectividadesMonth.push({porcentaje:0,count:0})
          }
          if(efectividad!=undefined){
            efectividades.push(efectividad);
          }
        })
        //console.log(efectividadesMonth)
        //console.log(efectividades)
        //let prom=((efectividades.reduce((a, b) => +a + +b.porcentaje_efectividad, 0)/measurements.length)).toFixed(1);
        //console.log(diferencia_fechas)
        let diferencia_registros100=diferencia_fechas*288;
        let ultimo_valor=tipo=="month"?((efectividadesMonth.reduce((a, b) => +a + +(b.porcentaje), 0)/efectividadesMonth.length)).toFixed(1):
                                        ((efectividades.reduce((a, b) => +a + +b.porcentaje, 0)/efectividades.length)).toFixed(1);
        //console.log(ultimo_valor)
        if(isNaN(ultimo_valor)){
          ultimo_valor=0;
        }
        //console.log(efectividadesMonth)
        let icon="";
        let mensaje="";
        if(ultimo_valor>60){
          icon=icon_up;
          mensaje="Tendencia sobre la media"
        }if(ultimo_valor<=60 && ultimo_valor>=50){
          icon=icon_right;
          mensaje="Tendencia entre la media"
        }if(ultimo_valor<50){
          icon=icon_down;
          mensaje="Tendencia bajo la media"
        }
        // if(ultimo_valor>=100){
        //   ultimo_valor=100;
        // }
       if(color=="month"){
         return ultimo_valor
       }
       if(color!=""){
            return <Fragment>
              <Progress style={{height:`${80}%`}} value={ultimo_valor}><font><span id={`oxd_tendencia${id}`} className={""}></span><b> {ultimo_valor} %</b></font></Progress>
            </Fragment>
      }
      else{
        return ultimo_valor
      }
    }catch(e){
      //console.log(e)
    }
      
  }

    getPromMaxCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      if(modulo==1000){
        modulo="MODULOS"
      }
      let measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p);
      if(color!="")
        return <font color={color}><b>max: {((measurements.reduce((a, b) => +a + +b.max, 0)/measurements.length)).toFixed(1)}</b></font>
      else
        return ((measurements.reduce((a, b) => +a + +b.max, 0)/measurements.length)).toFixed(1)
    }

    getPromMinCentro(id,type,color,modulo){
      if(modulo==900){
        modulo="Ponton"
      }
      if(modulo==1000){
        modulo="MODULOS"
      }
      let measurements=this.state.Mis_Prom2.filter(p=>p.centroId==id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p);
      if(color!="")
        return <font color={color}><b>min: {((measurements.reduce((a, b) => +a + +b.min, 0)/measurements.length)).toFixed(1)}</b></font>
      else
        return ((measurements.reduce((a, b) => +a + +b.min, 0)/measurements.length)).toFixed(1)
    }

    getColorCol(prom,min,max,unity,color,index,ultimo_valor,id,locationId,modulo){
      try{
        //let efectividad=this.state.Efectividad.filter(e=>e._id==id)[0];
        //console.log(efectividad)
        let efectividad=this.state.Control?this.state.Efectividad2.filter(e=>e.locationId==locationId)[0]:this.state.Efectividad.filter(e=>e._id==id)[0];
        // if(efectividad==undefined){
        //   efectividad=this.state.Efectividad2.filter(e=>e.locationId==locationId)[0];
        // }
        //console.log(efectividad)
        let efectividadMouth=this.state.GEfectividadMonth.filter(g=>g.locationId==locationId)[0]
        //console.log(efectividadMouth)
        //console.log(this.state.Mis_Prom2.filter(m=>m.name.includes(modulo)&&m.centroId==efectividad.workplaceId))
        // console.log(efectividadMouth)
        // console.log(diferencia_fechas)

        let icon="";
        let icon2="";
        let mensaje="";
        ultimo_valor=efectividad.porcentaje;
        if(ultimo_valor>60){
          icon=icon_up;
          // mensaje="Tendencia sobre la media"
        }if(ultimo_valor>=50 && ultimo_valor<=60){
          icon=icon_right;
          // mensaje="Tendencia entre la media"
        }if(ultimo_valor<50){
          icon=icon_down;
          // mensaje="Tendencia bajo la media"
        }
        if(ultimo_valor>100){
          ultimo_valor=100;
        }
        //console.log("dif "+diferencia_fechas)
        let diferencia_registros100=diferencia_fechas*288;
        //console.log(diferencia_registros100)
        let ultimo_valor2=(efectividadMouth.count)*100/diferencia_registros100;
        ultimo_valor2=Math.round(ultimo_valor2 * 100) / 100;
        if(ultimo_valor2>60){
          icon2=icon_up;
          // mensaje="Tendencia sobre la media"
        }if(ultimo_valor2=>50 && ultimo_valor2<=60){
          icon2=icon_right;
          // mensaje="Tendencia entre la media"
        }if(ultimo_valor2<50){
          icon2=icon_down;
          // mensaje="Tendencia bajo la media"
        }
        if(ultimo_valor>100){
          ultimo_valor=100;
        }
        
        if(isNaN(ultimo_valor) || ultimo_valor==undefined || ultimo_valor==null){
          ultimo_valor=0;
        }
        if(modulo=="control"){
          return (
            <Fragment>
              <Row>
              <Col>DIA</Col>
              <Col>MES</Col>
              </Row>
            <Row>
              <Col style={{maxWidth:`${100}%`}} id="PopoverFocus" onClick={this.toggle} data-event={index}>
                <font color={color} >
                  <Progress  value={ultimo_valor}>
                    <font><span className={""}></span><b> {Math.round(ultimo_valor*100)/100} %</b></font>
                  </Progress>
                </font>
              </Col>
              <Col style={{maxWidth:`${100}%`}}  id="PopoverFocus" onClick={this.toggle} data-event={index}>
                <font color={color} >
                  <Progress  value={ultimo_valor2}>
                    <font><span className={""}></span><b> {ultimo_valor2} %</b></font>
                  </Progress>
                </font>
              </Col>
            </Row>
            </Fragment>
            )
        }
        return (
        <Fragment>
          {/* <UncontrolledTooltip placement="top-end" target={`oxd_tendencia2${id}`}>
            {mensaje}
          </UncontrolledTooltip> */}
          {/* <Col id="PopoverFocus" onClick={this.toggle} data-event={index}><font color={color} >
            &nbsp;Efectividades</font>
          </Col> */}
          <Col id="PopoverFocus" onClick={this.toggle} data-event={index} style={{maxWidth:100}} >
            <font color={color} >
              <Progress style={{height:`${80}%`}} value={ultimo_valor}>
                <font><span className={""}></span><b> {Math.round(ultimo_valor*100)/100} %</b></font>
              </Progress>
            </font>
          </Col>
          <Col style={{maxWidth:100}}  id="PopoverFocus" onClick={this.toggle} data-event={index}>
            <font color={color} >
              <Progress style={{height:`${80}%`}} value={ultimo_valor2}>
                <font><span className={""}></span><b> {ultimo_valor2} %</b></font>
              </Progress>
            </font>
          </Col>
          {/* <Col id="PopoverFocus"><font color={color}>min {min}</font></Col>
          <Col id="PopoverFocus"><font color={color}>max {max}</font></Col> */}
        </Fragment>
        )
      }catch(e){
        //console.log(e)
        if(modulo=="control"){
          return (
            <Fragment>
              <Row>
              <Col>DIA</Col>
              <Col>MES</Col>
              </Row>
            <Row>
              <Col style={{maxWidth:`${100}%`}} id="PopoverFocus" onClick={this.toggle} data-event={index}>
                <font color={color} >
                  <Progress  value={0}>
                    <font><span className={""}></span><b> {0} %</b></font>
                  </Progress>
                </font>
              </Col>
              <Col style={{maxWidth:`${100}%`}}  id="PopoverFocus" onClick={this.toggle} data-event={index}>
                <font color={color} >
                  <Progress  value={0}>
                    <font><span className={""}></span><b> {0} %</b></font>
                  </Progress>
                </font>
              </Col>
            </Row>
            </Fragment>
            )
        }
        return (<Fragment>
          {/* <UncontrolledTooltip placement="top-end" target={`oxd_tendencia2${id}`}>
            {mensaje}
          </UncontrolledTooltip> */}
          {/* <Col id="PopoverFocus" onClick={this.toggle} data-event={index}><font color={color} >
            &nbsp;Efectividades</font>
          </Col> */}
          <Col id="PopoverFocus" onClick={this.toggle} data-event={index} style={{maxWidth:100}} >
            <font color={color} >
              <Progress style={{height:`${80}%`}} value={0}>
                <font><span className={""}></span><b> {0} %</b></font>
              </Progress>
            </font>
          </Col>
          <Col style={{maxWidth:100}}  id="PopoverFocus" onClick={this.toggle} data-event={index}>
            <font color={color} >
              <Progress style={{height:`${80}%`}} value={0}>
                <font><span className={""}></span><b> {0} %</b></font>
              </Progress>
            </font>
          </Col>
          {/* <Col id="PopoverFocus"><font color={color}>min {min}</font></Col>
          <Col id="PopoverFocus"><font color={color}>max {max}</font></Col> */}
        </Fragment>)
      }
      
    }
    getColorColControl(prom,unity,color,index,ultimo_valor,id){
      let icon="";
      let mensaje="";
      if(ultimo_valor>Math.round(prom)){
        icon=icon_up;
        mensaje="Tendencia sobre el promedio"
      }if(ultimo_valor=>prom && ultimo_valor<=Math.round(prom)){
        icon=icon_right;
        mensaje="Tendencia entre el promedio"
      }if(ultimo_valor<prom && ultimo_valor<Math.round(prom)){
        icon=icon_down;
        mensaje="Tendencia bajo el promedio"
      }
      return (
      <Fragment>
        <UncontrolledTooltip placement="top-end" target={`oxd_tendencia2${id}`}>
          {mensaje}
        </UncontrolledTooltip>
        <Col id="PopoverFocus"><font color={color}><span id={`oxd_tendencia2${id}`} className={""}></span> {check==true?prom:ultimo_valor}</font></Col>
      </Fragment>
      )
    }

    createTooltip(id,mensaje){
        return <UncontrolledTooltip placement="top-end" target={id}>
          {mensaje}
      </UncontrolledTooltip>
      
    }

    endDate(_id){
      let data=this.state.Centros2.filter(e=>e._id==_id && e.endDate!="").map((e)=>e.endDate)
      let data2=this.state.Centros2.filter(e=>{return e._id==_id}).map(e=>{
        // fecha.setHours(fecha.getHours()-1);
        return e.endDate
      })
      //alert(data2[0])
      if(data2[0]!=null && data[0]!=null){
        return moment(data2[0]).format("HH:mm DD-MMM")
      }else{
        return <Badge color="secondary" pill>sin registros actualizados</Badge>
      }
    }

    endDate2(_id){
      let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      let color="", color2="";
      let data=this.state.Centros2.filter(e=>e._id==_id && e.endDate!="").map((e)=>{
        color=this.getColorAlert(e,"oxd",100,miscolores,0,"");
        color2=this.getColorAlert(e,"oxd",200,miscolores,0,"");
        return e.endDate
      })
      let data2=this.state.Centros2.filter(e=>e._id==_id).map(e=>{
        var fecha=new Date(e.endDate);
        // fecha.setHours(fecha.getHours()-1);
        return fecha
      })
      let myId="status"+_id;
      if(data[0]!=null && data2[0]!=null){
        let fecha=new Date();
        let f1=data2[0];
        let f2=new Date(fecha.setHours(fecha.getHours()-1));
        let f3=new Date(fecha.setHours(fecha.getHours()-12));
        if(color!="" || color2!=""){
          //alert(color);
          return <Fragment>{this.createTooltip(myId,"Registros en 0")}
           <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        }
        else if(f1>=f2){
          //alert(f2)
          return <Fragment>{this.createTooltip(myId,"En linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div></Fragment>
        }
        else if(f1>=f3){
          return <Fragment>{this.createTooltip(myId,"En linea en las 12hrs.")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-dark">Dark</div></Fragment>
        }
        // else if(f1<moment(f2).format("HH:mm DD-MMM") && f1>=moment(f3).format("HH:mm DD-MMM")){
        //   return <Fragment>{this.createTooltip(myId,"En linea dentro de las 24 hrs")}
        //   <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-warning">Warning</div></Fragment>
        // }
        else{
          return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
          <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
        }
      }else{
        return <Fragment>{this.createTooltip(myId,"Fuera de linea")}
        <div id={myId} style={{width:20,height:20}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div></Fragment>
      }
    }

    

    
    getArea(type,n){
      const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
      if(type=="oxd"){
        return <Area dataKey={"oxd"+n} barSize={25}  fill={miscolores[0]} stroke={miscolores[0]} />
      }
      if(type=="temp"){
        return <Area dataKey={"temp"+n} barSize={25} fill={miscolores[5]} stroke={miscolores[5]}/>
      }
      if(type=="oxs"){
        return <Area dataKey={"oxs"+n} barSize={25} fill={miscolores[2]} stroke={miscolores[2]}/>
      }
      if(type=="sal"){
        return <Area dataKey={"sal"+n} barSize={25} fill={miscolores[1]} stroke={miscolores[1]}/>
      }
      
    }
    
    switchActive(act_all,act_oxd,act_temp,act_oxs,act_sal){
      this.setState({act_all,act_oxd, act_temp, act_oxs, act_sal})
    }
    switchActive2(act_oxd2,act_temp2,act_oxs2,act_sal2){
      this.setState({act_oxd2, act_temp2, act_oxs2, act_sal2})
    }

    inputChangeHandler = (event) => { 
      //console.log(event.target.id);  
      this.setState( { 
          ...this.state,
          [event.target.id]: event.target.value
      } );
  }

  getDetails(_id,type,color,visible){
    return this.state.Mis_Prom2.filter(p=>p.centroId==_id&&p.name.includes(type)).map(p=>{
      return (<Fragment><font color={color} style={{display:visible}}>
      <Row >
        <Col><b>{p.name}</b></Col>
      </Row>
      <Row >
        <Col><b>prom.</b> {p.prom}</Col>
        <Col><b>ultimo reg.</b> {p.ultimo_valor}</Col>
        <Col><b>min</b> {p.min}</Col>
        <Col><b>max</b> {p.max}</Col>
      </Row><br></br></font></Fragment>)
    })
  }

  getName(name){
    if(name.includes("OD ")){
      let new_name="";
      if(name.includes("oxd")){
        new_name=name.replace("oxd", "");
        return "Oxd de "+new_name
      }
      if(name.includes("temp")){
        new_name=name.replace("temp", "");
        return "Oxd de "+new_name
      }
      if(name.includes("oxs")){
        new_name=name.replace("oxs", "");
        return "Oxd de "+new_name
      }
      if(name.includes("sal")){
        new_name=name.replace("sal", "");
        return "Oxd de "+new_name
      }
    }else{
      return name
    }
  }

  getColorAlert(c,type,modulo,miscolores,ind,tipo){
    let alarma=0;
    let color="";
    if(modulo==900){
      modulo="Ponton"
    }
    if(modulo==1000){
      modulo="MODULOS"
    }
    let measurements=[];
    modulo!=""?measurements=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.name.includes(type)&&p.name.includes(modulo)).map(p=>p):
               measurements=this.state.Mis_Prom2.filter(p=>p.centroId==c._id&&p.name.includes(type)).map(p=>p)

    let efectividades=[];
    let efectividadesMonth=[];
      measurements.filter(m=>!m.name.includes('VoltajeR')).map(m=>{
        //let efectividad=this.getFindEfectividad(m.id);
        let efectividad=[];
        this.state.Control?
          efectividad=this.getFindEfectividad2(m.locationId):
          efectividad=this.getFindEfectividad(m.id)
        let efectividadMonth=this.getFindEfectividadMonth(m.locationId);
        if(efectividadMonth!=undefined){
          efectividadesMonth.push(efectividadMonth)
        }else{
          efectividadesMonth.push({porcentaje:0,count:0})
        }
        if(efectividad!=undefined){
          efectividades.push(efectividad);
        }
      })
      let diferencia_registros100=diferencia_fechas*288;
      let ultimo_valor=tipo=="month"?((efectividadesMonth.reduce((a, b) => +a + +(b.porcentaje), 0)/efectividadesMonth.length)).toFixed(1):
                                      ((efectividades.reduce((a, b) => +a + +b.porcentaje, 0)/efectividades.length)).toFixed(1);

      //console.log(isNaN(ultimo_valor))
      ultimo_valor=isNaN(ultimo_valor)?0:ultimo_valor;
      //console.log(ultimo_valor)
      if(ultimo_valor<98 && ultimo_valor>=90){
        alarma=1;
      }
      if(ultimo_valor<90){
        alarma=2;
      }
      //console.log(miscolores)
      if(alarma==0){
        return color
      }else if(alarma==1){
        color=miscolores[2];
        return color
      }
      else{
        color=miscolores[5];
        return color
      }
  }

  findOneEvent(workPlace){
    let val=this.state.Events.filter(e=>e.workplaceId==workPlace&&e.active==true)
    let c=val.length;
    return c
  }

  findActiveWorkplace(workPlace){
    let val=this.state.Centros2.filter(c=>c._id==workPlace&&c.active==true&&workPlace!="")
    let c=val.length;
    return c
  }

  getPar(name){
    let separador=name.split(' ');
    let separador2=String(separador[1]).split('_');
    let num=parseInt(separador2[0]);
    if(num%2==0){
      return "par"
    }else{
      return "impar"
    }
  }

  getColumParInp(l,i,largo,index,m,miscolores,data_par,data_inpar,name,data,superior,inferior,setpoint){
    //if(largo%2==0){
      if(name.includes('Yaguepe')){
        return <Row onClick={this.toggle} data-event={index+m}>
          <Col>
            {this.generateInpar(l,i,largo,index,m,miscolores,data,superior,inferior,setpoint)}
          </Col>
        </Row>
      }else{
        return <Row onClick={this.toggle} data-event={index+m}>
          <Col>
            {this.generateInpar(l,i,largo,index,m,miscolores,data_inpar)}
          </Col>
          <Col>
            {this.generatePar(l,i,largo,index,m,miscolores,data_par)}
          </Col>
        </Row>
      }
      

  }

  getNameCentro(name){
    let split=name.split('_Mod')
    return split[0]
  }

  generatePar(l,i,largo,index,m,miscolores,data){
    //,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`  
    return data.map(d=>{
      //console.log(d)
      return <Row>
         <UncontrolledTooltip placement="top-end" target={`oxd_divice2${d.id}`}>
        {!check?"valores porcentuales del día anterior a las 12:00 pm hasta la hora actual y valores  mensuales":
        "Promedio de sonda desde las 00:00 hasta hora actual"}
        </UncontrolledTooltip>
        <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${d.id}`} style={{color:"black",size:20}} ></i> 
          <Card style={{marginTop:20}}>
            <CardHeader style={{maxHeight:15,backgroundColor:"rgb(84, 92, 216)",color:"#fff"}}> {this.getNameCentro(d.name)}</CardHeader>
            <CardBody style={{maxHeight:60,marginTop:-20}}>
              {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],index+m,l.ultimo_valor,l.id,d.locationId,"control")}
            </CardBody>
          </Card>
          {/* <Button  style={{marginTop:15, width:`${55}%`}} 
                className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 animated fadeIn fast" color="primary">     
                            <div className="pl-1" style={{fontSize:10}}>  
                            {this.getNameCentro(d.name)}</div> 
                        <span className="badge badge-light m-0 ml-0 pl-1 w-100">  
                        <font size="2">{d.ultimo_valor}</font>
                        
                        </span>   
          </Button> */}
        </Col>
    </Row>
    })
    
  }
  getStatusColor(data,superior,inferior,setpoint){
    let media=data.alerta_max-data.alerta_min;
    media=media/2;
    media=data.alerta_min+media;
    if(data.ultimo_valor<inferior.ultimo_valor){
      return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF0000"}}> </span>
    }
    if(data.ultimo_valor>=inferior.ultimo_valor && data.ultimo_valor<setpoint.ultimo_valor){
      return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FF8000"}}> </span>
    }
    if(data.ultimo_valor>=setpoint.ultimo_valor && data.ultimo_valor<superior.ultimo_valor){
      return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#FFFF00"}}> </span>
    }
    if(data.ultimo_valor>=superior.ultimo_valor){
      return <span class="badge badge-dot badge-dot-lg badge-success" style={{backgroundColor:"#007bff"}}> </span>
    }
  }

  generateInpar(l,i,largo,index,m,miscolores,data,superior,inferior,setpoint){
    return data.map(d=>{
      //console.log(d.id)
      let name=d.name.split(" oxd");
      // let banda_superior=superior.filter(l=>l.name.includes(String(name[0])))[0];
      // let banda_inferior=inferior.filter(l=>l.name.includes(String(name[0])))[0];
      // let set_point=setpoint.filter(l=>l.name.includes(String(name[0])))[0];
      // console.log(name[0])
      //console.log(set_point)
      return <Row>
         <UncontrolledTooltip placement="top-end" target={`oxd_divice2${d.id}`}>
        {!check?"valores porcentuales del día anterior a las 12:00 pm hasta la hora actual y valores  mensuales":
        "Promedio de sonda desde las 00:00 hasta hora actual"}
        </UncontrolledTooltip>
        <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${d.id}`} style={{color:"black",size:20}} ></i> 
        <Card style={{marginTop:20}}>
            <CardHeader style={{maxHeight:15,backgroundColor:"rgb(84, 92, 216)",color:"#fff"}}> {this.getNameCentro(d.name)}</CardHeader>
            <CardBody style={{maxHeight:60,marginTop:-20}}>
              {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],index+m,l.ultimo_valor,l.id,d.locationId,"control")}
            </CardBody>
          </Card>
          {/* <Button  style={{marginTop:15, width:`${55}%`}} 
                className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 animated fadeIn fast" color="primary">     
                            <div className="pl-1" style={{fontSize:10}}>  
                            {this.getNameCentro(d.name)}</div> 
                        <span className="badge badge-light m-0 ml-0 pl-1 w-100">  
                        <font size="2">{d.ultimo_valor}</font>
                        
                        </span>   
          </Button> */}
        </Col>
    </Row>
    })
  }

  getAcordion(index,miscolores,collapse,c,modulo){
    //console.log(modulo.includes("MODULOS"))
    //console.log(c.name.includes(" Control"))
    let m=0;
    let visible="block";

    if(modulo.includes("Ponton")){
      m=900;
    }else{
      m=parseInt(modulo)
    }
    if(modulo.includes("MODULOS")){
      m=1000;
    }
    if(modulo.includes("100")){
      m=100;
    }
    if(modulo.includes("200")){
      m=200;
    }
    if(modulo.includes("300")){
      m=300;
    }
    if(modulo.includes("400")){
      m=400;
    }
    if(modulo.includes("500")){
      m=500;
    }
    if(modulo.includes("600")){
      m=600;
    }
    if(c.endDate=="" || c.endDate==null){
      visible="none"
    }
    let promedios=this.state.Mis_Prom;
    if(c.name.includes(' Control')){
      promedios=this.state.Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
                                                                 &&!l.name.includes("VoltajeR"))
    }else{
      promedios=this.state.Mis_Prom.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd")
                                      &&l.name.includes(m!=900?m:"Ponton"))
    }
    return <Fragment>
     {/* this.getColorAlert(c,"oxd",m,miscolores) */}
    <ListGroupItem style={{borderColor:this.getColorAlert(c,"oxd",m,miscolores),borderWidth:3,display:visible}}  action onClick={this.toggle} data-event={index+100}>
    <UncontrolledTooltip placement="top-end" target={`oxd_divice${c._id}`}>
      {!check?"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 12:00 pm del día anterior"
      :"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 00:00 hasta la hora actual"}
      
    </UncontrolledTooltip>
      <Row style={{height:25}}>
      {/* {console.log(this.state.Mis_Prom.filter(p=>p.centroId==c._id&&p.name.includes(m)&&p.name.includes(' oxd')).map(p=>p.prom))} */}
        <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice${c._id}`} style={{color:"black",size:20,marginTop:2}} class="pe-7s-help1"></i> Modulo {modulo} Ox.</Col>
        {/* <Col onClick={this.toggle} data-event={index+m}>Efectividades</Col> */}
        <Col style={{maxWidth:100}} onClick={this.toggle} data-event={index+m}>{this.getPromCentro(c._id," oxd",miscolores[0],m)}</Col>
        <Col style={{maxWidth:100}}  onClick={this.toggle} data-event={index+m}>{this.getPromCentro(c._id," oxd",miscolores[0],m,"month")}</Col>
      </Row>
    </ListGroupItem>
    <Collapse isOpen={collapse === index+m}>
    {promedios.map((l,i)=>{
        let data=promedios.filter((l)=>c._id==l.centroId&&l.name.includes(" oxd"))
        let largo=data.length;
        //console.log(data)
        let color="";
        // if(l.ultimo_valor>60){
        //   color=miscolores[2];
        // }
        // if(l.ultimo_valor<=60){
        //   color=miscolores[2];
        // }
        
        if(c.name.includes(' Control')){
          let data_par=data.filter(d=>this.getPar(d.name)=="par")
          data_par=_.orderBy(data_par,["name"],"asc");
          let data_inpar=data.filter(d=>this.getPar(d.name)!="par")
          data_inpar=_.orderBy(data_inpar,["name"],"asc");
          data=_.orderBy(data,["name"],"asc");
          let data_superior="",data_inferior="",data_setpoint="";
          // let data_superior=this.state.Mis_Prom.filter(l=>l.name.includes(" bandasuperior"))
          // let data_inferior=this.state.Mis_Prom.filter(l=>l.name.includes(" bandainferior"))
          // let data_setpoint=this.state.Mis_Prom.filter(l=>l.name.includes(" setpoint"))
          if(i==0){
            return (
              <ListGroupItem  onClick={this.toggle} action data-event={index+m}>
              <UncontrolledTooltip placement="top-end" target={`oxd_divice2${l.id}`}>
              {!check?"valores porcentuales del día anterior a las 12:00 pm hasta la hora actual y valores  mensuales":
              "Promedio de sonda desde las 00:00 hasta hora actual"}
              </UncontrolledTooltip>
              {this.getColumParInp(l,i,largo,index,m,miscolores,data_par,data_inpar,c.name,data,data_superior,data_inferior,data_setpoint)}
              
              </ListGroupItem>
            )
          }
          
        }else{
          return (
            <ListGroupItem style={{backgroundColor:color}} action onClick={this.toggle} data-event={index+m}>
            <UncontrolledTooltip placement="top-end" target={`oxd_divice2${l.id}`}>
            {!check?"valores porcentuales del día anterior a las 12:00 pm hasta la hora actual y valores  mensuales":
            "Promedio de sonda desde las 00:00 hasta hora actual"}
            </UncontrolledTooltip>
            <a onClick={this.toggle} data-event={index+m}>
            <Row onClick={this.toggle} data-event={index+m}>
              <Col onClick={this.toggle} data-event={index+m}><i id={`oxd_divice2${l.id}`} style={{color:"black",size:20}} class="pe-7s-help1"></i> {this.getName(l.name)}</Col>
              {this.getColorCol(l.prom,l.min,l.max,"mg/L",miscolores[0],index+m,l.ultimo_valor,l.id,l.locationId,m)}
            </Row>
            </a>
            </ListGroupItem>
          )
        }
        
    })}
    </Collapse>
    </Fragment>
  }

  onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
  }

  filterEvent(workplace){
    //let val=this.state.Events.filter(c=>c.workplaceId==workplace)
    this.tagservices.getfindEvent(workplace).then(respuesta=>{
      this.setState({Centro_Actividad:respuesta.filter(this.onlyUnique)})
    })
    .catch(e=>{
      //console.log(e)
    })
    
  }

  mostrar(){
    let mostrar="none"
    if(this.state.Abiotic || this.state.Control){
      mostrar="block"
    }
    return mostrar
  }

  // getEfectividadGlobal(type){
  //   let efectividades=[];
  //   let efectividadesMonth=[];
  //   this.state.Centros2.filter(c=>{
  //     if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
  //       return c.areaId==Buscar
  //     }
  //     if(this.state.Control){
  //       return c.name.includes(' Control')
  //     }
  //     if(Buscar=="" && this.state.Control==false){
  //       return !c.name.includes(' Control')
  //     }
  //   }).map(c=>{
  //     this.state.Mis_Prom2.filter(m=>!m.name.includes('Jaula ')&&m.name.includes('oxd')&&m.centroId==c._id).map(m=>{
  //       let efectividad=this.getFindEfectividad(m.id);
  //       let efectividadMonth=this.getFindEfectividadMonth(m.locationId)
  //       //efectividades.push(efectividad);
  //       if(efectividad!=undefined){
  //         efectividades.push(efectividad)
  //       }else{
  //         efectividades.push({porcentaje_efectividad:0,count:0})
  //       }
  //       if(efectividadMonth!=undefined){
  //         efectividadesMonth.push(efectividadMonth)
  //       }else{
  //         efectividadesMonth.push({porcentaje:0,count:0})
  //       }
  //     });
  //   })
  //   // console.log(efectividadesMonth)
  //   // let start_date=moment().format('YYYY-MM-DDT00:00:00');
  //   // console.log(`start_date ${start_date}, end_date hoy`)
  //   // let f1=new Date(start_date);
  //   // let f2=new Date();
  //   // var diff = f2 - f1;
  //   // diff=(diff/60000)/5;
  //   // console.log(diff)
  //   let diferencia_registros100=diferencia_fechas*288;
  //   try{
  //     let efec_month=efectividadesMonth.filter(e=>e.start_date<moment(new Date()).format('YYYY-MM-DDT00:00:00'))
  //     //console.log(efec_month)
  //     console.log(efectividadesMonth)
  //     let ultimo_valor=type=="month"?((efectividadesMonth.reduce((a, b) => +a + +(b.porcentaje), 0)/efectividadesMonth.length)).toFixed(1):
  //                                    ((efectividades.reduce((a, b) => +a + +b.porcentaje_efectividad, 0)/efectividades.length)).toFixed(1);
  //     // if(ultimo_valor>=100){
  //     //   ultimo_valor=100;
  //     // }
  //     aux_efectividad_global=false;
  //     return ultimo_valor+"%"
  //   }catch(e){
  //     //return this.state.GEfectividadDay+"%"
  //   }
  // }

  filtrosRapidos(tipo){
   this.setState({btn_filtro:tipo,btn_filtro2:"",btn_filtro3:""});
  }
  filtrosRapidos2(tipo){
    this.setState({btn_filtro2:tipo,btn_filtro3:""});
  }
  filtrosRapidos3(tipo){
    this.setState({btn_filtro3:tipo});
  }

  switchFiltroOrder(type){
    if(type=="up"){
      this.filtrosRapidos("down")
    }else if(type=="down"){
      this.filtrosRapidos("up")
    }
    
  }

  getNameFilter(type){
    if(type=="up"){
      return <Fragment><font size={2}> desc</font> <i style={{fontSize:20}} className="pe-7s-up-arrow"></i></Fragment>
    }else if(type=="down"){
      return <Fragment><font size={2}> asc</font> <i style={{fontSize:20}} className="pe-7s-bottom-arrow"></i></Fragment>
    }
  }

  getPromDayWorkplaceArrow(){
    let prom=this.state.EfectArrow;
    let p_hoy=((prom.reduce((a, b) => +a + +(b.medicion[0].prom), 0)/prom.length)).toFixed(1);
    let p_ayer=((prom.reduce((a, b) => +a + +(b.medicion[1].prom), 0)/prom.length)).toFixed(1);
    let arrow=this.getArrow(p_hoy,p_ayer);
    let icon=this.switchArrow(arrow,false);
    // let prom=this.state.Centros2.filter(c=>c.active==true);
    // console.log(this.state.Centros2)
    // let p=((prom.reduce((a, b) => +a + +(b.porcentaje), 0)/prom.length)).toFixed(1)
    //return  <i class="pe-7s-angle-down" style={{fontWeight:900}}></i>
    return <span className={icon} style={{fontSize:45,color:`${this.switchArrow(arrow,true)}`,fontWeight:900}}></span>
    //arrow+"%"+`-${p_ayer}-${p_hoy}` 
    //{`-${p_ayer}-${p_hoy}` }
  }

  getGlobalEfectivity(type,esperado){
    // console.log(Centros2)
    //console.log(this.state.Centros2)
    let centros=this.state.Centros2.filter(c=>c.active==true)
    let start_date=moment().startOf('month').format('YYYY-MM-DDT00:00:00');
    if(esperado=="esperado"){
      centros=centros.filter(c=>c.endDate!=undefined&&c.endDate>=start_date)
    }
    centros=centros.filter(c=>{
        if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
          return c.areaId==Buscar
        }
        if(this.state.Control){
          return c.name.includes(' Control')
        }
        if(Buscar=="" && this.state.Control==false){
          return !c.name.includes(' Control')
        }
    });
   
    //console.log(centros)
    let data=[];
    centros.map(c=>{
      let find=this.getPromCentro(c._id,"oxd","month","",type);
      data.push({"workplaceId":c._id,"prom":find})
    })
    //console.log(data)
    let prom=((data.reduce((a, b) => +a + +(isNaN(b.prom)?0:b.prom), 0)/data.length)).toFixed(1)
    if(isNaN(prom)){
      return "..."
    }else{
      return prom+" %"
    }
  }

  getCardPrincipal(){
    return <Fragment><Col md={3}>
      <Card>
        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
            <a style={{cursor:"pointer"}} onClick={(()=>{
                this.toggleModal1(true,"General","General")
              })}>
              <div class="icon-wrapper rounded-circle">
                  <div className={`icon-wrapper-bg opacity-9 bg-${"warning"}`} ></div>
                  <i class={"pe-7s-global"}></i>
              </div>
            </a>
            <div class="widget-chart-content">
                <div class="widget-subheading"><font ><font >{"Efectividad Centros Conectados"}</font></font></div>
                <div class="widget-numbers"><span><font ><font >{this.state.Mis_Prom.length>0?this.getGlobalEfectivity("day","esperado"):"..."}</font></font></span></div>
                <div class="widget-description opacity-8 text-focus">
                <UncontrolledTooltip placement="top-end" target={`promGeneralDia`}>
                  {!check?"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 12:00 pm del día anterior"
                  :"Promedio de oxígeno disuelto de las sondas instaladas en centro desde las 00:00 hasta la hora actual"}
                </UncontrolledTooltip>
                <i id={`promGeneralDia`} style={{color:"black",size:20,marginLeft:-10}} class="pe-7s-help1"></i>{"Efectividad General del Día"}
                    <span class="text-info pl-1">
                        <i class="fa fa-angle-down"></i>
                        <span class="pl-1">{this.state.Mis_Prom.length>0?this.getGlobalEfectivity("day"):"..."}</span>
                    </span>
                </div>
            </div>
            <div class="widget-numbers"><span><font ><font >{this.getPromDayWorkplaceArrow()}</font></font></span></div>
        </div>

        {/* <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
            <div class="icon-wrapper rounded-circle">
                <div class="icon-wrapper-bg opacity-9 bg-danger"></div>
                <i class="lnr-graduation-hat text-white"></i>
            </div>
            <div class="widget-chart-content">
                <div class="widget-subheading">Invested Dividents</div>
                <div class="widget-numbers"><span>9M</span></div>
                <div class="widget-description opacity-8 text-focus">
                    Grow Rate:
                    <span class="text-info pl-1">
                        <i class="fa fa-angle-down"></i>
                        <span class="pl-1">14.1%</span>
                    </span>
                </div>
            </div>
        </div> */}
      </Card>
    </Col>
    <Col md={3}>
      <Card>
        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
            <a style={{cursor:"pointer"}} onClick={(()=>{
                // this.toggleModal1(true,"General","General")
              })}>
              {/* <div class="icon-wrapper rounded-circle">
                  <div className={`icon-wrapper-bg opacity-9 bg-${"warning"}`} ></div>
                  <i class={"pe-7s-global"}></i>
              </div> */}
            </a>
            <div class="widget-chart-content">
                <div class="widget-subheading"><font ><font >{"Efectividad Centros Conectados" }</font></font></div>
                <div class="widget-numbers"><span><font ><font >{this.state.Mis_Prom.length>0?
                //this.getEfectividadGlobal("month")
                this.getGlobalEfectivity("month","esperado")
                :"..."}</font></font></span></div>
                <div class="widget-description opacity-8 text-focus">
                Efectividad General del Mes
                    <span class="text-info pl-1">
                        <i class="fa fa-angle-down"></i>
                        <span class="pl-1">{this.state.Mis_Prom.length>0?this.getGlobalEfectivity("month"):"..."}</span>
                    </span>
                </div>
            </div>
        </div>
      </Card>
    </Col>
    <Col md={3}>
      {/* <Card>
        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
            <a style={{cursor:"pointer"}} onClick={(()=>{
                
              })}>
             
            </a>
            <div class="widget-chart-content">
                <div class="widget-subheading"><font ><font >{"Efectividad General del Día Esperado"}</font></font></div>
                <div class="widget-numbers"><span><font ><font >{this.state.Mis_Prom.length>0?this.getGlobalEfectivity("day","esperado"):"..."}</font></font></span></div>
            </div>
            <div class="widget-numbers"><span><font ><font >{this.getPromDayWorkplaceArrow()}</font></font></span></div>
        </div>
      </Card> */}
    </Col> 
    <Col md={3}>
      <Card>
        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
            <a  onClick={(()=>{
                // this.toggleModal1(true,"General","General")
              })}>
              <div class="icon-wrapper rounded-circle">
                  <div className={`icon-wrapper-bg opacity-9 bg-${"light"}`} ></div>
                  <i class={"pe-7s-config"} style={{color:"black"}}></i>
              </div>
            </a>
            <div class="widget-chart-content">
                <div class="widget-subheading"><font ><font >{"Herraminetas de filtrado"}</font></font></div>
                <div class="widget-numbers">
                      <font ><font >
                      <span class="badge badge-pill badge-primary" style={{cursor:"pointer",fontSize:20}} onClick={(()=>{
                        if(this.state.btn_filtro=="up" || this.state.btn_filtro=="down"){
                          this.switchFiltroOrder(this.state.btn_filtro)
                        }
                      })}>{this.getNameFilter(this.state.btn_filtro)}</span> 
                      <span class="badge badge-pill badge-secondary" style={{marginLeft:10,cursor:"pointer",fontSize:20}} onClick={(()=>{this.filtrosRapidos2("alfabetic")})}><font size={2}> </font> <i style={{fontSize:20}} >A-Z</i></span>
                      <span class="badge badge-pill badge-alternate" style={{marginLeft:10,cursor:"pointer",fontSize:20}} onClick={(()=>{this.filtrosRapidos3("crea")})}><font size={2}> creacion</font> <i style={{fontSize:20}} className="pe-7s-timer"></i></span>
                      </font></font>
                </div>
            </div>
        </div>
      </Card>
    </Col>
    </Fragment>
    // return <Col md="4">
    //   <Card className="main-card mb-5 mt-3"  >
    //   <Row style={{height:25,fontWeight: 'bold' }}>
    //     <Col>Status General</Col>
    //     <Col>Promedio</Col>
    //   </Row>
    //   <Row style={{height:25}}>
    //     <Col></Col>
    //     <Col>Promedio</Col>
    //   </Row>
    //   </Card>
    // </Col>
  }

  efectivityCenterArray(array,miscolores){
    let centros=[];
    array.map(c=>{
      //let efectividades=this.state.Efectividad.filter(e=>e.workplaceId==c._id)
      let efectividades=this.state.Control?this.state.Efectividad2.filter(e=>e.workplaceId==c._id):this.state.Efectividad.filter(e=>e.workplaceId==c._id)
      c.porcentaje=Math.round(efectividades.length>0?((efectividades.reduce((a, b) => +a + +(b.porcentaje), 0)/efectividades.length)).toFixed(1):0)
      c.efectividades=efectividades
      centros.push(c)
    })
    //console.log(centros)
    return array
  }

  switchArrow(type,color){
    if(color){
      if(type==="up"){
        return "#007bff"
        //"blue"
      }else if(type==="down"){
        return "#dc3545"
        //"red"
      }else if(type=="right"){
        return "#ffc107"
      }else{
        return "#ffc107"
      }
    }else{
      if(type==="up"){
        return icon_up
      }else if(type==="down"){
        return icon_down
      }else if(type=="right"){
        return icon_right
      }else{
        return icon_right
      }
    }
  }

  getArrow(v1,v2){
    v1=v1>100?100:v1;
    v2=v2>100?100:v2;
    if(v1>v2){
      return "up"
    }
    else if(v1==v2){
      return "right"
    }else{
      return "down"
    }
  }

  getTendenciaEfectividad(workplaceId){
    let efect=this.state.EfectArrow.filter(e=>e.workplace==workplaceId)[0]
    let f1="",f2="",f3="",f4="";
    let t1="",t2="",t3="";
    try{
      //console.log(efect.medicion[0].prom>efect.medicion[1].prom)
      f1=this.getArrow(this.getPromCentro(workplaceId," oxd","","","day"),efect.medicion[2].prom);
      f2=efect.medicion[1].tendencia;
      f3=efect.medicion[2].tendencia;
      
      t1=this.switchArrow(f1,false);
      t2=this.switchArrow(f2,false);
      t3=this.switchArrow(f3,false);
      
      return  <Fragment>
              <Badge pill color={"Success"}>
                Tendencia
              <br></br>
              <br></br>
              <b>
              {/* {this.createTooltip("arrow3"+workplaceId,`${efect.medicion[0].fecha}-${efect.medicion[1].fecha}`)}
              {this.createTooltip("arrow2"+workplaceId,`${efect.medicion[1].fecha}-${efect.medicion[2].fecha}`)}
              {this.createTooltip("arrow1"+workplaceId,`${efect.medicion[2].fecha}-${efect.medicion[3].fecha}`)} */}
              <a id={"arrow1"+workplaceId} style={{fontSize:20}} ><span className={t3} style={{color:`${this.switchArrow(f3,true)}`,fontWeight:900}}></span></a>
              <a id={"arrow2"+workplaceId} style={{fontSize:20}} ><span className={t2} style={{color:`${this.switchArrow(f2,true)}`,fontWeight:900}}></span></a>
              <a id={"arrow3"+workplaceId} style={{fontSize:20}} ><span className={t1} style={{color:`${this.switchArrow(f1,true)}`,fontWeight:900}}></span></a>
              {/* <Button>{`${efect.medicion[0].prom}-${efect.medicion[0].fecha}`}</Button> */}
              {/* <Button>{`${efect.medicion[0].prom}-${efect.medicion[1].prom}-${efect.medicion[2].prom}-${this.getPromCentro(workplaceId," oxd","","","day")}`}</Button> */}
              {/* <span  className={t4}></span> */}
              </b>
              </Badge>
            </Fragment>
    }catch(e){
      //algo sucedera
    }    
  }

  getCardCentros(miscolores,collapse){
    let centros=this.efectivityCenterArray(this.state.Centros2,miscolores);
    if(this.state.btn_filtro3!="crea"){
      if(this.state.btn_filtro2=="alfabetic"){
        centros=_.orderBy(centros,["name"],["asc"])
      }else if(this.state.btn_filtro=="up"){
        centros=_.orderBy(centros,["porcentaje"],["asc"])
      }else if(this.state.btn_filtro=="down"){
        centros=_.orderBy(centros,["porcentaje"],["desc"])
      }
    }else{
      centros=_.orderBy(centros,["code"],["asc"])
    }
    
    
    centros=centros.filter(c=>{
        if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
          return c.areaId==Buscar
        }
        if(this.state.Control){
          return c.name.includes(' Control')
        }
        if(Buscar=="" && this.state.Control==false){
          return !c.name.includes(' Control')
        }
    })
    //console.log(centros.filter(c=>c.active==true).map(c=>c.porcentaje))
    return centros.map((c,index)=>{
        if(index==0 && aux_efectividad){
          //this.getEfectividad();
        }
        let fontsize=12;
        let fontsize_date=2;
        let size1=8;
        let size2=4;
        if (window.screen.width > 1729){
          size1=8;
          size2=4;
          fontsize=18;
          fontsize_date=3;
        }
        return  <Col md="4"  style={{display:this.findActiveWorkplace(c._id)>0?"block":"none"}}>
                    <Card className="main-card mb-5 mt-3" key={index} >
                        {/* {console.log(centros)} */}
                        <Row>
                          <Col>
                            
                          </Col>
                          <Col>
                            
                          </Col>
                          <Col style={{marginLeft:`${60}%`}}>
                            {this.createTooltip(`event_${c._id}`,"Ver Eventos")}
                            <i id={`event_${c._id}`} class="pe-7s-bell" style={{fontSize:20,cursor:"pointer",color:`${c.event==true?"orange":"silver"}`}} onClick={(()=>{
                              this.filterEvent(c._id)
                              this.toggleModal2(true,c._id,c.name)
                            })}></i>
                            <i class="pe-7s-config" style={{fontSize:20,cursor:"pointer",color:"silver",marginLeft:10}} onClick={(()=>{
                              
                              this.toggleModal3(true,c._id,c.name)
                              //this.ingresarCentroUsuario(`${c._id}/${c.name}`);
                              //this.setState({redirect2:true})
                            })}></i>
                          </Col>
                        </Row>
                        <CardHeader className="card-header-tab" row>
                          <Col md={size1} style={{marginLeft: -20}}>
                          <div class="card no-shadow rm-border bg-transparent widget-chart text-left" >
                            {this.createTooltip(`detalle${c._id}`,"Mas Detalles de Centros")}
                                  <a style={{cursor:"pointer"}} onClick={(()=>{
                                    //sessionStorage.setItem('workplace', JSON.stringify({"id":c._id,"name":c.name,"active":c.active}));
                                    //this.ingresarCentroUsuario(`${c._id}/${c.name}`);
                                    this.toggleModal1(true,c._id,c.name)
                                    //this.setState({redirect:true})
                                  })} id={`detalle${c._id}`}>
                                  <div class="icon-wrapper rounded-circle" style={{marginTop:-10}}>
                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                      <i class="pe-7s-display1 text-white"></i>
                                  </div>
                                  </a>
                                  <div class="widget-chart-content font-size-lg font-weight-normal" >
                                    <p style={{fontSize:fontsize}}>{c.name} <br></br>
                                      <font size={fontsize_date}><b>{!this.state.Equipos?this.endDate(c._id):this.endDate2(c._id)}</b></font>
                                    </p>
                                  </div>
                            </div>
                          </Col>
                          <Col style={{marginLeft:-100}}>
                            {this.getTendenciaEfectividad(c._id)}
                          </Col>
                          <Col md={size2}>
                            <Row style={{marginTop:10, alignItems: 'center'}}>
                              <Col md={6}>
                                <font color={miscolores[8]}>Día:<br></br> {this.getPromCentro(c._id," oxd",miscolores[0],"","day")}</font>
                              </Col>
                              <Col md={6}>
                                <font color={miscolores[8]}>Mes:<br></br> {this.getPromCentro(c._id," oxd",miscolores[0],"","month")}</font>
                                {/* <Badge color="primary" style={{fontSize:20,cursor:"pointer"}} pill><i className="pe-7s-display1"></i></Badge> */}
                              </Col>
                            </Row>
                          </Col>
                        {/* <div className="btn-actions-pane-center text-capitalize">
                           
                        </div> */}
                        {/* <div className="btn-actions-pane-right text-capitalize">
                            {/* {c.endDate} 
                            
                        </div>                            */}
                        </CardHeader>
                        <ListGroup style={{cursor:"pointer"}}>
                        <div style={{display:`${this.mostrar()}`}}>
                              {
                                this.state.Modulos.filter(m=>m.centro==c.name).map((m)=>{
                                  //console.log(object)
                                  let zonas = m.zonas;
                                  return zonas.map((z)=>{
                                    let modul=z.replace("Modulo ","")
                                    //console.log(modul)
                                    return this.getAcordion(index,miscolores,collapse,c,modul)
                                  })
                                })
                              }
                          </div>
                          <div style={{display:`${this.state.Meteo?"block":"none"}`}}>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Viento</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Velocidad</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+500}>Dirección del Humedad</Col>
                                  <Col></Col>
                                  <Col onClick={this.toggle} data-event={index+500}><span className="lnr-arrow-up"></span> 8.5</Col>
                                </Row>
                              </ListGroupItem>
                          </div>
                          <div style={{display:`${this.state.Proceso?"block":"none"}`}}>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+600}>Combustible</Col>
                                  <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                  <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                  <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+600)}>
                                <ListGroupItem tag="a" action>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+600}>Flujometro Abducción</Col>
                                    <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                  </Row>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+600}>Flujometro Retorno</Col>
                                    <Col onClick={this.toggle} data-event={index+600}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+600}>max</Col>
                                  </Row>
                                </ListGroupItem>
                              </Collapse>
                              <ListGroupItem style={{backgroundColor:"#F2F4F4"}} tag="a" action>
                                  <Row>
                                    <Col >Consumo calculado</Col>
                                    <Col><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col>min</Col>
                                    <Col>max</Col>
                                  </Row>
                              </ListGroupItem>
                          </div>
                          <div style={{display:`${this.state.Equipos?"block":"none"}`}}>
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}}  action onClick={this.toggle} data-event={index+400}>
                                <Row>
                                  <Col onClick={this.toggle} data-event={index+700}>Combustible</Col>
                                  <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                  <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                  <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                </Row>
                              </ListGroupItem>
                              <Collapse isOpen={collapse === (index+700)}>
                                <ListGroupItem tag="a" action>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+700}>Flujometro Abducción</Col>
                                    <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                  </Row>
                                  <Row>
                                    <Col onClick={this.toggle} data-event={index+700}>Flujometro Retorno</Col>
                                    <Col onClick={this.toggle} data-event={index+700}><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>min</Col>
                                    <Col onClick={this.toggle} data-event={index+700}>max</Col>
                                  </Row>
                                </ListGroupItem>
                              </Collapse> */}
                              {/* <ListGroupItem style={{backgroundColor:"#F2F4F4"}} tag="a" action>
                                  <Row>
                                    <Col >Consumo calculado</Col>
                                    <Col><span className="lnr-arrow-up"></span> 8.5 Ltr.</Col>
                                    <Col>min</Col>
                                    <Col>max</Col>
                                  </Row>
                              </ListGroupItem> */}
                          </div>
                        </ListGroup>
                    </Card>

                </Col>
      })
  }

  getDetailActivity(activityId){
    let act=this.state.Actividad.filter(a=>a._id==activityId).map((a)=>{
        return a.detail
    })
    //console.log(act)
    return act[0]
  }

  getActivity(){
      axios
      //.post(API_ROOT+"/"+empresa+"/centros_activos",{
      .get(API_ROOT+`/activity/all`)
      .then(response => {   
          // alert(response.data.statusCode==200);
          if(response.data.statusCode==200){
              let actividades=response.data.data;
              let new_actividades=actividades.map((a,i)=>{
                  return {
                      ind:i+1,
                      _id:a._id,
                      detail:a.detail,
                      dateTime:moment(a.dateTime).format("HH:mm DD-MMM"),
                      visible:"block"
                  }
              })
              this.setState({Actividad:new_actividades})
          }
          
      })
      .catch(error => {
          //console.log(error)
      });
  }

  switchActive0(act_graph0,act_graph1,act_graph2){
    this.setState({act_graph0,act_graph1,act_graph2})
  }
  
  switchActive_principal(Abiotic,Meteo,Proceso,Equipos,Control){
    // if(Abiotic || Control){
    //   this.setState({Centros:[],Centros2:[]})
    //   this.getCentros();
    // }
    this.setState({Abiotic,Meteo,Proceso,Equipos,Control})
  }
  
  switchActiveModal(TypeGraphic2){
    this.setState({TypeGraphic2})
  }

  handleChangeActivity = event => {
      const name = event.target.name;
      const value = event.target.value;
      this.setState({ [name]: value });
  }

  handleChangeArea = event => {
      const name = event.target.name;
      const value = true;
      //console.log(name)
      areas_centros.map((a,i)=>{
        if(name=="btn"+i){
          this.setState({ [name]: value });
        }else{
          this.setState({ ["btn"+i]: false });
        }
        
      })
  }

  generateAreasCentros(){
    try{
      return areas_centros.map((area,i)=>{
        return ubicaciones.filter(u=>u._id==area).map((a)=>{
            return <Button value={a.name} name={"btn"+i} color="primary" onClick={((e)=>{
              Buscar=a._id;
              active_todos=false;
              this.handleChangeArea(e)
            })} onChange={e => this.handleChangeArea(e)} active={this.state["btn"+i]} outline>{a.name}</Button>
        })
      })
    }catch(e){
      //console.log(e)
    }
  }

  viewTendencia(name){
    let centros=this.state.Centros2.filter(c=>c.active==true)
    // if(esperado=="esperado"){
    //centros=centros.filter(c=>c.endDate!=undefined)
    // }
    centros=centros.filter(c=>{
        if(c.areaId==Buscar&&Buscar!=""&&c.active==true){
          return c.areaId==Buscar
        }
        if(this.state.Control){
          return c.name.includes(' Control')
        }
        if(Buscar=="" && this.state.Control==false){
          return !c.name.includes(' Control')
        }
    });
    if(this.state.changeModal=="lnr-apartment"){
      return <Tendencia_Efectividad Centro={_id_centro} 
        All_Centros={centros}
        Mis_Prom={this.state.Mis_Prom.filter(m=>!m.name.includes('VoltajeR'))}
      />
    }else{
      return  <Tendencia2  Centro={_id_centro} Ncentro={name}/>
    }
    
  }

  SwitchConfig(){
    if(this.state.config_type=="sma"){
      return <Config_SMA Centros={centros_data}></Config_SMA>
    }else if(this.state.config_type=="actividades"){
      return <Configuracion Centros={centros_data}></Configuracion>
    }else if(this.state.config_type=="config_limites"){
      let split=centros_data.split('/')
      return <ConfiguracionLimites 
                Centros={centros_data} 
                Mis_Prom={this.state.Mis_Prom.filter(m=>m.centroId==split[0])}
                Modulos={this.state.Modulos.filter(m=>m.centro==split[1])}></ConfiguracionLimites>
    }                                     
  }

    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        // if(this.state.redirect2){
        //   return <Redirect to="/dashboards/configuracion"></Redirect>
        // }
        const {cards, collapse, isLoading} = this.state;
        let font={fontSize:"0.8rem",fontWeight: 500,fontSize:12.5}
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
             
         ///***********************************+++ */
        return (
            <Fragment>
                     {/* {console.log("wena :"+JSON.stringify(this.state.WorkplaceDateInit))} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Herramientas</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      <BreadcrumbItem active tag="span">Efectividad</BreadcrumbItem>
                      {/* <BreadcrumbItem active tag="span" style={{display:active_config,cursor:"pointer"}}><a onClick={(()=>{
                          this.setState({redirect2:true})
                      })}><Badge color="secondary" pill><i class="pe-7s-settings"></i>  Configuracion</Badge></a></BreadcrumbItem> */}
                      
                      </Breadcrumb>
                       
                        <Button active={active_todos} name={"btn-1"} color="primary" outline onClick={((e)=>{
                          Buscar="";
                          active_todos=true;
                          this.handleChangeArea(e)
                        })} onChange={e => this.handleChangeArea(e)} >Todos</Button>
                        {
                            this.generateAreasCentros()
                            
                        }
                        
                      <div style={{display:this.state.active_grafic}}>
                        <ButtonGroup size="sm" style={{marginTop:-50}}>
                        <UncontrolledTooltip placement="top-end" target="UncontrolledTooltipExample">
                          Grafico de promedios Generales
                              (Oxd, Temp, Oxs, Sal)
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Oxd">
                          Promedios de Oxigeno Disuelto
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Temp">
                          Promedios de Temperatura
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Oxs">
                          Promedios de Oxigeno Saturado
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="top-end" target="promedios_Sal">
                          Promedios de Salinidad
                        </UncontrolledTooltip>
                          <Button id="UncontrolledTooltipExample" color="primary"  active={this.state.act_all} onClick={()=>{
                            this.switchActive(true,false,false,false,false)
                          }}>All</Button>
                          <Button id="promedios_Oxd" color="primary" active={this.state.act_oxd} onClick={()=>{
                            this.switchActive(false,true,false,false,false)
                          }}>Oxd</Button>
                          <Button id="promedios_Temp" color="primary" active={this.state.act_temp} onClick={()=>{
                            this.switchActive(false,false,true,false,false)
                          }}>Temp</Button>
                          <Button id="promedios_Oxs" color="primary" active={this.state.act_oxs} onClick={()=>{
                            this.switchActive(false,false,false,true,false)
                          }}>Oxs</Button>
                          <Button id="promedios_Sal" color="primary" active={this.state.act_sal} onClick={()=>{
                            this.switchActive(false,false,false,false,true)
                          }}>Sal</Button>
                        </ButtonGroup>
                        
                        {/* {this.getChart()} */}
                      </div>
                     <Row>
                     
                       {/* {console.log(this.state.Mis_Prom)} */}
                     <Col md="12">
                            <Card className="main-card mb-1 p-0">
                            <CardHeader className="card-header-tab" style={{height:50}}>
                                  <div style={{marginLeft:-20}}>
                                                          
                                      <Button 
                                      className="widget-chart widget-chart-hover p-0 p-0  btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Abiotic?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                          this.switchActive_principal(true,false,false,false,false);
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Abiotic?"#6c757d":"#fff"}>&nbsp;&nbsp;Abióticas&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} opacity-1 */}
                                          </div>
                                      </Button>  
                                      <Button 
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" disabled style={{backgroundColor:`${!this.state.Meteo?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal(false,true,false,false,false);
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Meteo?"#6c757d":"#fff"}>&nbsp;&nbsp;Meteorologicas&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} opacity-1 */}
                                              </div>
                                      </Button> 
                                  
                                      <Button 
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" disabled style={{backgroundColor:`${!this.state.Proceso?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal(false,false,true,false,false);
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Proceso?"#6c757d":"#fff"}>&nbsp;&nbsp;Proceso&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} */}
                                              </div>
                                      </Button>  

                                      <Button 
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Equipos?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal(false,false,false,true,false);
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Equipos?"#6c757d":"#fff"}>&nbsp;&nbsp;Equipos&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} */}
                                              </div>
                                      </Button>
                                      <Button 
                                      disabled={false}
                                      className="widget-chart widget-chart-hover  p-0 p-0 btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Control?"":"#545cd8"}`}} outline color={miscolores[0]}
                                      onClick={()=>{
                                        this.switchActive_principal(false,false,false,false,true);
                                      }}>  
                                          {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                          <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                          {/* {data.measurements[data.measurements.length-1].value   }  */}
                                          <span className="opacity-6  pl-0 size_unidad">  </span>
                                          <font style={font} color={!this.state.Control?"#6c757d":"#fff"}>&nbsp;&nbsp;Control&nbsp;&nbsp;</font>
                                          </div>
                                          <div className="widget-subheading">
                                              {/* {data.shortName} */}
                                              </div>
                                      </Button>      
                                  </div>
                                </CardHeader>
                                <CardBody className="p-3"  >
                                {/* {console.log(this.state.Mis_Prom)} style={{display:`${this.state.Control?"none":"block"}`}} */}
                                    <div style={{display:`${!this.state.Control?"none":"block"}`}}>
                                        <span className="badge badge-pill badge-info" style={{backgroundColor:"#007bff"}}>Azul</span>
                                        &nbsp;Niveles normales de oxigeno.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FFFF00",color: "#007bff"}}>Amarillo</span>
                                        &nbsp;Ox. tendiente a la baja.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF8000",color:"#fff"}}>Naranjo</span>
                                        &nbsp;Nivel Critico Ox.&nbsp;
                                        <span class="badge badge-pill badge-info" style={{backgroundColor:"#FF0000",}}>Rojo</span>
                                        &nbsp;Ox. bajo nivel alarma.&nbsp;
                                    </div>
                                    <Row>
                                      {this.getCardPrincipal()}
                                    </Row>
                                    <Row >
                                      {this.getCardCentros(miscolores,collapse)}
                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                    </Row>

                      <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={ModalActive3} >
                          <BlockUi tag="div" blocking={this.state.blocking3} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal3(false)}>
                                        {this.createTooltip(`changeModal`,"Cambio de Tendencia")}
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <a style={{cursor:"pointer"}} onClick={(()=>{
                                                                        // this.state.changeModal=="lnr-apartment"?
                                                                        // this.setState({changeModal:"pe-7s-display1"}):
                                                                        this.setState({changeModal:"lnr-apartment"})
                                                                      })}>
                                                                  <div id={`changeModal`} class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class={`${this.state.changeModal} text-white`}></i>
                                                                  </div>
                                                                  </a>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                       <ButtonGroup style={{marginLeft:10}}>
                                         <Button color={"primary"} outline={this.state.config_type=="actividades"?false:true} onClick={(()=>{
                                           this.setState({config_type:"actividades"})
                                        })}>Actividades</Button>
                                         <Button color={"primary"} outline={this.state.config_type=="sma"?false:true} onClick={(()=>{
                                           this.setState({config_type:"sma"})
                                         })}>SMA</Button>
                                         <Button color={"primary"} outline={this.state.config_type=="config_limites"?false:true} onClick={(()=>{
                                           this.setState({config_type:"config_limites"})
                                         })}>Configuración de Limites</Button>
                                       </ButtonGroup>
                                        <ModalBody >
                                          {this.SwitchConfig()}
                                          {/* {this.viewTendencia(title_centro)} */}
                                      </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>

                      <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={ModalActive2} >
                          <BlockUi tag="div" blocking={this.state.blocking2} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal2(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                                <ButtonGroup>
                                                <Input placeholder="Evento..." value={Buscar2} onChange={((e)=>{
                                                  Buscar2=e.target.value;
                                                  this.handleChangeActivity(e)
                                                })}></Input>
                                                <Button color="secondary" disabled>Buscar</Button>
                                                </ButtonGroup>
                                                
                                                <ReactTable                                                           
                                                data={_.orderBy(this.state.Centro_Actividad,["dateTime"],["desc"]).filter((data)=>{
                                                  if(Buscar2!=""){
                                                      return data.detail.toLocaleUpperCase().includes(Buscar2.toLocaleUpperCase())
                                                  }else{
                                                    return data
                                                  }
                                                }).map((c,i)=>{
                                                  return {
                                                      ind:i+1,
                                                      _id:c._id,
                                                      detail:c.detail,
                                                      type:this.getDetailActivity(c.activityId),
                                                      dateTime:moment(c.dateTime).format("HH:mm DD-MMM"),
                                                      workplaceId:c.workplaceId,
                                                      activityId:c.activityId,
                                                      active:c.active,
                                                  }
                                              })}
                                                            
                                                // loading= {false}
                                                showPagination= {true}
                                                showPaginationTop= {false}
                                                showPaginationBottom= {true}
                                                showPageSizeOptions= {false}
                                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                defaultPageSize={10}
                                                columns={[
                                                    {
                                                        Header: "n°",
                                                        accessor: "ind",
                                                        width: 40
                                                        },
                                                        {
                                                        Header: "fecha Reg.",
                                                        accessor: "dateTime",                                                              
                                                        width: 100
                                                        },
                                                        {
                                                          Header: "asunto",
                                                          width: 350,
                                                          Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={"info"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.type}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        {
                                                        Header: "detalle",
                                                        accessor: "detail",                                                              
                                                        width: `${78}%`
                                                        },
                                                        {
                                                        Header: "estado",                                                             
                                                        width: 150,
                                                        Cell: ({ row }) => (
                                                            <Fragment>
                                                                <Badge color={row._original.active?"warning":"success"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                                    //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                                })} pill>{row._original.active?"activo":"finalizado"}</Badge>
                                                            </Fragment>
                                                            )
                                                        },
                                                        // {
                                                        // Header: "Accion",
                                                        // id: "id",
                                                        // width: 100,
                                                        // Cell: ({ row }) => (
                                                        //     <Fragment>
                                                        //         <Badge color="warning" style={{marginLeft:0}} onClick={((e)=>{
                                                        //             //this.updateEvent(row._original.id,true,row._original.workplaceId)
                                                        //         })} pill>
                                                        //         <i class="pe-7s-attention" 
                                                        //         style={{display:active_config,cursor:"pointer",fontSize:20}}></i></Badge>&nbsp;
                                                        //         <Badge color="success" pill onClick={((e)=>{
                                                        //             //this.updateEvent(row._original.id,false,row._original.workplaceId)
                                                        //             //this.switchModal("1")
                                                        //         })}><i style={{fontSize:20,cursor:"pointer"}} className="pe-7s-check                                                                " ></i></Badge>
                                                        //     </Fragment>
                                                        //     )
                                                        // },
                                                    ]                                                             
                                                }
                                                 
                                                className="-striped -highlight"
                                                />
                                          </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>
                      <Modal style={{maxHeight:660,maxWidth:`${80}%`}} isOpen={ModalActive} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1(false)}>
                                        {this.createTooltip(`changeModal`,"Cambio de Tendencia")}
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <a style={{cursor:"pointer"}} onClick={(()=>{
                                                                        this.state.changeModal=="lnr-apartment"?
                                                                        this.setState({changeModal:"pe-7s-display1"})
                                                                        :this.setState({changeModal:"lnr-apartment"})
                                                                      })}>
                                                                  <div id={`changeModal`} class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class={`${this.state.changeModal} text-white`}></i>
                                                                  </div>
                                                                  </a>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                          {this.viewTendencia(title_centro)}
                                      </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>
        
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({
  setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Resumen2);
