import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import {
  toast
} from 'react-toastify';
import { CSVLink } from "react-csv";
import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Form, Label, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,
    Modal, ModalHeader, ModalBody, ModalFooter,CardTitle,Badge,ListGroup,ListGroupItem} from 'reactstrap';
import moment, { relativeTimeThreshold } from 'moment';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';

import ReactTable from "react-table";

import {connect} from 'react-redux';
import { setBackgroundColor } from '../../../../reducers/ThemeOptions';
import CardFooter from 'reactstrap/lib/CardFooter';
import Tendencia from '../../Tendencia/ComponentTendencia/Tendencia/Tendencia'
import Toast from 'reactstrap/lib/Toast';
import Swal from 'sweetalert2'
import * as _ from "lodash";

//let Seleccion={fecha_inicio:"",fecha_fin:"",centro:"",centro_id:"",sensor:"",sensor_id:"",variable:""};
let disabled=false;
let filtro_alarma="";
let _id_centro="";
let title_centro="";
let tag_="";
let ModalActive2="";
let title_disp="";

//let Seleccionadas=[];


class Alarmas extends Component {

    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          NameWorkplace:"",
          blocking: true,
          loaderType: 'ball-triangle-path',
          modal1: false,
          modal2: false,
          modal3: false,
          blocking1:false,
          blocking3:false,
          Alarms:[],
          Workplace:[],
          TagsSelecionado:null,
          options:[
            {name:"todos".toUpperCase(),_id:"todos"},
            {name:"falla".toUpperCase(),_id:"falla"},
            {name:"alarma".toUpperCase(),_id:"alarma"}],
          txtSeleccion:"falla",
          txtStatus:"",
          en_vivo:false,
          check:true,
          filtroEmpresa:"all",
          filtroCentro:"all",
          filtroStatus:"all",
          ModalActive2:false,
          Tags:[],
          selectAction:false,
        };     
        this.tagservices = new TagServices();   
        this.handleChange =this.handleChange.bind(this);  
        this.getAlarmasTendencia =this.getAlarmasTendencia.bind(this);
        this.onVivo =this.onVivo.bind(this);
        this.applyCallback =this.applyCallback.bind(this);
        this.timeRef=React.createRef();
    }

  
    componentDidMount = () => {
        let url=window.location.href.split('/')
        filtro_alarma=url[url.length-1];
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        this.getAlarmasTendencia();
        let minutos=1;
        let segundos=60*minutos;
        let intervaloRefresco =segundos*1000;
        this.timeRef.current = setInterval(this.onVivo.bind(this),intervaloRefresco);
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
      }

    componentWillUnmount = () => {
      clearInterval(this.getAlarmasTendencia);
      clearInterval(this.timeRef.current);
   }
    
    componentWillReceiveProps(nextProps){
      if (nextProps.initialCount && nextProps.initialCount > this.state.count){
        this.setState({
          count : nextProps.initialCount
        });
      }
      //alert(JSON.stringify(nextProps));
      let separador=nextProps.centroUsuario;
      this.getAlarmasTendencia();
      let centro=separador.split('/');
      this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
      //this.getSondas(centro[0].toString());
    }

    toggleModal2(ModalState,_id,title,tipo,tag) {
      tag_=[];
      // if(!ModalState){
      //   //Buscar2=""
      //   this.setState({ModalState,Centro_Actividad:[]})
      // }
      ModalActive2=ModalState;
      this.setState({ModalState})
      _id_centro=_id;
      title_centro=title;
      tag_=tag;
    }

    // getAlarmsDay(){
    //   let accion="true"
    //   this.tagservices.getAlarm(accion).then(respuesta=>{
    //     if(respuesta.data.statusCode===201 || respuesta.data.statusCode===200){
    //       let data=respuesta.data.data;
    //       if(filtro_alarma!="all"){
    //         data=data.filter(d=>d.type==filtro_alarma)
    //       }
    //       this.setState({Alarms:data})
    //       let workplaces=data.map(a=>{return ({"_id":a.workplaceId})})
    //       console.log(workplaces)
    //       this.tagservices.getworkplaces_array(workplaces)
    //       .then(respuesta=>{
    //         this.setState({Workplace:respuesta.data.data})
    //       })
    //       .catch(e=>e)
    //     }
    //   }).catch(e=>e)
    // }

    getCodeTendencia(){
      this.tagservices.getCodeAlarms(this.state.txtCodigo)
      .then(response=>{
        //console.log(response.data.statusCode)
        if(response.data.statusCode==201 || response.data.statusCode==200){
            // tabsContent=[];
            disabled=false;
            console.log(response.data.data)
            this.setState({Alarms:response.data.data})
            let data=response.data.data
            let workplaces=data.map(a=>{return ({"_id":a.workplaceId})})
            // console.log(workplaces)
            if(workplaces.length>0){
              try{
                this.tagservices.getWorkplaces_array(workplaces)
                .then(respuesta=>{
                  this.setState({Workplace:respuesta.data.data})
                })
              }catch(e){
                //console.log(e)
              }
            }
        }else{
            console.log("status 500")
        }
    }).catch(e=>e)
    }

    getAlarmasTendencia(){
      // let fini=moment().format('YYYY-MM-01T00:00:00');
      // let ffin=moment().format('YYYY-MM-DDTHH:mm:ss');
      // console.log(this.state.txtSeleccion)
      let f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss");
      let f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss");
      let tendencia=null
      console.log(this.state.txtCodigo!=undefined)
      // if(this.state.txtCodigo!=undefined){
      //   tendencia=this.tagservices.getCodeAlarms(this.state.txtCodigo)
      // }
      if(this.state.check){
        tendencia=this.tagservices.getTendenciaAlarmsActive(f1,f2)
      }else{
        tendencia=this.tagservices.getTendenciaAlarms(f1,f2)
      }
      tendencia.then(response=>{
          //console.log(response.data.statusCode)
          if(response.data.statusCode==201 || response.data.statusCode==200){
              // tabsContent=[];
              disabled=false;
              //console.log(response.data.data)
              this.setState({Alarms:response.data.data})
              let data=response.data.data
              let workplaces=data.map(a=>{return ({"_id":a.workplaceId})})
              let locations=data.filter(l=>l.locationId!=null).map(l=>{return ({"locationId":l.locationId})})
              console.log(locations)
              if(workplaces.length>0){
                try{
                  this.tagservices.getWorkplaces_array(workplaces)
                  .then(respuesta=>{
                    this.setState({Workplace:respuesta.data.data})
                    this.tagservices.getTagsArray(locations).then(tags=>{
                      this.setState({Tags:tags.data.data})
                    }).catch(e=>e)
                  })
                }catch(e){
                  //console.log(e)
                }
              }
          }else{
              console.log("status 500")
          }
      }).catch(e=>e)
    }

    getWorkplace(id){
      let workplace=[]
      workplace=this.state.Workplace.filter(w=>w._id==id)
      if(workplace.length>0){
        return workplace[0].name
      }
    }

    duracionAlarma(fini,ffin){
      let minutes=0
      if(ffin==null){
        ffin=moment().format("YYYY-MM-DDTHH:mm:ss")+".000Z";
      }
      minutes=moment(ffin).diff(moment(fini), 'minute')
      //return fini+" / "+ffin
      if(minutes<=1440){
        if(minutes<=60){
          return Math.round(minutes)+" minutos"
        }else{
          return Math.round(minutes/60)+" horas"
        }
      }
      return Math.round(minutes/1440)==1?Math.round(minutes/1440)+" día":Math.round(minutes/1440)+" días"
    }

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
      let value1 = this.state.start.format("DD-MM-YYYY") 
      let value2 = this.state.end.format("DD-MM-YYYY");
      //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
      //console.log(value1);
      return (
        <div>
          <DateTimeRangeContainer
            ranges={ranges}
            start={this.state.start}
            end={this.state.end}
            local={local}
            maxDate={maxDate}
            applyCallback={this.applyCallback}
            rangeCallback={this.rangeCallback}
            autoApply
          >
              <InputGroup>
                  <InputGroupAddon addonType="prepend">
                      <div className="input-group-text">
                          <FontAwesomeIcon icon={faCalendarAlt}/>
                      </div>
                  </InputGroupAddon>
                  <Input
                      id="formControlsTextA"
                      type="text"
                      label="Text"
                      placeholder="Enter text"
                      style={{ cursor: "pointer" }}
                      disabled
                      value={value1}
                  />
                       <Input
                      id="formControlsTextb"
                      type="text"
                      label="Text"
                      placeholder="Enter text"
                      style={{ cursor: "pointer" }}
                      disabled
                      value={value2}
                  />
              </InputGroup>
            
          </DateTimeRangeContainer>   
        </div>
      );
    }

    handleChange = event => {
      const name = event.target.name;
      const value = event.target.value;
      this.setState({ [name]: value });
  }
    
    applyCallback = (startDate, endDate) =>{      
      this.setState({
          start: startDate,
          end: endDate
      });
    }

    renderSelecTag = (data) =>{ 
        return (
          <Input type="select" name="txtSeleccion" value={this.state.txtSeleccion} onChange={this.handleChange}>
            {data.map((d,ind)=>{
              if(d.name=="todos"){
                return <option selected value={d._id}>{d.name}</option>
              }else{
                return <option value={d._id}>{d.name}</option>
              }
            })}
          </Input>
        );
      }
    reformatDate(dateTime){
      return this.tagservices.reformatDate(dateTime)
    }

    options_status(status,id){
      // let option0="none"
      let option1="none"
      let option2="none"
      let option3="none"
      let option4="none"
      let status0="sin accion"
      let status1="en soporte";
      let status2="en servicio terreno";
      let status3="finalizada";
      if(status=="sin accion"){
        option1="block"
      }if(status=="en soporte"){
        // option0="block"
        option2="block"
        option4="block"
      }if(status=="en servicio terreno"){
        option3="block"
      }if(status=="finalizada"){
        option4="block"
      }
      return<Fragment>
        {/* <option style={{display:option0}} value={status0}>{status0}</option> */}
        <option style={{display:option1}} value={status1}>{status1}</option>
        <option style={{display:option2}} value={status2}>{status2}</option>
        <option style={{display:option3}} value={status3}>{status3}</option>
        <option style={{display:option4}} value={status0}>{status0}</option>
      </Fragment>
    }

    sendEmailSMA(msg,workplace,workplaceId,code){
      let data={
        message:msg,
        name_workplace:workplace,
        workplaceId:workplaceId,
        dateTime: new Date(),
        active:true,
        code:code
      }
      this.tagservices.createEmailSuport(data).then(respuesta=>{
        if(respuesta.data.statusCode===201 || respuesta.data.statusCode===200){
          console.log(respuesta.data)
          toast['success']('se envio el correo Correctamente', { autoClose: 4000 })
        }
      }).catch(e=>e)
    }

    onChangeStatus = event => {
      let warnig=""
      let split=String(event.target.id).split('/')
      const id = split[0];
      const value = event.target.value;
      let NameWorkplace=split[4]
      if(value=="finalizada"){
        this.updateStatus(id,{"status":value,"active":false,"dateTimeTer":new Date()})
        this.sendEmailSMA("alarma_finalizada",NameWorkplace,split[3],split[5])
        if(!String(split[1]).includes('desconexion')){
          let data={falla:null,fallaDate:null}
          this.tagservices.updateLocation(split[2],data).then(response=>{
            if(response.data.statusCode==201 || response.data.statusCode==200){
              toast['success']('Se actualizo el dispositivo', { autoClose: 4000 })
            }
          }).catch(e=>e)
        }else{
          //console.log(split[3])
          let data={desconect:false,desconectDate:null}
          this.tagservices.updateWorkplace(split[3],data).then(response=>{
            console.log(response.data.data)
          })
          .catch(e=>e)
        }
      }
      else if(value=="sin accion"){
        this.updateStatus(id,{"status":value,"active":true,"dateTimeSoport":null,"dateTimeTer":null})
      }else if(value=="en soporte"){
        warnig="1"
        if(String(split[1]).includes('desconexion') && NameWorkplace.includes('SMA')){
          Swal.fire({
            title: '¿Estas seguro de esto?',
            text: "Ingrese el detalle para la SMA",
            input: 'textarea',
            inputAttributes: {
              autocapitalize: 'off'
            },
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: '¡No, cancelar!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, cambiar estado!',
            preConfirm: (login) => {
              
              return fetch(`//api.github.com/users/${login}`)
                .then(response => {
                  // console.log(login)
                  if (login=="") {
                    throw new Error(response.statusText)
                  }
                  return login
                })
                .catch(error => {
                  Swal.showValidationMessage(
                    `Request failed: ${error}`
                  )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
          }).then((result) => {
            //alert(split[4])
            let NameWorkplace=split[4]
            if (result.isConfirmed) {
              moment().format('YYYY-MM-DD HH:mm:ss')
              // this.setState({Alarms:this.state.Alarms.filter(a=>a._id!=id).concat(
              //   this.state.Alarms.filter(a=>a._id==id).map(a=>{
              //     a.status=value
              //     return a
              //   })[0]
              // )})
              this.updateStatus(id,{"status":value,"active":true,"dateTimeSoport":new Date()})
              this.sendEmailSMA(result.value,NameWorkplace,split[3],split[5])
            }else{
              this.updateStatus(id,{"status":"sin accion","active":true,"dateTimeSoport":new Date()},"1")
            }
          })
        }else{
          Swal.fire({
            title: '¿Estas seguro de esto?',
            text: "Esto generara un registro de soporte",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, cambiar estado!'
          }).then((result) => {
            if (result.isConfirmed) {
              this.updateStatus(id,{"status":value,"active":true,"dateTimeSoport":new Date()})
            }else{
              this.updateStatus(id,{"status":"sin accion","active":true,"dateTimeSoport":new Date()},"1")
            }
          })
        }
      }
      else{
        this.updateStatus(id,{"status":value})
      }
      if(warnig==""){
        this.setState({ ["txtStatus"+id]: value });
      }
    }

    onChangeCheck= event => {
      const id = event.target.id;
      const value = event.target.value;
      const name = event.target.name;
      //alert(value)
      this.setState({ [name]: value==='true'?false:true });
    }

    onChangeSelect= event => {
      const value = event.target.value;
      const name = event.target.name;
      //alert(value)
      this.setState({ [name]: value });
    }

    updateStatus(id,data,sin_cambio){
      // {"status":value,"active":false,"dateTimeTer":new Date()}
      if(sin_cambio!=null){
        this.getAlarmasTendencia()
      }else{
        this.tagservices.uptadeAlarmsId(id,data).then(response=>{
          console.log(response.data.statusCode)
          if(response.data.statusCode==201 || response.data.statusCode==200){
            //console.log("object")
            toast['success']('La alarma cambio su estado', { autoClose: 4000 })
            this.getAlarmasTendencia()
          }
        }).catch(e=>e)
      }
    }

    getDetail(detail){
      if(detail.includes("desconexion a internet")){
        let det=detail.split('(');
        return det[0]
      }else{
        return detail
      }
    }

    getStatus(status){
      if(status){
        return<span style={{fontSize:20,color:"red"}} className="pe-7s-bell"></span>
      }else{
        return<span style={{fontSize:23,color:"green"}} className="pe-7s-flag"></span>
      }
    }

    getArray(data){
      return data.map(d=>{
        return{
          "dateTimeIni": d.dateTimeIni,
          "company": d.company,
          "suport_duration": this.duracionAlarma(d.dateTimeIni,d.dateTimeSoport==new Date()?null:d.dateTimeSoport),
          "workplaceId": this.getWorkplace(d.workplaceId),
          "divice": d.divice,
          "end_duration": this.duracionAlarma(d.dateTimeIni,d.dateTimeTer==null?new Date():d.dateTimeTer),
          "detail": d.detail,
        }
      })
    }

    showButtonExport(data){
      return <CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `Company`, key: "company" },
                { label: "workplaceId", key: "workplaceId" },
                { label: "divice" , key: "divice" },
                { label: "detail" , key: "detail" },
                { label: "dateTimeIni" , key: "dateTimeIni" },
                { label: "duracion Soporte" , key: "suport_duration" },
                { label: "duracion Final" , key: "end_duration" },
              ]                                         
            }
            data={this.getArray(data)}
            filename={`Alarmas ${moment(this.state.start).format("DD-MM-YYYYTHH:mm:ss")} - ${moment(this.state.end).format("DD-MM-YYYYTHH:mm:ss")}.csv`}
            className="btn btn-success btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            style={{marginLeft:5,display:`${data.length>0?"block":"none"}`}}
            target="_blank">
            Exportar Datos
      </CSVLink>
    }

    onlyUnique(value, index, self) { 
      return self.indexOf(value) === index;
    }

    filtrosOptions(type){
      let empresas=this.state.Alarms.map(a=>a.company)
      let centros=this.state.Alarms.map(c=>c.workplaceId)
      let estados=this.state.Alarms.map(c=>c.status)
      if(type=="empresa"){
        return empresas.filter(this.onlyUnique).map(e=>{
          return <option value={e}>{e.toLocaleUpperCase()}</option>
        })
      }
      if(type=="centro"){
        return centros.filter(this.onlyUnique).map(c=>{
          return <option value={c}>{this.getWorkplace(c)}</option>
        })
      }
      if(type=="estado"){
        return estados.filter(this.onlyUnique).map(c=>{
          return <option value={c}>{c.toLocaleUpperCase()}</option>
        })
      }
    }

    onVivo(){
      if(this.state.en_vivo){
        this.getAlarmasTendencia();
      }
      // if(this.state.en_vivo){
      //   let minutos=1;
      //   let segundos=60*minutos;
      //   let intervaloRefresco =segundos*1000;
      //   this.timeRef.current = setInterval(this.getAlarmasTendencia.bind(this),intervaloRefresco);
      // }else{
      //   clearInterval(this.timeRef.current)
      // }
    }

    showDivice(divice,locationId,workplaceId,ind){
      let workplace=this.getWorkplace(workplaceId)
      if(divice!=null){
        return <Fragment>
        {divice} {console.log(this.state.Tags.filter(t=>t.locationId==locationId))}
        
        <Badge style={{marginLeft:5,fontSize:18,cursor:"pointer"}} onClick={(()=>{
          let array_tendencia={
            active: true,
            code: "",
            index: ind,
            module: "modulo 100",
            name: divice,
            tags: this.state.Tags.filter(t=>t.locationId==locationId),
            zoneId: this.state.Tags.filter(t=>t.locationId==locationId)[0].zoneId,
            _id: locationId
          } 
          //alert(JSON.stringify(array_tendencia))
          tag_=[];
          title_disp="";
          title_disp=divice;
          this.toggleModal2(true,workplaceId,workplace,"tencencia_abiotica",array_tendencia)
        })} color={"primary"}><span  className={"pe-7s-graph2"}></span>
        </Badge>
        </Fragment>
      }else{
        return divice
      }
      
    }

    optionSelected(id){
      let find_alarm=this.state.Alarms.filter(a=>a._id==id)[0]
      console.log(find_alarm)
      return<option selected>{find_alarm.status}</option>
    }

    changeAction(valor){
      this.setState({selectAction:valor})
    }

    listItemFallas(workplaceId,type,filtro){
      let alarms=this.state.Alarms.filter(filtro)
      return alarms.filter(a=>a.workplaceId==workplaceId&&a.type==type).map((a,i)=>{
        let detail=a.detail.replace(`${type} -`,'')
        let endDate=moment(this.reformatDate(a.endDate)).format("YYYY-MM-DD HH:mm:ss")
        if(i<3){
          return <ListGroupItem>
            {detail}{a.divice!=null?" - "+a.divice+" ("+endDate+")":""}
          </ListGroupItem>
        }else if(i==4){
          return<>
            <ListGroupItem action className="text-center" name={workplaceId+"visible"} onChange={this.onChangeSelect}  
            style={{display:this.state[workplaceId+"visible"]?"none":"block"}}
            onClick={(()=>{
              this.setState({[workplaceId+"visible"]:true})
            })}>
              ...
            </ListGroupItem>
            <ListGroupItem style={{display:this.state[workplaceId+"visible"]?"block":"none"}} 
            onClick={(()=>{
              this.setState({[workplaceId+"visible"]:false})
            })} action>
              {detail}{a.divice!=null?" - "+a.divice+" ("+endDate+")":""}
            </ListGroupItem>
          </>
        }else{
          return<ListGroupItem style={{display:this.state[workplaceId+"visible"]?"block":"none"}} 
          onClick={(()=>{
            this.setState({[workplaceId+"visible"]:false})
          })} action>
            {detail}{a.divice!=null?" - "+a.divice+" ("+endDate+")":""}
          </ListGroupItem>
        }
        
      })
    }

    CardsCentros(filtro){
      let data=this.state.Alarms.filter(filtro).map(a=>{
        let fallas=this.state.Alarms.filter(b=>b.workplaceId==a.workplaceId&&b.type=="falla").length
        let alarmas=this.state.Alarms.filter(b=>b.workplaceId==a.workplaceId&&b.type=="alarma").length
        a.alertas=fallas+alarmas
        return a
      })
      data=_.orderBy(data,["alertas"],["desc"])
      let centros=data.map(c=>c.workplaceId)
      return centros.filter(this.onlyUnique).map(c=>{
        let nameWorkplace=this.getWorkplace(c)
        let alert=data.filter(a=>a.workplaceId==c&&a.active==true)
        let activa=alert.length>0?"block":"none"
        return (
        <Col xs="12" sm="12" md="3" lg="3" style={{display:!this.state.selectAction?"none":"block"}}>
          <Card>
            <CardHeader row>
              <Col>
                <CardTitle><span style={{fontSize:20,color:"red"}} className="pe-7s-bell"></span> {nameWorkplace} </CardTitle>
              </Col>
              <Col>
                <Button color={"success"} style={{position: 'absolute', right: 5,top:-15,display:`${activa}`}} onClick={(()=>{
                  this.finalizarAlarma(c,nameWorkplace,"finalizada")
                })} outline>Normalizar Centro</Button>
              </Col>
            </CardHeader>
            <CardBody>
              <ListGroup>
                <ListGroupItem style={{backgroundColor:"#d92550",borderColor:"#d92550"}} active>
                  Fallas
                </ListGroupItem>
                {this.listItemFallas(c,"falla",filtro)}
              </ListGroup>
              <ListGroup>
                <ListGroupItem style={{backgroundColor:"#f7b924",borderColor:"#f7b924"}}>
                  Alarmas
                </ListGroupItem>
                {this.listItemFallas(c,"alarma",filtro)}
              </ListGroup>
            </CardBody>
          </Card>
        </Col>)
      })
    }

    finalizarAlarma(workplaceId,nameWorkplace,value){
      if(value=="finalizada"){
        let centro=this.state.Alarms.filter(a=>a.workplaceId==workplaceId)
        centro.map(a=>{
          this.updateStatus(a._id,{"status":value,"active":false,"dateTimeTer":new Date()})
          if(!String(a.detail).includes('desconexion')){
            // this.sendEmailSMA("alarma_finalizada",nameWorkplace,workplaceId,a.code)
            let data={falla:null,fallaDate:null}
            this.tagservices.updateLocation(a.locationId,data).then(response=>{
              if(response.data.statusCode==201 || response.data.statusCode==200){
                toast['success']('Se actualizo el dispositivo', { autoClose: 4000 })
              }
            }).catch(e=>e)
          }else{
            //console.log(split[3])
            // this.sendEmailSMA("alarma_finalizada",nameWorkplace,workplaceId,a.code)
            let data={desconect:false,desconectDate:null}
            this.tagservices.updateWorkplace(workplaceId,data).then(response=>{
              console.log(response.data.data)
            })
            .catch(e=>e)
          }
        })
      }
    }

    render() {
        //dataExport
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
        let end = moment(start).add(1, "days").subtract(1, "seconds");         
        let ranges = {
        "Solo hoy": [moment(start), moment(end)],
        "Solo ayer": [
          moment(start).subtract(1, "days"),
          moment(end).subtract(1, "days")
        ],
        "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
        "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
        "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
        "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
        "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
        "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
        "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
      };
      let local = {
        format: "DD-MM-YYYY HH:mm",
        sundayFirst: false
      };
      let maxDate = moment(start).add(24, "hour");
      let filtro=a=>{
        if(this.state["filtroEmpresa"]!="all"){
          return a.company==this.state["filtroEmpresa"]
        }
        if(this.state["filtroCentro"]!="all"){
          return a.workplaceId==this.state["filtroCentro"]
        }
        if(this.state["filtroStatus"]!="all"){
          return a.status==this.state["filtroStatus"]
        }
        if(this.state.txtSeleccion=="todos"){
          return a
        }
        else{
          return a.type==this.state.txtSeleccion
        }
      }
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Herramientas</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Historico Alarmas</BreadcrumbItem>
                      {/* <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      </Breadcrumb>
                      <Row>
                        <Col md={9}></Col>
                        <Col md={3}>
                          <ButtonGroup style={{marginLeft:25}}>
                            <Button color={"primary"} outline={this.state.selectAction} onClick={(()=>{
                              this.changeAction(false)
                            })}>Tabla</Button>
                            <Button color={"primary"} outline={!this.state.selectAction} onClick={(()=>{
                              this.changeAction(true)
                            })}>Centros</Button>
                          </ButtonGroup>
                        </Col>
                      </Row>
                     <Row style={{marginTop:50}}>
                        <Col   md={12} lg={3}> 
                            {this.renderPickerAutoApply(ranges, local, maxDate)}   
                        </Col>
                        <Col md={12}  lg={4}>
                          {this.renderSelecTag(this.state.options)}
                        </Col>
                        <Col md={12}  lg={2}>
                          <Input placeholder="Buscar por codigo..." name={"txtCodigo"} value={this.state["txtValue"]} onChange={this.onChangeSelect}/>
                        </Col>
                        <Col   md={12} lg={3}> 
                            <ButtonGroup>
                              {/* <Button color="success" outline onClick={((e)=>{
                              this.buttonChangeDate(31)
                              })}>- 31 </Button>
                              <Button color="success" outline onClick={((e)=>{
                              this.buttonChangeDate(7)
                              })}>- 7 </Button>
                              <Button color="success" outline onClick={((e)=>{
                              this.buttonChangeDate(1)
                              })}>- 1 </Button> */}
                            </ButtonGroup>
                            <ButtonGroup style={{marginLeft:20}}>
                                <Button color="primary"
                                        outline
                                        disabled={disabled}
                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                        onClick={() => { 
                                          console.log(this.state.txtSeleccion=="")
                                          if(this.state.txtSeleccion != null){
                                          disabled=true;
                                            this.setState({
                                                dataExport:[],
                                                buttonExport:'block'
                                            });
                                              if(this.state.txtCodigo==undefined || this.state.txtCodigo==""){
                                                this.getAlarmasTendencia();
                                              }else{
                                                this.getCodeTendencia();
                                              }
                                              }}
                                          }
                                >Filtrar
                                </Button>
                            </ButtonGroup>
                            <ButtonGroup>
                              {this.showButtonExport(this.state.Alarms.filter(filtro))}
                              {/* <Button style={{width:`${100}%`,marginLeft:5}} color="success" onClick={(()=>{

                              })} outline>Exportar CSV</Button> */}
                            </ButtonGroup>
                        </Col>      
                     </Row>
                     <Row style={{marginTop:10}}>
                        <Col md={3}></Col>
                        <Col md={6}>
                          <Card>
                            <CardBody >
                              <CardTitle>Acciones</CardTitle>
                              <FormGroup style={{width:`${100}%`}}
                                check
                                inline>
                                <Input type="checkbox" defaultChecked={this.state.check} onChange={this.onChangeCheck} value={this.state.check} name={"check"}/>
                                <Label check={this.state.check==='true'?true:false}>
                                  Solo alarmas activas
                                </Label>
                                <Input type="select" style={{width:`${25}%`,marginLeft:10}} name={"filtroEmpresa"} value={this.state["filtroEmpresa"]} onChange={this.onChangeSelect}>
                                  <option selected value="all" disabled>{"Filtro empresas".toLocaleUpperCase()}</option>
                                  <option value="all">{"todas".toLocaleUpperCase()}</option>
                                  {this.filtrosOptions("empresa")}
                                </Input>
                                <Input type="select" style={{width:`${25}%`}} name={"filtroCentro"} value={this.state["filtroCentro"]} onChange={this.onChangeSelect}>
                                  <option selected value="all" disabled>{"Filtro centros".toLocaleUpperCase()}</option>
                                  <option value="all">TODOS</option>
                                  {this.filtrosOptions("centro")}
                                </Input>
                                <Input type="select" style={{width:`${25}%`}} name={"filtroStatus"} value={this.state["filtroStatus"]} onChange={this.onChangeSelect}>
                                  <option selected value="all" disabled>{"Filtro estados".toLocaleUpperCase()}</option>
                                  <option value="all">{"todos".toLocaleUpperCase()}</option>
                                  {this.filtrosOptions("estado")}
                                </Input>
                                <Button color={this.state.en_vivo?"success":"secondary"} style={{marginLeft:10,width:`${25}%`}} onClick={(()=>{
                                  this.onVivo();
                                  this.setState({en_vivo:!this.state.en_vivo})
                                })} outline>En vivo</Button>
                              </FormGroup>
                            </CardBody>
                          </Card>
                        {/* <FormGroup
                          check
                          inline
                        >
                          <Input type="checkbox" />
                          <Label check>
                            Alarmas en vivo
                          </Label>
                        </FormGroup> */}
                        </Col>
                        <Col md={3}></Col>
                      </Row>
                     <Row style={{marginTop:50,display:!this.state.selectAction?"block":"none"}}> 
                      <ReactTable  
                        style={{width:`${100}%`}}                                                         
                        data={ this.state.Alarms.filter(filtro)
                                // [
                                //   {
                                //   ind:1,
                                //   company:"camanchaca",
                                //   workplace:"ahoni",
                                //   location:"sensor 15m",
                                //   evento:"desconexion",
                                //   duracion:"5h",
                                //   estado:"En revision",
                                //   }
                                // ]
                              }
                        // loading= {false}
                        showPagination= {true}
                        showPaginationTop= {false}
                        showPaginationBottom= {true}
                        showPageSizeOptions= {false}
                        pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                        defaultPageSize={8}
                        columns={[
                                {
                                Header: "codigo",
                                accessor: "ind",
                                width: 75,
                                Cell: ({ row,index }) => (
                                  <div>
                                     {String(row._original.code).substr(-6)}
                                  </div>
                                  )
                                },
                                {
                                Header: "Empresa",
                                accessor: "company",                                                              
                                width: 220,
                                Cell: ({ row }) => (
                                  <div style={{backgroundColor:"#545cd8"}}>
                                     <img width="200" height="50" src={`https://ihc.idealcontrol.cl/idealcloud2/doc/imagenes/${row._original.company}-min.png`} />
                                  </div>
                                  )
                                },
                                {
                                Header: "Centro",
                                accessor: "workplaceId",                                                              
                                width: 200,
                                Cell: ({ row }) => (
                                  <Fragment>
                                     {this.getWorkplace(row._original.workplaceId)}
                                  </Fragment>
                                  )
                                },
                                {
                                Header: "Dispositivo",
                                accessor: "divice",                                                              
                                width: 280,
                                Cell: ({ row,ind }) => (
                                  <Fragment>
                                     {this.showDivice(row._original.divice,row._original.locationId,row._original.workplaceId,ind)}
                                  </Fragment>
                                  )
                                },
                                {
                                  Header: "Detalle",
                                  accessor: "detail",                                                              
                                  width: 250,
                                  Cell: ({ row }) => (
                                    <Fragment>
                                       <Badge color={row._original.detail.includes('falla')?"danger":"warning"}>{this.getDetail(row._original.detail)}</Badge>
                                    </Fragment>
                                    )
                                },
                                //this.duracionAlarma(d.dateTimeIni,d.dateTimeSoport==null?null:d.dateTimeSoport)
                                {
                                  Header: "Fecha Alarma",
                                  accessor: "dateTimeIni",                                                              
                                  width: 130,
                                  Cell: ({ row }) => (
                                    <Fragment>
                                       <Badge color={"warning"}>{moment(row._original.endDate).format("DD-MM-YYYY HH:mm")}</Badge>
                                    </Fragment>
                                    )
                                },
                                {
                                  Header: "Fecha Detección",
                                  accessor: "dateTimeIni",                                                              
                                  width: 130,
                                  Cell: ({ row }) => (
                                    <Fragment>
                                       <Badge color={"primary"}>{moment(this.reformatDate(row._original.dateTimeIni)).format("DD-MM-YYYY HH:mm")}</Badge>
                                    </Fragment>
                                    )
                                },
                                {
                                  Header: "Reconocimiento de Falla",
                                  accessor: "duracion",                                                              
                                  width: 150,
                                  Cell: ({ row }) => (
                                    <Fragment>
                                       {this.duracionAlarma(this.reformatDate(row._original.dateTimeIni),row._original.dateTimeSoport==null?new Date():row._original.dateTimeSoport)}
                                    </Fragment>
                                    )
                                },
                                {
                                  Header: "Soporte Finalizado",
                                  accessor: "duracion",                                                              
                                  width: 150,
                                  Cell: ({ row }) => (
                                    <Fragment>
                                       {this.duracionAlarma(this.reformatDate(row._original.dateTimeIni),row._original.dateTimeTer==null?new Date():row._original.dateTimeTer)}
                                    </Fragment>
                                    )
                                },
                                {
                                Header: "Estado",
                                width: 240,
                                Cell: ({ row }) => (
                                    <Fragment>
                                        <Input type="select" style={{width:130}} name={"txtStatus"+row._original._id} 
                                               id={row._original._id
                                                +"/"+row._original.detail
                                               +"/"+row._original.locationId
                                               +"/"+row._original.workplaceId
                                               +"/"+this.getWorkplace(row._original.workplaceId)
                                               +"/"+row._original.code} 
                                               value={this.state["txtStatus"+row._original._id]} onChange={this.onChangeStatus}>
                                          {this.optionSelected(row._original._id)}
                                          {this.options_status(row._original.status,row._original._id)}
                                        </Input>&nbsp;
                                      <font size={5}><span className="pe-7s-mail" style={{color:`${row._original.sentEmail?"green":"black"}`}}></span></font>&nbsp;
                                      {this.getStatus(row._original.active)}
                                    </Fragment>
                                    )
                                },
                                // {
                                //   Header: "Send Email",
                                //   width: 200,
                                //   Cell: ({ row }) => (
                                //       <Fragment>
                                //          <Badge color={row._original.sentEmail?"primary":"warning"} style={{fontSize:23}}><span className="pe-7s-mail"></span></Badge>
                                //       </Fragment>
                                //       )
                                // },
                            ]                                                             
                        }
                        
                        className="-striped -highlight"
                        />
                    </Row>
                    <Row style={{marginTop:50}}>
                      {
                        this.CardsCentros(filtro)
                      }
                    </Row>
                    {/* <Modal>
                      <Card>
                        <CardHeader></CardHeader>
                        <CardBody>
                          <Tendencia Centro={_id_centro} Ncentro={title_centro} Tag={tag_} ></Tendencia>
                        </CardBody>
                      </Card>
                    </Modal> */}
                    <Modal style={{maxHeight:`${100}%`,maxWidth:`${80}%`}} isOpen={ModalActive2} >
                          <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal2(false)}>
                                        <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                                                  <div class="icon-wrapper rounded-circle">
                                                                      <div class="icon-wrapper-bg opacity-9 bg-primary"></div>
                                                                      <i class="lnr-apartment text-white"></i>
                                                                  </div>
                                                                  <div class="widget-chart-content">
                                                                  {title_centro} / {title_disp}
                                                                  <i class="fa fa-angle-up"></i>
                                                                  </div>
                                                            </div>
                                       </ModalHeader>
                                        <ModalBody >
                                        <Tendencia Centro={_id_centro} Ncentro={title_centro} Tag={tag_} ></Tendencia>
                                        </ModalBody>
                              <ModalFooter>
                               
                              </ModalFooter>
                          </BlockUi>
                      </Modal>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Alarmas);
