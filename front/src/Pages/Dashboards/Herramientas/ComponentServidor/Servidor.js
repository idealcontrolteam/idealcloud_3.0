import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import {
  toast
} from 'react-toastify';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Form, Label, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,
    Modal, ModalHeader, ModalBody, ModalFooter,CardTitle,Badge} from 'reactstrap';
import moment from 'moment';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';

import ReactTable from "react-table";

import {connect} from 'react-redux';
import { setBackgroundColor } from '../../../../reducers/ThemeOptions';
import CardFooter from 'reactstrap/lib/CardFooter';
import ModalGlobal from '../../../Hooks/ModalGlobal'
import TendenciaServer from './ComponentTendenciaServer/TendenciaServer';


//let Seleccion={fecha_inicio:"",fecha_fin:"",centro:"",centro_id:"",sensor:"",sensor_id:"",variable:""};
let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
const CanvasJSChart = CanvasJSReact.CanvasJSChart;
//let Seleccionadas=[];
const item_right={display: 'flex',justifyContent: 'right',alignItems: 'right'}
const item_left={display: 'flex',justifyContent: 'left',alignItems: 'left'}


class UsuarioCentros extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          NameWorkplace:"",
          blocking: true,
          modal1: false,
          admin:false,
          NombreUsuario:"",
          Status:[],
          Tags:[],
          isLoading:true,
        };     
        //se requiere Company, NombreUsuario, Nombre, Clave, Code
        this.tagservices = new TagServices();   
        this.timeRef=React.createRef();
        
    }

  
    componentDidMount = () => {
        
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
        this.getStatusServer();
        let minutos=1;
        let segundos=60*minutos;
        let intervaloRefresco =segundos*1000;
        this.timeRef.current = setInterval(this.getStatusServer.bind(this),intervaloRefresco);
      }

    componentWillUnmount() {
      clearInterval(this.timeRef.current);
    }
    
    componentWillReceiveProps(nextProps){
      if (nextProps.initialCount && nextProps.initialCount > this.state.count){
        this.setState({
          count : nextProps.initialCount
        });
      }
      //alert(JSON.stringify(nextProps));
      let separador=nextProps.centroUsuario;
      let centro=separador.split('/');
      this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
      //this.getSondas(centro[0].toString());
    }

    getStatusServer(){
      let EndPoint = `${API_ROOT}/status_server`;
      axios.get(EndPoint)
      .then(response=>{
        console.log(response.data.statusCode)
        if(response.data.statusCode==200 || response.data.statusCode==201){
          let data=response.data.data[0]
          let keys=[]
          for (var key in data) {
            if(key!='_id')
              if(key=='cpu'||key=='total_disk'||key=='total_memory'||key=='used_disk'||key=='used_memory'){
                keys.push({name:key});
              }
          }
          this.setState({Status:data,Tags:keys,isLoading:false})
        }
      })
      .catch((e)=>e)
      // axios.get(API_ROOT+"status_server")
      // .then(response=>{
      //   console.log(response.data.statusCode)
      //   if(response.data.statusCode==200 || response.data.statusCode==201){
      //     this.setState({Status:response.data.data})
      //   }
      // })
      // .catch(e=>e)
    }

    getCardStadistic(icon,titulo,body,titulofooter,footer,type_color){
      return<Card>
        <div class="card no-shadow rm-border bg-transparent widget-chart text-left" style={{height:100}}>
            <div class="icon-wrapper rounded-circle">
                <div className={`icon-wrapper-bg opacity-9 bg-${type_color}`} ></div>
                <i class={icon}></i>
            </div>
            <div class="widget-chart-content">
                <div class="widget-subheading"><font ><font >{titulo}</font></font></div>
                <div class="widget-numbers"><span><font ><font color={miscolores[4]}>{body}</font></font></span></div>
                <div class="widget-description opacity-8 text-focus"><font><font >
                    {titulofooter}
                    </font></font><span class="text-info pl-1">
                        <i class="fa fa-angle-down"></i>
                        <span class="pl-1"><font ><font >{footer}</font></font></span>
                    </span>
                </div>
            </div>
        </div>
      </Card>
    }

    calculateRamUso(){
      let total_ram=(this.state.Status.total_memory/1024)* 100 + (this.state.Status.total_swap/1024)*100
      let total_ram_uso=(this.state.Status.used_memory/1024)* 100
      let ram_uso=total_ram_uso*100/total_ram
      return Math.round(ram_uso)
    }

    calculateDiskUso(){
      let total_ram=this.state.Status.total_disk
      let total_ram_uso=this.state.Status.used_disk
      let ram_uso=total_ram_uso*100/total_ram
      return Math.round(ram_uso)
    }

    cleanBuffer(){
      let EndPoint = `${API_ROOT}/status_server/${this.state.Status._id}`;
      let data={clearBuffer:true}
      axios.put(EndPoint,data)
      .then(response=>{
        console.log(response.data.statusCode)
        if(response.data.statusCode==200 || response.data.statusCode==201){
          toast['success']('El buffer esta sinedo vaciado... (esto puede tardar unos 2min. aprox.)', { autoClose: 4000 })
          this.getStatusServer()
        }
      })
      .catch((e)=>e)
    }


    render() {
      console.log(this.state.Status.cpu)
      const options = {
        animationEnabled: true,
        title: {
          text: "Uso CPU(s) %"
        },
        subtitles: [{
          text: (100-this.state.Status.cpu)+"% Disponible",
          verticalAlign: "center",
          fontSize: 24,
          dockInsidePlotArea: true
        }],
        data: [{
          type: "doughnut",
          // color :"#2F4F4F",
          showInLegend: true,
          indexLabel: "{name}: {y}",
          yValueFormatString: "#,###'%'",
          dataPoints: [
            { name: "% Disponible", y: (100-this.state.Status.cpu),color:miscolores[8]},
            { name: "% En Uso", y: this.state.Status.cpu, color:miscolores[5] },
            // { name: "Very Satisfied", y: 40 },
            // { name: "Satisfied", y: 17 },
            // { name: "Neutral", y: 7 }
          ]
        }]
      }
      const options2 = {
        animationEnabled: true,
        title: {
          text: "Uso RAM %"
        },
        subtitles: [{
          text: (100-this.calculateRamUso())+"% Disponible",
          verticalAlign: "center",
          fontSize: 24,
          dockInsidePlotArea: true
        }],
        data: [{
          type: "doughnut",
          // color :"#2F4F4F",
          showInLegend: true,
          indexLabel: "{name}: {y}",
          yValueFormatString: "#,###'%'",
          dataPoints: [
            { name: "% Disponible", y: (100-this.calculateRamUso()),color:miscolores[1]},
            { name: "% En Uso", y: (this.calculateRamUso()), color:miscolores[7] },
            // { name: "Very Satisfied", y: 40 },
            // { name: "Satisfied", y: 17 },
            // { name: "Neutral", y: 7 }
          ]
        }]
      }
      const options3 = {
        animationEnabled: true,
        title: {
          text: "Uso Disco %"
        },
        subtitles: [{
          text: (100-this.calculateDiskUso())+"% Disponible",
          verticalAlign: "center",
          fontSize: 24,
          dockInsidePlotArea: true
        }],
        data: [{
          type: "doughnut",
          // color :"#2F4F4F",
          showInLegend: true,
          indexLabel: "{name}: {y}",
          yValueFormatString: "#,###'%'",
          dataPoints: [
            { name: "% Disponible", y: (100-this.calculateDiskUso()),color:miscolores[9]},
            { name: "% En Uso", y: (this.calculateDiskUso()), color:miscolores[10] },
            // { name: "Very Satisfied", y: 40 },
            // { name: "Satisfied", y: 17 },
            // { name: "Neutral", y: 7 }
          ]
        }]
      }

     
      if (this.state.isLoading) {
          return <Loader type="ball-pulse"/>;
      }
        //dataExport
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      {/* <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Herramientas</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Servidor</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      </Breadcrumb> */}
                     <Row>  
                                         
                        <Col md="12">
                          <Card className="main-card mb-1 p-0">
                              <CardBody className="p-3">
                                  <Row>
                                    {/* <Col md={3}></Col> */}
                                    <Col md={4}></Col>
                                    <Col md={2}></Col>
                                    <Col md={6} style={item_right}>
                                      <ModalGlobal
                                        buttonName={'Aceptar'}
                                        typeButton={'Tendencia'}
                                        title={'Tendencia Servidor'}
                                      >
                                        <TendenciaServer 
                                          Tags={this.state.Tags}
                                        />
                                      </ModalGlobal>
                                      <a style={{cursor:'pointer'}} onClick={(e)=>{
                                          e.preventDefault()
                                          this.cleanBuffer()
                                        }}>
                                          <div class="card rm-border bg-transparent widget-chart text-left" style={{height:100,width:`${100}%`}}>
                                            <div class="icon-wrapper rounded-circle">
                                                <div className={`icon-wrapper-bg opacity-9 bg-${'info'}`} ></div>
                                                <i class={'pe-7s-rocket'} style={{color:'#fff'}}></i>
                                            </div>
                                            <div class="widget-chart-content">
                                                <div class="widget-subheading"><font ><font >{'Vaciar Memoria Buffer y Swap'}</font></font></div>
                                                <div class="widget-numbers"><span><font ><font color={miscolores[8]}>{'Limpiar'}</font></font></span></div>
                                            </div>
                                          </div>
                                      </a>
                                    </Col>
                                  </Row>
                                  <Row style={{marginTop:10}}>
                                    <Col md={4}>
                                      <CanvasJSChart options = {options3}/>
                                    </Col>
                                    <Col md={4} style={{textAlign: "center"}}>
                                      <CanvasJSChart options = {options}/>
                                    </Col>
                                    <Col md={4} style={{textAlign: "center"}}>
                                    <CanvasJSChart options = {options2}/>
                                    </Col>
                                    {/* <Col md={3}></Col> */}
                                      {/* <Button color={"primary"}>Limpiar Buffers</Button><span style={{fontSize:25}} className='pe-7s-rocket'></span> */}
                                    
                                  </Row>                             
                                  <Row style={{marginTop:10}}>
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-disk text-white","Memoria Disco Disponible",
                                                             `${this.state.Status.total_disk-this.state.Status.used_disk} GB`,"","","secondary")}
                                    </Col>
                                    <Col md={4}></Col>
                                    {/* <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Cache",
                                                              `${Math.round((this.state.Status.cache/1024)* 100) / 100} GB`,"","","primary")}
                                    </Col> */}
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Ram Disponible",
                                                              `${Math.round((this.state.Status.free_memory/1024)* 100) / 100} GB`,
                                                              "Expreción en MB",`${Math.round((this.state.Status.free_memory)* 100) / 100} MB`,"primary")}
                                    </Col>
                                    {/* <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Buffers",
                                                              `${Math.round((this.state.Status.buffers/1024)* 100) / 100} GB`,
                                                              "Expreción en MB",`${Math.round((this.state.Status.buffers)* 100) / 100} MB`,"primary")}
                                    </Col> */}
                                  </Row>
                                  <Row style={{marginTop:10}}>
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-disk text-white","Memoria Disco en Uso",
                                                             `${this.state.Status.used_disk} GB`,"","","secondary")}
                                    </Col>
                                    <Col md={4}></Col>
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Ram en Uso",
                                                             `${Math.round((this.state.Status.used_memory/1024)* 100) / 100} GB`,"","","primary")}
                                    </Col>
                                  </Row>
                                  <Row style={{marginTop:10}}>
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-disk text-white","Memoria Disco Total",
                                                             `${this.state.Status.total_disk} GB`,"","","secondary")}
                                    </Col>
                                    <Col md={4}></Col>
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Ram Total",
                                                             `${Math.round((this.state.Status.total_memory/1024)*100+
                                                             (this.state.Status.total_swap/1024)*100) / 100} GB`,"","","primary")}
                                    </Col>
                                    {/* <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Ram en Uso",
                                                             `${Math.round((this.state.Status.used_memory/1024)* 100) / 100} GB`,"","","primary")}
                                    </Col> */}
                                    {/* <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Porcentaje de uso Ram",
                                                             `${this.calculateRamUso()} %`,"","","warning")}
                                    </Col> */}
                                  </Row>
                                  {/* <Row>
                                    <Col md={4}></Col>
                                    <Col md={4}></Col>
                                    <Col md={4}>
                                      {this.getCardStadistic("pe-7s-timer text-white","Memoria Cache",
                                                              `${Math.round((this.state.Status.cache/1024)* 100) / 100} GB`,"","","primary")}
                                    </Col>
                                  </Row> */}
                              </CardBody>
                          </Card>
                        </Col>
                    </Row>
                      
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UsuarioCentros);
