import moment from 'moment';
import React, { useEffect, useState } from 'react'
import BlockUi from 'react-block-ui';
import Loader from 'react-loaders';
import { Button, Card, CardBody, CardHeader, CardTitle, Col, FormGroup, Input, Row } from 'reactstrap'
import { API_ROOT } from '../../../../../api-config';
import SeleccionEntreFechas from '../../../../ComponentGlobal/SeleccionEntreFechas';
import {dataChart, dataAxisY, dataConfig} from './SettingsGraphic';
import CanvasJSReact from '../../../../../assets/js/canvasjs.react';
import { ResponsiveContainer } from 'recharts';

const TendenciaServer = ({Tags}) => {
    const CanvasJSChart = CanvasJSReact.CanvasJSChart;
    const [blocking, setBlocking] = useState(false)
    const [loaderType, setLoaderType] = useState('ball-triangle-path')
    const [fechaInicio, setFechaInicio] = useState('')
    const [fechaFin, setFechaFin] = useState('')
    const center={display: 'flex', alignItems: 'left',justifyContent: 'left'}
    const [typeOp, setTypeOp] = useState('');
    // const [optionChart, setOptionChart] = useState({})
    const [data, setData] = useState([])
    const [datas, setDatas] = useState([])
    const [renderChart, setRenderChart] = useState(false)
    let miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

    const captureType = (e) => {
        setTypeOp(e.target.value);
    }

    const identifyType=(unity)=>{
        if(unity=='cpu'){
            return '%'
        }else{
            return 'GB'
        }
    }
    
    const identifyLimit=(unity)=>{
        if(unity=='cpu'){
            return 100
        }
        let split=unity.split('_')
        return datas.length>0?datas[0][`total_${split[1]}`]:100
    }

    const filtrar = async () => {
        if(typeOp!=''){
            setBlocking(true)
        }
        // if(typeOp!=''){
        //     const url=`${API_ROOT}/register_server/fechas/${moment(fechaInicio).format('YYYY-MM-DD HH:mm:00')}.000Z/${moment(fechaFin).format('YYYY-MM-DD HH:mm:59')}.000Z`
        //     const respuesta = await fetch(url, {
        //         method: 'get',
        //         headers: {'Content-Type':'application/json'},
        //         // body: {
        //         //     "first_name": this.firstName.value
        //         // }
        //     });
        //     const resultado = await respuesta.json()
        //     console.log(resultado.data)
        //     optionsChart1.data=dataChart(typeOp,resultado.data,miscolores[0])
        //     optionsChart1.axisY=dataAxisY(true,identifyType(typeOp),miscolores[0])
        //     setOptionChart(optionsChart1)
        // }
    }

    useEffect(() => {
        const filtrado = async () => {
            const url=`${API_ROOT}/register_server/fechas/${moment(fechaInicio).format('YYYY-MM-DD HH:mm:00')}.000Z/${moment(fechaFin).format('YYYY-MM-DD HH:mm:59')}.000Z`
            const respuesta = await fetch(url, {
                method: 'get',
                headers: {'Content-Type':'application/json'},
                // body: {
                //     "first_name": this.firstName.value
                // }
            });
            const resultado = await respuesta.json()
            setData(
                resultado.data.map(medicion=>{
                    var fecha=new Date(medicion.dateTime);
                    var zona_horaria=new Date(medicion.dateTime).getTimezoneOffset();
                    zona_horaria=zona_horaria/60;
                    fecha.setHours(fecha.getHours()+zona_horaria);
                    return {y:medicion[typeOp], x:fecha.getTime()}
                })
                //dataChart(typeOp,resultado.data,miscolores[0])
            )
            setDatas(resultado.data)
            setBlocking(false)
        }
        if (typeOp!='' && blocking){
            filtrado()
        }
       
        // console.log(Tags)
    },[blocking,data,typeOp])

    let optionsChart1 = {
        data: [{
            axisYIndex:0,
            axisYType: "primary",
            type: "spline",
            legendText: typeOp,
            name: typeOp,
            color:miscolores[0],
            dataPoints: data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,
            lineThickness: 3
        }],
        height:400,
        zoomEnabled: true,
        exportEnabled: true,
        animationEnabled: false, 
        toolTip: {
            shared: true,
            contentFormatter: function (e) {
                var content = " ";
                for (var i = 0; i < e.entries.length; i++){
                    content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
                } 
                content +=   "<br/> " ;
                for (let i = 0; i < e.entries.length; i++) {
                    // eslint-disable-next-line no-useless-concat
                    content += typeOp + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                    content += "<br/>";
                }
                return content;
            },
        },
        legend: {
            horizontalAlign: "center", 
            cursor: "pointer",
            fontSize: 11,
            itemclick: (e) => {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }
                setRenderChart(!renderChart);   
                
            }
        }, 
        axisX:{
            valueFormatString:  "DDMMM HH:mm",
            labelFontSize: 10

        },
        // title:{
        //   text: "Weekly Revenue Analysis for First Quarter"
        // },
        axisY:[
            {
            title: identifyType(typeOp),
            lineColor: miscolores[0],
            tickColor: miscolores[0],
            labelFontColor: miscolores[0],
            titleFontColor: miscolores[0],
            includeZero: true,
            labelFontSize: 11,  
            suffix: identifyType(typeOp),
            maximum: identifyLimit(typeOp)
            },
        ],
    }

    return (
        <> 
            <Row style={{width:`${100}%`}}>  
                <Col md={5}>
                    <SeleccionEntreFechas
                        fechaInicio={fechaInicio}
                        fechaFin={fechaFin}
                        setFechaInicio={setFechaInicio}
                        setFechaFin={setFechaFin}
                    />
                </Col>   
                <Col md={5}>
                    <FormGroup>
                        <Input type="select" onChange={captureType}>
                            <option value="Seleccione Tag" selected disabled>Seleccione Tag</option>
                            {
                                Tags.filter(tag=>!tag.name.includes('total')).map(tag=>{
                                    return(
                                    <option value={tag.name}>{tag.name.replace('_',' ').toUpperCase()}</option>)
                                })
                            }
                        </Input>
                    </FormGroup>
                </Col>   
                <Col md={2} style={center}>
                    <Button style={{width:150}} color={'primary'} outline={true} onClick={()=>filtrar()}>Filtrar</Button>
                </Col>   
                {/* <Col>
                    <video width="650" controls muted="muted" autoplay>
                        <source src='http://localhost:3014/archive/videos_streaming' type="video/mp4" ></source>
                    </video>
                </Col> */}
                <Col md="12">
                    <BlockUi tag="div" blocking={blocking} 
                    loader={<Loader active type={loaderType}/>}>
                        <Card className="main-card mb-1 p-0">
                        <CardHeader><CardTitle>{'Tendencia'}</CardTitle></CardHeader>
                        <CardBody className="p-3">                                 
                            <Row style={{width:`${100}%`,height:400}}>
                                {data.length>0&&(
                                // <ShowGraphic
                                // optionChart={optionsChart1}
                                // />
                                <Col md={12} lg={12}> 
                                    <ResponsiveContainer height='100%' width='100%' >
                                        <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                    </ResponsiveContainer>   
                                </Col> 
                                )}
                            </Row>
                        </CardBody>
                        </Card>
                    </BlockUi>
                </Col>
            </Row>
            <Row>
                <Col xs="6" sm="4"></Col>
            </Row>
        </>
    )
}

export default TendenciaServer