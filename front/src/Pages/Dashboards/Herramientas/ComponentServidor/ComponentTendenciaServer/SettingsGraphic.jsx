import React from 'react'

export const dataConfig = (data,axisy) =>{
    return ({
        data: data,
      
        height:400,
        zoomEnabled: true,
        exportEnabled: true,
        animationEnabled: false, 
      
        // toolTip: {
        //     shared: true,
        //     contentFormatter: function (e) {
        //         var content = " ";
        //         for (var i = 0; i < e.entries.length; i++){
        //             content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
        //         } 
        //         content +=   "<br/> " ;
        //         for (let i = 0; i < e.entries.length; i++) {
        //             // eslint-disable-next-line no-useless-concat
        //             content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
        //             content += "<br/>";
        //         }
        //         return content;
        //     }
        // }
        
        legend: {
            horizontalAlign: "center", 
            cursor: "pointer",
            fontSize: 11,
            itemclick: (e) => {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }
                this.setState({renderChart:!this.state.renderChart});   
                
            }
        }, 
        axisX:{
            valueFormatString:  "DDMMM HH:mm",
            labelFontSize: 10
    
        },
        axisY:axisy
    //     axisY :
    //        {title:active_volt?"Volt":"mg/L - °C"},
    //     axisY2 :{
    //        title:"% - PSU"
    //    },   
      
    })
}

export const dataChart=(name,mediciones,chartcolor)=>{
    return({
        axisYIndex:0,
        axisYType: "primary",
        type: "spline",
        legendText: name,
        name: name,
        color:chartcolor,
        dataPoints : mediciones.map(medicion=>{
            var fecha=new Date(medicion.dateTime);
            var zona_horaria=new Date(medicion.dateTime).getTimezoneOffset();
            zona_horaria=zona_horaria/60;
            fecha.setHours(fecha.getHours()+zona_horaria);
            return {y:medicion[name], x:fecha.getTime()}
        }),
        xValueType: "dateTime",
        indexLabelFontSize:"30",
        showInLegend: true,
        markerSize: 0,
        lineThickness: 3
    })
}

export const dataAxisY=(check,unity,chartcolor)=>{
    return({    
        labelFontSize: 11,                   
        lineColor: chartcolor,
        tickColor: chartcolor,
        labelFontColor: chartcolor,
        titleFontColor: chartcolor,
        suffix: " "+ unity,
        includeZero: check
    })
}