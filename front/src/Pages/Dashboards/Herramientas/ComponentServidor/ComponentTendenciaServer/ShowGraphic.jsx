import React, { useEffect,useState } from 'react'
import { Col } from 'reactstrap'
import { ResponsiveContainer } from 'recharts'
import CanvasJSReact from '../../../../../assets/js/canvasjs.react';

const ShowGraphic = ({optionChart}) => {
    const CanvasJSChart = CanvasJSReact.CanvasJSChart;
    const [option, setOption] = useState(optionChart)
    // useEffect(() => {
    //     console.log(optionChart)
    // }, [option])
    
    
    return (
        <Col md={12} lg={12}> 
            <ResponsiveContainer height='100%' width='100%' >
                <CanvasJSChart options = {option} className="altografico  "  />
            </ResponsiveContainer>   
        </Col> 
    )
}

export default ShowGraphic