import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import {
  toast
} from 'react-toastify';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Form, Label, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,
    Modal, ModalHeader, ModalBody, ModalFooter,CardTitle,Badge} from 'reactstrap';
import moment from 'moment';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';

import ReactTable from "react-table";

import {connect} from 'react-redux';
import { setBackgroundColor } from '../../../../reducers/ThemeOptions';
import CardFooter from 'reactstrap/lib/CardFooter';


//let Seleccion={fecha_inicio:"",fecha_fin:"",centro:"",centro_id:"",sensor:"",sensor_id:"",variable:""};
let centro="";
let sensor="";
let variable="";
let centro_usuario="";
let usuario_id="";
let nombres_usuarios="";
let correos_usuarios="";
let nombre_centros="";
let id_centro="0";
let id_usuario="0";
let filtroMonit="";
let filtroMyMonit="";
let filtroControl="";
let filtroMyControl="";
//let Seleccionadas=[];


class UsuarioCentros extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: true,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Divices:[],
          Usuario:[],
          Usuarios:[],
          Usuarios_Centros:[],
          CentrosMonitoreo:[],
          CentrosControl:[],
          MyCentrosMonitoreo:[],
          MyCentrosControl:[],
          Seleccionadas:[],
          modal1: false,
          modal2: false,
          modal3: false,
          selectValue:'0',
          selectValue_usu:'0',
          selectValue_cent:'0',
          Companys:[],
          blocking1:false,
          blocking3:false,
          visible_pass:false,
          admin:false,
          NombreUsuario:"",
          Company:"",
          Nombre:"",
          Clave:"",
          Code:"",
          Accion:"",
          AllCentros:[],
          AllControl:[],
          NombreCompany:"",
          AliasCompany:"",
        };     
        //se requiere Company, NombreUsuario, Nombre, Clave, Code
        this.tagservices = new TagServices();   
        
    }

  
    componentDidMount = () => {
        
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
        this.AllCompanys();
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
    } 
    
    componentWillReceiveProps(nextProps){
      if (nextProps.initialCount && nextProps.initialCount > this.state.count){
        this.setState({
          count : nextProps.initialCount
        });
      }
      //alert(JSON.stringify(nextProps));
      let separador=nextProps.centroUsuario;
      let centro=separador.split('/');
      this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
      //this.getSondas(centro[0].toString());
    }

  
   

    limpiar(){
        nombres_usuarios="";
        correos_usuarios="";
        this.setState({selectValue:'0'});
    }
    eliminar(id){
      //alert(id)
      const EndPointTag = `${API_ROOT}/user/${id}`;
      axios
      .delete(EndPointTag)
      .then(response => {
          if(response.data.statusCode==200){
              //let data = response.data.data;
              let tabla=this.state.Usuarios.filter((data)=>data._id!=id).map((data)=>data);
              this.setState({Usuarios:tabla});
              this.toggleModal2("-1");

              toast['success']('El Usuario ha sido elimiado correctamente', { autoClose: 4000 })
              usuario_id="";
              this.getUsuarios();
              //console.log(response.data.data)
          }else{
              toast['warning']('El Usuario no pudo ser eliminado', { autoClose: 4000 })
          }   
      })
      .catch(error => {
        // console.log(error);
      });
       
    }


    AllCompanys(){
      let data={};
      this.tagservices.crudCompany("",data,"GET").then(response=>{
          if(response.statusCode==200 || response.statusCode==201){
              console.log(response.statusCode)
              this.setState({Companys:response.data})
          }
      }).catch(e=>console.log(e))
    }

    eliminarCompany(id){
      let data={};
      this.tagservices.crudCompany(id,data,"DELETE").then(response=>{
          if(response.statusCode==200 || response.statusCode==201){
              toast['success']('La Empresa ha sido elimiada correctamente', { autoClose: 4000 })
              this.AllCompanys();
          }
      }).catch(e=>console.log(e))
    }

    render() {
        //dataExport
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Herramientas</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Empresas</BreadcrumbItem>
                      {/* <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      </Breadcrumb>
                     <Row>  
                                         
                        {/* <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                    <Row>
                                        <Col md={12}  lg={3}>
                                            <ButtonGroup style={{width:`${100}%`}}>
                                            <Input value={this.state.selectValue} type="select" id="centro" onChange={((e)=>{
                                                let index = e.target.selectedIndex;
                                                nombre_centros=e.target.options[index].text;
                                                centro=e.target.value+"/"+e.target.options[index].text;
                                                this.setState({selectValue:e.target.value})
                                            })}>
                                              <option disabled selected value="0">FILTRAR POR EMPRESA</option>
                                              {this.state.Companys.map((data,i)=>{
                                                return <option 
                                                value={data._id}>{data.name.toUpperCase()}</option>
                                              })}
                                            </Input>
                                            <Button color="success" onClick={(()=>{
                                              this.toggleModal3()
                                            })}>+</Button>
                                            </ButtonGroup>
                                        </Col>
                                        <Col md={12}  lg={3}>
                                            <Input value={nombres_usuarios} placeholder="FILTRAR POR NOMBRE..." onChange={e => {
                                                nombres_usuarios=e.target.value;
                                                this.inputChangeHandler(e);
                                                }} />
                                        </Col>
                                        <Col md={12}  lg={3}>
                                            <Input value={correos_usuarios} placeholder="FILTRAR POR CORREO..." onChange={e => {
                                                correos_usuarios=e.target.value;
                                                this.inputChangeHandler(e);
                                                }} />
                                        </Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            this.limpiar();
                                                             }}
                                                >limpiar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                            
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            this.setState({selectValue_usu:'0',selectValue_cent:'0'})
                                                            this.getCentros("");
                                                            this.toggleModal1("");
                                                        }}
                                                >Agreagar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>


                                    </Row>
                                </CardBody>
                            </Card>

                        </Col> */}
                        
                        {this.state.Companys.map((data)=>{
                          //let url=data.logo.split('imagenes/')
                          let url=data.name
                          const hostname = window && window.location && window.location.hostname;
                            try{
                              //return <a style={{cursor:"pointer"}} href="#/dashboards/resumen"><img src={require(`../../assets/logos/empresa/${data.name}.png`)} style={{display:visible?"none":"block"}} width="200" height="50"></img></a>
                              return <Col md="3">
                                <Card className="p-10 m-10"  style={{backgroundColor:"#545cd8",marginTop:10}}>
                                  <CardHeader><i className="pe-7s-close-circle" 
                                    style={{fontSize:25,marginRight:25,color:"red",cursor:"pointer"}}
                                    onClick={(()=>{
                                      this.eliminarCompany(data._id)
                                    })}></i> {data.name}</CardHeader>                      
                                  <a style={{cursor:"pointer"}} href="#/dashboards/resumen">
                                  <CardBody>
                                    {/* {console.log(url)} */}
                                    <img src={hostname.includes("localhost")?require(`../../../../../doc/imagenes/${url}.png`):
                                    `https://ihc.idealcontrol.cl/idealcloud2/doc/imagenes/${url}.png`} width="200" height="50"></img>
                                  </CardBody> 
                                  </a>        
                                  <CardFooter>Alias: {data.alias}</CardFooter>  
                                </Card>
                              </Col>
                            }catch(e){
                                console.log(e)
                            }
                        })}
                        

                    </Row>
                      

                      {/* <Modal isOpen={this.state.modal2} toggle={() => this.toggleModal2("-1")} className={this.props.className} style={{marginTop:80}}>
                        <BlockUi tag="div" blocking={this.state.blocking2} loader={<Loader active type={"ball-triangle-path"}/>}>
                          <ModalBody>

                          <Row>     
                              <Col xs="3" md="3" lg="3"></Col>
                              <Col  xs="12" md="12" lg="12">
                                  <Card className="main-card mb-3" style={{marginTop:20}}>
                                      <CardBody>
                                          <Form>
                                          <div style={{textAlign:"center"}}>
                                          <h2>¿Estás seguro?</h2>
                                          <br />
                                          <h3>¡No podrás revertir esto!</h3>
                                          </div>
                                          </Form>
                                      </CardBody>
                                  </Card>
                              </Col>
                              
                          </Row>
                          
                          </ModalBody>
                          <ModalFooter>
                          
                                  <Button color="link" onClick={() => this.toggleModal2("-1")}>Cancel</Button> 
                                  <Button color="danger"  onClick={() => this.eliminar(usuario_id)}>Eliminar <i className="pe-7s-trash btn-icon-wrapper"></i></Button>{' '}
                          
                          </ModalFooter>
                        </BlockUi>
                      </Modal> */}

                     
                      {/* <Label for="setpointOx" className="m-0">Centros</Label>
                          <Input value={this.state.selectValue_cent}  type="select" id="centro" onChange={((e)=>{
                              let index = e.target.selectedIndex;
                              //centro=e.target.value+"/"+e.target.options[index].text;
                              id_centro=e.target.value;
                              //this.getSondas(e.target.value);
                              this.setState({selectValue_cent:e.target.value})
                          })}>
                              <option disabled selected value="0">SELECCIONE UN CENTRO</option>
                              {centros.map((data,i)=>{
                                  return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.code}>{data.name}</option>
                              })}
                          </Input> */}
                          
                          {/* <Input value={this.state.selectValue_usu} type="select" id="dispositivo" onChange={((e)=>{
                              let index = e.target.selectedIndex;
                              //sensor=e.target.value+"/"+e.target.options[index].text;
                              id_usuario=e.target.value;
                              this.setState({selectValue_usu:e.target.value})
                          })}>
                          <option disabled selected value="0">SELECCIONE UN USUARIO</option>
                          {this.state.Usuarios.map((data,i)=>{
                              let name=data.name;
                              name=name.toUpperCase();
                              return <option value={data.id}>{name}</option>
                          })}
                          </Input> */}
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UsuarioCentros);
