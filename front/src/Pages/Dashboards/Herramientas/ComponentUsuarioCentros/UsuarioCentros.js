import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import {
  toast
} from 'react-toastify';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Form, Label, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,
    Modal, ModalHeader, ModalBody, ModalFooter,CardTitle,Badge} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");
let centros=JSON.parse(sessionStorage.getItem("Centros"));
//let Seleccion={fecha_inicio:"",fecha_fin:"",centro:"",centro_id:"",sensor:"",sensor_id:"",variable:""};
let centro="";
let sensor="";
let variable="";
let centro_usuario="";
let usuario_id="";
let nombres_usuarios="";
let correos_usuarios="";
let nombre_centros="";
let id_centro="0";
let id_usuario="0";
let filtroMonit="";
let filtroMyMonit="";
let filtroControl="";
let filtroMyControl="";
//let Seleccionadas=[];


class UsuarioCentros extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: true,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Divices:[],
          Usuario:[],
          Usuarios:[],
          Usuarios_Centros:[],
          CentrosMonitoreo:[],
          CentrosControl:[],
          MyCentrosMonitoreo:[],
          MyCentrosControl:[],
          Seleccionadas:[],
          modal1: false,
          modal2: false,
          modal3: false,
          selectValue:'0',
          selectValue_usu:'0',
          selectValue_cent:'0',
          Companys:[],
          blocking1:false,
          blocking3:false,
          visible_pass:false,
          admin:false,
          NombreUsuario:"",
          Company:"",
          Nombre:"",
          Clave:"",
          Code:"",
          Accion:"",
          AllCentros:[],
          AllControl:[],
          NombreCompany:"",
          AliasCompany:"",
        };     
        //se requiere Company, NombreUsuario, Nombre, Clave, Code
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);    
        this.handleSubmit = this.handleSubmit.bind(this);    
        
    }

  
    componentDidMount = () => {
        this.getUsuarios();
        //this.getUsuarios_Centros();
        this.getRoles();
        this.getCompanys();
        this.getCentros("");
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
    } 

    getCompanys(){
      try{
        let data={};
        this.tagservices.crudCompany("",data,"GET").then(respuesta=>{
          this.setState({Companys:respuesta.data})
        })
        .catch(e=>{
          this.setState({Companys:[]})
          console.log(e)
        })
      }catch(e){
        //console.log(e)
      }
      
    }

    getCentros(id){
      try{
        let data={};
        this.tagservices.crudCentro("",data,"GET").then(respuesta=>{
          let data=respuesta.data;
            let monitoreo=data.filter(d=>!d.name.includes(' Control'))
            let control=data.filter(d=>d.name.includes(' Control'))
            if(id!=""){
              this.setState({AllCentros:monitoreo.map(c=>{
                              return {
                              active: c.active,
                              code: c.code,
                              companyId: c.companyId,
                              name: c.name+"-"+this.getCompanyById(c.companyId),
                              }
                            })
                            ,AllControl:control.map(c=>{
                                return {
                                active: c.active,
                                code: c.code,
                                companyId: c.companyId,
                                name: c.name+"-"+this.getCompanyById(c.companyId),
                                }
                              }),blocking:false})
            }else{
              this.setState({CentrosMonitoreo:monitoreo.map(c=>{
                            return {
                            active: c.active,
                            code: c.code,
                            companyId: c.companyId,
                            name: c.name+"-"+this.getCompanyById(c.companyId),
                            }
                          }),CentrosControl:control.map(c=>{
                            return {
                            active: c.active,
                            code: c.code,
                            companyId: c.companyId,
                            name: c.name+"-"+this.getCompanyById(c.companyId),
                            }
                          }), 
                          MyCentrosMonitoreo:[],MyCentrosControl:[],AllCentros:monitoreo.map(c=>{
                            return {
                            active: c.active,
                            code: c.code,
                            companyId: c.companyId,
                            name: c.name+"-"+this.getCompanyById(c.companyId),
                            }
                          }),AllControl:control.map(c=>{
                            return {
                            active: c.active,
                            code: c.code,
                            companyId: c.companyId,
                            name: c.name+"-"+this.getCompanyById(c.companyId),
                            }
                          }),blocking:false})
            }
            
        })
      }catch(e){
        // console.log(e)
      }
     
    }

    componentWillReceiveProps(nextProps){
      if (nextProps.initialCount && nextProps.initialCount > this.state.count){
        this.setState({
          count : nextProps.initialCount
        });
      }
      //alert(JSON.stringify(nextProps));
      let separador=nextProps.centroUsuario;
      let centro=separador.split('/');
      this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
      //this.getSondas(centro[0].toString());
    }

      getUsuarios=()=>{
        const EndPointTag = `${API_ROOT}/user`;
        axios
        .get(EndPointTag)
        .then(response => {

            let data = response.data.data;
            //console.log(response.data.data)

          //this.setState({isLoading: false,Tags: data});   
          this.setState({Usuarios: data});
        })
        .catch(error => {
          // console.log(error);
        });
    }

    getUsuario=(id)=>{
        const EndPointTag = `${API_ROOT}/user/${id}`;
        axios
        .get(EndPointTag)
        .then(response => {
            let data = response.data.data;
            let monit=data.centros.split(',');
            let company=this.state.Companys.filter(c=>c._id==data.companyId)
            let adm=false
            // console.log(data.roleId=="5f6e1ac01a080102d0e268c1")
            if(data.roleId=="5f6e1ac01a080102d0e268c1"){
              adm=false;
            }else{
              adm=true;
            }
            let centros=[];
            let all_centros=this.state.AllCentros;
            let all_control=this.state.AllControl;
            monit.map(m=>{
              if(this.state.AllCentros.filter(a=>a.code==m)[0]!=undefined){
                centros.push(this.state.AllCentros.filter(a=>a.code==m)[0])
                let index = all_centros.findIndex(item => item.code === m);
                if(index > -1){
                  all_centros.splice(index, 1);
                }
              }
            })
            if(data.control!=undefined&&data.control!=null&&data.control!=""){
              let centros_control=[];
              let control=data.control.split(',');
              control.map(m=>{
                if(this.state.AllControl.filter(a=>a.code==m)[0]!=undefined){
                  centros_control.push(this.state.AllControl.filter(a=>a.code==m)[0])
                  let index = all_control.findIndex(item => item.code === m);
                  if(index > -1){
                    all_control.splice(index, 1);
                  }
                }
                this.asignarControl(m)
              })
              this.setState({Company:data.companyId, NombreUsuario:data.username, 
                          Nombre:data.name, Clave:"", Code:String(data.code).replace(company[0].alias,''),
                          MyCentrosMonitoreo:centros,CentrosMonitoreo:all_centros,
                          MyCentrosControl:centros_control,CentrosControl:all_control,admin:adm
                        })

              // this.setState({Company:data.companyId, NombreUsuario:data.username, 
              //   Nombre:data.name, Clave:"", Code:String(data.code).replace(company[0].alias,''),
              //   MyCentrosMonitoreo:centros,CentrosMonitoreo:all_centros,admin:adm
              // })
            }else{
              this.setState({Company:data.companyId, NombreUsuario:data.username, 
                          Nombre:data.name, Clave:"", Code:String(data.code).replace(company[0].alias,''),
                          MyCentrosMonitoreo:centros,CentrosMonitoreo:all_centros,admin:adm,
                          MyCentrosControl:[],CentrosControl:all_control,
                        })
            }
            
            //this.asignaControlCodes(monit);
            // monit.map(m=>{
            //   this.asignarMonitoreo(m);
            // })
           
            
            //console.log(response.data.data)

          //this.setState({isLoading: false,Tags: data});   
          this.setState({Usuario: data});
        })
        .catch(error => {
          //console.log(error);
        });
    }

   asignaControlCodes(codes){
    //  codes.map(c=>{
    //    this.setState({MyCentrosMonitoreo:this.state.MyCentrosMonitoreo.concat(this.)})
    //  })
   }

    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }


    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                      
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        isMulti
                        name="colors"
                        options={Tags}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Tag"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    toggleModal1(sala) {
        // console.log(sala);
        if(sala=="-1"){
          this.setState({modal1: !this.state.modal1})
        }else{
          if(sala==""){
            this.vaciarData();
          }else{
            this.getCentros("1");
            this.getUsuario(sala)
          }
          this.setState({modal1: true,Accion:sala})
        }
    }

    toggleModal2(sala) {
      //centro_usuario=sala;
      usuario_id=sala;
      //console.log(sala);
      if (this.state.modal2 === false){ 
        try{
            this.setState({
                modal2: !this.state.modal2
            });
        }catch(e){
            //console.log(e);
        }
      }else{
        this.setState({
            modal2: !this.state.modal2
        });
      }
    }

    toggleModal3(sala) {
      //console.log(sala);
      if(sala=="-1"){
        this.setState({modal3: !this.state.modal3})
      }else{
        this.setState({modal3: true})
      }
  }

  validateData(validacion){
    let aux=true;
    validacion.map(v=>{
      //console.log(this.state[v])
      if(this.state[v]==""){
        aux=false;
      }
    })
    return aux;
  }

  vaciarData(){
    this.setState({ Company:"", NombreUsuario:"", Nombre:"", Clave:"", Code:""})
  }

  agregarUsuario(id){
    let n=0;
    n=this.state.admin?1:0;
    let rol=[];
    let company=this.state.Companys.filter(c=>c._id==this.state.Company)
    if(company[0].alias=="ideal"){
      rol=this.state.Roles.filter(r=>r.code==String(2+1))
    }else{
      rol=this.state.Roles.filter(r=>r.code==String(n+1))
    }
    
    // console.log(rol[0].name)
    // console.log(this.state.Company)
    let validados="";
    let EndPoint="";
    let metod="";

    if(id!=""){
      EndPoint=`${API_ROOT}/user/${id}`
      metod="PUT";
      validados="Company,NombreUsuario,Nombre,Code"
    }else{
      EndPoint=`${API_ROOT}/user`
      metod="POST";
      validados="Company,NombreUsuario,Nombre,Clave,Code"
    }
    validados=validados.split(',')
    let monitoreo="";
    let control="";
    this.state.MyCentrosMonitoreo.filter(m=>{
      monitoreo+=m.code+",";
    })
    this.state.MyCentrosControl.filter(m=>{
      control+=m.code+",";
    })

    //console.log(monitoreo)
    //se requiere Company, NombreUsuario, Nombre, Clave, Code
    if(this.validateData(validados)){
      //console.log("No Falta Nada");
      let data={};
      if(this.state.Clave==""){
        data={
          "lastConnection": new Date(),
          "active": true,
          "companyId": String(this.state.Company),
          "email": this.state.NombreUsuario+"@"+this.getNameCompany(this.state.Company,"n")+".cl",
          "name": this.state.Nombre,
          "roleId": String(rol[0]._id),
          "username": this.state.NombreUsuario,
          "code": this.state.Code+this.getNameCompany(this.state.Company,"c"),
          "centros": monitoreo,
          "control": control
        };
      }else{
        data={
          "lastConnection": new Date(),
          "active": true,
          "companyId": String(this.state.Company),
          "email": this.state.NombreUsuario+"@"+this.getNameCompany(this.state.Company,"n")+".cl",
          "name": this.state.Nombre,
          "password": this.state.Clave,
          "roleId": String(rol[0]._id),
          "username": this.state.NombreUsuario,
          "code": this.state.Code+this.getNameCompany(this.state.Company,"c"),
          "centros": monitoreo,
          "control": control
        };
      }
      axios({method:metod, url:EndPoint, data})
      .then(response => {
          let data = response.data;
          if(data.statusCode==200 || data.statusCode==201){
            if(data.statusCode==200){
              toast['success']('El Usuario fue Actualizado con exito', { autoClose: 4000 })
            }else{
              toast['success']('El Usuario fue Registrado con exito', { autoClose: 4000 })
            }
            this.vaciarData();
            this.getUsuarios();
            this.toggleModal1('-1');
          }
      })
      .catch((e)=>e);
    }else{
      toast['warning']('Porfavor no deje campos vacios', { autoClose: 4000 })
    }

  }

  getNameCompany(id,type){
    let data=this.state.Companys.filter(c=>c._id==id)
    if(type=="n"){
      return data[0]==undefined?"":data[0].name;
    }else{
      return data[0]==undefined?"":data[0].alias;
    }
    
  }

    limpiar(){
        nombres_usuarios="";
        correos_usuarios="";
        this.setState({selectValue:'0'});
    }
    eliminar(id){
      //alert(id)
      const EndPointTag = `${API_ROOT}/user/${id}`;
      axios
      .delete(EndPointTag)
      .then(response => {
          if(response.data.statusCode==200){
              //let data = response.data.data;
              let tabla=this.state.Usuarios.filter((data)=>data._id!=id).map((data)=>data);
              this.setState({Usuarios:tabla});
              this.toggleModal2("-1");

              toast['success']('El Usuario ha sido elimiado correctamente', { autoClose: 4000 })
              usuario_id="";
              this.getUsuarios();
              //console.log(response.data.data)
          }else{
              toast['warning']('El Usuario no pudo ser eliminado', { autoClose: 4000 })
          }   
      })
      .catch(error => {
        // console.log(error);
      });
      
       
    }

    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    handleChange = event => {
      const name = event.target.name;
      const value = event.target.value;
      // console.log(value)
      this.setState({ [name]: value });
    }

    getCompanyById(id){
      try{
        let company=this.state.Companys.filter(c=>c._id==id)
        return company[0].name
      }catch(e){
        // console.log(e)
      }
      
    }
    
    asignarMonitoreo(code){
      let new_centros=this.state.CentrosMonitoreo.filter(c=>c.code==code)
      this.setState({MyCentrosMonitoreo:this.state.MyCentrosMonitoreo.concat(new_centros),
                     CentrosMonitoreo:this.state.CentrosMonitoreo.filter(c=>c.code!=code)})
    }

    asignarControl(code){
      let new_centros=this.state.CentrosControl.filter(c=>c.code==code)
      this.setState({MyCentrosControl:this.state.MyCentrosControl.concat(new_centros),
                     CentrosControl:this.state.CentrosControl.filter(c=>c.code!=code)})
    }

    quitarMonitoreo(code){
      let new_centros=this.state.MyCentrosMonitoreo.filter(c=>c.code==code)
      this.setState({CentrosMonitoreo:this.state.CentrosMonitoreo.concat(new_centros),
                     MyCentrosMonitoreo:this.state.MyCentrosMonitoreo.filter(c=>c.code!=code)})
    }

    quitarControl(code){
      let new_centros=this.state.MyCentrosControl.filter(c=>c.code==code)
      this.setState({CentrosControl:this.state.CentrosControl.concat(new_centros),
                     MyCentrosControl:this.state.MyCentrosControl.filter(c=>c.code!=code)})
    }

    getRoles(){
      this.tagservices.getRole().then(response=>{
        this.setState({Roles:response.data})
      })
      .catch(e=>console.log(e))
    }

    toggleAdmin(value){
      this.setState({admin:value})
    }
 
    handleSubmit(event) {
      event.preventDefault();
      const {menu1}=this.state;
      const fileInput = document.querySelector("#fileInput");
      const formData = new FormData();
      let name=String(fileInput.value)
      let validation=false;
      if(name.includes(".png") || name.includes(".PNG")){
        validation=true;
      }

      if(this.state.NombreCompany!="" && this.state.AliasCompany!="" && fileInput.files[0]!=null){
        if(validation){
           // let type=this.getType();
            // type=type.toLowerCase();
            formData.append("file", fileInput.files[0]);
            formData.append("alias", this.state.AliasCompany);
            formData.append("name", this.state.NombreCompany);
            formData.append("active", true);
          axios
          .post(API_ROOT+`/company`,formData)
          .then(response => {   
            if(response.data.statusCode===201){
              document.querySelector("#fileInput").value="";
              this.setState({NombreCompany:"",AliasCompany:""})
              this.toggleModal3("-1")
              toast['success']('La Empresa fue creada correctamente', { autoClose: 4000 })
              
            }
          })
          .catch(error => {
              console.log(error)
          });
        }else{
          toast['warning']('El formato de imagen es incorrecto, Debe ser .PNG o .png', { autoClose: 4000 })
        }
      }else{
        toast['warning']('Formulario incompleto', { autoClose: 4000 })
      }
        
    }

    render() {
        //dataExport
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Herramientas</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Usuario Centros</BreadcrumbItem>
                      {/* <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                    <Row>
                                        <Col md={12}  lg={3}>
                                            <ButtonGroup style={{width:`${100}%`}}>
                                            <Input value={this.state.selectValue} type="select" id="centro" onChange={((e)=>{
                                                let index = e.target.selectedIndex;
                                                nombre_centros=e.target.options[index].text;
                                                //alert(nombre_centros);
                                                centro=e.target.value+"/"+e.target.options[index].text;
                                                this.setState({selectValue:e.target.value})
                                                // this.getSondas(e.target.value);
                                            })}>
                                              <option disabled selected value="0">FILTRAR POR EMPRESA</option>
                                              {this.state.Companys.map((data,i)=>{
                                                return <option 
                                                //style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} 
                                                value={data._id}>{data.name.toUpperCase()}</option>
                                              })}
                                            </Input>
                                            <Button color="success" onClick={(()=>{
                                              this.toggleModal3()
                                            })}>+</Button>
                                            </ButtonGroup>
                                        </Col>
                                        <Col md={12}  lg={3}>
                                            <Input value={nombres_usuarios} placeholder="FILTRAR POR NOMBRE..." onChange={e => {
                                                nombres_usuarios=e.target.value;
                                                this.inputChangeHandler(e);
                                                }} />
                                        </Col>
                                        <Col md={12}  lg={3}>
                                            <Input value={correos_usuarios} placeholder="FILTRAR POR CORREO..." onChange={e => {
                                                correos_usuarios=e.target.value;
                                                this.inputChangeHandler(e);
                                                }} />
                                        </Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            this.limpiar();
                                                             }}
                                                >limpiar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                            
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            // this.setState({
                                                            //     dataExport:[],
                                                            //     buttonExport:'block'
                                                            // });
                                                            this.setState({selectValue_usu:'0',selectValue_cent:'0'})
                                                            this.getCentros("");
                                                            this.toggleModal1("");
                                                        }}
                                                >Agreagar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>


                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                            {
                            <Card className="p-10 m-10" >                                 
                            <BlockUi tag="div" blocking={this.state.blocking} loader={<Loader active type={"ball-triangle-path"}/>}>            
                            <ReactTable
                                style={{height:600}}                                                       
                                //data={this.state.Seleccionadas}
                                data={this.state.Usuarios
                                    .filter((data)=>{
                                        if(this.state.selectValue!=0 && nombres_usuarios!="" && correos_usuarios!=""){
                                            return data.name.toLocaleUpperCase().includes(nombres_usuarios.toLocaleUpperCase())&&data.companyId==this.state.selectValue
                                            &&data.email.toLocaleUpperCase().includes(correos_usuarios.toLocaleUpperCase())
                                        }else if(nombres_usuarios!=""){
                                            return data.name.toLocaleUpperCase().includes(nombres_usuarios.toLocaleUpperCase())
                                        }else if(correos_usuarios!=""){
                                          return data.email.toLocaleUpperCase().includes(correos_usuarios.toLocaleUpperCase())
                                        }
                                        else if(this.state.selectValue!=0){
                                            return data.companyId==this.state.selectValue
                                        }else{
                                             return data;
                                        }
                                        
                                    })
                                    .map((data)=>data)}
                      
                                //loading={true}
                                showPagination= {true}
                                showPaginationTop= {false}
                                showPaginationBottom= {true}
                                showPageSizeOptions= {false}
                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                defaultPageSize={10}
                                columns={[
                                        {
                                          Header: "CODE",
                                          width: 200,
                                          accessor: "code",
                                          Cell: ({ original }) => (
                                              <div style={{marginLeft:`${40}%`}}>
                                                {(original.code)}
                                              </div>
                                            )
                                        },
                                        {
                                          Header: "USUARIOS",
                                          accessor: "name",
                                          Cell: ({ original }) => (
                                              <div style={{marginLeft:`${40}%`}}>
                                                {(original.name).toUpperCase()}
                                              </div>
                                            )
                                        },
                                        {
                                          Header: "Correos",
                                          accessor: "correos",
                                          Cell: ({ original }) => (
                                              <div style={{marginLeft:`${40}%`}}>
                                                {(original.email)}
                                              </div>
                                            )
                                        },
                                        {
                                            width: 300,
                                            Header: "Acciones",
                                            Cell: ({ original }) => (
                                              <Fragment>
                                                {/* <Button color="primary"   onClick={(()=>{this.toggleModal3(original.id)})}>
                                                  Centros
                                                </Button>
                                                 &nbsp; */}
                                                <Button color="primary" style={{marginLeft:`${30}%`}} onClick={(()=>{
                                                  this.toggleModal1(original._id)
                                                  })}>
                                                  Editar
                                                </Button>
                                                 &nbsp;
                                                <Button color="danger" outline value={original._id} onClick={((e)=>{
                                                  //centro_usuario=original.id;
                                                  usuario_id=original._id;
                                                  this.toggleModal2(original._id);
                                                })} >
                                                  Quitar
                                                </Button>
                                               
                                                
                                              </Fragment>
                                            )
                                        }
                                    ]                                                             
                                }
                                
                                className="-striped -highlight"
                                />
                            </BlockUi>
                            </Card>
                            }
                        </Col>
                        
                        <Col>
                        
                        </Col>

                    </Row>
                      <Modal backdrop="static" isOpen={this.state.modal1} toggle={() => this.toggleModal1("-1")}  style={{marginTop:80,maxHeight:660,maxWidth:800}}>
                      <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                        <ModalHeader toggle={() => this.toggleModal1("-1")}>{this.state.Accion==""?"Agregar":"Editar"} Usuarios</ModalHeader>
                        <ModalBody>

                          <Row>     
                              <Col xs="3" md="3" lg="3"></Col>
                              <Col  xs="12" md="12" lg="12">
                                  <Card className="main-card mb-3">
                                      <CardBody>
                                          <CardTitle>Formulario de Registro</CardTitle>
                                          <Form>
                                              <FormGroup row>
                                                <div style={{marginTop:10}}></div>
                                                <Col size="4">
                                                  <Label for="" className="m-0">Empresa</Label>
                                                  <Input type="select" id={"txtCompany"} value={this.state["Company"]}  onChange={this.handleChange}
                                                    name={"Company"}>
                                                    <option disabled value="" selected>Seleccione una Empresa</option>
                                                    {this.state.Companys.map(c=>{
                                                      return <option value={c._id}>{c.name}</option>
                                                    })}
                                                  </Input>
                                                </Col>
                                                <Col size="4">
                                                  <Label for="" className="m-0" >Nombre</Label>
                                                  <Input autocomplete="off" id={"txtNombre"} value={this.state["Nombre"]}  onChange={this.handleChange}
                                                    name={"Nombre"}></Input>
                                                </Col>
                                                <Col size="4">
                                                  <Label for="" className="m-0">Code</Label>
                                                  <ButtonGroup>
                                                    <Input style={{maxWidth:`${80}%`}} autocomplete="off" id={"txtCode"} value={this.state["Code"]} 
                                                      onChange={this.handleChange} name={"Code"}></Input>
                                                    <Button color={this.state.admin?"primary":"secondary"} onClick={(()=>{
                                                      if(this.state.admin){
                                                        this.toggleAdmin(0);
                                                      }else{
                                                        this.toggleAdmin(1);
                                                      }
                                                    })}>Admin</Button>
                                                  </ButtonGroup>
                                                </Col>
                                              </FormGroup>
                                              <br></br>
                                              <FormGroup row>
                                                <Col size="3">
                                                  <Label for="" className="m-0">Nombre de Usuario</Label>
                                                  <Input type="text" autocomplete="off" id={"txtNombreUsuario"} value={this.state["NombreUsuario"]} 
                                                   onChange={this.handleChange} name={"NombreUsuario"}></Input>
                                                </Col>
                                                <Col size="3">
                                                  <Label for="" className="m-0">Correo</Label>
                                                  <Input for="" disabled 
                                                  value={this.state.NombreUsuario+"@"+this.getNameCompany(this.state.Company,"n")+".cl"}></Input>
                                                  {/* <Input type="email" autocomplete="off" id={"txtCorreo"} 
                                                    value={this.state["Correo"]}  onChange={this.handleChange} name={"Correo"}></Input> */}
                                                </Col>
                                                <Col size="3">
                                                  <Label for="" className="m-0">Clave</Label>
                                                  <ButtonGroup>
                                                    <Input type={this.state.visible_pass?"text":"password"} autocomplete="off" id={"txtClave"} 
                                                    value={this.state["Clave"]}  onChange={this.handleChange} name={"Clave"}></Input>
                                                    <Button color={this.state.visible_pass?"primary":"secondary"}
                                                    onClick={(()=>{
                                                        this.setState({visible_pass:!this.state.visible_pass})
                                                    })}
                                                    ><span className="icon-eye"></span></Button>
                                                  </ButtonGroup>
                                                </Col>
                                              </FormGroup>
                                              <br></br>
                                                <CardTitle>Monitoreo</CardTitle>
                                              <FormGroup row>
                                                <Col size="6">
                                                  <Label for="" className="m-0">Centros Agregados</Label>
                                                  <Input value={filtroMyMonit} placeholder="FILTRAR CENTROS AGREGADOS..." onChange={e => {
                                                    filtroMyMonit=e.target.value;
                                                    this.inputChangeHandler(e);
                                                  }} />
                                                  <Input type="select" name="select" id="exampleSelect" multiple>
                                                    {
                                                      this.state.MyCentrosMonitoreo.filter(c=>c.name.toLocaleLowerCase().includes(filtroMyMonit.toLocaleLowerCase())).map(c=>{
                                                        return <option value={c.code} onClick={(()=>{
                                                          this.quitarMonitoreo(c.code);
                                                        })} style={{color:`${c.active?"green":"silver"}`}}>{c.name}</option>
                                                      })
                                                    }
                                                    
                                                  </Input>
                                                </Col>
                                                <Col size="6">
                                                  <Label for="" className="m-0">Centros</Label>
                                                  <Input value={filtroMonit} placeholder="FILTRAR CENTROS..." onChange={e => {
                                                    filtroMonit=e.target.value;
                                                    this.inputChangeHandler(e);
                                                  }} />
                                                  <Input type="select" name="select" id="exampleSelect" multiple>
                                                    {
                                                      this.state.CentrosMonitoreo.filter(c=>c.name.toLocaleLowerCase().includes(filtroMonit.toLocaleLowerCase())).map(c=>{
                                                        return <option value={c.code} onClick={(()=>{
                                                          this.asignarMonitoreo(c.code);
                                                        })} style={{color:`${c.active?"green":"silver"}`}}>{c.name}</option>
                                                      })
                                                    }
                                                  </Input>
                                                </Col>
                                              </FormGroup>
                                              <br></br>
                                              <CardTitle>Control</CardTitle>
                                              <FormGroup row>
                                                <Col size="6">
                                                  <Label for="" className="m-0">Centros Agregados</Label>
                                                  <Input value={filtroMyControl} placeholder="FILTRAR CENTROS AGREGADOS..." onChange={e => {
                                                    filtroMyControl=e.target.value;
                                                    this.inputChangeHandler(e);
                                                  }} />
                                                  <Input type="select" name="select" id="exampleSelect" multiple>
                                                    {
                                                      this.state.MyCentrosControl.filter(c=>c.name.toLocaleLowerCase().includes(filtroMyControl.toLocaleLowerCase())).map(c=>{
                                                        return <option value={c.code} onClick={(()=>{
                                                          this.quitarControl(c.code);
                                                        })}>{c.name}</option>
                                                      })
                                                    }
                                                  </Input>
                                                </Col>
                                                <Col size="6">
                                                  <Label for="" className="m-0">Centros</Label>
                                                  <Input value={filtroControl} placeholder="FILTRAR CENTROS..." onChange={e => {
                                                    filtroControl=e.target.value;
                                                    this.inputChangeHandler(e);
                                                  }} />
                                                  <Input type="select" name="select" id="exampleSelect" multiple>
                                                    {
                                                      this.state.CentrosControl.filter(c=>c.name.toLocaleLowerCase().includes(filtroControl.toLocaleLowerCase())).map(c=>{
                                                        return <option value={c.code} onClick={(()=>{
                                                          this.asignarControl(c.code);
                                                        })}>{c.name}</option>
                                                      })
                                                    }
                                                  </Input>
                                                </Col>
                                              </FormGroup>
                                          </Form>
                                      </CardBody>
                                  </Card>
                              </Col>
                              
                          </Row>
                        
                        </ModalBody>
                        <ModalFooter>
                        
                                <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button> 
                                <Button color="primary"  onClick={() => this.agregarUsuario(this.state.Accion)}>{this.state.Accion==""?"Guardar":"Editar"}</Button>{' '}
                        
                        </ModalFooter>
                      </BlockUi>
                      </Modal>

                      <Modal isOpen={this.state.modal2} toggle={() => this.toggleModal2("-1")} className={this.props.className} style={{marginTop:80}}>
                        <BlockUi tag="div" blocking={this.state.blocking2} loader={<Loader active type={"ball-triangle-path"}/>}>
                          {/* <ModalHeader toggle={() => this.toggleModal2("-1")}></ModalHeader> */}
                          <ModalBody>

                          <Row>     
                              <Col xs="3" md="3" lg="3"></Col>
                              <Col  xs="12" md="12" lg="12">
                                  <Card className="main-card mb-3" style={{marginTop:20}}>
                                      <CardBody>
                                          <Form>
                                          <div style={{textAlign:"center"}}>
                                          <h2>¿Estás seguro?</h2>
                                          <br />
                                          <h3>¡No podrás revertir esto!</h3>
                                          </div>
                                          </Form>
                                      </CardBody>
                                  </Card>
                              </Col>
                              
                          </Row>
                          
                          </ModalBody>
                          <ModalFooter>
                          
                                  <Button color="link" onClick={() => this.toggleModal2("-1")}>Cancel</Button> 
                                  <Button color="danger"  onClick={() => this.eliminar(usuario_id)}>Eliminar <i className="pe-7s-trash btn-icon-wrapper"></i></Button>{' '}
                          
                          </ModalFooter>
                        </BlockUi>
                      </Modal>

                      <Modal isOpen={this.state.modal3} toggle={() => this.toggleModal3("-1")} className={this.props.className} style={{marginTop:80}}>
                        <BlockUi tag="div" blocking={this.state.blocking3} loader={<Loader active type={"ball-triangle-path"}/>}>
                        <ModalHeader toggle={() => this.toggleModal3("-1")}>Agregar Empresa</ModalHeader>
                        <ModalBody>

                          <Row>     
                              <Col xs="3" md="3" lg="3"></Col>
                              <Col  xs="12" md="12" lg="12">
                                  <Card className="main-card mb-3">
                                      <CardBody>
                                          <CardTitle>Formulario de Registro</CardTitle>
                                          <Form onSubmit={this.handleSubmit}>
                                              <div style={{marginTop:10}}></div>
                                              <FormGroup row>
                                                  <Col size="6">
                                                    <Input placeholder="Nombre de la Empresa" autocomplete="off"
                                                    value={this.state["NombreCompany"]}  onChange={this.handleChange} name={"NombreCompany"}></Input>
                                                  </Col>
                                                  <Col size="6">
                                                    <Input placeholder="Alias de la Empresa" autocomplete="off"
                                                    value={this.state["AliasCompany"]}  onChange={this.handleChange} name={"AliasCompany"}></Input>
                                                  </Col>
                                                 
                                              </FormGroup>
                                              <div style={{marginTop:15}}></div>
                                              <FormGroup>
                                                <Input type="file" name="file" id="fileInput" className=""></Input>
                                              </FormGroup>
                                              <div style={{marginTop:10}}></div>
                                              <FormGroup>
                                                <Col style={{marginLeft:`${60}%`}}>
                                                  <Button color="link" onClick={() => this.toggleModal3("-1")} >Cancel</Button> 
                                                  <Button color="primary">Guardar</Button>
                                                </Col>
                                              </FormGroup>
                                          </Form>
                                      </CardBody>
                                  </Card>
                              </Col>
                              
                          </Row>
                        
                        </ModalBody>
                        {/* <ModalFooter>
                        
                                <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button> 
                                <Button color="primary">Guardar</Button>{' '}
                        
                        </ModalFooter> */}
                        </BlockUi>
                      </Modal>
                      {/* <Label for="setpointOx" className="m-0">Centros</Label>
                          <Input value={this.state.selectValue_cent}  type="select" id="centro" onChange={((e)=>{
                              let index = e.target.selectedIndex;
                              //centro=e.target.value+"/"+e.target.options[index].text;
                              id_centro=e.target.value;
                              //this.getSondas(e.target.value);
                              this.setState({selectValue_cent:e.target.value})
                          })}>
                              <option disabled selected value="0">SELECCIONE UN CENTRO</option>
                              {centros.map((data,i)=>{
                                  return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.code}>{data.name}</option>
                              })}
                          </Input> */}
                          
                          {/* <Input value={this.state.selectValue_usu} type="select" id="dispositivo" onChange={((e)=>{
                              let index = e.target.selectedIndex;
                              //sensor=e.target.value+"/"+e.target.options[index].text;
                              id_usuario=e.target.value;
                              this.setState({selectValue_usu:e.target.value})
                          })}>
                          <option disabled selected value="0">SELECCIONE UN USUARIO</option>
                          {this.state.Usuarios.map((data,i)=>{
                              let name=data.name;
                              name=name.toUpperCase();
                              return <option value={data.id}>{name}</option>
                          })}
                          </Input> */}
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UsuarioCentros);
