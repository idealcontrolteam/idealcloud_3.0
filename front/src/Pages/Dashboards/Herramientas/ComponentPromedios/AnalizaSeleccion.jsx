import React, { useEffect, useState } from 'react'
import Table from '../../../ComponentGlobal/Table';
import {Row,Col, Card, Button, Input} from 'reactstrap';
import { TagServices } from '../../../../services/comun';
import Tendencia from './Tendencia';
import moment from 'moment';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { PromediarMediciones } from '../../../Hooks/PromediarArray';
import {toast} from 'react-toastify';
import Swal from 'sweetalert2'

const AnalizaSeleccion = ({
    dispositivoSeleccionado0,
    dispositivoSeleccionado1,
    dispositivoSeleccionado2,
    fechaInicio,
    fechaFin,
    analizar,
    setAnalizar
}) => {

    const tagservices=new TagServices();

    const [mediciones1, setMediciones1] = useState([])
    const [mediciones2, setMediciones2] = useState([])

    const [dispositivos, setDispositivos] = useState([])

    const [promediar, setPromediar] = useState(false)

    const [nuevaMedicion, setNuevaMedicion] = useState([])

    useEffect(() => {
        if(dispositivoSeleccionado0 && dispositivoSeleccionado1 && dispositivoSeleccionado2 && analizar){
            console.log(dispositivoSeleccionado0)
            setPromediar(false)
            const locationsId=[
                {locationId:dispositivoSeleccionado1.id},
                {locationId:dispositivoSeleccionado2.id}
            ]
            tagservices.getMeasurementForLocation(
                locationsId,
                moment(fechaInicio).format('YYYY-MM-DDTHH:mm:00'),
                moment(fechaFin).format('YYYY-MM-DDTHH:mm:59')
                ).then(respuesta=>{
                if(respuesta.statusCode==200){
                    setMediciones1(respuesta.data.filter(m=>m.locationId==dispositivoSeleccionado1.id))
                    setMediciones2(respuesta.data.filter(m=>m.locationId==dispositivoSeleccionado2.id))
                    setDispositivos([
                        dispositivoSeleccionado1,
                        dispositivoSeleccionado2
                    ])
                    setAnalizar(false)
                }else{
                    setAnalizar(false)
                }
            })
            // console.log(dispositivoSeleccionado1)
        }
    }, [dispositivoSeleccionado1,dispositivoSeleccionado2,fechaInicio,fechaFin,analizar])

    const generaPromedio=()=>{
        if(mediciones1.filter(m=>m.value==0).length>0 || mediciones2.filter(m=>m.value==0).length>0){
            Swal.fire({
                icon: 'warning',
                title: '¡Cuidado!',
                text: '¡Los valores en cero afectaran el promedio!',
              })
        }
        setPromediar(true)
        setNuevaMedicion(
            PromediarMediciones(
            mediciones1,
            mediciones2,
            dispositivoSeleccionado1.tags,
            dispositivoSeleccionado2.tags,
            dispositivoSeleccionado0.tags,
            )
        )
        //console.log(nuevaMedicion)
        // console.log(mediciones1)
    }
    
    const insertarPromedio=()=>{
        Swal.fire({
            title: '¿Estas seguro de insertar el promedio?',
            text: "Esto generara un registro en las mediciones",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, insertar!'
        }).then((result) => {
            if (result.isConfirmed) {
                tagservices.upDataProm(nuevaMedicion).then(respuesta => {
                    if(respuesta.statusCode === 200 || respuesta.statusCode === 201){
                        toast['success']('Los datos fueron subidos con exito', { autoClose: 4000 })
                    }
                })
            }
        })
    }

    return (
        <>
            <Row style={{marginTop:15}}> 
                <BlockUi style={{marginTop:10,width:`${100}%`}} tag="div" blocking={analizar} 
                    loader={<Loader active type={"ball-triangle-path"}/>}>
                    <Table 
                        data={[
                            {
                                name: dispositivoSeleccionado1.name,
                                cantidad_registros: mediciones1.length,
                                registros_0: mediciones1.filter(m=>m.value==0).length
                            },
                            {
                                name: dispositivoSeleccionado2.name,
                                cantidad_registros: mediciones2.length,
                                registros_0: mediciones2.filter(m=>m.value==0).length
                            }
                        ]}
                        columns={[
                            {
                                Header: "Indice",                                                             
                                width: 200,
                                Cell: ({ row, index }) => (
                                    <>
                                        <p>{index+1}</p>
                                    </>
                                )
                            },
                            {
                            Header: "Dispositivo",
                            accessor: "name"
                            },
                            {
                            Header: "Cantidad de Registros",
                            accessor: "cantidad_registros"
                            },
                            {
                            Header: "Cantidad de Valores 0",
                            accessor: "registros_0",                                                              
                            // width: 140
                            },
                        ]}
                        filas={2}
                    />
                </BlockUi>
            </Row>
            <Row style={{marginTop:10}}>
                <Col></Col>
                <Col>
                    {!promediar?(
                        <Button style={{width:`${100}%`}} color={'primary'} onClick={()=>generaPromedio()}>Promediar Datos</Button>
                    ):(
                        <Button style={{width:`${100}%`}} color={'primary'} onClick={()=>insertarPromedio()}>Insertar Mediciones</Button>
                    )}
                    
                </Col>
                <Col></Col>
            </Row>
            <Row style={{width:`${100}%`}}>
                {mediciones1.length>0 && mediciones2.length>0 && nuevaMedicion.length>0 && dispositivos.length>0 && promediar &&(
                <>  
                    <Tendencia 
                        titulo={`Nueva Medicion ${dispositivoSeleccionado1.id&&dispositivoSeleccionado1.id}`}
                        dispositivos={[dispositivoSeleccionado0]}
                        mediciones={
                            //[]
                            nuevaMedicion
                            //.concat(mediciones1).concat(mediciones2)
                        }
                    />
                    <Tendencia 
                        titulo={'Dispositivo 1'}
                        dispositivos={dispositivos}
                        mediciones={
                            //[]
                            mediciones1
                            //.concat(mediciones1).concat(mediciones2)
                        }
                    />
                    <Tendencia 
                        titulo={'Dispositivo 2'}
                        dispositivos={[dispositivoSeleccionado2]}
                        mediciones={
                            //[]
                            mediciones2
                            //.concat(mediciones1).concat(mediciones2)
                        }
                    />
                </>
                )}
            </Row>
        </>
    )
}

export default AnalizaSeleccion