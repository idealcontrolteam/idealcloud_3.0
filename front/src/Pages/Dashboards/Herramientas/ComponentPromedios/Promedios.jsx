import React, { useState, useEffect } from 'react'
import Menu from '../../MenuPage/MenuPage'
import {Row,Col, Card, Button, Input, CardTitle} from 'reactstrap';
import SeleccionEntreFechas from '../../../ComponentGlobal/SeleccionEntreFechas';
import { TagServices } from '../../../../services/comun';
import AnalizaSeleccion from './AnalizaSeleccion';
import {toast} from 'react-toastify';

const Promedios = () => {
    const listas=[
        {"name":'Herramientas', 'link':'#'},
        {"name":`Promedios`, 'link':'#'},
    ]

    const tagservices=new TagServices();

    const [fechaInicio, setFechaInicio] = useState('')
    const [fechaFin, setFechaFin] = useState('')

    const [fechaInicioDestino, setFechaInicioDestino] = useState('')
    const [fechaFinDestino, setFechaFinDestino] = useState('')

    const [centros, setCentros] = useState([])
    const [centroOrigen, setCentroOrigen] = useState('')
    const [centroDestino, setCentroDestino] = useState('')

    const [dispositivos0, setDispositivos0] = useState([])
    const [dispositivos1, setDispositivos1] = useState([])
    const [dispositivos2, setDispositivos2] = useState([])

    const [dispositivoSeleccionado0, setDispositivoSeleccionado0] = useState("")
    const [dispositivoSeleccionado1, setDispositivoSeleccionado1] = useState("")
    const [dispositivoSeleccionado2, setDispositivoSeleccionado2] = useState("")

    const [tags, setTags] = useState([])
    const [tags2, setTags2] = useState([])

    const [analizar, setAnalizar] = useState(false)
    

    useEffect(() => {
        if(centroOrigen.length==0 && centroDestino.length==0){
            let token=sessionStorage.getItem("token_cloud")
            tagservices.getUserToken(token).then(respuesta=>{
                let centros=respuesta.centros+respuesta.control;
                tagservices.getWorkplaceUser(centros).then(workplace=>{
                    setCentros(workplace)
                })
            })
        }
    }, [centros])

    const onlyUnique=(value, index, self) => { 
        return self.indexOf(value) === index;
    }
    
    const dataDispositivosForIds=(dispositivosIds,array)=>{
        return  dispositivosIds.map(d=>{
            if(array.filter(a=>a.locationId._id==d).length>0){
                return array.filter(a=>a.locationId._id==d)[0]
            }
        })
    }
    
    const selectWorkplace=(value,type)=>{
        type=='origen'?
        setCentroOrigen(value):
        setCentroDestino(value)
        tagservices.getTagsLocationForWorkplace(value).then(respuesta=>{
            if(respuesta.statusCode==200){
                type=='origen'?
                setTags(respuesta.data):
                setTags2(respuesta.data)
                if(respuesta.data.filter(d=>d.locationId==null).length==0){
                    console.log(respuesta.data)
                    console.log(respuesta.data.map(d=>d.locationId._id).filter(onlyUnique))
                    const new_data=dataDispositivosForIds(respuesta.data.map(d=>d.locationId._id).filter(onlyUnique),respuesta.data)
                    if(type=='origen'){
                        setDispositivos1(new_data)
                        setDispositivos2(new_data)
                    }else{
                        setDispositivos0(new_data)
                    }
                }else{
                    //alert(JSON.stringify(centros))
                    toast['warning'](`El centro ${centros.filter(c=>c._id==value)[0].name} tiene dispositivos con variables nulas`, { autoClose: 4000 })
                }
            }
        })
    }

    // const selectWorkplace2=(value)=>{
    //     //type=='origen'?
    //     //setCentroOrigen(value)
    //     setCentroDestino(value)
    //     tagservices.getTagsLocationForWorkplace(value).then(respuesta=>{
    //         if(respuesta.statusCode==200){
    //             setTags2(respuesta.data)
    //             const new_data=dataDispositivosForIds(respuesta.data.map(d=>d.locationId._id).filter(onlyUnique),respuesta.data)
    //             // setDispositivos1(new_data)
    //             // setDispositivos2(new_data)
    //             setDispositivos0(new_data)
    //         }
    //     })
    // }

    const asignTagsToLocation=(valor,n)=>{
        if(n=='0'){
            setDispositivoSeleccionado0({
                id:valor,
                name:tags2.filter(t=>t.locationId._id==valor)[0].locationId.name,
                tags:tags2.filter(t=>t.locationId._id==valor)
            })
        }
        else if(n=='1'){
            setDispositivoSeleccionado1({
                id:valor,
                name:tags.filter(t=>t.locationId._id==valor)[0].locationId.name,
                tags:tags.filter(t=>t.locationId._id==valor)
            })
        }else{
            setDispositivoSeleccionado2({
                id:valor,
                name:tags.filter(t=>t.locationId._id==valor)[0].locationId.name,
                tags:tags.filter(t=>t.locationId._id==valor)})
        }
    }

    const AnalizarDispositivos=()=>{
        if(dispositivoSeleccionado0 && dispositivoSeleccionado1 && dispositivoSeleccionado2){
            setAnalizar(true)
        }else{
            toast['warning']('Por favor seleccione 3 dispositivos', { autoClose: 4000 })
        }
    }
    
    return (
        <>
            <Menu listas={listas}  />
            <Card style={{textAlign: 'left'}}>
                <Row style={{marginTop:15}}>
                    <Col sm={2} md={2}>
                        <CardTitle>Selección de Origen</CardTitle>
                    </Col>
                </Row>
                <Row style={{marginTop:15}}>
                    <Col sm={3} md={3}>
                        <SeleccionEntreFechas
                            fechaInicio={fechaInicio}
                            fechaFin={fechaFin}
                            setFechaInicio={setFechaInicio}
                            setFechaFin={setFechaFin}
                        />
                    </Col>
                    <Col sm={3} md={3}>
                        <Input type={"select"} onChange={(e)=>selectWorkplace(e.target.value,'origen')}>
                            <option selected disabled value={""} >Seleccione Centro Origen</option>
                            {
                                centros.map(c=>{
                                    return<option key={c._id} value={c._id} style={{color:c.active?'green':'',fontSize:18}}>{c.name}</option>
                                })
                            }
                        </Input>
                    </Col>
                    <Col sm={3} md={3}>
                        <Input type={"select"} onChange={(e)=>asignTagsToLocation(e.target.value,'1')}>
                            <option selected disabled value={""}>Seleccione Dispositivo</option>
                            {
                                dispositivos1.map(d=>{
                                    return <option  key={d._id} value={d.locationId._id} id={d.name}>{d.locationId.name}</option>
                                })
                            }
                        </Input>
                    </Col>
                    <Col sm={3} md={3}>
                        <Input type={"select"} onChange={(e)=>asignTagsToLocation(e.target.value,'2')}>
                            <option selected disabled value={""}>Seleccione Dispositivo</option>
                            {
                                dispositivos2.map(d=>{
                                    return <option  key={d._id} value={d.locationId._id} id={d.name}>{d.locationId.name}</option>
                                })
                            }
                        </Input>
                    </Col>
                </Row>
                <Row style={{marginTop:15}}>
                    <Col sm={2} md={2}>
                        <CardTitle>Selección de Destino</CardTitle>
                    </Col>
                </Row>
                <Row style={{marginTop:15}}>
                    <Col sm={3} md={3}>
                        {/* <SeleccionEntreFechas
                            fechaInicio={fechaInicio}
                            fechaFin={fechaFin}
                            setFechaInicio={setFechaInicio}
                            setFechaFin={setFechaFin}
                        /> */}
                    </Col>
                    <Col sm={3} md={3}>
                        <Input type={"select"} onChange={(e)=>selectWorkplace(e.target.value,'destino')}>
                            <option selected disabled value={""} >Seleccione Centro Destino</option>
                            {
                                centros.map(c=>{
                                    return<option key={c._id} value={c._id} style={{color:c.active?'green':'',fontSize:18}}>{c.name}</option>
                                })
                            }
                        </Input>
                    </Col>
                    <Col sm={3} md={3}>
                        <Input type={"select"} onChange={(e)=>asignTagsToLocation(e.target.value,'0')}>
                            <option selected disabled value={""} >Dispositivo a Promediar</option>
                            {
                                dispositivos0.map(d=>{
                                    return <option  key={d._id} value={d.locationId._id} id={d.name}>{d.locationId.name}</option>
                                })
                            }
                        </Input>
                    </Col>
                    <Col sm={1} md={1}></Col>
                    <Col sm={2} md={2}>
                        <Button color={"primary"} style={{width:`${100}%`}} onClick={()=>AnalizarDispositivos()}>Analizar</Button>
                    </Col>
                </Row>
                <AnalizaSeleccion
                    dispositivoSeleccionado0={dispositivoSeleccionado0}
                    dispositivoSeleccionado1={dispositivoSeleccionado1}
                    dispositivoSeleccionado2={dispositivoSeleccionado2}
                    fechaInicio={fechaInicio}
                    fechaFin={fechaFin}
                    analizar={analizar}
                    setAnalizar={setAnalizar}
                />
            </Card>
        </>)
}

export default Promedios