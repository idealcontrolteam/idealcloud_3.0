import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner} from 'reactstrap';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import * as _ from "lodash";
import CardTitle from 'reactstrap/lib/CardTitle';
// import Swal from 'sweetalert2'

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let disabled=false;
let load=false;
let active_config="none";
let active_volt=false;


class Tendencia extends Component {

    
    constructor(props) {
        super(props);
  
        this.state = {       
          Tags:[],
          dataCharts:[],
          dataExport:[],
          Export1:[],
          Export2:[],
          Export3:[],
          Export4:[],
          Export5:[],
          Export6:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          MultiExport:false,
          solo_Oxd:true,
          setPoint:true,
          histerisis:true,
          inyecciones:true,
          alarma_minima:false,
        };     
        this.tagservices = new TagServices();    
        this.filtro1_=this.filtro1_.bind(this);
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        sessionCheck()
        let rol=sessionStorage.getItem("rol");
        if(rol=="5f6e1ac01a080102d0e268c2" || rol=="5f6e1ac01a080102d0e268c3"){
            active_config="block";
        }
        // console.log(this.props.mediciones)
        // console.log(this.props.dispositivos)
        if(this.props.mediciones && this.props.dispositivos){
          this.filtro1_(this.props.mediciones,this.props.dispositivos)
        }
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(this.props.Centro!=null&&this.props.Centro!=undefined){
          let data={NameWorkplace:{id:this.props.Centro,name:this.props.Ncentro}}
          this.setState(data);
        }
        else if(workplace!=null){
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
        }
        
      }

      componentWillUnmount = () => {
        clearInterval(this.loadDataChart_);
        clearInterval(this.filtro1_);
     }  

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        // console.log(this.props.mediciones)
        // console.log(this.props.dispositivos)
        this.filtro1_(this.props.mediciones,this.props.dispositivos)
      }
      
    getMydatachartInyeccion=(data,name,i,color)=>{
      let mydatachart = {
           axisYIndex: i,
           type: "stepLine",
           legendText: name,
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }

    getMydatachartAlarmaMinima=(data,name,i,color)=>{
      let mydatachart = {
           axisYIndex: i,
           type: "stepLine",
           lineDashType: "dash",
           legendText: name,
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }

    getMydatachartHisterisis=(data,name,i,color)=>{
      let mydatachart = {
           axisYIndex: i,
           type: "spline",
           legendText: name,
           lineDashType: "dash",
           name: name,
           color:color,
           dataPoints : data,
           xValueType: "dateTime",
           indexLabelFontSize:"30",
           showInLegend: true,
           markerSize: 0,  
           lineThickness: 3
            }
            i++;
          dataCha.push(mydatachart);
        return
    }

     getMydatachart=(data,name,i,color)=>{
       let mydatachart = {
            axisYIndex: i,
            type: "spline",
            legendText: name,
            name: name,
            color:color,
            dataPoints : data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,  
            lineThickness: 3
             }
             i++;
           dataCha.push(mydatachart);
         return
     }

     getMyaxis=(color,unity)=>{
        let axisy= {    
            id:unity,
            title: "mg/L - °C",
            labelFontSize: 11,                   
            // lineColor: color,
            // tickColor: color,
            // labelFontColor: color,
            // titleFontColor: color,
            // suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

     getMydatachart2=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "spline",
             axisYType: "secondary",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return
      }

      getMyaxis2=(color,unity)=>{
        let axisy= {
            id:unity,
            labelFontSize: 11,                   
            lineColor: color,
            tickColor: color,
            labelFontColor: color,
            titleFontColor: color,
            suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

    filtro1_=(mediciones,dispositivos)=>{
      console.log(dispositivos)
      dispositivos.map((dispositivo,index)=>{
        if(index==0){
          this.filtro_(
            dispositivo.tags,
            mediciones,
            dispositivo
          )
        }
      })
    }

    filtro_=(tags,mediciones,dispositivo)=>{      
      active_volt=false;
      disabled=true;
      this.setState({
          dataExport:[],
          buttonExport:'block',
          MultiExport:false
      });

      dataChaAxisy = [] ;
      dataCha = [] ;
      dataChaExport = [];
      i = 0;
      
      if(dispositivo.name.includes('VoltajeR')){
        active_volt=true;
        tags=tags.filter(l=>l.name.includes('volt')
        )
      }else{
        tags=tags.filter(l=>l.name.includes('oxd')
          ||l.name.includes('temp')
          ||l.name.includes('oxs')
          ||l.name.includes('sal')
          ||l.name.includes('inyection')
          ||l.name.includes('setpoint')
          ||l.name.includes('banda_superior')
          ||l.name.includes('banda_inferior')
          ||l.name.includes('alarma_minima')
        )
      }
      for (let i = 0; i < tags.length; i++) {
        this.setState({blocking: true});
        let sw =0
        if (i=== tags.length-1)
          sw = 1;
        this.loadDataChart_(
          tags[i].shortName,sw,tags[i].workplaceId,
          mediciones.filter(m=>m.tagId==tags[i]._id).map(m=>{
              return {x:this.tagservices.reformatDate(m.dateTime).getTime(),y:m.value}
          })
        );

      }
    }

    loadDataChart_ = (shortName,sw,workplaceId,mediciones) => {   
      const chartcolor= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
      console.log("punto 2")
      let  axisy= {};

      let _dataChartsExport = [];
      // mediciones.map( item => { 
      //   _dataChartsExport.push({ x: moment(item.x).format('DD-MM-YYYY HH:mm'), y : item.y.toString().replace(".",",") }) 
      // });
      load=false;
      let mydatachartExport = {}
      
      if(shortName.includes("inyection")){
        mediciones=mediciones
      }else{
        mediciones=active_config=="block"?mediciones:mediciones.filter(m=>m.y!=0)
      }
      
      if(active_volt){
        this.getMydatachart(mediciones,"Voltaje",3,chartcolor[10]);
        //console.log(chartcolor)
        axisy=this.getMyaxis(chartcolor[0]," ");
        i++;
        mydatachartExport = {  
          unity : "volt",              
          sonda: shortName.toString(),
          measurements : _dataChartsExport
          }
          this.setState({Export6:_dataChartsExport})
      }else{
          if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
            if(shortName.includes("oxd")){
              this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
              axisy=this.getMyaxis(chartcolor[2]," °C");
              i++;
            }
          }else{
            if(shortName.includes("oxd")){
              this.getMydatachart(mediciones,"Oxd",3,chartcolor[0]);
              axisy=this.getMyaxis(chartcolor[0]," ");
              i++;
          }
          mydatachartExport = {  
            unity : "mg/L",              
            sonda: shortName.toString(),
            measurements : _dataChartsExport
            }      
          this.setState({Export1:_dataChartsExport})
        }
        if(this.state.solo_Oxd){
          if(workplaceId=="5fbd33eabca38e0394bb24b8" || workplaceId=="602ef4ba3a885f18fd4b6008" || workplaceId=="5fdcdcbab11a950c53e3575c"){
            if(shortName.includes("oxs")){
              this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
              axisy=this.getMyaxis(chartcolor[2]," °C");
              i++;
              mydatachartExport = {  
                unity : "°C",              
                sonda: shortName.toString(),
                measurements : _dataChartsExport
                }
                this.setState({Export3:_dataChartsExport})
            }
            if(shortName.includes("temp")){
                this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
                axisy=this.getMyaxis2(chartcolor[3]," %");
                i++;
                mydatachartExport = {  
                  unity : "%",              
                  sonda: shortName.toString(),
                  measurements : _dataChartsExport
                  }
                  this.setState({Export2:_dataChartsExport})
            }
          }else{
            if(shortName.includes("temp")){
              this.getMydatachart(mediciones,"Temp",2,chartcolor[1]);
              axisy=this.getMyaxis(chartcolor[2]," °C");
              i++;
              mydatachartExport = {  
                unity : "°C",              
                sonda: shortName.toString(),
                measurements : _dataChartsExport
                }
                this.setState({Export3:_dataChartsExport})
            }
            if(shortName.includes("oxs")){
                this.getMydatachart2(mediciones,"Oxs",1,chartcolor[2]);
                axisy=this.getMyaxis2(chartcolor[3]," %");
                i++;
                mydatachartExport = {  
                  unity : "%",              
                  sonda: shortName.toString(),
                  measurements : _dataChartsExport
                  }
                  this.setState({Export2:_dataChartsExport})
            }
          }
          
          if(shortName.includes("sal")){
              this.getMydatachart2(mediciones,"Sal",0,chartcolor[3]);
              axisy=this.getMyaxis2(chartcolor[4]," PSU");
              i++;
              mydatachartExport = {  
                unity : "PSU",              
                sonda: shortName.toString(),
                measurements : _dataChartsExport
                }
                this.setState({Export4:_dataChartsExport})
          }
          
          //console.log(shortName.includes("Set_Point"));
        }
        if(!this.state.setPoint){
          if(shortName.includes("Set_Point")){
            // console.log(mediciones)
            this.getMydatachart(mediciones,"setPoint",4,chartcolor[4]);
            axisy=this.getMyaxis(chartcolor[4]," setPoint");
            i++;
            mydatachartExport = {  
              unity : "set_point",              
              sonda: shortName.toString(),
              measurements : _dataChartsExport
              }
              this.setState({Export5:_dataChartsExport})
          }
          this.filtrar2(shortName,workplaceId,_dataChartsExport,mydatachartExport)
        }
        if(!this.state.histerisis){
          if(shortName.includes("Banda_Superior")){
            // console.log(mediciones)
            this.getMydatachartHisterisis(mediciones,"bandasuperior",5,chartcolor[9]);
            axisy=this.getMyaxis(chartcolor[9]," bandasuperior");
            i++;
          }
          if(shortName.includes("Banda_Inferior")){
            // console.log(mediciones)
            this.getMydatachartHisterisis(mediciones,"bandainferior",5,chartcolor[9]);
            axisy=this.getMyaxis(chartcolor[9]," bandainferior");
            i++;
          }
        }
        if(!this.state.inyecciones){
          if(shortName.includes("inyection")){
            console.log(mediciones)
            this.getMydatachartInyeccion(mediciones,"inyection",6,chartcolor[10]);
            axisy=this.getMyaxis(chartcolor[10]," inyection");
            i++;
          }
        }
        if(!this.state.alarma_minima){
          if(shortName.includes("Alarma_Minima")){
            console.log(mediciones)
            this.getMydatachartAlarmaMinima(mediciones,"alarma_minima",6,chartcolor[6]);
            axisy=this.getMyaxis(chartcolor[11]," alarma_minima");
            i++;
          }
        }
    }

      this.setState({
        dataAxisy:dataChaAxisy,
        dataCharts:dataCha,
      }); 
      if (sw === 1){
        disabled=false;
        this.setState({blocking: false}); 
      }
    }

    getNameDispositivo(){
      if(this.state.TagsSelecionado!=null){
        return this.state.TagsSelecionado.name
      }else{
        return ""
      }
    }

    getArray(){
      try{
        let oxd=this.state.Export1;
        let oxs=this.state.Export2;
        let temp=this.state.Export3;
        let sal=this.state.Export4;
        let bat=this.state.Export6;
        //console.log(bat)
        
        if(this.state.TagsSelecionado!=null){
          let array=[]
          this.state.Export4.map((d,i)=>{
            if(this.state.TagsSelecionado.name.toLowerCase().includes('salinidad')){
              array.push({x:d.x, y3:d.y})
            }
            else if(this.state.TagsSelecionado.name.includes('VoltajeR')){
              //console.log("holanda")
              array.push({x:d.x, y4:bat[i].y})
            }
            else if(oxd!=null&&oxs!=null&&temp!=null&&sal!=null){
              array.push({
                      x:oxd[i].x,
                      y:oxd[i].y,
                      y1:oxs[i].y,
                      y2:temp[i].y,
                      y3:sal[i].y
                    })
            }else if(oxd==null){
              array.push( {
                x:oxs[i].x,
                y1:oxs[i].y,
                y2:temp[i].y,
                y3:sal[i].y
              })
            }else if(oxs==null){
              array.push({
                y:oxd[i].y,
                y2:temp[i].y,
                y3:sal[i].y
              })
            }else if(temp==null){
              array.push({
                y:oxd[i].y,
                y1:oxs[i].y,
                y3:sal[i].y
              })
            }
            else if(sal==null){
              array.push({
                y:oxd[i].y,
                y1:oxs[i].y,
                y2:temp[i].y,
              })
            }
          })
          return  array.reverse()
        }else{
          return []
        }
      }catch(e){
        console.log(e)
        return []
      }
      
    }

    showTableExport(){
      if(this.state.MultiExport && this.state.buttonExport=="none"&&load==false){
        return "block"
      }else{
        return "none"
      }
    }

    generateExport(){
      if (this.state.TagsSelecionado !== null){
        if(this.state.TagsSelecionado.name.includes('VoltajeR')){
          return<CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
                { label: "FECHA", key: "x" },
                { label: "Bateria (Volt)" , key: "y4" }
                // { label: "Temp (°C)", key: "y2" },
                // { label: "O2 SAT ( %)", key: "y1" },
                // { label: "Sal (PSU)", key: "y3" }
              ]                                         
            }
            data={this.getArray()}
            filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
            className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            target="_blank">
            Exportar Datos
          </CSVLink>
        }
        else if(!this.state.solo_Oxd){
          return<CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
                { label: "FECHA", key: "x" },
                { label: "O2 Disuelto (mg/L)" , key: "y" }
                // { label: "Temp (°C)", key: "y2" },
                // { label: "O2 SAT ( %)", key: "y1" },
                // { label: "Sal (PSU)", key: "y3" }
              ]                                         
            }
            data={this.getArray()}
            filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
            className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            target="_blank">
            Exportar Datos
          </CSVLink>
        }else{
          return<CSVLink                                         
            separator={";"}                                                                   
            headers={
              [
                { label: `${this.getNameDispositivo()}`, key: "NOMBRE" },
                { label: "FECHA", key: "x" },
                { label: "O2 Disuelto (mg/L)" , key: "y" },
                { label: "Temp (°C)", key: "y2" },
                { label: "O2 SAT ( %)", key: "y1" },
                { label: "Sal (PSU)", key: "y3" }
              ]                                         
            }
            data={this.getArray()}
            filename={`${this.state.NameWorkplace.name}_${this.getNameDispositivo()}.csv`}
            className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
            target="_blank">
            Exportar Datos
          </CSVLink>
        }
      }
    }

    generateTable(){
      let columna=[
          {
          Header: "FECHA",
          accessor: "x",
          width: 160
          },
      ]  
      if (this.state.TagsSelecionado !== null){
        if(this.state.TagsSelecionado.name.includes('VoltajeR')){
          //console.log("hola")
          columna.push(
            {
            Header: "Bateria (Volt)",
            accessor: "y4",                                                              
            width: 140
            })
        }
        else if(this.state.solo_Oxd){
          columna.push( {
            Header: "O2 mg/L",
            accessor: "y",                                                              
            width: 140
          })
          columna.push(
            {
            Header: "O2 %",
            accessor: "y1",                                                              
            width: 140
            },
            {
            Header: "°C",
            accessor: "y2",                                                              
            width: 140
            },
            {
            Header: "PSU",
            accessor: "y3",                                                              
            width: 140
            })
        }else if(!this.state.solo_Oxd){
          columna.push( {
            Header: "O2 mg/L",
            accessor: "y",                                                              
            width: 140
          })
        }
      }   
      
      
      
      return <ReactTable                                                           
            data={this.getArray()}
        
            // loading= {false}
            showPagination= {true}
            showPaginationTop= {false}
            showPaginationBottom= {true}
            showPageSizeOptions= {false}
            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
            defaultPageSize={10}
            columns={columna}
            
            className="-striped -highlight"
            />
    }
    
    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

         let optionsChart1 = {
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                {title:active_volt?"Volt":"mg/L - °C"},
             axisY2 :{
                title:"% - PSU"
            },   
            
             }

        return (
          <>
            <Row style={{width:`${100}%`}}>        
              <Col md="12">
                <BlockUi tag="div" blocking={this.state.blocking} 
                loader={<Loader active type={this.state.loaderType}/>}>
                  <Card className="main-card mb-1 p-0">
                    <CardHeader><CardTitle>{this.props.titulo}</CardTitle></CardHeader>
                    <CardBody className="p-3">                                 
                        <Row>
                            <Col   md={12} lg={12}> 
                                <ResponsiveContainer height='100%' width='100%' >
                                    <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                </ResponsiveContainer>   
                            </Col> 
                        </Row>
                    </CardBody>
                  </Card>
                </BlockUi>
              </Col>
            </Row>
            <Row>
              <Col xs="6" sm="4"></Col>
              {/* <Col xs="6" sm="4">
              <Spinner color="primary" style={{ marginTop:`${5}%`,marginLeft:`${50}%`,display:load?"block":"none", width: '2rem', height: '2rem'}}/>
                      <Button color="primary"
                          outline
                          className={"btn-shadow btn-wide btn-outline-2x btn-block"}
                          style={{ display:this.state.buttonExport,marginTop:`${5}%`}}
                          onClick={() => {
                                  load=true;
                                  this.filtrar2();
                                  // console.log(dataExport);
                              }}
                          >Ver Tablas de datos
                      </Button>
              </Col> */}
              <Col sm="4"></Col>
                    {/* {                                 
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="3" key={data.sonda} style={{display:`${this.state.MultiExport?"none":"block"}`}}>
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={data.measurements}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: data.unity,
                                                                    accessor: "y",                                                              
                                                                    width: 140
                                                                    }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         )
        
                            )
                            
                    } */}
                    {/* <Card className="main-card mb-5 mt-3" style={{display:`${this.showTableExport()}`}}>
                        <CardHeader className="card-header-tab  ">
                          <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                              {this.getNameDispositivo()}
                          </div>
                          <div className="btn-actions-pane-right text-capitalize">
                              {this.generateExport()}
                          </div>                                                  
                        </CardHeader>
                        <CardBody className="p-10 m-10" >          
                          {this.generateTable()}
                      </CardBody>
                    </Card> */}
                                       

                    </Row>
            </>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Tendencia);
