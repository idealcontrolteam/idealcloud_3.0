import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import {
  toast
} from 'react-toastify';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';

import ReactTable from "react-table";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import '../../Zonas/style.css';

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");
let centros=JSON.parse(sessionStorage.getItem("Centros"));
//let Seleccion={fecha_inicio:"",fecha_fin:"",centro:"",centro_id:"",sensor:"",sensor_id:"",variable:""};
let centro="";
let sensor="";
let variable="";
let indicie_seleccion=0;
let disabled=false;
let disabled_graficar=false;
//let Seleccionadas=[];


class Tendencia extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Divices:[],
          Variables:[
            "Oxigeno Disuelto",
            "Temperatura",
            "Saturación",
            "Salinidad"
          ],
          Seleccionadas:[],
          selectValue:'0'
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);   
        this.changeDivice = this.changeDivice.bind(this);    
        this.filtrar =this.filtrar.bind(this);
        this.filtrar2 =this.filtrar2.bind(this);
        this.loadDataChart =this.loadDataChart.bind(this);
        
        
    }
    changeDivice(e) {
      this.setState({
        selectValue: e.target.value
      })
    }
  
    componentDidMount = () => {
        sessionCheck()
        this.setState({redirect:false})
        // this.tagservices.getDataTag().then(data => 
        //     this.setState({Tags: data})
        // );  
        //this.this_Centro=setInterval(()=>this.getIdCentro(),2000);
        //var data = JSON.parse(sessionStorage.getItem('workplace'));
        // alert(JSON.stringify(data.id));
        // this.getIdCentro();
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
    } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        this.setState({redirect:true})
        //this.getSondas(centro[0].toString());
      }
      
      getSondas=(id)=>{
        const EndPointTag = `${API_ROOT}/workPlace/${id}/zone`;
        let Mis_Locations=[];
        var CancelToken = axios.CancelToken;
        var cancel;
        axios
        .get(EndPointTag,{
              cancelToken: new CancelToken(
                  function executor(c) {
                      cancel = c;
                   })
            })
        .then(response => {

            let zonas = response.data.data;
            let ind=0;
            zonas.map((z)=>{
              const EndPointLocation = `${API_ROOT}/zone/${z._id}/location`;
                    axios
                    .get(EndPointLocation,{
                      cancelToken: new CancelToken(
                          function executor(c) {
                              cancel = c;
                          })
                    })
                    .then(response => {
                        if(response.data.statusCode==200){
                           //console.log("ob"+response.data.data._id)
                           let locations=response.data.data;
                           locations.filter(l=>l.active==true).map((l,i)=>{
                            l.index=ind;
                            ind++;
                            l.module=z.name;
                            const EndPointLocation = `${API_ROOT}/location/${l._id}/tag`;
                            axios
                            .get(EndPointLocation,{
                              cancelToken: new CancelToken(
                                  function executor(c) {
                                      cancel = c;
                                  })
                            })
                            .then(response => {
                                if(response.data.statusCode==200){
                                   //console.log("ob"+response.data.data._id)
                                   let tags=response.data.data;
                                   l.tags=tags;
                                   this.setState({isLoading: false,Divices:this.state.Divices.filter((t)=>t._id!=l._id&&t.name!=l.name).concat(l)})
                                }
                            })
                            .catch(error => {
                              console.log(error);
                            });
                           })
                           Mis_Locations.push(locations);
                        }
                    })
                    .catch(error => {
                      console.log(error);
                    });
            })

           this.setState({isLoading: false});   
        })
        .catch(error => {
          console.log(error);
        });
    }
    
     getMydatachart=(data,name,i,color,leyenda)=>{
       let mydatachart = {
            axisYIndex: i,
            type: "line",
            legendText: leyenda,
            name: name+" "+leyenda,
            color:color,
            dataPoints : data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,  
            lineThickness: 3
             }
             i++;
           dataCha.push(mydatachart);
         return
     }
     getMyaxis=(color,unity)=>{
        let axisy= {    
            id:unity,
            //title: "Mg/L - °C",
            labelFontSize: 11,                   
            // lineColor: color,
            // tickColor: color,
            // labelFontColor: color,
            // titleFontColor: color,
            // suffix: " "+unity
             includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }
     getMydatachart2=(data,name,i,color,leyenda)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "line",
             axisYType: "secondary",
             legendText: leyenda,
             name: name+" "+leyenda,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return
      }
      getMyaxis2=(color,unity)=>{
        let axisy= {
            id:unity,
            labelFontSize: 11,                   
            lineColor: color,
            tickColor: color,
            labelFontColor: color,
            titleFontColor: color,
            suffix: " "+unity,
            includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

     loadDataChart = (name, URL,chartcolor,sw,variable) => {   
         
       // this.setState({blocking: true});
        const token = "tokenFalso";
        var CancelToken = axios.CancelToken;
        var cancel;
        axios
          .get(URL,{
              cancelToken: new CancelToken(
                  function executor(c) {
                      cancel = c;
                   })
            })
         .then(response => { 
            console.log("punto 2")
            //let sonda=response.data.data;
            let mediciones=response.data.data;
            console.log(mediciones)
            let  axisy= {};

            if(i==0){
              axisy= {    
                title: "Mg/L - °C",
                labelFontSize: 11,                   
              }
              dataChaAxisy.push(axisy);
            }
            name=name.replace("Sensor OD", " ");
            if(variable=="Oxigeno Disuelto"){
              let dataCharts = mediciones.map( item => { 
                  return { x: item.x , y : item.y }; 
              });
              this.getMydatachart(dataCharts,"Oxd",3,chartcolor,name);
              //axisy=this.getMyaxis(chartcolor," ");
              i++;}

            if(variable=="Temperatura"){

              let dataCharts3 = mediciones.map( item => { 
                  return { x: item.x , y : item.y }; 
              });
              this.getMydatachart(dataCharts3,"Temp",2,chartcolor,name);
              //axisy=this.getMyaxis(chartcolor[2]," °C");
              i++;}

            if(variable=="Saturación"){
              let dataCharts2 = mediciones.map( item => { 
                  return { x: item.x , y : item.y }; 
              });
              this.getMydatachart2(dataCharts2,"Oxs",1,chartcolor,name);
              //axisy=this.getMyaxis2(chartcolor," %");
              i++;}

            if(variable=="Salinidad"){
              let dataCharts4 = mediciones.map( item => { 
                  return { x: item.x , y : item.y }; 
              });
              this.getMydatachart2(dataCharts4,"Sal",0,chartcolor,name);
              //axisy=this.getMyaxis2(chartcolor," PSU");
              i++;}

            console.log(mediciones)
            let mydatachart ={};
          
             this.setState({
                 dataAxisy:dataChaAxisy,
                 dataCharts:dataCha
            });  
            //                 dataExport:dataChaExport,
            //   this.setState({dataExport:dataChaExport});  
            //   this.setState({dataCharts:dataCha});  
             if (sw === 1)
                disabled=false;
                disabled_graficar=false;
                this.setState({blocking: false}); 

                // console.timeEnd('loop2');
         })
         .catch(error => {
        //    console.log(error);
         });
         
 
     }
    

    filtrar =() => {

  
        const {Seleccionadas} = this.state;

        //this.setState({dataExport:[]});
        console.log(Seleccionadas)
        if (Seleccionadas !== null){           
 
          const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
          const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
          let f1_split=f1.split("T");
          let f2_split=f2.split("T");

            dataChaAxisy = [] ;
            dataCha = [] ;
            dataChaExport = [];
            i = 0;
            const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            let centro_sel=[];
            Seleccionadas.map((data)=>{
                centro_sel.push(data.centro)
            })
            centro_sel=centro_sel.filter(this.onlyUnique).map((data,i)=>{return {centro:data,ind:i}});

            // console.time('loop');
            
            
            for (let i = 0; i < Seleccionadas.length; i++) {
              //console.log(tags)
              // fecha_inicio:fecha_inicio,
              // fecha_fin:fecha_fin,
              let f1 =Seleccionadas[i].fecha_inicio;
              let f1_split=f1.split("-");
              let new_f1=f1_split[2]+"-"+f1_split[1]+"-"+f1_split[0];
              let f2 =Seleccionadas[i].fecha_fin;
              let f2_split=f2.split("-");
              let new_f2=f2_split[2]+"-"+f2_split[1]+"-"+f2_split[0];
              //console.log("fecha inicio : "+new_f1)

              let centro_color=centro_sel.filter((data)=>data.centro==Seleccionadas[i].centro)
              .map((data)=>{return ColorChart[data.ind] });
              console.log(centro_color)

              this.setState({blocking: true});     
              let APItagMediciones="";
              if(this.state.NameWorkplace.name.includes(' Control')){
                APItagMediciones=`${API_ROOT}/measurement-control/xy/tag/${Seleccionadas[i].tag}/${new_f1}T00:00/${new_f2}T23:59`;
              }else{
                APItagMediciones = `${API_ROOT}/measurement/xy/tag/${Seleccionadas[i].tag}/${new_f1}T00:00/${new_f2}T23:59`;
              }
              //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${Seleccionadas[i].id_sensor}/${new_f1}T00:00/${new_f2}T23:59`;
              //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${Seleccionadas[i].id_sensor}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
    
              let sw =0
              if (i=== Seleccionadas.length- 1)
                sw = 1;
                console.log("punto 1")
              this.loadDataChart(Seleccionadas[i].centro+"-"+Seleccionadas[i].sensor,APItagMediciones, centro_color,sw,Seleccionadas[i].variable);
            }
            // console.timeEnd('loop');
            
     
         
         //   console.log (dataChaAxisy);

        }
      
   
    }
    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    //carga los datos para la Exportación de la tendencia
    loadDataChart2 = (shortName, URL ,unity) => {   
         
        // this.setState({blocking: true});
         const token = "tokenFalso";
        var CancelToken = axios.CancelToken;
        var cancel;
         axios
           .get(URL, {
            headers: {  
              'Authorization': 'Bearer ' + token
             },
           },{
              cancelToken: new CancelToken(
                  function executor(c) {
                      cancel = c;
                   })
            })
          .then(response => { 
              const dataChart = response.data.data;
            //   console.log(dataChart);
             //  this.setState({gridDataChart:dataChart});  
              
            // console.log(dataChart); 
            //  console.time('loop2');
              let _dataCharts = dataChart.map( item => { 
                 return { x: moment(item.dateTime.substr(0,19)) , y : item.value }; 
               });
 
                //console.log(dataChart);
               
               let _dataChartsExport = dataChart.map( item => { 
                 return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , y : item.value.toString().replace(".",",") }; 
               });
 
 
            //   const mydatachart = {
            //      axisYIndex: i,
            //      type: "spline",
            //      legendText: shortName,
            //      name: shortName,
            //      color:chartcolor,
            //      dataPoints : response.data.data,
            //      xValueType: "dateTime",
            //      indexLabelFontSize:"30",
            //      showInLegend: true,
            //      markerSize: 0,  
            //      lineThickness: 3
            //       }
            //       i++;
 
            //      const  axisy= {    
            //          labelFontSize: 11,                   
            //          lineColor: chartcolor,
            //          tickColor: chartcolor,
            //          labelFontColor: chartcolor,
            //          titleFontColor: chartcolor,
            //          suffix: " "+ unity
            //         // includeZero: false
            //        }
 
              let mydatachartExport = {  
                     unity : unity,              
                     sonda: shortName,
                     measurements : _dataChartsExport
                      }
                 
                //dataChaAxisy.push(axisy);  
                 dataChaExport.push(mydatachartExport);   
                //dataCha.push(mydatachart);  
           
              this.setState({
                dataExport:dataChaExport
             });
            //  console.log("dataExport: ",dataChaExport);

             //                 dataExport:dataChaExport,
             //   this.setState({dataExport:dataChaExport});  
             //   this.setState({dataCharts:dataCha});  
 
                 //console.timeEnd('loop2');
                 //this.filtrar2();
          })
          .catch(error => {
            // console.log(error);
          });
          
  
      }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =() => {
        
        this.setState({buttonExport:'none'});
   
        const {Seleccionadas} = this.state;


        if (Seleccionadas !== null){   

            //this.filtrar();
  
             const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             let f1_split=f1.split("T");
            let f2_split=f2.split("T");
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;
            
            //  console.time('loop');
             for (let i = 0; i < Seleccionadas.length; i++) {
               //this.setState({blocking: true});      
               const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${Seleccionadas[i].code}/${f1_split[0]}T00:00/${f2_split[0]}T23:59`;
               //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/2019-06-07T00:00:00/2019-06-07T23:59:59`;

            //    console.log(APItagMediciones);
     
               this.loadDataChart2(Seleccionadas[i].shortName,APItagMediciones,Seleccionadas[i].unity);
             }
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                      
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        isMulti
                        name="colors"
                        options={Tags}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Tag"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
        let value1 = this.state.start.format("DD-MM-YYYY") 
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
      }

    agregar(){
      console.log("select "+this.state.selectValue);
      if(this.state.selectValue!=0 && variable!=""){
        let centro_=centro.split('/');
        let sensor_=sensor.split('/');
        let fecha_inicio = this.state.start.format("DD-MM-YYYY");
        let fecha_fin = this.state.end.format("DD-MM-YYYY");

        let tags=this.state.Divices.filter((d)=>d._id==this.state.selectValue).map((d)=>{
          return d.tags
        })
        let tag;
        tags[0].filter((t)=>{
          if(variable.includes("Oxigeno Disuelto")){
            if(t.name.includes("oxd")){
              tag=t._id
            }
          }
          if(variable.includes("Saturación")){
            if(t.name.includes("oxs")){
              tag=t._id
            }
          }
          if(variable.includes("Temperatura")){
            if(t.name.includes("temp")){
              tag=t._id
            }
          }
          if(variable.includes("Salinidad")){
            if(t.name.includes("sal")){
              tag=t._id
            }
          }              
        })
        console.log(tag)

        this.setState({Seleccionadas:this.state.Seleccionadas.concat({
          id:indicie_seleccion,
          fecha_inicio:fecha_inicio,
          fecha_fin:fecha_fin,
          id_centro:centro_[0],
          centro:centro_[1],
          id_sensor:this.state.selectValue,
          sensor:sensor_[1],
          variable:variable,
          tag:tag
      })});
      disabled=false;
      indicie_seleccion++;
      }else{
        toast['warning']('Favor de no dejar campos vacios', { autoClose: 4000 })
      }
      //Seleccionadas.push(Seleccion);
      //Seleccionadas.push({centro,sensor,variable});
      
      //alert("Seleccion "+JSON.stringify(this.state.Seleccionadas));
      // console.log("WENA"+JSON.stringify(this.state.Seleccionadas))
    }
    limpiar(){
      this.setState({Seleccionadas:[]});
      disabled=false;
    }
    eliminar(id){
      let tabla=this.state.Seleccionadas.filter((data)=>data.id!=id).map((data)=>data);
      this.setState({Seleccionadas:tabla});
    }
 
    render() {
        //dataExport
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
         
 
         let optionsChart1 = {

            
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMMYYYY HH:mm");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                dataAxisy.filter((data,i)=>!String(data.id).includes("PSU")&&!String(data.id).includes("%")),
             axisY2 :{
                title:"% - PSU"
            },   
            
             }
         
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Herramientas</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Comparador Tendencia Centros</BreadcrumbItem>
                      {/* <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                    <Row>
                                        <Col   md={12} lg={3}> 
                                            {this.renderPickerAutoApply(ranges, local, maxDate)}   
                                        </Col>
                                        <Col md={12}  lg={2}>
                                            <Input  type="select" id="centro" onChange={((e)=>{
                                                this.setState({Divices:[],selectValue:0})
                                                let index = e.target.selectedIndex;
                                                centro=e.target.value+"/"+e.target.options[index].text;
                                                this.getSondas(e.target.value);
                                            })}>
                                              <option disabled selected value="">SELECCIONE UN CENTRO</option>
                                              {centros.map((data,i)=>{
                                                return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.code}>{data.name}</option>
                                              })}
                                              
                                            </Input>
                                        </Col>
                                        <Col md={12}  lg={2}>
                                            <Input value={this.state.selectValue} type="select" id="dispositivo" onChange={((e)=>{
                                                this.changeDivice(e);
                                                let index = e.target.selectedIndex;
                                                sensor=e.target.value+"/"+e.target.options[index].text;
                                            })}>
                                              <option disabled selected value="0">SELECCIONE UN DISPOSITIVO</option>
                                              {this.state.Divices.map((data,i)=>{
                                                return <option value={data._id}>{data.name}</option>
                                              })}
                                            </Input>
                                        </Col>
                                        <Col md={12}  lg={2}>
                                            <Input type="select" id="variable" onChange={((e)=>{
                                                // let index = e.target.selectedIndex;
                                                // Seleccion.sensor=e.target.options[index].text;
                                                //Seleccion.variable=e.target.value;
                                                variable=e.target.value;
                                                //alert(JSON.stringify(Seleccion));
                                            })}>
                                              <option disabled selected value="">SELECCIONE UNA VARIANBLE</option>
                                              {this.state.Variables.map((data,i)=>{
                                                return <option value={data}>{data}</option>
                                              })}
                                            </Input>
                                        </Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                          if(this.state.Seleccionadas!=null){
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                             this.agregar();
                                                            }
                                                          }}
                                                >Agreagar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                            indicie_seleccion=0;
                                                             this.limpiar();
                                                             }}
                                                >Limpiar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                            



                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                            {
                            <Card className="p-10 m-10" >                                 
                                                      
                            <ReactTable
                                style={{height:250}}                                                       
                                //data={[Seleccion]}
                                data={this.state.Seleccionadas}
                      
                                // loading= {false}
                                showPagination= {true}
                                showPaginationTop= {false}
                                showPaginationBottom= {false}
                                showPageSizeOptions= {false}
                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                defaultPageSize={10}
                                columns={[
                                        {
                                        Header: "FECHA INICIO",
                                        accessor: "fecha_inicio",
                                        },
                                        {
                                        Header: "FECHA FIN",
                                        accessor: "fecha_fin"
                                        },
                                        {
                                          Header: "CENTRO",
                                          accessor: "centro"
                                        },
                                        {
                                          Header: "SENSOR",
                                          accessor: "sensor"
                                        },
                                        {
                                        Header: "VARIABLE",
                                        accessor: "variable",                                                          
                                        width: 130
                                        },
                                        {
                                          width: 300,
                                          Header: "ACCION",
                                          Cell: ({ original }) => (
                                            <Button value={original.centro} style={{marginLeft:`${45}%`}} color="danger" outline onClick={((e)=>{
                                                //let indice=this.state.Seleccionadas.filter((data)=>data.id_centro==original.id_centro&&data.id_sensor==original.id_sensor&&data.variable==original.variable).map((data,i)=>i)
                                                //alert(original.id)
                                                this.eliminar(original.id);
                                            })}>
                                              Quitar
                                            </Button>
                                          )
                                        }
                                    ]                                                             
                                }
                                
                                className="-striped -highlight"
                                />
                            </Card>
                            }
                        </Col>
                        <Col md="12" style={{marginTop:10}}>
                          <Row>
                            <Col md="4"></Col>
                            <Col md="4">
                              <Button color="primary"
                                    outline
                                    disabled={disabled}
                                    className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                    onClick={() => {  
                                            
                                            disabled=true;
                                            disabled_graficar=true;
                                            this.filtrar();
                                        }}
                                    >Graficar
                              </Button>
                            </Col>
                            <Col md="4"></Col>
                          </Row>
                        </Col>
                        <Col md="12" style={{marginTop:10}}>
                        <BlockUi tag="div" blocking={this.state.blocking} 
                                    loader={<Loader active type={this.state.loaderType}/>}>
                                    <Card className="main-card mb-1 p-0">
                                        <CardBody className="p-3">                                 
                                            <Row>
                                                <Col   md={12} lg={12}> 
                                                
                                                    <ResponsiveContainer height='100%' width='100%' >
                                                        <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                                    </ResponsiveContainer>   
                                                </Col> 
                                            </Row>
                                        </CardBody>
                                    </Card>
                            </BlockUi>

                        </Col>

                    </Row>
                    <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                            {/* <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%` }}
                                onClick={() => {  
                                        this.filtrar2();
                                        // console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button> */}
                    </Col>
                    <Col sm="4"></Col>
                    
                    
                    {/* {console.time("loop 3")} */}
                    
                    {
                                                          
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="3" key={data.sonda}  >
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        <CSVLink
                                                                        
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: "VALUE("+ data.unity + ")", key: "y" }
                                                                            ]
                                                                        
                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                                    </CSVLink>
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={data.measurements}
                                                  
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: "VALUE ("+ data.unity+ ")",
                                                                    accessor: "y",                                                              
                                                                    width: 90
                                                                    }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         
                         )
        
                            )
                            
                    }
                    {/* {console.timeEnd("loop 3")} */}
                                       

                    </Row>


                 
             
            
                                 

          

                    

               


                                                                 
                                                    
   

            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Tendencia);
