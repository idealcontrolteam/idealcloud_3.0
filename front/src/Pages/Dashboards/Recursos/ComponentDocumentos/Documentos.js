import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter, CardText,Form,CustomInput } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {
    toast

} from 'react-toastify';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT,API_ROOT2} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError, clearFields } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import '../../../../assets/icon/style.css';
import Swal from 'sweetalert2'
import ReactTable from "react-table";
import { ReactPDF } from 'react-pdf';

sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let visible=false; 
let ruta="";
let check=true;
class Documentos extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");

        this.state = {       
          isLoading:false,
          Mis_Prom:[],
          MyData:[],
          MyData2:[],
          NameWorkplace:"",
          loaderType: 'ball-triangle-path',
          redirect:false,
          name:"",
          modal1:false,
          menu1:true,
          menu2:false,
        };  
        this.tagservices = new TagServices(); 
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);       
    }

  
    componentDidMount = () => {
        if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c2"||sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c3"){
          visible=true;
        }else{
          visible=false;
        }
        window.scroll(0, 0);
        this.getArchives();
        this.getArchives2();
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
        }
        
        // let rol=sessionStorage.getItem("rol");
        // console.log(rol)
        // if(rol=="5f6e1ac01a080102d0e268c2"){
        //   active_config="block";
        // }
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
        
     } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        // this.filtrar(centro[0],centro[1])
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        // //this.getSondas(centro[0].toString());
        // if(c!=0)
        //   this.setState({redirect:true})
        // c++;
      }

      handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({[name]: value});
      }
    
      handleSubmit(event) {
        event.preventDefault();
        const {menu1}=this.state;
        const fileInput = document.querySelector("#fileInput");
        const formData = new FormData();
        let name=String(fileInput.value)
        let validation=false;
        if(name.includes(".doc") || name.includes(".pdf")){
          validation=true;
        }
        if(this.state.name!="" && fileInput.files[0]!=null){
          if(validation){
            let type=this.getType();
            type=type.toLowerCase() 
            formData.append("file", fileInput.files[0]);
            formData.append("name", this.state.name);
            formData.append("type", type);
            if(check){
              formData.append("workplaceId", this.state.NameWorkplace.id);
            }
            axios
            .post(API_ROOT+`/archive`,formData)
            .then(response => {   
              if(response.data.statusCode===201){
                document.querySelector("#fileInput").value="";
                this.setState({name:""})
                check=false;
                toast['success']('El archivo fue creado correctamente', { autoClose: 4000 })
                menu1?this.getArchives():this.getArchives2();
              }
            })
            .catch(error => {
                console.log(error)
            });
          }else{
            toast['warning']('El formato del certificado es incorrecto', { autoClose: 4000 })
          }
        }else{
          toast['warning']('Formulario incompleto', { autoClose: 4000 })
        }
          
      }

      getArchives(){
        axios
            .get(API_ROOT+`/archive/certificados`)
            .then(response => {   
              let archives=response.data.data;
              this.setState({MyData:archives.map((m,i)=>{
                  m.ind=i+1;
                  return m
              })})
            })
            .catch(error => {
                console.log(error)
            });
      }

      getArchives2(){
        axios
            .get(API_ROOT+`/archive/manuales`)
            .then(response => {   
              let archives=response.data.data;
              this.setState({MyData2:archives.map((m,i)=>{
                  m.ind=i+1;
                  return m
              })})
            })
            .catch(error => {
                console.log(error)
            });
      }

      downloadArchive(id){
        axios
        .get(API_ROOT+`/archive/descarga/${id}`)
        .then(response => {   
        })
        .catch(error => {
            console.log(error)
        });
      }

      deleteArchive(id){
        const {menu1}=this.state;
        axios
            .delete(API_ROOT+`/archive/${id}`)
            .then(response => {   
              console.log(response)
              menu1?this.getArchives():this.getArchives2();
              toast['success']('El archivo fue eliminado', { autoClose: 4000 })
            })
            .catch(error => {
                console.log(error)
            });
      }

      switchModal(val){
          this.setState({modal1:val})
      }

      getRuta(ruta,id){
        let type=this.getTypes();
        //this.getArchives();
        if((navigator.userAgent.match(/Android/i)) ||
           (navigator.userAgent.match(/webOS/i)) ||
           (navigator.userAgent.match(/iPhone/i)) ||
           (navigator.userAgent.match(/iPod/i)) ||
           (navigator.userAgent.match(/iPad/i)) ||
           (navigator.userAgent.match(/BlackBerry/i))){
             return API_ROOT+`/archive/descarga/${id}`
        }
        else if(ruta!=""){
          ruta=String(ruta);
          let split=ruta.split('/')
          try{
            console.log(ruta)
            return API_ROOT2+`/doc/${type}/${split[split.length-1]}`
          }catch(e){
            console.log(e)
          }
        }else{
          return ""
        }
      }

      createTooltip(id,mensaje){
          return <UncontrolledTooltip placement="top-end" target={id}>
            {mensaje}
        </UncontrolledTooltip>
      }

      getType(){
        if(this.state.menu1){
          return "Certificado"
        }else if(this.state.menu2){
          return "Manual"
        }else{
          return ""
        }
      }

      getTypes(){
        const {menu1,menu2}=this.state;
        if(menu1){
          return "certificados"
        }else if(menu2){
          return "manuales"
        }
      }

      switchArray(menu1){
        if(menu1){
          return this.state.MyData.filter(m=>m.workplaceId==this.state.NameWorkplace.id||m.workplaceId==undefined)
        }else{
          return this.state.MyData2.filter(m=>m.workplaceId==this.state.NameWorkplace.id||m.workplaceId==undefined)
        }
      }

      sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }

    render() {

        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        
        const {cards, collapse, isLoading} = this.state;
        
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        const {menu1,menu2,MyData,MyData2}=this.state;
             
         ///***********************************+++ */
        return (
            <Fragment>
                    
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        {console.log(this.state.Modulos)}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Recursos</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Documentos</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      {/* <BreadcrumbItem tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      
                      </Breadcrumb>
                    <Card>
                    <CardHeader>
                      <ButtonGroup>
                          <Button color="primary" outline={!menu1} onClick={(()=>{
                            ruta="";
                            this.setState({menu1:true,menu2:false})
                          })}>Certificados</Button>
                          <Button color="primary" outline={!menu2} onClick={(()=>{
                            ruta="";
                            this.setState({menu1:false,menu2:true})
                          })}>Manuales</Button>
                      </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                     <Row>
                        <Col md="12">
                           <Row>
                             <Col style={{display:`${visible?"block":"none"}`}}>
                                <Card>
                                  <CardHeader>
                                    {this.createTooltip("formato","El formato del certificado debe ser en PDF o WORD")}
                                  <CardText>Agregar {this.getType()} <i style={{fontSize:20}} id="formato" class="pe-7s-info"> </i></CardText>
                                  </CardHeader>
                                  <CardBody>
                                    <Form onSubmit={this.handleSubmit}>
                                      <Row>
                                        <Col md={12}>
                                          <Input placeholder="Ingrese nombre..." name="name" type="text" value={this.state.name} onChange={this.handleChange}></Input>
                                        </Col>
                                        <Col md={10}>
                                          <Input type="file" name="file" id="fileInput" ></Input>
                                        </Col>
                                        <Col md={2}>
                                          <CustomInput type="checkbox" id="exampleCustomCheckbox" label="Privado" checked={check} onChange={this.handleChange} onClick={((e)=>{
                                            if(check){
                                              check=false
                                            }else{
                                              check=true
                                            }
                                          })} />
                                        </Col>
                                      </Row>
                                      
                                      <br></br>
                                      <FormGroup>
                                        
                                      </FormGroup>
                                      
                                      <Row>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col><Button color="primary" >Agregar</Button></Col>
                                      </Row>
                                    </Form>
                                  </CardBody>
                                </Card>
                             </Col>
                             <Col>
                                <Card>
                                  <CardHeader>
                                  <CardText>Lista de {menu1?"Certificados":"Manuales"}</CardText>
                                  </CardHeader>
                                  <CardBody>
                                      <ReactTable                                                           
                                      data={this.switchArray(menu1)}
                                                                  
                                      // loading= {false}
                                      showPagination= {true}
                                      showPaginationTop= {false}
                                      showPaginationBottom= {true}
                                      showPageSizeOptions= {false}
                                      pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                      defaultPageSize={10}
                                      columns={[
                                          {
                                              Header: "N°",
                                              accessor: "ind",
                                              width: 60
                                              },
                                              {
                                              Header: "Nombre",
                                              accessor: "name",                                                              
                                              width: 500
                                              },
                                              {
                                                Header: "Estado",
                                                width: 70,
                                                Cell: ({ row }) => (
                                                  <Fragment>
                                                      {this.createTooltip("estado"+row._original._id,row._original.workplaceId==undefined?"Publico":"Privado")}
                                                      <Badge color={"link"} style={{marginLeft:0,cursor:"pointer",display:`${visible?"block":"none"}`}} onClick={((e)=>{
                                                      })} pill>
                                                      <i id={"estado"+row._original._id} class={row._original.workplaceId==undefined?"pe-7s-global":"pe-7s-lock"} 
                                                      style={{fontSize:20,color:`${row._original.workplaceId==undefined?"#007bff":"orange"}`}}></i>
                                                      </Badge>
                                                  </Fragment>
                                                  )
                                              },
                                              {
                                                Header: "Accion",
                                                width: 80,
                                                Cell: ({ row }) => (
                                                  <Fragment>
                                                      {/* <Badge color={"link"} href={API_ROOT+`/archive/descarga/${row._original._id}`} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                          //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                          ruta=row._original.ruta
                                                          if((navigator.userAgent.match(/Android/i)) ||
                                                          (navigator.userAgent.match(/webOS/i)) ||
                                                          (navigator.userAgent.match(/iPhone/i)) ||
                                                          (navigator.userAgent.match(/iPod/i)) ||
                                                          (navigator.userAgent.match(/iPad/i)) ||
                                                          (navigator.userAgent.match(/BlackBerry/i))){

                                                          }
                                                          else if(!ruta.includes('.doc')){
                                                            e.preventDefault()
                                                            this.switchModal(true);
                                                          }
                                                      })} pill><i class="pe-7s-look" style={{fontSize:25,color:"green"}}></i></Badge> */}
                                                      <a href={this.getRuta(row._original.ruta,row._original._id)} target="_blank" style={{marginLeft:0,cursor:"pointer"}} pill><i class="pe-7s-look" style={{fontSize:25,color:"green"}}></i></a>
                                                      
                                                      <Badge color={"link"} style={{marginLeft:0,cursor:"pointer",display:`${visible?"block":"none"}`}} onClick={((e)=>{
                                                          //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                          this.deleteArchive(row._original._id)
                                                      })} pill><i class="pe-7s-close-circle" style={{fontSize:25,color:"red"}}></i></Badge>
                                                  </Fragment>
                                                  )
                                              },
                                          ]                                                             
                                      }
                                      
                                      className="-striped -highlight"
                                      />
                                  </CardBody>
                                </Card>
                             </Col>
                             <Col style={{display:`${visible?"none":"block"}`}}></Col>
                           </Row>
                        </Col>
                    </Row>
                    </CardBody>
                    </Card>
                    <Modal style={{maxHeight:`${100}%`,maxWidth:`${90}%`}} isOpen={this.state.modal1} size="xl">
                            <ModalHeader toggle={() => this.switchModal(false)}></ModalHeader>
                            <ModalBody>
                              <object
                              style={{display:`${ruta!=""?"block":"none"}`}}
                              data={this.getRuta()}
                              width={`${100}%`}
                              height={window.outerHeight-window.outerHeight*25/100}
                              ></object>
                            </ModalBody>
                    </Modal>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Documentos);
