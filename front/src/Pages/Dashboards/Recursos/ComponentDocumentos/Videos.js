import React, {Component, Fragment,PureComponent} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,
  ButtonGroup,Breadcrumb, BreadcrumbItem,Spinner,ListGroup,ListGroupItem,Collapse,Badge,UncontrolledTooltip
  , UncontrolledPopover, PopoverHeader, PopoverBody, Modal, ModalHeader, ModalBody, ModalFooter, CardText,Form } from 'reactstrap';
import {faCalendarAlt, faExchangeAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {
    toast

} from 'react-toastify';
import {  ResponsiveContainer,
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, Line, ComposedChart, Area, AreaChart} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';
import {logout,sessionCheck} from '../../../../services/user';
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';
import * as _ from "lodash";
import '../../Zonas/style.css';
import Media from 'react-media';
import '../../../../assets/icon/style.css';
import Swal from 'sweetalert2'
import ReactTable from "react-table";

sessionCheck()
let empresa=sessionStorage.getItem("Empresa");
let visible=false; 
let ruta="";
class Documentos extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");

        this.state = {       
          isLoading:false,
          Mis_Prom:[],
          MyData:[],
          NameWorkplace:"",
          loaderType: 'ball-triangle-path',
          redirect:false,
          name:"",
          url:"",
          modal1:false,
          menu1:true,
          menu2:false,
        };  
        this.tagservices = new TagServices(); 
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);       
    }

  
    componentDidMount = () => {
        window.scroll(0, 0);
        this.getArchives();
        if(sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c2"||sessionStorage.getItem("rol")=="5f6e1ac01a080102d0e268c3"){
          visible=false;
        }else{
          visible=true;
        }
        // let rol=sessionStorage.getItem("rol");
        // console.log(rol)
        // if(rol=="5f6e1ac01a080102d0e268c2"){
        //   active_config="block";
        // }
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
        
     } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        // this.filtrar(centro[0],centro[1])
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        // //this.getSondas(centro[0].toString());
        // if(c!=0)
        //   this.setState({redirect:true})
        // c++;
      }

      handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({[name]: value});
      }
    
      handleSubmit(event) {
        event.preventDefault();
        const {menu1}=this.state;
        const formData = new FormData();
        let complemento="https://www.youtube.com/embed/";
        let url=this.state.url;
        let split=url.split('watch?v=');
        if(split[1].includes('&')){
          let split2=String(split[1]).split('&');
          url=complemento+split2[0];
        }else{
          url=complemento+split[1]
        }
        
        if(this.state.name!="" && url!=""){
            let type="video";
            formData.append("url", url);
            formData.append("name", this.state.name);
            formData.append("type", type);
            axios
            .post(API_ROOT+`/archive`,formData)
            .then(response => {   
              if(response.data.statusCode===201){
                this.setState({name:"",url:""})
                toast['success']('El archivo fue creado correctamente', { autoClose: 4000 })
                this.getArchives();
              }
            })
            .catch(error => {
                console.log(error)
            });
        }else{
          toast['warning']('Formulario incompleto', { autoClose: 4000 })
        }
          
      }

      getArchives(){
        axios
            .get(API_ROOT+`/archive/videos`)
            .then(response => {   
              let archives=response.data.data;
              this.setState({MyData:archives.map((m,i)=>{
                  m.ind=i+1;
                  return m
              })})
            })
            .catch(error => {
                console.log(error)
            });
      }

      deleteArchive(id){
        const {menu1}=this.state;
        axios
            .delete(API_ROOT+`/archive/${id}`)
            .then(response => {   
              this.getArchives();
              toast['success']('El archivo fue eliminado', { autoClose: 4000 })
            })
            .catch(error => {
                console.log(error)
            });
      }

      switchModal(n){
        if(n=="1"){
          this.setState({modal1:true})
        }else{
            this.setState({modal1:false})
        }
      }

      getRuta(){
        if(ruta!=""){
          return ruta
        }else{
          return ""
        }
      }

      createTooltip(id,mensaje){
          return <UncontrolledTooltip placement="top-end" target={id}>
            {mensaje}
        </UncontrolledTooltip>
      }

      getTypes(){
        const {menu1,menu2}=this.state;
        if(menu1){
          return "certificados"
        }else if(menu2){
          return "manuales"
        }
      }

    render() {

        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        
        const {cards, collapse, isLoading} = this.state;
        
        //dataExport      

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
         
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        const {menu1,menu2,MyData,MyData2}=this.state;
             
         ///***********************************+++ */
        return (
            <Fragment>
                    
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        {/* {this.state.Modulos} */}
                        {console.log(this.state.Modulos)}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Recursos</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Videos</BreadcrumbItem>
                      {/* {this.state.NameWorkplace.name} */}
                      {/* <BreadcrumbItem tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      
                      </Breadcrumb>
                    <Card>
                    <CardBody>
                     <Row>
                        <Col md="12">
                           <Row>
                             <Col style={{display:`${visible?"none":"block"}`}}>
                                <Card>
                                  <CardHeader>
                                    {this.createTooltip("formato","El formato del video debe ser en URL de YouTube")}
                                  <CardText>Agregar Video <i style={{fontSize:20}} id="formato" class="pe-7s-info"> </i></CardText>
                                  </CardHeader>
                                  <CardBody>
                                    <Form onSubmit={this.handleSubmit}>
                                      <Input placeholder="Ingrese nombre..." name="name" type="text" value={this.state.name} onChange={this.handleChange}></Input>
                                      <br></br>
                                      <Input placeholder="Ingrese url..." name="url" type="text" value={this.state.url} onChange={this.handleChange}></Input>
                                      <br></br>
                                      <Row>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col></Col>
                                        <Col><Button color="primary" >Agregar</Button></Col>
                                      </Row>
                                    </Form>
                                  </CardBody>
                                </Card>
                             </Col>
                             <Col>
                                <Card>
                                  <CardHeader>
                                  <CardText>Lista de Videos</CardText>
                                  </CardHeader>
                                  <CardBody>
                                      <ReactTable                                                           
                                      data={MyData}
                                                                  
                                      // loading= {false}
                                      showPagination= {true}
                                      showPaginationTop= {false}
                                      showPaginationBottom= {true}
                                      showPageSizeOptions= {false}
                                      pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                      defaultPageSize={10}
                                      columns={[
                                          {
                                              Header: "n°",
                                              accessor: "ind",
                                              width: 60
                                              },
                                              {
                                              Header: "Nombre",
                                              accessor: "name",                                                              
                                              width: 500
                                              },
                                              {
                                                Header: "accion",
                                                width: 80,
                                                Cell: ({ row }) => (
                                                  <Fragment>
                                                      <Badge color={"link"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                          //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                          ruta=row._original.url
                                                          this.switchModal(1)
                                                      })} pill><i class="icon-play" style={{fontSize:25,color:"orange"}}></i></Badge>
                                                      <Badge color={"link"} style={{marginLeft:0,cursor:"pointer"}} onClick={((e)=>{
                                                          //this.updateEvent(row._original.id,row._original.active,row._original.workplaceId)
                                                          this.deleteArchive(row._original._id)
                                                      })} pill><i class="pe-7s-close-circle" style={{fontSize:25,color:"red"}}></i></Badge>
                                                  </Fragment>
                                                  )
                                              },
                                          ]                                                             
                                      }
                                      
                                      className="-striped -highlight"
                                      />
                                  </CardBody>
                                </Card>
                             </Col>
                             <Col style={{display:`${visible?"block":"none"}`}}></Col>
                           </Row>
                        </Col>
                    </Row>
                    </CardBody>
                    </Card>
                    <Modal style={{maxHeight:`${100}%`,maxWidth:`${90}%`}} isOpen={this.state.modal1} size="xl">
                            <ModalHeader toggle={() => this.switchModal("-1")}></ModalHeader>
                            <ModalBody>
                            <iframe width="100%" height={window.outerHeight-window.outerHeight*25/100} src={this.getRuta()} frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </ModalBody>
                    </Modal>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Documentos);
