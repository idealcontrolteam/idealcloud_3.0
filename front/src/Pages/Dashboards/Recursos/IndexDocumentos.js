import React, {Component, Fragment} from 'react';
import Documentos from './ComponentDocumentos/Documentos';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {checkStatusLogin} from '../../../services/user';
import { Redirect, Route } from 'react-router-dom';



export default class IndexDocumentos extends Component { 
    constructor(props) {
        super(props);
        };
    render() {
        const isLoggedIn = checkStatusLogin();
        return (
            <Fragment>
                {/* {alert(JSON.stringify(this.props.empresa))} */}
                <Route 
                    render={() => 
                        isLoggedIn ? ( 
                            <ReactCSSTransitionGroup
                                component="div"
                                transitionName="TabsAnimation"
                                transitionAppear={true}
                                transitionAppearTimeout={0}
                                transitionEnter={false}
                                transitionLeave={false}>
                                <div className="app-inner-layout">                      
                                    <Documentos/>              
                                </div>
                            </ReactCSSTransitionGroup>
                                ) : (
                                    <Redirect to="/pages/login"></Redirect>
                                )
                        }      
                
                /> 
            </Fragment>
        )
    }
}