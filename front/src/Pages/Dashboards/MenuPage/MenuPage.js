import React from 'react'
import {
  Breadcrumb, BreadcrumbItem
} from 'reactstrap';

const ControlOxigeno = ({listas,children}) => {
  return (
    <div>
      <Breadcrumb tag="nav" listTag="div">
          {
            listas.map(menu=>{
              return <BreadcrumbItem tag="span" href={menu.link}>{menu.name.replace('_',' ').replace('_',' ')}</BreadcrumbItem>
            })
          }
          {children}
      </Breadcrumb>
    </div>
  )
}

export default ControlOxigeno
