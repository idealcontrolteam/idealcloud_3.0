import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';
import Ionicon from 'react-ionicons';
import {logout} from '../../../services/user';
//import {logout, currentAccount} from '../../../services/user';
import {connect} from 'react-redux';
// import CryptoJS from 'crypto-js';
import Media from 'react-media';
import AlarmasFallas from './AlarmasFallas'



import {

    Button
    
} from 'reactstrap';

import {
    toast,
    Bounce
} from 'react-toastify';


class UserBox extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            active: false,
        };
        
      
    }
   
   
   
    notify2 = () => this.toastId = toast("You don't have any new items in your calendar for today! Go out and play!", {
        transition: Bounce,
        closeButton: true,
        autoClose: 5000,
        position: 'bottom-center',
        type: 'success'
    });


    handleLogout = () => {
        logout();
        this.setState({ active: true });  
 
    }

    getAlarmas(){
        let empresa=sessionStorage.getItem("Empresa");
        if(empresa.includes("idealcontrol")){
            return <AlarmasFallas></AlarmasFallas>
        }
    }
    

    render() {  
         let {nameUsuario,emailUsuario } = this.props;    
         var w = window.innerWidth;
         let user_name=emailUsuario.split('@')
         if(w<550){
            user_name=user_name[0]
         }else{
            user_name=emailUsuario
         }
         
        if (this.state.active) {
            return <Redirect to="/pages/login"></Redirect>
        }   
        return (
            <Fragment>
                <Media query="(min-width: 991px)" render={() =>
                (
                    <img src={require(`../../../assets/logos/proveedor/aguasin_b.png`)} style={{marginTop:`${0}%`}} width="180" height="35"></img>
                )}
                />

                <div className="header-btn-lg pr-0">
                    <div className="widget-content p-0">
                        <div className="widget-content-wrapper">
                            {this.getAlarmas()}
                            <div className="widget-content-left  "> 
                                 <div className="header-user-icon">
                                    <i  className="rounded-circle pe-7s-user pe-3x  "/>
                                </div>  
                            </div>
                            <div className="widget-content-left  ml-3 ">
                                <div className="widget-heading">
                                    USUARIO{/* {nameUsuario} */}
                                </div>
                                <div className="widget-subheading">
                                   {user_name}
                                </div>
                            </div>

                            <div className="widget-content-right  ml-3">
                                <Button onClick={this.handleLogout}  className="btn-shadow p-1" size="sm"  color="primary">
                                    <Ionicon color="#ffffff" fontSize="20px" icon="ios-exit"/>
                                </Button>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}




const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,

});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UserBox);
