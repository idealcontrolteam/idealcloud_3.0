import React, {Component, Fragment} from 'react';

import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';

import PerfectScrollbar from 'react-perfect-scrollbar';
import {
 
    Nav,  Button, NavItem
} from 'reactstrap';
import moment from 'moment';
import {TagServices} from '../../../../services/comun';

class Fallas extends Component {
    constructor(props) {
        super(props);
        this.state = {   
            // Alarms:[],
        }
        this.tagservices = new TagServices();
        // this.timeRef=React.createRef();
    }

    reformatDate(dateTime){
        return this.tagservices.reformatDate(dateTime)
    }

    getColumn(){
        let {Alarms} = this.props;
        if(Alarms.length>0){
            return Alarms.map(a=>{
                return<Fragment>
                    <VerticalTimelineElement
                        className="vertical-timeline-item"
                        icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                    >
                        <p>
                        {String(a.NameWorkplace)} {a.divice} {a.detail} <b className="text-danger">{moment(this.reformatDate(a.dateTimeIni)).format("DD-MM-YYYY HH:mm")}</b>
                        </p>
                    </VerticalTimelineElement>
                </Fragment>
            })
        }
       
    }
    render() {
        return (
            <Fragment>
                <div className="scroll-area-sm">
                    <PerfectScrollbar>
                        <div className="p-3">
                            <VerticalTimeline layout="1-column" className="vertical-without-time">
                                {this.getColumn()}
                                {/* <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                                >
                                     <p>
                                     TK A  Falla termica Bomba Cono <b className="text-danger">12:00 PM</b>
                                    </p>
                                   
                                </VerticalTimelineElement> */}
                            </VerticalTimeline>
                        </div>
                    </PerfectScrollbar>
                </div>
                <Nav vertical>
                    <NavItem className="nav-item-divider"/>
                    <NavItem className="nav-item-btn text-center">
                    
                        <Button color="primary"
                            href="#/dashboards/alarmas"
                                        outline
                                        className={"btn-shadow btn-wide btn-outline-2x pb-2 mr-2 "}
                        
                                            
                                    >Ver Historial      
                                    
                            </Button>
                    
                    </NavItem>
                    
                </Nav>
            </Fragment>
        )
    }
}

export default Fallas;