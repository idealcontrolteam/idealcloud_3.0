import React, {Component, Fragment} from 'react';

import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';

import PerfectScrollbar from 'react-perfect-scrollbar';
import {
    Nav,  Button, NavItem
} from 'reactstrap';
import {TagServices} from '../../../../services/comun';
import moment from 'moment';

class Alarmas extends Component {
    constructor(props) {
        super(props);
        this.state = {   
            // Alarms:[],
        }
        this.tagservices = new TagServices();
        // this.timeRef=React.createRef();
    }

    reformatDate(dateTime){
        return this.tagservices.reformatDate(dateTime)
    }

    // componentDidMount = () => {
    //     let minutos=3;
    //     let segundos=60*minutos;
    //     let intervaloRefresco =segundos*1000;
    //     this.getAlarmas();
    //     this.timeRef.current = setInterval(this.getAlarmas.bind(this),intervaloRefresco);
    // }

    // getAlarmas(){
    //     let fini=moment().format('YYYY-MM-01T00:00:00');
    //     let ffin=moment().format('YYYY-MM-DDTHH:mm:ss');
    //     this.tagservices.getTendenciaAlarms(fini,ffin).then(response=>{
    //         if(response.data.statusCode=="201"||response.data.statusCode=="200"){
    //             this.setState({Alarms:response.data.data})
    //         }else{
    //             console.log("status 500")
    //         }
    //     }).catch(e=>e)
    // }

    getColumn(){
        let {Alarms} = this.props;
        if(Alarms.length>0){
            return Alarms.map(a=>{
                return<Fragment>
                    <VerticalTimelineElement
                    className="vertical-timeline-item"
                    icon={<i className="badge badge-dot badge-dot-xl badge-warning"> </i>}>
                        <p>
                        {String(a.NameWorkplace)} {a.detail} <b className="text-warning">{moment(this.reformatDate(a.dateTimeIni)).format("DD-MM-YYYY HH:mm")}</b>
                        </p>
                    
                    </VerticalTimelineElement>
                </Fragment>
            })
        }
       
    }

    render() {
        return (
            <Fragment>
                <div className="scroll-area-sm">
                    <PerfectScrollbar>
                        <div className="p-3">
                      
                            <VerticalTimeline layout="1-column" className="vertical-without-time">
                                {this.getColumn()}
                                {/* <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-warning"> </i>}
                                >
                                     <p>
                                     TK C Alarma nivel bajo temperatura' <b className="text-warning">12:00 PM</b>
                                    </p>
                                </VerticalTimelineElement> */}
                            </VerticalTimeline>
                        </div>
                    </PerfectScrollbar>
                </div>
                <Nav vertical>
                    <NavItem className="nav-item-divider"/>
                    <NavItem className="nav-item-btn text-center">
                        <Button color="primary"
                            href="#/dashboards/alarmas"
                                        outline
                                        className={"btn-shadow btn-wide btn-outline-2x pb-2 mr-2 "}
                                    >Ver Historial      
                        </Button>
                    </NavItem>
                </Nav>
            </Fragment>
        )
    }
}

export default Alarmas;