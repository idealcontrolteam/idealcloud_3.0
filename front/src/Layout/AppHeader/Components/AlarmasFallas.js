import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';


import Ionicon from 'react-ionicons';

import {
    UncontrolledDropdown, DropdownToggle, DropdownMenu,

} from 'reactstrap';

//import city3 from '../../../assets/utils/images/dropdown-header/abstract1.jpg';
import city3 from '../../../assets/utils/images/dropdown-header/headerbg2.jpg';


import Tabs from 'react-responsive-tabs';
import Alarmas from './TabsContent/Alarmas';
import Fallas from './TabsContent/Fallas';
import {TagServices} from '../../../services/comun';
import moment from 'moment';

let tabsContent = [
    // {
    //     title: 'Alarmas',
    //     content: <Alarmas/>
    // }
    // ,{
    //     title: 'Fallas',
    //     content: <Fallas/>
    // }
];



class AlarmasFallas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            redirect:false,
            Alarms:[],
            Workplaces:[]
        }
        this.tagservices = new TagServices();
        this.timeRef=React.createRef();
    }

    componentDidMount = () => {
        let minutos=1;
        let segundos=60*minutos;
        let intervaloRefresco =segundos*1000;
        this.getAlarmas();
        // this.getTabs();
        this.timeRef.current = setInterval(this.getAlarmas.bind(this),intervaloRefresco);
    }

    getWorkplaces(ids,array){
        this.tagservices.getWorkplaces_array(ids).then(response=>{
            let data=response.data.data;
            array.map(a=>{
                a.NameWorkplace=data.filter(w=>w._id==a.workplaceId).map(w=>w.name)[0]
            })
            this.setState({Alarms:array})
        })
        .catch(e=>e)
    }

    getAlarmas(){
        let fini=moment().format('YYYY-MM-DDT00:00:00');
        let ffin=moment().format('YYYY-MM-DDTHH:mm:ss');
        this.tagservices.getTendenciaAlarmsActive(fini,ffin).then(response=>{
            if(response.data.statusCode=="201"||response.data.statusCode=="200"){
                tabsContent=[];
                let data=response.data.data;
                if(data.length>0){
                    this.getWorkplaces(data.map(d=>{
                        return {"_id":d.workplaceId}
                    }),data)
                }
                //this.setState({Alarms:data})
            }else{
                console.log("status 500")
            }
        }).catch(e=>e)
    }
    
    setRedirect = () => {
        this.setState({
          myredirect: true
        })   
    }
    
    getTabs() {
        // let alarms=this.state.Alarms;
        // console.log(this.state.Workplaces)
        
        // console.log(alarms)
        if(this.state.Alarms.length>0){
            tabsContent.push(
            {
                title: 'Fallas',
                content: <Fallas Alarms={this.state.Alarms.filter(a=>a.type!="alarma")} />
            },
            {
                title: 'Alarmas',
                content: <Alarmas Alarms={this.state.Alarms.filter(a=>a.type=="alarma")}/>
            },
           )
            return tabsContent.map((tab, index) => ({
                title: tab.title,
                getContent: () => tab.content,
                key: index,
            }));
        }
    }
  // href="#/dashboards/histalarmasfallas" onClick={this.setRedirect}

  
    render() {
      
     
        if (this.state.myredirect) {
             return <Redirect to="tendencia"></Redirect>
     
        } 
        let danger=" badge-danger";
        let campana_activa=false;
        danger=""
        if(this.state.Alarms.length>0){
            danger=" badge-danger";
            campana_activa=true;
        }
        return (
            
            <Fragment>
              
                <div className="header-dots">
             
                    <UncontrolledDropdown>
                    <DropdownToggle className="p-0 mr-2" color="link">
                            <div className="icon-wrapper icon-wrapper-alt rounded-circle">
                                <div className="icon-wrapper-bg bg-danger"/>
                                <Ionicon beat={campana_activa} color="#d92550" fontSize="23px" icon="md-notifications-outline"/>
                                <div className={"badge badge-dot badge-dot-sm"+danger}>Notifications</div>
                            </div>
                        </DropdownToggle>
                        
                        <DropdownMenu right className="dropdown-menu-xl rm-pointers">
                            <div className="dropdown-menu-header mb-0">
                                <div className="dropdown-menu-header-inner ropdown-menu-header-inner bg-heavy-rain p-3  ">
                                    <div className="menu-header-image opacity-9"
                                        style={{
                                            backgroundImage: 'url(' + city3 + ')'
                                        }}
                                    />
                            
                                    <div className="menu-header-content text-dark">
                                        <h5 className="menu-header-title">Alarmas Presentes</h5>
                                        <h6 className="menu-header-subtitle"> <b className="text-danger">{this.state.Alarms.filter(a=>a.type!="alarma").length} Fallas </b></h6>
                                        <h6 className="menu-header-subtitle"> <b className="text-warning">{this.state.Alarms.filter(a=>a.type=="alarma").length} Alarmas</b></h6>
                                    </div>
                                </div>
                            </div>
                            <Tabs tabsWrapperClass="body-tabs body-tabs-alt2" transform={false} showInkBar={true}
                                  items={this.getTabs()}/>
                           
                        
                        </DropdownMenu>
                    </UncontrolledDropdown>
             
                </div>
            </Fragment>
        )
    }
}

export default AlarmasFallas;
