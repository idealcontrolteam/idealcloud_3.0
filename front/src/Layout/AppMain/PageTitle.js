import React, {Component} from 'react';

class PageTitle extends Component {  
    render() {
        let {      
            heading,
            icon,      
        } = this.props;       

        return (

            <div className="app-page-title">
                <div className="page-title-wrapper">
                    <div className="page-title-heading">
                         <div className="pr-2">
                             <i className={icon}> </i> 
                          </div>                   
                        <div>
                            {heading}   
                        </div>
                    </div>
              
                </div>
            </div>
        );
    }
}

export default (PageTitle);