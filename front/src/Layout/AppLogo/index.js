import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import Hamburger from 'react-hamburgers';
import AppMobileMenu from '../AppMobileMenu';
import {
    setEnableClosedSidebar,
    setEnableMobileMenu,
    setEnableMobileMenuSmall,
} from '../../reducers/ThemeOptions';

import {Empresas} from '../../Pages/Comun/data';
import {logout,sessionCheck} from '../../services/user'
import {TagServices} from '../../services/comun';
// import * as Push from 'push.js'; // if using ES6 
let empresa=sessionStorage.getItem("Empresa");
let logo="";
let i=0;

console.log("E: "+sessionStorage.getItem("Empresa"));

class HeaderLogo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            mobile: false,
            activeSecondaryMenuMobile: false,
            Companys:[],
            visible:false,
        };
        this.tagservices = new TagServices();
    }

    componentDidMount = () => {
        if(i==0 && empresa=="idealcontrol"){
            // Push.create("Bienvenido a IdealCloud", {
            //     body: "Una plataforma de Monitoreo y Control",
            //     icon: `https://ihc.idealcontrol.cl/idealcloud2/doc/imagenes/logo.png`,
            //     timeout: 9000,
            //     onClick: function () {
            //         window.focus();
            //         this.close();
            //     }
            // });
            i++;
        }
       this.AllCompanys();
    }

    toggleEnableClosedSidebar = () => {
        let {enableClosedSidebar, setEnableClosedSidebar} = this.props;
        setEnableClosedSidebar(!enableClosedSidebar);
    }

    AllCompanys(){
        let data={};
        this.tagservices.crudCompany("",data,"GET").then(response=>{
            if(response.statusCode==200 || response.statusCode==201){
                this.setState({Companys:response.data})
            }
        }).catch(e=>console.log(e))
    }

    

    state = {
        openLeft: false,
        openRight: false,
        relativeWidth: false,
        width: 280,
        noTouchOpen: false,
        noTouchClose: false,
    };

    render() {
        let {
            enableClosedSidebar,
            emailUsuario
        } = this.props;

       
        return (
            <Fragment>
                <div className="app-header__logo">
                    <div/>
                    {this.state.Companys.map((data)=>{
                        const hostname = window && window.location && window.location.hostname;
                        try{
                            let rename=String(emailUsuario).replace('.cl','');
                            rename=rename.split('@')
                            console.log(rename[1])
                            sessionCheck()
                            if(data.name==String(rename[1])){
                                let url=data.logo.split('imagenes/')
                                return <a style={{cursor:"pointer"}} href="#/dashboards/resumen">
                                    <img src={hostname.includes("localhost")?require(`../../../doc/imagenes/${url[1]}`):
                                    `https://ihc.idealcontrol.cl/idealcloud2/doc/imagenes/${url[1]}`} style={{display:this.state.visible?"none":"block"}} width="200" height="50"></img>
                                    </a>
                            }
                        }catch(e){
                            console.log(e)
                            //logout()
                        }
                    })}
                    
                    <div className="header__pane ml-auto">
                        <div onClick={(()=>{
                            this.toggleEnableClosedSidebar()
                            const {visible}=this.state;
                            visible?this.setState({visible:false}):this.setState({visible:true})
                        })  
                        }>
                            <Hamburger
                                active={enableClosedSidebar}
                                type="elastic"
                                onClick={() => this.setState({active: !this.state.active})}
                            />
                        </div>
                    </div>
                </div>
                <AppMobileMenu/>
            </Fragment>
        )
    }
}


const mapStateToProps = state => ({
    emailUsuario: state.Session.emailUsuario,
    enableClosedSidebar: state.ThemeOptions.enableClosedSidebar,
    enableMobileMenu: state.ThemeOptions.enableMobileMenu,
    enableMobileMenuSmall: state.ThemeOptions.enableMobileMenuSmall,
});

const mapDispatchToProps = dispatch => ({

    setEnableClosedSidebar: enable => dispatch(setEnableClosedSidebar(enable)),
    setEnableMobileMenu: enable => dispatch(setEnableMobileMenu(enable)),
    setEnableMobileMenuSmall: enable => dispatch(setEnableMobileMenuSmall(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderLogo);