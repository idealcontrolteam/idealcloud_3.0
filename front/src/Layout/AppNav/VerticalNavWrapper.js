import React, {Component, Fragment} from 'react';
import {withRouter,Route, Link, Redirect} from 'react-router-dom';
import MetisMenu from 'react-metismenu';
import { Menu } from './NavItems';
import { Button, Form, FormGroup, Label, Input, FormText,ListGroup, ListGroupItem, ButtonGroup } from 'reactstrap';
import {connect} from 'react-redux';
import {
    setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../reducers/Session';
import {getUserCompanys, getWorkPlace} from '../../services/user';
import Tendencia from '../../Pages/Dashboards/Tendencia/IndexTendencia'
import {reset} from 'redux-form';
import Tendencia_Online from '../../Pages/Dashboards/Tendencia/IndexTendencia_Online'
import moment from 'moment';
import "../../Pages/Dashboards/Zonas/style.css"
import axios from "axios";
import { API_ROOT } from '../../api-config'
import * as _ from "lodash";
import {TagServices} from '../../services/comun';
import MenuLateral from './Component/MenuLateral'

var ArrayMenu=[];
let select=1;
let select2=1;
let select3=1;
let buscar="";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
let sw=true;
let sw1=false;
let sw2=false;
let sw_rol="none";
let indBuscar=0;

let menu0="";
let menu1="active";
let menu2="";
let menu2_5="";
let menu3="";
let menu3_5="";
let menu4="";
let menu4_5="";
let menu5="";
let menu6="";
let menu7="";
let menu8="";
let menu9="";
let menu10="";
let menu11="";

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {         
            CompanyId:[],
            Companys:[],
            WorkPlace:[],
            ArrayMenu:[],
            actividad:1,
            array:[1],
            rolUsuario:"",
        }
        this.getUpdateCentros = this.getUpdateCentros.bind(this);
    }

    componentDidMount = () => {

        ArrayMenu=Menu; //.filter((data)=>data.content[1].label==Menu[1].content[1].label).map((data)=>data);
        let { emailUsuario,rolUsuario,centroUsuario } = this.props;
        this.intervalIdTag1 = setInterval(() => this.getUpdateCentros(),240000);//4 min
        if(sessionStorage.getItem('rol')=="5f6e1ac01a080102d0e268c3"){//rolUsuario=="SuperUsuario" ||
            this.setState({rolUsuario:"SuperAdmin"});
            sw_rol="block";
            if(Menu[1].content.length<=1){
                Menu[1].content.push({
                    icon: 'pe-7s-display1',
                    label: 'Usuario Centros',
                    to: '#/dashboards/usuario_centros'
                });
            }
        }
        if(sessionStorage.getItem('rol')=="5f6e1ac01a080102d0e268c2"){//rolUsuario=="SuperUsuario" ||
            this.setState({rolUsuario:"Admin"});
            sw_rol="block";
        }
        //console.log("Menu : "+JSON.stringify(Menu[1]))
        this.getCompanys();
        this.getUpdateCentros();
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let url=window.location.href;
        let separador=nextProps.centroUsuario;
        let cen=separador.split('/');
        if(cen[1]!=undefined && cen!=[]){
            let location=url.split('/')
            let largo=location.length;
            if(location[largo-1]=="tendencia_online"){
                this.switchMenu(1);
            }
            if(location[largo-1]=="resumen"){
                this.switchMenu(0);
            }
            buscar=`${cen[1]}*`;
            this.setId_Url(cen[0],cen[1])
        }
      }

    getUpdateCentros(){
        axios
        //.post(API_ROOT+"/"+empresa+"/centros_activos",{
        .get(API_ROOT+`/workPlace`)
        .then(response => {   
            // alert(response.data.statusCode==200);
            if(response.data.statusCode==200){
                let centros=response.data.data;
                //console.log("help "+JSON.stringify(centros))
                let my_centros=[];
                empresas.map((e)=>{
                    return centros.filter(c=>c._id==e.code).map((c)=>{
                        e.endDate=c.endDate!=null?c.endDate:"";
                        my_centros.push({
                            id:c._id,
                            code:c.code,
                            name:c.name,
                            active:c.active,
                            endDate:c.endDate!=null?c.endDate:""
                        })
                        return e;
                    })
                })
                
                //sessionStorage.setItem("Centros",JSON.stringify(cen));
                
                //console.log(my_centros)
                let new_centros=_.orderBy(my_centros, ['name'],['asc'])
                //console.log(new_centros)
                
                //sessionStorage.setItem("Centros",JSON.stringify(new_centros));
                //this.getCompanys(new_centros)
                let emp=new_centros.map((data)=>{
                    return {
                        id:data._id,
                        code:data.code,
                        name:data.name,
                        active:data.active,
                        endDate:data.endDate!=null?data.endDate:"",
                        areaId:data.areaId!=null?data.areaId:""
                    }
                })
                //sessionStorage.setItem("Centros",JSON.stringify(emp));
               
                this.setState({WorkPlace:emp})
            }else {
                this.setState({ status: "error", message: "Ops! Problemas" });   
            }
            
        })
        .catch(error => {
            console.log(error)
        });
        //console.log(this.state.)
    }

    switchMenu=(n)=>{
        this.clearMenuSelect();
        if(n==0){
            sw=true;
        }if(n==0.5){
            menu0=true;
        }else if(n==1){
            menu1=true;
        }else if(n==2){
            menu2=true;
        }else if(n==2.5){
            menu2_5=true;
        }
        else if(n==3){
            menu3=true;
        }else if(n==3.5){
            menu3_5=true;
        }else if(n==4){
            menu4=true;
        }else if(n==4.5){
            menu4_5=true;
        }else if(n==5){
            menu5=true;
        }else if(n==6){
            menu6=true;
        }else if(n==7){
            menu7=true;
        }else if(n==8){
            menu8=true;
        }else if(n==9){
            menu9=true;
        }else if(n==10){
            menu10=true;
        }else if(n==11){
            menu11=true;
        }

    }

    ingresarCentroUsuario = (centro) =>{
        let {setCentroUsuario} = this.props;
        setCentroUsuario(centro);
    }

    getNameCompany=(id)=>{
        return this.state.Companys.filter(data=>data._id==id).map((data)=>{
            return data.name
        })
    }

    getNameCompany=(id)=>{
        return this.state.Companys.filter(data=>data._id==id).map((data)=>{
            return data.name
        })
    }

    clearMenuSelect=()=>{
        sw=false;
        menu0=false;
        menu1=false;
        menu2=false;
        menu2_5=false;
        menu3=false;
        menu3_5=false;
        menu4=false;
        menu4_5=false;
        menu5=false;
        menu6=false;
        menu7=false;
        menu8=false;
        menu9=false;
        menu10=false;
        menu11=false;
    }

    getCompanys=(emp)=>{
        //alert("empresas : "+JSON.stringify(empresas));
        let new_empresas=[];
        if(empresas!=null){
            let ind=0;
            // console.log("FECHAAA "+date.toLocaleDateString())
            let centros_activos=empresas.filter(e=>e.active==1)
            let cantidad_centros=centros_activos.length;
            // console.log("FECHAAA "+cantidad_centros)
            let cont=0;
            empresas.map((data,i)=>{
                let ind_emp=[];
                if(data.active==true && ind==0){
                    cont++;
                    //alert(i)
                    let now=new Date();
                    let fecha_in=moment(now).subtract(15, "minutes").format('YYYY-MM-DD HH:mm:ss');
                    let fecha_ind="2020-07-12 18:30:00";
                    if(data.endDate>=fecha_in){
                        ind++;
                        buscar=data.name;
                        this.setId_Url(data.codigo,data.name);      
                    }else if(cont==cantidad_centros){
                        ind++;
                        buscar=data.name;
                        this.setId_Url(data.codigo,data.name,1);  
                    }
                }
                new_empresas.push({
                    id:data.code,
                    name:data.name,
                    active:data.active,
                    endDate:data.endDate!=null?data.endDate:"",
                    areaId:data.areaId!=null?data.areaId:"",
                });
                //console.log(data);
            })
        }
        
        this.setState({WorkPlace:new_empresas})
        let ubicacion="";
        let url=window.location;
        url=String(url).split("dashboards/");
        ubicacion=url[1]
        //alert(url[1])
        this.clearMenuSelect();
        if(ubicacion=="resumen"){
            sw=true;
        }else if(ubicacion=="diagnostico"){
            menu0=true;
        }else if(ubicacion=="tendencia_online"){
            menu1=true;
        }else if(ubicacion=="tendencia"){
            menu2=true;
        }else if(ubicacion=="servidor"){
            menu2_5=true;
        }else if(ubicacion=="comparador"){
            menu3=true;
        }else if(ubicacion=="empresas"){
            menu3_5=true;
        }else if(ubicacion=="usuario_centros"){
            menu4=true;
        }else if(ubicacion=="efectividad"){
            menu4_5=true;
        }else if(ubicacion=="grupos"){
            menu5=true;
        }else if(ubicacion=="videos"){
            menu6=true;
        }else if(ubicacion=="documentos"){
            menu7=true;
        }else if(ubicacion=="datos_historicos"){
            menu8=true;
        }else if(ubicacion=="alarmas"){
            menu9=true;
        }else if(ubicacion=="centros"){
            menu10=true;
        }else if(ubicacion=="promedios"){
            menu11=true;
        }
    }


    setId_Url=(id,name,n)=>{
        try{
            let centro=[];
            empresas.filter(data=>name==data.name&&id==data.codigo).map((data)=>{
                    centro.push(data)
            });
            //alert("active : "+active);
            let workplace="";
            if(JSON.parse(sessionStorage.getItem('workplace'))!=null){
                workplace=JSON.parse(sessionStorage.getItem('workplace'));
                console.log(workplace.id)
                if(workplace.id==centro[0].code){
                    buscar=workplace.name+"*";
                }
            }
            sessionStorage.setItem('workplace', JSON.stringify({"id":centro[0].code,"name":centro[0].name,"active":centro[0].active}));
            this.ingresarCentroUsuario(`${centro[0].code}/${name}`);
            //this.setState({WorkPlace:[]})
            sw=false;
            menu0=false;
            menu1=true;
            menu2=false;
            menu2_5=false;
            menu3=false;
            menu3_5=false;
            menu4=false;
            menu4_5=false;
            buscar+="*"
        }catch(e){
            return e
        }
    }

    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    validateOnline=(data)=>{
        console.log(data.endDate)
        let now=new Date();
        let fecha_in=moment(now).subtract(15, "minutes").format('YYYY-MM-DD HH:mm:ss');
        if(data.name.includes(' Control')){
            return <Fragment><span className="pe-7s-culture" style={{fontSize:20,color:"rgb(19, 185, 85)"}}></span><font color="blue"><b style={{fontSize:13}}>{data.name}</b></font></Fragment>
        }
        else if(data.active==true && data.endDate>=fecha_in){
            //alert("wena")
            return <Fragment><span className="pe-7s-culture" style={{fontSize:20,color:"rgb(19, 185, 85)"}}></span><font color="#29AD21"><b style={{fontSize:13}}>{data.name}</b></font></Fragment>
        }else if(data.active==true){
            return <Fragment><span className="pe-7s-culture" style={{fontSize:20,color:"rgb(19, 185, 85)"}}></span> {data.name}</Fragment>
        }else{
            return <Fragment><span className="pe-7s-culture" style={{fontSize:20}}></span> {data.name}</Fragment>
        }
    }
    
    render() {
        //this.selectCompany(this.state.CompanyId[0])
        let Menu = [
            {
                icon: 'pe-7s-display1',
                label: 'General',
                content: [
                    {
                        label: 'Resumen',
                        to: '#/dashboards/resumen',
                    },
                ],
            }, 
            {
                icon: 'pe-7s-graph1',
                label: 'Tendencias',
                content: [
                    {
                        label: 'Tendencia Online',
                        to: '#/dashboards/tendencia_online',
                    },
                    {
                        label: 'Tendencia Historica',
                        to: '#/dashboards/tendencia',
                    },
                    {
                        label: 'Datos Historicos',
                        to: '#/dashboards/datos_historicos',
                    },
                    
                ],
            }, 
            {
                icon: 'pe-7s-tools',
                label: 'Herramientas',
                content: [
                    {
                        label: 'Comparador',
                        to: '#/dashboards/comparador',
                    },
                    {
                        label: 'Servidor',
                        to: '#/dashboards/servidor',
                    },
                    {
                        label: 'Empresas',
                        to: '#/dashboards/empresas',
                    },
                    {
                        label: 'Usuario Centros',
                        to: '#/dashboards/usuario_centros',
                    },
                    {
                        label: 'Historico Alarmas',
                        to: '#/dashboards/alarmas',
                    },
                    {
                        label: 'Centros',
                        to: '#/dashboards/centros',
                    },
                    {
                        label: 'Promedios',
                        to: '#/dashboards/promedios',
                    },
                    {
                        label: 'Efectividad',
                        to: '#/dashboards/efectividad',
                    },
                ],
            }, 
            {
                icon: 'pe-7s-display1',
                label: 'Recursos',
                content: [
                    {
                        label: 'Videos',
                        to: '#/dashboards/videos',
                    },
                    {
                        label: 'Documentos',
                        to: '#/dashboards/documentos',
                    },
                ],
            }, 
            // {
            //     icon: 'pe-7s-graph1',
            //     label: 'Tendencia Historica',
            //     to: '#/dashboards/tendencia',
            // },
            // {
            //     icon: 'pe-7s-bell',
            //     label: 'Historico Alarmas',
            //     to: '#/dashboards/alarmas',
            // },
            // {
            //     icon: 'pe-7s-tools',
            //     label: 'Control de Oxígeno',
            //     to: '#/dashboards/control_oxigeno',
            // },
        ];
        return (
            <Fragment> 
                <h5 className="app-sidebar__heading">Centros</h5>
                <div className="app-sidebar__heading ">
                        <div className=" ml-3 ">
                                {/* <div>
                                    Centros
                                </div> */}
                                
                                <ButtonGroup>
                                    {/* <a href="#/dashboards/resumen"><Button className={sw?"":""} color="primary" style={{backgroundColor:`${sw?"white":"rgba(32, 32, 248, 0.10)"}`,borderColor:`${sw?"white":""}`}} onClick={(()=>{
                                    //    this.setState({actividad:1})
                                       sw=true;
                                       sw1=false;
                                       sw2=false;
                                       menu1=false;
                                       menu2=false;
                                       menu3=false;
                                       menu4=false;
                                   })}><font color={sw?"4C6971":"white"}>resumen</font></Button></a> */}
                                   <Button className={sw1?"":""} color="primary" style={{backgroundColor:`${sw1?"white":"rgba(32, 32, 248, 0.10)"}`,borderColor:`${sw1?"white":""}`}} onClick={(()=>{
                                       this.setState({actividad:1})
                                       buscar="";
                                       indBuscar=1;
                                       let ubicaion=window.location.href;
                                       ubicaion.includes("/resumen")?
                                       sw=true:sw=false
                                       sw1=true;
                                       sw2=false;
                                   })}><font color={sw1?"4C6971":"white"}>activos</font></Button> 
                                   <Button className={sw2?"":""} color="primary" style={{backgroundColor:`${sw2?"white":"rgba(32, 32, 248, 0.10)"}`,borderColor:`${sw2?"white":""}`}} onClick={(()=>{
                                       this.setState({actividad:0})
                                       buscar="";
                                       indBuscar=1;
                                       let ubicaion=window.location.href;
                                       ubicaion.includes("/resumen")?
                                       sw=true:sw=false
                                       sw1=false;
                                       sw2=true;
                                    })}><font color={sw2?"4C6971":"white"}>inactivos</font></Button> 
                                </ButtonGroup>
                                <div className="" style={{borderRadius:`${2}%`}}>
                                {/* {console.log(this.state.Companys)} */}
                                <ButtonGroup>
                                <Input placeholder="Buscar..." value={buscar} onChange={e => {
                                    buscar=e.target.value;
                                    this.inputChangeHandler(e);
                                }} />
                                <Button style={{height:38}} color="secondary" onClick={((e)=>{
                                    let workplace=JSON.parse(sessionStorage.getItem("workplace"));
                                    try{
                                        if(indBuscar==0){
                                            indBuscar++;
                                            buscar="";
                                        }else{
                                            indBuscar=0;
                                            buscar=workplace.name+"*";
                                        }
                                    }catch(e){
                                        console.log(e)
                                    }
                                    
                                    this.inputChangeHandler(e);
                                })}><h4><span className="pe-7s-angle-down"></span></h4></Button>
                                </ButtonGroup>
                                <div class="scrollbar" >
                                    <div className="force-overflow" style={{overflow:"auto",maxHeight:220}}>
                                    <ListGroup>
                                        {this.state.WorkPlace.filter((data)=>{
                                            if(buscar!=""){
                                                return data.name.toLocaleUpperCase().includes(buscar.toLocaleUpperCase())
                                            }
                                            if(buscar.includes("*")){
                                                return []
                                            }
                                            else{
                                                buscar="";
                                                return data&&data.active==this.state.actividad
                                            }
                                        }).map((data,i)=>{
                                            if(i==0){
                                                return <ListGroupItem tag="button" action onClick={(()=>{
                                                    //alert(data.id)
                                                    buscar=data.name;
                                                    //alert(JSON.stringify(data))
                                                    this.setId_Url(data.code,data.name);
                                                })}>{this.validateOnline(data)}</ListGroupItem>
                                            }
                                            return <ListGroupItem tag="button" action onClick={(()=>{
                                                //alert(data.id)
                                                buscar=data.name;
                                                //alert(JSON.stringify(data))
                                                this.setId_Url(data.code,data.name);
                                            })}>{this.validateOnline(data)}</ListGroupItem>
                                        })}
                                    </ListGroup>
                                    </div>
                                </div>
                                <FormGroup style={{display:"none"}}>
                                    <Input style={{marginTop:10}}  type="select" name="select" onChange={((e)=>{
                                            let index = e.target.selectedIndex;
                                            alert(index)
                                            this.setId_Url(e.target.value,e.target.options[index].text);
                                        })} id="exampleSelect">
                                    <option  selected disabled value='0'>SELECCIONE UN CENTRO</option>
                                    {this.state.WorkPlace.map((data,i)=>{
                                        //console.log("active "+data.active)
                                        //className={`${data.active==1?"badge-success":"badge-secondary"}`}
                                        // if(data.active==1){
                                        //     if(i==0){
                                        //     return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}}  selected value={data.id} id={data.name}>&#x1F7E2; {data.name}</option>}
                                        //     return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.id} id={data.name}>&#x1F7E2; {data.name}</option>
                                        // }else{
                                        //     if(i==0){
                                        //         return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}}  selected value={data.id} id={data.name}>&#x26AA; {data.name}</option>}
                                        //         return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.id} id={data.name}>&#x26AA; {data.name}</option>
                                        // }
                                        if(i==0){
                                            return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}}  selected value={data.id} id={data.name}>{data.name} hol</option>
                                        }
                                        return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.id} id={data.name}> {data.name}</option>
                                    })}
                                    </Input>
                                </FormGroup>
                                
                                </div>
                         </div>
                 
                </div>
                <h5 className="app-sidebar__heading">Menu</h5>
                {/* <MenuLateral listas={Menu} /> */}
                {/* <MetisMenu  content={ArrayMenu} activeLinkFromLocation activeLinkTo={true} className="vertical-nav-menu" iconNamePrefix=""  classNameStateIcon="pe-7s-angle-down"/> */}
                <div class="metismenu vertical-nav-menu">
                    <ul class="metismenu-container">
                    <li class="metismenu-item">
                        <a class="metismenu-link" ><i class="metismenu-icon pe-7s-display1"></i>General<i class="metismenu-state-icon caret-left rotate-minus-90"></i>
                        </a>
                            <ul class="metismenu-container visible">
                                <li class="metismenu-item">
                                    <a class={sw?`metismenu-link active`:`metismenu-link`} 
                                    onClick={((e)=>{
                                        this.switchMenu(0)
                                    })} href="#/dashboards/resumen"><i class="metismenu-icon pe-7s-display1"></i>Resumen</a>
                                </li>
                            </ul>
                            {
                            this.state.array.map(()=>{
                                if(this.state.rolUsuario=="SuperAdmin")
                                    return(
                                    <ul class="metismenu-container visible">
                                        <li class="metismenu-item">
                                            <a class={menu0?`metismenu-link active`:`metismenu-link`} 
                                            onClick={((e)=>{
                                                this.switchMenu(0.5)
                                            })} href="#/dashboards/diagnostico"><i class="metismenu-icon pe-7s-display1"></i>Diagnostico</a>
                                        </li>
                                    </ul>)
                            })
                            }
                        </li>
                        <li class="metismenu-item">
                            <a class="metismenu-link"><i class="metismenu-icon pe-7s-display1"></i>Tendencias<i class="metismenu-state-icon caret-left rotate-minus-90"></i></a>
                            <ul class="metismenu-container visible">
                                <li class="metismenu-item">
                                    <a class={menu1?`metismenu-link active`:`metismenu-link`}  
                                    onClick={((e)=>{
                                        this.switchMenu(1)
                                    })}  href="#/dashboards/tendencia_online"><i class="metismenu-icon pe-7s-display1"></i>Tendencia Online</a>
                                </li>
                                <li class="metismenu-item">
                                    <a class={menu2?`metismenu-link active`:`metismenu-link`}
                                     onClick={((e)=>{
                                        this.switchMenu(2)
                                    })}  href="#/dashboards/tendencia"><i class="metismenu-icon pe-7s-display1"></i>Tendencia Historica</a>
                                </li>
                                <li class="metismenu-item">
                                    <a class={menu8?`metismenu-link active`:`metismenu-link`}
                                     onClick={((e)=>{
                                        this.switchMenu(8)
                                    })}  href="#/dashboards/datos_historicos"><i class="metismenu-icon pe-7s-display1"></i>Datos Historicos</a>
                                </li>
                            </ul>
                        </li>
                        <li class="metismenu-item">
                        <a class="metismenu-link" ><i class="metismenu-icon pe-7s-display1"></i>Herramientas<i class="metismenu-state-icon caret-left rotate-minus-90"></i>
                        </a>
                            <ul class="metismenu-container visible">
                                        
                                        <li class="metismenu-item">
                                            <a class={menu3?`metismenu-link active`:`metismenu-link`} 
                                            onClick={((e)=>{
                                                this.switchMenu(3)
                                            })} href="#/dashboards/comparador"><i class="metismenu-icon pe-7s-display1"></i>Comparador</a>
                                        </li>
                                        {this.state.array.map(()=>{
                                            if(this.state.rolUsuario=="SuperAdmin")
                                                return <Fragment>
                                                    <li class="metismenu-item" style={{display:sw_rol}}>
                                                        <a class={menu2_5?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{
                                                            this.switchMenu(2.5)
                                                        })} href="#/dashboards/servidor"><i class="metismenu-icon pe-7s-display1"></i>Servidor</a>
                                                    </li>
                                                    <li class="metismenu-item">
                                                        <a class={menu3_5?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{
                                                            this.switchMenu(3.5)
                                                        })} href="#/dashboards/empresas"><i class="metismenu-icon pe-7s-display1"></i>Empresas</a>
                                                    </li>
                                                    <li class="metismenu-item">
                                                        <a class={menu4?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{
                                                            this.switchMenu(4)
                                                        })} href="#/dashboards/usuario_centros"><i class="metismenu-icon pe-7s-display1"></i>Usuario Centros</a>
                                                    </li>
                                                    <li class="metismenu-item">
                                                        <a class={menu9?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{
                                                            this.switchMenu(9)
                                                        })} href="#/dashboards/alarmas"><i class="metismenu-icon pe-7s-display1"></i>Historico Alarmas</a>
                                                    </li>
                                                    <li class="metismenu-item">
                                                        <a class={menu10?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{
                                                            this.switchMenu(10)
                                                        })} href="#/dashboards/centros"><i class="metismenu-icon pe-7s-display1"></i>Centros</a>
                                                    </li>
                                                    <li class="metismenu-item">
                                                        <a class={menu11?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{
                                                            this.switchMenu(11)
                                                        })} href="#/dashboards/promedios"><i class="metismenu-icon pe-7s-display1"></i>Promedios</a>
                                                    </li>
                                                    </Fragment>
                                            else
                                                return ""
                                        })}
                                        <li class="metismenu-item" style={{display:sw_rol}}>
                                            <a class={menu4_5?`metismenu-link active`:`metismenu-link`} 
                                            onClick={((e)=>{
                                                this.switchMenu(4.5)
                                            })} href="#/dashboards/efectividad"><i class="metismenu-icon pe-7s-display1"></i>Efectividad</a>
                                        </li>
                            </ul>
                        </li>
                        <li class="metismenu-item">
                        <a class="metismenu-link" ><i class="metismenu-icon pe-7s-display1"></i>Recursos<i class="metismenu-state-icon caret-left rotate-minus-90"></i>
                        </a>
                            <ul class="metismenu-container visible">
                                        <li class="metismenu-item" style={{display:sw_rol}}>
                                            <a class={menu5?`metismenu-link active`:`metismenu-link`} 
                                            onClick={((e)=>{
                                                this.switchMenu(5)
                                            })} href="#/dashboards/comparador"><i class="metismenu-icon pe-7s-display1"></i>Grupos</a>
                                        </li>
                                        <li class="metismenu-item" style={{display:sw_rol}}>
                                            <a class={menu6?`metismenu-link active`:`metismenu-link`} 
                                            onClick={((e)=>{
                                                this.switchMenu(6)
                                            })} href="#/dashboards/videos"><i class="metismenu-icon pe-7s-display1"></i>Videos</a>
                                        </li>
                                        <li class="metismenu-item" style={{display:"block"}}>
                                            <a class={menu7?`metismenu-link active`:`metismenu-link`}
                                            onClick={((e)=>{
                                                this.switchMenu(7)
                                            })} href="#/dashboards/documentos"><i class="metismenu-icon pe-7s-display1"></i>Documentos</a>
                                        </li>
                            </ul>
                        </li>
                    </ul>
                </div>

         
                {/* <h5 className="app-sidebar__heading">Salas</h5>
                <MetisMenu  content={Salas} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix="" classNameStateIcon="pe-7s-angle-down"/> */}
            </Fragment>
        );
    }

    isPathActive(path) {
        return this.props.location.pathname.startsWith(path);
    }
}
const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
    centroUsuario: state.Session.centroUsuario
  });

const mapDispatchToProps = dispatch => ({
    setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});
  

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Nav));