// import axios from 'axios';
// import { API_ROOT } from '../../api-config';
// import React, {Component, Fragment} from 'react';

// const EndPointTag = `${API_ROOT}/tag`;
// const token = "tokenfalso";
// let tags=[];
// let New_Menu=[];

// New_Menu.push({
//     icon: 'pe-7s-display1',
//     label: 'Panel General',
//     to: '#/dashboards/panelgeneral',
// }, 
// {
//     icon: 'pe-7s-display1',
//     label: 'Tendencia Historica',
//     to: '#/dashboards/tendencia',
// });

// const DataAxios=async url=>{
//     const response = await axios
//     .get(url, {
//       headers: {  
//         'Authorization': 'Bearer ' + token
//       }
//     }).then(response => {
//         tags = response.data.data;
//         let content_new=[];
//         let sub_men={};
//         tags=tags.map((tag)=>{
//            sub_men.label=tag.name;
//            sub_men.to='#/dashboards/nave/1'
//            return tag
//         });
//         content_new.push(sub_men);
//         New_Menu.push({icon: 'pe-7s-display1',
//         label: 'Naves',content:content_new});
//         console.log(JSON.stringify(New_Menu))
//         return New_Menu
//     })
//     .catch(error => {
//       console.log(error);
//     })
//     console.log("response_data :",response);
// }

// console.log(DataAxios(EndPointTag))
// // axios
// //  .get(EndPointTag, {
// //    headers: {  
// //      'Authorization': 'Bearer ' + token
// //    }
// //  })
// //  .then(response => {
// //      tags = response.data.data;
// //      let content_new=[];
// //      let sub_men={};
// //      tags=tags.map((tag)=>{
// //         sub_men.label=tag.name;
// //         sub_men.to='#/dashboards/nave/1'
// //         return tag
// //      });
// //      content_new.push(sub_men);
// //      New_Menu.push({icon: 'pe-7s-display1',
// //      label: 'Naves',content:content_new});
// //      console.log(JSON.stringify(New_Menu))
// //  })
// //  .catch(error => {
// //    console.log(error);
// //  });

//  export const Menu=DataAxios(EndPointTag);

export const Menu = [
    // {
    //     icon: 'pe-7s-display1',
    //     label: 'Centros',
    //     content: [
            
    //     ],

    // },
    {
        icon: 'pe-7s-display1',
        label: 'Tendencias',
        content: [
            {
                icon: 'pe-7s-display1',
                label: 'Tendencia Online',
                to: '#/dashboards/tendencia_online'
            },
            {
                icon: 'pe-7s-display1',
                label: 'Tendencia Historica',
                to: '#/dashboards/tendencia'
                               
            }
        ],

    },
    {
        icon: 'pe-7s-display1',
        label: 'Herramientas',
        content: [
            {
                icon: 'pe-7s-display1',
                label: 'Comparador',
                to: '#/dashboards/comparador'
                               
            },
            // {
            //     icon: 'pe-7s-display1',
            //     label: 'Usuario Centros',
            //     to: '#/dashboards/usuario_centros'
                               
            // }
        ],

    },
    
    // ,
    // {
    //     icon: 'pe-7s-display1',
    //     label: 'Fallas',
    //     to: '#/dashboards/histfallas'
                       
    // },
 
 
];

// export const Salas = [
 
//     {
//         icon: 'pe-7s-display1',
//         label: 'Sala Broodstock',
//         to: '#/dashboards/zonas1',
//     },
//     {
//         icon: 'pe-7s-display1',
//         label: 'Sala Smolt',
//         to: '#/dashboards/zonas2',
//     }
//     ,
//     {
//         icon: 'pe-7s-display1',
//         label: 'Sala Alevines',
//         to: '#/dashboards/zonas3',
//     }
 
 
// ];


