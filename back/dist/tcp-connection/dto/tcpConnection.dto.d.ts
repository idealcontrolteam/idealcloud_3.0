export declare class CreateTcpConnectionDTO {
    ip: string;
    port: string;
}
