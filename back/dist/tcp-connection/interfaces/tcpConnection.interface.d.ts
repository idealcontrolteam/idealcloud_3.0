import { Document } from 'mongoose';
export interface TcpConnection extends Document {
    ip: string;
    port: number;
}
