"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tcpConnection_dto_1 = require("./dto/tcpConnection.dto");
const tcp_connection_service_1 = require("./tcp-connection.service");
let TcpConnectionController = class TcpConnectionController {
    constructor(tcpConnectionService) {
        this.tcpConnectionService = tcpConnectionService;
    }
    createTcpConnection(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newTcpConnection = yield this.tcpConnectionService.createTcpConnection(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Tcp Connection created successfully',
                data: newTcpConnection,
            });
        });
    }
    getTcpConnections(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const tcpConnections = yield this.tcpConnectionService.getTcpConnections();
            let msg = tcpConnections.length == 0
                ? 'Tcp connections not found'
                : 'Tcp connections fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tcpConnections,
                count: tcpConnections.length,
            });
        });
    }
    getDevicesByTcpConnectionId(res, tcpConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.tcpConnectionService.getDevicesByTcpConnectionId(tcpConnectionId);
            let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: devices,
                count: devices.length,
            });
        });
    }
    getTcpConnection(res, tcpConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tcpConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Id is not a valid ObjectId');
            }
            const tcpConnection = yield this.tcpConnectionService.getTcpConnection(tcpConnectionId);
            if (!tcpConnection) {
                throw new common_1.NotFoundException('Tcp connection not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tcp connection found',
                data: tcpConnection,
            });
        });
    }
    updateTcpConnection(res, body, tcpConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tcpConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tcp connection id is not a valid  ObjectId');
            }
            const updatedTcpConnection = yield this.tcpConnectionService.updateTcpConnection(tcpConnectionId, body);
            if (!updatedTcpConnection) {
                throw new common_1.NotFoundException('Tcp connection not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tcp connection updated',
                data: updatedTcpConnection,
            });
        });
    }
    deleteTcpConnection(res, tcpConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedTcpConnection = yield this.tcpConnectionService.deleteTcpConnection(tcpConnectionId);
            if (!deletedTcpConnection) {
                throw new common_1.NotFoundException('Tcp Connection not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tcp Connection deleted',
                data: deletedTcpConnection,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, tcpConnection_dto_1.CreateTcpConnectionDTO]),
    __metadata("design:returntype", Promise)
], TcpConnectionController.prototype, "createTcpConnection", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TcpConnectionController.prototype, "getTcpConnections", null);
__decorate([
    common_1.Get('/:tcpConnectionId/device'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tcpConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TcpConnectionController.prototype, "getDevicesByTcpConnectionId", null);
__decorate([
    common_1.Get('/:tcpConnectionId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tcpConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TcpConnectionController.prototype, "getTcpConnection", null);
__decorate([
    common_1.Put('/:tcpConnectionId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('tcpConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, tcpConnection_dto_1.CreateTcpConnectionDTO, Object]),
    __metadata("design:returntype", Promise)
], TcpConnectionController.prototype, "updateTcpConnection", null);
__decorate([
    common_1.Delete('/:tcpConnectionId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tcpConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TcpConnectionController.prototype, "deleteTcpConnection", null);
TcpConnectionController = __decorate([
    common_1.Controller('tcpConnection'),
    __metadata("design:paramtypes", [tcp_connection_service_1.TcpConnectionService])
], TcpConnectionController);
exports.TcpConnectionController = TcpConnectionController;
//# sourceMappingURL=tcp-connection.controller.js.map