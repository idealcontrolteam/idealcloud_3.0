"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const TypesAlarm_1 = require("../hooks/TypesAlarm");
let MeasurementHourService = class MeasurementHourService {
    constructor(MeasurementHourModel) {
        this.MeasurementHourModel = MeasurementHourModel;
    }
    getMeasurementHours() {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find();
            return MeasurementHours;
        });
    }
    getMeasurementHoursAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel
                .find()
                .populate('tagId sensorId locationId');
            return MeasurementHours;
        });
    }
    getMeasurementHourByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            }).sort({ dateTime: 1 }).lean();
            return MeasurementHours;
        });
    }
    getAlarmMeasurementHourByTagfiltered(tagId, type, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const hours = 3;
            const MeasurementHours = yield this.MeasurementHourModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            }).sort({ dateTime: -1 });
            if (type == 'oxd') {
                return [{
                        status: TypesAlarm_1.searchAlarm(MeasurementHours, 'en cero', hours),
                        locationId: MeasurementHours.length > 0 ? MeasurementHours[0].locationId : ''
                    }];
            }
            else if (type == 'oxs') {
                return [{
                        status: TypesAlarm_1.searchAlarm(MeasurementHours, 'pegados', hours),
                        locationId: MeasurementHours.length > 0 ? MeasurementHours[0].locationId : ''
                    }];
            }
            else if (type == 'sal') {
                if (TypesAlarm_1.searchAlarm(MeasurementHours, 'salinidad 33', hours) != 'ok') {
                    return [{
                            status: TypesAlarm_1.searchAlarm(MeasurementHours, 'salinidad 33', hours),
                            locationId: MeasurementHours.length > 0 ? MeasurementHours[0].locationId : ''
                        }];
                }
                else if (TypesAlarm_1.searchAlarm(MeasurementHours, 'salinidad 34', hours) != 'ok') {
                    return [{
                            status: TypesAlarm_1.searchAlarm(MeasurementHours, 'salinidad 34', hours),
                            locationId: MeasurementHours.length > 0 ? MeasurementHours[0].locationId : ''
                        }];
                }
                else if (TypesAlarm_1.searchAlarm(MeasurementHours, 'en cero', hours) != 'ok') {
                    return [{
                            status: TypesAlarm_1.searchAlarm(MeasurementHours, 'en cero', hours),
                            locationId: MeasurementHours.length > 0 ? MeasurementHours[0].locationId : ''
                        }];
                }
                else {
                    return [{
                            status: TypesAlarm_1.searchAlarm(MeasurementHours, 'en cero', hours),
                            locationId: MeasurementHours.length > 0 ? MeasurementHours[0].locationId : ''
                        }];
                }
            }
        });
    }
    getMeasurementHourByTagfilteredXY(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            }, ['dateTime', 'value']).sort({ dateTime: -1 }).lean();
            let xy = [];
            MeasurementHours.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementHourByTagfilteredXYActive(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                tagId: tagId,
                active: true,
                dateTime: { $gte: fini, $lte: ffin },
            }).sort({ dateTime: 1 });
            let xy = [];
            MeasurementHours.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementHourFilteredByTagsAndDate(tags, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                tagId: { $in: tags },
                dateTime: { $gte: fini, $lte: ffin },
            });
            return MeasurementHours;
        });
    }
    getMeasurementHourBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return MeasurementHours;
        });
    }
    getMeasurementHourByLocationfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin }
            }).lean();
            return MeasurementHours;
        });
    }
    getMeasurementHourByLocationsfiltered(locationsId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                $or: locationsId,
                dateTime: { $gte: fini + ".000Z", $lte: ffin + ".000Z" }
            }).lean();
            return MeasurementHours;
        });
    }
    getMeasurementHourByLocationallfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin },
            });
            return MeasurementHours;
        });
    }
    getMeasurementHourByLocationfilteredXY(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin },
            }).sort({ dateTime: 1 });
            let xy = [];
            MeasurementHours.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementHour(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHour = yield this.MeasurementHourModel.findById(id);
            return MeasurementHour;
        });
    }
    getMeasurementHoursByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                locationId: locationId,
            });
            return MeasurementHours;
        });
    }
    getMeasurementHoursBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                sensorId: sensorId,
            });
            return MeasurementHours;
        });
    }
    getMeasurementHoursByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourModel.find({
                tagId: tagId,
            });
            return MeasurementHours;
        });
    }
    createMeasurementHour(createMeasurementHourDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurementHour = new this.MeasurementHourModel(createMeasurementHourDTO);
            return yield newMeasurementHour.save();
        });
    }
    deleteMeasurementHour(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.MeasurementHourModel.findByIdAndDelete(id);
        });
    }
    updateMeasurementHour(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.MeasurementHourModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
MeasurementHourService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('MeasurementHour')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], MeasurementHourService);
exports.MeasurementHourService = MeasurementHourService;
//# sourceMappingURL=measurement-hour.service.js.map