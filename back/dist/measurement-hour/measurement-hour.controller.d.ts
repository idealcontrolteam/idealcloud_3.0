import { CreateMeasurementHourDTO } from './dto/measurement-hour.dto';
import { MeasurementHourService } from './measurement-hour.service';
import { TagService } from '../tag/tag.service';
import { Connection } from 'mongoose';
export declare class MeasurementHourController {
    private MeasurementHourService;
    private tagService;
    private connection;
    constructor(MeasurementHourService: MeasurementHourService, tagService: TagService, connection: Connection);
    validateIds: (body: any) => void;
    createMeasurementHourMultiple(res: any, body: any): Promise<any>;
    createMeasurementHour(res: any, body: CreateMeasurementHourDTO): Promise<any>;
    getAlarmMeasurementHourByTagfiltered(res: any, body: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHours(res: any): Promise<any>;
    getMeasurementHoursAll(res: any): Promise<any>;
    getMeasurementHour(res: any, MeasurementHourId: any): Promise<any>;
    getMeasurementHourByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHourByTagfilteredXY(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHourByTagfilteredXYActive(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHourBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHourByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHourByLocationsfiltered(res: any, body: any): Promise<any>;
    getMeasurementHourByLocation_allfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementHourByLocationfilteredXY(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurementHour(res: any, body: CreateMeasurementHourDTO, MeasurementHourId: any): Promise<any>;
    deleteMeasurementHour(res: any, MeasurementHourId: any): Promise<any>;
}
