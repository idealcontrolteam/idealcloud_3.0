"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_hour_dto_1 = require("./dto/measurement-hour.dto");
const measurement_hour_service_1 = require("./measurement-hour.service");
const moment = require("moment");
const tag_service_1 = require("../tag/tag.service");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let MeasurementHourController = class MeasurementHourController {
    constructor(MeasurementHourService, tagService, connection) {
        this.MeasurementHourService = MeasurementHourService;
        this.tagService = tagService;
        this.connection = connection;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key != 'value' && key != 'dateTime' && key != 'active') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
    }
    createMeasurementHourMultiple(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.connection.readyState === 0) {
                throw new common_1.BadRequestException('Sin Conexión');
            }
            let newMeasurementHour = {};
            body.map((MeasurementHour) => __awaiter(this, void 0, void 0, function* () {
                newMeasurementHour = yield this.MeasurementHourService.createMeasurementHour(MeasurementHour);
            }));
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'MeasurementHour created successfully',
                data: newMeasurementHour,
            });
        });
    }
    createMeasurementHour(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newMeasurementHour = yield this.MeasurementHourService.createMeasurementHour(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'MeasurementHour created successfully',
                data: newMeasurementHour,
            });
        });
    }
    getAlarmMeasurementHourByTagfiltered(res, body, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let alarma = [];
                let status_alarma = '';
                if (body.tagId.length == 1) {
                    alarma = yield this.MeasurementHourService.getAlarmMeasurementHourByTagfiltered(body.tagId[0], body.type, fini, ffin);
                    status_alarma = alarma[0].status;
                }
                else {
                    let alarma2 = [];
                    alarma = yield this.MeasurementHourService.getAlarmMeasurementHourByTagfiltered(body.tagId[0], body.type, fini, ffin);
                    alarma2 = yield this.MeasurementHourService.getAlarmMeasurementHourByTagfiltered(body.tagId[1], body.type, fini, ffin);
                    if (alarma[0].status != "sin datos" || alarma2[0].status != "sin datos") {
                        status_alarma = [alarma[0].status, alarma2[0].status].includes('pegados') ? 'pegados' : 'ok';
                    }
                    else {
                        status_alarma = 'sin datos';
                    }
                }
                let msg = alarma.length == 0
                    ? 'MeasurementHours not found'
                    : 'MeasurementHours fetched';
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: msg,
                    data: status_alarma,
                });
            }
            catch (err) {
                throw new common_1.BadRequestException('Ops! ocurrio un error');
            }
        });
    }
    getMeasurementHours(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHours();
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHoursAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHoursAll();
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHour(res, MeasurementHourId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!MeasurementHourId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('MeasurementHour id is not a valid ObjectId');
            }
            const MeasurementHour = yield this.MeasurementHourService.getMeasurementHour(MeasurementHourId);
            if (!MeasurementHour) {
                throw new common_1.NotFoundException('MeasurementHour not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'MeasurementHour found',
                data: MeasurementHour,
            });
        });
    }
    getMeasurementHourByTagfiltered(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByTagfiltered(tagId, fini, ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHourByTagfilteredXY(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByTagfilteredXY(tagId, fini, ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            let prom = "";
            let max = "";
            let min = "";
            let ultimo_valor = "";
            let ultima_fecha = "";
            if (MeasurementHours.length != 0) {
                prom = ((MeasurementHours.reduce((a, b) => +a + +b.y, 0) / MeasurementHours.length)).toFixed(1);
                max = MeasurementHours.reduce((max, b) => Math.max(max, b.y), MeasurementHours[0].y);
                min = MeasurementHours.reduce((min, b) => Math.min(min, b.y), MeasurementHours[0].y);
                ultimo_valor = MeasurementHours[MeasurementHours.length - 1].y;
                ultima_fecha = moment(MeasurementHours[MeasurementHours.length - 1].x).format('YYYY-MM-DD HH:mm:ss');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
                prom: prom,
                max: max,
                min: min,
                ultimo_valor: ultimo_valor,
                ultima_fecha: ultima_fecha,
            });
        });
    }
    getMeasurementHourByTagfilteredXYActive(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByTagfilteredXYActive(tagId, fini, ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            let prom = "";
            let max = "";
            let min = "";
            let ultimo_valor = "";
            let ultima_fecha = "";
            if (MeasurementHours.length != 0) {
                prom = ((MeasurementHours.reduce((a, b) => +a + +b.y, 0) / MeasurementHours.length)).toFixed(1);
                max = MeasurementHours.reduce((max, b) => Math.max(max, b.y), MeasurementHours[0].y);
                min = MeasurementHours.reduce((min, b) => Math.min(min, b.y), MeasurementHours[0].y);
                ultimo_valor = MeasurementHours[MeasurementHours.length - 1].y;
                ultima_fecha = moment(MeasurementHours[MeasurementHours.length - 1].x).format('YYYY-MM-DD HH:mm:ss');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
                prom: prom,
                max: max,
                min: min,
                ultimo_valor: ultimo_valor,
                ultima_fecha: ultima_fecha,
            });
        });
    }
    getMeasurementHourBySensorfiltered(res, sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourBySensorfiltered(sensorId, fini, ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHourByLocationfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByLocationfiltered(locationId, fini, ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHourByLocationsfiltered(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByLocationsfiltered(body.locationsId, body.fini, body.ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHourByLocation_allfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByLocationallfiltered(locationId, fini, ffin);
            let oxd = MeasurementHours.filter(m => m.type == "oxd");
            let oxs = MeasurementHours.filter(m => m.type == "oxs");
            let temp = MeasurementHours.filter(m => m.type == "temp");
            let sal = MeasurementHours.filter(m => m.type == "sal");
            let data = sal.map((o, i) => {
                return {
                    "oxd": oxd != null ? oxd[i].value : [],
                    "oxs": oxs != null ? oxs[i].value : [],
                    "temp": temp != null ? temp[i].value : [],
                    "sal": sal != null ? sal[i].value : [],
                    "locationId": o.locationId,
                    "dateTime": o.dateTime
                };
            });
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: data,
                count: MeasurementHours.length,
            });
        });
    }
    getMeasurementHourByLocationfilteredXY(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const MeasurementHours = yield this.MeasurementHourService.getMeasurementHourByLocationfilteredXY(locationId, fini, ffin);
            let msg = MeasurementHours.length == 0
                ? 'MeasurementHours not found'
                : 'MeasurementHours fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: MeasurementHours,
                count: MeasurementHours.length,
            });
        });
    }
    updateMeasurementHour(res, body, MeasurementHourId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!MeasurementHourId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('MeasurementHour id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedMeasurementHour = yield this.MeasurementHourService.updateMeasurementHour(MeasurementHourId, body);
            if (!updatedMeasurementHour) {
                throw new common_1.NotFoundException('MeasurementHour not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'MeasurementHour updated',
                data: updatedMeasurementHour,
            });
        });
    }
    deleteMeasurementHour(res, MeasurementHourId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedMeasurementHour = yield this.MeasurementHourService.deleteMeasurementHour(MeasurementHourId);
            if (!deletedMeasurementHour) {
                throw new common_1.NotFoundException('MeasurementHour not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'MeasurementHour deleted',
                data: deletedMeasurementHour,
            });
        });
    }
};
__decorate([
    common_1.Post('/multiple'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "createMeasurementHourMultiple", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, measurement_hour_dto_1.CreateMeasurementHourDTO]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "createMeasurementHour", null);
__decorate([
    common_1.Post('/alarm/tag/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getAlarmMeasurementHourByTagfiltered", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHours", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHoursAll", null);
__decorate([
    common_1.Get('/:MeasurementHourId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('MeasurementHourId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHour", null);
__decorate([
    common_1.Get('/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByTagfiltered", null);
__decorate([
    common_1.Get('/xy/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByTagfilteredXY", null);
__decorate([
    common_1.Get('/xy/tag/active/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByTagfilteredXYActive", null);
__decorate([
    common_1.Get('/sensor/:sensorId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('sensorId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourBySensorfiltered", null);
__decorate([
    common_1.Get('/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByLocationfiltered", null);
__decorate([
    common_1.Post('/locations_array'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByLocationsfiltered", null);
__decorate([
    common_1.Get('/location_all/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByLocation_allfiltered", null);
__decorate([
    common_1.Get('/xy/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "getMeasurementHourByLocationfilteredXY", null);
__decorate([
    common_1.Put('/:MeasurementHourId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('MeasurementHourId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, measurement_hour_dto_1.CreateMeasurementHourDTO, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "updateMeasurementHour", null);
__decorate([
    common_1.Delete('/:MeasurementHourId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('MeasurementHourId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementHourController.prototype, "deleteMeasurementHour", null);
MeasurementHourController = __decorate([
    common_1.Controller('measurement-hour'),
    __param(2, mongoose_1.InjectConnection()),
    __metadata("design:paramtypes", [measurement_hour_service_1.MeasurementHourService,
        tag_service_1.TagService,
        mongoose_2.Connection])
], MeasurementHourController);
exports.MeasurementHourController = MeasurementHourController;
//# sourceMappingURL=measurement-hour.controller.js.map