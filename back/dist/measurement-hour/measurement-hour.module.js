"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_hour_controller_1 = require("./measurement-hour.controller");
const measurement_hour_service_1 = require("./measurement-hour.service");
const mongoose_1 = require("@nestjs/mongoose");
const measurement_hour_schema_1 = require("./schemas/measurement-hour.schema");
const tag_module_1 = require("../tag/tag.module");
let MeasurementHourModule = class MeasurementHourModule {
};
MeasurementHourModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'MeasurementHour',
                    schema: measurement_hour_schema_1.MeasurementHourSchema,
                    collection: 'measurementHour',
                },
            ]),
            common_1.forwardRef(() => tag_module_1.TagModule),
        ],
        controllers: [measurement_hour_controller_1.MeasurementHourController],
        providers: [measurement_hour_service_1.MeasurementHourService,
        ],
        exports: [measurement_hour_service_1.MeasurementHourService],
    })
], MeasurementHourModule);
exports.MeasurementHourModule = MeasurementHourModule;
//# sourceMappingURL=measurement-hour.module.js.map