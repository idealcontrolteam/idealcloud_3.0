import { Model } from 'mongoose';
import { MeasurementHour } from './interfaces/measurement-hour.interface';
import { CreateMeasurementHourDTO } from './dto/measurement-hour.dto';
export declare class MeasurementHourService {
    private MeasurementHourModel;
    constructor(MeasurementHourModel: Model<MeasurementHour>);
    getMeasurementHours(): Promise<MeasurementHour[]>;
    getMeasurementHoursAll(): Promise<MeasurementHour[]>;
    getMeasurementHourByTagfiltered(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getAlarmMeasurementHourByTagfiltered(tagId: any, type: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementHourByTagfilteredXY(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementHourByTagfilteredXYActive(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementHourFilteredByTagsAndDate(tags: any, fini: any, ffin: any): Promise<MeasurementHour[]>;
    getMeasurementHourBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<MeasurementHour[]>;
    getMeasurementHourByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementHourByLocationsfiltered(locationsId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementHourByLocationallfiltered(locationId: any, fini: any, ffin: any): Promise<MeasurementHour[]>;
    getMeasurementHourByLocationfilteredXY(locationId: any, fini: any, ffin: any): Promise<MeasurementHour[]>;
    getMeasurementHour(id: any): Promise<MeasurementHour>;
    getMeasurementHoursByLocationId(locationId: any): Promise<MeasurementHour[]>;
    getMeasurementHoursBySensorId(sensorId: any): Promise<MeasurementHour[]>;
    getMeasurementHoursByTagId(tagId: any): Promise<MeasurementHour[]>;
    createMeasurementHour(createMeasurementHourDTO: CreateMeasurementHourDTO): Promise<MeasurementHour>;
    deleteMeasurementHour(id: any): Promise<MeasurementHour>;
    updateMeasurementHour(id: string, body: CreateMeasurementHourDTO): Promise<MeasurementHour>;
}
