import { Document } from 'mongoose';
export interface Zone extends Document {
    code?: string;
    name: string;
    active: boolean;
    workPlaceId: string;
}
