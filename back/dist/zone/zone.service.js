"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const location_service_1 = require("../location/location.service");
const tag_service_1 = require("../tag/tag.service");
let ZoneService = class ZoneService {
    constructor(zoneModel, locationService, tagService) {
        this.zoneModel = zoneModel;
        this.locationService = locationService;
        this.tagService = tagService;
    }
    getZones() {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.zoneModel.find();
            return zones;
        });
    }
    getZonesAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.zoneModel.find().populate('workPlaceId');
            return zones;
        });
    }
    getZoneWithTagsAndMeasurements(zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagService.getTagsByZoneId(zoneId);
            tags.map((tag, i) => {
                console.log('Position: ' + i);
            });
            return tags;
        });
    }
    getLocationsByZoneId(zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const locations = yield this.locationService.getLocationsByZoneId(zoneId);
            return locations;
        });
    }
    getLocationsByZoneIdTrue(zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const locations = yield this.locationService.getLocationsByZoneIdTrue(zoneId);
            return locations;
        });
    }
    getZonesByWorkPlaceId(workPlaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.zoneModel.find({ workPlaceId: workPlaceId });
            return zones;
        });
    }
    getZone(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const zone = yield this.zoneModel.findById(id);
            return zone;
        });
    }
    getTagsByZoneId(zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagService.getTagsByZoneActiveId(zoneId);
            return tags;
        });
    }
    getOxdByZoneId(zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            let name = "oxd";
            const tags = yield this.tagService.getOxdByZoneActiveId(zoneId);
            return tags;
        });
    }
    createZone(createZoneDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newZone = new this.zoneModel(createZoneDTO);
            return yield newZone.save();
        });
    }
    deleteZone(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.locationService.deleteLocationsByZoneId(id);
            return yield this.zoneModel.findByIdAndDelete(id);
        });
    }
    updateZone(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.zoneModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
ZoneService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Zone')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        location_service_1.LocationService,
        tag_service_1.TagService])
], ZoneService);
exports.ZoneService = ZoneService;
//# sourceMappingURL=zone.service.js.map