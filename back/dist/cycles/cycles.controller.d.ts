import { CyclesService } from './cycles.service';
import { CreateCyclesDTO } from './dto/cycles.dto';
export declare class CyclesController {
    private cyclesService;
    constructor(cyclesService: CyclesService);
    createCycles(res: any, body: CreateCyclesDTO): Promise<any>;
    getArrayCycles(res: any, body: CreateCyclesDTO): Promise<any>;
    getCyclessAll(res: any): Promise<any>;
    getCycles(res: any, cyclesId: any): Promise<any>;
    getCyclesWorkplace(res: any, workplaceId: any): Promise<any>;
    updateCycles(res: any, body: CreateCyclesDTO, cyclesId: any): Promise<any>;
    deleteCycles(res: any, cyclesId: any): Promise<any>;
}
