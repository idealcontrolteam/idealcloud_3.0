import { Model } from 'mongoose';
import { CreateCyclesDTO } from './dto/cycles.dto';
import { Cycles } from './interfaces/cycles.interface';
export declare class CyclesService {
    private cyclesModel;
    constructor(cyclesModel: Model<Cycles>);
    getCyclesAll(): Promise<Cycles[]>;
    getCycles(id: any): Promise<Cycles>;
    getCyclesWorkplace(id: any): Promise<any>;
    createCycles(createCyclesDTO: CreateCyclesDTO): Promise<any>;
    getArrayCycles(workplaces: any): Promise<any>;
    deleteCycles(id: any): Promise<Cycles>;
    updateCycles(id: string, body: CreateCyclesDTO): Promise<Cycles>;
}
