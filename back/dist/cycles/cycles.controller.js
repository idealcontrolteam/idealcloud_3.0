"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cycles_service_1 = require("./cycles.service");
const cycles_dto_1 = require("./dto/cycles.dto");
let CyclesController = class CyclesController {
    constructor(cyclesService) {
        this.cyclesService = cyclesService;
    }
    createCycles(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let newCycles = [];
            try {
                newCycles = yield this.cyclesService.createCycles(body);
            }
            catch (e) {
                throw new common_1.NotFoundException('Cycles not found');
            }
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Cycles created successfully',
                data: newCycles,
            });
        });
    }
    getArrayCycles(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let newCycles = [];
            try {
                newCycles = yield this.cyclesService.getArrayCycles(body);
            }
            catch (e) {
                throw new common_1.NotFoundException('Cycles not found');
            }
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Cycles created successfully',
                data: newCycles,
            });
        });
    }
    getCyclessAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const cycless = yield this.cyclesService.getCyclesAll();
            let msg = cycless.length == 0 ? 'Cycless not found' : 'Cycless fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: cycless,
                count: cycless.length,
            });
        });
    }
    getCycles(res, cyclesId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!cyclesId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Cycles id is not a valid  ObjectId');
            }
            const cycles = yield this.cyclesService.getCycles(cyclesId);
            if (!cycles) {
                throw new common_1.NotFoundException('Cycles not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Cycles found',
                data: cycles,
            });
        });
    }
    getCyclesWorkplace(res, workplaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const cycles = yield this.cyclesService.getCyclesWorkplace(workplaceId);
            if (!cycles) {
                throw new common_1.NotFoundException('Cycles not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Cycles found',
                data: cycles,
            });
        });
    }
    updateCycles(res, body, cyclesId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!cyclesId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Cycles id is not a valid  ObjectId');
            }
            const updatedCycles = yield this.cyclesService.updateCycles(cyclesId, body);
            if (!updatedCycles) {
                throw new common_1.NotFoundException('Cycles not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Cycles updated',
                data: updatedCycles,
            });
        });
    }
    deleteCycles(res, cyclesId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedCycles = yield this.cyclesService.deleteCycles(cyclesId);
            if (!deletedCycles) {
                throw new common_1.NotFoundException('Cycles not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Cycles deleted',
                data: deletedCycles,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, cycles_dto_1.CreateCyclesDTO]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "createCycles", null);
__decorate([
    common_1.Post('/workplaces'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, cycles_dto_1.CreateCyclesDTO]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "getArrayCycles", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "getCyclessAll", null);
__decorate([
    common_1.Get('/:cyclesId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('cyclesId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "getCycles", null);
__decorate([
    common_1.Get('/workplace/:workplaceId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('workplaceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "getCyclesWorkplace", null);
__decorate([
    common_1.Put('/:cyclesId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('cyclesId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, cycles_dto_1.CreateCyclesDTO, Object]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "updateCycles", null);
__decorate([
    common_1.Delete('/:cyclesId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('cyclesId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CyclesController.prototype, "deleteCycles", null);
CyclesController = __decorate([
    common_1.Controller('cycles'),
    __metadata("design:paramtypes", [cycles_service_1.CyclesService])
], CyclesController);
exports.CyclesController = CyclesController;
//# sourceMappingURL=cycles.controller.js.map