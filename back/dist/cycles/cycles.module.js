"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cycles_controller_1 = require("./cycles.controller");
const cycles_service_1 = require("./cycles.service");
const mongoose_1 = require("@nestjs/mongoose");
const cycles_schema_1 = require("./schemas/cycles.schema");
let CyclesModule = class CyclesModule {
};
CyclesModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'Cycles', schema: cycles_schema_1.CyclesSchema, collection: 'cycles' },
            ]),
        ],
        controllers: [cycles_controller_1.CyclesController],
        providers: [cycles_service_1.CyclesService]
    })
], CyclesModule);
exports.CyclesModule = CyclesModule;
//# sourceMappingURL=cycles.module.js.map