import { Model } from 'mongoose';
import { EfectividadMonth } from './interfaces/efectividad-month.interface';
import { CreateEfectividadMonthDTO } from './dto/efectividad-month.dto';
export declare class EfectividadMonthService {
    private EfectividadMonthModel;
    constructor(EfectividadMonthModel: Model<EfectividadMonth>);
    getMeasurements(): Promise<EfectividadMonth[]>;
    getMeasurementsAll(): Promise<EfectividadMonth[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<EfectividadMonth[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<EfectividadMonth[]>;
    restartDate(f1: any, f2: any): number;
    getDifDays(fe1: any, fe2: any): number;
    getMeasurementByLocationfiltered(workplaceId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurement(id: any): Promise<EfectividadMonth>;
    getMeasurementsByLocationId(locationId: any): Promise<EfectividadMonth[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<EfectividadMonth[]>;
    getMeasurementsByTagId(tagId: any): Promise<EfectividadMonth[]>;
    createMeasurement(createEfectividadMonthDTO: CreateEfectividadMonthDTO): Promise<EfectividadMonth>;
    deleteMeasurement(id: any): Promise<EfectividadMonth>;
    updateMeasurement(id: string, body: CreateEfectividadMonthDTO): Promise<EfectividadMonth>;
}
