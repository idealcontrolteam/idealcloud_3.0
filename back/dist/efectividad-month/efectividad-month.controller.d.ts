import { CreateEfectividadMonthDTO } from './dto/efectividad-month.dto';
import { EfectividadMonthService } from './efectividad-month.service';
export declare class EfectividadMonthController {
    private measurementService;
    constructor(measurementService: EfectividadMonthService);
    validateIds: (body: any) => void;
    createMeasurement(res: any, body: CreateEfectividadMonthDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, workplaceId: any, fini: any, ffin: any, body: any): Promise<any>;
    updateMeasurement(res: any, body: CreateEfectividadMonthDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
