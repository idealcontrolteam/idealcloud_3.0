"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.EfectividadMonthSchema = new mongoose_1.Schema({
    count: Number,
    locationId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    workplaceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'WorkPlace',
        required: true,
    },
    start_date: Date,
    end_date: Date,
    active: Boolean,
    dateTime: Date,
}, { versionKey: false });
//# sourceMappingURL=efectividad-month.schema.js.map