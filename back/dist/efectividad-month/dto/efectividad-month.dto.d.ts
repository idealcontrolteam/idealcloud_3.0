export declare class CreateEfectividadMonthDTO {
    count: Number;
    locationId: string;
    start_date: Date;
    end_date: Date;
    active: Boolean;
    workplaceId: String;
    dateTime: Date;
}
