"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const moment = require("moment");
let EfectividadMonthService = class EfectividadMonthService {
    constructor(EfectividadMonthModel) {
        this.EfectividadMonthModel = EfectividadMonthModel;
    }
    getMeasurements() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel.find();
            return measurements;
        });
    }
    getMeasurementsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel
                .find()
                .populate('tagId sensorId locationId');
            return measurements;
        });
    }
    getMeasurementByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    restartDate(f1, f2) {
        return f2 - f1;
    }
    getDifDays(fe1, fe2) {
        let f1 = new Date(fe1);
        let f2 = new Date(fe2);
        let diff = this.restartDate(f1, f2);
        return Math.floor(diff / (1000 * 60 * 60 * 24));
    }
    getMeasurementByLocationfiltered(workplaceId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurements = [];
            let f1 = moment().format('YYYY-MM-DDT00:00:00');
            let f = new Date(f1);
            let start_date = moment().startOf('month').format('YYYY-MM-DDT00:00:00');
            let diff = 0;
            let dif_resta = this.restartDate(f, new Date());
            let diff_ = this.getDifDays(start_date, new Date());
            if (workplaceId == "General") {
                measurements = yield this.EfectividadMonthModel.find({
                    start_date: { $gte: fini, $lt: ffin },
                });
            }
            else {
                measurements = yield this.EfectividadMonthModel.find({
                    workplaceId: workplaceId,
                    start_date: { $gte: fini, $lt: ffin },
                });
            }
            return measurements.map(m => {
                if (new Date(m.start_date).getTime() === new Date(moment(start_date).format("YYYY-MM-01T00:00:00") + ".000Z").getTime()) {
                    diff = diff_;
                    if (diff == 0) {
                        diff = 1;
                    }
                }
                else {
                    diff = diff_;
                }
                let p = 0;
                let c = 0;
                if (m.count > 288) {
                    c = m.count - (Math.floor(dif_resta / (1000 * 60)) / 5);
                    p = c * 100 / (288 * diff);
                    if (Math.sign(p) == -1) {
                        p = m.count * 100 / (288 * diff);
                    }
                }
                else {
                    p = m.count * 100 / (288 * diff);
                }
                return {
                    "count": c,
                    "end_date": m.end_date,
                    "locationId": m.locationId,
                    "workplaceId": m.workplaceId,
                    "start_date": m.start_date,
                    "dateTime": m.dateTime,
                    "porcentaje": p
                };
            });
        });
    }
    getMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.EfectividadMonthModel.findById(id);
            return measurement;
        });
    }
    getMeasurementsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel.find({
                locationId: locationId,
            });
            return measurements;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel.find({
                sensorId: sensorId,
            });
            return measurements;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadMonthModel.find({
                tagId: tagId,
            });
            return measurements;
        });
    }
    createMeasurement(createEfectividadMonthDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurement = new this.EfectividadMonthModel(createEfectividadMonthDTO);
            return yield newMeasurement.save();
        });
    }
    deleteMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.EfectividadMonthModel.findByIdAndDelete(id);
        });
    }
    updateMeasurement(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.EfectividadMonthModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
EfectividadMonthService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('EfectividadMonth')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], EfectividadMonthService);
exports.EfectividadMonthService = EfectividadMonthService;
//# sourceMappingURL=efectividad-month.service.js.map