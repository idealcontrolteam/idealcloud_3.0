"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.TagConfigControlSchema = new mongoose_1.Schema({
    code: String,
    code_control: String,
    name: String,
    setpoint: Number,
    histeresis: Number,
    timeOn: Number,
    timeOff: Number,
    histeresisMax: Number,
    histeresisMin: Number,
    active: Boolean,
    locationId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: false,
    },
}, { versionKey: false });
//# sourceMappingURL=TagConfigControl.schema.js.map