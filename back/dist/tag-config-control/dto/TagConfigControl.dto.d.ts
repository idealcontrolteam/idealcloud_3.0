export declare class CreateConfigControlDTO {
    code: string;
    code_control: string;
    name: string;
    setpoint: number;
    histeresis: number;
    timeOn: number;
    timeOff: number;
    histeresisMax: number;
    histeresisMin: number;
    active: boolean;
    locationId: string;
}
