"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tag_config_control_service_1 = require("./tag-config-control.service");
const TagConfigControl_dto_1 = require("./dto/TagConfigControl.dto");
let TagConfigControlController = class TagConfigControlController {
    constructor(tagConfigService) {
        this.tagConfigService = tagConfigService;
    }
    createTagConfigControl(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newTagConfigControl = yield this.tagConfigService.createTagConfigControl(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Tag config control created successfully',
                data: newTagConfigControl,
            });
        });
    }
    getTagConfigControls(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const tagConfigControls = yield this.tagConfigService.getTagConfigControls();
            let msg = tagConfigControls.length == 0
                ? 'Tag config control not found'
                : 'Tag config control fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tagConfigControls,
                count: tagConfigControls.length,
            });
        });
    }
    getTagConfigControlsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const tagConfigControls = yield this.tagConfigService.getTagConfigControlsAll();
            let msg = tagConfigControls.length == 0
                ? 'Tag config control not found'
                : 'Tag config control fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tagConfigControls,
                count: tagConfigControls.length,
            });
        });
    }
    getTagConfigControl(res, tagConfigControlId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagConfigControlId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag config control id is not a valid ObjectId');
            }
            const tagConfigControl = yield this.tagConfigService.getTagConfigControl(tagConfigControlId);
            if (!tagConfigControl) {
                throw new common_1.NotFoundException('Tag config control not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag config control found',
                data: tagConfigControl,
            });
        });
    }
    updateTagConfigControl(res, body, tagConfigControlId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagConfigControlId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag config control id is not a valid ObjectId');
            }
            const updatedTagConfigControl = yield this.tagConfigService.updateTagConfigControl(tagConfigControlId, body);
            if (!updatedTagConfigControl) {
                throw new common_1.NotFoundException('Tag config control not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag config control updated',
                data: updatedTagConfigControl,
            });
        });
    }
    deleteTagConfigControl(res, tagConfigControlId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedTagConfigControl = yield this.tagConfigService.deleteTagConfigControl(tagConfigControlId);
            if (!deletedTagConfigControl) {
                throw new common_1.NotFoundException('Tag config control not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag config control deleted',
                data: deletedTagConfigControl,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, TagConfigControl_dto_1.CreateConfigControlDTO]),
    __metadata("design:returntype", Promise)
], TagConfigControlController.prototype, "createTagConfigControl", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TagConfigControlController.prototype, "getTagConfigControls", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TagConfigControlController.prototype, "getTagConfigControlsAll", null);
__decorate([
    common_1.Get('/:tagConfigControlId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagConfigControlId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagConfigControlController.prototype, "getTagConfigControl", null);
__decorate([
    common_1.Put('/:roleId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('tagConfigControlId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, TagConfigControl_dto_1.CreateConfigControlDTO, Object]),
    __metadata("design:returntype", Promise)
], TagConfigControlController.prototype, "updateTagConfigControl", null);
__decorate([
    common_1.Delete('/:tagConfigControlId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagConfigControlId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagConfigControlController.prototype, "deleteTagConfigControl", null);
TagConfigControlController = __decorate([
    common_1.Controller('tagConfigControl'),
    __metadata("design:paramtypes", [tag_config_control_service_1.TagConfigControlService])
], TagConfigControlController);
exports.TagConfigControlController = TagConfigControlController;
//# sourceMappingURL=tag-config-control.controller.js.map