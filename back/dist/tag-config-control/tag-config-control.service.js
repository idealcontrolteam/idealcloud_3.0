"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let TagConfigControlService = class TagConfigControlService {
    constructor(tagConfigControlModel) {
        this.tagConfigControlModel = tagConfigControlModel;
    }
    getTagConfigControls() {
        return __awaiter(this, void 0, void 0, function* () {
            const tagConfigControls = yield this.tagConfigControlModel.find();
            return tagConfigControls;
        });
    }
    getTagConfigControlsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const tagConfigControls = yield this.tagConfigControlModel
                .find()
                .populate('tagId');
            return tagConfigControls;
        });
    }
    geTagConfigControlsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tagConfigControls = yield this.tagConfigControlModel.find({
                tagId: tagId,
            });
            return tagConfigControls;
        });
    }
    getTagConfigControl(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tagConfigControl = yield this.tagConfigControlModel.findById(id);
            return tagConfigControl;
        });
    }
    createTagConfigControl(createConfigControlDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newTagConfigControls = new this.tagConfigControlModel(createConfigControlDTO);
            return yield newTagConfigControls.save();
        });
    }
    deleteTagConfigControl(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.tagConfigControlModel.findByIdAndDelete(id);
        });
    }
    updateTagConfigControl(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.tagConfigControlModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
TagConfigControlService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('TagConfigControl')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], TagConfigControlService);
exports.TagConfigControlService = TagConfigControlService;
//# sourceMappingURL=tag-config-control.service.js.map