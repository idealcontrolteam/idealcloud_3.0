import { Model } from 'mongoose';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';
import { TagType } from './interfaces/tagType.interface';
import { CreateTagTypeDTO } from './dto/tagType.dto';
export declare class TagTypeService {
    private tagTypeModel;
    private tagService;
    constructor(tagTypeModel: Model<TagType>, tagService: TagService);
    getTagTypes(): Promise<TagType[]>;
    getTagType(id: any): Promise<TagType>;
    getTagsByTagTypeId(tagTypeId: any): Promise<Tag[]>;
    createTagType(createTagTypeDTO: CreateTagTypeDTO): Promise<TagType>;
    deleteTagType(id: any): Promise<TagType>;
    updateTagType(id: string, body: CreateTagTypeDTO): Promise<TagType>;
}
