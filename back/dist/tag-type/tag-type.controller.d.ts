import { CreateTagTypeDTO } from './dto/tagType.dto';
import { TagTypeService } from './tag-type.service';
export declare class TagTypeController {
    private tagTypeService;
    constructor(tagTypeService: TagTypeService);
    createTagType(res: any, body: CreateTagTypeDTO): Promise<any>;
    getTagType(res: any): Promise<any>;
    getTagsByTagTypeId(res: any, tagTypeId: any): Promise<any>;
    getTagTypeById(res: any, tagTypeId: any): Promise<any>;
    updateTagType(res: any, body: CreateTagTypeDTO, tagTypeid: any): Promise<any>;
    deleteTagType(res: any, tagTypeid: any): Promise<any>;
}
