export declare class CreateTagTypeDTO {
    name: string;
    active: boolean;
}
