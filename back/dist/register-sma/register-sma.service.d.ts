import { Model } from 'mongoose';
import { RegisterSMA } from './interfaces/register-sma.interface';
import { CreateRegisterSMADTO } from './dto/register-sma.dto';
export declare class RegisterSMAService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<RegisterSMA>);
    getMeasurements(): Promise<RegisterSMA[]>;
    getMeasurementsAll(): Promise<RegisterSMA[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurement(id: any): Promise<RegisterSMA>;
    getMeasurement_dispositivo(id: any): Promise<any[]>;
    getMeasurementsByLocationId(locationId: any): Promise<RegisterSMA[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<RegisterSMA[]>;
    getMeasurementsByTagId(tagId: any): Promise<RegisterSMA[]>;
    createMeasurement(createRegisterSMADTO: CreateRegisterSMADTO): Promise<RegisterSMA>;
    deleteMeasurement(id: any): Promise<RegisterSMA>;
    updateMeasurement(id: string, body: CreateRegisterSMADTO): Promise<RegisterSMA>;
}
