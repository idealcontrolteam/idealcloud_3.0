export declare class CreateRegisterSMADTO {
    dateTime: string;
    dispositivoId: string;
    parametros: any;
    respuesta: any;
    procesoId: string;
}
