"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.RegisterSMASchema = new mongoose_1.Schema({
    dateTime: String,
    dispositivoId: String,
    parametros: Array,
    respuesta: Array,
    procesoId: String,
}, { versionKey: false });
//# sourceMappingURL=register-sma.schema.js.map