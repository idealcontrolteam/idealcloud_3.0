import { CreateRegisterSMADTO } from './dto/register-sma.dto';
import { RegisterSMAService } from './register-sma.service';
export declare class RegisterSMAController {
    private measurementService;
    constructor(measurementService: RegisterSMAService);
    createMeasurement(res: any, body: CreateRegisterSMADTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurement_dispositivo(res: any, measurementId: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, body: any): Promise<any>;
    updateMeasurement(res: any, body: CreateRegisterSMADTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
