import { Document } from 'mongoose';
export interface RegisterSMA extends Document {
    dateTime: string;
    dispositivoId: string;
    parametros: any;
    respuesta?: any;
    procesoId?: string;
}
