"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const register_sma_controller_1 = require("./register-sma.controller");
const register_sma_service_1 = require("./register-sma.service");
const mongoose_1 = require("@nestjs/mongoose");
const register_sma_schema_1 = require("./schemas/register-sma.schema");
let RegisterSMAModule = class RegisterSMAModule {
};
RegisterSMAModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'Registros',
                    schema: register_sma_schema_1.RegisterSMASchema,
                    collection: 'registros',
                },
            ]),
        ],
        controllers: [register_sma_controller_1.RegisterSMAController],
        providers: [register_sma_service_1.RegisterSMAService],
    })
], RegisterSMAModule);
exports.RegisterSMAModule = RegisterSMAModule;
//# sourceMappingURL=register-sma.module.js.map