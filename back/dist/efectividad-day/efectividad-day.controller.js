"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const efectividad_day_dto_1 = require("./dto/efectividad-day.dto");
const efectividad_day_service_1 = require("./efectividad-day.service");
const moment = require("moment");
let EfectividadDayController = class EfectividadDayController {
    constructor(measurementService) {
        this.measurementService = measurementService;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key != 'value' && key != 'dateTime' && key != 'active') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
    }
    createMeasurement(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurement = yield this.measurementService.createMeasurement(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    restarfechas(f1, f2) {
        return f2 - f1;
    }
    createMeasurementMultiple(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let start_date = moment().startOf('month').format('YYYY-MM-DDT00:00:00');
            let end_date = moment().endOf('month').format('YYYY-MM-DDT00:00:00');
            let f1 = new Date(start_date);
            let f2 = new Date(end_date);
            let diff = this.restarfechas(f1, f2);
            let diferenciaDias = Math.floor(diff / (1000 * 60 * 60 * 24)) + 1;
            let c = 1;
            let efectividades = [];
            while (c <= diferenciaDias) {
                let data = {
                    "count": 0,
                    "end_date": moment(end_date).format(`YYYY-MM-DDT23:59:00`) + ".000Z",
                    "locationId": body.locationId,
                    "workplaceId": body.workplaceId,
                    "start_date": moment(end_date).format(`YYYY-MM-DDT00:00:00`) + ".000Z"
                };
                efectividades.push(data);
                end_date = moment(f2).subtract(c, "days").format(`YYYY-MM-DDT00:00:00`);
                c++;
            }
            let newMeasurement = {};
            efectividades.map((Measurement) => __awaiter(this, void 0, void 0, function* () {
                let find_efectividad = yield this.measurementService.getMeasurementsExist(body.locationId, Measurement.start_date);
                if (find_efectividad.length == 0) {
                    newMeasurement = yield this.measurementService.createMeasurement(Measurement);
                    console.log("Se crea");
                }
                else {
                    console.log("Si existe");
                }
            }));
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    getMeasurements(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurements();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementsAll();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            const measurement = yield this.measurementService.getMeasurement(measurementId);
            if (!measurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement found',
                data: measurement,
            });
        });
    }
    getMeasurementByTagfiltered(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByTagfiltered(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementBySensorfiltered(res, sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementBySensorfiltered(sensorId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationfiltered(locationId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getTendencia(v1, v2) {
        if (v1 == 0 && v2 == 0) {
            return "cero";
        }
        else if (v1 == v2) {
            return "right";
        }
        else {
            return parseInt(v1) > parseInt(v2) ? "up" : "down";
        }
    }
    restarDate(f1, f2) {
        return f1 - f2;
    }
    getMeasurementByWorkplaceArrowfiltered(res, workplaceId, f1, f2, f3, f4, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurement1 = [];
            let measurement2 = [];
            let measurement3 = [];
            let measurement4 = [];
            measurement1 = yield this.measurementService.getMeasurementByWorkplaceArrowfiltered(workplaceId, f1);
            measurement2 = yield this.measurementService.getMeasurementByWorkplaceArrowfiltered(workplaceId, f2);
            measurement3 = yield this.measurementService.getMeasurementByWorkplaceArrowfiltered(workplaceId, f3);
            measurement4 = yield this.measurementService.getMeasurementByWorkplaceArrowfiltered(workplaceId, f4);
            let medicion = [];
            let prom1 = 0, prom2 = 0, prom3 = 0, prom4 = 0;
            let hoy = moment().format("YYYY-MM-DDT00:00:00");
            let now = new Date();
            let h1 = new Date(hoy);
            let diff = this.restarfechas(h1, now);
            let diferecia_fechas = Math.floor(diff / (1000 * 60));
            let dif_100 = diferecia_fechas / 5;
            measurement1.length > 0 ?
                prom1 = parseFloat(((measurement1.reduce((a, b) => +a + +(b.count), 0) / measurement1.length)).toFixed(1)) * 100 / dif_100 :
                prom1;
            measurement2.length > 0 ?
                prom2 = parseFloat(((measurement2.reduce((a, b) => +a + +(b.count), 0) / measurement2.length)).toFixed(1)) * 100 / 288 :
                prom2;
            measurement3.length > 0 ?
                prom3 = parseFloat(((measurement3.reduce((a, b) => +a + +(b.count), 0) / measurement3.length)).toFixed(1)) * 100 / 288 :
                prom3;
            measurement4.length > 0 ?
                prom4 = parseFloat(((measurement4.reduce((a, b) => +a + +(b.count), 0) / measurement4.length)).toFixed(1)) * 100 / 288 :
                prom4;
            medicion.push({ "fecha": f1,
                "prom": prom1,
                "tendencia": this.getTendencia(prom1, prom2)
            });
            medicion.push({ "fecha": f2,
                "prom": prom2,
                "tendencia": this.getTendencia(prom2, prom3)
            });
            medicion.push({ "fecha": f3,
                "prom": prom3,
                "tendencia": this.getTendencia(prom3, prom4)
            });
            medicion.push({ "fecha": f4,
                "prom": prom4,
                "tendencia": this.getTendencia(prom4, prom3)
            });
            let msg = measurement1.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: medicion,
                count: medicion.length,
            });
        });
    }
    getMeasurementByWorkplacefiltered(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurements = [];
            measurements = yield this.measurementService.getMeasurementByWorkplacefiltered(body.workplaceId, body.fini, body.ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    updateMeasurement(res, body, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedMeasurement = yield this.measurementService.updateMeasurement(measurementId, body);
            if (!updatedMeasurement) {
                throw new common_1.NotFoundException('Measurement not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement updated',
                data: updatedMeasurement,
            });
        });
    }
    deleteMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedMeasurement = yield this.measurementService.deleteMeasurement(measurementId);
            if (!deletedMeasurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement deleted',
                data: deletedMeasurement,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, efectividad_day_dto_1.CreateEfectividadDayDTO]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "createMeasurement", null);
__decorate([
    common_1.Post('/multiple'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "createMeasurementMultiple", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurements", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurementsAll", null);
__decorate([
    common_1.Get('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurement", null);
__decorate([
    common_1.Get('/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurementByTagfiltered", null);
__decorate([
    common_1.Get('/sensor/:sensorId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('sensorId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurementBySensorfiltered", null);
__decorate([
    common_1.Get('/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurementByLocationfiltered", null);
__decorate([
    common_1.Get('/workplace/arrow/:workplaceId/:f1/:f2/:f3/:f4'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('workplaceId')),
    __param(2, common_1.Param('f1')),
    __param(3, common_1.Param('f2')),
    __param(4, common_1.Param('f3')),
    __param(5, common_1.Param('f4')),
    __param(6, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurementByWorkplaceArrowfiltered", null);
__decorate([
    common_1.Post('/workplace'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "getMeasurementByWorkplacefiltered", null);
__decorate([
    common_1.Put('/:measurementId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, efectividad_day_dto_1.CreateEfectividadDayDTO, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "updateMeasurement", null);
__decorate([
    common_1.Delete('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EfectividadDayController.prototype, "deleteMeasurement", null);
EfectividadDayController = __decorate([
    common_1.Controller('efectivity_day'),
    __metadata("design:paramtypes", [efectividad_day_service_1.EfectividadDayService])
], EfectividadDayController);
exports.EfectividadDayController = EfectividadDayController;
//# sourceMappingURL=efectividad-day.controller.js.map