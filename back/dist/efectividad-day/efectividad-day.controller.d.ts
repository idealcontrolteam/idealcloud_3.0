import { CreateEfectividadDayDTO } from './dto/efectividad-day.dto';
import { EfectividadDayService } from './efectividad-day.service';
export declare class EfectividadDayController {
    private measurementService;
    constructor(measurementService: EfectividadDayService);
    validateIds: (body: any) => void;
    createMeasurement(res: any, body: CreateEfectividadDayDTO): Promise<any>;
    restarfechas(f1: any, f2: any): number;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getTendencia(v1: any, v2: any): "right" | "up" | "down" | "cero";
    restarDate(f1: any, f2: any): number;
    getMeasurementByWorkplaceArrowfiltered(res: any, workplaceId: any, f1: any, f2: any, f3: any, f4: any, body: any): Promise<any>;
    getMeasurementByWorkplacefiltered(res: any, body: any): Promise<any>;
    updateMeasurement(res: any, body: CreateEfectividadDayDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
