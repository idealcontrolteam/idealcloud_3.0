import { Model } from 'mongoose';
import { EfectividadDay } from './interfaces/efectividad-day.interface';
import { CreateEfectividadDayDTO } from './dto/efectividad-day.dto';
export declare class EfectividadDayService {
    private EfectividadDayModel;
    constructor(EfectividadDayModel: Model<EfectividadDay>);
    getMeasurements(): Promise<EfectividadDay[]>;
    getMeasurementsAll(): Promise<EfectividadDay[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<EfectividadDay[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<EfectividadDay[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<EfectividadDay[]>;
    restartDate(f2: any, f1: any): number;
    getMeasurementByWorkplaceArrowfiltered(workplaceId: any, fini: any): Promise<any[]>;
    getMeasurementByWorkplacefiltered(workplaceIds: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementOneWorkplacefiltered(workplaceId: any, fini: any): Promise<any[]>;
    getMeasurement(id: any): Promise<EfectividadDay>;
    getMeasurementsExist(locationId: any, start_date: any): Promise<EfectividadDay[]>;
    getMeasurementsByLocationId(locationId: any): Promise<EfectividadDay[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<EfectividadDay[]>;
    getMeasurementsByTagId(tagId: any): Promise<EfectividadDay[]>;
    createMeasurement(createEfectividadDayDTO: CreateEfectividadDayDTO): Promise<any>;
    deleteMeasurement(id: any): Promise<EfectividadDay>;
    updateMeasurement(id: string, body: CreateEfectividadDayDTO): Promise<EfectividadDay>;
}
