"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const efectividad_day_controller_1 = require("./efectividad-day.controller");
const efectividad_day_service_1 = require("./efectividad-day.service");
const mongoose_1 = require("@nestjs/mongoose");
const efectividad_day_schema_1 = require("./schemas/efectividad-day.schema");
let EfectividadDayModule = class EfectividadDayModule {
};
EfectividadDayModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'EfectividadDay',
                    schema: efectividad_day_schema_1.EfectividadDaySchema,
                    collection: 'efectivityDay',
                },
            ]),
        ],
        controllers: [efectividad_day_controller_1.EfectividadDayController],
        providers: [efectividad_day_service_1.EfectividadDayService],
    })
], EfectividadDayModule);
exports.EfectividadDayModule = EfectividadDayModule;
//# sourceMappingURL=efectividad-day.module.js.map