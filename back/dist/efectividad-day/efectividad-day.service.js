"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const moment = require("moment");
let EfectividadDayService = class EfectividadDayService {
    constructor(EfectividadDayModel) {
        this.EfectividadDayModel = EfectividadDayModel;
    }
    getMeasurements() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find();
            return measurements;
        });
    }
    getMeasurementsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel
                .find()
                .populate('tagId sensorId locationId');
            return measurements;
        });
    }
    getMeasurementByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin },
            });
            return measurements;
        });
    }
    restartDate(f2, f1) {
        return f2 - f1;
    }
    getMeasurementByWorkplaceArrowfiltered(workplaceId, fini) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurements = [];
            measurements = yield this.EfectividadDayModel.find({
                workplaceId: workplaceId,
                start_date: fini,
            });
            let data = [];
            data = measurements.map(m => {
                return {
                    "count": m.count,
                };
            });
            return data;
        });
    }
    getMeasurementByWorkplacefiltered(workplaceIds, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurements = [];
            measurements = yield this.EfectividadDayModel.find({
                $or: workplaceIds,
                start_date: { $gte: fini, $lte: ffin },
            });
            let f1 = moment().format('YYYY-MM-DDT00:00:00');
            let f2 = new Date();
            let diff = 0;
            diff = this.restartDate(f2, new Date(f1));
            let diff_100 = (diff / (1000 * 60)) / 5;
            let data = [];
            data = measurements.map(m => {
                if (m.start_date >= f1 + "000.Z") {
                    return {
                        "count": m.count,
                        "end_date": m.end_date,
                        "dateTime": m.dateTime,
                        "locationId": m.locationId,
                        "workplaceId": m.workplaceId,
                        "start_date": m.start_date,
                        "porcentaje": isNaN(m.count * 100 / diff_100) ? 0 : m.count * 100 / 288,
                    };
                }
                else {
                    return {
                        "count": m.count,
                        "end_date": m.end_date,
                        "dateTime": m.dateTime,
                        "locationId": m.locationId,
                        "workplaceId": m.workplaceId,
                        "start_date": m.start_date,
                        "porcentaje": isNaN(m.count * 100 / 288) ? 0 : m.count * 100 / 288,
                    };
                }
            });
            data
                .push({ "promedio_general": ((data.reduce((a, b) => +a + +(b.porcentaje), 0) / data.length)).toFixed(1) });
            return data;
        });
    }
    getMeasurementOneWorkplacefiltered(workplaceId, fini) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurements = [];
            if (workplaceId == "General") {
                measurements = yield this.EfectividadDayModel.find({
                    start_date: fini,
                });
            }
            else {
                measurements = yield this.EfectividadDayModel.find({
                    workplaceId: workplaceId,
                    start_date: fini,
                });
            }
            return measurements.map(m => {
                return {
                    "count": m.count,
                    "end_date": m.end_date,
                    "dateTime": m.dateTime,
                    "locationId": m.locationId,
                    "workplaceId": m.workplaceId,
                    "start_date": m.start_date,
                    "porcentaje": m.count * 100 / 288
                };
            });
        });
    }
    getMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.EfectividadDayModel.findById(id);
            return measurement;
        });
    }
    getMeasurementsExist(locationId, start_date) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                locationId,
                start_date,
            });
            return measurements;
        });
    }
    getMeasurementsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                locationId: locationId,
            });
            return measurements;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                sensorId: sensorId,
            });
            return measurements;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.EfectividadDayModel.find({
                tagId: tagId,
            });
            return measurements;
        });
    }
    createMeasurement(createEfectividadDayDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            let newMeasurement = new this.EfectividadDayModel(createEfectividadDayDTO);
            return yield newMeasurement.save();
        });
    }
    deleteMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.EfectividadDayModel.findByIdAndDelete(id);
        });
    }
    updateMeasurement(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.EfectividadDayModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
EfectividadDayService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('EfectividadDay')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], EfectividadDayService);
exports.EfectividadDayService = EfectividadDayService;
//# sourceMappingURL=efectividad-day.service.js.map