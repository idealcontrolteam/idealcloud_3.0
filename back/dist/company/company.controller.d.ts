import { CreateCompanyDTO } from './dto/company.dto';
import { CompanyService } from './company.service';
export declare class CompanyController {
    private companyService;
    constructor(companyService: CompanyService);
    createCompany(res: any, body: CreateCompanyDTO, file: any): Promise<any>;
    getCompanies(res: any): Promise<any>;
    getWorkPlacesByCompanyId(res: any, companyId: any): Promise<any>;
    getUsersByCompanyId(res: any, companyId: any): Promise<any>;
    getNameCompanyId(res: any, name: any): Promise<any>;
    getCompany(res: any, companyId: any): Promise<any>;
    updateCompany(res: any, body: CreateCompanyDTO, companyId: any): Promise<any>;
    deleteCompany(res: any, companyId: any): Promise<any>;
}
