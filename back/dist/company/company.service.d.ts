import { Model } from 'mongoose';
import { Company } from './interfaces/company.interface';
import { CreateCompanyDTO } from './dto/company.dto';
import { WorkPlaceService } from '../work-place/work-place.service';
import { WorkPlace } from '../work-place/interfaces/workPlace.interface';
import { UserService } from '../user/user.service';
import { User } from '../user/interfaces/user.interface';
export declare class CompanyService {
    private companyModel;
    private workPlaceService;
    private userService;
    constructor(companyModel: Model<Company>, workPlaceService: WorkPlaceService, userService: UserService);
    getCompanies(): Promise<Company[]>;
    countCompanies(): Promise<number>;
    getWorkPlacesByCompanyId(companyId: any): Promise<WorkPlace[]>;
    getUsersByCompanyId(companyId: any): Promise<User[]>;
    getNameCompanyId(name: any): Promise<any>;
    getCompany(id: any): Promise<Company>;
    createCompany(createCompanyDTO: CreateCompanyDTO): Promise<Company>;
    deleteCompany(id: any): Promise<Company>;
    updateCompany(id: string, createCompanyDTO: CreateCompanyDTO): Promise<Company>;
}
