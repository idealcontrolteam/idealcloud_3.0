import { Document } from 'mongoose';
export interface Device extends Document {
    name: string;
    active: boolean;
    modelId: string;
    tcpConnectionId?: string;
    serialConnectionId?: string;
    gatewayId: string;
}
