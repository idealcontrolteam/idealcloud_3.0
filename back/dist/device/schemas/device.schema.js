"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.DeviceSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
    modelId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Model',
        required: true,
    },
    tcpConnectionId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'TcpConnection',
        required: false,
    },
    serialConnectionId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'SerialConnection',
        required: false,
    },
    gatewayId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Gateway',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=device.schema.js.map