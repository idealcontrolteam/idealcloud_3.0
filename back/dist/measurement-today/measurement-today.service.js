"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const tag_service_1 = require("../tag/tag.service");
const measurement_today_control_service_1 = require("../measurement-today-control/measurement-today-control.service");
const moment = require("moment");
let MeasurementTodayService = class MeasurementTodayService {
    constructor(measurementModel, tagService, measurementControlService) {
        this.measurementModel = measurementModel;
        this.tagService = tagService;
        this.measurementControlService = measurementControlService;
    }
    getMeasurements() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find();
            return measurements;
        });
    }
    getMeasurementsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel
                .find()
                .populate('tagId sensorId locationId');
            return measurements;
        });
    }
    getMeasurementByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            }).sort({ dateTime: 1 });
            return measurements;
        });
    }
    getEfectividadesToday(tags) {
        return __awaiter(this, void 0, void 0, function* () {
            let now = new Date();
            let f1 = moment(now).subtract(1, 'days').format('YYYY-MM-DDT12:00:00') + ".000Z";
            let f2 = moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
            let minutos_dia = moment(f1).diff(moment(f2), 'minutes') * -1;
            let registros_100 = minutos_dia / 5;
            let ids_tag = [];
            let _id_tag = [];
            let ids_location = [];
            let _id_location = [];
            tags.map(t => {
                ids_tag.push({ tagId: t });
                _id_tag.push({ _id: t });
            });
            const find_tags = yield this.tagService.getAnyTags(_id_tag);
            let new_measurements = [];
            let measurements = yield this.measurementModel.find({
                $or: ids_tag,
                dateTime: { $gte: f1, $lte: f2 },
                value: { $ne: 0 },
            }).sort({ dateTime: 1 });
            let control = false;
            if (measurements.length == 0) {
                let measurement_control = [];
                control = true;
                find_tags.map(t => {
                    ids_location.push({ id_dispositivos: t.locationId });
                    _id_location.push({ _id: t.locationId });
                });
                measurement_control = yield this.measurementControlService.getMeasurementEfectivity({
                    $or: ids_location,
                    fecha_registros: { $gte: f1, $lte: f2 },
                    oxd: { $ne: 0 },
                });
                ids_location.map(l => {
                    let med = [], medicion = 0;
                    med = measurement_control.filter(m => m.id_dispositivos == l.id_dispositivos);
                    medicion = measurement_control.filter(m => m.id_dispositivos == l.id_dispositivos).length;
                    let tag = find_tags.filter(m => m.locationId == l.id_dispositivos)[0];
                    medicion > 0 ?
                        new_measurements.push({
                            my_register: medicion,
                            register_100: registros_100,
                            porcentaje_efectividad: medicion * 100 / registros_100,
                            _id: l.id_dispositivos,
                            locationId: med[0].id_dispositivos != undefined ? med[0].id_dispositivos : "",
                            workplaceId: tag.workplaceId
                        }) : new_measurements.push({
                        my_register: medicion,
                        register_100: registros_100,
                        porcentaje_efectividad: medicion * 100 / registros_100,
                        _id: l.id_dispositivos,
                        workplaceId: tag.workplaceId,
                    });
                });
            }
            else {
                tags.map(t => {
                    let med = [], medicion = 0;
                    if (control) {
                        med = measurements.filter(m => m.tagId == t);
                        medicion = measurements.filter(m => m.tagId == t).length;
                    }
                    else {
                        med = measurements.filter(m => m.tagId == t);
                        medicion = measurements.filter(m => m.tagId == t).length;
                    }
                    let tag = find_tags.filter(m => m._id == t)[0];
                    medicion > 0 ?
                        new_measurements.push({
                            my_register: medicion,
                            register_100: registros_100,
                            porcentaje_efectividad: medicion * 100 / registros_100,
                            _id: t,
                            locationId: med[0].locationId != undefined ? med[0].locationId : "",
                            workplaceId: tag.workplaceId
                        }) : new_measurements.push({
                        my_register: medicion,
                        register_100: registros_100,
                        porcentaje_efectividad: medicion * 100 / registros_100,
                        _id: t,
                        workplaceId: tag.workplaceId,
                    });
                });
            }
            return new_measurements;
        });
    }
    getEfectividadToday(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            let now = new Date();
            let f1 = moment(now).format('YYYY-MM-DDT00:00:00') + ".000Z";
            let f2 = moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
            let minutos_dia = moment(f1).diff(moment(f2), 'minutes') * -1;
            let registros_100 = minutos_dia / 5;
            const measurements = yield this.measurementModel.countDocuments({
                tagId: tagId,
                dateTime: { $gte: f1, $lte: f2 },
                value: { $ne: 0 },
            }).sort({ dateTime: 1 });
            return {
                my_register: measurements,
                register_100: registros_100,
                porcentaje_efectividad: measurements * 100 / registros_100,
                _id: tagId
            };
        });
    }
    getMeasurementByTagfilteredXY(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            }).sort({ dateTime: 1 });
            let xy = [];
            measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementByTagfilteredXYActive(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                active: true,
                dateTime: { $gte: fini, $lte: ffin },
            }).sort({ dateTime: 1 });
            let xy = [];
            measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementFilteredByTagsAndDate(tags, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: { $in: tags },
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfilteredXY(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin },
            }).sort({ dateTime: 1 });
            let xy = [];
            measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.measurementModel.findById(id);
            return measurement;
        });
    }
    getMeasurementsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
            });
            return measurements;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                sensorId: sensorId,
            });
            return measurements;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
            });
            return measurements;
        });
    }
    createMeasurement(createMeasurementDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurement = new this.measurementModel(createMeasurementDTO);
            return yield newMeasurement.save();
        });
    }
    deleteMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementModel.findByIdAndDelete(id);
        });
    }
    updateMeasurement(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
MeasurementTodayService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('MeasurementToday')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        tag_service_1.TagService,
        measurement_today_control_service_1.MeasurementTodayControlService])
], MeasurementTodayService);
exports.MeasurementTodayService = MeasurementTodayService;
//# sourceMappingURL=measurement-today.service.js.map