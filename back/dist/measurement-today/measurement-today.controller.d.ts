import { CreateMeasurementTodayDTO } from './dto/measurement.dto';
import { MeasurementTodayService } from './measurement-today.service';
export declare class MeasurementTodayController {
    private measurementService;
    constructor(measurementService: MeasurementTodayService);
    validateIds: (body: any) => void;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    createMeasurement(res: any, body: CreateMeasurementTodayDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getEfectividadesToday(res: any, body: any): Promise<any>;
    getEfectividadToday(res: any, tagId: any): Promise<any>;
    getMeasurementByTagfilteredXY(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByTagfilteredXYActive(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfilteredXY(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateMeasurementTodayDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
