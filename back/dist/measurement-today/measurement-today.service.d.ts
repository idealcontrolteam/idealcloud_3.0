import { Model } from 'mongoose';
import { MeasurementToday } from './interfaces/measurement.interface';
import { TagService } from '../tag/tag.service';
import { MeasurementTodayControlService } from '../measurement-today-control/measurement-today-control.service';
import { CreateMeasurementTodayDTO } from './dto/measurement.dto';
export declare class MeasurementTodayService {
    private measurementModel;
    private tagService;
    private measurementControlService;
    constructor(measurementModel: Model<MeasurementToday>, tagService: TagService, measurementControlService: MeasurementTodayControlService);
    getMeasurements(): Promise<MeasurementToday[]>;
    getMeasurementsAll(): Promise<MeasurementToday[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<MeasurementToday[]>;
    getEfectividadesToday(tags: any): Promise<any>;
    getEfectividadToday(tagId: any): Promise<any>;
    getMeasurementByTagfilteredXY(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementByTagfilteredXYActive(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementFilteredByTagsAndDate(tags: any, fini: any, ffin: any): Promise<MeasurementToday[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<MeasurementToday[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<MeasurementToday[]>;
    getMeasurementByLocationfilteredXY(locationId: any, fini: any, ffin: any): Promise<MeasurementToday[]>;
    getMeasurement(id: any): Promise<MeasurementToday>;
    getMeasurementsByLocationId(locationId: any): Promise<MeasurementToday[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<MeasurementToday[]>;
    getMeasurementsByTagId(tagId: any): Promise<MeasurementToday[]>;
    createMeasurement(createMeasurementDTO: CreateMeasurementTodayDTO): Promise<MeasurementToday>;
    deleteMeasurement(id: any): Promise<MeasurementToday>;
    updateMeasurement(id: string, body: CreateMeasurementTodayDTO): Promise<MeasurementToday>;
}
