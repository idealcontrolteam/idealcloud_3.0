"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_today_controller_1 = require("./measurement-today.controller");
const measurement_today_service_1 = require("./measurement-today.service");
const mongoose_1 = require("@nestjs/mongoose");
const measurement_schema_1 = require("./schemas/measurement.schema");
const tag_service_1 = require("../tag/tag.service");
const tag_module_1 = require("../tag/tag.module");
const measurement_today_control_service_1 = require("../measurement-today-control/measurement-today-control.service");
const measurement_today_control_module_1 = require("../measurement-today-control/measurement-today-control.module");
let MeasurementTodayModule = class MeasurementTodayModule {
};
MeasurementTodayModule = __decorate([
    common_1.Module({
        imports: [
            tag_module_1.TagModule,
            measurement_today_control_module_1.MeasurementTodayControlModule,
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'MeasurementToday',
                    schema: measurement_schema_1.MeasurementTodaySchema,
                    collection: 'measurementToday',
                },
            ]),
        ],
        controllers: [measurement_today_controller_1.MeasurementTodayController],
        providers: [measurement_today_service_1.MeasurementTodayService,
            tag_service_1.TagService, measurement_today_control_service_1.MeasurementTodayControlService
        ],
        exports: [measurement_today_service_1.MeasurementTodayService, tag_service_1.TagService, measurement_today_control_module_1.MeasurementTodayControlModule],
    })
], MeasurementTodayModule);
exports.MeasurementTodayModule = MeasurementTodayModule;
//# sourceMappingURL=measurement-today.module.js.map