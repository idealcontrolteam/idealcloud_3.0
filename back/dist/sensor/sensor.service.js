"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const measurement_service_1 = require("../measurement/measurement.service");
const tag_service_1 = require("../tag/tag.service");
let SensorService = class SensorService {
    constructor(sensorModel, measurementService, tagService) {
        this.sensorModel = sensorModel;
        this.measurementService = measurementService;
        this.tagService = tagService;
    }
    getSensors() {
        return __awaiter(this, void 0, void 0, function* () {
            const sensors = yield this.sensorModel.find();
            return sensors;
        });
    }
    getSensorsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const sensors = yield this.sensorModel.find().populate('locationId');
            return sensors;
        });
    }
    getSensorsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const sensors = yield this.sensorModel.find({ locationId: locationId });
            return sensors;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.measurementService.getMeasurementsBySensorId(sensorId);
            return measurement;
        });
    }
    getTagsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagService.getTagsBySensorId(sensorId);
            return tags;
        });
    }
    getSensor(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const sensor = yield this.sensorModel.findById(id);
            return sensor;
        });
    }
    createSensor(createSensorDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newSensor = new this.sensorModel(createSensorDTO);
            return yield newSensor.save();
        });
    }
    deleteSensor(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.sensorModel.findByIdAndDelete(id);
        });
    }
    updateSensor(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.sensorModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
SensorService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Sensor')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        measurement_service_1.MeasurementService,
        tag_service_1.TagService])
], SensorService);
exports.SensorService = SensorService;
//# sourceMappingURL=sensor.service.js.map