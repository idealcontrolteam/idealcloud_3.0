"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const sensor_controller_1 = require("./sensor.controller");
const sensor_service_1 = require("./sensor.service");
const mongoose_1 = require("@nestjs/mongoose");
const sensor_schema_1 = require("./schemas/sensor.schema");
const measurement_module_1 = require("../measurement/measurement.module");
const measurement_service_1 = require("../measurement/measurement.service");
const tag_module_1 = require("../tag/tag.module");
const tag_service_1 = require("../tag/tag.service");
let SensorModule = class SensorModule {
};
SensorModule = __decorate([
    common_1.Module({
        imports: [
            measurement_module_1.MeasurementModule,
            tag_module_1.TagModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'Sensor', schema: sensor_schema_1.SensorSchema, collection: 'sensor' },
            ]),
        ],
        controllers: [sensor_controller_1.SensorController],
        providers: [sensor_service_1.SensorService, measurement_service_1.MeasurementService, tag_service_1.TagService],
        exports: [sensor_service_1.SensorService],
    })
], SensorModule);
exports.SensorModule = SensorModule;
//# sourceMappingURL=sensor.module.js.map