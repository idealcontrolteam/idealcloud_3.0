import { Document } from 'mongoose';
export interface Measurement extends Document {
    value: number;
    dateTime: Date;
    tagId: string;
    sensorId?: string;
    locationId: string;
    active: boolean;
    type: string;
}
