import { Model } from 'mongoose';
import { Measurement } from './interfaces/measurement.interface';
import { CreateMeasurementDTO } from './dto/measurement.dto';
export declare class MeasurementControlService {
    private measurementModel;
    constructor(measurementModel: Model<Measurement>);
    getMeasurements(): Promise<Measurement[]>;
    getMeasurementsAll(): Promise<Measurement[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getAlarmMeasurementByTagfiltered(tagId: any, type: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementByTagfilteredXY(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementByTagfilteredXYActive(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementFilteredByTagsAndDate(tags: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementByLocationallfiltered(locationId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementByLocationfilteredXY(locationId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurement(id: any): Promise<Measurement>;
    getMeasurementsByLocationId(locationId: any): Promise<Measurement[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<Measurement[]>;
    getMeasurementsByTagId(tagId: any): Promise<Measurement[]>;
    createMeasurement(createMeasurementDTO: CreateMeasurementDTO): Promise<Measurement>;
    deleteMeasurement(id: any): Promise<Measurement>;
    updateMeasurement(id: string, body: CreateMeasurementDTO): Promise<Measurement>;
}
