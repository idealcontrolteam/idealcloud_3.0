import { CreateMeasurementDTO } from './dto/measurement.dto';
import { MeasurementControlService } from './measurement-control.service';
import { Connection } from 'mongoose';
export declare class MeasurementControlController {
    private measurementService;
    private connection;
    constructor(measurementService: MeasurementControlService, connection: Connection);
    validateIds: (body: any) => void;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    createMeasurement(res: any, body: CreateMeasurementDTO): Promise<any>;
    getAlarmMeasurementByTagfiltered(res: any, body: any, fini: any, ffin: any): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByTagfilteredXY(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByTagfilteredXYActive(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocation_allfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfilteredXY(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateMeasurementDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
