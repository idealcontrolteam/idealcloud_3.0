"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const TypesAlarm_1 = require("../hooks/TypesAlarm");
let MeasurementControlService = class MeasurementControlService {
    constructor(measurementModel) {
        this.measurementModel = measurementModel;
    }
    getMeasurements() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find();
            return measurements;
        });
    }
    getMeasurementsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel
                .find()
                .populate('tagId sensorId locationId');
            return measurements;
        });
    }
    getMeasurementByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lte: ffin.replace('.000Z', '') },
            }).sort({ dateTime: 1 });
            let fechahoy = new Date();
            return measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() - zona_horaria);
                item.dateTime = fecha;
                return item;
            });
        });
    }
    getAlarmMeasurementByTagfiltered(tagId, type, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const hours = 3;
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lte: ffin.replace('.000Z', '') },
            }).sort({ dateTime: 1 });
            if (type == 'oxd') {
                return [{
                        status: TypesAlarm_1.searchAlarm(measurements, 'en cero', hours),
                        locationId: measurements.length > 0 ? measurements[0].locationId : ''
                    }];
            }
            else if (type == 'oxs') {
                return [{
                        status: TypesAlarm_1.searchAlarm(measurements, 'pegados', hours),
                        locationId: measurements.length > 0 ? measurements[0].locationId : ''
                    }];
            }
            else if (type == 'sal') {
                if (TypesAlarm_1.searchAlarm(measurements, 'salinidad 33', hours) != 'ok') {
                    return [{
                            status: TypesAlarm_1.searchAlarm(measurements, 'salinidad 33', hours),
                            locationId: measurements.length > 0 ? measurements[0].locationId : ''
                        }];
                }
                else if (TypesAlarm_1.searchAlarm(measurements, 'salinidad 34', hours) != 'ok') {
                    return [{
                            status: TypesAlarm_1.searchAlarm(measurements, 'salinidad 34', hours),
                            locationId: measurements.length > 0 ? measurements[0].locationId : ''
                        }];
                }
                else if (TypesAlarm_1.searchAlarm(measurements, 'en cero', hours) != 'ok') {
                    return [{
                            status: TypesAlarm_1.searchAlarm(measurements, 'en cero', hours),
                            locationId: measurements.length > 0 ? measurements[0].locationId : ''
                        }];
                }
                else {
                    return [{
                            status: TypesAlarm_1.searchAlarm(measurements, 'en cero', hours),
                            locationId: measurements.length > 0 ? measurements[0].locationId : ''
                        }];
                }
            }
        });
    }
    getMeasurementByTagfilteredXY(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lte: ffin.replace('.000Z', '') },
            }).sort({ dateTime: 1 });
            let xy = [];
            measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours());
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementByTagfilteredXYActive(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
                active: true,
                dateTime: { $gte: fini.replace('.000Z', ''), $lte: ffin.replace('.000Z', '') },
            }).sort({ dateTime: 1 });
            let xy = [];
            measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours());
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurementFilteredByTagsAndDate(tags, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: { $in: tags },
                dateTime: { $gte: fini.replace('.000Z', ''), $lte: ffin.replace('.000Z', '') },
            });
            return measurements;
        });
    }
    getMeasurementBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lte: ffin.replace('.000Z', '') },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lt: ffin.replace('.000Z', '') }
            });
            return measurements;
        });
    }
    getMeasurementByLocationallfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lt: ffin.replace('.000Z', '') },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfilteredXY(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
                dateTime: { $gte: fini.replace('.000Z', ''), $lt: ffin.replace('.000Z', '') },
            }).sort({ dateTime: 1 });
            let xy = [];
            measurements.map(item => {
                var fecha = new Date(item.dateTime);
                var zona_horaria = new Date(item.dateTime).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                fecha.setHours(fecha.getHours() + zona_horaria);
                xy.push({ x: fecha.getTime(), y: item.value });
            });
            return xy;
        });
    }
    getMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.measurementModel.findById(id);
            return measurement;
        });
    }
    getMeasurementsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                locationId: locationId,
            });
            return measurements;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                sensorId: sensorId,
            });
            return measurements;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementModel.find({
                tagId: tagId,
            });
            return measurements;
        });
    }
    createMeasurement(createMeasurementDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurement = new this.measurementModel(createMeasurementDTO);
            return yield newMeasurement.save();
        });
    }
    deleteMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementModel.findByIdAndDelete(id);
        });
    }
    updateMeasurement(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
MeasurementControlService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('MeasurementControl')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], MeasurementControlService);
exports.MeasurementControlService = MeasurementControlService;
//# sourceMappingURL=measurement-control.service.js.map