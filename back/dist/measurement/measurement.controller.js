"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_dto_1 = require("./dto/measurement.dto");
const measurement_service_1 = require("./measurement.service");
const measurement_hour_service_1 = require("../measurement-hour/measurement-hour.service");
const moment = require("moment");
const tag_service_1 = require("../tag/tag.service");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let MeasurementController = class MeasurementController {
    constructor(measurementService, tagService, measurementHourService, connection) {
        this.measurementService = measurementService;
        this.tagService = tagService;
        this.measurementHourService = measurementHourService;
        this.connection = connection;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key != 'value' && key != 'dateTime' && key != 'active') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
        this.diferenciaDias = (finiDate, ffinDate) => {
            return ffinDate - finiDate;
        };
    }
    createMeasurementMultiple(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.connection.readyState === 0) {
                throw new common_1.BadRequestException('Sin Conexión');
            }
            let newMeasurement = {};
            body.map((Measurement) => __awaiter(this, void 0, void 0, function* () {
                newMeasurement = yield this.measurementService.createMeasurement(Measurement);
            }));
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    createMeasurement(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newMeasurement = yield this.measurementService.createMeasurement(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    getAlarmMeasurementByTagfiltered(res, body, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let alarma = [];
                let status_alarma = '';
                if (body.tagId.length == 1) {
                    alarma = yield this.measurementService.getAlarmMeasurementByTagfiltered(body.tagId[0], body.type, fini, ffin);
                    status_alarma = alarma[0].status;
                }
                else {
                    let alarma2 = [];
                    alarma = yield this.measurementService.getAlarmMeasurementByTagfiltered(body.tagId[0], body.type, fini, ffin);
                    alarma2 = yield this.measurementService.getAlarmMeasurementByTagfiltered(body.tagId[1], body.type, fini, ffin);
                    if (alarma[0].status != "sin datos" || alarma2[0].status != "sin datos") {
                        status_alarma = [alarma[0].status, alarma2[0].status].includes('pegados') ? 'pegados' : 'ok';
                    }
                    else {
                        status_alarma = 'sin datos';
                    }
                }
                let msg = alarma.length == 0
                    ? 'Measurements not found'
                    : 'Measurements fetched';
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: msg,
                    data: status_alarma,
                });
            }
            catch (err) {
                throw new common_1.BadRequestException('Ops! ocurrio un error');
            }
        });
    }
    getMeasurements(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurements();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementsAll();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            const measurement = yield this.measurementService.getMeasurement(measurementId);
            if (!measurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement found',
                data: measurement,
            });
        });
    }
    getMeasurementByTagfiltered(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByTagfiltered(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByTagfilteredXY(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            let measurements = [];
            measurements = yield this.measurementService.getMeasurementByTagfilteredXY(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            let prom = "";
            let max = "";
            let min = "";
            let ultimo_valor = "";
            let ultima_fecha = "";
            if (measurements.length != 0) {
                prom = ((measurements.reduce((a, b) => +a + +b.y, 0) / measurements.length)).toFixed(1);
                max = measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
                min = measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
                ultimo_valor = measurements[measurements.length - 1].y;
                ultima_fecha = moment(measurements[measurements.length - 1].x).format('YYYY-MM-DD HH:mm:ss');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
                prom: prom,
                max: max,
                min: min,
                ultimo_valor: ultimo_valor,
                ultima_fecha: ultima_fecha,
            });
        });
    }
    getMeasurementByTagfilteredXYActive(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByTagfilteredXYActive(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            let prom = "";
            let max = "";
            let min = "";
            let ultimo_valor = "";
            let ultima_fecha = "";
            if (measurements.length != 0) {
                prom = ((measurements.reduce((a, b) => +a + +b.y, 0) / measurements.length)).toFixed(1);
                max = measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
                min = measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
                ultimo_valor = measurements[measurements.length - 1].y;
                ultima_fecha = moment(measurements[measurements.length - 1].x).format('YYYY-MM-DD HH:mm:ss');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
                prom: prom,
                max: max,
                min: min,
                ultimo_valor: ultimo_valor,
                ultima_fecha: ultima_fecha,
            });
        });
    }
    getMeasurementBySensorfiltered(res, sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementBySensorfiltered(sensorId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationfiltered(locationId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationsfiltered(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementByLocationsfiltered(body.locationsId, body.fini, body.ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocation_allfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationallfiltered(locationId, fini, ffin);
            let oxd = measurements.filter(m => m.type == "oxd");
            let oxs = measurements.filter(m => m.type == "oxs");
            let temp = measurements.filter(m => m.type == "temp");
            let sal = measurements.filter(m => m.type == "sal");
            let data = sal.map((o, i) => {
                return {
                    "oxd": oxd != null ? oxd[i].value : [],
                    "oxs": oxs != null ? oxs[i].value : [],
                    "temp": temp != null ? temp[i].value : [],
                    "sal": sal != null ? sal[i].value : [],
                    "locationId": o.locationId,
                    "dateTime": o.dateTime
                };
            });
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: data,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationfilteredXY(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationfilteredXY(locationId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    updateMeasurement(res, body, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedMeasurement = yield this.measurementService.updateMeasurement(measurementId, body);
            if (!updatedMeasurement) {
                throw new common_1.NotFoundException('Measurement not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement updated',
                data: updatedMeasurement,
            });
        });
    }
    deleteMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedMeasurement = yield this.measurementService.deleteMeasurement(measurementId);
            if (!deletedMeasurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement deleted',
                data: deletedMeasurement,
            });
        });
    }
};
__decorate([
    common_1.Post('/multiple'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "createMeasurementMultiple", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, measurement_dto_1.CreateMeasurementDTO]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "createMeasurement", null);
__decorate([
    common_1.Post('/alarm/tag/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getAlarmMeasurementByTagfiltered", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurements", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementsAll", null);
__decorate([
    common_1.Get('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurement", null);
__decorate([
    common_1.Get('/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByTagfiltered", null);
__decorate([
    common_1.Get('/xy/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByTagfilteredXY", null);
__decorate([
    common_1.Get('/xy/tag/active/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByTagfilteredXYActive", null);
__decorate([
    common_1.Get('/sensor/:sensorId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('sensorId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementBySensorfiltered", null);
__decorate([
    common_1.Get('/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByLocationfiltered", null);
__decorate([
    common_1.Post('/locations_array'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByLocationsfiltered", null);
__decorate([
    common_1.Get('/location_all/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByLocation_allfiltered", null);
__decorate([
    common_1.Get('/xy/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurementByLocationfilteredXY", null);
__decorate([
    common_1.Put('/:measurementId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, measurement_dto_1.CreateMeasurementDTO, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "updateMeasurement", null);
__decorate([
    common_1.Delete('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "deleteMeasurement", null);
MeasurementController = __decorate([
    common_1.Controller('measurement'),
    __param(3, mongoose_1.InjectConnection()),
    __metadata("design:paramtypes", [measurement_service_1.MeasurementService,
        tag_service_1.TagService,
        measurement_hour_service_1.MeasurementHourService,
        mongoose_2.Connection])
], MeasurementController);
exports.MeasurementController = MeasurementController;
//# sourceMappingURL=measurement.controller.js.map