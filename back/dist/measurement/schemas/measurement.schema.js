"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.MeasurementSchema = new mongoose_1.Schema({
    value: Number,
    dateTime: Date,
    tagId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Tag',
        required: true,
    },
    sensorId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Sensor',
        required: false,
    },
    locationId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    active: Boolean,
    type: String,
}, { versionKey: false });
//# sourceMappingURL=measurement.schema.js.map