"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_controller_1 = require("./measurement.controller");
const measurement_service_1 = require("./measurement.service");
const mongoose_1 = require("@nestjs/mongoose");
const measurement_hour_module_1 = require("../measurement-hour/measurement-hour.module");
const measurement_schema_1 = require("./schemas/measurement.schema");
const tag_module_1 = require("../tag/tag.module");
let MeasurementModule = class MeasurementModule {
};
MeasurementModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'Measurement',
                    schema: measurement_schema_1.MeasurementSchema,
                    collection: 'measurement',
                },
            ]),
            common_1.forwardRef(() => tag_module_1.TagModule),
            common_1.forwardRef(() => measurement_hour_module_1.MeasurementHourModule)
        ],
        controllers: [measurement_controller_1.MeasurementController],
        providers: [measurement_service_1.MeasurementService,
        ],
        exports: [measurement_service_1.MeasurementService],
    })
], MeasurementModule);
exports.MeasurementModule = MeasurementModule;
//# sourceMappingURL=measurement.module.js.map