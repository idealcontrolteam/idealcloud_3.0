/// <reference types="mongodb" />
import { Model } from 'mongoose';
import { Gateway } from './interfaces/gateway.interface';
import { CreateGatewayDTO } from './dto/gateway.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';
export declare class GatewayService {
    private gatewayModel;
    private deviceService;
    constructor(gatewayModel: Model<Gateway>, deviceService: DeviceService);
    getGateways(): Promise<Gateway[]>;
    getGateway(id: any): Promise<Gateway>;
    getGatewaysAll(): Promise<Gateway[]>;
    getGateWaysByWorkPlaceId(workPlaceId: any): Promise<Gateway[]>;
    createGateway(createGatewayDTO: CreateGatewayDTO): Promise<Gateway>;
    getDevicesByGatewayId(gatewayId: any): Promise<Device[]>;
    deleteGateway(id: any): Promise<Gateway>;
    deleteGatewaysByWorkPlaceId(workPlaceId: any): Promise<{
        ok?: number;
        n?: number;
    }>;
    updateGateway(id: string, body: CreateGatewayDTO): Promise<Gateway>;
}
