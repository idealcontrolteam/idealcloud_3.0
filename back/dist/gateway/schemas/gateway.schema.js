"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.GatewaySchema = new mongoose_1.Schema({
    name: String,
    description: String,
    active: Boolean,
    workPlaceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'WorkPlace',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=gateway.schema.js.map