"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const gateway_dto_1 = require("./dto/gateway.dto");
const gateway_service_1 = require("./gateway.service");
let GatewayController = class GatewayController {
    constructor(gatewayService) {
        this.gatewayService = gatewayService;
    }
    createGateway(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            if (body.workPlaceId !== undefined &&
                !body.workPlaceId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Work place id is not a valid ObjectId');
            }
            const newGateway = yield this.gatewayService.createGateway(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Gateway created successfully',
                data: newGateway,
            });
        });
    }
    getGateways(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const gateways = yield this.gatewayService.getGateways();
            let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: gateways,
                count: gateways.length,
            });
        });
    }
    getGatewaysAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const gateways = yield this.gatewayService.getGatewaysAll();
            let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: gateways,
                count: gateways.length,
            });
        });
    }
    getDevicesByGatewayId(res, gatewayId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.gatewayService.getDevicesByGatewayId(gatewayId);
            let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: devices,
                count: devices.length,
            });
        });
    }
    getGateway(res, gatewayId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!gatewayId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Gateway id is not a valid ObjectId');
            }
            const gateway = yield this.gatewayService.getGateway(gatewayId);
            if (!gateway) {
                throw new common_1.NotFoundException('Gateway not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Gateway found',
                data: gateway,
            });
        });
    }
    updateGateway(res, body, gatewayId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!gatewayId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Gateway id is not a valid ObjectId');
            }
            if (body.workPlaceId !== undefined &&
                !body.workPlaceId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Work place id is not a valid ObjectId');
            }
            const updatedGateway = yield this.gatewayService.updateGateway(gatewayId, body);
            if (!updatedGateway) {
                throw new common_1.NotFoundException('Gateway not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Gateway updated',
                data: updatedGateway,
            });
        });
    }
    deleteGateway(res, gatewayId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedGateway = yield this.gatewayService.deleteGateway(gatewayId);
            if (!deletedGateway) {
                throw new common_1.NotFoundException('Gateway not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Gateway deleted',
                data: deletedGateway,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, gateway_dto_1.CreateGatewayDTO]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "createGateway", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "getGateways", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "getGatewaysAll", null);
__decorate([
    common_1.Get(':gatewayId/device'),
    __param(0, common_1.Res()), __param(1, common_1.Param('gatewayId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "getDevicesByGatewayId", null);
__decorate([
    common_1.Get('/:gatewayId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('gatewayId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "getGateway", null);
__decorate([
    common_1.Put('/:gatewayId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('gatewayId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, gateway_dto_1.CreateGatewayDTO, Object]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "updateGateway", null);
__decorate([
    common_1.Delete('/:gatewayId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('gatewayId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], GatewayController.prototype, "deleteGateway", null);
GatewayController = __decorate([
    common_1.Controller('gateway'),
    __metadata("design:paramtypes", [gateway_service_1.GatewayService])
], GatewayController);
exports.GatewayController = GatewayController;
//# sourceMappingURL=gateway.controller.js.map