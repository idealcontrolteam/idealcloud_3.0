import { Model } from 'mongoose';
import { CreateAlarmDTO } from './dto/alarm.dto';
import { Alarm } from './interfaces/alarm.interface';
export declare class AlarmService {
    private alarmModel;
    constructor(alarmModel: Model<Alarm>);
    getAlarms(): Promise<Alarm[]>;
    getAlarmsAll(): Promise<Alarm[]>;
    getAlarmsByTagId(tagId: any): Promise<Alarm[]>;
    getAlarmActive(filtro: any): Promise<any>;
    getAlarm(id: any): Promise<Alarm>;
    getCodeAlarm(id: any): Promise<any>;
    createAlarm(createAlarmDTO: CreateAlarmDTO): Promise<Alarm>;
    deleteAlarm(id: any): Promise<Alarm>;
    updateAlarm(id: string, body: CreateAlarmDTO): Promise<Alarm>;
    getAlarmByfilteredDate(fini: any, ffin: any): Promise<Alarm[]>;
    getAlarmByfilteredDateActive(fini: any, ffin: any): Promise<Alarm[]>;
}
