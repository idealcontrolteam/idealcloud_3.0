"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.AlarmSchema = new mongoose_1.Schema({
    code: String,
    endDate: Date,
    dateTimeIni: Date,
    dateTimeTer: Date,
    dateTimeSoport: Date,
    detail: String,
    company: String,
    workplaceId: String,
    locationId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    active: Boolean,
    alarmValue: Number,
    sentEmail: Boolean,
    status: String,
    type: String,
    divice: String,
}, { versionKey: false });
//# sourceMappingURL=alarm.schema.js.map