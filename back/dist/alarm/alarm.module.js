"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const alarm_controller_1 = require("./alarm.controller");
const alarm_service_1 = require("./alarm.service");
const mongoose_1 = require("@nestjs/mongoose");
const alarm_schema_1 = require("./schemas/alarm.schema");
let AlarmModule = class AlarmModule {
};
AlarmModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'Alarm', schema: alarm_schema_1.AlarmSchema, collection: 'alarm' },
            ]),
        ],
        controllers: [alarm_controller_1.AlarmController],
        providers: [alarm_service_1.AlarmService],
        exports: [alarm_service_1.AlarmService],
    })
], AlarmModule);
exports.AlarmModule = AlarmModule;
//# sourceMappingURL=alarm.module.js.map