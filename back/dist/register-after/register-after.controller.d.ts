import { CreateRegisterAfterDTO } from './dto/register-after.dto';
import { RegisterAfterService } from './register-after.service';
export declare class RegisterAfterController {
    private measurementService;
    constructor(measurementService: RegisterAfterService);
    createMeasurement(res: any, body: CreateRegisterAfterDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, id_dispositivos: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateRegisterAfterDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
