export declare class CreateRegisterAfterDTO {
    fecha_registros: Date;
    code: string;
    id_dispositivos: string;
    oxd: number;
    oxs: number;
    temp: number;
    sal: number;
}
