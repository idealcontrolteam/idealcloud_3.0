"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let RegisterAfterService = class RegisterAfterService {
    constructor(measurementCopyModel) {
        this.measurementCopyModel = measurementCopyModel;
    }
    getMeasurements() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find();
            return measurements;
        });
    }
    getMeasurementsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel
                .find()
                .populate('tagId sensorId locationId');
            return measurements;
        });
    }
    getMeasurementByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfiltered(id_dispositivos, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            let measurements = yield this.measurementCopyModel.find({
                id_dispositivos: id_dispositivos,
                fecha_registros: { $gte: fini, $lt: ffin },
            }).lean();
            return measurements.map(d => {
                var fecha = new Date(d.fecha_registros);
                var zona_horaria = new Date(d.fecha_registros).getTimezoneOffset();
                zona_horaria = zona_horaria / 60;
                let f = fecha.setHours(fecha.getHours() + zona_horaria);
                return {
                    "_id": d._id,
                    "code": d.code,
                    "temp": d.temp,
                    "sal": d.sal,
                    "oxs": d.oxs,
                    "oxd": d.oxd,
                    "fecha_registros": f,
                    "id_dispositivos": d.id_dispositivos
                };
            });
        });
    }
    getMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.measurementCopyModel.findById(id);
            return measurement;
        });
    }
    getMeasurementsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                locationId: locationId,
            });
            return measurements;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                sensorId: sensorId,
            });
            return measurements;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                tagId: tagId,
            });
            return measurements;
        });
    }
    createMeasurement(createRegisterAfterDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurement = new this.measurementCopyModel(createRegisterAfterDTO);
            return yield newMeasurement.save();
        });
    }
    deleteMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementCopyModel.findByIdAndDelete(id);
        });
    }
    updateMeasurement(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementCopyModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
RegisterAfterService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('RegisterAfter')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], RegisterAfterService);
exports.RegisterAfterService = RegisterAfterService;
//# sourceMappingURL=register-after.service.js.map