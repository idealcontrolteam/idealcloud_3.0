"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const register_after_controller_1 = require("./register-after.controller");
const register_after_service_1 = require("./register-after.service");
const mongoose_1 = require("@nestjs/mongoose");
const register_after_schema_1 = require("./schemas/register-after.schema");
let RegisterAfterModule = class RegisterAfterModule {
};
RegisterAfterModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'RegisterAfter',
                    schema: register_after_schema_1.RegisterAfterSchema,
                    collection: 'registerAfter',
                },
            ]),
        ],
        controllers: [register_after_controller_1.RegisterAfterController],
        providers: [register_after_service_1.RegisterAfterService],
    })
], RegisterAfterModule);
exports.RegisterAfterModule = RegisterAfterModule;
//# sourceMappingURL=register-after.module.js.map