import { Model } from 'mongoose';
import { RegisterAfter } from './interfaces/register-after.interface';
import { CreateRegisterAfterDTO } from './dto/register-after.dto';
export declare class RegisterAfterService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<RegisterAfter>);
    getMeasurements(): Promise<RegisterAfter[]>;
    getMeasurementsAll(): Promise<RegisterAfter[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<RegisterAfter[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<RegisterAfter[]>;
    getMeasurementByLocationfiltered(id_dispositivos: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurement(id: any): Promise<RegisterAfter>;
    getMeasurementsByLocationId(locationId: any): Promise<RegisterAfter[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<RegisterAfter[]>;
    getMeasurementsByTagId(tagId: any): Promise<RegisterAfter[]>;
    createMeasurement(createRegisterAfterDTO: CreateRegisterAfterDTO): Promise<RegisterAfter>;
    deleteMeasurement(id: any): Promise<RegisterAfter>;
    updateMeasurement(id: string, body: CreateRegisterAfterDTO): Promise<RegisterAfter>;
}
