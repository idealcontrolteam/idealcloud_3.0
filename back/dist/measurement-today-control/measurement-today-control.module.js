"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_today_control_controller_1 = require("./measurement-today-control.controller");
const measurement_today_control_service_1 = require("./measurement-today-control.service");
const mongoose_1 = require("@nestjs/mongoose");
const register_today_schema_1 = require("./schemas/register-today.schema");
let MeasurementTodayControlModule = class MeasurementTodayControlModule {
};
MeasurementTodayControlModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'MeasurementTodayControl',
                    schema: register_today_schema_1.RegisterTodaySchema,
                    collection: 'measurementTodayControl',
                },
            ]),
        ],
        controllers: [measurement_today_control_controller_1.MeasurementTodayControlController],
        providers: [measurement_today_control_service_1.MeasurementTodayControlService],
    })
], MeasurementTodayControlModule);
exports.MeasurementTodayControlModule = MeasurementTodayControlModule;
//# sourceMappingURL=measurement-today-control.module.js.map