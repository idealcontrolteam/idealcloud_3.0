import { Model } from 'mongoose';
import { RegisterToday } from './interfaces/register-today.interface';
import { CreateRegisterTodayDTO } from './dto/register-today.dto';
export declare class MeasurementTodayControlService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<RegisterToday>);
    getMeasurements(): Promise<RegisterToday[]>;
    getMeasurementsAll(): Promise<RegisterToday[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<RegisterToday[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<RegisterToday[]>;
    getMeasurementByLocationfiltered(id_dispositivos: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurement(id: any): Promise<RegisterToday>;
    getMeasurementsByLocationId(locationId: any): Promise<RegisterToday[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<RegisterToday[]>;
    getMeasurementsByTagId(tagId: any): Promise<RegisterToday[]>;
    getMeasurementEfectivity(data: any): Promise<any[]>;
    createMeasurement(createRegisterTodayDTO: CreateRegisterTodayDTO): Promise<RegisterToday>;
    deleteMeasurement(id: any): Promise<RegisterToday>;
    updateMeasurement(id: string, body: CreateRegisterTodayDTO): Promise<RegisterToday>;
}
