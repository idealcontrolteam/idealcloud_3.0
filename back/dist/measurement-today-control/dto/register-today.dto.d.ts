export declare class CreateRegisterTodayDTO {
    fecha_registros: Date;
    code: string;
    id_dispositivos: string;
    oxd: number;
    oxs: number;
    temp: number;
    sal: number;
    bat: number;
    alarma_min: number;
}
