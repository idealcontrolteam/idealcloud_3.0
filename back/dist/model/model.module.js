"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const model_controller_1 = require("./model.controller");
const model_service_1 = require("./model.service");
const mongoose_1 = require("@nestjs/mongoose");
const model_schema_1 = require("./schemas/model.schema");
const device_module_1 = require("../device/device.module");
const device_service_1 = require("../device/device.service");
let ModelModule = class ModelModule {
};
ModelModule = __decorate([
    common_1.Module({
        imports: [
            device_module_1.DeviceModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'Model', schema: model_schema_1.ModelSchema, collection: 'model' },
            ]),
        ],
        controllers: [model_controller_1.ModelController],
        providers: [model_service_1.ModelService, device_service_1.DeviceService],
        exports: [model_service_1.ModelService],
    })
], ModelModule);
exports.ModelModule = ModelModule;
//# sourceMappingURL=model.module.js.map