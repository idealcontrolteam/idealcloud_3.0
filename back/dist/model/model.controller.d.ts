import { CreateModelDTO } from './dto/model.dto';
import { ModelService } from './model.service';
import { DeviceService } from '../device/device.service';
export declare class ModelController {
    private modelService;
    private deviceService;
    constructor(modelService: ModelService, deviceService: DeviceService);
    createModel(res: any, body: CreateModelDTO): Promise<any>;
    getModels(res: any): Promise<any>;
    getModelsAll(res: any): Promise<any>;
    getDevicesByModelId(res: any, modelid: any): Promise<any>;
    getModel(res: any, modelid: any): Promise<any>;
    updateModel(res: any, body: CreateModelDTO, modelid: any): Promise<any>;
    deleteModelsByBrandId(res: any, brandId: any): Promise<any>;
    deleteModel(res: any, modelid: any): Promise<any>;
}
