import { Document } from 'mongoose';
export interface Location extends Document {
    code?: string;
    name: string;
    active: boolean;
    zoneId: string;
    workPlaceId: string;
    falla?: boolean;
    fallaDate?: Date;
    status?: string;
    status_sal?: string;
}
