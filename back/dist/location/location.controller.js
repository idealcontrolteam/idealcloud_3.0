"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const location_dto_1 = require("./dto/location.dto");
const location_service_1 = require("./location.service");
let LocationController = class LocationController {
    constructor(locationService) {
        this.locationService = locationService;
    }
    createLocation(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newLocation = yield this.locationService.createLocation(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Location created successfully',
                data: newLocation,
            });
        });
    }
    getLocationIds(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newLocation = yield this.locationService.getLocationsIds(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Location created successfully',
                data: newLocation,
            });
        });
    }
    getLocations(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const locations = yield this.locationService.getLocations();
            let msg = locations.length == 0 ? 'Locations not found' : 'Locations fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: locations,
                count: locations.length,
            });
        });
    }
    getLocationsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const locations = yield this.locationService.getLocationsAll();
            let msg = locations.length == 0 ? 'Locations not found' : 'Locations fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: locations,
                count: locations.length,
            });
        });
    }
    getMesurementsByLocationId(res, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.locationService.getMesurementsByLocationId(locationId);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getSensorsByLocationId(res, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const sensors = yield this.locationService.getSensorsByLocationId(locationId);
            let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: sensors,
                count: sensors.length,
            });
        });
    }
    getTagsByLocationId(res, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const tags = yield this.locationService.getTagsByLocationId(locationId);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getLocation(res, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const location = yield this.locationService.getLocation(locationId);
            if (!location) {
                throw new common_1.NotFoundException('Location not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Location found',
                data: location,
            });
        });
    }
    updateGateway(res, body, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const updatedLocation = yield this.locationService.updateLocation(locationId, body);
            if (!updatedLocation) {
                throw new common_1.NotFoundException('Location not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Location updated',
                data: updatedLocation,
            });
        });
    }
    deleteLocation(res, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedLocation = yield this.locationService.deleteLocation(locationId);
            if (!deletedLocation) {
                throw new common_1.NotFoundException('Location not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Location deleted',
                data: deletedLocation,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, location_dto_1.CreateLcationDTO]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "createLocation", null);
__decorate([
    common_1.Post('/locations_ids'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getLocationIds", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getLocations", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getLocationsAll", null);
__decorate([
    common_1.Get('/:locationId/measurement'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getMesurementsByLocationId", null);
__decorate([
    common_1.Get('/:locationId/sensor'),
    __param(0, common_1.Res()), __param(1, common_1.Param('locationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getSensorsByLocationId", null);
__decorate([
    common_1.Get('/:locationId/tag'),
    __param(0, common_1.Res()), __param(1, common_1.Param('locationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getTagsByLocationId", null);
__decorate([
    common_1.Get('/:locationId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('locationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getLocation", null);
__decorate([
    common_1.Put('/:locationId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('locationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, location_dto_1.CreateLcationDTO, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "updateGateway", null);
__decorate([
    common_1.Delete('/:locationId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('locationId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "deleteLocation", null);
LocationController = __decorate([
    common_1.Controller('location'),
    __metadata("design:paramtypes", [location_service_1.LocationService])
], LocationController);
exports.LocationController = LocationController;
//# sourceMappingURL=location.controller.js.map