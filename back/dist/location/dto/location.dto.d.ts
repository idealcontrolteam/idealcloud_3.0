export declare class CreateLcationDTO {
    code?: string;
    name: string;
    active: boolean;
    zoneId: string;
    workPlaceId?: string;
    falla?: boolean;
    fallaDate?: Date;
    status?: String;
    status_sal?: String;
}
