"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.LocationSchema = new mongoose_1.Schema({
    code: String,
    name: String,
    active: Boolean,
    workPlaceId: String,
    falla: Boolean,
    fallaDate: Date,
    status: String,
    status_sal: String,
    zoneId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Zone',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=location.chema.js.map