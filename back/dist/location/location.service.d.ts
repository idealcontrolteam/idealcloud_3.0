/// <reference types="mongodb" />
import { Model } from 'mongoose';
import { Location } from './interfaces/location.interface';
import { CreateLcationDTO } from './dto/location.dto';
import { Measurement } from '../measurement/interfaces/measurement.interface';
import { MeasurementService } from '../measurement/measurement.service';
import { Sensor } from '../sensor/interfaces/sensor.interface';
import { SensorService } from '../sensor/sensor.service';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';
export declare class LocationService {
    private locationModel;
    private measurementService;
    private sensorService;
    private tagService;
    constructor(locationModel: Model<Location>, measurementService: MeasurementService, sensorService: SensorService, tagService: TagService);
    getLocations(): Promise<Location[]>;
    getLocationsAll(): Promise<Location[]>;
    getLocationsByZoneIdTrue(zoneId: any): Promise<Location[]>;
    getLocationsByZoneId(zoneId: any): Promise<Location[]>;
    getLocation(id: any): Promise<Location>;
    getTagsByLocationId(locationId: any): Promise<Tag[]>;
    getMesurementsByLocationId(locationId: any): Promise<Measurement[]>;
    getSensorsByLocationId(locationId: any): Promise<Sensor[]>;
    getLocationsIds(ids: any): Promise<any[]>;
    createLocation(createLcationDTO: CreateLcationDTO): Promise<Location>;
    deleteLocation(id: any): Promise<Location>;
    deleteLocationsByZoneId(zoneId: any): Promise<{
        ok?: number;
        n?: number;
    }>;
    updateLocation(id: string, body: any): Promise<Location>;
}
