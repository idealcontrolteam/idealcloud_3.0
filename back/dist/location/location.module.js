"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const location_controller_1 = require("./location.controller");
const location_service_1 = require("./location.service");
const mongoose_1 = require("@nestjs/mongoose");
const location_chema_1 = require("./schemas/location.chema");
const measurement_module_1 = require("../measurement/measurement.module");
const measurement_service_1 = require("../measurement/measurement.service");
const sensor_module_1 = require("../sensor/sensor.module");
const sensor_service_1 = require("../sensor/sensor.service");
const tag_module_1 = require("../tag/tag.module");
const tag_service_1 = require("../tag/tag.service");
let LocationModule = class LocationModule {
};
LocationModule = __decorate([
    common_1.Module({
        imports: [
            measurement_module_1.MeasurementModule,
            sensor_module_1.SensorModule,
            tag_module_1.TagModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'Location', schema: location_chema_1.LocationSchema, collection: 'location' },
            ]),
        ],
        controllers: [location_controller_1.LocationController],
        providers: [location_service_1.LocationService, measurement_service_1.MeasurementService, sensor_service_1.SensorService, tag_service_1.TagService],
        exports: [location_service_1.LocationService],
    })
], LocationModule);
exports.LocationModule = LocationModule;
//# sourceMappingURL=location.module.js.map