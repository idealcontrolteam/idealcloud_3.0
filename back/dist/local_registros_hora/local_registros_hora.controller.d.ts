import { CreateLocalRegistrosHoraDTO } from './dto/local_registros_hora.dto';
import { LocalRegistrosHoraService } from './local_registros_hora.service';
export declare class LocalRegistrosHoraController {
    private measurementService;
    constructor(measurementService: LocalRegistrosHoraService);
    createMeasurement(res: any, body: CreateLocalRegistrosHoraDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, id_dispositivos: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateLocalRegistrosHoraDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
