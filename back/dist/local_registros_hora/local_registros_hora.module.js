"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const local_registros_hora_controller_1 = require("./local_registros_hora.controller");
const local_registros_hora_service_1 = require("./local_registros_hora.service");
const mongoose_1 = require("@nestjs/mongoose");
const local_registros_hora_schema_1 = require("./schemas/local_registros_hora.schema");
let LocalRegistrosHoraModule = class LocalRegistrosHoraModule {
};
LocalRegistrosHoraModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'LocalRegistrosHora',
                    schema: local_registros_hora_schema_1.LocalRegistrosHoraSchema,
                    collection: 'local_registros_hora',
                },
            ]),
        ],
        controllers: [local_registros_hora_controller_1.LocalRegistrosHoraController],
        providers: [local_registros_hora_service_1.LocalRegistrosHoraService],
    })
], LocalRegistrosHoraModule);
exports.LocalRegistrosHoraModule = LocalRegistrosHoraModule;
//# sourceMappingURL=local_registros_hora.module.js.map