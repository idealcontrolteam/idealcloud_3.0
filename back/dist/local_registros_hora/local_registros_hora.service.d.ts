import { Model } from 'mongoose';
import { LocalRegistrosHora } from './interfaces/local_registros_hora.interface';
import { CreateLocalRegistrosHoraDTO } from './dto/local_registros_hora.dto';
export declare class LocalRegistrosHoraService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<LocalRegistrosHora>);
    getMeasurements(): Promise<LocalRegistrosHora[]>;
    getMeasurementsAll(): Promise<LocalRegistrosHora[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<LocalRegistrosHora[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<LocalRegistrosHora[]>;
    getMeasurementByLocationfiltered(id_dispositivos: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurement(id: any): Promise<LocalRegistrosHora>;
    getMeasurementsByLocationId(locationId: any): Promise<LocalRegistrosHora[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<LocalRegistrosHora[]>;
    getMeasurementsByTagId(tagId: any): Promise<LocalRegistrosHora[]>;
    createMeasurement(createLocalRegistrosHoraDTO: CreateLocalRegistrosHoraDTO): Promise<LocalRegistrosHora>;
    deleteMeasurement(id: any): Promise<LocalRegistrosHora>;
    updateMeasurement(id: string, body: CreateLocalRegistrosHoraDTO): Promise<LocalRegistrosHora>;
}
