"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.LocalRegistrosHoraSchema = new mongoose_1.Schema({
    fecha_registros: Date,
    code: String,
    id_dispositivos: String,
    oxd: Number,
    oxs: Number,
    temp: Number,
    sal: Number,
}, { versionKey: false });
//# sourceMappingURL=local_registros_hora.schema.js.map