import { Model } from 'mongoose';
import { Createemail_suportDTO } from './dto/email_suport.dto';
import { Email_Suport } from './interfaces/email_suport.interface';
export declare class EmailSuportService {
    private activityModel;
    constructor(activityModel: Model<Email_Suport>);
    getActivityAll(): Promise<any[]>;
    getActivity(id: any): Promise<Email_Suport>;
    createActivity(Createemail_suportDTO: Createemail_suportDTO): Promise<Email_Suport>;
    deleteActivity(id: any): Promise<Email_Suport>;
    updateActivity(id: string, body: Createemail_suportDTO): Promise<Email_Suport>;
}
