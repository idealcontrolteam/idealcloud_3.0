"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const email_suport_controller_1 = require("./email_suport.controller");
const email_suport_service_1 = require("./email_suport.service");
const mongoose_1 = require("@nestjs/mongoose");
const email_suport_schema_1 = require("./schemas/email_suport.schema");
let EmailSuportModule = class EmailSuportModule {
};
EmailSuportModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'EmailSuport', schema: email_suport_schema_1.email_suportSchema, collection: 'emailSuport' },
            ]),
        ],
        controllers: [email_suport_controller_1.EmailSuportController],
        providers: [email_suport_service_1.EmailSuportService]
    })
], EmailSuportModule);
exports.EmailSuportModule = EmailSuportModule;
//# sourceMappingURL=email_suport.module.js.map