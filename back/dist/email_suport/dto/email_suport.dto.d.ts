export declare class Createemail_suportDTO {
    message: String;
    name_workplace: String;
    workplaceId: String;
    dateTime: Date;
    active: Boolean;
    code: String;
}
