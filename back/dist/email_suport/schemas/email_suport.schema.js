"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.email_suportSchema = new mongoose_1.Schema({
    message: String,
    name_workplace: String,
    workplaceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'workPlace',
        required: true,
    },
    dateTime: Date,
    active: Boolean,
    code: String,
}, { versionKey: false });
//# sourceMappingURL=email_suport.schema.js.map