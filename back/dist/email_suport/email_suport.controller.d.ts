import { EmailSuportService } from './email_suport.service';
import { Createemail_suportDTO } from './dto/email_suport.dto';
export declare class EmailSuportController {
    private activityService;
    constructor(activityService: EmailSuportService);
    createActivity(res: any, body: Createemail_suportDTO): Promise<any>;
    getActivitysAll(res: any): Promise<any>;
    getActivity(res: any, activityId: any): Promise<any>;
    updateActivity(res: any, body: Createemail_suportDTO, activityId: any): Promise<any>;
    deleteActivity(res: any, activityId: any): Promise<any>;
}
