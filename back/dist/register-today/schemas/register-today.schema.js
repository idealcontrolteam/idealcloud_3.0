"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.RegisterTodaySchema = new mongoose_1.Schema({
    fecha_registros: Date,
    code: String,
    id_dispositivos: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    oxd: Number,
    oxs: Number,
    temp: Number,
    sal: Number,
}, { versionKey: false });
//# sourceMappingURL=register-today.schema.js.map