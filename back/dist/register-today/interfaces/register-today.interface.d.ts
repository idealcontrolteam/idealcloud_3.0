import { Document } from 'mongoose';
export interface RegisterToday extends Document {
    fecha_registros: Date;
    code: string;
    id_dispositivos: string;
    oxd: number;
    oxs: number;
    temp: number;
    sal: number;
}
