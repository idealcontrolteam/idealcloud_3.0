import { CreateRegisterTodayDTO } from './dto/register-today.dto';
import { RegisterTodayService } from './register-today.service';
export declare class RegisterTodayController {
    private measurementService;
    constructor(measurementService: RegisterTodayService);
    createMeasurement(res: any, body: CreateRegisterTodayDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, id_dispositivos: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateRegisterTodayDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
