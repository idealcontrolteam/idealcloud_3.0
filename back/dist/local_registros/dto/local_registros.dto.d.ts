export declare class CreateLocal_RegistrosDTO {
    id_dispositivos: String;
    fecha_registros: Date;
    oxd: Number;
    oxs: Number;
    temp: Number;
    sal: Number;
    code: String;
}
