"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const local_registros_controller_1 = require("./local_registros.controller");
const local_registros_service_1 = require("./local_registros.service");
const mongoose_1 = require("@nestjs/mongoose");
const local_registros_schema_1 = require("./schemas/local_registros.schema");
let LocalRegistrosModule = class LocalRegistrosModule {
};
LocalRegistrosModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'Local_Registros',
                    schema: local_registros_schema_1.Local_RegistrosSchema,
                    collection: 'local_registros_',
                },
            ]),
        ],
        controllers: [local_registros_controller_1.LocalRegistrosController],
        providers: [local_registros_service_1.LocalRegistrosService],
    })
], LocalRegistrosModule);
exports.LocalRegistrosModule = LocalRegistrosModule;
//# sourceMappingURL=local_registros.module.js.map