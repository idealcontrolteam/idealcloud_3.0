"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.Local_RegistrosSchema = new mongoose_1.Schema({
    id_dispositivos: String,
    fecha_registros: Date,
    oxd: Number,
    oxs: Number,
    temp: Number,
    sal: Number,
    code: String,
}, { versionKey: false });
//# sourceMappingURL=local_registros.schema.js.map