import { Model } from 'mongoose';
import { Local_Registros } from './interfaces/local_registros.interface';
import { CreateLocal_RegistrosDTO } from './dto/local_registros.dto';
export declare class LocalRegistrosService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<Local_Registros>);
    getMeasurements(): Promise<Local_Registros[]>;
    getMeasurementsAll(): Promise<Local_Registros[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<Local_Registros[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<Local_Registros[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<Local_Registros[]>;
    getMeasurement(id: any): Promise<Local_Registros>;
    getMeasurementsByLocationId(locationId: any): Promise<Local_Registros[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<Local_Registros[]>;
    getMeasurementsByTagId(tagId: any): Promise<Local_Registros[]>;
    createMeasurement(createMeasurementCopyDTO: CreateLocal_RegistrosDTO): Promise<Local_Registros>;
    deleteMeasurement(id: any): Promise<Local_Registros>;
    updateMeasurement(id: string, body: CreateLocal_RegistrosDTO): Promise<Local_Registros>;
}
