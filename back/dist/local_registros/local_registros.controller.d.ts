import { CreateLocal_RegistrosDTO } from './dto/local_registros.dto';
import { LocalRegistrosService } from './local_registros.service';
export declare class LocalRegistrosController {
    private measurementService;
    constructor(measurementService: LocalRegistrosService);
    createMeasurement(res: any, body: CreateLocal_RegistrosDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateLocal_RegistrosDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
