import { Document } from 'mongoose';
export interface Local_Registros extends Document {
    id_dispositivos: String;
    fecha_registros: Date;
    oxd: Number;
    oxs: Number;
    temp: Number;
    sal: Number;
    code: String;
}
