import { Document } from 'mongoose';
export interface Event extends Document {
    detail: string;
    n_visit: string;
    dateTime: Date;
    endDateTime?: Date;
    activityId: string;
    workplaceId: string;
    active: boolean;
}
