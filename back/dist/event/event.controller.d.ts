import { EventService } from './event.service';
import { CreateEventDTO } from './dto/event.dto';
export declare class EventController {
    private eventService;
    constructor(eventService: EventService);
    createEvent(res: any, body: CreateEventDTO): Promise<any>;
    getEventsAll(res: any): Promise<any>;
    getEventWorkplace(res: any, workplaceId: any): Promise<any>;
    getEvent(res: any, eventId: any): Promise<any>;
    getFindActiveEvent(res: any, workplaceId: any): Promise<any>;
    getFindActiveEventEnd(res: any, body: any): Promise<any>;
    updateEvent(res: any, body: CreateEventDTO, eventId: any): Promise<any>;
    deleteEvent(res: any, eventId: any): Promise<any>;
}
