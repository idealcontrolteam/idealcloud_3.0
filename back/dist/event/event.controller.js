"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const event_service_1 = require("./event.service");
const event_dto_1 = require("./dto/event.dto");
let EventController = class EventController {
    constructor(eventService) {
        this.eventService = eventService;
    }
    createEvent(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newEvent = yield this.eventService.createEvent(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Event created successfully',
                data: newEvent,
            });
        });
    }
    getEventsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const events = yield this.eventService.getEventAll();
            let msg = events.length == 0 ? 'Events not found' : 'Events fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: events,
                count: events.length,
            });
        });
    }
    getEventWorkplace(res, workplaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const event = yield this.eventService.getEventWorkplace(workplaceId);
            if (!event) {
                throw new common_1.NotFoundException('Event not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Event found',
                data: event,
            });
        });
    }
    getEvent(res, eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!eventId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Event id is not a valid  ObjectId');
            }
            const event = yield this.eventService.getEvent(eventId);
            if (!event) {
                throw new common_1.NotFoundException('Event not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Event found',
                data: event,
            });
        });
    }
    getFindActiveEvent(res, workplaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const event = yield this.eventService.getfindActiveEvent(workplaceId);
            if (!event) {
                throw new common_1.NotFoundException('Event not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Event found',
                data: event,
            });
        });
    }
    getFindActiveEventEnd(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const event = yield this.eventService.getfindActiveEventEnd(body);
            if (!event) {
                throw new common_1.NotFoundException('Event not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Event found',
                data: event,
            });
        });
    }
    updateEvent(res, body, eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!eventId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Event id is not a valid  ObjectId');
            }
            const updatedEvent = yield this.eventService.updateEvent(eventId, body);
            if (!updatedEvent) {
                throw new common_1.NotFoundException('Event not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Event updated',
                data: updatedEvent,
            });
        });
    }
    deleteEvent(res, eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedEvent = yield this.eventService.deleteEvent(eventId);
            if (!deletedEvent) {
                throw new common_1.NotFoundException('Event not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Event deleted',
                data: deletedEvent,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, event_dto_1.CreateEventDTO]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "createEvent", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getEventsAll", null);
__decorate([
    common_1.Get('/workplace/:workplaceId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('workplaceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getEventWorkplace", null);
__decorate([
    common_1.Get('/:eventId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('eventId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getEvent", null);
__decorate([
    common_1.Get('/:workplaceId/active'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('workplaceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getFindActiveEvent", null);
__decorate([
    common_1.Post('/workplaceIds/active_end'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getFindActiveEventEnd", null);
__decorate([
    common_1.Put('/:eventId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('eventId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, event_dto_1.CreateEventDTO, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "updateEvent", null);
__decorate([
    common_1.Delete('/:eventId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('eventId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "deleteEvent", null);
EventController = __decorate([
    common_1.Controller('event'),
    __metadata("design:paramtypes", [event_service_1.EventService])
], EventController);
exports.EventController = EventController;
//# sourceMappingURL=event.controller.js.map