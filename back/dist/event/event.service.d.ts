import { Model } from 'mongoose';
import { CreateEventDTO } from './dto/event.dto';
import { Event } from './interfaces/event.interface';
export declare class EventService {
    private eventModel;
    constructor(eventModel: Model<Event>);
    getEventAll(): Promise<Event[]>;
    getEventWorkplace(workplaceId: any): Promise<any>;
    getfindActiveEvent(workplaceId: any): Promise<any>;
    getfindActiveEventEnd(workplaceIds: any): Promise<any>;
    getEvent(id: any): Promise<Event>;
    createEvent(createEventDTO: CreateEventDTO): Promise<Event>;
    deleteEvent(id: any): Promise<Event>;
    updateEvent(id: string, body: CreateEventDTO): Promise<Event>;
}
