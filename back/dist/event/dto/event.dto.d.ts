export declare class CreateEventDTO {
    detail: String;
    dateTime: Date;
    endDateTime: Date;
    n_visit: number;
    activityId: String;
    workplaceId: String;
    active: boolean;
}
