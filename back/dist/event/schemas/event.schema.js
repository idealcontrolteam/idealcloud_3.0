"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.EventSchema = new mongoose_1.Schema({
    detail: String,
    n_visit: Number,
    dateTime: Date,
    endDateTime: Date,
    activityId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Activity',
        required: false,
    },
    workplaceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Workplace',
        required: false,
    },
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=event.schema.js.map