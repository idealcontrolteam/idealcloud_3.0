/// <reference types="mongodb" />
import { Model } from 'mongoose';
import { CreateUserDTO } from './dto/user.dto';
import { User } from './interfaces/user.interface';
export declare class UserService {
    private userModel;
    constructor(userModel: Model<User>);
    getUsers(): Promise<User[]>;
    getUsersByRoleId(roleId: any): Promise<User[]>;
    getUsersAll(): Promise<User[]>;
    getUsersByCompanyId(companyId: any): Promise<User[]>;
    getUser(id: any): Promise<User>;
    checkToken(u: any): Promise<User>;
    hashPassword: (password: any) => Promise<string>;
    createUser(createUserDTO: CreateUserDTO): Promise<User>;
    login(user: any): Promise<User>;
    deleteUser(id: any): Promise<User>;
    deleteUsersByCompanyId(companyId: any): Promise<{
        ok?: number;
        n?: number;
    }>;
    updateUser(id: string, body: CreateUserDTO): Promise<User>;
    updateUserToken(id: string, body: any): Promise<User>;
}
