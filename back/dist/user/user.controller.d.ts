import { CreateUserDTO } from './dto/user.dto';
import { UserService } from './user.service';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    createUser(res: any, body: CreateUserDTO): Promise<any>;
    login(res: any, body: any): Promise<any>;
    private getToken;
    checkToken(res: any, token: any): Promise<any>;
    getUsers(res: any): Promise<any>;
    getUsersAll(res: any): Promise<any>;
    getUser(res: any, userId: any): Promise<any>;
    updateUser(res: any, body: CreateUserDTO, userId: any): Promise<any>;
    deleteUser(res: any, userId: any): Promise<any>;
    uploadFile(file: any): Promise<void>;
}
