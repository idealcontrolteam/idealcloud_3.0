import { Model } from 'mongoose';
import { LocalMonit } from './interfaces/local-monit.interface';
import { CreateLocalMonitDTO } from './dto/local-monit.dto';
export declare class LocalMonitService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<LocalMonit>);
    getMeasurements(): Promise<LocalMonit[]>;
    getMeasurementsAll(): Promise<LocalMonit[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<LocalMonit[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<LocalMonit[]>;
    getMeasurementByLocationfiltered(id_dispositivos: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurement(id: any): Promise<LocalMonit>;
    getMeasurementsByLocationId(locationId: any): Promise<LocalMonit[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<LocalMonit[]>;
    getMeasurementsByTagId(tagId: any): Promise<LocalMonit[]>;
    createMeasurement(createLocalMonitDTO: CreateLocalMonitDTO): Promise<LocalMonit>;
    deleteMeasurement(id: any): Promise<LocalMonit>;
    updateMeasurement(id: string, body: CreateLocalMonitDTO): Promise<LocalMonit>;
}
