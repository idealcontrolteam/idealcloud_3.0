import { CreateLocalMonitDTO } from './dto/local-monit.dto';
import { LocalMonitService } from './local-monit.service';
export declare class LocalMonitController {
    private measurementService;
    constructor(measurementService: LocalMonitService);
    createMeasurement(res: any, body: CreateLocalMonitDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, id_dispositivos: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateLocalMonitDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
