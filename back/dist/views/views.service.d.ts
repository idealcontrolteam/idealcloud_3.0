import { Model } from 'mongoose';
import { View1 } from './view.interface';
export declare class ViewsService {
    private tagModel;
    constructor(tagModel: Model<View1>);
    getViews(tagId: any, fini: any, ffin: any): Promise<any[]>;
}
