"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ViewSchema = new mongoose_1.Schema({
    name: String,
    shortName: String,
    unity: Number,
    meditions: [{
            dateTime: Date,
            value: Number,
            active: Boolean
        }]
}, { versionKey: false });
//# sourceMappingURL=view.schema.js.map