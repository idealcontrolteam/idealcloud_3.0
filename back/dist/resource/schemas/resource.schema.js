"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ResourceSchema = new mongoose_1.Schema({
    archiveId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Archive',
        required: false,
    },
    groupId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Group',
        required: false,
    },
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=resource.schema.js.map