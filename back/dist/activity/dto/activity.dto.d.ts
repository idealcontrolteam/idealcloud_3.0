export declare class CreateActivityDTO {
    detail: String;
    active: boolean;
    dateTime: Date;
}
