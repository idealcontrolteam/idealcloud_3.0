"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ActivitySchema = new mongoose_1.Schema({
    detail: String,
    active: Boolean,
    dateTime: Date,
}, { versionKey: false });
//# sourceMappingURL=actitvity.schema.js.map