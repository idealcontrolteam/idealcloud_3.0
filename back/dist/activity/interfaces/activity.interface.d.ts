import { Document } from 'mongoose';
export interface Activity extends Document {
    detail: string;
    active: boolean;
    dateTime: Date;
}
