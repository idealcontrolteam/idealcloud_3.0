import { Model } from 'mongoose';
import { CreateActivityDTO } from './dto/activity.dto';
import { Activity } from './interfaces/activity.interface';
export declare class ActivityService {
    private activityModel;
    constructor(activityModel: Model<Activity>);
    getActivityAll(): Promise<Activity[]>;
    getActivity(id: any): Promise<Activity>;
    createActivity(createActivityDTO: CreateActivityDTO): Promise<Activity>;
    deleteActivity(id: any): Promise<Activity>;
    updateActivity(id: string, body: CreateActivityDTO): Promise<Activity>;
}
