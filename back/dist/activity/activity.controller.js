"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const activity_service_1 = require("./activity.service");
const activity_dto_1 = require("./dto/activity.dto");
let ActivityController = class ActivityController {
    constructor(activityService) {
        this.activityService = activityService;
    }
    createActivity(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newActivity = yield this.activityService.createActivity(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Activity created successfully',
                data: newActivity,
            });
        });
    }
    getActivitysAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const activitys = yield this.activityService.getActivityAll();
            let msg = activitys.length == 0 ? 'Activitys not found' : 'Activitys fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: activitys,
                count: activitys.length,
            });
        });
    }
    getActivity(res, activityId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!activityId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Activity id is not a valid  ObjectId');
            }
            const activity = yield this.activityService.getActivity(activityId);
            if (!activity) {
                throw new common_1.NotFoundException('Activity not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Activity found',
                data: activity,
            });
        });
    }
    updateActivity(res, body, activityId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!activityId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Activity id is not a valid  ObjectId');
            }
            const updatedActivity = yield this.activityService.updateActivity(activityId, body);
            if (!updatedActivity) {
                throw new common_1.NotFoundException('Activity not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Activity updated',
                data: updatedActivity,
            });
        });
    }
    deleteActivity(res, activityId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedActivity = yield this.activityService.deleteActivity(activityId);
            if (!deletedActivity) {
                throw new common_1.NotFoundException('Activity not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Activity deleted',
                data: deletedActivity,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, activity_dto_1.CreateActivityDTO]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "createActivity", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "getActivitysAll", null);
__decorate([
    common_1.Get('/:activityId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('activityId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "getActivity", null);
__decorate([
    common_1.Put('/:activityId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('activityId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, activity_dto_1.CreateActivityDTO, Object]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "updateActivity", null);
__decorate([
    common_1.Delete('/:activityId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('activityId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "deleteActivity", null);
ActivityController = __decorate([
    common_1.Controller('activity'),
    __metadata("design:paramtypes", [activity_service_1.ActivityService])
], ActivityController);
exports.ActivityController = ActivityController;
//# sourceMappingURL=activity.controller.js.map