import { ActivityService } from './activity.service';
import { CreateActivityDTO } from './dto/activity.dto';
export declare class ActivityController {
    private activityService;
    constructor(activityService: ActivityService);
    createActivity(res: any, body: CreateActivityDTO): Promise<any>;
    getActivitysAll(res: any): Promise<any>;
    getActivity(res: any, activityId: any): Promise<any>;
    updateActivity(res: any, body: CreateActivityDTO, activityId: any): Promise<any>;
    deleteActivity(res: any, activityId: any): Promise<any>;
}
