"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ArchiveSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
    url: String,
    ruta: String,
    type: String,
    workplaceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'WorkPlace',
        required: false,
    },
}, { versionKey: false });
//# sourceMappingURL=archive.schema.js.map