export declare class CreateArchiveDTO {
    name: String;
    active: boolean;
    url: String;
    ruta: String;
    type: String;
    workplaceId: String;
}
