"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const archive_service_1 = require("./archive.service");
const archive_dto_1 = require("./dto/archive.dto");
const platform_express_1 = require("@nestjs/platform-express");
const fs_1 = require("fs");
let ArchiveController = class ArchiveController {
    constructor(archiveService) {
        this.archiveService = archiveService;
    }
    createArchive(res, body, file) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let name = "";
                if (body.url == "" || body.url == null) {
                    name = String(file.originalname);
                    name = name.replace(' ', '_');
                    var date = new Date;
                    var seconds = String(date.getSeconds());
                    var minutes = String(date.getMinutes());
                    var hour = String(date.getHours());
                    var day = String(date.getDate());
                    name = day + hour + minutes + seconds + name;
                    let path = "";
                    let raiz = "/var/www/idealcontrol_cloud/ideal_cloud3/front/doc";
                    if (body.type == "certificado")
                        path = raiz + "/certificados/" + name;
                    else if (body.type == "video")
                        path = raiz + "/videos/" + file.originalname;
                    else if (body.type == "manual")
                        path = raiz + "/manuales/" + file.originalname;
                    else
                        path = raiz + "/otros/" + file.originalname;
                    body.ruta = path;
                    let fileStream = fs_1.createWriteStream(path);
                    fileStream.write(file.buffer);
                    fileStream.end();
                }
                const newArchive = yield this.archiveService.createArchive(body);
                return res.status(common_1.HttpStatus.CREATED).json({
                    statusCode: common_1.HttpStatus.CREATED,
                    message: 'Archive created successfully',
                    data: newArchive,
                });
            }
            catch (e) {
                throw new common_1.NotFoundException('Archive no tiene todos la data necesaria');
            }
        });
    }
    download(archiveId, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const archive = yield this.archiveService.getArchive(archiveId);
            return res.download(archive.ruta);
        });
    }
    getArchivesAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const archives = yield this.archiveService.getArchiveAll();
            let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: archives,
                count: archives.length,
            });
        });
    }
    getArchivesCertificados(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const archives = yield this.archiveService.getArchiveCertificados();
            let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: archives,
                count: archives.length,
            });
        });
    }
    getArchivesManuales(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const archives = yield this.archiveService.getArchivesManuales();
            let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: archives,
                count: archives.length,
            });
        });
    }
    getArchivesVideos(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const archives = yield this.archiveService.getArchivesVideos();
            let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: archives,
                count: archives.length,
            });
        });
    }
    getVideosStreaming(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const range = req.headers.range;
            if (!range) {
                res.status(400).send("Requires Range header");
                return;
            }
            const videoPath = '/idealcontrol/idealcloud_3.0/back_/videos/GraphQL.mp4';
            const videoSize = fs_1.statSync(videoPath).size;
            const chunkSize = 1 * 1e+6;
            const start = Number(range.replace(/\D/g, ''));
            const end = Math.min(start + chunkSize, videoSize - 1);
            const contentLength = end - start + 1;
            const headers = {
                "Content-Range": `bytes ${start}-${end}/${videoSize}`,
                "Accept-Ranges": "bytes",
                "Content-Length": contentLength,
                "Content-Type": "video/mp4"
            };
            res.writeHead(206, headers);
            const stream = fs_1.createReadStream(videoPath, { start, end });
            stream.pipe(res);
        });
    }
    getArchive(res, archiveId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!archiveId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Archive id is not a valid  ObjectId');
            }
            const archive = yield this.archiveService.getArchive(archiveId);
            if (!archive) {
                throw new common_1.NotFoundException('Archive not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Archive found',
                data: archive,
            });
        });
    }
    updateArchive(res, body, archiveId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!archiveId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Archive id is not a valid  ObjectId');
            }
            const updatedArchive = yield this.archiveService.updateArchive(archiveId, body);
            if (!updatedArchive) {
                throw new common_1.NotFoundException('Archive not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Archive updated',
                data: updatedArchive,
            });
        });
    }
    deleteArchive(res, archiveId) {
        return __awaiter(this, void 0, void 0, function* () {
            const archive = yield this.archiveService.getArchive(archiveId);
            const deletedArchive = yield this.archiveService.deleteArchive(archiveId);
            if (deletedArchive != null && archive.url == null && archive.ruta != null) {
                const path = archive.ruta;
                fs_1.unlinkSync(path);
                return res.status(common_1.HttpStatus.CREATED).json({
                    statusCode: common_1.HttpStatus.CREATED,
                    message: 'El Archivo fue eliminado'
                });
            }
            if (!deletedArchive) {
                throw new common_1.NotFoundException('Archive not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Archive deleted',
                data: deletedArchive,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file')),
    __param(0, common_1.Res()), __param(1, common_1.Body()), __param(2, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, archive_dto_1.CreateArchiveDTO, Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "createArchive", null);
__decorate([
    common_1.Get('/descarga/:archiveId'),
    __param(0, common_1.Param('archiveId')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "download", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "getArchivesAll", null);
__decorate([
    common_1.Get('/certificados'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "getArchivesCertificados", null);
__decorate([
    common_1.Get('/manuales'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "getArchivesManuales", null);
__decorate([
    common_1.Get('/videos'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "getArchivesVideos", null);
__decorate([
    common_1.Get('/videos_streaming'),
    __param(0, common_1.Req()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "getVideosStreaming", null);
__decorate([
    common_1.Get('/:archiveId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('archiveId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "getArchive", null);
__decorate([
    common_1.Put('/:archiveId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('archiveId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, archive_dto_1.CreateArchiveDTO, Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "updateArchive", null);
__decorate([
    common_1.Delete('/:archiveId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('archiveId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ArchiveController.prototype, "deleteArchive", null);
ArchiveController = __decorate([
    common_1.Controller('archive'),
    __metadata("design:paramtypes", [archive_service_1.ArchiveService])
], ArchiveController);
exports.ArchiveController = ArchiveController;
//# sourceMappingURL=archive.controller.js.map