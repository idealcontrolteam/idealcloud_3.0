"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const archive_controller_1 = require("./archive.controller");
const archive_service_1 = require("./archive.service");
const archive_schema_1 = require("./schemas/archive.schema");
let ArchiveModule = class ArchiveModule {
};
ArchiveModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'Archive', schema: archive_schema_1.ArchiveSchema, collection: 'archive' },
            ]),
        ],
        controllers: [archive_controller_1.ArchiveController],
        providers: [archive_service_1.ArchiveService]
    })
], ArchiveModule);
exports.ArchiveModule = ArchiveModule;
//# sourceMappingURL=archive.module.js.map