import { Model } from 'mongoose';
import { CreateArchiveDTO } from './dto/archive.dto';
import { Archive } from './interfaces/archive.interface';
export declare class ArchiveService {
    private activityModel;
    constructor(activityModel: Model<Archive>);
    getArchiveAll(): Promise<Archive[]>;
    getArchiveCertificados(): Promise<Archive[]>;
    getArchivesManuales(): Promise<Archive[]>;
    getArchivesVideos(): Promise<Archive[]>;
    getArchive(id: any): Promise<Archive>;
    createArchive(createArchiveDTO: CreateArchiveDTO): Promise<Archive>;
    deleteArchive(id: any): Promise<any>;
    updateArchive(id: string, body: CreateArchiveDTO): Promise<Archive>;
}
