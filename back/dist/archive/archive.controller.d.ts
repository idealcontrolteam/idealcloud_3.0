import { ArchiveService } from './archive.service';
import { CreateArchiveDTO } from './dto/archive.dto';
export declare class ArchiveController {
    private archiveService;
    constructor(archiveService: ArchiveService);
    createArchive(res: any, body: CreateArchiveDTO, file: any): Promise<any>;
    download(archiveId: any, res: any): Promise<any>;
    getArchivesAll(res: any): Promise<any>;
    getArchivesCertificados(res: any): Promise<any>;
    getArchivesManuales(res: any): Promise<any>;
    getArchivesVideos(res: any): Promise<any>;
    getVideosStreaming(req: any, res: any): Promise<void>;
    getArchive(res: any, archiveId: any): Promise<any>;
    updateArchive(res: any, body: CreateArchiveDTO, archiveId: any): Promise<any>;
    deleteArchive(res: any, archiveId: any): Promise<any>;
}
