"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const fail_dto_1 = require("./dto/fail.dto");
const fail_service_1 = require("./fail.service");
let FailController = class FailController {
    constructor(failService) {
        this.failService = failService;
    }
    createFail(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newAFail = yield this.failService.createFail(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Fail created successfully',
                data: newAFail,
            });
        });
    }
    getFails(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failService.getFails();
            let msg = fails.length == 0 ? 'Fails not found' : 'Fails fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: fails,
                count: fails.length,
            });
        });
    }
    getFailByfilteredDate(res, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failService.getFailByfilteredDate(fini, ffin);
            let msg = fails.length == 0
                ? 'fail not found'
                : 'fails fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: fails,
                count: fails.length,
            });
        });
    }
    getFailsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failService.getFailsAll();
            let msg = fails.length == 0 ? 'Fails not found' : 'Fails fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: fails,
                count: fails.length,
            });
        });
    }
    getFail(res, failId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!failId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Fail id is not a valid  ObjectId');
            }
            const fail = yield this.failService.getFail(failId);
            if (!fail) {
                throw new common_1.NotFoundException('Fail not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Fail found',
                data: fail,
            });
        });
    }
    updateFail(res, body, failId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!failId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Fail id is not a valid  ObjectId');
            }
            const updatedFail = yield this.failService.updateFail(failId, body);
            if (!updatedFail) {
                throw new common_1.NotFoundException('Fail not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Fail updated',
                data: updatedFail,
            });
        });
    }
    deleteFail(res, failId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedFail = yield this.failService.deleteFail(failId);
            if (!deletedFail) {
                throw new common_1.NotFoundException('Fail not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Fail deleted',
                data: deletedFail,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, fail_dto_1.CreateFailDTO]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "createFail", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "getFails", null);
__decorate([
    common_1.Get('/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('fini')),
    __param(2, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "getFailByfilteredDate", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "getFailsAll", null);
__decorate([
    common_1.Get('/:failId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('failId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "getFail", null);
__decorate([
    common_1.Put('/:failId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('failId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, fail_dto_1.CreateFailDTO, Object]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "updateFail", null);
__decorate([
    common_1.Delete('/:failId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('failId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FailController.prototype, "deleteFail", null);
FailController = __decorate([
    common_1.Controller('fail'),
    __metadata("design:paramtypes", [fail_service_1.FailService])
], FailController);
exports.FailController = FailController;
//# sourceMappingURL=fail.controller.js.map