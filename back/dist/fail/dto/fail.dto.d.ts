export declare class CreateFailDTO {
    dateTimeIni: Date;
    dateTimeTer: Date;
    description: string;
    tagId: string;
    active: boolean;
    presentValue: number;
    failValue: number;
    sentEmail: boolean;
    accepted: boolean;
}
