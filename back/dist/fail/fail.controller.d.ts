import { CreateFailDTO } from './dto/fail.dto';
import { FailService } from './fail.service';
export declare class FailController {
    private failService;
    constructor(failService: FailService);
    createFail(res: any, body: CreateFailDTO): Promise<any>;
    getFails(res: any): Promise<any>;
    getFailByfilteredDate(res: any, fini: any, ffin: any): Promise<any>;
    getFailsAll(res: any): Promise<any>;
    getFail(res: any, failId: any): Promise<any>;
    updateFail(res: any, body: CreateFailDTO, failId: any): Promise<any>;
    deleteFail(res: any, failId: any): Promise<any>;
}
