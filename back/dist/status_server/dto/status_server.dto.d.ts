export declare class CreateStatusServerDTO {
    total_memory: Number;
    used_memory: Number;
    ram: Number;
    free_memory: Number;
    cpu: Number;
    cache: Number;
    swap: Number;
    buffers: Number;
    clearBuffer: boolean;
    total_swap: Number;
    total_disk: Number;
    used_disk: Number;
}
