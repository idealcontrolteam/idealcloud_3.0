import { CreateStatusServerDTO } from './dto/status_server.dto';
import { StatusServerService } from './status_server.service';
export declare class StatusServerController {
    private areaService;
    constructor(areaService: StatusServerService);
    createArea(res: any, body: CreateStatusServerDTO): Promise<any>;
    getModels(res: any): Promise<any>;
    updateSensor(res: any, body: CreateStatusServerDTO, serverId: any): Promise<any>;
}
