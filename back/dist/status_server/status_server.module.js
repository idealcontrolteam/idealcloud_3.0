"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const status_server_service_1 = require("./status_server.service");
const status_server_controller_1 = require("./status_server.controller");
const mongoose_1 = require("@nestjs/mongoose");
const status_server_1 = require("./schemas/status_server");
let StatusServerModule = class StatusServerModule {
};
StatusServerModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'StatusServer', schema: status_server_1.StatusServerSchema, collection: 'status_server' },
            ]),
        ],
        controllers: [status_server_controller_1.StatusServerController],
        providers: [status_server_service_1.StatusServerService],
        exports: [status_server_service_1.StatusServerService],
    })
], StatusServerModule);
exports.StatusServerModule = StatusServerModule;
//# sourceMappingURL=status_server.module.js.map