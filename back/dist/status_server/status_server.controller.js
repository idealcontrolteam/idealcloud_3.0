"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const status_server_dto_1 = require("./dto/status_server.dto");
const status_server_service_1 = require("./status_server.service");
let StatusServerController = class StatusServerController {
    constructor(areaService) {
        this.areaService = areaService;
    }
    createArea(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newCycles = yield this.areaService.createArea(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Status Server created successfully',
                data: newCycles,
            });
        });
    }
    getModels(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const models = yield this.areaService.getAreas();
            let msg = models.length == 0 ? 'Status Server not found' : 'Status Server fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: models,
                count: models.length,
            });
        });
    }
    updateSensor(res, body, serverId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!serverId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const updatedSensor = yield this.areaService.updateServer(serverId, body);
            if (!updatedSensor) {
                throw new common_1.NotFoundException('Sensor not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Sensor updated successfully',
                data: updatedSensor,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, status_server_dto_1.CreateStatusServerDTO]),
    __metadata("design:returntype", Promise)
], StatusServerController.prototype, "createArea", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], StatusServerController.prototype, "getModels", null);
__decorate([
    common_1.Put('/:serverId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('serverId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, status_server_dto_1.CreateStatusServerDTO, Object]),
    __metadata("design:returntype", Promise)
], StatusServerController.prototype, "updateSensor", null);
StatusServerController = __decorate([
    common_1.Controller('status_server'),
    __metadata("design:paramtypes", [status_server_service_1.StatusServerService])
], StatusServerController);
exports.StatusServerController = StatusServerController;
//# sourceMappingURL=status_server.controller.js.map