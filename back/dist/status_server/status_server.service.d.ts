import { Model } from 'mongoose';
import * as AreaI from './interfaces/status_server.interface';
import { CreateStatusServerDTO } from './dto/status_server.dto';
export declare class StatusServerService {
    private areaModel;
    constructor(areaModel: Model<AreaI.StatusServer>);
    createArea(createCyclesDTO: CreateStatusServerDTO): Promise<AreaI.StatusServer>;
    getAreas(): Promise<AreaI.StatusServer[]>;
    updateServer(id: string, body: CreateStatusServerDTO): Promise<AreaI.StatusServer>;
}
