"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.StatusServerSchema = new mongoose_1.Schema({
    total_memory: Number,
    used_memory: Number,
    ram: Number,
    free_memory: Number,
    cpu: Number,
    cache: Number,
    swap: Number,
    buffers: Number,
    clearBuffer: Boolean,
    total_swap: Number,
    total_disk: Number,
    used_disk: Number,
});
//# sourceMappingURL=status_server.js.map