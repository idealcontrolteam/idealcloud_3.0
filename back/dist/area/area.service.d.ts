import { Model } from 'mongoose';
import * as AreaI from './interfaces/area.interface';
import { CreateAreaDTO } from './dto/area.dto';
export declare class AreaService {
    private areaModel;
    constructor(areaModel: Model<AreaI.Area>);
    createArea(createCyclesDTO: CreateAreaDTO): Promise<AreaI.Area>;
    getAreas(): Promise<AreaI.Area[]>;
}
