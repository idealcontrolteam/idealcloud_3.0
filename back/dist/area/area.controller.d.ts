import { CreateAreaDTO } from './dto/area.dto';
import { AreaService } from './area.service';
export declare class AreaController {
    private areaService;
    constructor(areaService: AreaService);
    createArea(res: any, body: CreateAreaDTO): Promise<any>;
    getModels(res: any): Promise<any>;
}
