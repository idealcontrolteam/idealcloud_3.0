"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.AreaSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
});
//# sourceMappingURL=area.schema.js.map