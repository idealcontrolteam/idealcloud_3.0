"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const area_service_1 = require("./area.service");
const area_controller_1 = require("./area.controller");
const mongoose_1 = require("@nestjs/mongoose");
const area_schema_1 = require("./schemas/area.schema");
let AreaModule = class AreaModule {
};
AreaModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'Area', schema: area_schema_1.AreaSchema, collection: 'area' },
            ]),
        ],
        controllers: [area_controller_1.AreaController],
        providers: [area_service_1.AreaService],
        exports: [area_service_1.AreaService],
    })
], AreaModule);
exports.AreaModule = AreaModule;
//# sourceMappingURL=area.module.js.map