export declare class CreateGroupDTO {
    name: String;
    userId: String;
    type: String;
}
