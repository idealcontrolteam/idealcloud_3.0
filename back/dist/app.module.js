"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const mongoose_1 = require("@nestjs/mongoose");
const email_suport_service_1 = require("./email_suport/email_suport.service");
const email_suport_module_1 = require("./email_suport/email_suport.module");
const email_suport_controller_1 = require("./email_suport/email_suport.controller");
const tcp_connection_module_1 = require("./tcp-connection/tcp-connection.module");
const serial_connection_module_1 = require("./serial-connection/serial-connection.module");
const gateway_module_1 = require("./gateway/gateway.module");
const device_module_1 = require("./device/device.module");
const local_control_module_1 = require("./local_control/local_control.module");
const local_registros_module_1 = require("./local_registros/local_registros.module");
const local_registros_service_1 = require("./local_registros/local_registros.service");
const local_registros_controller_1 = require("./local_registros/local_registros.controller");
const category_module_1 = require("./category/category.module");
const company_module_1 = require("./company/company.module");
const work_place_module_1 = require("./work-place/work-place.module");
const user_module_1 = require("./user/user.module");
const role_module_1 = require("./role/role.module");
const zone_module_1 = require("./zone/zone.module");
const location_module_1 = require("./location/location.module");
const sensor_module_1 = require("./sensor/sensor.module");
const measurement_module_1 = require("./measurement/measurement.module");
const tag_module_1 = require("./tag/tag.module");
const tag_type_module_1 = require("./tag-type/tag-type.module");
const tag_config_control_module_1 = require("./tag-config-control/tag-config-control.module");
const alarm_module_1 = require("./alarm/alarm.module");
const fail_module_1 = require("./fail/fail.module");
const views_controller_1 = require("./views/views.controller");
const views_module_1 = require("./views/views.module");
const area_controller_1 = require("./area/area.controller");
const area_module_1 = require("./area/area.module");
const activity_module_1 = require("./activity/activity.module");
const cycles_controller_1 = require("./cycles/cycles.controller");
const cycles_module_1 = require("./cycles/cycles.module");
const cycles_service_1 = require("./cycles/cycles.service");
const event_module_1 = require("./event/event.module");
const group_controller_1 = require("./group/group.controller");
const group_service_1 = require("./group/group.service");
const group_module_1 = require("./group/group.module");
const resource_controller_1 = require("./resource/resource.controller");
const resource_service_1 = require("./resource/resource.service");
const resource_module_1 = require("./resource/resource.module");
const archive_controller_1 = require("./archive/archive.controller");
const archive_service_1 = require("./archive/archive.service");
const archive_module_1 = require("./archive/archive.module");
const measurement_today_controller_1 = require("./measurement-today/measurement-today.controller");
const measurement_today_service_1 = require("./measurement-today/measurement-today.service");
const measurement_today_module_1 = require("./measurement-today/measurement-today.module");
const register_today_controller_1 = require("./register-today/register-today.controller");
const register_today_module_1 = require("./register-today/register-today.module");
const register_today_service_1 = require("./register-today/register-today.service");
const register_sma_controller_1 = require("./register-sma/register-sma.controller");
const register_sma_module_1 = require("./register-sma/register-sma.module");
const register_sma_service_1 = require("./register-sma/register-sma.service");
const measurement_control_controller_1 = require("./measurement-control/measurement-control.controller");
const measurement_control_service_1 = require("./measurement-control/measurement-control.service");
const measurement_control_module_1 = require("./measurement-control/measurement-control.module");
const measurement_today_control_controller_1 = require("./measurement-today-control/measurement-today-control.controller");
const measurement_today_control_module_1 = require("./measurement-today-control/measurement-today-control.module");
const measurement_today_control_service_1 = require("./measurement-today-control/measurement-today-control.service");
const status_server_module_1 = require("./status_server/status_server.module");
const status_server_service_1 = require("./status_server/status_server.service");
const status_server_controller_1 = require("./status_server/status_server.controller");
const efectividad_day_module_1 = require("./efectividad-day/efectividad-day.module");
const efectividad_day_service_1 = require("./efectividad-day/efectividad-day.service");
const efectividad_day_controller_1 = require("./efectividad-day/efectividad-day.controller");
const efectividad_month_module_1 = require("./efectividad-month/efectividad-month.module");
const efectividad_month_service_1 = require("./efectividad-month/efectividad-month.service");
const efectividad_month_controller_1 = require("./efectividad-month/efectividad-month.controller");
const register_now_controller_1 = require("./register-now/register-now.controller");
const register_now_service_1 = require("./register-now/register-now.service");
const register_now_module_1 = require("./register-now/register-now.module");
const local_registros_hora_controller_1 = require("./local_registros_hora/local_registros_hora.controller");
const local_registros_hora_service_1 = require("./local_registros_hora/local_registros_hora.service");
const local_registros_hora_module_1 = require("./local_registros_hora/local_registros_hora.module");
const local_monit_controller_1 = require("./local-monit/local-monit.controller");
const local_monit_service_1 = require("./local-monit/local-monit.service");
const local_monit_module_1 = require("./local-monit/local-monit.module");
const register_after_controller_1 = require("./register-after/register-after.controller");
const register_after_service_1 = require("./register-after/register-after.service");
const register_after_module_1 = require("./register-after/register-after.module");
const register_server_controller_1 = require("./register-server/register-server.controller");
const register_server_service_1 = require("./register-server/register-server.service");
const register_server_module_1 = require("./register-server/register-server.module");
const measurement_hour_controller_1 = require("./measurement-hour/measurement-hour.controller");
const measurement_hour_module_1 = require("./measurement-hour/measurement-hour.module");
const measurement_hour_service_1 = require("./measurement-hour/measurement-hour.service");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@169.53.138.44:27017/bdideal_cloud?authSource=admin', {
                useNewUrlParser: true,
                useFindAndModify: false
            }),
            tcp_connection_module_1.TcpConnectionModule,
            serial_connection_module_1.SerialConnectionModule,
            gateway_module_1.GatewayModule,
            device_module_1.DeviceModule,
            local_control_module_1.MeasurementCopyModule,
            category_module_1.CategoryModule,
            company_module_1.CompanyModule,
            work_place_module_1.WorkPlaceModule,
            user_module_1.UserModule,
            role_module_1.RoleModule,
            zone_module_1.ZoneModule,
            location_module_1.LocationModule,
            sensor_module_1.SensorModule,
            measurement_module_1.MeasurementModule,
            tag_module_1.TagModule,
            tag_type_module_1.TagTypeModule,
            tag_config_control_module_1.TagConfigControlModule,
            alarm_module_1.AlarmModule,
            fail_module_1.FailModule,
            views_module_1.ViewsModule,
            area_module_1.AreaModule,
            activity_module_1.ActivityModule,
            event_module_1.EventModule,
            group_module_1.GroupModule,
            resource_module_1.ResourceModule,
            archive_module_1.ArchiveModule,
            cycles_module_1.CyclesModule,
            measurement_today_module_1.MeasurementTodayModule,
            register_today_module_1.RegisterTodayModule,
            register_sma_module_1.RegisterSMAModule,
            measurement_control_module_1.MeasurementControlModule,
            measurement_today_control_module_1.MeasurementTodayControlModule,
            status_server_module_1.StatusServerModule,
            efectividad_day_module_1.EfectividadDayModule,
            efectividad_month_module_1.EfectividadMonthModule,
            local_registros_module_1.LocalRegistrosModule,
            email_suport_module_1.EmailSuportModule,
            register_now_module_1.RegisterNowModule,
            register_after_module_1.RegisterAfterModule,
            local_monit_module_1.LocalMonitModule,
            register_server_module_1.RegisterServerModule,
            local_registros_hora_module_1.LocalRegistrosHoraModule,
            measurement_hour_module_1.MeasurementHourModule
        ],
        controllers: [app_controller_1.AppController, views_controller_1.ViewsController, area_controller_1.AreaController, group_controller_1.GroupController, resource_controller_1.ResourceController, archive_controller_1.ArchiveController, cycles_controller_1.CyclesController,
            measurement_today_controller_1.MeasurementTodayController, register_today_controller_1.RegisterTodayController, register_sma_controller_1.RegisterSMAController, measurement_control_controller_1.MeasurementControlController, measurement_today_control_controller_1.MeasurementTodayControlController,
            status_server_controller_1.StatusServerController, efectividad_day_controller_1.EfectividadDayController, efectividad_month_controller_1.EfectividadMonthController, local_registros_controller_1.LocalRegistrosController, email_suport_controller_1.EmailSuportController,
            register_now_controller_1.RegisterNowController, register_after_controller_1.RegisterAfterController, local_monit_controller_1.LocalMonitController, register_server_controller_1.RegisterServerController, local_registros_hora_controller_1.LocalRegistrosHoraController,
            measurement_hour_controller_1.MeasurementHourController],
        providers: [app_service_1.AppService, group_service_1.GroupService, resource_service_1.ResourceService, archive_service_1.ArchiveService, cycles_service_1.CyclesService, measurement_today_service_1.MeasurementTodayService, register_today_service_1.RegisterTodayService,
            register_sma_service_1.RegisterSMAService, measurement_control_service_1.MeasurementControlService, measurement_today_control_service_1.MeasurementTodayControlService, status_server_service_1.StatusServerService, efectividad_day_service_1.EfectividadDayService,
            efectividad_month_service_1.EfectividadMonthService, local_registros_service_1.LocalRegistrosService, email_suport_service_1.EmailSuportService, register_now_service_1.RegisterNowService, register_after_service_1.RegisterAfterService,
            local_monit_service_1.LocalMonitService, register_server_service_1.RegisterServerService, local_registros_hora_service_1.LocalRegistrosHoraService, measurement_hour_service_1.MeasurementHourService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map