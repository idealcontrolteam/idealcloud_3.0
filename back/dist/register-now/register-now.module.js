"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const register_now_controller_1 = require("./register-now.controller");
const register_now_service_1 = require("./register-now.service");
const mongoose_1 = require("@nestjs/mongoose");
const register_now_schema_1 = require("./schemas/register-now.schema");
let RegisterNowModule = class RegisterNowModule {
};
RegisterNowModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'RegisterNow',
                    schema: register_now_schema_1.RegisterNowSchema,
                    collection: 'registerNow',
                },
            ]),
        ],
        controllers: [register_now_controller_1.RegisterNowController],
        providers: [register_now_service_1.RegisterNowService],
    })
], RegisterNowModule);
exports.RegisterNowModule = RegisterNowModule;
//# sourceMappingURL=register-now.module.js.map