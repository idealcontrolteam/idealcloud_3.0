import { CreateRegisterNowDTO } from './dto/register-now.dto';
import { RegisterNowService } from './register-now.service';
export declare class RegisterNowController {
    private measurementService;
    constructor(measurementService: RegisterNowService);
    createMeasurement(res: any, body: CreateRegisterNowDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, id_dispositivos: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateRegisterNowDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
