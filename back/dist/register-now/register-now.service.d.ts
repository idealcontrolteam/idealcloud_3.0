import { Model } from 'mongoose';
import { RegisterNow } from './interfaces/register-now.interface';
import { CreateRegisterNowDTO } from './dto/register-now.dto';
export declare class RegisterNowService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<RegisterNow>);
    getMeasurements(): Promise<RegisterNow[]>;
    getMeasurementsAll(): Promise<RegisterNow[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<RegisterNow[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<RegisterNow[]>;
    getMeasurementByLocationfiltered(id_dispositivos: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurement(id: any): Promise<RegisterNow>;
    getMeasurementsByLocationId(locationId: any): Promise<RegisterNow[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<RegisterNow[]>;
    getMeasurementsByTagId(tagId: any): Promise<RegisterNow[]>;
    createMeasurement(createRegisterNowDTO: CreateRegisterNowDTO): Promise<RegisterNow>;
    deleteMeasurement(id: any): Promise<RegisterNow>;
    updateMeasurement(id: string, body: CreateRegisterNowDTO): Promise<RegisterNow>;
}
