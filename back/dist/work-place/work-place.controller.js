"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const workPlace_dto_1 = require("./dto/workPlace.dto");
const work_place_service_1 = require("./work-place.service");
const zone_service_1 = require("../zone/zone.service");
const webpush = require('web-push');
const publicVapidKey = "BFD6nyuAMhu8GAE9geEWtBvFGm6NjSKV8IljbfSriqTAZ9XkyXIxTm4lSdAPPLoYU30xb5zsVHtsqZWC8c9CEA4";
const privateVapidKey = "I87tzLTPNT8sB0k3zE90cekp0w8DzLr0R3RDGoFPRAE";
webpush.setVapidDetails('mailto:jordyp60@gmail.com', publicVapidKey, privateVapidKey);
let WorkPlaceController = class WorkPlaceController {
    constructor(workPlaceService, zoneService) {
        this.workPlaceService = workPlaceService;
        this.zoneService = zoneService;
        this.validateIds = body => {
            try {
                Object.keys(body).map(key => {
                    if (key == 'companyId' || key == 'categoryId') {
                        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                            throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                        }
                    }
                });
            }
            catch (e) {
                throw new common_1.BadRequestException(`is not a valid ObjectId`);
            }
        };
    }
    notificationSuscription(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const subscription = body;
            const payload = JSON.stringify({ title: 'test', body: "Bienvenidos a IdealCloud" });
            webpush.sendNotification(subscription, payload).catch(error => {
                console.error(error.stack);
            });
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Work place created successfully',
                data: "",
            });
        });
    }
    createWorkPlace(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newWorkPlace = yield this.workPlaceService.createWorkPlace(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Work place created successfully',
                data: newWorkPlace,
            });
        });
    }
    getWorkPlacesArray(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let newWorkPlace = [];
            try {
                newWorkPlace = yield this.workPlaceService.getWorkPlacesArray(body);
            }
            catch (e) {
                throw new common_1.NotFoundException('Work place not found');
            }
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Work place created successfully',
                data: newWorkPlace,
            });
        });
    }
    getWorkPlaces(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlaces = yield this.workPlaceService.getWorkPlaces();
            let msg = workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: workPlaces,
                count: workPlaces.length,
            });
        });
    }
    getWorkPlacesCompany(res, idCompany) {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlaces = yield this.workPlaceService.getWorkPlacesByCompanyId(idCompany);
            let msg = workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: workPlaces,
                count: workPlaces.length,
            });
        });
    }
    getCentro(res, code) {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlaces = yield this.workPlaceService.getCentro(code);
            let msg = workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: workPlaces,
                count: workPlaces.length,
            });
        });
    }
    getCentros(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let centros = body.centros;
                let workPlaces = yield this.workPlaceService.getWorkPlaces();
                let cen = centros.split(',');
                let new_centros = [];
                cen.map((c, i) => {
                    if (i != cen.length - 1)
                        new_centros.push(workPlaces.filter(w => w.code == c)[0]);
                });
                new_centros = new_centros.filter(c => c != null);
                let msg = workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: msg,
                    data: new_centros,
                    count: new_centros.length,
                });
            }
            catch (e) {
                throw new common_1.NotFoundException('Work place not found');
            }
        });
    }
    getWorkPlacesAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlaces = yield this.workPlaceService.getWorkPlacesAll();
            let msg = workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: workPlaces,
                count: workPlaces.length,
            });
        });
    }
    getGateWaysByWorkPlaceId(res, idWorkPlace) {
        return __awaiter(this, void 0, void 0, function* () {
            const gateways = yield this.workPlaceService.getGateWaysByWorkPlaceId(idWorkPlace);
            let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: gateways,
                count: gateways.length,
            });
        });
    }
    getZonesByWorkPlaceId(res, idWorkPlace) {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.workPlaceService.getZonesByWorkPlaceId(idWorkPlace);
            let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: zones,
                count: zones.length,
            });
        });
    }
    getWorkPlace(res, idWorkPlace) {
        return __awaiter(this, void 0, void 0, function* () {
            if (idWorkPlace == null) {
                throw new common_1.BadRequestException('WorkPlace id is not a valid ObjectId');
            }
            if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('WorkPlace id is not a valid ObjectId');
            }
            const workPlace = yield this.workPlaceService.getWorkPlace(idWorkPlace);
            if (!workPlace) {
                throw new common_1.NotFoundException('Work place not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Work place found',
                data: workPlace,
            });
        });
    }
    updateWorkPlace(res, body, idWorkPlace) {
        return __awaiter(this, void 0, void 0, function* () {
            if (idWorkPlace == null) {
                throw new common_1.BadRequestException('WorkPlace id is not a valid ObjectId');
            }
            if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Work place id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedWorkPlace = yield this.workPlaceService.updateWorkPlace(idWorkPlace, body);
            if (!updatedWorkPlace) {
                throw new common_1.NotFoundException('Work place not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Work place updated',
                data: updatedWorkPlace,
            });
        });
    }
    deleteWorkPlace(res, idWorkPlace) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedWorkPlace = yield this.workPlaceService.deleteWorkPlace(idWorkPlace);
            if (!deletedWorkPlace) {
                throw new common_1.NotFoundException('Work place not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Work place deleted',
                data: deletedWorkPlace,
            });
        });
    }
};
__decorate([
    common_1.Post("/subscribe"),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "notificationSuscription", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, workPlace_dto_1.CreateWorkPlaceDTO]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "createWorkPlace", null);
__decorate([
    common_1.Post("/workplaces_array"),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getWorkPlacesArray", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getWorkPlaces", null);
__decorate([
    common_1.Get('/:idCompany/company'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('idCompany')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getWorkPlacesCompany", null);
__decorate([
    common_1.Get('/:code/centro'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getCentro", null);
__decorate([
    common_1.Post('/user_centros'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getCentros", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getWorkPlacesAll", null);
__decorate([
    common_1.Get('/:idWorkPlace/gateway'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('idWorkPlace')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getGateWaysByWorkPlaceId", null);
__decorate([
    common_1.Get('/:idWorkPlace/zone'),
    __param(0, common_1.Res()), __param(1, common_1.Param('idWorkPlace')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getZonesByWorkPlaceId", null);
__decorate([
    common_1.Get('/:idWorkPlace'),
    __param(0, common_1.Res()), __param(1, common_1.Param('idWorkPlace')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "getWorkPlace", null);
__decorate([
    common_1.Put('/:idWorkPlace'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('idWorkPlace')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, workPlace_dto_1.CreateWorkPlaceDTO, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "updateWorkPlace", null);
__decorate([
    common_1.Delete('/:idWorkPlace'),
    __param(0, common_1.Res()), __param(1, common_1.Param('idWorkPlace')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkPlaceController.prototype, "deleteWorkPlace", null);
WorkPlaceController = __decorate([
    common_1.Controller('workPlace'),
    __metadata("design:paramtypes", [work_place_service_1.WorkPlaceService,
        zone_service_1.ZoneService])
], WorkPlaceController);
exports.WorkPlaceController = WorkPlaceController;
//# sourceMappingURL=work-place.controller.js.map