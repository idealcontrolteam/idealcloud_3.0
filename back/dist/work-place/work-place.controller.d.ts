import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { WorkPlaceService } from './work-place.service';
import { ZoneService } from '../zone/zone.service';
export declare class WorkPlaceController {
    private workPlaceService;
    private zoneService;
    constructor(workPlaceService: WorkPlaceService, zoneService: ZoneService);
    validateIds: (body: any) => void;
    notificationSuscription(res: any, body: any): Promise<any>;
    createWorkPlace(res: any, body: CreateWorkPlaceDTO): Promise<any>;
    getWorkPlacesArray(res: any, body: any): Promise<any>;
    getWorkPlaces(res: any): Promise<any>;
    getWorkPlacesCompany(res: any, idCompany: any): Promise<any>;
    getCentro(res: any, code: any): Promise<any>;
    getCentros(res: any, body: any): Promise<any>;
    getWorkPlacesAll(res: any): Promise<any>;
    getGateWaysByWorkPlaceId(res: any, idWorkPlace: any): Promise<any>;
    getZonesByWorkPlaceId(res: any, idWorkPlace: any): Promise<any>;
    getWorkPlace(res: any, idWorkPlace: any): Promise<any>;
    updateWorkPlace(res: any, body: CreateWorkPlaceDTO, idWorkPlace: any): Promise<any>;
    deleteWorkPlace(res: any, idWorkPlace: any): Promise<any>;
}
