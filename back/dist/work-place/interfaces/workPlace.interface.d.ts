import { Document } from 'mongoose';
export interface WorkPlace extends Document {
    code: string;
    name: string;
    ubicacion: string;
    alertEmail: string;
    failEmail: string;
    active: boolean;
    companyId: string;
    categoryId: string;
    endDate: string;
    areaId?: string;
    desconect?: boolean;
    desconectDate?: string;
}
