"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const register_server_controller_1 = require("./register-server.controller");
const register_server_service_1 = require("./register-server.service");
const mongoose_1 = require("@nestjs/mongoose");
const register_server_schema_1 = require("./schemas/register-server.schema");
let RegisterServerModule = class RegisterServerModule {
};
RegisterServerModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'RegisterServer',
                    schema: register_server_schema_1.RegisterServerSchema,
                    collection: 'register_server',
                },
            ]),
        ],
        controllers: [register_server_controller_1.RegisterServerController],
        providers: [register_server_service_1.RegisterServerService,
        ],
        exports: [register_server_service_1.RegisterServerService],
    })
], RegisterServerModule);
exports.RegisterServerModule = RegisterServerModule;
//# sourceMappingURL=register-server.module.js.map