import { CreateRegisterServerDTO } from './dto/register-server.dto';
import { RegisterServerService } from './register-server.service';
export declare class RegisterServerController {
    private measurementService;
    constructor(measurementService: RegisterServerService);
    validateIds: (body: any) => void;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    createMeasurement(res: any, body: CreateRegisterServerDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByTagfilteredXY(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByTagfilteredXYActive(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationsfiltered(res: any, body: any): Promise<any>;
    getMeasurementByLocationfilteredXY(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateRegisterServerDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
