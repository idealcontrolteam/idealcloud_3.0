import { Model } from 'mongoose';
import { RegisterServer } from './interfaces/register-server.interface';
import { CreateRegisterServerDTO } from './dto/register-server.dto';
export declare class RegisterServerService {
    private measurementModel;
    constructor(measurementModel: Model<RegisterServer>);
    getMeasurements(): Promise<RegisterServer[]>;
    getMeasurementsAll(): Promise<RegisterServer[]>;
    getMeasurementByTagfiltered(fini: any, ffin: any): Promise<any[]>;
    getMeasurementByTagfilteredXY(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementByTagfilteredXYActive(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementFilteredByTagsAndDate(tags: any, fini: any, ffin: any): Promise<RegisterServer[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<RegisterServer[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementByLocationsfiltered(locationsId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementByLocationallfiltered(locationId: any, fini: any, ffin: any): Promise<RegisterServer[]>;
    getMeasurementByLocationfilteredXY(locationId: any, fini: any, ffin: any): Promise<RegisterServer[]>;
    getMeasurement(id: any): Promise<RegisterServer>;
    getMeasurementsByLocationId(locationId: any): Promise<RegisterServer[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<RegisterServer[]>;
    getMeasurementsByTagId(tagId: any): Promise<RegisterServer[]>;
    createMeasurement(createMeasurementDTO: CreateRegisterServerDTO): Promise<RegisterServer>;
    deleteMeasurement(id: any): Promise<RegisterServer>;
    updateMeasurement(id: string, body: CreateRegisterServerDTO): Promise<RegisterServer>;
}
