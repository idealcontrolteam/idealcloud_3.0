"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.RegisterServerSchema = new mongoose_1.Schema({
    dateTime: Date,
    tatotal_diskgId: Number,
    total_memory: Number,
    used_memory: Number,
    used_disk: Number,
    cpu: Number,
}, { versionKey: false });
//# sourceMappingURL=register-server.schema.js.map