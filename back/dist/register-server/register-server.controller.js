"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const register_server_dto_1 = require("./dto/register-server.dto");
const register_server_service_1 = require("./register-server.service");
const moment = require("moment");
let RegisterServerController = class RegisterServerController {
    constructor(measurementService) {
        this.measurementService = measurementService;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key != 'value' && key != 'dateTime' && key != 'active') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
    }
    createMeasurementMultiple(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let newMeasurement = {};
            body.map((Measurement) => __awaiter(this, void 0, void 0, function* () {
                newMeasurement = yield this.measurementService.createMeasurement(Measurement);
            }));
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    createMeasurement(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newMeasurement = yield this.measurementService.createMeasurement(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    getMeasurements(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurements();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('hola');
            const measurements = yield this.measurementService.getMeasurementsAll();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            const measurement = yield this.measurementService.getMeasurement(measurementId);
            if (!measurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement found',
                data: measurement,
            });
        });
    }
    getMeasurementByTagfiltered(res, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementByTagfiltered(fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByTagfilteredXY(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByTagfilteredXY(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            let prom = "";
            let max = "";
            let min = "";
            let ultimo_valor = "";
            let ultima_fecha = "";
            if (measurements.length != 0) {
                prom = ((measurements.reduce((a, b) => +a + +b.y, 0) / measurements.length)).toFixed(1);
                max = measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
                min = measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
                ultimo_valor = measurements[measurements.length - 1].y;
                ultima_fecha = moment(measurements[measurements.length - 1].x).format('YYYY-MM-DD HH:mm:ss');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
                prom: prom,
                max: max,
                min: min,
                ultimo_valor: ultimo_valor,
                ultima_fecha: ultima_fecha,
            });
        });
    }
    getMeasurementByTagfilteredXYActive(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByTagfilteredXYActive(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            let prom = "";
            let max = "";
            let min = "";
            let ultimo_valor = "";
            let ultima_fecha = "";
            if (measurements.length != 0) {
                prom = ((measurements.reduce((a, b) => +a + +b.y, 0) / measurements.length)).toFixed(1);
                max = measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
                min = measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
                ultimo_valor = measurements[measurements.length - 1].y;
                ultima_fecha = moment(measurements[measurements.length - 1].x).format('YYYY-MM-DD HH:mm:ss');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
                prom: prom,
                max: max,
                min: min,
                ultimo_valor: ultimo_valor,
                ultima_fecha: ultima_fecha,
            });
        });
    }
    getMeasurementBySensorfiltered(res, sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementBySensorfiltered(sensorId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationfiltered(locationId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationsfiltered(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementByLocationsfiltered(body.locationsId, body.fini, body.ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationfilteredXY(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationfilteredXY(locationId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    updateMeasurement(res, body, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedMeasurement = yield this.measurementService.updateMeasurement(measurementId, body);
            if (!updatedMeasurement) {
                throw new common_1.NotFoundException('Measurement not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement updated',
                data: updatedMeasurement,
            });
        });
    }
    deleteMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedMeasurement = yield this.measurementService.deleteMeasurement(measurementId);
            if (!deletedMeasurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement deleted',
                data: deletedMeasurement,
            });
        });
    }
};
__decorate([
    common_1.Post('/multiple'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "createMeasurementMultiple", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, register_server_dto_1.CreateRegisterServerDTO]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "createMeasurement", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurements", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementsAll", null);
__decorate([
    common_1.Get('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurement", null);
__decorate([
    common_1.Get('/fechas/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('fini')),
    __param(2, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementByTagfiltered", null);
__decorate([
    common_1.Get('/xy/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementByTagfilteredXY", null);
__decorate([
    common_1.Get('/xy/tag/active/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementByTagfilteredXYActive", null);
__decorate([
    common_1.Get('/sensor/:sensorId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('sensorId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementBySensorfiltered", null);
__decorate([
    common_1.Get('/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementByLocationfiltered", null);
__decorate([
    common_1.Post('/locations_array'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementByLocationsfiltered", null);
__decorate([
    common_1.Get('/xy/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "getMeasurementByLocationfilteredXY", null);
__decorate([
    common_1.Put('/:measurementId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, register_server_dto_1.CreateRegisterServerDTO, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "updateMeasurement", null);
__decorate([
    common_1.Delete('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RegisterServerController.prototype, "deleteMeasurement", null);
RegisterServerController = __decorate([
    common_1.Controller('register_server'),
    __metadata("design:paramtypes", [register_server_service_1.RegisterServerService])
], RegisterServerController);
exports.RegisterServerController = RegisterServerController;
//# sourceMappingURL=register-server.controller.js.map