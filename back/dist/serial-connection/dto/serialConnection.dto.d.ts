export declare class CreateSerialConnectionDTO {
    dataLength: string;
    parity: string;
    stopBits: string;
    baudRate: string;
    port: string;
}
