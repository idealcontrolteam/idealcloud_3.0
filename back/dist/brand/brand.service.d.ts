import { Model } from 'mongoose';
import { Brand } from './interfaces/brand.interface';
import * as ModelI from '../model/interfaces/model.interface';
import { CreateBrandDTO } from './dto/brand.dto';
import { ModelService } from '../model/model.service';
export declare class BrandService {
    private brandModel;
    private modelService;
    constructor(brandModel: Model<Brand>, modelService: ModelService);
    getBrands(): Promise<Brand[]>;
    getBrand(id: any): Promise<Brand>;
    getModelsByBrandId(brandId: any): Promise<ModelI.Model[]>;
    createBrand(createBrandDTO: CreateBrandDTO): Promise<Brand>;
    deleteBrand(id: any): Promise<Brand>;
    updateBrand(id: string, createBrandDTO: CreateBrandDTO): Promise<Brand>;
}
