import { CreateBrandDTO } from './dto/brand.dto';
import { BrandService } from './brand.service';
export declare class BrandController {
    private brandService;
    constructor(brandService: BrandService);
    createBrand(res: any, createBrandDTO: CreateBrandDTO): Promise<any>;
    getBrands(res: any): Promise<any>;
    getBrand(res: any, brandid: any): Promise<any>;
    getModelsByBrandId(res: any, brandid: any): Promise<any>;
    updateBrand(res: any, body: CreateBrandDTO, brandid: any): Promise<any>;
    deleteBrand(res: any, brandid: any): Promise<any>;
}
