"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.MeasurementCopySchema = new mongoose_1.Schema({
    fecha: String,
    code: String,
    oxd: Number,
    oxs: Number,
    temp: Number,
    sal: Number,
    bat: Number,
    setpoint: Number,
    histeresis_max: Number,
    histeresis_min: Number,
    time_on: Number,
    time_off: Number,
    subida: Number,
    id_control: String,
    id_centros: String,
    id_grupos: String,
    id_registros: Number,
    grupo: String,
    centro: String,
    nombre_dispostivos: String,
    id_settings: String,
    flujo: String,
    alarma_minima: Number,
}, { versionKey: false });
//# sourceMappingURL=local_control.schema.js.map