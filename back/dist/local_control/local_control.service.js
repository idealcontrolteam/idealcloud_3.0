"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let MeasurementCopyService = class MeasurementCopyService {
    constructor(measurementCopyModel) {
        this.measurementCopyModel = measurementCopyModel;
    }
    getMeasurements() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find();
            return measurements;
        });
    }
    getMeasurementsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel
                .find()
                .populate('tagId sensorId locationId');
            return measurements;
        });
    }
    getMeasurementByTagfiltered(tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                tagId: tagId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementBySensorfiltered(sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                sensorId: sensorId,
                dateTime: { $gte: fini, $lte: ffin },
            });
            return measurements;
        });
    }
    getMeasurementByLocationfiltered(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                locationId: locationId,
                dateTime: { $gte: fini, $lt: ffin },
            });
            return measurements;
        });
    }
    getMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurement = yield this.measurementCopyModel.findById(id);
            return measurement;
        });
    }
    getMeasurementsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                locationId: locationId,
            });
            return measurements;
        });
    }
    getMeasurementsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                sensorId: sensorId,
            });
            return measurements;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementCopyModel.find({
                tagId: tagId,
            });
            return measurements;
        });
    }
    createMeasurement(createMeasurementCopyDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeasurement = new this.measurementCopyModel(createMeasurementCopyDTO);
            return yield newMeasurement.save();
        });
    }
    deleteMeasurement(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementCopyModel.findByIdAndDelete(id);
        });
    }
    updateMeasurement(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.measurementCopyModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
MeasurementCopyService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Local_Control')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], MeasurementCopyService);
exports.MeasurementCopyService = MeasurementCopyService;
//# sourceMappingURL=local_control.service.js.map