import { Model } from 'mongoose';
import { MeasurementCopy } from './interfaces/local_control.interface';
import { CreateMeasurementCopyDTO } from './dto/local_control.dto';
export declare class MeasurementCopyService {
    private measurementCopyModel;
    constructor(measurementCopyModel: Model<MeasurementCopy>);
    getMeasurements(): Promise<MeasurementCopy[]>;
    getMeasurementsAll(): Promise<MeasurementCopy[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<MeasurementCopy[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<MeasurementCopy[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<MeasurementCopy[]>;
    getMeasurement(id: any): Promise<MeasurementCopy>;
    getMeasurementsByLocationId(locationId: any): Promise<MeasurementCopy[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<MeasurementCopy[]>;
    getMeasurementsByTagId(tagId: any): Promise<MeasurementCopy[]>;
    createMeasurement(createMeasurementCopyDTO: CreateMeasurementCopyDTO): Promise<MeasurementCopy>;
    deleteMeasurement(id: any): Promise<MeasurementCopy>;
    updateMeasurement(id: string, body: CreateMeasurementCopyDTO): Promise<MeasurementCopy>;
}
