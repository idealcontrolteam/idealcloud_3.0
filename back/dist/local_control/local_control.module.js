"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const local_control_controller_1 = require("./local_control.controller");
const local_control_service_1 = require("./local_control.service");
const mongoose_1 = require("@nestjs/mongoose");
const local_control_schema_1 = require("./schemas/local_control.schema");
let MeasurementCopyModule = class MeasurementCopyModule {
};
MeasurementCopyModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'Local_Control',
                    schema: local_control_schema_1.MeasurementCopySchema,
                    collection: 'local_control',
                },
            ]),
        ],
        controllers: [local_control_controller_1.MeasurementCopyController],
        providers: [local_control_service_1.MeasurementCopyService],
    })
], MeasurementCopyModule);
exports.MeasurementCopyModule = MeasurementCopyModule;
//# sourceMappingURL=local_control.module.js.map