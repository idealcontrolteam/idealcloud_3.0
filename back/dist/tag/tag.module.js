"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tag_controller_1 = require("./tag.controller");
const tag_service_1 = require("./tag.service");
const mongoose_1 = require("@nestjs/mongoose");
const tag_schema_1 = require("./schemas/tag.schema");
const measurement_module_1 = require("../measurement/measurement.module");
const measurement_service_1 = require("../measurement/measurement.service");
const tag_config_control_module_1 = require("../tag-config-control/tag-config-control.module");
const tag_config_control_service_1 = require("../tag-config-control/tag-config-control.service");
const alarm_service_1 = require("../alarm/alarm.service");
const alarm_module_1 = require("../alarm/alarm.module");
const fail_module_1 = require("../fail/fail.module");
const fail_service_1 = require("../fail/fail.service");
let TagModule = class TagModule {
};
TagModule = __decorate([
    common_1.Module({
        imports: [
            measurement_module_1.MeasurementModule,
            tag_config_control_module_1.TagConfigControlModule,
            alarm_module_1.AlarmModule,
            fail_module_1.FailModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'Tag', schema: tag_schema_1.TagSchema, collection: 'tag' },
            ]),
        ],
        controllers: [tag_controller_1.TagController],
        providers: [
            tag_service_1.TagService,
            measurement_service_1.MeasurementService,
            tag_config_control_service_1.TagConfigControlService,
            alarm_service_1.AlarmService,
            fail_service_1.FailService,
        ],
        exports: [tag_service_1.TagService],
    })
], TagModule);
exports.TagModule = TagModule;
//# sourceMappingURL=tag.module.js.map