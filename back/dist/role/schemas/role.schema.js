"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.RoleSchema = new mongoose_1.Schema({
    code: String,
    name: String,
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=role.schema.js.map