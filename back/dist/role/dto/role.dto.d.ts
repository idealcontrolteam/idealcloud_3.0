export declare class CreateRoleDTO {
    code?: string;
    name: string;
    active: boolean;
}
