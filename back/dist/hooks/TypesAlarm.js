"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.searchAlarm = (mediciones, type, time) => {
    let alarma = false;
    let contador = 0;
    let valueAnterior = 0;
    if (type == 'pegados') {
        mediciones.map(medicion => {
            if (!alarma) {
                if (contador >= (time * 60) / 5) {
                    alarma = true;
                }
                else if (contador == 0) {
                    valueAnterior = medicion.value;
                    contador = contador + 1;
                }
                else if (medicion.value == valueAnterior) {
                    contador = contador + 1;
                }
                else {
                    valueAnterior = medicion.value;
                    contador = 0;
                }
            }
        });
    }
    else if (type == 'en cero') {
        mediciones.map(medicion => {
            if (!alarma) {
                if (0 == medicion.value) {
                    alarma = true;
                }
            }
        });
    }
    else if (type == 'salinidad 33' || type == 'salinidad 34') {
        let split = type.split(' ');
        let num = parseInt(split[1]);
        mediciones.map(medicion => {
            if (!alarma) {
                if (num == medicion.value) {
                    alarma = true;
                }
            }
        });
    }
    else {
        console.log('exept');
    }
    if (mediciones.length > 0) {
        return alarma ? type : 'ok';
    }
    else {
        return 'sin datos';
    }
};
//# sourceMappingURL=TypesAlarm.js.map