import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateBrandDTO } from './dto/brand.dto';
import { BrandService } from './brand.service';
// import { AuthGuard } from 'src/shared/auth.guard';
import { AuthGuard } from './../shared/auth.guard';




@Controller('brand')
//@UseGuards(new AuthGuard())
export class BrandController {
  constructor(private brandService: BrandService) {}

  // @Post('/multiple')
  // async createBrandMultiple(@Res() res, @Body() createBrandDTO) {

  //   let brand = {};
  //   createBrandDTO.map(async (brand) => {
  //         brand =  await this.brandService.createBrand(brand);

  //   });

  //   return res.status(HttpStatus.CREATED).json({
  //     statusCode: HttpStatus.CREATED,
  //     message: 'Brand created successfully',
  //     data: brand,
  //   });
  // }

  @Post()
  async createBrand(@Res() res, @Body() createBrandDTO: CreateBrandDTO) {
     const brand =  await this.brandService.createBrand(createBrandDTO);

    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Brand created successfully',
      data: brand,
    });
  }

  @Get()
  async getBrands(@Res() res) {
    const brands = await this.brandService.getBrands();
    console.log("Aqui")
    let msg = brands.length == 0 ? 'Brands not found' : 'Brands fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: brands,
      count: brands.length,
    });
  }

  @Get('/:brandid')
  async getBrand(@Res() res, @Param('brandid') brandid) {
    console.log('[JUST BY BRAND ID]');
    if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Brand id is not a valid ObjectId');
    }

    const brand = await this.brandService.getBrand(brandid);
    if (!brand) {
      throw new NotFoundException('Brand not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Brand found',
      data: brand,
    });
  }

  @Get('/:brandid/model')
  async getModelsByBrandId(@Res() res, @Param('brandid') brandid) {
    if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Brand id is not a valid ObjectId');
    }

    const models = await this.brandService.getModelsByBrandId(brandid);

    let msg = models.length == 0 ? 'Models not found' : 'Models fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: models,
      count: models.length,
    });
  }

  @Put('/:brandid')
  async updateBrand(
    @Res() res,
    @Body() body: CreateBrandDTO,
    @Param('brandid') brandid,
  ) {
    if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Brand id is not a valid ObjectId');
    }
    const updatedBrand = await this.brandService.updateBrand(brandid, body);
    if (!updatedBrand) {
      throw new NotFoundException('Brand not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Brand updated successfully',
      data: updatedBrand,
    });
  }

  @Delete('/:brandid')
  async deleteBrand(@Res() res, @Param('brandid') brandid) {
    if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Brand id is not a valid ObjectId');
    }

    const brand = await this.brandService.deleteBrand(brandid);
    if (!brand) {
      throw new NotFoundException('Brand not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Brand Deleted',
      data: brand,
    });
  }
}
