import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateTcpConnectionDTO } from './dto/tcpConnection.dto';
import { TcpConnectionService } from './tcp-connection.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('tcpConnection')
//@UseGuards(new AuthGuard())
export class TcpConnectionController {
  constructor(private tcpConnectionService: TcpConnectionService) {}

  @Post()
  async createTcpConnection(@Res() res, @Body() body: CreateTcpConnectionDTO) {
    const newTcpConnection = await this.tcpConnectionService.createTcpConnection(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Tcp Connection created successfully',
      data: newTcpConnection,
    });
  }

  @Get()
  async getTcpConnections(@Res() res) {
    const tcpConnections = await this.tcpConnectionService.getTcpConnections();

    let msg =
      tcpConnections.length == 0
        ? 'Tcp connections not found'
        : 'Tcp connections fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tcpConnections,
      count: tcpConnections.length,
    });
  }

  @Get('/:tcpConnectionId/device')
  async getDevicesByTcpConnectionId(
    @Res() res,
    @Param('tcpConnectionId') tcpConnectionId,
  ) {
    const devices = await this.tcpConnectionService.getDevicesByTcpConnectionId(
      tcpConnectionId,
    );

    let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: devices,
      count: devices.length,
    });
  }

  @Get('/:tcpConnectionId')
  async getTcpConnection(
    @Res() res,
    @Param('tcpConnectionId') tcpConnectionId,
  ) {
    if (!tcpConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Id is not a valid ObjectId');
    }

    const tcpConnection = await this.tcpConnectionService.getTcpConnection(
      tcpConnectionId,
    );
    if (!tcpConnection) {
      throw new NotFoundException('Tcp connection not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tcp connection found',
      data: tcpConnection,
    });
  }

  @Put('/:tcpConnectionId')
  async updateTcpConnection(
    @Res() res,
    @Body() body: CreateTcpConnectionDTO,
    @Param('tcpConnectionId') tcpConnectionId,
  ) {
    if (!tcpConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException(
        'Tcp connection id is not a valid  ObjectId',
      );
    }

    const updatedTcpConnection = await this.tcpConnectionService.updateTcpConnection(
      tcpConnectionId,
      body,
    );
    if (!updatedTcpConnection) {
      throw new NotFoundException('Tcp connection not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tcp connection updated',
      data: updatedTcpConnection,
    });
  }

  @Delete('/:tcpConnectionId')
  async deleteTcpConnection(
    @Res() res,
    @Param('tcpConnectionId') tcpConnectionId,
  ) {
    const deletedTcpConnection = await this.tcpConnectionService.deleteTcpConnection(
      tcpConnectionId,
    );

    if (!deletedTcpConnection) {
      throw new NotFoundException('Tcp Connection not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tcp Connection deleted',
      data: deletedTcpConnection,
    });
  }
}
