import { Module } from '@nestjs/common';
import { LocalMonitController } from './local-monit.controller';
import { LocalMonitService } from './local-monit.service';
import { MongooseModule } from '@nestjs/mongoose';
import { LocalMonitSchema } from './schemas/local-monit.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'LocalMonit',
        schema: LocalMonitSchema,
        collection: 'local_monit',
      },
    ]),
  ],
  controllers: [LocalMonitController],
  providers: [LocalMonitService],
})
export class LocalMonitModule {}
