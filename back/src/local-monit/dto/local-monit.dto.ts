export class CreateLocalMonitDTO {
  fecha_registros: Date;
  code:string;
  id_dispositivos: string;
  oxd: number;
  oxs: number;
  temp: number;
  sal: number;

   // RELACIONES POR IMPLEMENTAR A FUTURO
  // // CENTROS
  // @ManyToOne(type => Centros, centros => centros.id_centros)
  // id_centros: Centros;

  // // DISPOSITIVOS
  // @ManyToOne(type => Dispositivos, dispositivos => dispositivos.id_dispositivos)
  // id_dispositivos: Dispositivos;
 
  //antiguo
  // value: number;
  // dateTime: Date;
  // tagId: string;
  // sensorId?: string;
  // locationId: string;
  // active: boolean;
  // id_registros: number;

 
}
