import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { EfectividadMonth } from './interfaces/efectividad-month.interface';
import { CreateEfectividadMonthDTO } from './dto/efectividad-month.dto';
import * as moment from 'moment';

@Injectable()
export class EfectividadMonthService {
  constructor(
    @InjectModel('EfectividadMonth')
    private EfectividadMonthModel: Model<EfectividadMonth>,
  ) {}

  async getMeasurements(): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  restartDate(f1,f2){
    return f2 - f1;
  }

  getDifDays(fe1,fe2){
    let f1=new Date(fe1);
    let f2=new Date(fe2);
    let diff=this.restartDate(f1,f2)
    return  Math.floor(diff / (1000 * 60 * 60 * 24));
  }

  async getMeasurementByLocationfiltered(
    workplaceId,
    fini,
    ffin,
  ): Promise<any[]> {
    let measurements=[];
    let f1=moment().format('YYYY-MM-DDT00:00:00')
    let f=new Date(f1)
    let start_date=moment().startOf('month').format('YYYY-MM-DDT00:00:00');
    let diff=0;
    let dif_resta=this.restartDate(f,new Date());
    //console.log(Math.floor(dif_resta / (1000 * 60))/5)
    let diff_=this.getDifDays(start_date,new Date());
    //console.log(diff_);
    // Calcular días
    //console.log(diff)
    if(workplaceId=="General"){
      measurements = await this.EfectividadMonthModel.find({
        start_date: { $gte: fini, $lt: ffin },
      });
    }else{
      measurements = await this.EfectividadMonthModel.find({
        workplaceId: workplaceId,
        start_date: { $gte: fini, $lt: ffin },
      });
    }
    
    //console.log(measurements)
    return measurements.map(m=>{
      //console.log(moment(start_date).format("YYYY-MM-DDT00:00:00")+".000Z")
      //console.log(m.start_date)
      //console.log(new Date(m.start_date).getTime()===new Date(moment(start_date).format("YYYY-MM-DDT00:00:00")+".000Z").getTime())
      if(new Date(m.start_date).getTime()===new Date(moment(start_date).format("YYYY-MM-01T00:00:00")+".000Z").getTime()){
        diff=diff_;
        if(diff==0){
          diff=1;
        }
      }else{
        //let end_date=moment(ffin).format('YYYY-MM-DDT23:59:00');
        //diff=this.getDifDays(m.start_date,end_date);
        diff=diff_;
      }
      //console.log(diff)
      // console.log(diff-1);
      // console.log(m.count)
      // console.log(288*(diff-1))
      //console.log()
      let p=0;
      let c=0;
      if(m.count>288){
        //p=m.count*100/(288*diff)-(Math.floor(dif_resta / (1000 * 60))/5)
        c=m.count-(Math.floor(dif_resta / (1000 * 60))/5)
        p=c*100/(288*diff)
        if(Math.sign(p)==-1){
          p=m.count*100/(288*diff)
        }
      }else{
        p=m.count*100/(288*diff)
      }
      return{
        "count": c,
        "end_date": m.end_date,
        "locationId": m.locationId,
        "workplaceId": m.workplaceId,
        "start_date": m.start_date,
        "dateTime": m.dateTime,
        "porcentaje": p
      }
    });
  }

  async getMeasurement(id): Promise<EfectividadMonth> {
    const measurement = await this.EfectividadMonthModel.findById(id);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<EfectividadMonth[]> {
    const measurements = await this.EfectividadMonthModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createEfectividadMonthDTO: CreateEfectividadMonthDTO,
  ): Promise<EfectividadMonth> {
    const newMeasurement = new this.EfectividadMonthModel(
      createEfectividadMonthDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<EfectividadMonth> {
    return await this.EfectividadMonthModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateEfectividadMonthDTO,
  ): Promise<EfectividadMonth> {
    return await this.EfectividadMonthModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
