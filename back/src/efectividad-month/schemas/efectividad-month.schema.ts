import { Schema } from 'mongoose';

export const EfectividadMonthSchema = new Schema(
  {
    count: Number,
    locationId: {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: true,
    },
    workplaceId: {
      type: Schema.Types.ObjectId,
      ref: 'WorkPlace',
      required: true,
    },
    start_date: Date,
    end_date: Date,
    active: Boolean,
    dateTime: Date,
  },
  { versionKey: false },
);
