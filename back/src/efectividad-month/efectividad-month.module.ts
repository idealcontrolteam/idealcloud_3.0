import { Module } from '@nestjs/common';
import { EfectividadMonthController } from './efectividad-month.controller';
import { EfectividadMonthService } from './efectividad-month.service';
import { MongooseModule } from '@nestjs/mongoose';
import { EfectividadMonthSchema } from './schemas/efectividad-month.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'EfectividadMonth',
        schema: EfectividadMonthSchema,
        collection: 'efectivityMonth',
      },
    ]),
  ],
  controllers: [EfectividadMonthController],
  providers: [EfectividadMonthService],
})
export class EfectividadMonthModule {}
