import { Module } from '@nestjs/common';
import { LocalRegistrosController } from './local_registros.controller';
import { LocalRegistrosService } from './local_registros.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Local_RegistrosSchema } from './schemas/local_registros.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Local_Registros',
        schema: Local_RegistrosSchema,
        collection: 'local_registros_',
      },
    ]),
  ],
  controllers: [LocalRegistrosController],
  providers: [LocalRegistrosService],
})
export class LocalRegistrosModule {}
