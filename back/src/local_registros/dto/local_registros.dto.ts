export class CreateLocal_RegistrosDTO {
  id_dispositivos:String;
  fecha_registros:Date;
  oxd:Number;
  oxs:Number;
  temp:Number;
  sal:Number;
  code:String;

   // RELACIONES POR IMPLEMENTAR A FUTURO
  // // CENTROS
  // @ManyToOne(type => Centros, centros => centros.id_centros)
  // id_centros: Centros;

  // // DISPOSITIVOS
  // @ManyToOne(type => Dispositivos, dispositivos => dispositivos.id_dispositivos)
  // id_dispositivos: Dispositivos;
 
  //antiguo
  // value: number;
  // dateTime: Date;
  // tagId: string;
  // sensorId?: string;
  // locationId: string;
  // active: boolean;
  // id_registros: number;

 
}
