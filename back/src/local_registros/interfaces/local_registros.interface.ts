import { Document } from 'mongoose';

export interface Local_Registros extends Document {
    id_dispositivos:String,
    fecha_registros:Date,
    oxd:Number,
    oxs:Number,
    temp:Number,
    sal:Number,
    code:String,

    // fecha: string;
    // code: string;
    // oxd: Number;
    // oxs: Number;
    // temp: Number;
    // sal: Number;
    // bat: Number;
    // setpoint?: Number;
    // histeresis_max?: Number;
    // histeresis_min?: Number;
    // time_on?: Number;
    // time_off?: Number;
    // subida?: Number;
    // id_control?: string;
    // id_grupos: string;
    // id_registros: Number;
    // grupo: string;
    // centro?: String;
    // nombre_dispostivos: string;
    // id_settings?: string;
}
