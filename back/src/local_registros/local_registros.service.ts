import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Local_Registros } from './interfaces/local_registros.interface';
import { CreateLocal_RegistrosDTO } from './dto/local_registros.dto';

@Injectable()
export class LocalRegistrosService {
  constructor(
    @InjectModel('Local_Registros')
    private measurementCopyModel: Model<Local_Registros>,
  ) {}

  async getMeasurements(): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    locationId,
    fini,
    ffin,
  ): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
      dateTime: { $gte: fini, $lt: ffin },
    });
    return measurements;
  }

  async getMeasurement(id): Promise<Local_Registros> {
    const measurement = await this.measurementCopyModel.findById(id);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<Local_Registros[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createMeasurementCopyDTO: CreateLocal_RegistrosDTO,
  ): Promise<Local_Registros> {
    const newMeasurement = new this.measurementCopyModel(
      createMeasurementCopyDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<Local_Registros> {
    return await this.measurementCopyModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateLocal_RegistrosDTO,
  ): Promise<Local_Registros> {
    return await this.measurementCopyModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
