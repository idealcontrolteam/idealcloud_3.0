import { Schema } from 'mongoose';

export const Local_RegistrosSchema = new Schema(
  {
    id_dispositivos:String,
    fecha_registros:Date,
    oxd:Number,
    oxs:Number,
    temp:Number,
    sal:Number,
    code:String,
    // value: Number,
    // dateTime: Date,
    // tagId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Tag',
    //   required: true,
    // },
    // sensorId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Sensor',
    //   required: false,
    // },
    // locationId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Location',
    //   required: true,
    // },
    // active: Boolean,
  },
  { versionKey: false },
);
