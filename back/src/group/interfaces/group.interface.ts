import { Document } from 'mongoose';

export interface Group extends Document {
  name: string;
  userId: string;
  type?: string;
}