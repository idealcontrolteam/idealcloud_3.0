import { Schema } from 'mongoose';
export const GroupSchema = new Schema(
  {
    name: String,
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: false,
    },
    type: String,
  },
  { versionKey: false },
);