import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { MeasurementHour } from './interfaces/measurement-hour.interface';
import { CreateMeasurementHourDTO } from './dto/measurement-hour.dto';
import * as moment from 'moment';
import { searchAlarm } from '../hooks/TypesAlarm'
//import { TagService } from '../tag/tag.service';
//import { Tag } from '../tag/interfaces/tag.interface';


@Injectable()
export class MeasurementHourService {
  constructor(
    @InjectModel('MeasurementHour') private MeasurementHourModel: Model<MeasurementHour>,
    //private TagService: TagService,
  ) {}

  async getMeasurementHours(): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find();
    return MeasurementHours;
  }

  async getMeasurementHoursAll(): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel
      .find()
      .populate('tagId sensorId locationId');
    return MeasurementHours;
  }

  async getMeasurementHourByTagfiltered(tagId, fini, ffin): Promise<any[]> {

    const MeasurementHours = await this.MeasurementHourModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    }).sort({dateTime: 1}).lean();

    //   const MeasurementHourOrder = MeasurementHours.forEach((MeasurementHour)=>{
    //     console.log(MeasurementHour.dateTime)
    // })
    //let fechahoy=new Date();

    //console.log(moment(fechahoy).format('YYYY/MM/dd'))
    return MeasurementHours;
  }

  async getAlarmMeasurementHourByTagfiltered(tagId, type, fini, ffin): Promise<any[]> {

    // console.log(type)

    const hours=3

    const MeasurementHours = await this.MeasurementHourModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    }).sort({dateTime: -1});
    
    if(type=='oxd'){
      return [{
        status:searchAlarm(MeasurementHours,'en cero',hours),
        locationId:MeasurementHours.length>0?MeasurementHours[0].locationId:''
      }]
    }else if(type=='oxs'){
      return [{
        status:searchAlarm(MeasurementHours,'pegados',hours),
        locationId:MeasurementHours.length>0?MeasurementHours[0].locationId:''
      }]
    }else if(type=='sal'){
      if(searchAlarm(MeasurementHours,'salinidad 33',hours)!='ok'){
        return [{
          status:searchAlarm(MeasurementHours,'salinidad 33',hours),
          locationId:MeasurementHours.length>0?MeasurementHours[0].locationId:''
        }]
      }else if(searchAlarm(MeasurementHours,'salinidad 34',hours)!='ok'){
        return [{
          status:searchAlarm(MeasurementHours,'salinidad 34',hours),
          locationId:MeasurementHours.length>0?MeasurementHours[0].locationId:''
        }]
      }else if(searchAlarm(MeasurementHours,'en cero',hours)!='ok'){
        return [{
          status:searchAlarm(MeasurementHours,'en cero',hours),
          locationId:MeasurementHours.length>0?MeasurementHours[0].locationId:''
        }]
      }else{
        return [{
          status:searchAlarm(MeasurementHours,'en cero',hours),
          locationId:MeasurementHours.length>0?MeasurementHours[0].locationId:''
        }]
      }
    }
    
    //return _.orderBy(MeasurementHours, ['fecha'],['desc']);
  }

  async getMeasurementHourByTagfilteredXY(tagId, fini, ffin): Promise<any[]> {

    const MeasurementHours = await this.MeasurementHourModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    },['dateTime','value']).sort({dateTime: -1}).lean();

    //  let i = 0;

    //  const ColorChart =["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

     //const tags=await this.TagService.getTagsAll();
      // for (let i = 0; i < MeasurementHours.length; i++) {
      //   let sw =0
      //   tags.forEach((tag)=>{
      //     if(tag._id==MeasurementHours[i]._id)
      //       this.loadDataChart(tag.shortName, APItagMediciones, ColorChart[i],TagsSelecionado[i].unity,sw);
      //   })
      // }

    let xy=[];


     MeasurementHours.map( item => {
		    var fecha=new Date(item.dateTime);
        var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
        zona_horaria=zona_horaria/60;
        fecha.setHours(fecha.getHours()+zona_horaria);

      xy.push({ x: fecha.getTime() , y : item.value });
    });

    return xy;
  }

  async getMeasurementHourByTagfilteredXYActive(tagId, fini, ffin): Promise<any[]> {

    const MeasurementHours = await this.MeasurementHourModel.find({
      tagId: tagId,
      active: true,
      dateTime: { $gte: fini, $lte: ffin },
    }).sort({dateTime: 1});

    //  let i = 0;

    //  const ColorChart =["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

     //const tags=await this.TagService.getTagsAll();
      // for (let i = 0; i < MeasurementHours.length; i++) {
      //   let sw =0
      //   tags.forEach((tag)=>{
      //     if(tag._id==MeasurementHours[i]._id)
      //       this.loadDataChart(tag.shortName, APItagMediciones, ColorChart[i],TagsSelecionado[i].unity,sw);
      //   })
      // }

    let xy=[];


     MeasurementHours.map( item => {
		    var fecha=new Date(item.dateTime);
        var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
        zona_horaria=zona_horaria/60;
        fecha.setHours(fecha.getHours()+zona_horaria);

      xy.push({ x: fecha.getTime() , y : item.value });
    });

    return xy;
  }

  async getMeasurementHourFilteredByTagsAndDate(
    tags,
    fini,
    ffin,
  ): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      tagId: { $in: tags },
      dateTime: { $gte: fini, $lte: ffin },
    });

    //console.log(tags);
    return MeasurementHours;
  }

  async getMeasurementHourBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return MeasurementHours;
  }

  async getMeasurementHourByLocationfiltered(
    locationId,
    fini,
    ffin,
  ): Promise<any[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      locationId: locationId,
      dateTime: { $gte: fini, $lt: ffin }
    }).lean();
    return MeasurementHours;
  }

  async getMeasurementHourByLocationsfiltered(
    locationsId,
    fini,
    ffin,
  ): Promise<any[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      $or: locationsId,
      dateTime: { $gte: fini+".000Z", $lte: ffin+".000Z" }
    }).lean();
    return MeasurementHours;
  }

  async getMeasurementHourByLocationallfiltered(
    locationId,
    fini,
    ffin,
  ): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      locationId: locationId,
      dateTime: { $gte: fini, $lt: ffin },
    });
    return MeasurementHours;
  }

  async getMeasurementHourByLocationfilteredXY(
    locationId,
    fini,
    ffin,
  ): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      locationId: locationId,
      dateTime: { $gte: fini, $lt: ffin },
    }).sort({dateTime: 1});
    
    
    let xy=[];


     MeasurementHours.map( item => {
		    var fecha=new Date(item.dateTime);
        var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
        zona_horaria=zona_horaria/60;
        fecha.setHours(fecha.getHours()+zona_horaria);

      xy.push({ x: fecha.getTime() , y : item.value });
    });

    return xy;
  }

  async getMeasurementHour(id): Promise<MeasurementHour> {
    const MeasurementHour = await this.MeasurementHourModel.findById(id);
    return MeasurementHour;
  }

  async getMeasurementHoursByLocationId(locationId): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      locationId: locationId,
    });
    return MeasurementHours;
  }

  async getMeasurementHoursBySensorId(sensorId): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      sensorId: sensorId,
    });
    return MeasurementHours;
  }

  async getMeasurementHoursByTagId(tagId): Promise<MeasurementHour[]> {
    const MeasurementHours = await this.MeasurementHourModel.find({
      tagId: tagId,
    });
    //console.log(MeasurementHours.dateTime);
    return MeasurementHours;
  }

  async createMeasurementHour(
    createMeasurementHourDTO: CreateMeasurementHourDTO,
  ): Promise<MeasurementHour> {
    const newMeasurementHour = new this.MeasurementHourModel(createMeasurementHourDTO);
    //console.log(newMeasurementHour)
    return await newMeasurementHour.save();
  }

  async deleteMeasurementHour(id): Promise<MeasurementHour> {
    return await this.MeasurementHourModel.findByIdAndDelete(id);
  }

  async updateMeasurementHour(
    id: string,
    body: CreateMeasurementHourDTO,
  ): Promise<MeasurementHour> {
    return await this.MeasurementHourModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
