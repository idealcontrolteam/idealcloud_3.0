import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateMeasurementHourDTO } from './dto/measurement-hour.dto';
import { MeasurementHourService } from './measurement-hour.service';
import { AuthGuard } from 'src/shared/auth.guard';
import { MeasurementHour } from './interfaces/measurement-hour.interface';
import * as moment from 'moment';
import { TagService } from '../tag/tag.service';
// import { LocationService } from '../location/location.service';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';

@Controller('measurement-hour')
// //@UseGuards(new AuthGuard())
export class MeasurementHourController {
  constructor(private MeasurementHourService: MeasurementHourService,
              private tagService: TagService,
              // private locationService: LocationService,
              @InjectConnection() private connection: Connection
  ) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'value' && key != 'dateTime' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post('/multiple')
  async createMeasurementHourMultiple(@Res() res, @Body() body) {
     //this.validateIds(body);
    if(this.connection.readyState===0){
      throw new BadRequestException('Sin Conexión');
    }
    let newMeasurementHour = {};
    body.map(async (MeasurementHour) => { 
      // console.log(MeasurementHour);     
      newMeasurementHour =  await this.MeasurementHourService.createMeasurementHour(MeasurementHour);
         
    });  
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'MeasurementHour created successfully',
      data: newMeasurementHour,
    });
  }

  @Post()
  async createMeasurementHour(@Res() res, @Body() body: CreateMeasurementHourDTO) {
    this.validateIds(body);

    const newMeasurementHour = await this.MeasurementHourService.createMeasurementHour(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'MeasurementHour created successfully',
      data: newMeasurementHour,
    });
  }

  

  @Post('/alarm/tag/:fini/:ffin')
  async getAlarmMeasurementHourByTagfiltered(
    @Res() res,
    @Body() body,
    // @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    try{
      let alarma=[];
      let status_alarma=''
      if(body.tagId.length == 1){
        alarma = await this.MeasurementHourService.getAlarmMeasurementHourByTagfiltered(
          body.tagId[0],
          body.type,
          fini,
          ffin,
        );
        status_alarma=alarma[0].status
      }else{
        let alarma2=[];
        alarma = await this.MeasurementHourService.getAlarmMeasurementHourByTagfiltered(
          body.tagId[0],
          body.type,
          fini,
          ffin,
        );
        alarma2 = await this.MeasurementHourService.getAlarmMeasurementHourByTagfiltered(
          body.tagId[1],
          body.type,
          fini,
          ffin,
        );
        if(alarma[0].status!="sin datos" || alarma2[0].status!="sin datos"){
          status_alarma=[alarma[0].status,alarma2[0].status].includes('pegados')?'pegados':'ok'
        }else{
          status_alarma='sin datos'
        }
      }
      //let data=body.type=="sal"?{'status_sal':alarma}:{'status':alarma}

      //let update_tag= await this.tagService.updateTag(body.locationId,{'status':alarma})
      // let update_location=await this.locationService.updateLocation(body.locationId,data)

      let msg =
        alarma.length == 0
          ? 'MeasurementHours not found'
          : 'MeasurementHours fetched';
  
          //---------------------Aqui
  
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: status_alarma,
        // count: MeasurementHours.length,
      });
    }catch(err){
      throw new BadRequestException('Ops! ocurrio un error');
    }
  }

  @Get()
  async getMeasurementHours(@Res() res) {
    const MeasurementHours = await this.MeasurementHourService.getMeasurementHours();

    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Get('/all')
  async getMeasurementHoursAll(@Res() res) {
    const MeasurementHours = await this.MeasurementHourService.getMeasurementHoursAll();

    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Get('/:MeasurementHourId')
  async getMeasurementHour(@Res() res, @Param('MeasurementHourId') MeasurementHourId) {
    if (!MeasurementHourId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('MeasurementHour id is not a valid ObjectId');
    }

    const MeasurementHour = await this.MeasurementHourService.getMeasurementHour(
      MeasurementHourId,
    );
    if (!MeasurementHour) {
      throw new NotFoundException('MeasurementHour not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'MeasurementHour found',
      data: MeasurementHour,
    });
  }

  @Get('/tag/:tagId/:fini/:ffin')
  async getMeasurementHourByTagfiltered(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByTagfiltered(
      tagId,
      fini,
      ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

        //---------------------Aqui


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Get('/xy/tag/:tagId/:fini/:ffin')
  async getMeasurementHourByTagfilteredXY(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByTagfilteredXY(
      tagId,
      fini,
      ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    let prom="";
    let max="";
    let min="";
    let ultimo_valor="";
    let ultima_fecha="";
    if(MeasurementHours.length!=0){
      prom=((MeasurementHours.reduce((a, b) => +a + +b.y, 0)/MeasurementHours.length)).toFixed(1);
      max=MeasurementHours.reduce((max, b) => Math.max(max, b.y), MeasurementHours[0].y);
      min=MeasurementHours.reduce((min, b) => Math.min(min, b.y), MeasurementHours[0].y);
      ultimo_valor=MeasurementHours[MeasurementHours.length-1].y;
      ultima_fecha=moment(MeasurementHours[MeasurementHours.length-1].x).format('YYYY-MM-DD HH:mm:ss');
    }


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
      prom:prom,
      max:max,
      min:min,
      ultimo_valor:ultimo_valor,
      ultima_fecha:ultima_fecha,
    });
  }

  @Get('/xy/tag/active/:tagId/:fini/:ffin')
  async getMeasurementHourByTagfilteredXYActive(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByTagfilteredXYActive(
      tagId,
      fini,
      ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    let prom="";
    let max="";
    let min="";
    let ultimo_valor="";
    let ultima_fecha="";
    if(MeasurementHours.length!=0){
      prom=((MeasurementHours.reduce((a, b) => +a + +b.y, 0)/MeasurementHours.length)).toFixed(1);
      max=MeasurementHours.reduce((max, b) => Math.max(max, b.y), MeasurementHours[0].y);
      min=MeasurementHours.reduce((min, b) => Math.min(min, b.y), MeasurementHours[0].y);
      ultimo_valor=MeasurementHours[MeasurementHours.length-1].y;
      ultima_fecha=moment(MeasurementHours[MeasurementHours.length-1].x).format('YYYY-MM-DD HH:mm:ss');
    }


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
      prom:prom,
      max:max,
      min:min,
      ultimo_valor:ultimo_valor,
      ultima_fecha:ultima_fecha,
    });
  }



  @Get('/sensor/:sensorId/:fini/:ffin')
  async getMeasurementHourBySensorfiltered(
    @Res() res,
    @Param('sensorId') sensorId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourBySensorfiltered(
      sensorId,
      fini,
      ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Get('/location/:locationId/:fini/:ffin')
  async getMeasurementHourByLocationfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByLocationfiltered(
      locationId,
      fini,
      ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Post('/locations_array')
  async getMeasurementHourByLocationsfiltered(
    @Res() res,
    // @Param('locationId') locationId,
    // @Param('fini') fini,
    // @Param('ffin') ffin,
    @Body() body: any
  ) {

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByLocationsfiltered(
      body.locationsId,
      body.fini,
      body.ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Get('/location_all/:locationId/:fini/:ffin')
  async getMeasurementHourByLocation_allfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByLocationallfiltered(
      locationId,
      fini,
      ffin,
    );
    let oxd=MeasurementHours.filter(m=>m.type=="oxd");
    let oxs=MeasurementHours.filter(m=>m.type=="oxs");
    let temp=MeasurementHours.filter(m=>m.type=="temp");
    let sal=MeasurementHours.filter(m=>m.type=="sal");

    let data=sal.map((o,i)=>{
      return {
        "oxd":oxd!=null?oxd[i].value:[],
        "oxs":oxs!=null?oxs[i].value:[],
        "temp":temp!=null?temp[i].value:[],
        "sal":sal!=null?sal[i].value:[],
        "locationId":o.locationId,
        "dateTime":o.dateTime
      }
    })
    
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: data,
      count: MeasurementHours.length,
    });
  }

  @Get('/xy/location/:locationId/:fini/:ffin')
  async getMeasurementHourByLocationfilteredXY(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const MeasurementHours = await this.MeasurementHourService.getMeasurementHourByLocationfilteredXY(
      locationId,
      fini,
      ffin,
    );
    let msg =
      MeasurementHours.length == 0
        ? 'MeasurementHours not found'
        : 'MeasurementHours fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: MeasurementHours,
      count: MeasurementHours.length,
    });
  }

  @Put('/:MeasurementHourId')
  async updateMeasurementHour(
    @Res() res,
    @Body() body: CreateMeasurementHourDTO,
    @Param('MeasurementHourId') MeasurementHourId,
  ) {
    if (!MeasurementHourId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('MeasurementHour id is not a valid ObjectId');
    }

    this.validateIds(body);

    const updatedMeasurementHour = await this.MeasurementHourService.updateMeasurementHour(
      MeasurementHourId,
      body,
    );
    if (!updatedMeasurementHour) {
      throw new NotFoundException('MeasurementHour not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'MeasurementHour updated',
      data: updatedMeasurementHour,
    });
  }

  @Delete('/:MeasurementHourId')
  async deleteMeasurementHour(@Res() res, @Param('MeasurementHourId') MeasurementHourId) {
    const deletedMeasurementHour = await this.MeasurementHourService.deleteMeasurementHour(
      MeasurementHourId,
    );

    if (!deletedMeasurementHour) {
      throw new NotFoundException('MeasurementHour not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'MeasurementHour deleted',
      data: deletedMeasurementHour,
    });
  }
}
