import { forwardRef, Module } from '@nestjs/common';
import { MeasurementHourController } from './measurement-hour.controller';
import { MeasurementHourService } from './measurement-hour.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementHourSchema } from './schemas/measurement-hour.schema';
// import { LocationService } from 'src/location/location.service';
import { TagModule } from '../tag/tag.module';
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'MeasurementHour',
        schema: MeasurementHourSchema,
        collection: 'measurementHour',
      },
    ]),
    forwardRef(() => TagModule),
  ],
  controllers: [MeasurementHourController],
  providers: [MeasurementHourService,
              //ViewsService
  ],
  exports: [MeasurementHourService],
})
export class MeasurementHourModule {}
