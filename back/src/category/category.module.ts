import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema } from './schemas/category.schema';
import { WorkPlaceModule } from '../work-place/work-place.module';
import { WorkPlaceService } from '../work-place/work-place.service';

@Module({
  imports: [
    WorkPlaceModule,
    MongooseModule.forFeature([
      { name: 'Category', schema: CategorySchema, collection: 'category' },
    ]),
  ],
  controllers: [CategoryController],
  providers: [CategoryService, WorkPlaceService],
})
export class CategoryModule {}
