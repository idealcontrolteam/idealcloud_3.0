import { Schema } from 'mongoose';

export const CategorySchema = new Schema(
  {
    name: String,
    active: Boolean,
  },
  { versionKey: false },
);
