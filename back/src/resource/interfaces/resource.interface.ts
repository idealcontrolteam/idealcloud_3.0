import { Document } from 'mongoose';

export interface Resource extends Document {
  archiveId: string;
  groupId: string;
  active: boolean;
}