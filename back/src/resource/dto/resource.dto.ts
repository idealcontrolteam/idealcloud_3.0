export class CreateResourceDTO {
  archiveId: String;
  groupId: String;
  active: boolean;
}
