import { Schema } from 'mongoose';
export const ResourceSchema = new Schema(
  {
    archiveId: {
      type: Schema.Types.ObjectId,
      ref: 'Archive',
      required: false,
    },
    groupId: {
      type: Schema.Types.ObjectId,
      ref: 'Group',
      required: false,
    },
    active: Boolean,
  },
  { versionKey: false },
);