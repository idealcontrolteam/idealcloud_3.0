import { Module } from '@nestjs/common';
import { TagConfigControlController } from './tag-config-control.controller';
import { TagConfigControlService } from './tag-config-control.service';
import { TagConfigControlSchema } from './schemas/TagConfigControl.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'TagConfigControl',
        schema: TagConfigControlSchema,
        collection: 'tagConfigControl',
      },
    ]),
  ],
  controllers: [TagConfigControlController],
  providers: [TagConfigControlService],
  exports: [TagConfigControlService],
})
export class TagConfigControlModule {}
