import { Schema } from 'mongoose';

export const TagConfigControlSchema = new Schema(
  {
    code: String,
    code_control: String,
    name: String,
    setpoint: Number,
    histeresis: Number,
    timeOn: Number,
    timeOff: Number,
    histeresisMax: Number,
    histeresisMin: Number,
    active: Boolean,
    locationId: {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: false,
    },
  },
  { versionKey: false },
);
