import { Schema } from 'mongoose';

export const RegisterAfterSchema = new Schema(
  {
    fecha_registros: Date,
    code: String,
    //id_dispositivos: String,
    id_dispositivos: String,
    oxd: Number,
    oxs: Number,
    temp: Number,
    sal: Number,
    // value: Number,
    // dateTime: Date,
    // tagId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Tag',
    //   required: true,
    // },
    // sensorId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Sensor',
    //   required: false,
    // },
    // locationId: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'Location',
    //   required: true,
    // },
    // active: Boolean,
  },
  { versionKey: false },
);
