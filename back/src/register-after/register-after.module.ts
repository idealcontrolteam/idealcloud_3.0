import { Module } from '@nestjs/common';
import { RegisterAfterController } from './register-after.controller';
import { RegisterAfterService } from './register-after.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterAfterSchema } from './schemas/register-after.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'RegisterAfter',
        schema: RegisterAfterSchema,
        collection: 'registerAfter',
      },
    ]),
  ],
  controllers: [RegisterAfterController],
  providers: [RegisterAfterService],
})
export class RegisterAfterModule {}
