const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;


const RoleSchema = new Schema({
  name: String,
  active: Boolean,
}, {
  versionKey: false
}, );

const CompanySchema = new Schema({
  name: String,
  logo: String,
  active: Boolean,
}, {
  versionKey: false
}, );

const UserSchema = new Schema({
  name: String,
  email: String,
  username: String,
  password: String,
  roleId: {
    type: Schema.Types.ObjectId,
    ref: 'Role',
    required: true,
  },
  companyId: {
    type: Schema.Types.ObjectId,
    ref: 'Company',
    required: true,
  },
  active: Boolean,
  lastConnection: {
    type: Date,
    default: null,
  },
}, {
  versionKey: false
}, );

let idRole;
let idCompany;

/*
    Configurar nombre de la base de datos
*/
mongoose
  .connect("mongodb://localhost:27017/bdf4f",{ useNewUrlParser: true })
  .then((mongodb) => {

      mongodb.model('role', RoleSchema, 'role').create({
        name: "Admin",
        active: true
      })
      .then(newRole => {
        idRole = newRole._id.toString();
        console.log('[NEW ROLE CREATED]', newRole.name);
        /*
          Configurar datos de la compañia
        */
        return mongodb.model('company', CompanySchema, 'company').create({
          name: 'Company Test',
          logo: 'Logo Test',
          active: true,
        });
      })
      .then(newCompany => {
        idCompany = newCompany._id.toString();
        console.log('[NEW COMPANY CREATED]', newCompany.name);
        /*
          Configurar contraseña
        */
        return bcrypt.hash('123456', 10);
      })
      .then(hashedPassword => {
        /*
          Configurar datos usuario
        */
        return mongodb.model('user', UserSchema, 'user').create({
          name: 'User Admin',
          email: 'admin@admin.cl',
          username: 'Administrator',
          password: hashedPassword,
          roleId: idRole,
          companyId: idCompany,
          active: true,
          lastConnection: Date.now()
        });
      })
      .then(newUser => {
        console.log('[NEW USER CREATED]', newUser.name);
        console.log('Username: ' + newUser.email);
        console.log('Password: 123456');
      })
      .catch(err => {
        console.log(err);
      })

  })
  .catch(err => {
    console.log(err);
  });