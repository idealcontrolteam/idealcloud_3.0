import { Module } from '@nestjs/common';
import { MeasurementCopyController } from './local_control.controller';
import { MeasurementCopyService } from './local_control.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementCopySchema } from './schemas/local_control.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Local_Control',
        schema: MeasurementCopySchema,
        collection: 'local_control',
      },
    ]),
  ],
  controllers: [MeasurementCopyController],
  providers: [MeasurementCopyService],
})
export class MeasurementCopyModule {}
