import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { MeasurementCopy } from './interfaces/local_control.interface';
import { CreateMeasurementCopyDTO } from './dto/local_control.dto';

@Injectable()
export class MeasurementCopyService {
  constructor(
    @InjectModel('Local_Control')
    private measurementCopyModel: Model<MeasurementCopy>,
  ) {}

  async getMeasurements(): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    locationId,
    fini,
    ffin,
  ): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
      dateTime: { $gte: fini, $lt: ffin },
    });
    return measurements;
  }

  async getMeasurement(id): Promise<MeasurementCopy> {
    const measurement = await this.measurementCopyModel.findById(id);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<MeasurementCopy[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createMeasurementCopyDTO: CreateMeasurementCopyDTO,
  ): Promise<MeasurementCopy> {
    const newMeasurement = new this.measurementCopyModel(
      createMeasurementCopyDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<MeasurementCopy> {
    return await this.measurementCopyModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateMeasurementCopyDTO,
  ): Promise<MeasurementCopy> {
    return await this.measurementCopyModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
