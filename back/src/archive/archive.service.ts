import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateArchiveDTO } from './dto/archive.dto';
import { Archive } from './interfaces/archive.interface';

@Injectable()
export class ArchiveService {
    constructor(@InjectModel('Archive') private activityModel: Model<Archive>) {}
    
      async getArchiveAll(): Promise<Archive[]> {
        const activitys = await this.activityModel.find();
        return activitys;
      }

      async getArchiveCertificados(): Promise<Archive[]> {
        const activitys = await this.activityModel.find({type:"certificado"});
        return activitys;
      }
      
      async getArchivesManuales(): Promise<Archive[]> {
        const activitys = await this.activityModel.find({type:"manual"});
        return activitys;
      }

      async getArchivesVideos(): Promise<Archive[]> {
        const activitys = await this.activityModel.find({type:"video"});
        return activitys;
      }
    
    //   async getAlarmsByTagId(tagId): Promise<Archive[]> {
    //     const activitys = await this.activityModel.find({ tagId: tagId });
    //     return activitys;
    //   }
    
      async getArchive(id): Promise<Archive> {
        const activity = await this.activityModel.findById(id);
        return activity;
      }
    
      async createArchive(createArchiveDTO: CreateArchiveDTO): Promise<Archive> {
        const newArchive = new this.activityModel(createArchiveDTO);
        return await newArchive.save();
      }
    
      async deleteArchive(id): Promise<any> {
        return await this.activityModel.findByIdAndDelete(id);
      }
    
      async updateArchive(id: string, body: CreateArchiveDTO): Promise<Archive> {
        return await this.activityModel.findByIdAndUpdate(id, body, {
          new: true,
        });
      }
    
    //   async getAlarmByfilteredDate(   
    //     fini,
    //     ffin,
    //   ): Promise<Archive[]> {
    //     const alarms = await this.activityModel.find({      
    //       dateTimeIni: { $gte: fini, $lte: ffin },
    //     });
    //     return alarms;
    //   }
}
