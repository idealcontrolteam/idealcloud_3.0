import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
    UseInterceptors,
    UploadedFile,
    Req,
  } from '@nestjs/common';
import { ArchiveService } from './archive.service';
import { CreateArchiveDTO } from './dto/archive.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { createWriteStream,unlinkSync,statSync,createReadStream } from 'fs';
import { PATH_METADATA } from '@nestjs/common/constants';
import { MetadataStorage } from 'class-validator';

@Controller('archive')
export class ArchiveController {
    constructor(private archiveService: ArchiveService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async createArchive(@Res() res, @Body() body: CreateArchiveDTO, @UploadedFile() file) {
    try{
        let name="";
        if(body.url=="" || body.url==null){
          //console.log(file);
          name=String(file.originalname);
          name=name.replace(' ','_');

          var date = new Date;
          var seconds = String(date.getSeconds());
          var minutes = String(date.getMinutes());
          var hour = String(date.getHours());
          var day = String(date.getDate());
          name=day+hour+minutes+seconds+name;
          //console.log(name)
          
          let path="";
          //let raiz="/IdealControl/idealcloud_3.0/front/doc";
          let raiz="/var/www/idealcontrol_cloud/ideal_cloud3/front/doc";
          if(body.type=="certificado")
              path = raiz+"/certificados/"+name;
          else if(body.type=="video")
              path = raiz+"/videos/"+file.originalname;
          else if(body.type=="manual")
              path = raiz+"/manuales/"+file.originalname;
          else
              path = raiz+"/otros/"+file.originalname;
          
          body.ruta=path;

          let fileStream=createWriteStream(path);
          fileStream.write(file.buffer);
          fileStream.end();
        }
        const newArchive = await this.archiveService.createArchive(body);
    
        return res.status(HttpStatus.CREATED).json({
          statusCode: HttpStatus.CREATED,
          message: 'Archive created successfully',
          data: newArchive,
        });
    }catch(e){
        throw new NotFoundException('Archive no tiene todos la data necesaria');
    }
    
  }

//   @Post('/certificado')
//   @UseInterceptors(FileInterceptor('file'))
//   async uploadFile(@UploadedFile() file,@Body() body: CreateArchiveDTO) {
//     console.log(file);
//     console.log(body)
//     //console.log(body);
//     //const path = "/var/www/idealcontrol_cloud/ideal_cloud3/document/certificados"+file.originalname;
//     const path = "/"+file.originalname;
//     let fileStream=createWriteStream(path);
//     fileStream.write(file.buffer);
//     fileStream.end();
//     return path;
//   }

  @Get('/descarga/:archiveId')
  async download(@Param('archiveId') archiveId,
  @Res() res){
    //console.log(archiveId)
    const archive = await this.archiveService.getArchive(archiveId);
    //console.log(archive.ruta)
    return res.download(archive.ruta)
  }

//   @Get()
//   async getArchives(@Res() res) {
//     const archives = await this.archiveService.getArchiveAll();

//     let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: archives,
//       count: archives.length,
//     });
//   }
//   @Get('/:fini/:ffin')
//   async getArchiveByfilteredDate(
//     @Res() res,   
//     @Param('fini') fini,
//     @Param('ffin') ffin,
//   ) {  
//     const archives = await this.archiveService.getArchiveByfilteredDate(     
//       fini,
//       ffin,
//     );
//     let msg =
//     archives.length == 0
//         ? 'archives not found'
//         : 'archives fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: archives,
//       count: archives.length,
//     });
//   }

  @Get('/all')
  async getArchivesAll(@Res() res) {
    const archives = await this.archiveService.getArchiveAll();

    let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: archives,
      count: archives.length,
    });
  }

  @Get('/certificados')
  async getArchivesCertificados(@Res() res) {
    const archives = await this.archiveService.getArchiveCertificados();

    let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: archives,
      count: archives.length,
    });
  }

  @Get('/manuales')
  async getArchivesManuales(@Res() res) {
    const archives = await this.archiveService.getArchivesManuales();

    let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: archives,
      count: archives.length,
    });
  }

  @Get('/videos')
  async getArchivesVideos(@Res() res) {
    const archives = await this.archiveService.getArchivesVideos();

    let msg = archives.length == 0 ? 'Archives not found' : 'Archives fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: archives,
      count: archives.length,
    });
  }

  @Get('/videos_streaming')
  async getVideosStreaming(@Req() req,@Res() res) {
    const range = req.headers.range;
    if (!range) {
      res.status(400).send("Requires Range header");
      return;
    }
    const videoPath = '/idealcontrol/idealcloud_3.0/back_/videos/GraphQL.mp4';
    const videoSize = statSync(videoPath).size;

    const chunkSize = 1 * 1e+6;
    const start = Number(range.replace(/\D/g, ''));
    const end = Math.min(start + chunkSize, videoSize -1);

    const contentLength = end - start + 1;

    const headers = {
        "Content-Range": `bytes ${start}-${end}/${videoSize}`,
        "Accept-Ranges": "bytes",
        "Content-Length": contentLength,
        "Content-Type": "video/mp4"
    }
    res.writeHead(206, headers);

    const stream = createReadStream(videoPath, { start, end })
    stream.pipe(res);
  }

  @Get('/:archiveId')
  async getArchive(
    @Res() res, 
    @Param('archiveId') archiveId
    ) {
    if (!archiveId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Archive id is not a valid  ObjectId');
    }

    const archive = await this.archiveService.getArchive(archiveId);
    if (!archive) {
      throw new NotFoundException('Archive not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Archive found',
      data: archive,
    });
  }

  @Put('/:archiveId')
  async updateArchive(
    @Res() res,
    @Body() body: CreateArchiveDTO,
    @Param('archiveId') archiveId,
  ) {
    if (!archiveId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Archive id is not a valid  ObjectId');
    }
    const updatedArchive = await this.archiveService.updateArchive(archiveId, body);
    if (!updatedArchive) {
      throw new NotFoundException('Archive not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Archive updated',
      data: updatedArchive,
    });
  }

  @Delete('/:archiveId')
  async deleteArchive(@Res() res, @Param('archiveId') archiveId) {
    const archive = await this.archiveService.getArchive(archiveId);
    //console.log(archive.ruta)
    const deletedArchive = await this.archiveService.deleteArchive(archiveId);

    if(deletedArchive!=null && archive.url==null && archive.ruta!=null){
        const path=archive.ruta;
        unlinkSync(path);
        return res.status(HttpStatus.CREATED).json({
            statusCode: HttpStatus.CREATED,
            message: 'El Archivo fue eliminado'
          });
    }

    if (!deletedArchive) {
      throw new NotFoundException('Archive not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Archive deleted',
      data: deletedArchive,
    });
  }
}
