import { Schema } from 'mongoose';
export const ArchiveSchema = new Schema(
  {
    name: String,
    active: Boolean,
    url: String,
    ruta: String,
    type: String,
    workplaceId: {
      type: Schema.Types.ObjectId,
      ref: 'WorkPlace',
      required: false,
    },
  },

  { versionKey: false },
);

