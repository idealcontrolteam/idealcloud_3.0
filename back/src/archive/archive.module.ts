import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArchiveController } from './archive.controller';
import { ArchiveService } from './archive.service';
import { ArchiveSchema } from './schemas/archive.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
          { name: 'Archive', schema: ArchiveSchema, collection: 'archive' },
        ]),
      ],
      controllers: [ArchiveController],
      providers: [ArchiveService]
})
export class ArchiveModule {}
