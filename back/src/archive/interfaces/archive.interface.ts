import { Document } from 'mongoose';

export interface Archive extends Document {
  name: string;
  active: boolean;
  url?: string;
  ruta?: string;
  type: string;
  workplaceId?: string;
}