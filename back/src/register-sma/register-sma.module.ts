import { Module } from '@nestjs/common';
import { RegisterSMAController } from './register-sma.controller';
import { RegisterSMAService } from './register-sma.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterSMASchema } from './schemas/register-sma.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Registros',
        schema: RegisterSMASchema,
        collection: 'registros',
      },
    ]),
  ],
  controllers: [RegisterSMAController],
  providers: [RegisterSMAService],
})
export class RegisterSMAModule {}
