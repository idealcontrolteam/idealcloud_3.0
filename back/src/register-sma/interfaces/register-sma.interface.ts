import { Document } from 'mongoose';

export interface RegisterSMA extends Document {
  dateTime: string;
  dispositivoId: string;
  parametros: any;
  respuesta?: any;
  procesoId?: string;
  // fecha_registros: Date;
  // code: string;
  // id_dispositivos: string;
  // oxd: number;
  // oxs: number;
  // temp: number;
  // sal: number;
}
