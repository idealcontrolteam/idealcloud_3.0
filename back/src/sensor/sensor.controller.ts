import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateSensorDTO } from './dto/sensor.dto';
import { SensorService } from './sensor.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('sensor')
//@UseGuards(new AuthGuard())
export class SensorController {
  constructor(private sensorService: SensorService) {}

  @Post()
  async createSensor(@Res() res, @Body() createSensorDTO: CreateSensorDTO) {
    const sensor = await this.sensorService.createSensor(createSensorDTO);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Sensor created successfully',
      data: sensor,
    });
  }

  @Get()
  async getSensors(@Res() res) {
    const sensors = await this.sensorService.getSensors();

    let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: sensors,
      count: sensors.length,
    });
  }

  @Get('/all')
  async getSensorsAll(@Res() res) {
    const sensors = await this.sensorService.getSensorsAll();

    let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: sensors,
      count: sensors.length,
    });
  }

  @Get('/:sensorId/measurement')
  async getMeasurementsBySensorId(@Res() res, @Param('sensorId') sensorId) {
    const measurements = await this.sensorService.getMeasurementsBySensorId(
      sensorId,
    );

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:sensorId/tag')
  async getTagsBySensorId(@Res() res, @Param('sensorId') sensorId) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const tags = await this.sensorService.getTagsBySensorId(sensorId);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/:sensorId')
  async getBrand(@Res() res, @Param('sensorId') sensorId) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const sensor = await this.sensorService.getSensor(sensorId);
    if (!sensor) {
      throw new NotFoundException('Sensor not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Sensor found',
      data: sensor,
    });
  }

  @Put('/:sensorId')
  async updateSensor(
    @Res() res,
    @Body() body: CreateSensorDTO,
    @Param('sensorId') sensorId,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }
    const updatedSensor = await this.sensorService.updateSensor(sensorId, body);
    if (!updatedSensor) {
      throw new NotFoundException('Sensor not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Sensor updated successfully',
      data: updatedSensor,
    });
  }

  @Delete('/:sensorId')
  async deleteSensor(@Res() res, @Param('sensorId') sensorId) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const sensor = await this.sensorService.deleteSensor(sensorId);
    if (!sensor) {
      throw new NotFoundException('Sensor not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Sensor Deleted',
      data: sensor,
    });
  }
}
