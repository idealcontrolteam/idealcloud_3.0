import { Schema } from 'mongoose';

export const SensorSchema = new Schema(
  {
    name: String,
    serialNumber: String,
    active: Boolean,
    locationId: {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: true,
    },
    shortName: String,
    description: String,
  },
  { versionKey: false },
);
