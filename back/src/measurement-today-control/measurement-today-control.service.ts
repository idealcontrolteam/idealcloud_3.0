import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { RegisterToday } from './interfaces/register-today.interface';
import { CreateRegisterTodayDTO } from './dto/register-today.dto';
import { AnyMxRecord } from 'dns';
import {concat, filter, orderBy} from 'lodash';
// import { TagService } from '../tag/tag.service';
import * as moment from 'moment';

@Injectable()
export class MeasurementTodayControlService {
  constructor(
    @InjectModel('MeasurementTodayControl')
    private measurementCopyModel: Model<RegisterToday>,
    // private tagService: TagService,
  ) {}

  async getMeasurements(): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    id_dispositivos,
    fini,
    ffin,
  ): Promise<any[]> {
    let measurements = await this.measurementCopyModel.find({
      id_dispositivos: id_dispositivos,
      fecha_registros: { $gte: fini, $lt: ffin },
    }).sort({fecha_registros: 1});
    return measurements.map(d=>{
      var fecha=new Date(d.fecha_registros);
      var zona_horaria=new Date(d.fecha_registros).getTimezoneOffset();
      zona_horaria=zona_horaria/60;
      let f=fecha.setHours(fecha.getHours()+zona_horaria);
      return {
        "_id": d._id,
        "code": d.code,
        "temp": d.temp,
        "sal": d.sal,
        "oxs": d.oxs,
        "oxd": d.oxd,
        "bat":d.bat==null?0:d.bat,
        "alarma_min":d.alarma_min==null?0:d.alarma_min,
        "fecha_registros": f,
        "id_dispositivos": d.id_dispositivos
      }
    });
  }

  async getMeasurement(id): Promise<RegisterToday> {
    const measurement = await this.measurementCopyModel.findById(id);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async getMeasurementEfectivity(data):Promise<any[]>{
    const measurements = await this.measurementCopyModel.find(data).sort({fecha_registros: 1});;
    return measurements;
  }

  // async getEfectividadesToday(locations): Promise<any> {
  //   let now=new Date();
  //   let f1=moment(now).subtract(1, 'days').format('YYYY-MM-DDT12:00:00') + ".000Z";
  //   let f2=moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
  //   let minutos_dia= moment(f1).diff(moment(f2), 'minutes')*-1;
  //   let registros_100=minutos_dia/5;
  //   let ids_location=[];
  //   let _id_location=[];
  //   locations.map(t=>{
  //     ids_location.push({id_dispositivos:t});
  //     _id_location.push({_id:t})
  //   })
  //   let new_measurements=[];
    
  //   const measurements = await this.measurementCopyModel.find({
  //     $or:ids_location,
  //     dateTime: { $gte: f1, $lte: f2 },
  //     value: { $ne: 0 },
  //   }).sort({dateTime: 1});

  //   // const find_tags = await this.tagService.getAnyTagsOxd(_id_location);

  //   // locations.map(t=>{
  //   //   let med=measurements.filter(m=>m.id_dispositivos==t);
  //   //   let medicion=measurements.filter(m=>m.id_dispositivos==t).length;
  //   //   // let tag=find_tags.filter(m=>m.locationId==t)[0]
  //   //   medicion>0?
  //   //   new_measurements.push({
  //   //     my_register:medicion,
  //   //     register_100:registros_100,
  //   //     porcentaje_efectividad:medicion*100/registros_100,
  //   //     _id:tag._id,
  //   //     locationId:med[0].id_dispositivos!=undefined?med[0].id_dispositivos:"",
  //   //     workplaceId:tag.workplaceId
  //   //   }):new_measurements.push({
  //   //     my_register:medicion,
  //   //     register_100:registros_100,
  //   //     porcentaje_efectividad:medicion*100/registros_100,
  //   //     _id:tag._id,
  //   //     workplaceId:tag.workplaceId,
  //   //   })
  //   // })

  //   return new_measurements;
    
  // }

  async createMeasurement(
    createRegisterTodayDTO: CreateRegisterTodayDTO,
  ): Promise<RegisterToday> {
    const newMeasurement = new this.measurementCopyModel(
      createRegisterTodayDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<RegisterToday> {
    return await this.measurementCopyModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateRegisterTodayDTO,
  ): Promise<RegisterToday> {
    return await this.measurementCopyModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
