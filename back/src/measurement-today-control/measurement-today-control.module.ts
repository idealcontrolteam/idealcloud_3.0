import { Module } from '@nestjs/common';
import { MeasurementTodayControlController } from './measurement-today-control.controller';
import { MeasurementTodayControlService } from './measurement-today-control.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterTodaySchema } from './schemas/register-today.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'MeasurementTodayControl',
        schema: RegisterTodaySchema,
        collection: 'measurementTodayControl',
      },
    ]),
  ],
  controllers: [MeasurementTodayControlController],
  providers: [MeasurementTodayControlService],
})
export class MeasurementTodayControlModule {}
