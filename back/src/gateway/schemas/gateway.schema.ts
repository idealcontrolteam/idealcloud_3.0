import { Schema } from 'mongoose';

export const GatewaySchema = new Schema(
  {
    name: String,
    description: String,
    active: Boolean,
    workPlaceId: {
      type: Schema.Types.ObjectId,
      ref: 'WorkPlace',
      required: true,
    },
  },
  { versionKey: false },
);
