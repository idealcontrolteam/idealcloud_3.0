import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { CreateCompanyDTO } from './dto/company.dto';
import { CompanyService } from './company.service';
import { AuthGuard } from 'src/shared/auth.guard';
import { createWriteStream,unlinkSync } from 'fs';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('company')
//@UseGuards(new AuthGuard())
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async createCompany(@Res() res, @Body() body: CreateCompanyDTO, @UploadedFile() file) {
    let name="";
        if(body.logo=="" || body.logo==null){
          //console.log(file);
          name=String(file.originalname);
          //name=name.replace(' ','_');
          let rename=name.split('.');

          // var date = new Date;
          // var seconds = String(date.getSeconds());
          // var minutes = String(date.getMinutes());
          // var hour = String(date.getHours());
          // var day = String(date.getDate());
          // name=day+hour+minutes+seconds+name;
          // let rename=file.originalname.split('.')
          
          let path="";
          //let raiz="/IdealControl/idealcloud_3.0/front/doc";
          let raiz="/var/www/idealcontrol_cloud/ideal_cloud3/front/doc";
          path = raiz+"/imagenes/"+file.originalname;
          //path = raiz+"/imagenes/"+body.name+"."+rename[1];
          body.logo=path;

          let fileStream=createWriteStream(path);
          fileStream.write(file.buffer);
          fileStream.end();
    }
    const newCompany = await this.companyService.createCompany(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Company created successfully',
      data: newCompany,
    });
  }

  @Get()
  async getCompanies(@Res() res) {
    const companies = await this.companyService.getCompanies();

    let msg =
      companies.length == 0 ? 'Companies not found' : 'Companies fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: companies,
      count: companies.length,
    });
  }

  @Get('/:companyId/workPlace')
  async getWorkPlacesByCompanyId(@Res() res, @Param('companyId') companyId) {
    const workplaces = await this.companyService.getWorkPlacesByCompanyId(
      companyId,
    );

    let msg =
      workplaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workplaces,
      count: workplaces.length,
    });
  }

  @Get('/:companyId/user')
  async getUsersByCompanyId(@Res() res, @Param('companyId') companyId) {
    const users = await this.companyService.getUsersByCompanyId(companyId);

    let msg = users.length == 0 ? 'Users not found' : 'Users fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: users,
      count: users.length,
    });
  }

  @Get('/:name/name')
  async getNameCompanyId(@Res() res, @Param('name') name) {
    const users = await this.companyService.getNameCompanyId(name);

    let msg = users.length == 0 ? 'Users not found' : 'Users fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: users,
      count: users.length,
    });
  }

  @Get('/:companyId')
  async getCompany(@Res() res, @Param('companyId') companyId) {
    if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Company id is not a valid ObjectId');
    }
    const company = await this.companyService.getCompany(companyId);
    if (!company) {
      throw new NotFoundException('Company not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Company found',
      data: company,
    });
  }

  @Put('/:companyId')
  async updateCompany(
    @Res() res,
    @Body() body: CreateCompanyDTO,
    @Param('companyId') companyId,
  ) {
    if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Company id is not a valid ObjectId');
    }
    const updatedCompany = await this.companyService.updateCompany(
      companyId,
      body,
    );
    if (!updatedCompany) {
      throw new NotFoundException('Company not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Company updated successfully',
      data: updatedCompany,
    });
  }

  @Delete('/:companyId')
  async deleteCompany(@Res() res, @Param('companyId') companyId) {
    // if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
    //   throw new BadRequestException('Company id is not a valid ObjectId');
    // }
    const company = await this.companyService.getCompany(companyId);
    const delete_company = await this.companyService.deleteCompany(companyId);
    console.log(company)
    if(delete_company!=null && company.logo!=null){
        const path=company.logo;
        unlinkSync(path);
        return res.status(HttpStatus.CREATED).json({
            statusCode: HttpStatus.CREATED,
            message: 'El Archivo fue eliminado'
          });
    }
    if (!delete_company) {
      throw new NotFoundException('Company not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Company Deleted',
      data: delete_company,
    });
  }
}
