export class CreateCompanyDTO {
  name: string;
  logo: string;
  active: boolean;
  alias: string;
}
