import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { SerialConnection } from './interfaces/serialConnection.interface';
import { CreateSerialConnectionDTO } from './dto/serialConnection.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';

@Injectable()
export class SerialConnectionService {
  constructor(
    @InjectModel('SerialConnection')
    private serialConnectionModel: Model<SerialConnection>,
    private deviceService: DeviceService,
  ) {}

  async getSerialConnections(): Promise<SerialConnection[]> {
    const serialConnections = await this.serialConnectionModel.find();
    return serialConnections;
  }

  async getDevicesBySerialConnectionId(serialConnectionId): Promise<Device[]> {
    const devices = await this.deviceService.getDevicesBySerialConnectionId(
      serialConnectionId,
    );
    return devices;
  }

  async getSerialConnection(id): Promise<SerialConnection> {
    const serialConnections = await this.serialConnectionModel.findById(id);
    return serialConnections;
  }

  async createSerialConnection(
    createSerialConnectionDTO: CreateSerialConnectionDTO,
  ): Promise<SerialConnection> {
    const newSerialConnection = new this.serialConnectionModel(
      createSerialConnectionDTO,
    );
    return await newSerialConnection.save();
  }

  async deleteSerialConnection(id): Promise<SerialConnection> {
    return await this.serialConnectionModel.findByIdAndDelete(id);
  }

  async updateSerialConnection(
    id: string,
    body: CreateSerialConnectionDTO,
  ): Promise<SerialConnection> {
    return await this.serialConnectionModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
