export class CreateRoleDTO {
  code?: string;
  name: string;
  active: boolean;
}
