import { Schema } from 'mongoose';

export const RoleSchema = new Schema(
  {
    code: String,
    name: String,
    active: Boolean,
  },
  { versionKey: false },
);
