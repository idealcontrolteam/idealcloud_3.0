import { Document } from 'mongoose';

export interface Role extends Document {
  code?: string;
  name: string;
  active: boolean;
}
