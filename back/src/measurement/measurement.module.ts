import { forwardRef, Module } from '@nestjs/common';
import { MeasurementController } from './measurement.controller';
import { MeasurementService } from './measurement.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementHourModule } from '../measurement-hour/measurement-hour.module'
import { MeasurementSchema } from './schemas/measurement.schema';
// import { LocationService } from 'src/location/location.service';
import { TagModule } from '../tag/tag.module';
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'Measurement',
        schema: MeasurementSchema,
        collection: 'measurement',
      },
    ]),
    forwardRef(() => TagModule),
    forwardRef(() => MeasurementHourModule)
  ],
  controllers: [MeasurementController],
  providers: [MeasurementService,
              //ViewsService
  ],
  exports: [MeasurementService],
})
export class MeasurementModule {}
