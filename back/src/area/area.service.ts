import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as AreaI from './interfaces/area.interface';
import { CreateAreaDTO } from './dto/area.dto';

@Injectable()
export class AreaService {
    constructor(
        @InjectModel('Area') private areaModel: Model<AreaI.Area>
      ){}

    async createArea(createCyclesDTO: CreateAreaDTO): Promise<AreaI.Area> {
       const newCycles = new this.areaModel(createCyclesDTO);
       return await newCycles.save();
    }

    async getAreas(): Promise<AreaI.Area[]> {
        const areas = await this.areaModel.find();
        return areas;
    }
}
