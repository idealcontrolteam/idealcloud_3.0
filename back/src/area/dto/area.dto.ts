export class CreateAreaDTO {
  name: String;
  active: Boolean;
}
