import { Module } from '@nestjs/common';
import { AreaService } from './area.service';
import { AreaController } from './area.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { AreaSchema } from './schemas/area.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Area', schema: AreaSchema, collection: 'area' },
    ]),
  ],
  controllers: [AreaController],
  providers: [AreaService],
  exports: [AreaService],
})
export class AreaModule {}
