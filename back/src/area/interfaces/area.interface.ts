import { Document } from 'mongoose';

export interface Area extends Document {
  name: string;
  active: boolean;
}
