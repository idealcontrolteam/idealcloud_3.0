import { Schema } from 'mongoose';

export const AreaSchema = new Schema(
  {
    name: String,
    active: Boolean,
  }
);
