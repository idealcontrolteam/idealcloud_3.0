import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { CreateAreaDTO } from './dto/area.dto';
  import { AreaService } from './area.service';
  import { AuthGuard } from 'src/shared/auth.guard';

@Controller('area')
export class AreaController {
   constructor(
    private areaService: AreaService
  ) {}

  @Post()
  async createArea(@Res() res, @Body() body: CreateAreaDTO) {
    //console.log(body)
    const newCycles = await this.areaService.createArea(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Cycles created successfully',
      data: newCycles,
    });
  }

  @Get()
  async getModels(@Res() res) {
    const models = await this.areaService.getAreas();

    let msg = models.length == 0 ? 'Areas not found' : 'Areas fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: models,
      count: models.length,
    });
  }
}
