import { Module } from '@nestjs/common';
import { RegisterTodayController } from './register-today.controller';
import { RegisterTodayService } from './register-today.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterTodaySchema } from './schemas/register-today.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'RegisterToday',
        schema: RegisterTodaySchema,
        collection: 'registerToday',
      },
    ]),
  ],
  controllers: [RegisterTodayController],
  providers: [RegisterTodayService],
})
export class RegisterTodayModule {}
