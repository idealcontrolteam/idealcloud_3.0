import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { RegisterToday } from './interfaces/register-today.interface';
import { CreateRegisterTodayDTO } from './dto/register-today.dto';
import { AnyMxRecord } from 'dns';

@Injectable()
export class RegisterTodayService {
  constructor(
    @InjectModel('RegisterToday')
    private measurementCopyModel: Model<RegisterToday>,
  ) {}

  async getMeasurements(): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    id_dispositivos,
    fini,
    ffin,
  ): Promise<any[]> {
    let measurements = await this.measurementCopyModel.find({
      id_dispositivos: id_dispositivos,
      fecha_registros: { $gte: fini, $lt: ffin },
    }).lean();
    return measurements.map(d=>{
      var fecha=new Date(d.fecha_registros);
      var zona_horaria=new Date(d.fecha_registros).getTimezoneOffset();
      zona_horaria=zona_horaria/60;
      let f=fecha.setHours(fecha.getHours()+zona_horaria+2);
      return {
        "_id": d._id,
        "code": d.code,
        "temp": d.temp,
        "sal": d.sal,
        "oxs": d.oxs,
        "oxd": d.oxd,
        "fecha_registros": f,
        "id_dispositivos": d.id_dispositivos
      }
    });
  }

  async getMeasurement(id): Promise<RegisterToday> {
    const measurement = await this.measurementCopyModel.findById(id);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<RegisterToday[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createRegisterTodayDTO: CreateRegisterTodayDTO,
  ): Promise<RegisterToday> {
    const newMeasurement = new this.measurementCopyModel(
      createRegisterTodayDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<RegisterToday> {
    return await this.measurementCopyModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateRegisterTodayDTO,
  ): Promise<RegisterToday> {
    return await this.measurementCopyModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
