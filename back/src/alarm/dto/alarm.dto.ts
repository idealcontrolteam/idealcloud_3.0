export class CreateAlarmDTO {
  code:string;
  endDate: Date;
  dateTimeIni: Date;
  dateTimeTer: Date;
  dateTimeSoport: Date;
  detail: string;
  company: string;
  workplaceId:string;
  locationId?: string;
  active: boolean;
  alarmValue?: number;
  sentEmail: boolean;
  status: string;
  type: string;
  divice: string;
  // presentValue: number;
}

// "workplaceId"
// "datetime"
// "type"
// "endDate"
// "view"