import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { AlarmService } from './alarm.service';
import { CreateAlarmDTO } from './dto/alarm.dto';
import { AuthGuard } from 'src/shared/auth.guard';
// import { NodeMailgun } from 'ts-mailgun';

@Controller('alarm')
//@UseGuards(new AuthGuard())
export class AlarmController {
  constructor(private alarmService: AlarmService) {}

  @Post()
  async createAlarm(@Res() res, @Body() body: CreateAlarmDTO) {
    const newAlarm = await this.alarmService.createAlarm(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Alarm created successfully',
      data: newAlarm,
    });
  }

  // @Post('/sendEmailSuport/sma')
  // async sendAlarmSMA(@Res() res, @Body() body: any) {
  //   const mailer = new NodeMailgun();
  //   mailer.apiKey = '68e27ee8ad17d11f897cb8174ba34597-3e51f8d2-c2cc172d'; // Set your API key
  //   mailer.domain = 'https://api.mailgun.net/v3/mailgun.idealcontrol.cl/messages'; // Set the domain you registered earlier
  //   mailer.fromEmail = 'alarmas@idealcontrol.cl'; // Set your from email
  //   mailer.fromTitle = 'My Sample App'; // Set the name you would like to send from

  //   mailer.options = {
  //     host: 'https://api.mailgun.net/v3/mailgun.idealcontrol.cl/messages'
  //   };
    

  //   mailer.init();

  //   // Send an email to test@example.com
  //   mailer
  //       .send('jordyp60@gmail.com', 'Hello!', '<h1>hsdf</h1>')
  //       .then((result) => console.log('Done', result))
  //       .catch((error) => console.error('Error: ', error));
  //   // let auth=["api", "68e27ee8ad17d11f897cb8174ba34597-3e51f8d2-c2cc172d"]
  //   // //const newAlarm = await this.alarmService.createAlarm(body);
  //   // return await axios.post('https://api.mailgun.net/v3/mailgun.idealcontrol.cl/messages',
  //   //   //auth,
  //   //   body,
  //   //   {
  //   //     headers: {
  //   //       'Content-Type': 'application/json',
  //   //     },
  //   //   }).then((res) => {
  //   //       return res.data;
  //   // });
  //   // return res.status(HttpStatus.CREATED).json({
  //   //   statusCode: HttpStatus.CREATED,
  //   //   message: 'La alarma se envio correctamnete',
  //   //   //data: newAlarm,
  //   // });
  // }


  @Get()
  async getAlarms(@Res() res) {
    const alarms = await this.alarmService.getAlarms();

    let msg = alarms.length == 0 ? 'Alarms not found' : 'Alarms fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: alarms,
      count: alarms.length,
    });
  }

  @Get('/tendencia/:fini/:ffin')
  async getAlarmByfilteredDate(
    @Res() res,   
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {  
    const alarms = await this.alarmService.getAlarmByfilteredDate(     
      fini,
      ffin,
    );
    let msg =
    alarms.length == 0
        ? 'alarms not found'
        : 'alarms fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: alarms,
      count: alarms.length,
    });
  }

  @Get('/tendencia/:fini/:ffin/active')
  async getAlarmByfilteredDateActive(
    @Res() res,   
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {  
    const alarms = await this.alarmService.getAlarmByfilteredDateActive(     
      fini,
      ffin,
    );
    let msg =
    alarms.length == 0
        ? 'alarms not found'
        : 'alarms fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: alarms,
      count: alarms.length,
    });
  }

  @Get('/all')
  async getAlarmsAll(
    @Res() res,
    ) {
    const alarms = await this.alarmService.getAlarmsAll();

    let msg = alarms.length == 0 ? 'Alarms not found' : 'Alarms fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: alarms,
      count: alarms.length,
    });
  }

  @Get('/:alarmId')
  async getAlarm(
    @Res() res, 
    @Param('alarmId') alarmId
    ) {
    if (!alarmId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Alarm id is not a valid  ObjectId');
    }

    const alarm = await this.alarmService.getAlarm(alarmId);
    if (!alarm) {
      throw new NotFoundException('Alarm not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Alarm found',
      data: alarm,
    });
  }

  @Get('/find/:alarmId')
  async getCodeAlarm(
    @Res() res, 
    @Param('alarmId') alarmId
    ) {

    //console.log(alarmId)
    // if (!alarmId.match(/^[0-9a-fA-F]{24}$/)) {
    //   throw new BadRequestException('Alarm id is not a valid  ObjectId');
    // }

    const alarm = await this.alarmService.getCodeAlarm(alarmId);
    if (!alarm) {
      throw new NotFoundException('Alarm not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Alarm found',
      data: alarm,
    });
  }

  @Get('/accion/:accion')
  async getAlarmActive(
    @Res() res, 
    @Param('accion') accion
    ) {
    // console.log(accion)
    let filtro={};
    if(accion=="true"){filtro={active:true}}
    else if(accion=="false"){filtro={active:false}}
    const alarm = await this.alarmService.getAlarmActive(filtro);
    if (!alarm) {
      throw new NotFoundException('Alarm not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Alarm found',
      data: alarm,
    });
  }

  @Put('/:alarmId')
  async updateAlarm(
    @Res() res,
    @Body() body: CreateAlarmDTO,
    @Param('alarmId') alarmId,
  ) {
    if (!alarmId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Alarm id is not a valid  ObjectId');
    }
    const updatedAlarm = await this.alarmService.updateAlarm(alarmId, body);
    if (!updatedAlarm) {
      throw new NotFoundException('Alarm not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Alarm updated',
      data: updatedAlarm,
    });
  }

  @Delete('/:alarmId')
  async deleteAlarm(@Res() res, @Param('alarmId') alarmId) {
    const deletedAlarm = await this.alarmService.deleteAlarm(alarmId);

    if (!deletedAlarm) {
      throw new NotFoundException('Alarm not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Alarm deleted',
      data: deletedAlarm,
    });
  }
}
