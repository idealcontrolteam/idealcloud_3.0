import { Schema } from 'mongoose';
export const AlarmSchema = new Schema(
  {
    code:String,
    endDate: Date,
    dateTimeIni: Date,
    dateTimeTer: Date,
    dateTimeSoport: Date,
    detail: String,
    company: String,
    workplaceId: String,
    locationId: {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: true,
    },
    active: Boolean,
    alarmValue: Number,
    sentEmail: Boolean,
    status: String,
    type: String,
    divice: String,
  },
  { versionKey: false },
);

