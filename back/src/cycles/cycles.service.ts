import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCyclesDTO } from './dto/cycles.dto';
import { Cycles } from './interfaces/cycles.interface';

@Injectable()
export class CyclesService {
    constructor(@InjectModel('Cycles') private cyclesModel: Model<Cycles>) {}
    
      async getCyclesAll(): Promise<Cycles[]> {
        const cycless = await this.cyclesModel.find();
        return cycless;
      }
    
    //   async getAlarmsByTagId(tagId): Promise<Cycles[]> {
    //     const cycless = await this.cyclesModel.find({ tagId: tagId });
    //     return cycless;
    //   }
    
      async getCycles(id): Promise<Cycles> {
        const cycles = await this.cyclesModel.findById(id);
        return cycles;
      }

      async getCyclesWorkplace(id): Promise<any> {
        //console.log(id)
        const cycles = await this.cyclesModel.find({workplaceId:id});
        return cycles;
      }
    
      async createCycles(createCyclesDTO: CreateCyclesDTO): Promise<any> {
        const newCycles = new this.cyclesModel(createCyclesDTO);
        return await newCycles.save();
      }

      async getArrayCycles(workplaces): Promise<any> {
        const cycles = await this.cyclesModel.find({
          $or:workplaces,
          start_date:{ $ne: null }
        });
        return cycles;
      }
    
      async deleteCycles(id): Promise<Cycles> {
        return await this.cyclesModel.findByIdAndDelete(id);
      }
    
      async updateCycles(id: string, body: CreateCyclesDTO): Promise<Cycles> {
        return await this.cyclesModel.findByIdAndUpdate(id, body, {
          new: true,
        });
      }
    
      // async getCyclesByfilteredDate(   
      //   fini,
      //   ffin,
      //   divice,
      // ): Promise<Cycles[]> {
      //   const alarms = await this.cyclesModel.find({      
      //     dateTime: { $gte: fini, $lte: ffin },
      //     dispositivoId:divice
      //   });
      //   return alarms;
      // }
}
