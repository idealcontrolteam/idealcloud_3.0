import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { CyclesService } from './cycles.service';
  import { CreateCyclesDTO } from './dto/cycles.dto';
  import { AuthGuard } from 'src/shared/auth.guard';
import { CreateEfectividadDayDTO } from '../efectividad-day/dto/efectividad-day.dto';

@Controller('cycles')
export class CyclesController {
    constructor(private cyclesService: CyclesService) {}

  @Post()
  async createCycles(@Res() res, @Body() body: CreateCyclesDTO) {
    let newCycles=[];
    try{
      newCycles = await this.cyclesService.createCycles(body);
    }catch(e){
      throw new NotFoundException('Cycles not found');
    }
    
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Cycles created successfully',
      data: newCycles,
    });
  }

  @Post('/workplaces')
  async getArrayCycles(@Res() res, @Body() body: CreateCyclesDTO) {
    let newCycles=[];
    try{
      newCycles = await this.cyclesService.getArrayCycles(body);
    }catch(e){
      throw new NotFoundException('Cycles not found');
    }
    
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Cycles created successfully',
      data: newCycles,
    });
  }


//   @Get()
//   async getCycless(@Res() res) {
//     const cycless = await this.cyclesService.getCyclesAll();

//     let msg = cycless.length == 0 ? 'Cycless not found' : 'Cycless fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: cycless,
//       count: cycless.length,
//     });
//   }
  // @Get('/divice/:divice/:fini/:ffin')
  // async getCyclesByfilteredDate(
  //   @Res() res,   
  //   @Param('divice') divice,
  //   @Param('fini') fini,
  //   @Param('ffin') ffin,
  // ) {  
  //   const cycles = await this.cyclesService.getCyclesByfilteredDate(     
  //     fini,
  //     ffin,
  //     divice,
  //   );
  //   let msg =
  //   cycles.length == 0
  //       ? 'cycless not found'
  //       : 'cycless fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: cycles,
  //     count: cycles.length,
  //   });
  // }

  @Get('/all')
  async getCyclessAll(@Res() res) {
    const cycless = await this.cyclesService.getCyclesAll();

    let msg = cycless.length == 0 ? 'Cycless not found' : 'Cycless fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: cycless,
      count: cycless.length,
    });
  }

  @Get('/:cyclesId')
  async getCycles(
    @Res() res, 
    @Param('cyclesId') cyclesId
    ) {
    if (!cyclesId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Cycles id is not a valid  ObjectId');
    }

    const cycles = await this.cyclesService.getCycles(cyclesId);
    if (!cycles) {
      throw new NotFoundException('Cycles not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Cycles found',
      data: cycles,
    });
  }

  @Get('/workplace/:workplaceId')
  async getCyclesWorkplace(
    @Res() res, 
    @Param('workplaceId') workplaceId
    ) {
    // if (!workplaceId.match(/^[0-9a-fA-F]{24}$/)) {
    //   throw new BadRequestException('Cycles id is not a valid  ObjectId');
    // }
    //console.log(workplaceId)
    const cycles = await this.cyclesService.getCyclesWorkplace(workplaceId);
    if (!cycles) {
      throw new NotFoundException('Cycles not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Cycles found',
      data: cycles,
    });
  }

  @Put('/:cyclesId')
  async updateCycles(
    @Res() res,
    @Body() body: CreateCyclesDTO,
    @Param('cyclesId') cyclesId,
  ) {
    if (!cyclesId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Cycles id is not a valid  ObjectId');
    }
    const updatedCycles = await this.cyclesService.updateCycles(cyclesId, body);
    if (!updatedCycles) {
      throw new NotFoundException('Cycles not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Cycles updated',
      data: updatedCycles,
    });
  }

  @Delete('/:cyclesId')
  async deleteCycles(@Res() res, @Param('cyclesId') cyclesId) {
    const deletedCycles = await this.cyclesService.deleteCycles(cyclesId);

    if (!deletedCycles) {
      throw new NotFoundException('Cycles not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Cycles deleted',
      data: deletedCycles,
    });
  }
}
