import { Module } from '@nestjs/common';
import { CyclesController } from './cycles.controller';
import { CyclesService } from './cycles.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CyclesSchema } from './schemas/cycles.schema'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Cycles', schema: CyclesSchema, collection: 'cycles' },
    ]),
  ],
  controllers: [CyclesController],
  providers: [CyclesService]
})
export class CyclesModule {}
