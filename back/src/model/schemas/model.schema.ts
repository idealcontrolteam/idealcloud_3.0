import { Schema } from 'mongoose';

export const ModelSchema = new Schema(
  {
    name: String,
    active: Boolean,
    brandId: {
      type: Schema.Types.ObjectId,
      ref: 'Brand',
      required: true,
    },
  },
  { versionKey: false },
);

/*ModelSchema.virtual('brand', {
  ref: 'Brand',
  localField: 'brandId',
  foreignField: '_id',
  justOne: true,
});*/

/*
{
    //toJSON: { virtuals: true },
    versionKey: false,
  },
*/

/*virtual: {
      brands: {
        ref: 'Brand',
        localField: 'brandId',
        foreignField: '_id',
      },
    },*/

/*
ModelSchema.set('toObject', { virtuals: true });
ModelSchema.set('toJSON', { virtuals: true });*/
