import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateEventDTO } from './dto/event.dto';
import { Event } from './interfaces/event.interface';

@Injectable()
export class EventService {
    constructor(@InjectModel('Event') private eventModel: Model<Event>) {}
    
      async getEventAll(): Promise<Event[]> {
        const events = await this.eventModel.find();
        return events;
      }
    
    //   async getAlarmsByTagId(tagId): Promise<Event[]> {
    //     const events = await this.eventModel.find({ tagId: tagId });
    //     return events;
    //   }

      async getEventWorkplace(workplaceId): Promise<any>{
        const event = await this.eventModel.find({workplaceId});
        return event;
      }

      async getfindActiveEvent(workplaceId): Promise<any> {
        const event = await this.eventModel.find({workplaceId,active:true});
        return event;
      }

      async getfindActiveEventEnd(workplaceIds): Promise<any> {
        const event = await this.eventModel.find({
                                            $or:workplaceIds,
                                            active:true
                                          }).sort({dateTime:-1});
        return event;
      }
    
      async getEvent(id): Promise<Event> {
        const event = await this.eventModel.findById(id);
        return event;
      }
    
      async createEvent(createEventDTO: CreateEventDTO): Promise<Event> {
        const newEvent = new this.eventModel(createEventDTO);
        return await newEvent.save();
      }
    
      async deleteEvent(id): Promise<Event> {
        return await this.eventModel.findByIdAndDelete(id);
      }
    
      async updateEvent(id: string, body: CreateEventDTO): Promise<Event> {
        return await this.eventModel.findByIdAndUpdate(id, body, {
          new: true,
        });
      }
    
    //   async getAlarmByfilteredDate(   
    //     fini,
    //     ffin,
    //   ): Promise<Event[]> {
    //     const alarms = await this.eventModel.find({      
    //       dateTimeIni: { $gte: fini, $lte: ffin },
    //     });
    //     return alarms;
    //   }
}
