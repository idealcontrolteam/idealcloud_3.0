import { Schema } from 'mongoose';
export const EventSchema = new Schema(
  {
    detail: String,
    n_visit: Number,
    dateTime: Date,
    endDateTime: Date,
    activityId: {
      type: Schema.Types.ObjectId,
      ref: 'Activity',
      required: false,
    },
    workplaceId: {
      type: Schema.Types.ObjectId,
      ref: 'Workplace',
      required: false,
    },
    active: Boolean,
  },
  { versionKey: false },
);