import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { EventService } from './event.service';
  import { CreateEventDTO } from './dto/event.dto';
  import { AuthGuard } from 'src/shared/auth.guard';

@Controller('event')
export class EventController {
    constructor(private eventService: EventService) {}

  @Post()
  async createEvent(@Res() res, @Body() body: CreateEventDTO) {
    const newEvent = await this.eventService.createEvent(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Event created successfully',
      data: newEvent,
    });
  }


//   @Get()
//   async getEvents(@Res() res) {
//     const events = await this.eventService.getEventAll();

//     let msg = events.length == 0 ? 'Events not found' : 'Events fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: events,
//       count: events.length,
//     });
//   }
//   @Get('/:fini/:ffin')
//   async getEventByfilteredDate(
//     @Res() res,   
//     @Param('fini') fini,
//     @Param('ffin') ffin,
//   ) {  
//     const events = await this.eventService.getEventByfilteredDate(     
//       fini,
//       ffin,
//     );
//     let msg =
//     events.length == 0
//         ? 'events not found'
//         : 'events fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: events,
//       count: events.length,
//     });
//   }

  @Get('/all')
  async getEventsAll(@Res() res) {
    const events = await this.eventService.getEventAll();

    let msg = events.length == 0 ? 'Events not found' : 'Events fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: events,
      count: events.length,
    });
  }

  @Get('/workplace/:workplaceId')
  async getEventWorkplace(
    @Res() res, 
    @Param('workplaceId') workplaceId
    ) {
    // if (!workplaceId.match(/^[0-9a-fA-F]{24}$/)) {
    //   throw new BadRequestException('Event id is not a valid  ObjectId');
    // }
    //console.log(workplaceId)
    const event = await this.eventService.getEventWorkplace(workplaceId);
    if (!event) {
      throw new NotFoundException('Event not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Event found',
      data: event,
    });
  }

  @Get('/:eventId')
  async getEvent(
    @Res() res, 
    @Param('eventId') eventId
    ) {
    if (!eventId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Event id is not a valid  ObjectId');
    }

    const event = await this.eventService.getEvent(eventId);
    if (!event) {
      throw new NotFoundException('Event not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Event found',
      data: event,
    });
  }

  @Get('/:workplaceId/active')
  async getFindActiveEvent(
    @Res() res, 
    @Param('workplaceId') workplaceId
    ) {
    // if (!workplaceId.match(/^[0-9a-fA-F]{24}$/)) {
    //   throw new BadRequestException('Event id is not a valid  ObjectId');
    // }

    const event = await this.eventService.getfindActiveEvent(workplaceId);
    if (!event) {
      throw new NotFoundException('Event not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Event found',
      data: event,
    });
  }

  @Post('/workplaceIds/active_end')
  async getFindActiveEventEnd(
    @Res() res, 
    @Body() body,
    //@Param('workplaceId') workplaceId
    ) {
    // if (!workplaceId.match(/^[0-9a-fA-F]{24}$/)) {
    //   throw new BadRequestException('Event id is not a valid  ObjectId');
    // }

    const event = await this.eventService.getfindActiveEventEnd(body);
    if (!event) {
      throw new NotFoundException('Event not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Event found',
      data: event,
    });
  }

  @Put('/:eventId')
  async updateEvent(
    @Res() res,
    @Body() body: CreateEventDTO,
    @Param('eventId') eventId,
  ) {
    if (!eventId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Event id is not a valid  ObjectId');
    }
    const updatedEvent = await this.eventService.updateEvent(eventId, body);
    if (!updatedEvent) {
      throw new NotFoundException('Event not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Event updated',
      data: updatedEvent,
    });
  }

  @Delete('/:eventId')
  async deleteEvent(@Res() res, @Param('eventId') eventId) {
    const deletedEvent = await this.eventService.deleteEvent(eventId);

    if (!deletedEvent) {
      throw new NotFoundException('Event not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Event deleted',
      data: deletedEvent,
    });
  }
}
