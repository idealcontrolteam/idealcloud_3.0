import { Module } from '@nestjs/common';
import { LocalRegistrosHoraController } from './local_registros_hora.controller';
import { LocalRegistrosHoraService } from './local_registros_hora.service';
import { MongooseModule } from '@nestjs/mongoose';
import { LocalRegistrosHoraSchema } from './schemas/local_registros_hora.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'LocalRegistrosHora',
        schema: LocalRegistrosHoraSchema,
        collection: 'local_registros_hora',
      },
    ]),
  ],
  controllers: [LocalRegistrosHoraController],
  providers: [LocalRegistrosHoraService],
})
export class LocalRegistrosHoraModule {}
