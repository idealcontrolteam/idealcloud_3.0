import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateLocalRegistrosHoraDTO } from './dto/local_registros_hora.dto';
import { LocalRegistrosHoraService } from './local_registros_hora.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('local_registros_hora')
//@UseGuards(new AuthGuard())
export class LocalRegistrosHoraController {
  constructor(private measurementService: LocalRegistrosHoraService) {}

  @Post()
  async createMeasurement(@Res() res, @Body() body: CreateLocalRegistrosHoraDTO) {
    //this.validateIds(body);

    const newMeasurement = await this.measurementService.createMeasurement(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Get()
  async getMeasurements(@Res() res) {
    const measurements = await this.measurementService.getMeasurements();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/all')
  async getMeasurementsAll(@Res() res) {
    const measurements = await this.measurementService.getMeasurementsAll();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Post('/multiple')
  async createMeasurementMultiple(@Res() res, @Body() body) {
     //this.validateIds(body);
    let newMeasurement = {};
    body.map(async (Measurement) => { 
      // console.log(Measurement);     
      newMeasurement =  await this.measurementService.createMeasurement(Measurement);
         
    });  
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Get('/:measurementId')
  async getMeasurement(@Res() res, @Param('measurementId') measurementId) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    const measurement = await this.measurementService.getMeasurement(
      measurementId,
    );
    if (!measurement) {
      throw new NotFoundException('Measurement not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Get('/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfiltered(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfiltered(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/sensor/:sensorId/:fini/:ffin')
  async getMeasurementBySensorfiltered(
    @Res() res,
    @Param('sensorId') sensorId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementBySensorfiltered(
      sensorId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/location/:id_dispositivos/:fini/:ffin')
  async getMeasurementByLocationfiltered(
    @Res() res,
    @Param('id_dispositivos') id_dispositivos,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!id_dispositivos.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfiltered(
      id_dispositivos,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Put('/:measurementId')
  async updateMeasurement(
    @Res() res,
    @Body() body: CreateLocalRegistrosHoraDTO,
    @Param('measurementId') measurementId,
  ) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    const updatedMeasurement = await this.measurementService.updateMeasurement(
      measurementId,
      body,
    );
    if (!updatedMeasurement) {
      throw new NotFoundException('Measurement not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement updated',
      data: updatedMeasurement,
    });
  }

  @Delete('/:measurementId')
  async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
    const deletedMeasurement = await this.measurementService.deleteMeasurement(
      measurementId,
    );

    if (!deletedMeasurement) {
      throw new NotFoundException('Measurement not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement deleted',
      data: deletedMeasurement,
    });
  }
}
