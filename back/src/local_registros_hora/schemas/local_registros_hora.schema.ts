import { Schema } from 'mongoose';

export const LocalRegistrosHoraSchema = new Schema(
  {
    fecha_registros: Date,
    code: String,
    id_dispositivos: String,
    oxd: Number,
    oxs: Number,
    temp: Number,
    sal: Number,
  },
  { versionKey: false },
);
