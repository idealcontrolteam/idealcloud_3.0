import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateMeasurementDTO } from './dto/measurement.dto';
import {  MeasurementControlService } from './measurement-control.service';
import { AuthGuard } from 'src/shared/auth.guard';
import { Measurement } from './interfaces/measurement.interface';
import * as moment from 'moment';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';

@Controller('measurement-control')
// //@UseGuards(new AuthGuard())
export class MeasurementControlController {
  constructor(private measurementService:  MeasurementControlService,
              @InjectConnection() private connection: Connection
  ) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'value' && key != 'dateTime' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post('/multiple')
  async createMeasurementMultiple(@Res() res, @Body() body) {
     //this.validateIds(body);
    if(this.connection.readyState===0){
      throw new BadRequestException('Sin Conexión');
    }
    let newMeasurement = {};
    body.map(async (Measurement) => { 
      // console.log(Measurement);     
      newMeasurement =  await this.measurementService.createMeasurement(Measurement);
         
    });  
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }
  @Post()
  async createMeasurement(@Res() res, @Body() body: CreateMeasurementDTO) {
    this.validateIds(body);

    const newMeasurement = await this.measurementService.createMeasurement(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Post('/alarm/tag/:fini/:ffin')
  async getAlarmMeasurementByTagfiltered(
    @Res() res,
    @Body() body,
    // @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    try{
      let alarma=[];
      let status_alarma=''
      if(body.tagId.length == 1){
        alarma = await this.measurementService.getAlarmMeasurementByTagfiltered(
          body.tagId[0],
          body.type,
          fini,
          ffin,
        );
        status_alarma=alarma[0].status
      }else{
        let alarma2=[];
        alarma = await this.measurementService.getAlarmMeasurementByTagfiltered(
          body.tagId[0],
          body.type,
          fini,
          ffin,
        );
        alarma2 = await this.measurementService.getAlarmMeasurementByTagfiltered(
          body.tagId[1],
          body.type,
          fini,
          ffin,
        );
        if(alarma[0].status!="sin datos" || alarma2[0].status!="sin datos"){
          status_alarma=[alarma[0].status,alarma2[0].status].includes('pegados')?'pegados':'ok'
        }else{
          status_alarma='sin datos'
        }
      }
      //let data=body.type=="sal"?{'status_sal':alarma}:{'status':alarma}

      //let update_tag= await this.tagService.updateTag(body.locationId,{'status':alarma})
      // let update_location=await this.locationService.updateLocation(body.locationId,data)

      let msg =
        alarma.length == 0
          ? 'Measurements not found'
          : 'Measurements fetched';
  
          //---------------------Aqui
  
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: status_alarma,
        // count: measurements.length,
      });
    }catch(err){
      throw new BadRequestException('Ops! ocurrio un error');
    }
  }

  @Get()
  async getMeasurements(@Res() res) {
    const measurements = await this.measurementService.getMeasurements();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/all')
  async getMeasurementsAll(@Res() res) {
    const measurements = await this.measurementService.getMeasurementsAll();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:measurementId')
  async getMeasurement(@Res() res, @Param('measurementId') measurementId) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    const measurement = await this.measurementService.getMeasurement(
      measurementId,
    );
    if (!measurement) {
      throw new NotFoundException('Measurement not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Get('/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfiltered(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfiltered(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

        //---------------------Aqui


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/xy/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfilteredXY(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfilteredXY(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    let prom="";
    let max="";
    let min="";
    let ultimo_valor="";
    let ultima_fecha="";
    if(measurements.length!=0){
      prom=((measurements.reduce((a, b) => +a + +b.y, 0)/measurements.length)).toFixed(1);
      max=measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
      min=measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
      ultimo_valor=measurements[measurements.length-1].y;
      ultima_fecha=moment(measurements[measurements.length-1].x).format('YYYY-MM-DD HH:mm:ss');
    }


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
      prom:prom,
      max:max,
      min:min,
      ultimo_valor:ultimo_valor,
      ultima_fecha:ultima_fecha,
    });
  }

  @Get('/xy/tag/active/:tagId/:fini/:ffin')
  async getMeasurementByTagfilteredXYActive(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfilteredXYActive(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    let prom="";
    let max="";
    let min="";
    let ultimo_valor="";
    let ultima_fecha="";
    if(measurements.length!=0){
      prom=((measurements.reduce((a, b) => +a + +b.y, 0)/measurements.length)).toFixed(1);
      max=measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
      min=measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
      ultimo_valor=measurements[measurements.length-1].y;
      ultima_fecha=moment(measurements[measurements.length-1].x).format('YYYY-MM-DD HH:mm:ss');
    }


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
      prom:prom,
      max:max,
      min:min,
      ultimo_valor:ultimo_valor,
      ultima_fecha:ultima_fecha,
    });
  }



  @Get('/sensor/:sensorId/:fini/:ffin')
  async getMeasurementBySensorfiltered(
    @Res() res,
    @Param('sensorId') sensorId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementBySensorfiltered(
      sensorId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/location/:locationId/:fini/:ffin')
  async getMeasurementByLocationfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfiltered(
      locationId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/location_all/:locationId/:fini/:ffin')
  async getMeasurementByLocation_allfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationallfiltered(
      locationId,
      fini,
      ffin,
    );
    let oxd=measurements.filter(m=>m.type=="oxd");
    let oxs=measurements.filter(m=>m.type=="oxs");
    let temp=measurements.filter(m=>m.type=="temp");
    let sal=measurements.filter(m=>m.type=="sal");

    let data=sal.map((o,i)=>{
      return {
        "oxd":oxd!=null?oxd[i].value:[],
        "oxs":oxs!=null?oxs[i].value:[],
        "temp":temp!=null?temp[i].value:[],
        "sal":sal!=null?sal[i].value:[],
        "locationId":o.locationId,
        "dateTime":o.dateTime
      }
    })
    
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: data,
      count: measurements.length,
    });
  }

  @Get('/xy/location/:locationId/:fini/:ffin')
  async getMeasurementByLocationfilteredXY(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfilteredXY(
      locationId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Put('/:measurementId')
  async updateMeasurement(
    @Res() res,
    @Body() body: CreateMeasurementDTO,
    @Param('measurementId') measurementId,
  ) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    this.validateIds(body);

    const updatedMeasurement = await this.measurementService.updateMeasurement(
      measurementId,
      body,
    );
    if (!updatedMeasurement) {
      throw new NotFoundException('Measurement not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement updated',
      data: updatedMeasurement,
    });
  }

  @Delete('/:measurementId')
  async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
    const deletedMeasurement = await this.measurementService.deleteMeasurement(
      measurementId,
    );

    if (!deletedMeasurement) {
      throw new NotFoundException('Measurement not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement deleted',
      data: deletedMeasurement,
    });
  }
}
