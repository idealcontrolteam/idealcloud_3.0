import { Module } from '@nestjs/common';
import {  MeasurementControlController } from './measurement-control.controller';
import {  MeasurementControlService } from './measurement-control.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementSchema } from './schemas/measurement.schema';
import { LocationService } from 'src/location/location.service';
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'MeasurementControl',
        schema: MeasurementSchema,
        collection: 'measurementControl',
      },
    ]),
  ],
  controllers: [ MeasurementControlController],
  providers: [ MeasurementControlService,
              //ViewsService
  ],
  exports: [ MeasurementControlService],
})
export class  MeasurementControlModule {}
