import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { WorkPlace } from './interfaces/workPlace.interface';
import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { GatewayService } from '../gateway/gateway.service';
import { Gateway } from '../gateway/interfaces/gateway.interface';
import { ZoneService } from '../zone/zone.service';
import { Zone } from '../zone/interfaces/zone.interface';
import * as moment from 'moment';

@Injectable()
export class WorkPlaceService {
  constructor(
    @InjectModel('WorkPlace') private workPlaceModel: Model<WorkPlace>,
    private gatewayService: GatewayService,
    private zoneService: ZoneService,
  ) {}

  async getWorkPlaces(): Promise<WorkPlace[]> {
    const workPlaces = await this.workPlaceModel.find();
    return workPlaces
    // .map(w=>{
    //   var fecha=new Date(w.endDate);
    //   var zona_horaria=new Date(w.endDate).getTimezoneOffset();
    //   zona_horaria=zona_horaria/60;
    //   fecha.setHours(fecha.getHours()+zona_horaria-3);
    //   w.endDate=moment(fecha).format('YYYY-MM-DD HH:mm:ss');
    //   return w
    // });
  }

  async getWorkPlacesAll(): Promise<WorkPlace[]> {
    const workPlaces = await this.workPlaceModel
      .find()
      .populate('companyId categoryId');
    return workPlaces;
  }

  async getGateWaysByWorkPlaceId(workPlaceId): Promise<Gateway[]> {
    const gateways = await this.gatewayService.getGateWaysByWorkPlaceId(
      workPlaceId,
    );
    return gateways;
  }

  async getZonesByWorkPlaceId(workPlaceId): Promise<Zone[]> {
    const zones = await this.zoneService.getZonesByWorkPlaceId(workPlaceId);
    return zones;
  }

  async getWorkPlacesByCategoryId(categoryId): Promise<WorkPlace[]> {
    const workplaces = await this.workPlaceModel.find({
      categoryId: categoryId,
    });
    return workplaces;
  }
  
  async getCentro(code): Promise<any> {
    const centro = await this.workPlaceModel.findOne({code});
    return centro;
  }

  async getWorkPlacesByCompanyId(companyId): Promise<WorkPlace[]> {
    const workplaces = await this.workPlaceModel.find({
      companyId: companyId,
    });
    // let workplacesIdZone = {};
    // workplaces.map((w, pos) => (workplacesIdZone[w._id] = pos));

    // const zones = await this.zoneService.getZonesByWorkPlaceId(
    //   Object.keys(workplacesIdZone),
    // );
    // let new_workplace=[]; 
    // workplaces.map(w=>{
    //   let zone=zones.map(z=>{
    //     return z._id
    //   })
    //   new_workplace.push({
    //     "workplace":w,
    //     "zones":zone
    //   })
    // })
    // console.log(new_workplace)
    return workplaces;
  }

  async getWorkPlace(id): Promise<WorkPlace> {
    const workPlace = await this.workPlaceModel.findById(id);
    return workPlace;
  }

  // async getWorkPlaceCompany(idCompany): Promise<any> {
  //   const workPlace = await this.workPlaceModel.find({companyId:idCompany});
  //   return workPlace;
  // }

  async createWorkPlace(
    createWorkPlaceDTO: CreateWorkPlaceDTO,
  ): Promise<WorkPlace> {
    const newWorkPlace = new this.workPlaceModel(createWorkPlaceDTO);
    return await newWorkPlace.save();
  }

  async getWorkPlacesArray(workplaces): Promise<any> {
    const workPlace = await this.workPlaceModel.find({
      $or:workplaces
    });
    return workPlace.map(w=>{
      var fecha=new Date(w.endDate);
      var zona_horaria=new Date(w.endDate).getTimezoneOffset();
      zona_horaria=zona_horaria/60;
      fecha.setHours(fecha.getHours()+zona_horaria-1);
      w.endDate=moment(fecha).format('YYYY-MM-DD HH:mm:ss');
      return w
    });
  }

  async deleteWorkPlace(id): Promise<WorkPlace> {
    return await this.workPlaceModel.findByIdAndDelete(id);
  }

  async deleteWorkPlacesByCompanyId(companyId) {
    return await this.workPlaceModel.deleteMany({ companyId: companyId });
  }

  async updateWorkPlace(
    id: string,
    body: CreateWorkPlaceDTO,
  ): Promise<WorkPlace> {
    return await this.workPlaceModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
