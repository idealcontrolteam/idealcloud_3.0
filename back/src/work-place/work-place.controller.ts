import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { WorkPlaceService } from './work-place.service';
import { AuthGuard } from 'src/shared/auth.guard';
import { ZoneService } from '../zone/zone.service';
import webpush from 'web-push'; 

const webpush = require('web-push');

//const vapidKeys = webpush.generateVAPIDKeys();
const publicVapidKey ="BFD6nyuAMhu8GAE9geEWtBvFGm6NjSKV8IljbfSriqTAZ9XkyXIxTm4lSdAPPLoYU30xb5zsVHtsqZWC8c9CEA4";
const privateVapidKey ="I87tzLTPNT8sB0k3zE90cekp0w8DzLr0R3RDGoFPRAE";

// Replace with your email
webpush.setVapidDetails('mailto:jordyp60@gmail.com', publicVapidKey, privateVapidKey);

// Prints 2 URL Safe Base64 Encoded Strings
//console.log(vapidKeys.publicKey, vapidKeys.privateKey);

@Controller('workPlace')
//@UseGuards(new AuthGuard())
export class WorkPlaceController {
  constructor(private workPlaceService: WorkPlaceService,
              private zoneService: ZoneService,
              ) {}

  public validateIds = body => {
    try{
      Object.keys(body).map(key => {
        if (key == 'companyId' || key == 'categoryId') {
          if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
            throw new BadRequestException(`${key} is not a valid ObjectId`);
          }
        }
      });
    }catch(e){
      throw new BadRequestException(`is not a valid ObjectId`);
    }
    
  };

  @Post("/subscribe")
  async notificationSuscription(@Res() res, @Body() body: any) {
    const subscription = body;
    //res.status(201).json({});
    const payload = JSON.stringify({ title: 'test',body:"Bienvenidos a IdealCloud"});

    //console.log(subscription);

    webpush.sendNotification(subscription, payload).catch(error => {
      console.error(error.stack);
    });
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Work place created successfully',
      data: "",
    });
  }

  @Post()
  async createWorkPlace(@Res() res, @Body() body: CreateWorkPlaceDTO) {
    this.validateIds(body);
    
    const newWorkPlace = await this.workPlaceService.createWorkPlace(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Work place created successfully',
      data: newWorkPlace,
    });
  }

  @Post("/workplaces_array")
  async getWorkPlacesArray(@Res() res, @Body() body: any) {
    let newWorkPlace=[]
    //console.log(body)
    try{
      newWorkPlace = await this.workPlaceService.getWorkPlacesArray(body);
    }catch(e){
      throw new NotFoundException('Work place not found');
    }

    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Work place created successfully',
      data: newWorkPlace,
    });
  }

  @Get()
  async getWorkPlaces(@Res() res) {
    const workPlaces = await this.workPlaceService.getWorkPlaces();

    let msg =
      workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Get('/:idCompany/company')
  async getWorkPlacesCompany(@Res() res,
  @Param('idCompany') idCompany,) {
    const workPlaces = await this.workPlaceService.getWorkPlacesByCompanyId(idCompany);

    let msg =
      workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Get('/:code/centro')
  async getCentro(@Res() res,
  @Param('code') code) {
    const workPlaces = await this.workPlaceService.getCentro(code);

    let msg =
      workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Post('/user_centros')
  async getCentros(@Res() res,
  @Body() body:any) {
    try{
      let centros = body.centros;
      let workPlaces = await this.workPlaceService.getWorkPlaces();
      let cen=centros.split(',');
      //console.log(cen)
      let new_centros=[];
      cen.map((c,i)=>{
        if(i!=cen.length-1)
          new_centros.push(workPlaces.filter(w=>w.code==c)[0])
      })
      new_centros=new_centros.filter(c=>c!=null)
      //console.log(new_centros)
      //console.log(workPlaces)
      let msg =
        workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';
  
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: new_centros,
        count: new_centros.length,
      });
    }catch(e){
      throw new NotFoundException('Work place not found');
    }
    
  }

  // @Get('/:idCompany/resumen')
  // async getResumen(@Res() res,
  // @Param('idCompany') idCompany,) {
  //   const workPlaces = await this.workPlaceService.getWorkPlacesByCompanyId(idCompany);
  //   let zones = [];
  //   for(let i=0;i<workPlaces.length-1;i++){
  //      zones.push(await this.zoneService.getZonesByWorkPlaceId(workPlaces[i]._id));
  //   }

  //   let msg =
  //     workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: workPlaces,
  //     data2:zones,
  //     count: workPlaces.length,
  //   });
  // }

  @Get('/all')
  async getWorkPlacesAll(@Res() res) {
    const workPlaces = await this.workPlaceService.getWorkPlacesAll();

    let msg =
      workPlaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workPlaces,
      count: workPlaces.length,
    });
  }

  @Get('/:idWorkPlace/gateway')
  async getGateWaysByWorkPlaceId(
    @Res() res,
    @Param('idWorkPlace') idWorkPlace,
  ) {
    const gateways = await this.workPlaceService.getGateWaysByWorkPlaceId(
      idWorkPlace,
    );

    let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: gateways,
      count: gateways.length,
    });
  }

  @Get('/:idWorkPlace/zone')
  async getZonesByWorkPlaceId(@Res() res, @Param('idWorkPlace') idWorkPlace) {
    const zones = await this.workPlaceService.getZonesByWorkPlaceId(
      idWorkPlace,
    );

    let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: zones,
      count: zones.length,
    });
  }

  @Get('/:idWorkPlace')
  async getWorkPlace(@Res() res, @Param('idWorkPlace') idWorkPlace) {
    if (idWorkPlace==null) {
      throw new BadRequestException('WorkPlace id is not a valid ObjectId');
    }
    if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('WorkPlace id is not a valid ObjectId');
    }

    const workPlace = await this.workPlaceService.getWorkPlace(idWorkPlace);
    if (!workPlace) {
      throw new NotFoundException('Work place not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place found',
      data: workPlace,
    });
  }

  // @Get('/:nameCompany/nameCompany')
  // async getWorkPlaceCompany(@Res() res, @Param('nameCompany') nameCompany) {
  //   // if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
  //   //   throw new BadRequestException('WorkPlace id is not a valid ObjectId');
  //   // }
  //   const company = await this.companyService.getCompanyName(nameCompany);
  //   console.log(company)
  //   const workPlace = await this.workPlaceService.getWorkPlaceCompany(idWorkPlace);
  //   // if (!workPlace) {
  //   //   throw new NotFoundException('Work place not found');
  //   // }

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: 'Work place found',
  //     data: "workPlace",
  //   });
  // }

  @Put('/:idWorkPlace')
  async updateWorkPlace(
    @Res() res,
    @Body() body: CreateWorkPlaceDTO,
    @Param('idWorkPlace') idWorkPlace,
  ) {
    if (idWorkPlace==null) {
      throw new BadRequestException('WorkPlace id is not a valid ObjectId');
    }
    if (!idWorkPlace.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Work place id is not a valid ObjectId');
    }
    this.validateIds(body);
    //console.log(body)
    const updatedWorkPlace = await this.workPlaceService.updateWorkPlace(
      idWorkPlace,
      body,
    );
    if (!updatedWorkPlace) {
      throw new NotFoundException('Work place not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place updated',
      data: updatedWorkPlace,
    });
  }

  @Delete('/:idWorkPlace')
  async deleteWorkPlace(@Res() res, @Param('idWorkPlace') idWorkPlace) {
    const deletedWorkPlace = await this.workPlaceService.deleteWorkPlace(
      idWorkPlace,
    );

    if (!deletedWorkPlace) {
      throw new NotFoundException('Work place not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Work place deleted',
      data: deletedWorkPlace,
    });
  }
}
