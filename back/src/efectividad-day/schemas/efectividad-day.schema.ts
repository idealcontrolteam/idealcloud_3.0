import { Schema } from 'mongoose';

export const EfectividadDaySchema = new Schema(
  {
    count: Number,
    locationId: {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: true,
    },
    workplaceId: {
      type: Schema.Types.ObjectId,
      ref: 'workPlace',
      required: true,
    },
    start_date: Date,
    end_date: Date,
    active: Boolean,
    dateTime: Date,
  },
  { versionKey: false },
);
