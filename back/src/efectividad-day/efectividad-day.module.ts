import { Module } from '@nestjs/common';
import { EfectividadDayController } from './efectividad-day.controller';
import { EfectividadDayService } from './efectividad-day.service';
import { MongooseModule } from '@nestjs/mongoose';
import { EfectividadDaySchema } from './schemas/efectividad-day.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'EfectividadDay',
        schema: EfectividadDaySchema,
        collection: 'efectivityDay',
      },
    ]),
  ],
  controllers: [EfectividadDayController],
  providers: [EfectividadDayService],
})
export class EfectividadDayModule {}
