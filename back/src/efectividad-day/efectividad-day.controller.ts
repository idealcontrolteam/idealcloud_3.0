import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateEfectividadDayDTO } from './dto/efectividad-day.dto';
import { EfectividadDayService } from './efectividad-day.service';
import { AuthGuard } from 'src/shared/auth.guard';
import * as moment from 'moment';

@Controller('efectivity_day')
//@UseGuards(new AuthGuard())
export class EfectividadDayController {
  constructor(private measurementService: EfectividadDayService) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'value' && key != 'dateTime' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post()
  async createMeasurement(@Res() res, @Body() body: CreateEfectividadDayDTO) {
    const newMeasurement = await this.measurementService.createMeasurement(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  restarfechas(f1,f2){
    return f2-f1;
  }

  @Post('/multiple')
  async createMeasurementMultiple(@Res() res, @Body() body) {
     //this.validateIds(body);
     //console.log(body.locationId)
     let start_date=moment().startOf('month').format('YYYY-MM-DDT00:00:00');
    let end_date=moment().endOf('month').format('YYYY-MM-DDT00:00:00');
    let f1=new Date(start_date);
    let f2=new Date(end_date);
    let diff = this.restarfechas(f1,f2);
    let diferenciaDias = Math.floor(diff / (1000 * 60 * 60 * 24))+1;
    //console.log(diferenciaDias)
    let c=1;
    let efectividades=[];
    while(c<=diferenciaDias){
      let data={
        "count": 0,
        "end_date": moment(end_date).format(`YYYY-MM-DDT23:59:00`)+".000Z",
        "locationId":body.locationId,
        "workplaceId": body.workplaceId,
        "start_date": moment(end_date).format(`YYYY-MM-DDT00:00:00`)+".000Z"
      } 
      efectividades.push(data);
      end_date=moment(f2).subtract(c, "days").format(`YYYY-MM-DDT00:00:00`);
      c++;
    }
    //console.log(efectividades)
    let newMeasurement = {};
    efectividades.map(async (Measurement) => { 
      // console.log(Measurement);     
      let find_efectividad=await this.measurementService.getMeasurementsExist(body.locationId,Measurement.start_date);
      if(find_efectividad.length==0){
        newMeasurement =  await this.measurementService.createMeasurement(Measurement);
        console.log("Se crea")
      }else{
        console.log("Si existe")
      }
    });  
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Get()
  async getMeasurements(@Res() res) {
    const measurements = await this.measurementService.getMeasurements();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/all')
  async getMeasurementsAll(@Res() res) {
    const measurements = await this.measurementService.getMeasurementsAll();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:measurementId')
  async getMeasurement(@Res() res, @Param('measurementId') measurementId) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    const measurement = await this.measurementService.getMeasurement(
      measurementId,
    );
    if (!measurement) {
      throw new NotFoundException('Measurement not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Get('/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfiltered(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfiltered(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/sensor/:sensorId/:fini/:ffin')
  async getMeasurementBySensorfiltered(
    @Res() res,
    @Param('sensorId') sensorId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementBySensorfiltered(
      sensorId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/location/:locationId/:fini/:ffin')
  async getMeasurementByLocationfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfiltered(
      locationId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  getTendencia(v1,v2){
    if(v1==0&&v2==0){
      return "cero"
    }else if(v1==v2){
      return "right"
    }
    else{
      return parseInt(v1)>parseInt(v2)?"up":"down"
    }
    
  }

  restarDate(f1,f2){
    return f1-f2
  }

  @Get('/workplace/arrow/:workplaceId/:f1/:f2/:f3/:f4')
  async getMeasurementByWorkplaceArrowfiltered(
    @Res() res,
    @Param('workplaceId') workplaceId,
    @Param('f1') f1,
    @Param('f2') f2,
    @Param('f3') f3,
    @Param('f4') f4,
    @Body() body: any
    ) {
      let measurement1=[];
      let measurement2=[];
      let measurement3=[];
      let measurement4=[];
      //console.log(workplaceId)
      measurement1 = await this.measurementService.getMeasurementByWorkplaceArrowfiltered(
        workplaceId,
        f1,
      );
      measurement2 = await this.measurementService.getMeasurementByWorkplaceArrowfiltered(
        workplaceId,
        f2,
      );
      measurement3 = await this.measurementService.getMeasurementByWorkplaceArrowfiltered(
        workplaceId,
        f3,
      );
      measurement4 = await this.measurementService.getMeasurementByWorkplaceArrowfiltered(
        workplaceId,
        f4,
      );
      let medicion=[];
      let prom1=0,prom2=0,prom3=0,prom4=0;
      let hoy=moment().format("YYYY-MM-DDT00:00:00");
      let now=new Date();
      let h1=new Date(hoy);
      let diff=this.restarfechas(h1,now);
      let diferecia_fechas=Math.floor(diff / (1000 * 60));
      let dif_100=diferecia_fechas/5;

      measurement1.length>0?
        prom1=parseFloat(((measurement1.reduce((a, b) => +a + +(b.count), 0)/measurement1.length)).toFixed(1))*100/dif_100:
        prom1
      measurement2.length>0?
        prom2=parseFloat(((measurement2.reduce((a, b) => +a + +(b.count), 0)/measurement2.length)).toFixed(1))*100/288:
        prom2
      measurement3.length>0?
        prom3=parseFloat(((measurement3.reduce((a, b) => +a + +(b.count), 0)/measurement3.length)).toFixed(1))*100/288:
        prom3
      measurement4.length>0?
        prom4=parseFloat(((measurement4.reduce((a, b) => +a + +(b.count), 0)/measurement4.length)).toFixed(1))*100/288:
        prom4

      medicion.push({"fecha":f1,
                     "prom":prom1,
                     "tendencia":this.getTendencia(prom1,prom2)
                    })
      medicion.push({"fecha":f2,
                     "prom":prom2,
                     "tendencia":this.getTendencia(prom2,prom3)
                    })
      medicion.push({"fecha":f3,
                     "prom":prom3,
                     "tendencia":this.getTendencia(prom3,prom4)
                    })
      medicion.push({"fecha":f4,
                      "prom":prom4,
                      "tendencia":this.getTendencia(prom4,prom3)
                    })

      let msg =
        measurement1.length == 0
          ? 'Measurements not found'
          : 'Measurements fetched';

      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: medicion,
        count: medicion.length,
      });
  }

  @Post('/workplace')
  async getMeasurementByWorkplacefiltered(
    @Res() res,
    // @Param('workplaceId') workplaceId,
    // @Param('fini') fini,
    // @Param('ffin') ffin,
    @Body() body: any
    ) {
      let measurements=[];
      //console.log(workplaceId)
      measurements = await this.measurementService.getMeasurementByWorkplacefiltered(
        body.workplaceId,
        body.fini,
        body.ffin,
      );
      let msg =
        measurements.length == 0
          ? 'Measurements not found'
          : 'Measurements fetched';

      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: measurements,
        count: measurements.length,
      });
  }

  @Put('/:measurementId')
  async updateMeasurement(
    @Res() res,
    @Body() body: CreateEfectividadDayDTO,
    @Param('measurementId') measurementId,
  ) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    this.validateIds(body);

    const updatedMeasurement = await this.measurementService.updateMeasurement(
      measurementId,
      body,
    );
    if (!updatedMeasurement) {
      throw new NotFoundException('Measurement not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement updated',
      data: updatedMeasurement,
    });
  }

  @Delete('/:measurementId')
  async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
    const deletedMeasurement = await this.measurementService.deleteMeasurement(
      measurementId,
    );

    if (!deletedMeasurement) {
      throw new NotFoundException('Measurement not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement deleted',
      data: deletedMeasurement,
    });
  }
}
