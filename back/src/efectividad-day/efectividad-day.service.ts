import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { EfectividadDay } from './interfaces/efectividad-day.interface';
import { CreateEfectividadDayDTO } from './dto/efectividad-day.dto';
import * as moment from 'moment';
import { ObjectID } from 'bson';

@Injectable()
export class EfectividadDayService {
  constructor(
    @InjectModel('EfectividadDay')
    private EfectividadDayModel: Model<EfectividadDay>,
  ) {}

  async getMeasurements(): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    locationId,
    fini,
    ffin,
  ): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      locationId: locationId,
      dateTime: { $gte: fini, $lt: ffin },
    });
    return measurements;
  }

  restartDate(f2,f1){
    return f2-f1
  }

  async getMeasurementByWorkplaceArrowfiltered(
    workplaceId,
    fini,
  ): Promise<any[]> {
    let measurements=[];
    
    measurements = await this.EfectividadDayModel.find({
      workplaceId:workplaceId,
      start_date: fini,
    });

    let data=[]
    data=measurements.map(m=>{
      return{
        "count": m.count,
        // "end_date": m.end_date,
        // "dateTime": m.dateTime,
        // "locationId": m.locationId,
        // "workplaceId": m.workplaceId,
        // "start_date": m.start_date,
        // "porcentaje":isNaN(m.count*100/288)?0:m.count*100/288,
      }
    })
    return data;
  }

  async getMeasurementByWorkplacefiltered(
    workplaceIds,
    fini,
    ffin,
  ): Promise<any[]> {
    let measurements=[];
    // if(workplaceId=="General"){
    //   measurements = await this.EfectividadDayModel.find({
    //     workplaceId: { $in: workplaceIds },
    //     start_date: { $gte: fini, $lt: ffin },
    //   });
    // }else{
    //   console.log(fini)
    //   console.log(ffin)
    //   console.log(workplaceIds)
      measurements = await this.EfectividadDayModel.find({
        $or:workplaceIds,
        start_date: { $gte: fini, $lte: ffin },
      });
      //console.log(measurements)
    //}
    let f1=moment().format('YYYY-MM-DDT00:00:00');
    let f2=new Date();
    let diff=0;
    diff=this.restartDate(f2,new Date(f1))
    let diff_100=(diff/(1000*60))/5
    //console.log((diff/(1000*60))/5)
    //debo restar las fechas hasta que llegue a la final
    // let f1=new Date(fini);
    // let f2=new Date(ffin);
    // let diff=this.restartDate(f2,f1);
    // let diferecia_fechas=0;
    // diferecia_fechas=Math.floor(diff / (1000 * 60 * 60 * 24))+1;
    // let c=0;
    // //onsole.log(diff)
    // let end_date=moment(f2).format(`YYYY-MM-DDT00:00:00`);
    // let array=[];
    // console.log(diferecia_fechas)
    // let count2=[];
    // while(c<=diferecia_fechas){
    //   // console.log(end_date==moment(measurements[0].start_date).format(`YYYY-MM-DDT00:00:00`))
    //   // console.log(moment(measurements[0].start_date).format(`YYYY-MM-DDT00:00:00`)+".000Z")
    //   // console.log(end_date+".000Z")
    //   let count=[];
    //   let efect=measurements.filter(m=>moment(m.start_date).format(`YYYY-MM-DDT00:00:00`)==end_date).map(m=>{
    //     count.push(m.count*100/288)
    //     if(isNaN(m.count*100/288)){
    //       console.log(m.count)
    //       count2.push(m.count)
    //     }
    //     return{
    //       "porcentaje":m.count*100/288
    //     }
    //   })
    //   let prom="";
    //   prom=((count.reduce((a, b) => +a + +(b), 0)/count.length)).toFixed(1);
    //   console.log(prom)
    //   end_date=moment(f2).subtract(c, "days").format(`YYYY-MM-DDT00:00:00`);
    //   array.push({"fecha":end_date,"prom":efect.length>0?parseFloat(prom):0})
    //   //console.log(end_date)
    //   //console.log(efect)
    //   c++;
    // }
    // console.log(array)
    // console.log("prom :"+((array.reduce((a, b) => +a + +(b.prom), 0)/array.length)).toFixed(1))
    // let prom_g="";
    // prom_g=((array.reduce((a, b) => +a + +(b.prom), 0)/array.length)).toFixed(1);
    //console.log(measurements)
    let data=[]
    //console.log(diff_100)
    data=measurements.map(m=>{
      // console.log(new Date(moment(f1).format("YYYY-MM-DDT00:00:00")+".000Z"))
      // console.log(m.start_date)
      // console.log(m.start_date>=new Date(moment(f1).format("YYYY-MM-DDT00:00:00")+".000Z"))
      if(m.start_date>=f1+"000.Z"){
        //console.log("hola")
        return{
          "count": m.count,
          "end_date": m.end_date,
          "dateTime": m.dateTime,
          "locationId": m.locationId,
          "workplaceId": m.workplaceId,
          "start_date": m.start_date,
          "porcentaje":isNaN(m.count*100/diff_100)?0:m.count*100/288,
        }
      }else{
        //console.log("chao")
        return{
          "count": m.count,
          "end_date": m.end_date,
          "dateTime": m.dateTime,
          "locationId": m.locationId,
          "workplaceId": m.workplaceId,
          "start_date": m.start_date,
          "porcentaje":isNaN(m.count*100/288)?0:m.count*100/288,
        }
      }
    })

    // console.log(data)

    // let f1=new Date(fini);
    // let f2=new Date(ffin);
    // let diff=this.restartDate(f2,f1);
    // let diferecia_fechas=0;
    // diferecia_fechas=Math.floor(diff / (1000 * 60 * 60 * 24))+1;
    // let end_date=moment(f2).format(`YYYY-MM-DDT00:00:00`);
    // let new_data=[];
    // workplaceIds.map(w=>{
    //   let c=1;
    //   while(c<diferecia_fechas){
    //     let efect=data.filter(m=>moment(m.start_date).format(`YYYY-MM-DDT00:00:00`)+".000Z"==end_date+".000Z"&&m.workplaceId==w.workplaceId)
    //     if(efect.length>0){
    //       new_data.push({workplaceId:w.workplaceId,fecha:end_date+".000Z",efect:((efect.reduce((a, b) => +a + +(b.porcentaje), 0)/efect.length)).toFixed(1)})
    //     }else{
    //       new_data.push({workplaceId:w.workplaceId,fecha:end_date+".000Z",efect:0})
    //     }
    //     end_date=moment(f2).subtract(c, "days").format(`YYYY-MM-DDT00:00:00`);
    //     c++;
    //   }
    //   //console.log(new_data)
    //   // new_data.push(array.map(a=>{workplaceId:a.workplaceId}))
    // })
    // console.log(new_data)
    // data.push({"promedio_general":((new_data.reduce((a, b) => +a + +(b.efect), 0)/new_data.length)).toFixed(1),"efect":new_data})
    data
    .push({"promedio_general":((data.reduce((a, b) => +a + +(b.porcentaje), 0)/data.length)).toFixed(1)})
    return data;
  }

  async getMeasurementOneWorkplacefiltered(
    workplaceId,
    fini,
  ): Promise<any[]> {
    let measurements=[];
    if(workplaceId=="General"){
      measurements = await this.EfectividadDayModel.find({
        start_date: fini,
      });
    }else{
      measurements = await this.EfectividadDayModel.find({
        workplaceId: workplaceId,
        start_date: fini,
      });
    }

    //console.log(measurements)
    return measurements.map(m=>{
      return{
        "count": m.count,
        "end_date": m.end_date,
        "dateTime": m.dateTime,
        "locationId": m.locationId,
        "workplaceId": m.workplaceId,
        "start_date": m.start_date,
        "porcentaje":m.count*100/288
      }
    });
  }

  async getMeasurement(id): Promise<EfectividadDay> {
    const measurement = await this.EfectividadDayModel.findById(id);
    return measurement;
  }

  async getMeasurementsExist(locationId,start_date): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      locationId,
      start_date,
    });
    //console.log(measurements)
    return measurements;
  }

  async getMeasurementsByLocationId(locationId): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<EfectividadDay[]> {
    const measurements = await this.EfectividadDayModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createEfectividadDayDTO: CreateEfectividadDayDTO,
  ): Promise<any> {    
    let newMeasurement = new this.EfectividadDayModel(
      createEfectividadDayDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<EfectividadDay> {
    return await this.EfectividadDayModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateEfectividadDayDTO,
  ): Promise<EfectividadDay> {
    return await this.EfectividadDayModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
