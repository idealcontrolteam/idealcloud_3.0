import { Schema } from 'mongoose';

export const LocationSchema = new Schema(
  {
    code: String,
    name: String,
    active: Boolean,
    workPlaceId: String,
    falla: Boolean,
    fallaDate: Date,
    status: String,
    status_sal: String,
    zoneId: {
      type: Schema.Types.ObjectId,
      ref: 'Zone',
      required: true,
    },
  },
  { versionKey: false },
);
