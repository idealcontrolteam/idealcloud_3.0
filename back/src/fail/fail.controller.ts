import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateFailDTO } from './dto/fail.dto';
import { FailService } from './fail.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('fail')
//@UseGuards(new AuthGuard())
export class FailController {
  constructor(private failService: FailService) {}

  @Post()
  async createFail(@Res() res, @Body() body: CreateFailDTO) {
    const newAFail = await this.failService.createFail(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Fail created successfully',
      data: newAFail,
    });
  }

  @Get()
  async getFails(@Res() res) {
    const fails = await this.failService.getFails();

    let msg = fails.length == 0 ? 'Fails not found' : 'Fails fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: fails,
      count: fails.length,
    });
  }
  @Get('/:fini/:ffin')
  async getFailByfilteredDate(
    @Res() res,   
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {  
    const fails = await this.failService.getFailByfilteredDate(     
      fini,
      ffin,
    );
    let msg =
    fails.length == 0
        ? 'fail not found'
        : 'fails fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: fails,
      count: fails.length,
    });
  }

  @Get('/all')
  async getFailsAll(@Res() res) {
    const fails = await this.failService.getFailsAll();

    let msg = fails.length == 0 ? 'Fails not found' : 'Fails fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: fails,
      count: fails.length,
    });
  }

  @Get('/:failId')
  async getFail(@Res() res, @Param('failId') failId) {
    if (!failId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Fail id is not a valid  ObjectId');
    }

    const fail = await this.failService.getFail(failId);
    if (!fail) {
      throw new NotFoundException('Fail not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Fail found',
      data: fail,
    });
  }

  @Put('/:failId')
  async updateFail(
    @Res() res,
    @Body() body: CreateFailDTO,
    @Param('failId') failId,
  ) {
    if (!failId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Fail id is not a valid  ObjectId');
    }
    const updatedFail = await this.failService.updateFail(failId, body);
    if (!updatedFail) {
      throw new NotFoundException('Fail not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Fail updated',
      data: updatedFail,
    });
  }

  @Delete('/:failId')
  async deleteFail(@Res() res, @Param('failId') failId) {
    const deletedFail = await this.failService.deleteFail(failId);

    if (!deletedFail) {
      throw new NotFoundException('Fail not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Fail deleted',
      data: deletedFail,
    });
  }
}
