import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Createemail_suportDTO } from './dto/email_suport.dto';
import { Email_Suport } from './interfaces/email_suport.interface';

@Injectable()
export class EmailSuportService {
    constructor(@InjectModel('EmailSuport') private activityModel: Model<Email_Suport>) {}
    
      async getActivityAll(): Promise<any[]> {
        const activitys = await this.activityModel.find();
        return activitys;
      }
    
    //   async getAlarmsByTagId(tagId): Promise<Activity[]> {
    //     const activitys = await this.activityModel.find({ tagId: tagId });
    //     return activitys;
    //   }
    
      async getActivity(id): Promise<Email_Suport> {
        const activity = await this.activityModel.findById(id);
        return activity;
      }
    
      async createActivity(Createemail_suportDTO: Createemail_suportDTO): Promise<Email_Suport> {
        const newActivity = new this.activityModel(Createemail_suportDTO);
        return await newActivity.save();
      }
    
      async deleteActivity(id): Promise<Email_Suport> {
        return await this.activityModel.findByIdAndDelete(id);
      }
    
      async updateActivity(id: string, body: Createemail_suportDTO): Promise<Email_Suport> {
        return await this.activityModel.findByIdAndUpdate(id, body, {
          new: true,
        });
      }
    
    //   async getAlarmByfilteredDate(   
    //     fini,
    //     ffin,
    //   ): Promise<Activity[]> {
    //     const alarms = await this.activityModel.find({      
    //       dateTimeIni: { $gte: fini, $lte: ffin },
    //     });
    //     return alarms;
    //   }
}
