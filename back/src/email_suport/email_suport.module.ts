import { Module } from '@nestjs/common';
import { EmailSuportController } from './email_suport.controller';
import { EmailSuportService } from './email_suport.service';
import { MongooseModule } from '@nestjs/mongoose';
import { email_suportSchema } from './schemas/email_suport.schema'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'EmailSuport', schema: email_suportSchema, collection: 'emailSuport' },
    ]),
  ],
  controllers: [EmailSuportController],
  providers: [EmailSuportService]
})
export class EmailSuportModule {}
