import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { EmailSuportService } from './email_suport.service';
  import { Createemail_suportDTO } from './dto/email_suport.dto';
  import { AuthGuard } from 'src/shared/auth.guard';

@Controller('email_suport')
export class EmailSuportController {
    constructor(private activityService: EmailSuportService) {}

  @Post()
  async createActivity(@Res() res, @Body() body: Createemail_suportDTO) {
    const newActivity = await this.activityService.createActivity(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Email Suport created successfully',
      data: newActivity,
    });
  }


//   @Get()
//   async getActivitys(@Res() res) {
//     const activitys = await this.activityService.getActivityAll();

//     let msg = activitys.length == 0 ? 'Activitys not found' : 'Activitys fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: activitys,
//       count: activitys.length,
//     });
//   }
//   @Get('/:fini/:ffin')
//   async getActivityByfilteredDate(
//     @Res() res,   
//     @Param('fini') fini,
//     @Param('ffin') ffin,
//   ) {  
//     const activitys = await this.activityService.getActivityByfilteredDate(     
//       fini,
//       ffin,
//     );
//     let msg =
//     activitys.length == 0
//         ? 'activitys not found'
//         : 'activitys fetched';

//     return res.status(HttpStatus.OK).json({
//       statusCode: HttpStatus.OK,
//       message: msg,
//       data: activitys,
//       count: activitys.length,
//     });
//   }

  @Get('/all')
  async getActivitysAll(@Res() res) {
    const activitys = await this.activityService.getActivityAll();

    let msg = activitys.length == 0 ? 'Activitys not found' : 'Activitys fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: activitys,
      count: activitys.length,
    });
  }

  @Get('/:activityId')
  async getActivity(
    @Res() res, 
    @Param('activityId') activityId
    ) {
    if (!activityId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Activity id is not a valid  ObjectId');
    }

    const activity = await this.activityService.getActivity(activityId);
    if (!activity) {
      throw new NotFoundException('Activity not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Activity found',
      data: activity,
    });
  }

  @Put('/:activityId')
  async updateActivity(
    @Res() res,
    @Body() body: Createemail_suportDTO,
    @Param('activityId') activityId,
  ) {
    if (!activityId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Activity id is not a valid  ObjectId');
    }
    const updatedActivity = await this.activityService.updateActivity(activityId, body);
    if (!updatedActivity) {
      throw new NotFoundException('Activity not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Activity updated',
      data: updatedActivity,
    });
  }

  @Delete('/:activityId')
  async deleteActivity(@Res() res, @Param('activityId') activityId) {
    const deletedActivity = await this.activityService.deleteActivity(activityId);

    if (!deletedActivity) {
      throw new NotFoundException('Activity not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Activity deleted',
      data: deletedActivity,
    });
  }
}
