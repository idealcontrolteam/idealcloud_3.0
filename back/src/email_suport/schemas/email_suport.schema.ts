import { Schema } from 'mongoose';
export const email_suportSchema = new Schema(
  {
    message: String,
    name_workplace: String,
    workplaceId: {
      type: Schema.Types.ObjectId,
      ref: 'workPlace',
      required: true,
    },
    dateTime: Date,
    active: Boolean,
    code :String,
  },
  { versionKey: false },
);

