import { Document } from 'mongoose';

export interface Email_Suport extends Document {
  message: string;
  name_workplace: string;
  workplaceId: string;
  dateTime: Date;
  active: Boolean;
  code :string;
}
