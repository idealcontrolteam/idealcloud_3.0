import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as AreaI from './interfaces/status_server.interface';
import { CreateStatusServerDTO } from './dto/status_server.dto';

@Injectable()
export class StatusServerService {
    constructor(
        @InjectModel('StatusServer') private areaModel: Model<AreaI.StatusServer>
      ){}

    async createArea(createCyclesDTO: CreateStatusServerDTO): Promise<AreaI.StatusServer> {
       const newCycles = new this.areaModel(createCyclesDTO);
       return await newCycles.save();
    }

    async getAreas(): Promise<AreaI.StatusServer[]> {
        const areas = await this.areaModel.find();
        return areas;
    }

    async updateServer(id: string, body: CreateStatusServerDTO): Promise<AreaI.StatusServer> {
        return await this.areaModel.findByIdAndUpdate(id, body, {
          new: true,
        });
    }

}
