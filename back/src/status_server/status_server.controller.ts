import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { CreateStatusServerDTO } from './dto/status_server.dto';
  import { StatusServerService } from './status_server.service';
  import { AuthGuard } from 'src/shared/auth.guard';

@Controller('status_server')
export class StatusServerController {
   constructor(
    private areaService: StatusServerService
  ) {}

  @Post()
  async createArea(@Res() res, @Body() body: CreateStatusServerDTO) {
    //console.log(body)
    const newCycles = await this.areaService.createArea(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Status Server created successfully',
      data: newCycles,
    });
  }

  @Get()
  async getModels(@Res() res) {
    const models = await this.areaService.getAreas();

    let msg = models.length == 0 ? 'Status Server not found' : 'Status Server fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: models,
      count: models.length,
    });
  }

  @Put('/:serverId')
  async updateSensor(
    @Res() res,
    @Body() body: CreateStatusServerDTO,
    @Param('serverId') serverId,
  ) {
    if (!serverId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }
    const updatedSensor = await this.areaService.updateServer(serverId, body);
    if (!updatedSensor) {
      throw new NotFoundException('Sensor not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Sensor updated successfully',
      data: updatedSensor,
    });
  }
}
