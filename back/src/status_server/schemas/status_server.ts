import { Schema } from 'mongoose';

export const StatusServerSchema = new Schema(
  {
    total_memory: Number,
    used_memory: Number,
    ram: Number,
    free_memory: Number,
    cpu: Number,
    cache: Number,
    swap: Number,
    buffers: Number,
    clearBuffer: Boolean,
    total_swap: Number,
    total_disk: Number,
	  used_disk: Number,
  }
);
