import { Module } from '@nestjs/common';
import { StatusServerService } from './status_server.service';
import { StatusServerController } from './status_server.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { StatusServerSchema } from './schemas/status_server';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'StatusServer', schema: StatusServerSchema, collection: 'status_server' },
    ]),
  ],
  controllers: [StatusServerController],
  providers: [StatusServerService],
  exports: [StatusServerService],
})
export class StatusServerModule {}
