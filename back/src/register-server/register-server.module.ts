import { Module } from '@nestjs/common';
import { RegisterServerController } from './register-server.controller';
import { RegisterServerService } from './register-server.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterServerSchema } from './schemas/register-server.schema';
import { LocationService } from 'src/location/location.service';
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'RegisterServer',
        schema: RegisterServerSchema,
        collection: 'register_server',
      },
    ]),
  ],
  controllers: [RegisterServerController],
  providers: [RegisterServerService,
              //ViewsService
  ],
  exports: [RegisterServerService],
})
export class RegisterServerModule {}
