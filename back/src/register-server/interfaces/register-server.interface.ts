import { Document } from 'mongoose';

export interface RegisterServer extends Document {
  dateTime: Date;
  tatotal_diskgId: number;
  total_memory: number;
  used_memory: number;
  used_disk: number;
  cpu: number;
}
