import { Schema } from 'mongoose';

export const RegisterServerSchema = new Schema(
  {
    dateTime: Date,
    tatotal_diskgId: Number,
    total_memory: Number,
    used_memory: Number,
    used_disk: Number,
    cpu: Number,
  },
  { versionKey: false },
);
