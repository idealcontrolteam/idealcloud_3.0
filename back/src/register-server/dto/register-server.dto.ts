export class CreateRegisterServerDTO {
  dateTime: Date;
  tatotal_diskgId: number;
  total_memory: number;
  used_memory: number;
  used_disk: number;
  cpu: number;
}
