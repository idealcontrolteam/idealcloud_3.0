import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { RegisterNow } from './interfaces/register-now.interface';
import { CreateRegisterNowDTO } from './dto/register-now.dto';
import { AnyMxRecord } from 'dns';

@Injectable()
export class RegisterNowService {
  constructor(
    @InjectModel('RegisterNow')
    private measurementCopyModel: Model<RegisterNow>,
  ) {}

  async getMeasurements(): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel.find();
    return measurements;
  }

  async getMeasurementsAll(): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel
      .find()
      .populate('tagId sensorId locationId');
    return measurements;
  }

  async getMeasurementByTagfiltered(
    tagId,
    fini,
    ffin,
  ): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementBySensorfiltered(
    sensorId,
    fini,
    ffin,
  ): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
      dateTime: { $gte: fini, $lte: ffin },
    });
    return measurements;
  }

  async getMeasurementByLocationfiltered(
    id_dispositivos,
    fini,
    ffin,
  ): Promise<any[]> {
    let measurements = await this.measurementCopyModel.find({
      id_dispositivos: id_dispositivos,
      fecha_registros: { $gte: fini, $lt: ffin },
    }).lean();
    return measurements.map(d=>{
      var fecha=new Date(d.fecha_registros);
      var zona_horaria=new Date(d.fecha_registros).getTimezoneOffset();
      zona_horaria=zona_horaria/60;
      let f=fecha.setHours(fecha.getHours()+zona_horaria);
      return {
        "_id": d._id,
        "code": d.code,
        "temp": d.temp,
        "sal": d.sal,
        "oxs": d.oxs,
        "oxd": d.oxd,
        "fecha_registros": f,
        "id_dispositivos": d.id_dispositivos
      }
    });
  }

  async getMeasurement(id): Promise<RegisterNow> {
    const measurement = await this.measurementCopyModel.findById(id);
    return measurement;
  }

  async getMeasurementsByLocationId(locationId): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel.find({
      locationId: locationId,
    });
    return measurements;
  }

  async getMeasurementsBySensorId(sensorId): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel.find({
      sensorId: sensorId,
    });
    return measurements;
  }

  async getMeasurementsByTagId(tagId): Promise<RegisterNow[]> {
    const measurements = await this.measurementCopyModel.find({
      tagId: tagId,
    });
    return measurements;
  }

  async createMeasurement(
    createRegisterNowDTO: CreateRegisterNowDTO,
  ): Promise<RegisterNow> {
    const newMeasurement = new this.measurementCopyModel(
      createRegisterNowDTO,
    );
    return await newMeasurement.save();
  }

  async deleteMeasurement(id): Promise<RegisterNow> {
    return await this.measurementCopyModel.findByIdAndDelete(id);
  }

  async updateMeasurement(
    id: string,
    body: CreateRegisterNowDTO,
  ): Promise<RegisterNow> {
    return await this.measurementCopyModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
