import { Module } from '@nestjs/common';
import { RegisterNowController } from './register-now.controller';
import { RegisterNowService } from './register-now.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterNowSchema } from './schemas/register-now.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'RegisterNow',
        schema: RegisterNowSchema,
        collection: 'registerNow',
      },
    ]),
  ],
  controllers: [RegisterNowController],
  providers: [RegisterNowService],
})
export class RegisterNowModule {}
