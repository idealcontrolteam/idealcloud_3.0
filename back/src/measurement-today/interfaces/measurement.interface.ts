import { Document } from 'mongoose';

export interface MeasurementToday extends Document {
  value: number;
  dateTime: Date;
  tagId: string;
  sensorId?: string;
  locationId: string;
  active: boolean;
}
