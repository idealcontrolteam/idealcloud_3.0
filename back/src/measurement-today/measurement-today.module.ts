import { Module } from '@nestjs/common';
import { MeasurementTodayController } from './measurement-today.controller';
import { MeasurementTodayService } from './measurement-today.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementTodaySchema } from './schemas/measurement.schema';
import { LocationService } from 'src/location/location.service';
import { TagService } from '../tag/tag.service';
import { TagModule } from '../tag/tag.module';
import { MeasurementTodayControlService } from '../measurement-today-control/measurement-today-control.service';
import { MeasurementTodayControlModule } from '../measurement-today-control/measurement-today-control.module';

@Module({
    imports: [
      TagModule,
      MeasurementTodayControlModule,
      MongooseModule.forFeature([
        {
          name: 'MeasurementToday',
          schema: MeasurementTodaySchema,
          collection: 'measurementToday',
        },
      ]),
    ],
    controllers: [MeasurementTodayController],
    providers: [MeasurementTodayService,
                TagService,MeasurementTodayControlService
    ],
    exports: [MeasurementTodayService,TagService,MeasurementTodayControlModule],
  })
export class MeasurementTodayModule {}
