import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { CreateMeasurementTodayDTO } from './dto/measurement.dto';
  import { MeasurementTodayService } from './measurement-today.service';
  import { AuthGuard } from 'src/shared/auth.guard';
  import { MeasurementToday } from './interfaces/measurement.interface';
  import * as moment from 'moment';
import { Measurement } from '../measurement/interfaces/measurement.interface';

@Controller('measurement_today')
export class MeasurementTodayController {
    constructor(private measurementService: MeasurementTodayService) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'value' && key != 'dateTime' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post('/multiple')
  async createMeasurementMultiple(@Res() res, @Body() body) {
     //this.validateIds(body);
    let newMeasurement = {};
    body.map(async (Measurement) => { 
      // console.log(Measurement);     
      newMeasurement =  await this.measurementService.createMeasurement(Measurement);
         
    });  
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }
  @Post()
  async createMeasurement(@Res() res, @Body() body: CreateMeasurementTodayDTO) {
    this.validateIds(body);

    const newMeasurement = await this.measurementService.createMeasurement(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Get()
  async getMeasurements(@Res() res) {
    const measurements = await this.measurementService.getMeasurements();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/all')
  async getMeasurementsAll(@Res() res) {
    const measurements = await this.measurementService.getMeasurementsAll();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:measurementId')
  async getMeasurement(@Res() res, @Param('measurementId') measurementId) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    const measurement = await this.measurementService.getMeasurement(
      measurementId,
    );
    if (!measurement) {
      throw new NotFoundException('Measurement not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Get('/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfiltered(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfiltered(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Post('/tags/efectividad')
  async getEfectividadesToday(
    @Res() res,
    @Body() body: any
  ) {
    let measurements=[];
    //console.log(body)
    let tags=body.tags;
    if(tags!=undefined&&tags.length>0){
      measurements = await this.measurementService.getEfectividadesToday(
        tags,
      );
     
      let msg =
        measurements.length == 0
          ? 'Measurements not found'
          : 'Measurements fetched';
  
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: measurements,
        tags:body.tags
        //count: measurements.length,
      });
    }else{
      throw new BadRequestException('Tags vacios');
    }
    
  }

  @Get('/tag/:tagId/efectividad')
  async getEfectividadToday(
    @Res() res,
    @Param('tagId') tagId,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getEfectividadToday(
      tagId,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      //count: measurements.length,
    });
  }

  @Get('/xy/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfilteredXY(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfilteredXY(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    let prom="";
    let max="";
    let min="";
    let ultimo_valor="";
    let ultima_fecha="";
    if(measurements.length!=0){
      prom=((measurements.reduce((a, b) => +a + +b.y, 0)/measurements.length)).toFixed(1);
      max=measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
      min=measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
      ultimo_valor=measurements[measurements.length-1].y;
      ultima_fecha=moment(measurements[measurements.length-1].x).format('YYYY-MM-DD HH:mm:ss');
    }


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
      prom:prom,
      max:max,
      min:min,
      ultimo_valor:ultimo_valor,
      ultima_fecha:ultima_fecha,
    });
  }

  @Get('/xy/tag/active/:tagId/:fini/:ffin')
  async getMeasurementByTagfilteredXYActive(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfilteredXYActive(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    let prom="";
    let max="";
    let min="";
    let ultimo_valor="";
    let ultima_fecha="";
    if(measurements.length!=0){
      prom=((measurements.reduce((a, b) => +a + +b.y, 0)/measurements.length)).toFixed(1);
      max=measurements.reduce((max, b) => Math.max(max, b.y), measurements[0].y);
      min=measurements.reduce((min, b) => Math.min(min, b.y), measurements[0].y);
      ultimo_valor=measurements[measurements.length-1].y;
      ultima_fecha=moment(measurements[measurements.length-1].x).format('YYYY-MM-DD HH:mm:ss');
    }


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
      prom:prom,
      max:max,
      min:min,
      ultimo_valor:ultimo_valor,
      ultima_fecha:ultima_fecha,
    });
  }



  @Get('/sensor/:sensorId/:fini/:ffin')
  async getMeasurementBySensorfiltered(
    @Res() res,
    @Param('sensorId') sensorId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementBySensorfiltered(
      sensorId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/location/:locationId/:fini/:ffin')
  async getMeasurementByLocationfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfiltered(
      locationId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/xy/location/:locationId/:fini/:ffin')
  async getMeasurementByLocationfilteredXY(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfilteredXY(
      locationId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Put('/:measurementId')
  async updateMeasurement(
    @Res() res,
    @Body() body: CreateMeasurementTodayDTO,
    @Param('measurementId') measurementId,
  ) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    this.validateIds(body);

    const updatedMeasurement = await this.measurementService.updateMeasurement(
      measurementId,
      body,
    );
    if (!updatedMeasurement) {
      throw new NotFoundException('Measurement not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement updated',
      data: updatedMeasurement,
    });
  }

  @Delete('/:measurementId')
  async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
    const deletedMeasurement = await this.measurementService.deleteMeasurement(
      measurementId,
    );

    if (!deletedMeasurement) {
      throw new NotFoundException('Measurement not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement deleted',
      data: deletedMeasurement,
    });
  }
}
