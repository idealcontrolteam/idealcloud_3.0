export class CreateMeasurementTodayDTO {
  value: number;
  dateTime: Date;
  tagId: string;
  sensorId?: string;
  locationId: string;
  active: boolean;
}
