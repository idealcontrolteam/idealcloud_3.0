import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { MeasurementToday } from './interfaces/measurement.interface';
import { TagService } from '../tag/tag.service';
import { MeasurementTodayControlService } from '../measurement-today-control/measurement-today-control.service';
import { CreateMeasurementTodayDTO } from './dto/measurement.dto';
import * as moment from 'moment';

@Injectable()
export class MeasurementTodayService {
    constructor(
        @InjectModel('MeasurementToday') private measurementModel: Model<MeasurementToday>,
        private tagService: TagService,
        private measurementControlService: MeasurementTodayControlService,
      ) {}
    
      async getMeasurements(): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find();
        return measurements;
      }
    
      async getMeasurementsAll(): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel
          .find()
          .populate('tagId sensorId locationId');
        return measurements;
      }
    
      async getMeasurementByTagfiltered(tagId, fini, ffin): Promise<MeasurementToday[]> {
    
        const measurements = await this.measurementModel.find({
          tagId: tagId,
          dateTime: { $gte: fini, $lte: ffin },
        }).sort({dateTime: 1});
    
        return measurements;
      }

      async getEfectividadesToday(tags): Promise<any> {
        let now=new Date();
        let f1=moment(now).subtract(1, 'days').format('YYYY-MM-DDT12:00:00') + ".000Z";
        let f2=moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
        let minutos_dia= moment(f1).diff(moment(f2), 'minutes')*-1;
        let registros_100=minutos_dia/5;
        let ids_tag=[];
        let _id_tag=[];
        let ids_location=[];
        let _id_location=[];
        tags.map(t=>{
          ids_tag.push({tagId:t});
          _id_tag.push({_id:t})
        })
        const find_tags = await this.tagService.getAnyTags(_id_tag);
        let new_measurements=[];
        
        let measurements = await this.measurementModel.find({
          $or:ids_tag,
          dateTime: { $gte: f1, $lte: f2 },
          value: { $ne: 0 },
        }).sort({dateTime: 1});
        let control=false;

        if(measurements.length==0){
          let measurement_control=[];
          control=true;
          //console.log("h")
          find_tags.map(t=>{
            ids_location.push({id_dispositivos:t.locationId});
            _id_location.push({_id:t.locationId})
          })
          measurement_control = await this.measurementControlService.getMeasurementEfectivity({
            $or:ids_location,
            fecha_registros: { $gte: f1, $lte: f2 },
            oxd: { $ne: 0 },
          })
          ids_location.map(l=>{
            let med=[],medicion=0;
            med=measurement_control.filter(m=>m.id_dispositivos==l.id_dispositivos);
            medicion=measurement_control.filter(m=>m.id_dispositivos==l.id_dispositivos).length;
            //console.log(find_tags)
            let tag=find_tags.filter(m=>m.locationId==l.id_dispositivos)[0]
            medicion>0?
            new_measurements.push({
              my_register:medicion,
              register_100:registros_100,
              porcentaje_efectividad:medicion*100/registros_100,
              _id:l.id_dispositivos,
              locationId:med[0].id_dispositivos!=undefined?med[0].id_dispositivos:"",
              workplaceId:tag.workplaceId
            }):new_measurements.push({
              my_register:medicion,
              register_100:registros_100,
              porcentaje_efectividad:medicion*100/registros_100,
              _id:l.id_dispositivos,
              workplaceId:tag.workplaceId,
            })
          })
        }else{
          tags.map(t=>{
            let med=[],medicion=0;
            if(control){
              med=measurements.filter(m=>m.tagId==t);
              medicion=measurements.filter(m=>m.tagId==t).length;
            }else{
              med=measurements.filter(m=>m.tagId==t);
              medicion=measurements.filter(m=>m.tagId==t).length;
            }
            let tag=find_tags.filter(m=>m._id==t)[0]
            medicion>0?
            new_measurements.push({
              my_register:medicion,
              register_100:registros_100,
              porcentaje_efectividad:medicion*100/registros_100,
              _id:t,
              locationId:med[0].locationId!=undefined?med[0].locationId:"",
              workplaceId:tag.workplaceId
            }):new_measurements.push({
              my_register:medicion,
              register_100:registros_100,
              porcentaje_efectividad:medicion*100/registros_100,
              _id:t,
              workplaceId:tag.workplaceId,
            })
          })
        }
        //console.log(measurements)
        
        
    
        return new_measurements;
        
      }

      async getEfectividadToday(tagId): Promise<any> {
        let now=new Date();
        let f1=moment(now).format('YYYY-MM-DDT00:00:00') + ".000Z";
        let f2=moment(now).format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
        let minutos_dia= moment(f1).diff(moment(f2), 'minutes')*-1;
        let registros_100=minutos_dia/5;
       
        const measurements = await this.measurementModel.countDocuments({
          tagId: tagId,
          dateTime: { $gte: f1, $lte: f2 },
          value: { $ne: 0 },
        }).sort({dateTime: 1});
    
        return {
          my_register:measurements,
          register_100:registros_100,
          porcentaje_efectividad:measurements*100/registros_100,
          _id:tagId
        };
      }
    
      async getMeasurementByTagfilteredXY(tagId, fini, ffin): Promise<any[]> {
    
        const measurements = await this.measurementModel.find({
          tagId: tagId,
          dateTime: { $gte: fini, $lte: ffin },
        }).sort({dateTime: 1});
    
        //  let i = 0;
    
        //  const ColorChart =["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
    
         //const tags=await this.TagService.getTagsAll();
          // for (let i = 0; i < measurements.length; i++) {
          //   let sw =0
          //   tags.forEach((tag)=>{
          //     if(tag._id==measurements[i]._id)
          //       this.loadDataChart(tag.shortName, APItagMediciones, ColorChart[i],TagsSelecionado[i].unity,sw);
          //   })
          // }
    
        let xy=[];
    
    
         measurements.map( item => {
            var fecha=new Date(item.dateTime);
            var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
            zona_horaria=zona_horaria/60;
            fecha.setHours(fecha.getHours()+zona_horaria);
    
          xy.push({ x: fecha.getTime() , y : item.value });
        });
    
        return xy;
      }
    
      async getMeasurementByTagfilteredXYActive(tagId, fini, ffin): Promise<any[]> {
    
        const measurements = await this.measurementModel.find({
          tagId: tagId,
          active: true,
          dateTime: { $gte: fini, $lte: ffin },
        }).sort({dateTime: 1});
    
        //  let i = 0;
    
        //  const ColorChart =["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
    
         //const tags=await this.TagService.getTagsAll();
          // for (let i = 0; i < measurements.length; i++) {
          //   let sw =0
          //   tags.forEach((tag)=>{
          //     if(tag._id==measurements[i]._id)
          //       this.loadDataChart(tag.shortName, APItagMediciones, ColorChart[i],TagsSelecionado[i].unity,sw);
          //   })
          // }
    
        let xy=[];
    
    
         measurements.map( item => {
                var fecha=new Date(item.dateTime);
            var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
            zona_horaria=zona_horaria/60;
            fecha.setHours(fecha.getHours()+zona_horaria);
    
          xy.push({ x: fecha.getTime() , y : item.value });
        });
    
        return xy;
      }
    
      async getMeasurementFilteredByTagsAndDate(
        tags,
        fini,
        ffin,
      ): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          tagId: { $in: tags },
          dateTime: { $gte: fini, $lte: ffin },
        });
    
        //console.log(tags);
        return measurements;
      }
    
      async getMeasurementBySensorfiltered(
        sensorId,
        fini,
        ffin,
      ): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          sensorId: sensorId,
          dateTime: { $gte: fini, $lte: ffin },
        });
        return measurements;
      }
    
      async getMeasurementByLocationfiltered(
        locationId,
        fini,
        ffin,
      ): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          locationId: locationId,
          dateTime: { $gte: fini, $lt: ffin },
        });
        return measurements;
      }
    
      async getMeasurementByLocationfilteredXY(
        locationId,
        fini,
        ffin,
      ): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          locationId: locationId,
          dateTime: { $gte: fini, $lt: ffin },
        }).sort({dateTime: 1});
        
        
        let xy=[];
    
    
         measurements.map( item => {
                var fecha=new Date(item.dateTime);
            var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
            zona_horaria=zona_horaria/60;
            fecha.setHours(fecha.getHours()+zona_horaria);
    
          xy.push({ x: fecha.getTime() , y : item.value });
        });
    
        return xy;
      }
    
      async getMeasurement(id): Promise<MeasurementToday> {
        const measurement = await this.measurementModel.findById(id);
        return measurement;
      }
    
      async getMeasurementsByLocationId(locationId): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          locationId: locationId,
        });
        return measurements;
      }
    
      async getMeasurementsBySensorId(sensorId): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          sensorId: sensorId,
        });
        return measurements;
      }
    
      async getMeasurementsByTagId(tagId): Promise<MeasurementToday[]> {
        const measurements = await this.measurementModel.find({
          tagId: tagId,
        });
        //console.log(measurements.dateTime);
        return measurements;
      }
    
      async createMeasurement(
        createMeasurementDTO: CreateMeasurementTodayDTO,
      ): Promise<MeasurementToday> {
        const newMeasurement = new this.measurementModel(createMeasurementDTO);
        //console.log(newMeasurement)
        return await newMeasurement.save();
      }
    
      async deleteMeasurement(id): Promise<MeasurementToday> {
        return await this.measurementModel.findByIdAndDelete(id);
      }
    
      async updateMeasurement(
        id: string,
        body: CreateMeasurementTodayDTO,
      ): Promise<MeasurementToday> {
        return await this.measurementModel.findByIdAndUpdate(id, body, {
          new: true,
        });
      }
}
