export class CreateTagTypeDTO {
  name: string;
  active: boolean;
}
