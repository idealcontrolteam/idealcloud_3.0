import { Module } from '@nestjs/common';
import { ViewsController } from './views.controller';
import { ViewsService } from './views.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ViewSchema } from './view.schema';




@Module({
    imports: [
      MongooseModule.forFeature([
        { name: 'View1', schema: ViewSchema, collection: 'view1' },
      ]),
    ],
    controllers: [ViewsController],
    providers: [ViewsService],
    exports: [ViewsService],
  })
export class ViewsModule {}