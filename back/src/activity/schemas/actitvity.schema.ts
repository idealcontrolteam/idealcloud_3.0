import { Schema } from 'mongoose';
export const ActivitySchema = new Schema(
  {
    detail: String,
    active: Boolean,
    dateTime: Date,
  },
  { versionKey: false },
);

