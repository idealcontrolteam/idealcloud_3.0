import { Module } from '@nestjs/common';
import { ActivityController } from './activity.controller';
import { ActivityService } from './activity.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ActivitySchema } from './schemas/actitvity.schema'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Activity', schema: ActivitySchema, collection: 'activity' },
    ]),
  ],
  controllers: [ActivityController],
  providers: [ActivityService]
})
export class ActivityModule {}
