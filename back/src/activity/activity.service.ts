import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateActivityDTO } from './dto/activity.dto';
import { Activity } from './interfaces/activity.interface';

@Injectable()
export class ActivityService {
    constructor(@InjectModel('Activity') private activityModel: Model<Activity>) {}
    
      async getActivityAll(): Promise<Activity[]> {
        const activitys = await this.activityModel.find();
        return activitys;
      }
    
    //   async getAlarmsByTagId(tagId): Promise<Activity[]> {
    //     const activitys = await this.activityModel.find({ tagId: tagId });
    //     return activitys;
    //   }
    
      async getActivity(id): Promise<Activity> {
        const activity = await this.activityModel.findById(id);
        return activity;
      }
    
      async createActivity(createActivityDTO: CreateActivityDTO): Promise<Activity> {
        const newActivity = new this.activityModel(createActivityDTO);
        return await newActivity.save();
      }
    
      async deleteActivity(id): Promise<Activity> {
        return await this.activityModel.findByIdAndDelete(id);
      }
    
      async updateActivity(id: string, body: CreateActivityDTO): Promise<Activity> {
        return await this.activityModel.findByIdAndUpdate(id, body, {
          new: true,
        });
      }
    
    //   async getAlarmByfilteredDate(   
    //     fini,
    //     ffin,
    //   ): Promise<Activity[]> {
    //     const alarms = await this.activityModel.find({      
    //       dateTimeIni: { $gte: fini, $lte: ffin },
    //     });
    //     return alarms;
    //   }
}
